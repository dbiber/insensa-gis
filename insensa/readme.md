# How to build and start Insensa-GIS #

## Install java (JDK) ##
 Although the minimum requirement for Insensa-GIS is 1.6, 
 it is recommended to use the most recent version if possible.
 
## Install Ant ##
 Installation instructions can be found here:
 http://ant.apache.org/manual/install.html
    
## create ant.properties ##
 create a file called "ant.properties" within the insensa/ant directory
 Insensa-gis depends on the native library gdal.
 
 - i.system=<lin64,win64 or mac64>

 

## Get/install gdal ##
 The latest tested gdal version is 1.11.1
 There are binaries for all major operating systems on insensa.org.
 Usually you do not have to do anything because the ant **deploy** target
 will download and unzip the gdal libraries for you.

Of course it is also possible to build gdal from sources.
Either way, the following criterias are important so that insensa can find and work with gdal.

1. gdal.jar has to be in the lib directory together with the other third-person libraries  
2. The native jni (eg. gdaljni.dll, libgdal.so) libraries can be placed wherever you want, is is only important that insensa-gis   
  is aware of the directory. This could be done with the java statement **-Djava.library.path=\<PATH\>**  

  By default these libraries are placed whithin the **lib** directory and insensa-gis can be startet with:  
  **java -Djava.library.path=lib -jar insensa.jar**

3. The gdal jni libraries will dynamically load the necesarry native gdal libraries.
   The native gdal files (eg. gdal.dll, libgdal.so...) usually are in a system directory.

### Windows ###
If you want to take a closer look on the dll Search Order on Windows, take a look at
[Dynamic-Link Library Search Order](https://msdn.microsoft.com/en-us/library/windows/desktop/ms682586%28v=vs.85%29.aspx)
Point 6 under **Standard Search Order for Desktop Applications** describes that all directories
listed in the PATH environment variable are searched for Dynamic-Link Libraries (dll), 
therefore default way would be to register the gdal directory right before starting insensa gis.

1. cd \<insensa\>/tmp  
2. PATH=%PATH%;lib/gdal  
You are free to choose whatever option you like

### Linux ###
To ensure that the gdal jni libraries can find the dynamic gdal libraries the library (libgdal.so) 
has to be within the system environment. If you install gdal with your package manager, this should already be done
because linux is automatically searching some system default directories (like /usr/lib).
If you rather want to use the libraries delivered with this sources (wich is prefered) you have to adjust
the environment variable "LD_LIBRARY_PATH". Set it to the path where "libgdal.so is stored. This can be relative to the
main directory

**Example**

1. cd \<insensa\>/tmp
2. export LD_LIBRARY_PATH=lib/lib64/gdal
3. java -Djava.library.path=lib/lin64 -jar insensa.jar


### MAC-OS ###

1. cd INSENSA/tmp
2. DYLD_LIBRARY_PATH=lib/:lib/gdal/
3. java -jar insensa.jar

## configure properties for gdal ##
within the directory **insensa/ant** create a file **ant.properties** ans insert the following lines  
> gdal.libraries=\<LIBRARY PATH\>  
> gdal.jni=\<JNI LIBRARY PATH\>  
> gdal.data=\<GDAL DATA DIRECTORY\>  

## Build Insensa-GIS ##
 - Open a terminal and go to the insensa/ant directory
 - Within this directory execute "ant deploy"
 After executing this ant command, a complete insensa installation bundle should 
 have been build within a "tmp" directory!
 
 If you try to start gdal now you will get an error:
 
  ```
  java -jar insensa.jar
  java.lang.NoClassDefFoundError:org/gdal/gdal...
  ```
 This is because gdal has do be installed separately.
 
 If you already have gdal installed correctly the the process is done and you can start
 Insensa-GIS with "java -jar insensa.jar". Otherwise continue with gdal installation
 
 
   
   
## Get/Install gdal ##
The latest tested gdal version is 1.11.1
You can either get the build binaries on insensa.org or get the source from gdal.org and build it yourself.

### Windows 32Bit ###

## troubleshooting ##
  1. Window opens: "Error Opening libraries: org.gdal.gdal.gdalJNI.AllRegister()V"
   Terminal shows: "Native library load failed. java.lang.UnsatisfiedLinkError: no gdaljni in java.library.path"
    1. Missing jni library files. Put all native files from <GDAL>/bin/gdal/java to <INSENSA>/lib
    2. Start insensa with: java -Djava.library.path=lib -jar insensa.jar
  2. Window opens: "Error Opening libraries: org.gdal.gdal.gdalJNI.AllRegister()V"
     Terminal shows: "...gdaljni.dll: can't find dependent libraries" <br>
     1. The native jdal binary libraries cannot be found (gdal.dll...)
     2. On Windows Add lib/gdal to PATH (PATH=%PATH%;lib/gdal), on Linux create Environment variable LD_LIBRARY_PATH 
     (export LD_LIBRARY_PATH=lib/gdal)



   

 
 
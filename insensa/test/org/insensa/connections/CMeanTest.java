package org.insensa.connections;

import org.insensa.IRasterGisFile;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.Mockito;

import java.io.IOException;

import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;

public class CMeanTest extends AbstractConnectionTest {

  @Override
  protected ConnectionFileChanger createConnection() {
    return new CMean();
  }

  @Override
  protected int getRows() {
    return super.getRows();
  }

  @Override
  protected int getCols() {
    return 6;
  }

  @Override
  protected float[] readRaster1() {
    return new float[]{1.0f, 2.0f, 3.0f, 4.0f, 9999.0f, 6.0f};
  }

  @Override
  protected float[] readRaster2() {
    return new float[]{11.0f, 3.0f, 3.0f, 999.0f, 9999.0f, 6.0f};
  }

  @Test
  public void write() throws IOException {
    connection.write(factory);

    Mockito.verify(rasterOutputFile, times(1))
        .writeRaster(anyInt(),
            anyInt(),
            anyInt(),
            anyInt(),
            AdditionalMatchers
                .aryEq(new float[]{6f, 2.5f, 3f, 501.5f, -9999.0f, 6f}));

    Mockito.verify(rasterOutputFile, times(1))
        .createNewFile(anyObject(), anyObject(), anyDouble());
  }

}
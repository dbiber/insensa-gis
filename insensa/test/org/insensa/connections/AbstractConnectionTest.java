package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IGisFileContainerFactory;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.junit.Before;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

abstract public class AbstractConnectionTest {

  @Mock
  RasterFileContainer sourceFile1;
  @Mock
  RasterFileContainer sourceFile2;
  @Mock
  RasterFileContainer outputFile;
  @Mock
  IRasterGisFile rasterGisFile1;
  @Mock
  IRasterGisFile rasterGisFile2;
  @Mock
  IRasterGisFile rasterOutputFile;
  @Mock
  IGisFileContainerFactory factory;
  @Captor
  ArgumentCaptor argCaptor;
  ConnectionFileChanger connection;

  @Before
  public void init_tested_and_mocks() {
    MockitoAnnotations.initMocks(this);
  }

  protected abstract ConnectionFileChanger createConnection();

  @Before
  public void setUp() throws Exception {

    when(sourceFile1.getGisFile()).thenReturn(rasterGisFile1);
    when(sourceFile1.getPathRelativeToProject()).thenReturn("Filesets/MyPath1/");
    when(sourceFile1.getOutputFileName()).thenReturn("Filesets/MyPath1/myfile1");
    when(rasterGisFile1.getNRows()).thenReturn(getRows());
    when(rasterGisFile1.getNCols()).thenReturn(getCols());
    when(rasterGisFile1.getNoDataValue()).thenReturn(9999.0f);
    when(rasterGisFile1.getAbsolutePath()).thenReturn("My/Path1/myfile1.tif");

    when(sourceFile2.getGisFile()).thenReturn(rasterGisFile2);
    when(sourceFile2.getPathRelativeToProject()).thenReturn("Filesets/MyPath2/");
    when(sourceFile2.getOutputFileName()).thenReturn("Filesets/MyPath2/myfile2");
    when(rasterGisFile2.getNRows()).thenReturn(getRows());
    when(rasterGisFile2.getNCols()).thenReturn(getCols());
    when(rasterGisFile2.getNoDataValue()).thenReturn(9999.0f);
    when(rasterGisFile2.getAbsolutePath()).thenReturn("My/Path2/myfile2.tif");
    when(outputFile.getGisFile()).thenReturn(rasterOutputFile);

    when(factory.create(anyObject(),anyString(),anyString(),anyBoolean(),anyObject()))
        .thenReturn(outputFile);
    List<IGisFileContainer> list = Arrays.asList(sourceFile1,sourceFile2);
    this.connection = createConnection();
    connection.setSourceFileList(list);
    connection.setOutputFileContainer(outputFile);

    when(rasterGisFile1.readRaster(anyInt(), anyInt(), anyInt(), anyInt()))
        .thenReturn(readRaster1());
    when(rasterGisFile2.readRaster(anyInt(), anyInt(), anyInt(), anyInt()))
        .thenReturn(readRaster2());
  }

//  @Test
//  public void write() throws IOException {
//    Mockito.verify(rasterOutputFile, times(1))
//        .createNewFile(anyObject(), anyObject(), anyDouble());
//  }


  protected int getRows() {
    return 1;
  }

  protected int getCols() {
    return 7;
  }

  protected abstract float[] readRaster1();

  protected abstract float[] readRaster2();

}

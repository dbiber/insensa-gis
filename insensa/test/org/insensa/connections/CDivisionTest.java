package org.insensa.connections;

import org.insensa.IRasterGisFile;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;

public class CDivisionTest extends AbstractConnectionTest {

  @Override
  protected ConnectionFileChanger createConnection() {
    return new CDivision();
  }

  @Override
  protected float[] readRaster1() {
    return new float[]{13.0f, 2.0f, 3.0f, 4.0f, 9999.0f, 6.0f, 0f};
  }

  @Override
  protected float[] readRaster2() {
    return new float[]{5f, 3.0f, 3.0f, 999.0f, 9999.0f, 6.0f, -3f};
  }

  @Test
  public void write() throws IOException {
    ((CDivision)connection).setDividend(super.sourceFile1);
    ((CDivision)connection).setDivisor(super.sourceFile2);
    connection.write(factory);
    Mockito.verify(rasterOutputFile, times(1))
        .writeRaster(anyInt(),
            anyInt(),
            anyInt(),
            anyInt(),
            AdditionalMatchers
                .aryEq(new float[]{2.6f, 0.6666667f, 1f, 0.004004004f, Float.MAX_VALUE,1f, -0f}));
    Mockito.verify(rasterOutputFile, times(1))
        .createNewFile(anyObject(), anyObject(), anyDouble());
  }
}
package org.insensa.connections;

import org.insensa.IRasterGisFile;
import org.insensa.extensions.IPluginExec;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.MeanValue;
import org.insensa.inforeader.MinMaxValues;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.io.IOException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class CIndexationTest extends AbstractConnectionTest {

  @Spy
  CountValues spyCountValues = new CountValues();
  @Spy
  MinMaxValues spyMinMaxValues1 = new MinMaxValues();
  @Spy
  MinMaxValues spyMinMaxValues2 = new MinMaxValues();
  @Spy
  MeanValue spyMeanValue1 = new MeanValue();
  @Spy
  MeanValue spyMeanValue2 = new MeanValue();


  public CIndexationTest() throws IOException {
  }

  @Override
  protected ConnectionFileChanger createConnection() {
    when(spyMinMaxValues1.getMinValue()).thenReturn(0.00001f);
    when(spyMinMaxValues1.getMaxValue()).thenReturn(6f);
    when(sourceFile1.getInfoReader(MinMaxValues.NAME)).thenReturn(spyMinMaxValues1);

    when(spyMinMaxValues2.getMinValue()).thenReturn(0.00011f);
    when(spyMinMaxValues2.getMaxValue()).thenReturn(999.0f);
    when(sourceFile2.getInfoReader(MinMaxValues.NAME)).thenReturn(spyMinMaxValues2);

    when(spyMeanValue1.getMeanValue()).thenReturn(2.666668);
    when(sourceFile1.getInfoReader(MeanValue.NAME)).thenReturn(spyMeanValue1);
    when(spyMeanValue2.getMeanValue()).thenReturn(170.3334);
    when(sourceFile2.getInfoReader(MeanValue.NAME)).thenReturn(spyMeanValue2);

    ExecDependencyController execDependencyController1 = new ExecDependencyController() {
      @Override
      public synchronized void checkInfoDependencies(IPluginExec pluginExec) {
      }
    };
    ExecDependencyController execDependencyController2 = new ExecDependencyController() {
      @Override
      public synchronized void checkInfoDependencies(IPluginExec pluginExec) {
      }
    };
    when(sourceFile1.getDepChecker()).thenReturn(execDependencyController1);
    when(sourceFile2.getDepChecker()).thenReturn(execDependencyController2);
    return new CIndexation();
  }

  @Override
  protected float[] readRaster1() {
    return new float[]{1.0f, 2.0f, 3.0f, 4.0f, 9999.0f, 6.0f, 0.00001f};
  }

  @Override
  protected float[] readRaster2() {
    return new float[]{11.0f, 3.0f, 3.0f, 999.0f, 9999.0f, 6.0f, 0.00011f};
  }

  @Test
  public void write() throws IOException {
    ((CIndexation) connection).getWeighting(sourceFile1).weight = 1.0f;
    ((CIndexation) connection).getWeighting(sourceFile1).penalty = new CIndexation.Threshold();
    ((CIndexation) connection).getWeighting(sourceFile1).penalty.setActivate(false);
    ((CIndexation) connection).getWeighting(sourceFile1).reward = new CIndexation.Threshold();
    ((CIndexation) connection).getWeighting(sourceFile1).reward.setActivate(false);

    ((CIndexation) connection).getWeighting(sourceFile2).weight = 2.0f;
    ((CIndexation) connection).getWeighting(sourceFile2).penalty = new CIndexation.Threshold();
    ((CIndexation) connection).getWeighting(sourceFile2).penalty.setActivate(false);
    ((CIndexation) connection).getWeighting(sourceFile2).reward = new CIndexation.Threshold();
    ((CIndexation) connection).getWeighting(sourceFile2).reward.setActivate(false);

    ((CIndexation) connection).setMethod(CIndexation.AggregationMethod.ADDITIVE);

    connection.write(factory);

    Mockito.verify(rasterOutputFile, times(1))
        .writeRaster(anyInt(),
            anyInt(),
            anyInt(),
            anyInt(),
            (float[]) argCaptor.capture());
    float[] myF = (float[]) argCaptor.getValue();
    Assert.assertArrayEquals(new float[]{23f, 8f, 9f, 2002f, -Float.MAX_VALUE, 18f, 0.00023f},
        myF, 0.000001f);
  }


}
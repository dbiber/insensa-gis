package org.insensa.connections;

import org.insensa.IGisFileSet;
import org.insensa.IRasterGisFile;
import org.insensa.model.storage.ISavableSaver;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.IOException;

import static org.mockito.Matchers.anyFloat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CThresholdVolatilityTest extends AbstractConnectionTest {


  @Mock
  ISavableSaver saver;
  @Mock
  IGisFileSet gisFileSet;

//  @Before
//  public void setUp() throws Exception {
//    when(gisFileSet.getName()).thenReturn("MyFileSet");
//  }
//
//  @Before
//  public void init_tested_and_mocks() {
//    MockitoAnnotations.initMocks(this);
//  }

  @Override
  protected ConnectionFileChanger createConnection() {
    return new CThresholdVolatility();
  }

  @Override
  protected float[] readRaster1() {
    return new float[0];
  }

  @Override
  protected float[] readRaster2() {
    return new float[0];
  }


  @Test @Ignore
  public void saveData() throws IOException {
    connection.setOutputFileSet(gisFileSet);
    connection.save(saver);
    Mockito.verify((saver), times(1))
        .save(eq("threshold1"),anyInt());
    Mockito.verify((saver), times(1))
        .save(eq("threshold2"),anyInt());

    connection.write(factory);

    verify(saver,times(2))
        .save(eq("threshold1"), anyFloat());

  }

}
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IGisFileContainerFactory;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class CoefficientOfVariationTest {

  @Mock
  RasterFileContainer sourceFile1;
  @Mock
  RasterFileContainer sourceFile2;
  @Mock
  RasterFileContainer outputFile;
  @Mock
  IRasterGisFile rasterGisFile1;
  @Mock
  IRasterGisFile rasterGisFile2;
  @Mock
  IRasterGisFile rasterOutputFile;
  @Mock
  IGisFileContainerFactory factory;

  @Captor
  ArgumentCaptor argCaptor;

  ConnectionFileChanger connection = new CoefficientOfVariation();

  @Before
  public void init_tested_and_mocks() {
    MockitoAnnotations.initMocks(this);
  }

  @Before
  public void setUp() throws Exception {
    when(sourceFile1.getGisFile()).thenReturn(rasterGisFile1);
    when(rasterGisFile1.getNRows()).thenReturn(1);
    when(rasterGisFile1.getNCols()).thenReturn(6);
    when(rasterGisFile1.getNoDataValue()).thenReturn(9999.0f);
    when(rasterGisFile1.getAbsolutePath()).thenReturn("My/Path1/myfile.tif");

    when(sourceFile2.getGisFile()).thenReturn(rasterGisFile2);
    when(rasterGisFile2.getNRows()).thenReturn(1);
    when(rasterGisFile2.getNCols()).thenReturn(6);
    when(rasterGisFile2.getNoDataValue()).thenReturn(9999.0f);
    when(rasterGisFile2.getAbsolutePath()).thenReturn("My/Path2/myfile.tif");
    when(outputFile.getGisFile()).thenReturn(rasterOutputFile);

    when(factory.create(anyObject(), anyString(), anyString(), anyBoolean(), anyObject()))
        .thenReturn(outputFile);
    List<IGisFileContainer> list = Arrays.asList(sourceFile1, sourceFile2);
    connection.setSourceFileList(list);
    connection.setOutputFileContainer(outputFile);
  }

  @Test
  public void mainTest() throws IOException {

    when(rasterGisFile1.readRaster(anyInt(), anyInt(), anyInt(), anyInt()))
        .thenReturn(new float[]{1.0f, 2.0f, 3.0f, 4.0f, 9999.0f, 6.0f,0.00001f});

    when(rasterGisFile2.readRaster(anyInt(), anyInt(), anyInt(), anyInt()))
        .thenReturn(new float[]{11.0f, 3.0f, 3.0f, 999.0f, 9999.0f, 6.0f,0.00011f});
    connection.write(factory);

    Mockito.verify(rasterOutputFile, times(1))
        .writeRaster(anyInt(),
            anyInt(),
            anyInt(),
            anyInt(),
            (float[]) argCaptor.capture());
    float[] myF = (float[]) argCaptor.getValue();
    Assert.assertArrayEquals(new float[]{0.833333f, 0.2f, 0f, 0.9920239f, -Float.MAX_VALUE, 0f},
        myF, 0.000001f);
  }
}
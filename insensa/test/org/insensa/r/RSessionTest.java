package org.insensa.r;

import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class RSessionTest {

  @Parameterized.Parameter(0)
  public VariableType type;

  @Parameterized.Parameters(name = "{index}: {0}")
  public static Iterable<? extends Object> data() {
    return Arrays.asList(VariableType.values());
  }

  @Test
  public void testImplementationOfAllVariableTypes() throws REngineException, REXPMismatchException {
//        IScriptConnection connection = Mockito.mock(IScriptConnection.class);
//        RSession session = new RSession(connection);
//        IVariable var = Mockito.spy(IVariable.class);
//        IValue val = Mockito.mock(IValue.class);
//
//        when(val.toScriptString()).thenReturn("1");
//        when(var.getValue()).thenReturn(Optional.of(val));
//        when(var.getName()).thenReturn("name");
//        when(var.getType()).thenReturn(type);
//
//
//        session.assign(var);
  }

}
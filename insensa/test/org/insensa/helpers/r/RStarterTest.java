/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.helpers.r;

import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.insensa.updater.main.InsensaUpdater;
import org.insensa.updater.utils.PathUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import java.io.File;

public class RStarterTest {

  @Test
  @Ignore
  public void testCheckLocalRserve() {
    RStarter.checkLocalRserve();
    RSession rSession = null;
    try {
      rSession = new RSession("127.0.0.1");
      rSession.loadOutput(RMessageType.ALL);
      rSession.unloadOuput();
      rSession.closeAndClear();
    } catch (REXPMismatchException | REngineException e) {
      e.printStackTrace();
    } finally {
      if(rSession != null) {
        try {
          rSession.closeAndClear();
        } catch (REXPMismatchException | REngineException e) {
          e.printStackTrace();
        }
      }
    }
  }

}

package org.insensa;

import org.apache.commons.io.FileUtils;
import org.jdom.JDOMException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.*;

@Category(org.insensa.Integration.class)
public class UserTest {

  public static final File OUTPUT_PATH = new File("build/test-results/intTest");

  @Before
  public void setUp() throws Exception {
    File file = new File(OUTPUT_PATH, "TestProject2");
    if(file.exists()) {
      FileUtils.forceDelete(file);
    }
  }

  @Test
  public void createTest() throws IOException, JDOMException {
    User user = new User("Iam", OUTPUT_PATH.getAbsolutePath());
    user.addNewProject("TestProject2", Collections.emptyList());
    Project project = user.getProject("TestProject2");
    project.create();
    Assert.assertNotNull(project);

    project.addFileSet("NewFileset1");
    IGisFileSet fileSet = project.getFileSetByPath("/FileSets/NewFileset1/");
    assertNotNull(fileSet);
  }

}
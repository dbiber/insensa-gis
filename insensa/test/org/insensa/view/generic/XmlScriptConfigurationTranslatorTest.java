package org.insensa.view.generic;

import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IScriptConfigurationTranslator;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.XmlScriptConfigurationTranslator;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.IntegerValue;
import org.insensa.model.generic.value.ParameterListValue;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class XmlScriptConfigurationTranslatorTest {

  @Test
  public void parseXmlFile_WithSubParameter_NoErrors() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects.requireNonNull(classLoader
        .getResource("import-settings.xml"))
        .getFile());
    translator.parse(file);

    Optional<IVariable> oVariable =  translator.getVariable("bio");
    Assert.assertTrue(oVariable.isPresent());
    IVariable var = oVariable.get();
    List<IParameter> parentPar = var.getParameter();
    Assert.assertEquals(1, parentPar.size());
    Assert.assertEquals(19, parentPar.get(0).getParameter().size());
    Assert.assertEquals("BIO9",
        parentPar.get(0).getParameter().get(8).getName());
    Assert.assertEquals("Mean Temperature of Driest Quarter",
        parentPar.get(0).getParameter().get(8).getValue());
  }

  @Test
  public void parseXmlFileForSelectOne_WithThreeParameter() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects.requireNonNull(classLoader
        .getResource("select-one-without-defaults.xml"))
        .getFile());
    translator.parse(file);
    Optional<IVariable> oVariable =  translator.getVariable("variable");

    Assert.assertTrue(oVariable.isPresent());
    IVariable var = oVariable.get();

    List<IParameter> parameters = var.getParameter().get(0).getParameter();
    Assert.assertEquals(3,parameters.size());
  }

  @Test
  public void parseXmlFile_SelectOneType_WithDefault() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects.requireNonNull(classLoader
        .getResource("import-settings.xml"))
        .getFile());
    translator.parse(file);
    Optional<IVariable> oVariable =  translator.getVariable("bio");

    Assert.assertTrue(oVariable.isPresent());
    IVariable var = oVariable.get();

    Assert.assertTrue(var.getValue().isPresent());
    Assert.assertTrue(var.getValue().get() instanceof IParameter);
    Assert.assertEquals("BIO4",((IParameter)var.getValue().get()).getName());
  }



  @Test
  public void parseXmlFile_SelectMultipleType_WithDefaultValues() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects.requireNonNull(classLoader
        .getResource("import-settings.xml"))
        .getFile());
    translator.parse(file);
    translator.setActiveGroup("bio");
    Optional<IVariable> oVariable =  translator.getVariable("layers");

    Assert.assertTrue(oVariable.isPresent());
    IVariable var = oVariable.get();

    Assert.assertTrue(var.getValue().isPresent());
    Assert.assertTrue(var.getValue().get() instanceof ParameterListValue);
    Assert.assertEquals(3,((ParameterListValue)var.getValue()
        .get()).getDataList().size());
    List<IParameter> parameterList = ((ParameterListValue) var.getValue()
        .get()).getDataList();
    Assert.assertEquals("bio4",parameterList.get(0).getValue());
    Assert.assertEquals("bio9",parameterList.get(1).getValue());
    Assert.assertEquals("bio17",parameterList.get(2).getValue());
  }

  @Test
  public void parseXmlFile_Numeric_WithDefaultValues() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects.requireNonNull(classLoader
        .getResource("import-settings.xml"))
        .getFile());
    translator.parse(file);
    Optional<IVariable> oVariable =  translator.getVariable("index");

    Assert.assertTrue(oVariable.isPresent());
    IVariable var = oVariable.get();

    Assert.assertTrue(var.getValue().isPresent());
    IValue val = var.getValue().get();
    Assert.assertTrue(val instanceof IntegerValue);
    IntegerValue iVal = (IntegerValue) val;
    Assert.assertEquals(1, iVal.getValue().intValue());
  }


  @Test
  public void paseXmlFile_noError() throws ScriptParseException, IOException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("test.xml").getFile());
    translator.parse(file);
  }

  @Test
  public void pasterStringStream_noParam_noError() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    translator.parse("<settings><variable name=\"asd\" type=\"string\"/></settings>");
    List<IVariable> variableList = translator.getVariables();
    Assert.assertEquals(1, variableList.size());
    Assert.assertEquals("asd",variableList.get(0).getName());
    Assert.assertEquals(VariableType.STRING, variableList.get(0).getType());
  }

  @Test
  public void pasterStringStream_withParams_noError() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    translator.parse("<settings>" +
        "<variable name=\"asd\" type=\"string\">" +
        "<param name=\"min\" value=\"3\"/>" +
        "</variable>" +
        "</settings>");
    List<IVariable> variableList = translator.getVariables();
    Assert.assertEquals(1, variableList.size());
    Assert.assertEquals(1, variableList.get(0).getParameter().size());
    Assert.assertEquals("min", variableList.get(0).getParameter().get(0).getName());
    Assert.assertEquals("3", variableList.get(0).getParameter().get(0).getValue());
  }

  @Test(expected = ScriptParseException.class)
  public void parseFileWithParamCollection_withoutCollectionElements_ExpectError()
      throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects
        .requireNonNull(classLoader.getResource("import-settings-missing-collection.xml"))
        .getFile());
    translator.parse(file);
  }

  @Test
  public void parseFileWithParamCollection_NoError() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects
        .requireNonNull(classLoader.getResource("import-settings.xml")).getFile());
    translator.parse(file);
    IVariable variable = translator.getVariable("bio").orElse(null);
    Assert.assertNotNull(variable);

    IParameter parameter = variable.getParameter("collection").orElse(null);
    Assert.assertNotNull(parameter);

    List<IParameter> params = parameter.getParameter();
    Assert.assertEquals(19, params.size());
  }

  @Test
  public void paseFile_withParams_noError() throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(Objects
        .requireNonNull(classLoader.getResource("test.xml")).getFile());
    translator.parse(file);
    List<IVariable> variableList = translator.getVariables();
    Assert.assertEquals(1, variableList.size());
    Assert.assertEquals(1, variableList.get(0).getParameter().size());
    Assert.assertEquals("min", variableList.get(0).getParameter().get(0).getName());
    Assert.assertEquals("3", variableList.get(0).getParameter().get(0).getValue());
  }

  @Test(expected = ScriptParseException.class)
  public void pasterStringStream_wrongParamAttrType_ScriptParseException()
      throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    translator.parse("<settings>" +
        "<variable names=\"asd\" typse=\"string\">" +
        "</variable>" +
        "</settings>");
  }

  @Test(expected = ScriptParseException.class)
  public void pasterStringStream_wrongParamAttrName_ScriptParseException()
      throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    translator.parse("<settings>" +
        "<variable names=\"asd\" type=\"string\">" +
        "</variable>" +
        "</settings>");
  }

  @Test(expected = ScriptParseException.class)
  public void pasterStringStreamWithParams_wrongParamAttrName_ScriptParseException()
      throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    translator.parse("<settings>" +
        "<variable name=\"asd\" type=\"string\">" +
        "<param namse=\"mn\" value=\"3\"/>" +
        "</variable>" +
        "</settings>");
  }

  @Test(expected = ScriptParseException.class)
  public void pasterStringStreamWithParams_wrongParamAttrValue_ScriptParseException()
      throws ScriptParseException {
    IScriptConfigurationTranslator translator = new XmlScriptConfigurationTranslator();
    translator.parse("<settings>" +
        "<variable name=\"asd\" type=\"string\">" +
        "<param name=\"mn\" valuse=\"3\"/>" +
        "</variable>" +
        "</settings>");
  }
}
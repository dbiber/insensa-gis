package org.insensa.view.generic.swing;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.ParameterImpl;
import org.insensa.model.generic.VariableBuilder;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.ParameterListValue;
import org.insensa.view.generic.IVariableObject;
import org.insensa.view.generic.IVariableValueListener;
import org.insensa.view.widgets.WidgetMultiSelectionPopupMenu;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Vector;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SelectMultipleInputTest {

  @Test
  public void createSelectMultipleInput_WithDefaultValueSetAfterwards_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);

    SelectMultipleInput multipleInput = new SelectMultipleInput();
    IVariable variable = new VariableBuilder("layers", VariableType.SELECT_MULTIPLE)
        .withParam("collection", "Select a bio model")
        .withSubParam("Annual Mean Temperature", "bio1")
        .withSubParam("Mean Diurnal Range (Mean of monthly (max temp - min temp))", "bio2")
        .withSubParam("Max Temperature of Warmest Month","bio5")
        .withSubParam("Mean Temperature of Wettest Quarter","bio8")
        .withSubParam("Mean Temperature of Driest Quarter","bio9")
        .build();
    multipleInput.setVariable(variable);
    multipleInput.build();

    IParameter parameter = new ParameterImpl("Annual Mean Temperature","bio1");
    ParameterListValue listValue = new ParameterListValue(Collections.singletonList(parameter));
    multipleInput.setValue(listValue);

    multipleInput.addValueListener(mockListener);

    Assert.assertTrue(multipleInput.getValue().isPresent());
    IValue val = multipleInput.getValue().get();
    Assert.assertTrue(val instanceof ParameterListValue);
    Assert.assertEquals(1, ((ParameterListValue)val).getDataList().size());
  }

  @Test
  public void createSelectMultipleInput_WithDefaultValue_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);

    SelectMultipleInput multipleInput = new SelectMultipleInput();
    IVariable variable = new VariableBuilder("layers",VariableType.SELECT_MULTIPLE)
        .withValueParameter("Annual Mean Temperature","bio1")
        .withValueParameter("Max Temperature of Warmest Month","bio5")
        .withParam("collection", "Select a bio model")
        .withSubParam("Annual Mean Temperature", "bio1")
        .withSubParam("Mean Diurnal Range (Mean of monthly (max temp - min temp))", "bio2")
        .withSubParam("Max Temperature of Warmest Month","bio5")
        .withSubParam("Mean Temperature of Wettest Quarter","bio8")
        .withSubParam("Mean Temperature of Driest Quarter","bio9")
        .build();
    multipleInput.setVariable(variable);
    multipleInput.build();

    multipleInput.addValueListener(mockListener);
    verify(mockListener,never()).valueChanged(anyObject(),anyObject(),anyObject());

    Assert.assertTrue(multipleInput.getValue().isPresent());
    IValue val = multipleInput.getValue().get();
    Assert.assertTrue(val instanceof ParameterListValue);
    Assert.assertEquals(2, ((ParameterListValue)val).getDataList().size());
  }

  @Test
  public void createSelectMultipleInput_NoDefaultValue_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariable variable = new VariableBuilder("bio",VariableType.SELECT_MULTIPLE)
        .withParam("collection","Select a bio model")
        .withSubParam("BIO1", "Annual Mean Temperature")
        .withSubParam("BIO3", "Isothermality (BIO2/BIO7) (* 100)")
        .build();
    IVariableObject object = new SelectMultipleInput();
    object.setVariable(variable);
    object.build();

    object.addValueListener(mockListener);

    Object drawObject = object.getDrawableObject();
    Assert.assertTrue(drawObject instanceof WidgetMultiSelectionPopupMenu);
    WidgetMultiSelectionPopupMenu<IParameter> widget = (WidgetMultiSelectionPopupMenu<IParameter>) drawObject;
    Vector<IParameter> params = widget.getSelectedElements();
    assertEquals(0,params.size());

    widget.changeSelection(1);
    params = widget.getSelectedElements();
    assertEquals(1,params.size());


    widget.changeSelection(0);
    params = widget.getSelectedElements();
    assertEquals(2,params.size());
    verify(mockListener, times(2))
        .valueChanged(anyString(),anyObject(),anyObject());

    widget.changeSelection(1);
    widget.changeSelection(0);
    params = widget.getSelectedElements();
    verify(mockListener, times(4))
        .valueChanged(anyString(),anyObject(),anyObject());

    widget.changeSelection(0);
    widget.changeSelection(1);
    params = widget.getSelectedElements();
    assertEquals(params.get(0).getName(),"BIO1");
    assertEquals(params.get(1).getName(),"BIO3");

  }

}
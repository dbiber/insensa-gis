package org.insensa.view.generic.swing;

import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableBuilder;
import org.insensa.model.generic.VariableImpl;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.IntegerValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.view.generic.IVariableObject;
import org.insensa.view.generic.IVariableObjectFactory;
import org.insensa.view.generic.IVariableValueListener;
import org.insensa.view.widgets.WidgetMultiSelectionPopupMenu;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SwingObjectFactoryTest {

  @Test
  public void createNumericObjectWithoutParams_ExpectSpinner_NoErrors() {
    IVariable variable = new VariableImpl("name", VariableType.NUMERIC);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariableObject object = fac.createObject(variable);

    IntegerValue val = (IntegerValue) object.getValue().get();

    Assert.assertEquals(0,val.getValue().intValue());
    Assert.assertTrue("Jspinner", object.getDrawableObject() instanceof JSpinner);
  }

  @Test
  public void createNumericObjectWithParams_ExpectSpinner_NoErrors() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new VariableBuilder("name",VariableType.NUMERIC)
        .withParam("min","1")
        .withParam("max","30")
        .withParam("stepsize","2")
        .withParam("default","3")
        .build();
    IVariableObject object = fac.createObject(variable);
    object.addValueListener(mockListener);

    IValue val = object.getValue().get();
    IntegerValue iVal = (IntegerValue) val;

    Assert.assertEquals(3,iVal.getValue().intValue());
    Assert.assertTrue(object.getDrawableObject() instanceof JSpinner);
    JSpinner spinner = (JSpinner) object.getDrawableObject();
    Assert.assertEquals(5,spinner.getNextValue());

    ArgumentCaptor<IntegerValue> captor = ArgumentCaptor.forClass(IntegerValue.class);

    spinner.setValue(6);
    verify(mockListener,times(1))
        .valueChanged(eq("name"),captor.capture(),eq(VariableType.NUMERIC));
    assertEquals(6, captor.getValue().getValue().intValue());
  }



  @Test
  public void createStringInput_ExpectJTextField_NotError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new VariableBuilder("myVar",VariableType.STRING)
        .build();
    IVariableObject object = fac.createObject(variable);
    object.addValueListener(mockListener);

    assertTrue(object instanceof StringInput);
    assertTrue(object.getDrawableObject() instanceof JTextField);
    JTextField textField = (JTextField) object.getDrawableObject();

    ArgumentCaptor<StringValue> argument = ArgumentCaptor.forClass(StringValue.class);

    textField.setText("muhaha");
    textField.setText("muh");
    textField.setText("muhMatsch");
    textField.setText("GRRR");
    textField.setText("");
    verify(mockListener,times(5))
        .valueChanged(eq("myVar"),argument.capture(),eq(VariableType.STRING));
    List<StringValue> values =  argument.getAllValues();
    assertEquals("muhaha",values.get(0).getValue());
    assertEquals("muh",values.get(1).getValue());
    assertEquals("muhMatsch",values.get(2).getValue());
    assertEquals("GRRR",values.get(3).getValue());
    assertEquals("",values.get(4).getValue());
  }

  @Test
  public void createDirectoryInput_ExpectJPanelWithTextAndButton_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new VariableBuilder("myVar",VariableType.DIRECTORY)
        .build();
    IVariableObject object = fac.createObject(variable);
    object.addValueListener(mockListener);

    assertTrue(object instanceof FileFolderInput);
    Object drawObject = object.getDrawableObject();
    assertTrue(drawObject instanceof JPanel);
    assertEquals(2, ((JPanel)drawObject).getComponentCount());

    JFileChooser fileChooser = ((FileFolderInput)object).getFileChooser();
    assertEquals(JFileChooser.DIRECTORIES_ONLY,fileChooser.getFileSelectionMode());
  }

  @Test
  public void createSelectOneInput_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new VariableBuilder("resolution",VariableType.SELECT_ONE)
        .withParam("collection","Bla Blub")
        .withSubParam("res1", "0.5")
        .withSubParam("res2", "2.5")
        .build();
    IVariableObject object = fac.createObject(variable);
    object.addValueListener(mockListener);

    Assert.assertTrue(object instanceof SelectOneInput);

    SelectOneInput input = (SelectOneInput) object;
    Object drawObject = object.getDrawableObject();
    Assert.assertTrue(drawObject instanceof JComboBox);
  }

  @Test
  public void createSelectMultipleInput_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new VariableBuilder("bio",VariableType.SELECT_MULTIPLE)
        .withParam("collection","Select a bio model")
        .withSubParam("BIO1", "Annual Mean Temperature")
        .withSubParam("BIO3", "Isothermality (BIO2/BIO7) (* 100)")
        .build();
    IVariableObject object = fac.createObject(variable);
    object.addValueListener(mockListener);

    Assert.assertTrue(object instanceof SelectMultipleInput);
    Object drawObject = object.getDrawableObject();
    Assert.assertTrue(drawObject instanceof WidgetMultiSelectionPopupMenu);
  }

  @Test
  public void createFileInput_ExpectJPanelWithTextAndButton_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new VariableBuilder("myVar",VariableType.FILE)
        .build();
    IVariableObject object = fac.createObject(variable);
    object.addValueListener(mockListener);

    assertTrue(object instanceof FileFolderInput);
    Object drawObject = object.getDrawableObject();
    assertNotNull(drawObject);
    assertTrue(drawObject instanceof JPanel);
    assertEquals(2, ((JPanel)drawObject).getComponentCount());

    JFileChooser fileChooser = ((FileFolderInput)object).getFileChooser();
    assertEquals(JFileChooser.FILES_ONLY,fileChooser.getFileSelectionMode());
    JTextField textField = ((FileFolderInput)object).getTextField();


    ArgumentCaptor<StringValue> argument = ArgumentCaptor.forClass(StringValue.class);

    textField.setText("muhaha");
    textField.setText("muh");
    textField.setText("muhMatsch");
    textField.setText("GRRR");
    textField.setText("");
    verify(mockListener,times(5))
        .valueChanged(eq("myVar"),argument.capture(), eq(VariableType.FILE));
    List<StringValue> values =  argument.getAllValues();
    assertEquals("muhaha",values.get(0).getValue());
    assertEquals("muh",values.get(1).getValue());
    assertEquals("muhMatsch",values.get(2).getValue());
    assertEquals("GRRR",values.get(3).getValue());
    assertEquals("",values.get(4).getValue());
  }
}
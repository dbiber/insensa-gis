package org.insensa.view.generic.swing;

import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableBuilder;
import org.insensa.model.generic.VariableImpl;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.IntegerValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.view.generic.IVariableObject;
import org.insensa.view.generic.IVariableObjectFactory;
import org.insensa.view.generic.IVariableValueListener;
import org.insensa.view.widgets.WidgetMultiSelectionPopupMenu;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class SwingObjectFactoryGenericTest {
  @Parameterized.Parameters(name = "{index}: {0}")
  public static Iterable<? extends Object> data() {
    return Arrays.asList(VariableType.values());
  }

  @Parameterized.Parameter(0)
  public VariableType type;

  @Test
  public void testImplementationOfAllVariableTypes() {
//    IVariable var = Mockito.spy(VariableImpl.class);
//    when(var.getType()).thenReturn(type);
//    when(var.getName()).thenReturn("asd");
//    SwingObjectFactory fac = new SwingObjectFactory();
//    fac.createObject(var);
  }
}
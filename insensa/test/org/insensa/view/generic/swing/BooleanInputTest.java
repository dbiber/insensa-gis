package org.insensa.view.generic.swing;

import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableBuilder;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.XmlVariableBuilder;
import org.insensa.model.generic.value.BooleanValue;
import org.insensa.view.generic.IVariableObject;
import org.insensa.view.generic.IVariableObjectFactory;
import org.insensa.view.generic.IVariableValueListener;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.swing.JCheckBox;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class BooleanInputTest {

  @Test
  public void testIfSwingObjectFactorReturnsABooleanInput() {
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable var = new VariableBuilder("myVar", VariableType.BOOLEAN).build();

    IVariableObject variableObject = fac.createObject(var);
    assertTrue(variableObject instanceof BooleanInput);
  }

  @Test
  public void testIfBooleanInputHasAJCheckbox() {
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable var = new VariableBuilder("myVar", VariableType.BOOLEAN).build();

    IVariableObject variableObject = fac.createObject(var);
    assertTrue(variableObject.getDrawableObject() instanceof JCheckBox);
  }

  @Test
  public void testIfChangingJCheckboxTriggeresIVariableValueListener() {
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable var = new VariableBuilder("myVar", VariableType.BOOLEAN).build();

    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariableObject variableObject = fac.createObject(var);
    variableObject.addValueListener(mockListener);

    JCheckBox checkBox = (JCheckBox) variableObject.getDrawableObject();

    ArgumentCaptor<BooleanValue> argument = ArgumentCaptor.forClass(BooleanValue.class);

    checkBox.setSelected(true);
    verify(mockListener, times(1))
        .valueChanged(eq("myVar"), argument.capture(), eq(VariableType.BOOLEAN));
    assertTrue(argument.getValue().getValue());
  }

  @Test
  public void testIfDefaultValueSettingIsWorking() {
    IVariableObjectFactory fac = new SwingObjectFactory();
    IVariable variable = new XmlVariableBuilder("MyVar", VariableType.BOOLEAN)
        .withValue("true").build();
    assertNotNull(variable);
    assertTrue(variable.getValue().isPresent());
    assertTrue(variable.getValue().get() instanceof BooleanValue);
    assertTrue(((BooleanValue)variable.getValue().get()).getValue());


    variable = new XmlVariableBuilder("MyVar", VariableType.BOOLEAN)
        .withValue("false").build();
    assertFalse(((BooleanValue)variable.getValue().get()).getValue());
  }
}
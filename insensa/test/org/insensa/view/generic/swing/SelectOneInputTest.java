package org.insensa.view.generic.swing;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.ParameterImpl;
import org.insensa.model.generic.VariableBuilder;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.view.generic.IVariableObject;
import org.insensa.view.generic.IVariableValueListener;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Matchers;

import java.util.Optional;

import javax.swing.JComboBox;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SelectOneInputTest {

  @Test
  public void createSelectOneInput_NoError() {
    IVariableValueListener mockListener = mock(IVariableValueListener.class);
    IVariable variable = new VariableBuilder("resolution", VariableType.SELECT_ONE)
        .withParam("collection","Bla Blub")
        .withSubParam("res1", "0.5")
        .withSubParam("res2", "2.5")
        .build();
    IVariableObject object = new SelectOneInput();
    object.setVariable(variable);
    object.build();
    object.addValueListener(mockListener);

    Assert.assertTrue(object instanceof SelectOneInput);

    SelectOneInput input = (SelectOneInput) object;
    Object drawObject = object.getDrawableObject();
    Assert.assertTrue(drawObject instanceof JComboBox);
    Optional<IValue> val = input.getValue();
    assertNull(val.orElse(null));

    JComboBox<IParameter> widget = (JComboBox<IParameter>) drawObject;
    //The two params and one initial param
    Assert.assertEquals(3, widget.getModel().getSize());

    widget.setSelectedIndex(2);
    val = input.getValue();
    assertTrue(val.isPresent());
    assertTrue(val.get() instanceof IParameter);
    assertEquals("res2",((IParameter)val.get()).getName());
    verify(mockListener, times(1))
        .valueChanged(anyString(),Matchers.any(IParameter.class),anyObject());

    widget.setSelectedIndex(1);
    val = input.getValue();
    assertTrue(val.isPresent());
    assertTrue(val.get() instanceof IParameter);
    assertEquals("res1",((IParameter)val.get()).getName());
    verify(mockListener, times(2))
        .valueChanged(anyString(),Matchers.any(IParameter.class),anyObject());
  }

  @Test
  public void createSelectOne_WithDefaultParamSetAfterwards_NoError() {
    IVariable variable = new VariableBuilder("resolution",VariableType.SELECT_ONE)
        .withParam("collection","Bla Blub")
        .withSubParam("res1", "0.5")
        .withSubParam("res2", "2.5")
        .build();
    IVariableObject object = new SelectOneInput();
    object.setVariable(variable);
    object.build();
    ((SelectOneInput) object).setValue(new ParameterImpl("res1","0.5"));

    assertTrue(object.getValue().isPresent());
    assertTrue(object.getValue().get() instanceof ParameterImpl);
    IParameter param = (IParameter) object.getValue().get();
    assertEquals("res1", param.getName());
    assertEquals("0.5", param.getValue());

    ((SelectOneInput) object).setValue(new ParameterImpl("res2","2.5"));
    assertTrue(object.getValue().isPresent());
    assertTrue(object.getValue().get() instanceof ParameterImpl);
    param = (IParameter) object.getValue().get();
    assertEquals("res2", param.getName());
    assertEquals("2.5", param.getValue());
  }

  @Test
  public void createSelectOne_WithDefaultParam_NoError() {
    IVariable variable = new VariableBuilder("resolution",VariableType.SELECT_ONE)
        .withValueParameter("res1","0.5")
        .withParam("collection","Bla Blub")
        .withSubParam("res1", "0.5")
        .withSubParam("res2", "2.5")
        .build();
    IVariableObject object = new SelectOneInput();
    object.setVariable(variable);
    object.build();
    ((SelectOneInput) object).setValue(new ParameterImpl("res1","0.5"));

    assertTrue(object.getValue().isPresent());
    assertTrue(object.getValue().get() instanceof ParameterImpl);
    IParameter param = (IParameter) object.getValue().get();
    assertEquals("res1", param.getName());
    assertEquals("0.5", param.getValue());

    ((SelectOneInput) object).setValue(new ParameterImpl("res2","2.5"));
    assertTrue(object.getValue().isPresent());
    assertTrue(object.getValue().get() instanceof ParameterImpl);
    param = (IParameter) object.getValue().get();
    assertEquals("res2", param.getName());
    assertEquals("2.5", param.getValue());
  }

}
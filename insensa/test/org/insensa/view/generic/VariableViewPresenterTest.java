package org.insensa.view.generic;

import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.IScriptConfigurationTranslator;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableBuilder;
import org.insensa.model.generic.VariableType;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VariableViewPresenterTest {

  @Test
  public void mainTest() throws ScriptParseException {
    IGenericSettingsView viewMock = mock(IGenericSettingsView.class);
    IScriptConfigurationTranslator translator = mock(IScriptConfigurationTranslator.class);
    IVariableObjectFactory factory = mock(IVariableObjectFactory.class);
    IVariableObject object = mock(IVariableObject.class);

    IVariable variable = new VariableBuilder("bla", VariableType.NUMERIC).build();


    when(translator.getVariables()).then(invocation -> Collections.singletonList(variable));
    when(factory.createObject(Mockito.any())).then(invocation -> object);

    VariableViewPresenter presenter = new VariableViewPresenter(translator,viewMock,factory);
    IGenericSettingsView view = presenter.buildSettingsView();

    verify(object,times(1)).addValueListener(Mockito.any());
    verify(viewMock,times(1)).addVariableObject(Mockito.any());
  }

}
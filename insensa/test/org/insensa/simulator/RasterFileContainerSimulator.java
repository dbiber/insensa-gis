/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.simulator;

import org.insensa.exceptions.GisFileException;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.inforeader.InfoReader;
import org.insensa.properties.IGisFileInformationStorage;

import java.io.IOException;
import java.util.List;

public class RasterFileContainerSimulator extends RasterFileContainer {

  private float[][] data;
  private String driverName;
  private int dataType;
  IGisFileInformationStorage rasterFileInformationStorage;

  public RasterFileContainerSimulator(String outputFileName,
                                      String fullName,
                                      float[][] data,
                                      String driverName,
                                      int dataType,
                                      IGisFileInformationStorage rasterFileInformationStorage) throws IOException, GisFileException {
    super(outputFileName, fullName);
    this.data = data;
    this.driverName = driverName;
    this.dataType = dataType;
    this.rasterFileInformationStorage = rasterFileInformationStorage;
  }

  @Override
  public int getDataType() {
    return dataType;
  }

  @Override
  public int getXSize() throws IOException {
    return data.length;
  }

  @Override
  public int getYSize() throws IOException {
    return data[0].length;
  }

  @Override
  public void readRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException {
    int counter = 0;
    for (int xVal=0 ; xVal < xSize ; xVal++) {
      for (int yVal = 0 ; yVal<ySize ; yVal++) {
        arrayReference[counter] = data[xVal+xOff][yVal+yOff];
        counter++;
      }
    }
  }

//  @Override
//  public String getDriverShortName() throws IOException {
//    return driverName;
//  }

  @Override
  public void removeInfoReader(InfoReader iReader) throws IOException {
//    super.iThreadPool.removeInfoReader(iReader);
//    if (this.infoReaders.remove(iReader) == false)
//      throw new IOException("RasterFileContainer: removeInfoReader: InfoReader " + iReader.getInfoId() + " not found");
  }

  @Override
  public boolean addInfoReader(InfoReader info, boolean serialize) {
    return super.addInfoReader(info, false);
  }

  @Override
  public void addInfoReaders(List<InfoReader> infos, boolean serialize) throws IOException {
    super.addInfoReaders(infos, false);
  }

  public float[][] getData() {
    return data;
  }

  @Override
  public boolean equals(Object obj) {
    return this.data == ((RasterFileContainerSimulator)obj).getData();
  }

  @Override
  public IGisFileInformationStorage getGisFileInformationStorage() {
    return rasterFileInformationStorage;
  }
}

package org.insensa.XMLProperties;

import org.apache.commons.io.FileUtils;
import org.insensa.IGisFileContainer;
import org.insensa.XMLPropertyException;
import org.insensa.properties.IGisFileInformationStorage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import static org.mockito.Mockito.when;

@Category(org.insensa.Integration.class)
public class XmlGisFileInformationTest {

//  public static final File OUTPUT_PATH = new File("build/test-results/intTest");

  @Test
  public void Constructor1_CreateNewConfig_noErrors() throws IOException {
    Path p = Files.createTempDirectory(null);
    Path pInfos = p.resolve("infos");
    pInfos.toFile().mkdirs();


    IGisFileContainer fileContainer = Mockito.mock(IGisFileContainer.class);
    when(fileContainer.getOutputFileName()).thenReturn("test.tif");
    when(fileContainer.getAbsOutputDirectoryPath()).thenReturn(p.toString());

    IGisFileInformationStorage fileInformationStorage = new XmlGisFileInformation(fileContainer);
    File xmlFile = ((XmlGisFileInformation) fileInformationStorage).getXmlFile();
    Assert.assertEquals(pInfos.toString() + File.separator + "test.xml",xmlFile.getAbsolutePath());

    FileUtils.deleteDirectory(p.toFile());
  }

  @Test(expected = XMLPropertyException.class)
  public void Constructor1_CraeteNewConfig_emptyExistingFile() throws IOException {
    Path p = Files.createTempDirectory(null);
    Path pInfos = p.resolve("infos");
    pInfos.toFile().mkdirs();
    Path pInfosFile = pInfos.resolve("test.xml");
    pInfosFile.toFile().createNewFile();

    IGisFileContainer fileContainer = Mockito.mock(IGisFileContainer.class);
    when(fileContainer.getOutputFileName()).thenReturn("test.tif");
    when(fileContainer.getAbsOutputDirectoryPath()).thenReturn(p.toString());



    IGisFileInformationStorage fileInformationStorage = new XmlGisFileInformation(fileContainer);
    File xmlFile = ((XmlGisFileInformation) fileInformationStorage).getXmlFile();

    Assert.assertEquals(pInfos.toString() + File.separator + "test.xml",xmlFile.getAbsolutePath());

    FileUtils.deleteDirectory(p.toFile());
  }

  @Test
  public void Constructor1_ExitingConfig_noErrors() throws IOException {
    Path p = Files.createTempDirectory(null);
    Path pInfos = p.resolve("infos");
    pInfos.toFile().mkdirs();
    ClassLoader classLoader = getClass().getClassLoader();

    File file = new File(Objects.requireNonNull(classLoader
        .getResource("geotiff.xml"))
        .getFile());

    FileUtils.copyFile(file,pInfos.resolve("geotiff.xml").toFile());
    File targetConf = pInfos.resolve("geotiff.xml").toFile();

    IGisFileContainer fileContainer = Mockito.mock(IGisFileContainer.class);
    when(fileContainer.getOutputFileName()).thenReturn("geotiff.tif");
    when(fileContainer.getAbsOutputDirectoryPath()).thenReturn(p.toString());

    IGisFileInformationStorage fileInformationStorage = new XmlGisFileInformation(fileContainer);
    File xmlFile = ((XmlGisFileInformation) fileInformationStorage).getXmlFile();

    Assert.assertEquals(xmlFile.getAbsolutePath(), targetConf.getAbsolutePath());

    ((XmlGisFileInformation) fileInformationStorage).openGlobal();
    XmlGisFileInformation.SavableGroup group = ((XmlGisFileInformation) fileInformationStorage)
        .getSavableGroup();

    Assert.assertEquals(group.rootElement.getName(),"raster");

    FileUtils.deleteDirectory(p.toFile());
  }

//
//  public class TestWithExampleData {
//
//  }
//
//
//



  @Test
  public void Restore_OptionFileChanger_CountValues() throws IOException {

  }


//  public void Constructor1_CraeteNewConfig_emptyExistingFile() {
//
//  }

}
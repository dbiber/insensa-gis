package org.insensa.XMLProperties;

import org.apache.commons.io.FileUtils;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CMean;
import org.insensa.extensions.IPluginExec;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.MinMaxValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.optionfilechanger.EqualBreaks;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.properties.IGisFileInformationStorage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.when;

@Category(org.insensa.Integration.class)
public class XmlGisFileInformationWithDataTest {
  private static final Logger log = LoggerFactory.getLogger(XmlGisFileInformationWithDataTest.class);

  private File targetFile;
  private Path tmpPath;
  private IGisFileInformationStorage storage;
  private List<RasterFileContainer> mockedSourceFileList;

  @Before
  public void setUp() throws IOException {
    tmpPath = Files.createTempDirectory(null);
    Path pInfos = tmpPath.resolve("infos");
    pInfos.toFile().mkdirs();
    ClassLoader classLoader = getClass().getClassLoader();

    File file = new File(Objects.requireNonNull(classLoader
        .getResource("geotiff.xml"))
        .getFile());

    FileUtils.copyFile(file, pInfos.resolve("geotiff.xml").toFile());
    targetFile = pInfos.resolve("geotiff.xml").toFile();
    storage = new XmlGisFileInformation(targetFile);
    log.debug("targetFile = " + targetFile.getAbsolutePath());
    mockedSourceFileList = createMockedFileList();
  }

  @After
  public void tearDown() throws Exception {
    FileUtils.deleteDirectory(tmpPath.toFile());
  }

  public IPluginExec restorePluginExec(IPluginExec pluginExec, String group) {
    ISavableRestorer restorer = storage.getRestorer();
    restorer.loadCollection(group, (s, restorer1) -> {
      if (pluginExec.getId().toLowerCase().equals(s.toLowerCase())) {
        int orderId = restorer1.loadInteger("orderId").orElse(0);
        if (orderId == pluginExec.getOrderId()) {
          pluginExec.restore(restorer1);
        }
      }
    });
//    restorer.loadRestorable(group, restorer1 -> {
//      restorer1.loadRestorable(pluginExec.getId(), pluginExec::restore);
//    });
    return pluginExec;
  }

  @Test
  public void SaveAndRestoreInfoReader() throws IOException {

    long originLengh = targetFile.length();
    CountValues mcv = (CountValues) restorePluginExec(new CountValues(), "infos");//new CountValues();

    Assert.assertNotNull(mcv);
    Assert.assertEquals(5, mcv.getNumOfCounts());
    Assert.assertEquals(450426, mcv.noDataValueCount);
    Assert.assertEquals(227637, mcv.getNumOfData());

    InfoReader cv2 = new MinMaxValues();
    cv2.setUsed(false);
    storage.addInfoReader(cv2);
    Assert.assertTrue("orig: " + originLengh + " , new:" + targetFile.length(), originLengh < targetFile.length());

    MinMaxValues cv3 = (MinMaxValues) restorePluginExec(new MinMaxValues(), "infos");
    Assert.assertNotNull(cv3);

    storage.openGlobal();
    Assert.assertTrue(((XmlGisFileInformation) storage).pluginExists(cv3));
    storage.closeGlobal();

    storage.removeInfoReaders();

    storage.openGlobal();
    Assert.assertFalse(((XmlGisFileInformation) storage).pluginExists(cv3));
    storage.closeGlobal();

  }

  @Test
  public void saveAndRestoreDescription() throws IOException {
    long originLength = targetFile.length();
    this.storage.addDescription("blablub");
    Assert.assertTrue("orig: " + originLength +
        " , new:" + targetFile.length(), originLength < targetFile.length());
    originLength = targetFile.length();
    String desc = this.storage.getDescription();
    Assert.assertEquals("blablub", desc);
    this.storage.addDescription("");
    Assert.assertTrue("orig: " + originLength +
        " , new:" + targetFile.length(), originLength > targetFile.length());
  }

  @Test
  public void saveAndRestoreOption() throws IOException {
    storage.openGlobal();
    OptionFileChanger optionFileChanger = new EqualBreaks();
    ((EqualBreaks) optionFileChanger).setNumOfClasses(30);
    this.storage.addOptionFileChanger(optionFileChanger);

    EqualBreaks newEB = (EqualBreaks) restorePluginExec(
        new EqualBreaks(), "options");
    Assert.assertEquals(30, newEB.getNumOfClasses());
    Assert.assertTrue(((XmlGisFileInformation) storage)
        .pluginExists(optionFileChanger));
    storage.closeGlobal();

    storage.removePluginExec(optionFileChanger);
    storage.openGlobal();
    Assert.assertFalse(((XmlGisFileInformation) storage)
        .pluginExists(optionFileChanger));
    storage.closeGlobal();


    storage.addOptionFileChanger(optionFileChanger);
    storage.openGlobal();
    Assert.assertTrue(((XmlGisFileInformation) storage)
        .pluginExists(optionFileChanger));
    storage.closeGlobal();

    storage.removeAllOptionFileChangers();
    storage.openGlobal();
    Assert.assertFalse(((XmlGisFileInformation) storage)
        .pluginExists(optionFileChanger));
    storage.closeGlobal();

  }

  @Test
  public void saveAndRestoreOption_MultipleTimes_newOrderId() throws IOException {
    OptionFileChanger optionFileChanger = new EqualBreaks();
    ((EqualBreaks) optionFileChanger).setNumOfClasses(10);
    optionFileChanger.setOrderId(0);
    this.storage.addOptionFileChanger(optionFileChanger);

    OptionFileChanger optionFileChanger2 = new EqualBreaks();
    ((EqualBreaks) optionFileChanger2).setNumOfClasses(20);
    optionFileChanger2.setOrderId(2);
    this.storage.addOptionFileChanger(optionFileChanger2);

    EqualBreaks newEB = (EqualBreaks)
        restorePluginExec(new EqualBreaks(), "options");
    Assert.assertEquals(10, newEB.getNumOfClasses());

    EqualBreaks newEB2 = new EqualBreaks();
    newEB2.setOrderId(2);
    newEB2 = (EqualBreaks) restorePluginExec(newEB2, "options");
    Assert.assertEquals(20, newEB2.getNumOfClasses());

    newEB2.setNumOfClasses(3);
    this.storage.refreshPluginExec(newEB2);

    newEB2 = new EqualBreaks();
    newEB2.setOrderId(2);
    newEB2 = (EqualBreaks) restorePluginExec(newEB2, "options");
    Assert.assertEquals(3, newEB2.getNumOfClasses());
  }

  @Test
  public void saveAndRestoreOption_MultipleTimes_sameOrderId() throws IOException {
    OptionFileChanger optionFileChanger = new EqualBreaks();
    ((EqualBreaks) optionFileChanger).setNumOfClasses(10);
    optionFileChanger.setOrderId(1);
    this.storage.addOptionFileChanger(optionFileChanger);

    OptionFileChanger optionFileChanger2 = new EqualBreaks();
    ((EqualBreaks) optionFileChanger2).setNumOfClasses(20);
    optionFileChanger2.setOrderId(1);
    this.storage.addOptionFileChanger(optionFileChanger2);


//    EqualBreaks newEB = (EqualBreaks) restoreDefaultPluginExecValues(new EqualBreaks(),"options");
//    Assert.assertEquals(30,newEB.getNumOfClasses());
  }

  public List<RasterFileContainer> createMockedFileList() {
    RasterFileContainer container1 = Mockito.mock(RasterFileContainer.class);
    when(container1.getPathRelativeToProject()).thenReturn("my/path");
    when(container1.getOutputFileName()).thenReturn("f1");

    RasterFileContainer container2 = Mockito.mock(RasterFileContainer.class);
    when(container2.getPathRelativeToProject()).thenReturn("my/path");
    when(container2.getOutputFileName()).thenReturn("f2");

    RasterFileContainer container3 = Mockito.mock(RasterFileContainer.class);
    when(container3.getPathRelativeToProject()).thenReturn("my/path");
    when(container3.getOutputFileName()).thenReturn("f3");

    List<RasterFileContainer> sourceList =
        Arrays.asList(container1, container2, container3);
    return sourceList;
  }

  @Test
  public void saveAndRestoreConnection() throws IOException {
    //Creating And Restoring
    IGisFileSet fileSet = Mockito.mock(IGisFileSet.class);
    when(fileSet.getName()).thenReturn("MyFileSet");

    CMean cMean = new CMean();
    cMean.setRelative(true);
    cMean.setUsed(false);
    cMean.setOutputFileSet(fileSet);
    cMean.setSourceFileList(mockedSourceFileList);
    cMean.setRelativeSource(mockedSourceFileList.get(1));
    this.storage.setConnectionFileChanger(cMean);

    CMean newCMean = new CMean();
    newCMean.setSourceFileList(mockedSourceFileList);
    newCMean = (CMean) restorePluginExec(newCMean, "connections");
    Assert.assertTrue(newCMean.getRelative());

    IGisFileContainer relativeSource = newCMean.getRelativeSource();
    Assert.assertNotEquals(mockedSourceFileList.get(2), relativeSource);
    Assert.assertEquals(mockedSourceFileList.get(1), relativeSource);


    newCMean.setRelative(true);
    newCMean.setOutputFileSet(fileSet);
    newCMean.setSourceFileList(mockedSourceFileList);
    newCMean.setRelativeSource(mockedSourceFileList.get(0));
    Assert.assertTrue(((XmlGisFileInformation) storage)
        .pluginExists(newCMean));
    storage.refreshPluginExec(newCMean);

    CMean newCMean2 = new CMean();
    newCMean2.setUsed(false);
    newCMean2.setSourceFileList(mockedSourceFileList);
    newCMean2 = (CMean) restorePluginExec(newCMean2,
        "connections");
    Assert.assertEquals(mockedSourceFileList.get(0),
        newCMean2.getRelativeSource());
  }

  @Test
  public void Constructor_CopyOldConfig() throws IOException {

    Path tmpPath = Files.createTempDirectory(null);
    Path pInfos = tmpPath.resolve("infos");
    pInfos.toFile().mkdirs();

    IGisFileContainer fileContainer =
        Mockito.mock(IGisFileContainer.class);
//    when(fileContainer.getPathRelativeToProject()).thenReturn(pInfos.toString());
    when(fileContainer.getOutputFileName()).thenReturn("newtiff.tif");
    when(fileContainer.getAbsOutputDirectoryPath())
        .thenReturn(tmpPath.toString());

    XmlGisFileInformation newGisInformation =
        new XmlGisFileInformation((XmlGisFileInformation) storage, fileContainer);

    Assert.assertEquals(pInfos.resolve("newtiff.xml").toString(),
        newGisInformation.getXmlFile().getAbsolutePath());

    Assert.assertTrue(pInfos.resolve("newtiff.xml").toFile().exists());

  }

  @Test
  public void Constructor_RenameTo() throws IOException {

    Path tmpPath = Files.createTempDirectory(null);
    Path pInfos = tmpPath.resolve("infos");
    pInfos.toFile().mkdirs();

    IGisFileContainer fileContainer =
        Mockito.mock(IGisFileContainer.class);
//    when(fileContainer.getPathRelativeToProject()).thenReturn(pInfos.toString());
    when(fileContainer.getOutputFileName()).thenReturn("geotiff.tif");
    when(fileContainer.getAbsOutputDirectoryPath())
        .thenReturn(tmpPath.toString());

    String oldName = ((XmlGisFileInformation)storage).getXmlFile().getName();
    ((XmlGisFileInformation)storage).renameTo(fileContainer,tmpPath.toString());
//    XmlGisFileInformation newGisInformation =
//        new XmlGisFileInformation((XmlGisFileInformation) storage, fileContainer);

    Assert.assertEquals(pInfos.resolve(oldName).toString(),
        ((XmlGisFileInformation)storage).getXmlFile().getAbsolutePath());

    Assert.assertTrue(pInfos.resolve(oldName).toFile().exists());

  }
}

package org.insensa.model.generic;

import org.insensa.model.generic.value.IValue;
import org.insensa.model.storage.ISavableRestorer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Consumer;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class VariableTypeRepeatingTest {

  @Parameterized.Parameter(0)
  public VariableType type;

  @Parameterized.Parameters(name = "{index}: {0}")
  public static Iterable<? extends Object> data() {
    return Arrays.asList(VariableType.values());
  }

  /**
   * Every enum value withing enum VariableType should have an entry in the method
   * {@link VariableImpl#restore(ISavableRestorer)}
   * If a new VariableType is added and not mentioned within VariableImpl#restore, create an error.
   */
  @Test
  public void testVariableTypes_testEnumTypesUsageWithinClassVariableImpl_everyEnumIsUsedWithinClass() {
    IVariable var = Mockito.spy(VariableImpl.class);

    ISavableRestorer restorer2 = Mockito.mock(ISavableRestorer.class);
    ISavableRestorer restorer = Mockito.mock(ISavableRestorer.class);
    Mockito.when(restorer.loadRestorable(Mockito.any(), Mockito.any(Class.class)))
        .thenReturn(Optional.empty());
    Mockito.when(restorer2.loadRestorable(Mockito.any(), Mockito.any(Class.class)))
        .thenReturn(Optional.empty());

    Mockito.doAnswer(invocation -> {
      Consumer<ISavableRestorer> callback = (Consumer<ISavableRestorer>)
          invocation.getArguments()[1];
      callback.accept(restorer2);
      return null;
    }).when(restorer).loadRestorable(anyString(), Mockito.any(Consumer.class));

    Mockito.when(restorer.loadString("name"))
        .thenReturn(java.util.Optional.of("bla"));
    Mockito.when(restorer.loadString("groups"))
        .thenReturn(Optional.empty());
    Mockito.when(restorer.loadString("type"))
        .thenReturn(Optional.ofNullable(type.toString()));
    Mockito.when(restorer.loadString("display"))
        .thenReturn(java.util.Optional.of("Display Bla"));
    Mockito.when(restorer.loadBoolean("global"))
        .thenReturn(Optional.of(false));
    var.restore(restorer);
    verify(var, times(1)).setValue(Mockito.any());
  }

  /**
   * Every enum value withing enum VariableType should have an entry in the method
   * {@link VariableProcessor#assign(IVariable, String)} method
   * If a new VariableType is added and not mentioned within {@link VariableProcessor#assign(IVariable, String)}
   * , create an error.
   */
  @Test
  public void testVariableTypes_testEnumTypesUsageVariableProcessor_everyEnumIsUsedWithinClass() {

    IVariableAssigner assigner = mock(IVariableAssigner.class);
    VariableProcessor variableProcessor = new VariableProcessor(assigner);
    IVariable var = Mockito.spy(VariableImpl.class);
    IValue val = Mockito.mock(IValue.class);
    when(var.getValue()).thenReturn(Optional.of(val));
    when(var.getName()).thenReturn("name");
    when(var.getType()).thenReturn(type);

    variableProcessor.assign(var, var.getName());
  }
}
package org.insensa.model.generic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XmlVariableBuilderTest {

  @Test
  public void testIfVariableXmlStringCreationWorks() {
    XmlVariableBuilder builder = new XmlVariableBuilder("MyVar", VariableType.BOOLEAN);
    String string  = builder.withValue("true").toString().replaceAll("\\s", "");
    String toCompare = ("" +
        "<variable name=\"MyVar\" type=\"boolean\">" +
        "<value value=\"true\"/>" +
        "</variable>").replaceAll("\\s","");
    assertEquals(toCompare, string);


    string = builder.withParam("collection", "Select Variable").toString().replaceAll("\\s", "");
    toCompare = ("" +
        "<variable name=\"MyVar\" type=\"boolean\">" +
        "<value value=\"true\"/>" +
        "<param name=\"collection\" value=\"Select Variable\"/>" +
        "</variable>").replaceAll("\\s","");
    assertEquals(toCompare, string);
  }

}
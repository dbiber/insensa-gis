package org.insensa.model.storage;

import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.*;

public class JdomRestorerTest {



  @Test
  public void goToSubtree() {
    JdomRestorer restorer = new JdomRestorer("<raster><infos>" +
        "<countValues read=\"true\" />" +
        "</infos>" +
        "</raster>");

    restorer.setRootElem("infos");
    restorer.setRootElem("countValues");
  }

  @Test
  public void loadBooleanVariableFromSubtree() {
    JdomRestorer restorer = new JdomRestorer("<raster><infos>" +
        "<countValues used=\"true\" />" +
        "</infos>" +
        "</raster>");

    restorer.setRootElem("infos");
    restorer.setRootElem("countValues");
    Boolean read = restorer.loadBoolean("used").get();
    assertTrue(read);
  }

  @Test
  public void loadCollectionFromSubtree() {
    JdomRestorer restorer = new JdomRestorer("<raster><infos>" +
        "<countValues read=\"true\" >" +
        "<CountValue Value=\"1\" Count=\"11\" />" +
        "<CountValue Value=\"2\" Count=\"12\" />" +
        "<CountValue Value=\"3\" Count=\"13\" />" +
        "<CountValue Value=\"4\" Count=\"14\" />" +
        "<CountValue Value=\"5\" Count=\"15\" />" +
        "</countValues>" +
        "</infos>" +
        "</raster>");

    restorer.setRootElem("infos");
    restorer.setRootElem("countValues");

    Map<Float, Long> flMap = new TreeMap<>();
    restorer.loadCollection("CountValue", restorer1 -> {
      flMap.put(
          restorer1.loadFloat("Value").get(),
          restorer1.loadLong("Count").get());
    });

    assertEquals(flMap.size(),5);
    assertEquals(11l,flMap.values().iterator().next().longValue());
  }


}
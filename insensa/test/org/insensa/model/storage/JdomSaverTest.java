package org.insensa.model.storage;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class JdomSaverTest {

  @Test
  public void init() {
    Map<Float, Long> myMap = new HashMap<>();
    myMap.put(1.222F,4L);
    myMap.put(1.223F,4L);
    myMap.put(1.224F,4L);
    myMap.put(1.225F,4L);
    myMap.put(1.226F,4L);
    myMap.put(1.227F,4L);

    JdomSaver saver = new JdomSaver("raster");
    saver.save("infos", saver1 -> {
      saver1.save("countValues",saver2 -> {
        myMap.forEach((aFloat, aLong) -> {
          saver2.save("CountValue",saver3 -> {
            saver3.save("Value",aFloat);
            saver3.save("Count",aLong);
          });
        });
      });
    });
  }

}
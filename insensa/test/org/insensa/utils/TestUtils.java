package org.insensa.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class TestUtils {

    public static File getFileStream(String filename) {
        URL url = TestUtils.class.getClassLoader().getResource(filename);
        return new File(url.getFile());
    }

    public static final String getFileResource() throws IOException {
        InputStream propStream = TestUtils.class.getClassLoader().getResourceAsStream("test.properties");
        Properties properties = new Properties();
        properties.load(propStream);
        if (properties.getProperty("resourceFile")!=null) {
            return properties.getProperty("resourceFile");
        }
        throw new IOException("Property \"resourceFile\" not found");
    }

    public static final String getProperty(String propertyName) throws IOException {
        InputStream propStream = TestUtils.class.getClassLoader().getResourceAsStream("test.properties");
        Properties properties = new Properties();

        properties.load(propStream);
        if (properties.getProperty(propertyName)!=null) {
            return properties.getProperty(propertyName);
        }
        throw new IOException("Property \""+propertyName+ "\" not found");
    }

    public static final String getResultProperty(String property) throws IOException {
        String filePath = getProperty("resultFile");
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream(filePath);
        properties.load(fileInputStream);
        if (properties.getProperty(property)!=null) {
            return properties.getProperty(property);
        }
        throw new IOException("Property \""+property+ "\" not found");
    }

    public static final double getResultPropertyAsDouble(String property) throws IOException {
        String prop = getResultProperty(property);
        if (prop==null) {
            throw new IOException("Property: "+property);
        }
        return Double.valueOf(prop);
    }

    public static final float getResultPropertyAsFloat(String property) throws IOException {
        String prop = getResultProperty(property);
        if (prop==null) {
            throw new IOException("Property: "+property);
        }
        return Float.valueOf(prop);
    }

    public static final int getResultPropertyAsInteger(String property) throws IOException {
        String prop = getResultProperty(property);
        if (prop==null) {
            throw new IOException("Property: "+property);
        }
        return Integer.valueOf(prop);
    }

    public static final long getResultPropertyAsLong(String property) throws IOException {
        String prop = getResultProperty(property);
        if (prop==null) {
            throw new IOException("Property: "+property);
        }
        return Long.valueOf(prop);
    }
}

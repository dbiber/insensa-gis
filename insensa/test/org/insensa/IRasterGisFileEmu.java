package org.insensa;

import org.gdal.osr.SpatialReference;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.DataTypeHelper;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class IRasterGisFileEmu implements IRasterGisFile {
  @Override
  public float getCellSize() {
    return 0;
  }

  @Override
  public float getXllCorner() {
    return 0;
  }

  @Override
  public float getYllCorner() {
    return 0;
  }

  @Override
  public int getNRows() {
    return 1;
  }

  @Override
  public int getNCols() {
    return 6;
  }

  @Override
  public float getNoDataValue() {
    return 9999f;
  }

  @Override
  public int getDataType() {
    return 0;
  }

  @Override
  public double[] getGeoTransform() {
    return new double[0];
  }

  @Override
  public Map<String, String> getSpatialReferenceAttr() {
    return null;
  }

  @Override
  public void readRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException {

  }

  @Override
  public float[] readRaster(int xOff, int yOff, int xSize, int ySize) throws IOException {
    return new float[0];
  }

  @Override
  public void createNewFile(IRasterGisFile gisFile, DataTypeHelper.RasterDataType rasterDataType, double maxValue) throws IOException {
  }

  @Override
  public void writeNodataValue(double newValue) throws IOException {

  }

  @Override
  public SpatialReference getSpatialReference() {
    return null;
  }

  @Override
  public void writeNodataValueMax() throws IOException {

  }

  @Override
  public String getProjectionWKT() {
    return null;
  }

  @Override
  public String getDriverShortName() throws IOException {
    return null;
  }

  @Override
  public String getParent() {
    return null;
  }

  @Override
  public boolean isTypeFloat() {
    return false;
  }

  @Override
  public void writeRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException {

  }

  @Override
  public GisFileType getType() {
    return null;
  }

  @Override
  public boolean delete() {
    return false;
  }

  @Override
  public boolean exists() {
    return false;
  }

  @Override
  public String getAbsolutePath() {
    return null;
  }

  @Override
  public File getFileRef() {
    return null;
  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public void relocateFile(String fullPath) {

  }

  @Override
  public boolean renameTo(File dest) {
    return false;
  }

  @Override
  public void unlock() {

  }

  @Override
  public void refresh() throws GisFileException {

  }

  @Override
  public GisAttributes getGisAttributes() {
    return null;
  }
}

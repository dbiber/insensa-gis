package org.insensa;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.OptionThreadPool;
import org.insensa.properties.IGisFileInformationStorage;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class IGisFileContainerEmu implements IGisFileContainer {

  @Override
  public IRasterGisFile getGisFile() {
    return null;
  }

  @Override
  public void addFileObserver(FileObserver fileObserver) {

  }

  @Override
  public boolean addInfoReader(InfoReader info, boolean serialize) throws IOException {
    return false;
  }

  @Override
  public void addInfoReaders(List<InfoReader> infos, boolean serialize) throws IOException {

  }

  @Override
  public void addOption(OptionFileChanger option) throws IOException {

  }

  @Override
  public void changeOutputFileName(String newOutputFileName) throws IOException {

  }

  @Override
  public void changeOutputFilePath(String outputFilePath) throws IOException {

  }

  @Override
  public boolean deleteFile() {
    return false;
  }

  @Override
  public boolean deleteLink() {
    return false;
  }

  @Override
  public void executeAllInfos(WorkerStatusList workerStatusList) throws IOException {

  }

  @Override
  public void executeAllOptions(WorkerStatusList workerStatusList) throws IOException, JDOMException {

  }

  @Override
  public void executeInfo(String infoReaderName, WorkerStatusList workerStatusList) throws IOException {

  }

  @Override
  public void executeInfos(List<String> infoReaderList, WorkerStatusList workerStatusList) throws IOException {

  }

  @Override
  public String getAbsOutputDirectoryPath() {
    return null;
  }

  @Override
  public ConnectionFileChanger getConnectionFileChanger() {
    return null;
  }

  @Override
  public void setConnectionFileChanger(ConnectionFileChanger conn) throws IOException {

  }

  @Override
  public void addInfoConnection(InfoConnection infoConnection) {

  }

  @Override
  public List<InfoConnection> getInfoConnections() {
    return null;
  }

  @Override
  public ExecDependencyController getDepChecker() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }

  @Override
  public void setDescription(String description) throws IOException {

  }

  @Override
  public List<FileObserver> getFileObservers() {
    return null;
  }

  @Override
  public InfoReader getInfoReader(String infoReaderName) {
    return null;
  }

  @Override
  public List<InfoReader> getInfoReaders() {
    return null;
  }

  @Override
  public List<OptionFileChanger> getOptions() {
    return null;
  }

  @Override
  public OptionThreadPool getOptionThreadPool() {
    return null;
  }

  @Override
  public String getOutputFileName() {
    return null;
  }

  @Override
  public void setOutputFileName(String outputFileName) throws IOException {

  }

  @Override
  public ParentModule getParentModule() {
    return null;
  }

  @Override
  public void setParentModule(ParentModule parentModule) {

  }

  @Override
  public String getPathRelativeToProject() {
    return null;
  }

  @Override
  public IGisFileContainer getTargetGisFileContainer() {
    return null;
  }

  @Override
  public void setTargetGisFileContainer(IGisFileContainer targetGisFileContainer) {

  }

  @Override
  public int getUnusedInfoReaderCount() {
    return 0;
  }

  @Override
  public int getUnusedOptionCount() {
    return 0;
  }

  @Override
  public IGisFileInformationStorage getGisFileInformationStorage() {
    return null;
  }

  @Override
  public void setGisFileInformationStorage(IGisFileInformationStorage xmlInformation) {

  }

  @Override
  public boolean isHardCopy() {
    return false;
  }

  @Override
  public void notifyObserversDelete() {

  }

  @Override
  public void relocateFile() {

  }

  @Override
  public void removeFileObserver(FileObserver fileObserver) {

  }

  @Override
  public void removeInfoReader(InfoReader infoReader) throws IOException {

  }

  @Override
  public void removeInfoConnection(InfoConnection infoConnection) throws IOException {

  }

  @Override
  public void removeOptionFileChanger(OptionFileChanger option) throws IOException {

  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public boolean delete() {
    return false;
  }

  @Override
  public String getAbsolutePath() {
    return null;
  }

  @Override
  public boolean renameTo(File dest) throws IOException {
    return false;
  }

  @Override
  public boolean isConnection() {
    return false;
  }

  @Override
  public boolean isStack() {
    return false;
  }

  @Override
  public boolean isLocked() {
    return false;
  }

  @Override
  public void setLock(boolean locked) {

  }

  @Override
  public void update(IGisFileContainer file) {

  }
}

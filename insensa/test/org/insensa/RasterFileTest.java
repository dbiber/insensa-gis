package org.insensa;

import org.gdal.gdal.Driver;
import org.gdal.gdal.gdal;
import org.insensa.utils.TestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.io.IOException;

@Ignore
public class RasterFileTest {

  private static String fileResource;
  private RasterFile rasterFile;

  @BeforeClass
  public static void initClass() throws IOException {
    fileResource = TestUtils.getFileResource();
    if (fileResource == null) {
      fileResource = System.getProperty("fileResource");
    }
    gdal.AllRegister();
    gdal.SetConfigOption("GDAL_DATA", "lib/gdal/data");
  }

  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void createRasterReferenceInstance() throws IOException {
    RasterFile rasterFile = new RasterFile("this");
  }

  @Test(expected = IOException.class)
  public void getDriver_fileDoesNotExist_ExpectException() throws IOException {
    RasterFile rasterFile = new RasterFile("this");
    Assert.assertSame(rasterFile.getType(), GisFileType.GEO_TIFF);
    rasterFile.getDriver(RasterFileAccess.READ_ONLY);
  }

  @Test
  public void testSomeMethods() throws IOException {
    Driver driver = Mockito.mock(Driver.class);
    RasterFile rasterFile = Mockito.spy(new RasterFile("bla.tif"));

    Mockito.doReturn(driver).when(rasterFile).getDriver(Matchers.any());

    rasterFile.getDriver(RasterFileAccess.READ_ONLY);
    Assert.assertFalse(rasterFile.exists());
   }
}
package org.insensa.inforeader;

import org.insensa.GisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.model.storage.JdomSaver;
import org.jdom.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

public class PrecisionValuesTest {

  @Mock
  GisFileContainer gisFileContainer;
  @Mock
  IRasterGisFile gisFile;

  @Before
  public void init_tested_and_mocks() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testRun() throws IOException {

    Mockito.when(gisFile.getDriverShortName()).thenReturn("");
    Mockito.when(gisFile.isTypeFloat()).thenReturn(true);
    Mockito.when(gisFileContainer.getGisFile()).thenReturn(gisFile);

    PrecisionValues precisionValues = new PrecisionValues();
    precisionValues.setTargetFile(gisFileContainer);
    precisionValues.setMaxPrecision(4);
    Assert.assertEquals(0,precisionValues.getData().getAttibutes().size());

    precisionValues.run();
    Assert.assertEquals("0.####",precisionValues.getPrecisionRegex());
    Assert.assertEquals(4,precisionValues.getMaxPrecision());
    Assert.assertEquals(1,precisionValues.getData().getAttibutes().size());

    JdomSaver saver = new JdomSaver(new Element("infos"));
    JdomRestorer restorer = new JdomRestorer(saver.getCurrentElem());

    precisionValues.save(saver);
    PrecisionValues restoredPrecisionValues = new PrecisionValues();
    restoredPrecisionValues.restore(restorer);

    Assert.assertEquals("0.####",restoredPrecisionValues.getPrecisionRegex());
    Assert.assertEquals(4,restoredPrecisionValues.getMaxPrecision());
    Assert.assertEquals(1,restoredPrecisionValues.getData().getAttibutes().size());
  }
}
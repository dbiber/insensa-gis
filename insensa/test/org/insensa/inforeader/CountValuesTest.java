package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.model.storage.JdomSaver;
import org.insensa.properties.IGisFileInformationStorage;
import org.jdom.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Matchers.anyInt;

@RunWith(Parameterized.class)
public class CountValuesTest {
  @Parameterized.Parameter(0)
  public float[] rasterData;
  @Parameterized.Parameter(1)
  public int rows;
  @Parameterized.Parameter(2)
  public int exNumOfData;
  @Parameterized.Parameter(3)
  public int exNumOfCounts;
  @Mock
  IGisFileContainer gisFileContainer;
  @Mock
  IRasterGisFile gisFile;
  @Spy
  PrecisionValues precisionValues;
  @Mock
  IGisFileInformationStorage storage;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
                      //Data                      //Rows //exNumOfData //exNumOfCounts
        {new float[]{0.1F, 0.2F, 0.1F, 0.2F, 0.2F}, 1, 5, 2},
        {new float[]{0.00001F, 0.2F, 0.00001F, 0.2F, 0.2F}, 1, 5, 2},
        {new float[]{0.0F, 0.2F, 0.0F, 0.2F, 0.2F}, 1, 3, 1},
        {new float[]{1000F, 9000F, 0.0001F, 0.0002F, 0.0F, 1000F}, 1, 5, 4}
    });
  }

  @Before
  public void init_tested_and_mocks() {
    MockitoAnnotations.initMocks(this);
  }

  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void testMultipleParameterForReadValues() throws IOException {

    Mockito.when(gisFileContainer.getGisFile()).thenReturn(gisFile);

    precisionValues.setUsed(true);
    precisionValues.setMaxPrecision(8);
    Mockito.when(gisFileContainer.getInfoReader(PrecisionValues.NAME))
        .thenReturn(precisionValues);

    Mockito.when(gisFile.getNRows()).thenReturn(rows);
    Mockito.when(gisFile.getNoDataValue()).thenReturn(0.0F);
    Mockito.when(gisFile.readRaster(anyInt(), anyInt(), anyInt(), anyInt()))
        .thenReturn(rasterData);


    CountValues countValues = new CountValues();
    countValues.setTargetFile(gisFileContainer);
    countValues.run();

    Assert.assertEquals("NumOfData: ", exNumOfData, countValues.getNumOfData());
    Assert.assertEquals("NumOfCounts", exNumOfCounts, countValues.getNumOfCounts());
    Assert.assertNotNull(countValues.getCountValues());
    Assert.assertEquals("SizeIfMap", exNumOfCounts, countValues.getCountValues().size());

    JdomSaver saver = new JdomSaver(new Element("cv"));
    JdomRestorer restorer = new JdomRestorer(saver.getCurrentElem());

    saver.save("infos", saver1 -> {
      saver1.save("CountValues", countValues);
    });
    countValues.save(saver);
    CountValues restoredCountValues = new CountValues();

    Mockito.when(storage.getRestorer()).thenReturn(restorer);
    Mockito.when(gisFileContainer.getGisFileInformationStorage()).thenReturn(storage);

    restoredCountValues.setTargetFile(gisFileContainer);
    restoredCountValues.restore(restorer);



    Assert.assertEquals("NumOfData: ", exNumOfData, restoredCountValues
        .getNumOfData());
    Assert.assertEquals("NumOfCounts", exNumOfCounts, restoredCountValues
        .getNumOfCounts());
    Assert.assertNotNull(restoredCountValues.getCountValues());
    Assert.assertEquals(exNumOfCounts, restoredCountValues
        .getCountValues().size());


  }
}
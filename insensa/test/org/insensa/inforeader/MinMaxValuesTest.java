package org.insensa.inforeader;

import org.gdal.gdal.gdal;
import org.insensa.RasterFileContainer;
import org.insensa.utils.TestUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class MinMaxValuesTest {

  @Spy
  CountValues spyCountValues = new CountValues();

  @BeforeClass
  public static void initExpected() throws IOException {
    gdal.AllRegister();
    gdal.SetConfigOption("GDAL_DATA", "lib/gdal/data");
  }

  @Test
  public void getMaxValue() throws IOException, InterruptedException {
    File rast1File = TestUtils.getFileStream("rast1.tif");

    RasterFileContainer rasterFileContainer = new RasterFileContainer(
        "test",
        rast1File.getAbsolutePath());
    rasterFileContainer.setGisFileInformationStorage(null);

    PrecisionValues precisionValues = new PrecisionValues();
    precisionValues.setMaxPrecision(14);
    rasterFileContainer.addInfoReader(precisionValues, false);

    MinMaxValues minMaxValues = new MinMaxValues();
    rasterFileContainer.addInfoReader(minMaxValues, false);

    MeanValue mv = new MeanValue();
    rasterFileContainer.addInfoReader(mv,false);

    rasterFileContainer.executeAllInfos(null,true);
  }

  @Test
  public void getMinValue() {
  }
}
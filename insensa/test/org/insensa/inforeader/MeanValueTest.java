/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.inforeader;

import org.gdal.gdal.gdal;
import org.insensa.RasterFileContainer;
import org.insensa.utils.TestUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class MeanValueTest {

    private static String fileResource;
    @Spy
    CountValues spyCountValues = new CountValues();

    @BeforeClass
    public static void initExpected() throws IOException {
        fileResource = TestUtils.getFileResource();
        if (fileResource == null) {
            fileResource = System.getProperty("fileResource");
        }
        gdal.AllRegister();
        gdal.SetConfigOption("GDAL_DATA", "lib/gdal/data");
    }

    @Test
    @Ignore
    public void calculateMeanValue() throws Exception {
        RasterFileContainer rasterFileInformation = new RasterFileContainer(
                "test.tif",
                fileResource);
        PrecisionValues precisionValues = new PrecisionValues();
        precisionValues.setMaxPrecision(3);
        precisionValues.readInfos(rasterFileInformation);
        rasterFileInformation.addInfoReader(precisionValues,false);
        Mockito.doReturn(spyCountValues.getCountValuesFromCache()).when(spyCountValues).getCountValues();
//        spyCountValues.setWorkerStatus(new QuantileBreaksValuesTest.TestWorkerStatus());
        spyCountValues.readInfos(rasterFileInformation);
        rasterFileInformation.addInfoReader(spyCountValues,false);

        MeanValue meanValue = new MeanValue();
        meanValue.readInfos(rasterFileInformation);

    }
}

package org.insensa.inforeader;

import org.gdal.gdal.gdal;
import org.insensa.RasterFileContainer;
import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ClassificationRange;
import org.insensa.utils.TestUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class QuantileBreaksValuesTest {

    private static String fileResource;
    @Spy
    CountValues spyCountValues = new CountValues();
    //
//    @Before
//    public void setUp() throws Exception {
//
//        MockitoAnnotations.initMocks(this);

    @BeforeClass
    public static void initExpected() throws IOException {
        fileResource = TestUtils.getFileResource();
        if (fileResource == null) {
            fileResource = System.getProperty("fileResource");
        }
        gdal.AllRegister();
        gdal.SetConfigOption("GDAL_DATA", "lib/gdal/data");
    }

//    }

    public void checkResult(List<ClassificationRange> ranges) throws IOException {
        for (int i=0 ; i<ranges.size() ; i++) {
            Assert.assertEquals(TestUtils.getResultPropertyAsDouble
                    ("QuantileBreaksValuesTest.break"+(i+1)+".low"),
                    ranges.get(i).getLowValue(),0);
            Assert.assertEquals(TestUtils.getResultPropertyAsDouble
                            ("QuantileBreaksValuesTest.break"+(i+1)+".high"),
                    ranges.get(i).getHighValue(),0);
        }
    }

    @Test
    @Ignore
    public void quantileCalculationWith4ClassesforAllTypes() {
        try {
            File rast1File = TestUtils.getFileStream("rast1.tif");

            RasterFileContainer rasterFileInformation = new RasterFileContainer(
                    "test",
                    rast1File.getAbsolutePath());
            PrecisionValues precisionValues = new PrecisionValues();
            precisionValues.setMaxPrecision(12);
            precisionValues.readInfos(rasterFileInformation);
            rasterFileInformation.addInfoReader(precisionValues,false);

            Mockito.doReturn(spyCountValues.getCountValuesFromCache())
                .when(spyCountValues).getCountValues();

            spyCountValues.setWorkerStatus(new TestWorkerStatus());
            spyCountValues.readInfos(rasterFileInformation);
            rasterFileInformation.addInfoReader(spyCountValues,false);

            QuantileBreaksValues quantileBreaksValues = new QuantileBreaksValues();
            quantileBreaksValues.setNumOfClasses(4);

            quantileBreaksValues.setType(1);
            quantileBreaksValues.readInfos(rasterFileInformation);
            Assert.assertEquals(-0.4725827d,
                quantileBreaksValues.getRangeList().get(0).getLowValue(),
                0);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }

    }

    class TestWorkerStatus implements WorkerStatus {

        int lastStatus=0;
        @Override
        public void endPause() {

        }

        @Override
        public void endProgress() {

        }

        @Override
        public void errorProcess() {

        }

        @Override
        public void errorProcess(String message) {

        }

        @Override
        public void errorProcess(Throwable e) {

        }

        @Override
        public WorkerStatusList getChildWorkerStatusList(int index) {
            return null;
        }

        @Override
        public WorkerStatusList getParentWorkerStatusList() {
            return null;
        }

        @Override
        public float getProgress() {
            return 0;
        }

        @Override
        public float getStepSize() {
            return 0;
        }

        @Override
        public void refreshPercentage(float percent) {
            int status = (int) percent;
            if(status>lastStatus) {
                lastStatus = status;
            }

        }

        @Override
        public void removeChildrenWorkerStatusLists() {

        }

        @Override
        public void setChildrenWorkerStatusLists(int numOfLists) {

        }

        @Override
        public void setParentWorkerStatusList(WorkerStatusList mProgress) {

        }

        @Override
        public void setProgressName(String name) {

        }

        @Override
        public void setStepSize(float stepSize) {

        }

        @Override
        public void startPause(String text) {

        }

        @Override
        public void startProcess() {

        }
    }
}

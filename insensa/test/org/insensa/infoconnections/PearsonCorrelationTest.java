/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.infoconnections;

import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.MeanValue;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.properties.IGisFileInformationStorage;
import org.insensa.simulator.RasterFileContainerSimulator;
import org.insensa.utils.TestUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PearsonCorrelationTest {
  float[][] testValues = new float[][]{{43,21,25,42,57,59}};
  float[][] testValues2 = new float[][]{{99,65,79,75,87,81}};
  private static String fileResource;

  RasterFileContainerSimulator rasterFileInformation;
  RasterFileContainerSimulator rasterFileInformation2;
//  @Spy
//  RasterFileContainer rasterFileInformation = new RasterFileContainer(
//      "test",
//      "asd");
  @Spy
  CountValues spyCountValues = new CountValues();

  @BeforeClass
  public static void init() throws IOException {
    fileResource = TestUtils.getFileResource();
    if (fileResource == null) {
      fileResource = System.getProperty("fileResource");
    }
    gdal.AllRegister();
    gdal.SetConfigOption("GDAL_DATA", "lib/gdal/data");
  }

  @Before
  public void setUp() throws Exception {

    IGisFileInformationStorage storage1 = Mockito.mock(IGisFileInformationStorage.class);
    IGisFileInformationStorage storage2 = Mockito.mock(IGisFileInformationStorage.class);
    rasterFileInformation = new RasterFileContainerSimulator("test1.tif","test1.tif",
        testValues,"UNKNOWN", gdalconstConstants.GDT_Float32,
        storage1);
    rasterFileInformation2 = new RasterFileContainerSimulator("test2.tif","test2.tif",
        testValues2,"UNKNOWN", gdalconstConstants.GDT_Float32,
        storage2);
    Mockito.doReturn(spyCountValues.getCountValuesFromCache()).when(spyCountValues).getCountValues();
//    Mockito.doReturn(rasterFileInformation.readRaster()).when(rasterFileInformation).getBand(0);
//    Mockito.doAnswer(invocation -> {
//      Object[] arguments = invocation.getArguments();
//      int xOdd = (int) arguments[0];
//      int yOff = (int) arguments[1];
//      int xSize = (int) arguments[2];
//      int ySize = (int) arguments[3];
//      float[] floatRef = (float[]) arguments[4];
//      int counter = 0;
//      for (int xVal=0 ; xVal < xSize ; xVal++) {
//        for (int yVal = 0 ; yVal<ySize ; yVal++) {
//          floatRef[counter] = testValues[xVal+xOdd][yVal+yOff];
//          counter++;
//        }
//      }
//      return null;
//    }).when(rasterFileInformation).readRaster(Mockito.anyInt(),Mockito.anyInt(),
//        Mockito.anyInt(),Mockito.anyInt(),Mockito.any());
//
//    Mockito.doReturn(2).when(rasterFileInformation).getXSize();
//    Mockito.doReturn(6).when(rasterFileInformation).getYSize();
//    Mockito.doReturn(gdalconstConstants.GDT_Float32).when(rasterFileInformation).getDataType();
//    Mockito.doReturn("UNKNOWN").when(rasterFileInformation).getDriverShortName();
  }

  public PearsonCorrelationTest() throws IOException, GisFileException {
  }


  @Test
  @Ignore
  public void TestPearson1() throws Exception {
    PrecisionValues precisionValues = new PrecisionValues();
    precisionValues.setMaxPrecision(3);
    precisionValues.readInfos(rasterFileInformation);
    rasterFileInformation.addInfoReader(precisionValues,false);

    spyCountValues.readInfos(rasterFileInformation);
    rasterFileInformation.addInfoReader(spyCountValues,false);

    MeanValue meanValue = new MeanValue();
    meanValue.readInfos(rasterFileInformation);
    rasterFileInformation.addInfoReader(meanValue,false);

    spyCountValues.setUsed(false);
     precisionValues = new PrecisionValues();
    precisionValues.setMaxPrecision(3);
    precisionValues.readInfos(rasterFileInformation2);
    rasterFileInformation2.addInfoReader(precisionValues,false);
    spyCountValues.readInfos(rasterFileInformation2);
    rasterFileInformation2.addInfoReader(spyCountValues,false);

    PearsonCorrelation pearsonCorrelation = new PearsonCorrelation();
    pearsonCorrelation.setT_test(false);
    pearsonCorrelation.setPartial(false);
    List<IGisFileContainer> sourceList = new ArrayList<>();
    sourceList.add(rasterFileInformation);
    sourceList.add(rasterFileInformation2);
    pearsonCorrelation.setSourceFileList(sourceList);
//    pearsonCorrelation.setOutputFile(new RasterFileContainer("test.tif",
//        "/home/dennis/temp/test2.tif"));
    pearsonCorrelation.readInfos();
  }
}

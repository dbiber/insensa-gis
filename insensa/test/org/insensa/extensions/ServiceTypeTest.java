package org.insensa.extensions;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceTypeTest {

  @Test
  public void createTypeFromString() {
    ServiceType type1 = ServiceType.from("exec");
    Assert.assertEquals(ServiceType.EXEC,type1);

    type1 = ServiceType.from("exeC");
    Assert.assertEquals(ServiceType.EXEC,type1);

    type1 = ServiceType.from("SETTINGS");
    Assert.assertEquals(ServiceType.SETTINGS,type1);

    type1 = ServiceType.from("SETTings");
    Assert.assertEquals(ServiceType.SETTINGS,type1);

    type1 = ServiceType.from("Viewer");
    Assert.assertEquals(ServiceType.VIEWER,type1);

    type1 = ServiceType.from("");
    Assert.assertEquals(ServiceType.UNKNOWN,type1);

    type1 = ServiceType.from(null);
    Assert.assertEquals(ServiceType.UNKNOWN,type1);

  }
}
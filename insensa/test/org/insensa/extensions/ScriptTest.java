package org.insensa.extensions;

import org.insensa.utils.TestUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.*;

@Category(org.insensa.Integration.class)
public class ScriptTest {

  @Test(expected = IOException.class)
  public void extract_notExistingFile() throws IOException {
    Script script = new Script();
    File rast1File = TestUtils.getFileStream("TemplateCut.jar");
    assertNotNull(rast1File);


    script.setArchiveFile(rast1File);
    script.setFilename("bla.xml");
    script.setLanguage(Languages.XML);

    script.extractFromFile();
  }

  @Test
  public void extract() throws IOException {
    Script script = new Script();
    File rast1File = TestUtils.getFileStream("TemplateCut.jar");
    assertNotNull(rast1File);


    script.setArchiveFile(rast1File);
    script.setFilename("template_cut_ui.xml");
    script.setLanguage(Languages.XML);

    script.extractFromFile(Files.createTempDirectory("_pref").toFile());

  }
}
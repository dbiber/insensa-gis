package org.insensa.extensions;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServicePriorityTest {

  @Test
  public void createFromName() {
    ServicePriority priority = ServicePriority.fromName("medium");
    Assert.assertEquals(ServicePriority.MEDIUM,priority);

    priority = ServicePriority.fromName("low");
    Assert.assertEquals(ServicePriority.LOW,priority);

    priority = ServicePriority.fromName("lowest");
    Assert.assertEquals(ServicePriority.LOWEST,priority);

    priority = ServicePriority.fromName("high");
    Assert.assertEquals(ServicePriority.HIGH,priority);

    priority = ServicePriority.fromName("highest");
    Assert.assertEquals(ServicePriority.HIGHEST,priority);


  }

  @Test
  public void testOrderComparisons() {
    Assert.assertTrue(ServicePriority.LOW.lowerThen(ServicePriority.MEDIUM));
    Assert.assertTrue(ServicePriority.HIGH.higherThen(ServicePriority.MEDIUM));
    Assert.assertFalse(ServicePriority.MEDIUM.lowerThen(ServicePriority.MEDIUM));
  }
}
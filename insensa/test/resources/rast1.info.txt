> summary(rast1)
             layer
Min.    -0.4725827
1st Qu.  0.6880551
Median   1.0067603
3rd Qu.  1.3202537
Max.     2.2493692
NA's     0.0000000
> nrow(freq(rast1, digits=3))
[1] 355
> quantile(rast1)
        0%        25%        50%        75%       100%
-0.4725827  0.6880551  1.0067603  1.3202537  2.2493692

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

public class GisFileFactory {
  private static final Logger log = LoggerFactory.getLogger(GisFileFactory.class);

  public static IGisFile create(IGisFile oldFile) throws IOException {
    return create(oldFile, oldFile.getAbsolutePath());
  }

  /**
   * Creates a new instance of {@link IGisFile} as a copy of oldFile.
   * This is only a reference to a file, no physical file will be created.
   * If the file does not exists, copy all low level File attributes to the new {@link IGisFile}
   *
   * So the difference between this and the old file is primarily the fileRef in {@link IGisFile}
   *
   * @param newFullFileName the file to open. If the file exists (does not have to),
   *                        the file will be opened and information's will be read.
   * @apiNote this method only makes sence if the file newFullFileName does not exist!!
   * @see RasterFile#RasterFile(String)
   * @see ShapeFile#ShapeFile(String)
   */
  public static IGisFile create(IGisFile oldFile,
                                String newFullFileName) throws IOException {
    Objects.requireNonNull(oldFile);
    Objects.requireNonNull(newFullFileName);
    switch (oldFile.getType()) {
      case GEO_TIFF:
        return new RasterFile((RasterFile) oldFile, newFullFileName);
      case SHAPE:
        return new ShapeFile((ShapeFile) oldFile, newFullFileName);
      default:
        throw new RuntimeException("Could not create IGisFile");
    }
  }

  /**
   * Creates a new instance of {@link IGisFile} dependent on the
   * the extension of the absoluteFilePath. This is only a reference to a file,
   * no physical file will be created
   *
   * If the file extension could not determine the file type, result will be empty.
   *
   * @param absoluteFilePath the file to open. If the file exists (does not have to),
   *                         the file will be opened and information's will be read
   * @see RasterFile#RasterFile(String)
   * @see ShapeFile#ShapeFile(String)
   */
  public static Optional<IGisFile> create(String absoluteFilePath) {
    GisFileType type = GisFileType.byName(FilenameUtils.getExtension(absoluteFilePath));
    if (type == GisFileType.UNKNOWN) {
      File absFile = new File(absoluteFilePath);
      if (absFile.isDirectory() && new File(absFile, "w001001.adf").exists()) {
        type = GisFileType.GEO_TIFF;
      }
    }
    switch (type) {
      case GEO_TIFF:
        return Optional.of(new RasterFile(absoluteFilePath));
      case SHAPE:
        return Optional.of(new ShapeFile(absoluteFilePath));
      default:
        return Optional.empty();
    }
  }

}

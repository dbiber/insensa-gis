/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;

import org.insensa.FileInspector;
import org.insensa.GenericGisFileSet;
import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.commands.LinkFileCommand;
import org.insensa.commands.ModelCommand;
import org.insensa.commands.MoveFileCommand;
import org.insensa.commands.RenameFileSetCommand;
import org.insensa.commands.RenameRasterFileCommand;
import org.insensa.connections.StackFilesThreadPools;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.MultipleFileInfoReaderThreadPool;
import org.insensa.optionfilechanger.MultipleFileOptionThreadPool;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.RefreshTreeNodeListener;
import org.insensa.view.View;
import org.insensa.view.dialogs.DeleteFilesConfirmDialog;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.insensa.view.dialogs.processes.ConnectionProgressTree;
import org.insensa.view.dialogs.processes.InfoReaderProgressTree;
import org.insensa.view.dialogs.processes.OptionFileChangerProgressTree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


public class ControllerFileEditing {
  private List<DefaultMutableTreeNode> fileNodeList = new ArrayList<>();
  private List<IGisFileContainer> fileList = new ArrayList<>();
  private List<DefaultMutableTreeNode> fileSetNodeList = new ArrayList<>();
  private List<GenericGisFileSet> fileSetList = new ArrayList<>();

  private List<DefaultMutableTreeNode> infoReaderNodeList = new ArrayList<>();
  private List<InfoReader> infoReaderList = new ArrayList<>();

  private List<DefaultMutableTreeNode> infoConnectionNodeList = new ArrayList<>();
  private List<InfoConnection> infoConnectionList = new ArrayList<>();

  private List<DefaultMutableTreeNode> optionNodeList = new ArrayList<>();
  private List<OptionFileChanger> optionFileChangerList = new ArrayList<>();
  private List<DefaultMutableTreeNode> connectionNodeList = new ArrayList<>();
  private List<ConnectionFileChanger> connectionList = new ArrayList<>();
  private View view;
  private Model model;
  private TreePath[] treePaths;
  private DefaultMutableTreeNode targetTreeNode;

  public ControllerFileEditing(TreePath[] treePaths, View view, Model model) {
    this.view = view;
    this.model = model;
    this.treePaths = treePaths;
    if (treePaths != null)
      for (TreePath treePath : treePaths) {
        Object[] treeObj = treePath.getPath();
        Object lastObj = treeObj[treeObj.length - 1];
        if (lastObj instanceof DefaultMutableTreeNode) {
          if (((DefaultMutableTreeNode) lastObj).getUserObject() instanceof IGisFileContainer) {
            this.fileNodeList.add((DefaultMutableTreeNode) lastObj);
            this.fileList.add((IGisFileContainer) ((DefaultMutableTreeNode) lastObj).getUserObject());
          } else if (((DefaultMutableTreeNode) lastObj).getUserObject() instanceof GenericGisFileSet) {
            this.fileSetNodeList.add((DefaultMutableTreeNode) lastObj);
            this.fileSetList.add((GenericGisFileSet) ((DefaultMutableTreeNode) lastObj).getUserObject());
          } else if (((DefaultMutableTreeNode) lastObj).getUserObject() instanceof InfoReader) {
            this.infoReaderNodeList.add((DefaultMutableTreeNode) lastObj);
            this.infoReaderList.add((InfoReader) ((DefaultMutableTreeNode) lastObj).getUserObject());
          } else if (((DefaultMutableTreeNode) lastObj).getUserObject() instanceof InfoConnection) {
            this.infoConnectionNodeList.add((DefaultMutableTreeNode) lastObj);
            this.infoConnectionList.add((InfoConnection) ((DefaultMutableTreeNode) lastObj).getUserObject());
          } else if (((DefaultMutableTreeNode) lastObj).getUserObject() instanceof ConnectionFileChanger) {
            this.connectionNodeList.add((DefaultMutableTreeNode) lastObj);
            this.connectionList.add((ConnectionFileChanger) ((DefaultMutableTreeNode) lastObj).getUserObject());
          } else if (((DefaultMutableTreeNode) lastObj).getUserObject() instanceof OptionFileChanger) {
            this.optionNodeList.add((DefaultMutableTreeNode) lastObj);
            this.optionFileChangerList.add((OptionFileChanger) ((DefaultMutableTreeNode) lastObj).getUserObject());
          }
        }

      }
  }

  private boolean checkFilesInFileSet(IGisFileSet fileSet) {
    for (IGisFileContainer iterFile : fileSet.getFileList()) {
      if (!iterFile.getFileObservers().isEmpty() || ((FileInspector) iterFile).isLocked())
        return false;
    }
    for (IGisFileSet iterFileSet : fileSet.getChildSets()) {
      if (this.checkFilesInFileSet(iterFileSet) == false)
        return false;
    }
    return true;
  }

  public void copyFiles() throws Exception {
    if (this.targetTreeNode == null)
      throw new IOException("ControllerFileEditing: copyFiles: no target defined");
    GenericGisFileSet fileSet;

    if (this.targetTreeNode.getUserObject() instanceof GenericGisFileSet)
      fileSet = (GenericGisFileSet) this.targetTreeNode.getUserObject();
    else {
      return;
    }

    List<ModelCommand> commandList = new ArrayList<ModelCommand>();
    for (int i = 0; i < this.fileList.size(); i++) {
      if (this.fileList.get(i).getConnectionFileChanger() != null
          && !this.fileList.get(i).getConnectionFileChanger().isUsed()) {
        continue;
      }
      commandList.add(new LinkFileCommand(fileSet, this.fileList.get(i)));
      ViewCommandManager.executeCommands(this.view, new RefreshTreeNodeListener(this.targetTreeNode, this.view), commandList);
    }
  }


  private void deleteConnection(ConnectionFileChanger con) throws IOException {
    if (con != null) {
      for (IGisFileContainer iterFile : con.getSourceFileList()) {
        iterFile.removeFileObserver(con);
      }
      if (!con.isUsed()) {
        this.model.getActiveProject().removeConnection(con);
      }
    }
  }

  public void executeAllConnections() throws IOException {
    List<IGisFileContainer> fileContainers = new ArrayList<>();
    fileContainers.addAll(fileList);
    connectionList.forEach(connectionFileChanger -> {
      fileContainers.add(connectionFileChanger.getOutputFileContainer());
    });
    if (fileContainers.isEmpty()) {
      return;
    }

    DefaultMutableTreeNode nodeToRefresh;
    if (!fileNodeList.isEmpty()) {
      nodeToRefresh = (DefaultMutableTreeNode) this.fileNodeList.get(0).getRoot();
    } else if (!connectionNodeList.isEmpty()) {
      nodeToRefresh = (DefaultMutableTreeNode) this.connectionNodeList.get(0).getRoot();
    } else {
      return;
    }
    ConnectionProgressTree progressCon =
        new ConnectionProgressTree(this.view, nodeToRefresh);

//        ConnectionProgressTree progressCon =
//                new ConnectionProgressTree(this.view,
//                        (DefaultMutableTreeNode) this.fileNodeList.get(0).getRoot());
    StackFilesThreadPools threadPool = new StackFilesThreadPools();
    for (IGisFileContainer iFile : fileContainers) {
      if (iFile.getConnectionFileChanger() != null
          && !iFile.getConnectionFileChanger().isUsed())

        for (InfoConnection infoConnection : iFile.getInfoConnections()) {
          if (!infoConnection.isUsed()) {
            threadPool.addPluginExec(infoConnection);
          }
        }
      threadPool.addPluginExec(iFile.getConnectionFileChanger());
    }
    progressCon.setThreadPool(threadPool);
    threadPool.setWorkerList(progressCon);
    threadPool.executeThreads();
  }

  public void executeAllInfoConnections() throws IOException {
    StackFilesThreadPools threadPool = new StackFilesThreadPools();
    List<InfoConnection> infoConnections = new ArrayList<>();
    infoConnections.addAll(getInfoConnectionList());
    for (IGisFileContainer iterFile : this.fileList) {
      infoConnections.addAll(iterFile.getInfoConnections());
    }
    for (InfoConnection infoConnection : infoConnections) {
      if (!infoConnection.isUsed()) {
        threadPool.addPluginExec(infoConnection);
      }
    }
    DefaultMutableTreeNode nodeToRefresh;
    if (!fileNodeList.isEmpty()) {
      nodeToRefresh = (DefaultMutableTreeNode) this.fileNodeList.get(0).getRoot();
    } else if (!infoConnectionNodeList.isEmpty()) {
      nodeToRefresh = (DefaultMutableTreeNode) this.infoConnectionNodeList.get(0).getRoot();
    } else {
      return;
    }
    ConnectionProgressTree progressCon =
        new ConnectionProgressTree(this.view, nodeToRefresh);


//    for (IGisFileContainer iterFile: this.fileList) {
//
//    }
    progressCon.setThreadPool(threadPool);
    threadPool.setWorkerList(progressCon);
    threadPool.executeThreads();
  }

  public void executeAllInfoReaders() {
    InfoReaderProgressTree progressTree;
    try {
      List<IGisFileContainer> fileList = this.fileList;
      List<IGisFileContainer> activeFileList = new ArrayList<>();
      for (IGisFileContainer iFile : fileList) {
        if (iFile.getUnusedInfoReaderCount() > 0) {
          activeFileList.add(iFile);
        }
      }
      progressTree = new InfoReaderProgressTree(fileList, this.view, false);

      // Create the new ThreatPool for all Files with InfoReader's
      MultipleFileInfoReaderThreadPool threadPool = new MultipleFileInfoReaderThreadPool();
      progressTree.setThreadPool(threadPool);
      // get All InfoReaders from all Files and add these to
      // the new Thread Pool
      for (int i = 0; i < activeFileList.size(); i++) {
        List<InfoReader> tmpList = activeFileList.get(i).getInfoReaders();
        for (int j = 0; j < tmpList.size(); j++) {
          threadPool.addInfoThread(activeFileList.get(i), tmpList.get(j));
        }
      }

      // Now Let the new Thread Pool execute all Threads
      threadPool.executeThreads(activeFileList, progressTree.getWorkerStatusList());
    } catch (IOException e2) {
      JOptionPane.showMessageDialog(null, e2.getMessage());
    }
  }


  public void executeAllOptionFileChanger() {
    OptionFileChangerProgressTree progressTree;
    try {
      List<IGisFileContainer> fileList = this.fileList;
      List<IGisFileContainer> activeFileList = new ArrayList<>();
      // DefaultMutableTreeNode((DefaultMutableTreeNode)fileNodeList.get(0).getParent()).getPath();
      List<DefaultMutableTreeNode> fileSetNodeList = new ArrayList<DefaultMutableTreeNode>();
      // DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode)
      // treeNode.getParent();
      // if(fileSetNodeList.contains(parentNode))
      // continue;
      // else
      // fileSetNodeList.add(parentNode);
      //
      for (int i = 0; i < fileList.size(); i++) {
        IGisFileContainer iFile = fileList.get(i);
        if (iFile.getUnusedOptionCount() > 0) {
          activeFileList.add(iFile);
          DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) this.fileNodeList.get(i).getParent();
          if (fileSetNodeList.contains(parentNode))
            continue;
          else
            fileSetNodeList.add(parentNode);
        }
      }
      progressTree = new OptionFileChangerProgressTree(activeFileList, fileSetNodeList, this.view, false);
      // progressTree = new OptionFileChangerProgressTree(fileList, org.insensa.view,
      // false);
      MultipleFileOptionThreadPool threadPool = new MultipleFileOptionThreadPool();
      progressTree.setThreadPool(threadPool);
      for (int i = 0; i < activeFileList.size(); i++) {
        List<OptionFileChanger> tmpList = activeFileList.get(i).getOptions();
        for (int j = 0; j < tmpList.size(); j++) {
          threadPool.addOptionThread(activeFileList.get(i), tmpList.get(j));
        }
      }
      threadPool.executeThreads(activeFileList, progressTree.getWorkerStatusList());
    } catch (IOException e) {
      JOptionPane.showMessageDialog(null, e.getMessage());
    }
  }

  public List<ConnectionFileChanger> getConnectionList() {
    return this.connectionList;
  }

  public List<DefaultMutableTreeNode> getConnectionNodeList() {
    return this.connectionNodeList;
  }

  public List<IGisFileContainer> getFileList() {
    return this.fileList;
  }

  public List<DefaultMutableTreeNode> getFileNodeList() {
    return this.fileNodeList;
  }

  public List<GenericGisFileSet> getFileSetList() {
    return fileSetList;
  }

  public List<DefaultMutableTreeNode> getFileParentNodes() {
    List<DefaultMutableTreeNode> fsNodeList = new ArrayList<DefaultMutableTreeNode>();
    for (DefaultMutableTreeNode iNode : this.fileNodeList) {
      if (iNode.getParent() != null)
        if (iNode.getParent() instanceof DefaultMutableTreeNode)
          if (((DefaultMutableTreeNode) iNode.getParent()).getUserObject() instanceof GenericGisFileSet)
            if (!(fsNodeList.contains(iNode.getParent())))
              fsNodeList.add((DefaultMutableTreeNode) iNode.getParent());
    }
    return fsNodeList;
  }

  public List<InfoConnection> getInfoConnectionList() {
    return infoConnectionList;
//    List<InfoConnection> infoConnectionList = new ArrayList<InfoConnection>();
//    for (IGisFileContainer iFile: this.fileList) {
//      ConnectionFileChanger iCon = iFile.getConnectionFileChanger();
//      if (iCon != null && iCon instanceof InfoConnection)
//        infoConnectionList.add((InfoConnection) iCon);
//    }
//    return infoConnectionList;
  }

  public List<InfoReader> getInfoReaderList() {
    return this.infoReaderList;
  }

  public int getNumOfDifferentObjects() {
    int num = 0;
    if (this.infoConnectionList.size() > 0)
      num++;
    if (this.connectionList.size() > 0)
      num++;
    if (this.infoReaderList.size() > 0)
      num++;
    if (this.optionFileChangerList.size() > 0)
      num++;
    if (this.fileList.size() > 0)
      num++;
    if (this.fileSetList.size() > 0)
      num++;
    return num;
  }

  public int getReadInfoReaderCount() {
    int cnt = 0;
    for (InfoReader iReader : this.infoReaderList)
      if (iReader.isUsed())
        cnt++;
    return cnt;
  }

  public List<InfoReader> getReadInfoReaderList() {
    List<InfoReader> readReader = new ArrayList<InfoReader>();
    for (InfoReader iReader : this.infoReaderList)
      if (iReader.isUsed())
        readReader.add(iReader);
    return readReader;
  }

  public List<OptionFileChanger> getUnusedOptionFileChanger() {
    return this.optionFileChangerList.stream()
        .filter(optionFileChanger -> !optionFileChanger.isUsed())
        .collect(Collectors.toList());
  }

  public List<DefaultMutableTreeNode> getReadInfoReaderNodeList() {
    List<DefaultMutableTreeNode> readReader = new ArrayList<DefaultMutableTreeNode>();
    for (DefaultMutableTreeNode iNode : this.infoReaderNodeList) {
      InfoReader reader = (InfoReader) iNode.getUserObject();
      if (reader.isUsed())
        readReader.add(iNode);
    }
    return readReader;
  }

  public DefaultMutableTreeNode getTargetTreeNode() {
    return this.targetTreeNode;
  }

  public void moveFiles() throws Exception {
    if (this.targetTreeNode == null)
      throw new IOException("ControllerFileEditing: moveFiles: no target defined");
    GenericGisFileSet fileSet;

    if (this.targetTreeNode.getUserObject() instanceof GenericGisFileSet)
      fileSet = (GenericGisFileSet) this.targetTreeNode.getUserObject();
    else {
      return;
    }
    List<ModelCommand> commandList = new ArrayList<ModelCommand>();
    ModelCommand command;
    for (int i = 0; i < this.fileList.size(); i++) {
      // TODO BUG 1002
      if (!this.fileList.get(i).getFileObservers().isEmpty() || this.fileList.get(i).isLocked()) {
        JOptionPane.showMessageDialog(this.view, "This file has dependencies or is locked\n and can not be moved", "File Deleting Error",
            JOptionPane.ERROR_MESSAGE);
        continue;
      }
      command = new MoveFileCommand((GenericGisFileSet) ((DefaultMutableTreeNode) this.fileNodeList.get(i).getParent()).getUserObject(), fileSet,
          this.fileList.get(i));
      commandList.add(command);
    }

    List<DefaultMutableTreeNode> refreshList = new ArrayList<DefaultMutableTreeNode>();
    refreshList.addAll(this.getFileParentNodes());
    refreshList.add(this.targetTreeNode);
    ViewCommandManager.executeCommands(this.view, new RefreshTreeNodeListener(refreshList, this.view), commandList);
  }


  public void removeDescendantNodes() {
    List<DefaultMutableTreeNode> tmpNodeList = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> tmpNodeList2 = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> tmpNodeList3 = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> tmpNodeList4 = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> tmpNodeList5 = new ArrayList<DefaultMutableTreeNode>();
    tmpNodeList.addAll(this.fileSetNodeList);
    tmpNodeList2.addAll(this.fileSetNodeList);
    tmpNodeList3.addAll(this.fileNodeList);
    tmpNodeList4.addAll(this.infoReaderNodeList);
    tmpNodeList5.addAll(this.connectionNodeList);

    for (int i = 0; i < tmpNodeList.size() - 1; i++) {
      for (int j = i + 1; j < tmpNodeList2.size(); j++) {
        if (tmpNodeList.get(i).isNodeDescendant(tmpNodeList2.get(j))) {
          int index = this.fileSetNodeList.indexOf(tmpNodeList2.get(j));
          if (index >= 0) {
            this.fileSetNodeList.remove(index);
            this.fileSetList.remove(index);
          }
        } else if (tmpNodeList.get(i).isNodeAncestor(tmpNodeList2.get(j))) {
          int index = this.fileSetNodeList.indexOf(tmpNodeList.get(i));
          if (index >= 0) {
            this.fileSetNodeList.remove(index);
            this.fileSetList.remove(index);
          }
        }
      }
    }
    tmpNodeList.clear();
    tmpNodeList.addAll(this.fileSetNodeList);
    for (int i = 0; i < tmpNodeList.size(); i++) {
      for (int j = 0; j < tmpNodeList3.size(); j++) {
        if (tmpNodeList.get(i).isNodeDescendant(tmpNodeList3.get(j))) {
          int index = this.fileNodeList.indexOf(tmpNodeList3.get(j));
          if (index >= 0) {
            this.fileNodeList.remove(index);
            this.fileList.remove(index);
          }
        }
      }
      for (int j = 0; j < tmpNodeList4.size(); j++) {
        if (tmpNodeList.get(i).isNodeDescendant(tmpNodeList4.get(j))) {
          int index = this.infoReaderNodeList.indexOf(tmpNodeList4.get(j));
          if (index >= 0) {
            this.infoReaderNodeList.remove(index);
            this.infoReaderList.remove(index);
          }
        }
      }
      for (int j = 0; j < tmpNodeList5.size(); j++) {
        if (tmpNodeList.get(i).isNodeDescendant(tmpNodeList5.get(j))) {
          int index = this.connectionNodeList.indexOf(tmpNodeList5.get(j));
          if (index >= 0) {
            this.connectionNodeList.remove(index);
            this.connectionList.remove(index);
          }
        }
      }
    }

    tmpNodeList.clear();
    tmpNodeList.addAll(this.fileNodeList);
    tmpNodeList2.clear();
    tmpNodeList2.addAll(this.infoReaderNodeList);
    for (int i = 0; i < tmpNodeList.size(); i++) {
      for (int j = 0; j < tmpNodeList2.size(); j++) {
        if (tmpNodeList.get(i).isNodeDescendant(tmpNodeList2.get(j))) {
          int index = this.infoReaderNodeList.indexOf(tmpNodeList2.get(j));
          if (index >= 0) {
            this.infoReaderNodeList.remove(index);
            this.infoReaderList.remove(index);
          }
        }
      }
    }

  }

  /**
   * FIXME nicht mehr genutzt seitdem die Connection an der Datei sind anstatt
   * an einem FileSet.
   */
  public void removeSelectedConnection() throws IOException {
    List<DefaultMutableTreeNode> parentFileSetNodes = new ArrayList<DefaultMutableTreeNode>();
    for (DefaultMutableTreeNode connNode : this.connectionNodeList) {
      DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) connNode.getParent().getParent();
      if (parentFileSetNodes.contains(parentNode) == false)
        parentFileSetNodes.add(parentNode);
      parentNode.getUserObject();
    }
    for (DefaultMutableTreeNode ifNode : parentFileSetNodes)
      this.view.refreshTreeNode(ifNode, true);
  }

  public void removeSelectedFiles() throws IOException {
    List<DefaultMutableTreeNode> fileNodeList = this.getFileNodeList();
    List<DefaultMutableTreeNode> linkFiles = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> hardCopyFiles = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> lockedFiles = new ArrayList<DefaultMutableTreeNode>();
    List<DefaultMutableTreeNode> dependFiles = new ArrayList<>();
    for (DefaultMutableTreeNode iObj : fileNodeList) {
      IGisFileContainer selFile = (IGisFileContainer) iObj.getUserObject();
      String outputFile = selFile.getAbsOutputDirectoryPath() + selFile.getOutputFileName();
      if (!selFile.getFileObservers().isEmpty()) {
        dependFiles.add(iObj);
      } else if (selFile.isLocked()) {
        lockedFiles.add(iObj);
      } else if (selFile.getAbsolutePath().equals(outputFile) && selFile.getGisFile().exists())
        hardCopyFiles.add(iObj);
      else {
        linkFiles.add(iObj);
      }
    }
    if (!hardCopyFiles.isEmpty()) {
      int answer = DeleteFilesConfirmDialog.OPTION_UNKNOWN;
      for (DefaultMutableTreeNode node : hardCopyFiles) {
        IGisFileContainer selFile = (IGisFileContainer) node.getUserObject();
        if (answer != DeleteFilesConfirmDialog.OPTION_ALL) {
          DeleteFilesConfirmDialog dia = new DeleteFilesConfirmDialog(this.view, true);
          dia.getTextArea().setText("Do you really want to delete the file \n\""
              + selFile.getAbsolutePath()
              + "\" \nfrom your hard disk");
          dia.setVisible(true);
          answer = dia.getOption();
        }
        if (answer == DeleteFilesConfirmDialog.OPTION_YES
            || answer == DeleteFilesConfirmDialog.OPTION_ALL) {
          ConnectionFileChanger con = selFile.getConnectionFileChanger();
          this.deleteConnection(con);

          DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();
          if (parentNode.getUserObject() instanceof GenericGisFileSet) {
            GenericGisFileSet selFileSet = (GenericGisFileSet) parentNode.getUserObject();
            selFile.delete();
            selFileSet.removeGisFile(selFile);
          }
        } else if (answer == DeleteFilesConfirmDialog.OPTION_NO) {
          continue;
        } else if (answer == DeleteFilesConfirmDialog.OPTION_CANCEL) {
          for (DefaultMutableTreeNode iNode : this.getFileParentNodes()) {
            this.view.refreshTreeNode(iNode, true);
          }
          return;
        }
      }
    }
    if (!linkFiles.isEmpty()) {
      for (DefaultMutableTreeNode iObj : linkFiles) {
        IGisFileContainer selFile = (IGisFileContainer) iObj.getUserObject();
        ConnectionFileChanger con = selFile.getConnectionFileChanger();
        this.deleteConnection(con);
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) iObj.getParent();
        if (parentNode != null && (parentNode.getUserObject()) != null && (parentNode.getUserObject() instanceof GenericGisFileSet)) {
          GenericGisFileSet selFileSet = (GenericGisFileSet) parentNode.getUserObject();
          selFile.deleteLink();
          selFileSet.removeGisFile(selFile);
        }
      }
    }
    String message = "";
    if (!dependFiles.isEmpty())
      message += "-Some files have dependencies an can not be removed\n";
    if (!lockedFiles.isEmpty())
      message += "-Some files are locked an can not be removed";
    if (!message.isEmpty())
      JOptionPane.showMessageDialog(this.view, message, "File Deleting Error", JOptionPane.ERROR_MESSAGE);
    for (DefaultMutableTreeNode iNode : this.getFileParentNodes())
      this.view.refreshTreeNode(iNode, true);
  }

  public void removeSelectedFileSets() throws IOException {
    for (DefaultMutableTreeNode fileSetNode : this.fileSetNodeList) {
      DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) fileSetNode.getParent();
      GenericGisFileSet fileSet = (GenericGisFileSet) fileSetNode.getUserObject();
      if (this.checkFilesInFileSet(fileSet) == false) {
        JOptionPane.showMessageDialog(this.view, "This fileset has dependencies or locked files\n and can not be removed", "File Set Deleting Error",
            JOptionPane.ERROR_MESSAGE);
        return;
      }

      int answer = JOptionPane.showConfirmDialog(this.view, "do you really want to delete this" + "FileSet and all files from your hard disk");
      if (answer == JOptionPane.YES_OPTION) {
        if (parentNode.isRoot()) {
          this.removeSelectedFileSetsDependencies(fileSet);
          this.model.getActiveProject().removeFileSet(fileSet);
          this.view.getViewProject().getFileSetView().removeNodeIter(fileSetNode);
          this.view.getViewProject().getFileSetView().updateTree();
        } else if (parentNode.getUserObject() instanceof GenericGisFileSet) {
          this.removeSelectedFileSetsDependencies(fileSet);
          ((GenericGisFileSet) parentNode.getUserObject()).removeChildFileSet(fileSet);
          this.view.getViewProject().getFileSetView().removeNodeIter(fileSetNode);
          this.view.getViewProject().getFileSetView().updateTree();
        }
      }
    }
  }

  private void removeSelectedFileSetsDependencies(IGisFileSet fileSet) throws IOException {
    List<IGisFileSet> childSets = fileSet.getChildSets();
    if (childSets != null)
      for (IGisFileSet iSet : childSets)
        this.removeSelectedFileSetsDependencies(iSet);
    for (IGisFileContainer selFile : fileSet.getFileList()) {
      ConnectionFileChanger con = selFile.getConnectionFileChanger();
      this.deleteConnection(con);
    }
  }

  public void removeSelectedInfoReader() throws IOException {
    List<DefaultMutableTreeNode> parentFileNodes = new ArrayList<DefaultMutableTreeNode>();
    for (DefaultMutableTreeNode infoNode : this.infoReaderNodeList) {
      DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) infoNode.getParent();
      if (!parentFileNodes.contains(parentNode)) {
        parentFileNodes.add(parentNode);
      }
      RasterFileContainer file = (RasterFileContainer) parentNode.getUserObject();
      file.removeInfoReader((InfoReader) infoNode.getUserObject());
    }
    for (DefaultMutableTreeNode ifNode : parentFileNodes) {
      this.view.refreshTreeNode(ifNode, true);
    }
  }

  public void removeSelectedInfoConnection() throws IOException {
    List<DefaultMutableTreeNode> parentFileNodes = new ArrayList<>();
    for (DefaultMutableTreeNode infoNode : this.infoConnectionNodeList) {
      DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) infoNode.getParent();
      if (!parentFileNodes.contains(parentNode)) {
        parentFileNodes.add(parentNode);
      }
      RasterFileContainer file = (RasterFileContainer) parentNode.getUserObject();
      file.removeInfoConnection((InfoConnection) infoNode.getUserObject());
    }
    for (DefaultMutableTreeNode ifNode : parentFileNodes) {
      this.view.refreshTreeNode(ifNode, true);
    }
  }

  public void removeSelectedItem() throws IOException {
    if (this.treePaths.length == 1) {
      if (this.fileList.size() == 1) {
        this.removeSelectedFiles();
      } else if (this.fileSetNodeList.size() == 1) {
        DefaultMutableTreeNode fileSetNode = this.fileSetNodeList.get(0);
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) fileSetNode.getParent();
        GenericGisFileSet fileSet = (GenericGisFileSet) fileSetNode.getUserObject();
        if (!this.checkFilesInFileSet(fileSet)) {
          JOptionPane.showMessageDialog(this.view, "This fileset has dependencies or locked files\n and can not be removed",
              "File Set Deleting Error", JOptionPane.ERROR_MESSAGE);
          return;
        }
        int answer = JOptionPane.showConfirmDialog(this.view, "do you really want to delete this" + "FileSet and all files from your hard disk");
        if (answer == JOptionPane.YES_OPTION) {
          if (parentNode.isRoot()) {
            this.removeSelectedFileSetsDependencies(fileSet);
            this.model.getActiveProject().removeFileSet(fileSet);
            this.view.getViewProject().getFileSetView().removeNodeIter(fileSetNode);
            this.view.getViewProject().getFileSetView().updateTree();
          } else if (parentNode.getUserObject() instanceof GenericGisFileSet) {
            this.removeSelectedFileSetsDependencies(fileSet);
            ((GenericGisFileSet) parentNode.getUserObject()).removeChildFileSet(fileSet);
            this.view.refreshTreeNode(parentNode, true);
          }
        }
      } else if (this.infoReaderNodeList.size() == 1) {
        DefaultMutableTreeNode infoNode = this.infoReaderNodeList.get(0);
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) infoNode.getParent();
        RasterFileContainer file = (RasterFileContainer) parentNode.getUserObject();
        file.removeInfoReader((InfoReader) infoNode.getUserObject());
        this.view.refreshTreeNode(parentNode, true);
      } else if (this.connectionNodeList.size() == 1) {
        DefaultMutableTreeNode connNode = this.connectionNodeList.get(0);
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) connNode.getParent().getParent();
        this.view.refreshTreeNode(parentNode, true);
      } else if (this.optionNodeList.size() == 1) {
        DefaultMutableTreeNode optionNode = this.optionNodeList.get(0);
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) optionNode.getParent();
        RasterFileContainer file = (RasterFileContainer) parentNode.getUserObject();
        file.removeOptionFileChanger((OptionFileChanger) optionNode.getUserObject());
        this.view.refreshTreeNode(parentNode, true);
      } else if (this.infoConnectionNodeList.size() == 1) {
        DefaultMutableTreeNode infoConnectionNode = this.infoConnectionNodeList.get(0);
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) infoConnectionNode.getParent();
        IGisFileContainer file = (IGisFileContainer) parentNode.getUserObject();
        file.removeInfoConnection((InfoConnection) infoConnectionNode.getUserObject());
        this.view.refreshTreeNode(parentNode, true);
      }
    }
  }

  public void removeSelectedItems() throws IOException {
    this.removeDescendantNodes();
    this.removeSelectedInfoReader();
    this.removeSelectedOptions();
    this.removeSelectedInfoConnection();
    //    this.removeSelectedConnection();
    this.removeSelectedFiles();
    this.removeSelectedFileSets();
    this.view.refreshGui();
  }


  public void removeSelectedOptions() throws IOException {
    List<DefaultMutableTreeNode> parentFileNodes = new ArrayList<DefaultMutableTreeNode>();
    for (DefaultMutableTreeNode optionNode : this.optionNodeList) {
      DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) optionNode.getParent();
      if (parentFileNodes.contains(parentNode) == false)
        parentFileNodes.add(parentNode);
      RasterFileContainer file = (RasterFileContainer) parentNode.getUserObject();
      file.removeOptionFileChanger((OptionFileChanger) optionNode.getUserObject());
    }
    for (DefaultMutableTreeNode ifNode : parentFileNodes)
      this.view.refreshTreeNode(ifNode, true);
  }

  public void renameSelected() throws Exception {
    if (this.treePaths.length == 1) {
      if (this.fileSetList.size() == 1) {
        GenericGisFileSet selSet = this.fileSetList.get(0);

        if (this.checkFilesInFileSet(selSet) == false) {
          JOptionPane.showMessageDialog(this.view, "This fileset has dependencies or locked files\n and can not be renamed", "Fileset Rename Error",
              JOptionPane.ERROR_MESSAGE);
          return;
        }

        String newFileSetName = JOptionPane.showInputDialog("New name of this file set ?", selSet.getName());
        if (newFileSetName != null) {
          RenameFileSetCommand command = new RenameFileSetCommand(selSet, newFileSetName);
          ViewCommandManager.executeCommands(this.view,
              new RefreshTreeNodeListener((DefaultMutableTreeNode) this.view.getActiveTreeNode(), this.view), command);
        }
      } else if (this.fileList.size() == 1) {
        IGisFileContainer selFile = this.fileList.get(0);

        if (!selFile.getFileObservers().isEmpty()) {
          JOptionPane.showMessageDialog(this.view, "This file has dependencies \n and can not be renamed", "File Rename Error",
              JOptionPane.ERROR_MESSAGE);
          return;
        }
        if (selFile.isLocked()) {
          JOptionPane.showMessageDialog(this.view, "This file is locked\n and can not be renamed", "File Rename Error", JOptionPane.ERROR_MESSAGE);
          return;
        }

        GenericGisFileSet fileSet = (GenericGisFileSet) ((DefaultMutableTreeNode) this.fileNodeList.get(0).getParent()).getUserObject();
        String newFileName = JOptionPane.showInputDialog("New name of this file?", selFile.getOutputFileName());
        if (newFileName != null) {
          RenameRasterFileCommand renameCommand = new RenameRasterFileCommand(fileSet, selFile, newFileName);
          ViewCommandManager.executeCommands(this.view, new RefreshTreeNodeListener((DefaultMutableTreeNode) this.fileNodeList.get(0).getParent(),
              this.view), renameCommand);
        }
      }
    }
  }

  public void setTargetTreePath(DefaultMutableTreeNode targetTreeNode) {
    this.targetTreeNode = targetTreeNode;
  }


}

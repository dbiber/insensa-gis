/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;


import org.insensa.view.View;
import org.insensa.view.image.ImageViewToolbar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ImageViewToolBarController
{
	private static final Logger log = LoggerFactory.getLogger(ImageViewToolBarController.class);

	private class ArrangeWindowAll implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			JDesktopPane desktopPane = ImageViewToolBarController.this.view.getViewProject().getImageView();
			JInternalFrame[] frames = desktopPane.getAllFrames();
			Dimension dim = desktopPane.getSize();
			List<JInternalFrame> frameList = new ArrayList<JInternalFrame>();
			for (JInternalFrame frame : frames)
			{
				if (frame.isShowing())
					frameList.add(frame);
			}
			if (frameList.size() <= 0)
				return;
			int wCount = (int) Math.round(Math.sqrt((double) frameList.size()));
			double div = (double) frameList.size() / (double) wCount;
			double ceil = Math.ceil(div);
			int hCount = (int) ceil;
			int w = dim.width / wCount;
			int h = dim.height / hCount;

			int fIter = 0;
			for (int i = 0; i < wCount; i++)
			{
				for (int j = 0; j < hCount; j++)
				{
					if (fIter >= frameList.size())
						return;
					frameList.get(fIter).setSize(new Dimension(w, h));
					frameList.get(fIter).setLocation(i * w, j * h);
					fIter++;
				}
			}
		}
	}

	private class ArrangeWindowHorizontal implements ActionListener
	{

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			JDesktopPane desktopPane = ImageViewToolBarController.this.view.getViewProject().getImageView();
			JInternalFrame[] frames = desktopPane.getAllFrames();
			Dimension dim = desktopPane.getSize();
			List<JInternalFrame> frameList = new ArrayList<JInternalFrame>();
			for (JInternalFrame frame : frames)
			{
				if (frame.isShowing())
					frameList.add(frame);
			}
			if (frameList.size() <= 0)
				return;
			int w = dim.width / frameList.size();
			for (int i = 0; i < frameList.size(); i++)
			{
				frameList.get(i).setSize(new Dimension(w, frameList.get(i).getSize().height));
				frameList.get(i).setLocation(i * w, 0);
			}
		}
	}

	private class ArrangeWindowVertical implements ActionListener
	{

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			JDesktopPane desktopPane = ImageViewToolBarController.this.view.getViewProject().getImageView();
			JInternalFrame[] frames = desktopPane.getAllFrames();
			Dimension dim = desktopPane.getSize();
			List<JInternalFrame> frameList = new ArrayList<JInternalFrame>();
			for (JInternalFrame frame : frames)
			{
				if (frame.isShowing())
					frameList.add(frame);
			}
			if (frameList.size() <= 0)
				return;
			int h = dim.height / frameList.size();
			for (int i = 0; i < frameList.size(); i++)
			{
				frameList.get(i).setSize(new Dimension(frameList.get(i).getSize().width, h));
				frameList.get(i).setLocation(0, i * h);
			}
		}
	}

	private class CloseAllWindows implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			JDesktopPane desktopPane = ImageViewToolBarController.this.view.getViewProject().getImageView();
			JInternalFrame[] frames = desktopPane.getAllFrames();
			for (JInternalFrame frame : frames)
				try
				{
					frame.setClosed(true);
				} catch (PropertyVetoException e1)
				{
					log.error( "UNKNOWN", e1);
				}
		}
	}

	private class CollabseAll implements ActionListener
	{

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			JDesktopPane desktopPane = ImageViewToolBarController.this.view.getViewProject().getImageView();
			JInternalFrame[] frames = desktopPane.getAllFrames();
			desktopPane.getSize();
			List<JInternalFrame> frameList = new ArrayList<JInternalFrame>();
			for (JInternalFrame frame : frames)
			{
				if (frame.isShowing())
					frameList.add(frame);
			}
			if (frameList.size() <= 0)
				return;
			for (int i = 0; i < frameList.size(); i++)
			{
				try
				{
					frameList.get(i).setIcon(true);
				} catch (PropertyVetoException e)
				{
					log.error( "UNKNOWN", e);
				}
			}
		}
	}

	private class RestoreAllWindows implements ActionListener
	{
		
		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			JDesktopPane desktopPane = ImageViewToolBarController.this.view.getViewProject().getImageView();
			JInternalFrame[] frames = desktopPane.getAllFrames();
			for (JInternalFrame frame : frames)
				try
				{
					frame.setIcon(false);
				} catch (PropertyVetoException e1)
				{
					log.error( "UNKNOWN", e1);
				}
		}
	}

	private ImageViewToolbar toolbar;

	private View view;

	public ImageViewToolBarController(View view)
	{
		this.view = view;
		this.toolbar = view.getViewProject().getImageViewToolbar();
		// TODO Auto-generated constructor stub
	}


	public void initActions()
	{
		this.toolbar.getButtonHorizontal().addActionListener(new ArrangeWindowHorizontal());
		this.toolbar.getButtonVertical().addActionListener(new ArrangeWindowVertical());
		this.toolbar.getButtonAll().addActionListener(new ArrangeWindowAll());
		this.toolbar.getButtonCloseAll().addActionListener(new CloseAllWindows());
		this.toolbar.getButtonCollapseAll().addActionListener(new CollabseAll());
		this.toolbar.getButtonExpandAll().addActionListener(new RestoreAllWindows());
	}

}

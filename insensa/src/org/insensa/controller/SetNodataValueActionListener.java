/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;


import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSetNodataValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;



public class SetNodataValueActionListener implements ActionListener
{
	private static final Logger log = LoggerFactory.getLogger(SetNodataValueActionListener.class);

	private Model model;
	private View view;

	public SetNodataValueActionListener(Model model, View view)
	{
		this.model = model;
		this.view = view;
	}

	/** 
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		DialogSetNodataValue dialog;
		try
		{
			RasterFileContainer rasterFile = (RasterFileContainer) this.model.getActiveGisFileInformation();
			int answer = JOptionPane.YES_OPTION;
			if (!rasterFile.getAbsolutePath().equals(rasterFile.getAbsOutputDirectoryPath() + rasterFile.getOutputFileName()))
			{
				answer = JOptionPane.showConfirmDialog(this.view, "It is risky to change a original file"
						+ "and with some types of files, this feature is not possible.\n"
						+ "It is recommended to create a new instance of this file before continueing" + "\n\n" + "Continue anyway ?", "Warning",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			}
			if (answer == JOptionPane.YES_OPTION)
			{
				dialog = new DialogSetNodataValue(rasterFile);
				dialog.setVisible(true);
			}

		} catch (IOException e1)
		{
			log.error( "UNKNOWN", e1);
		}
		// RasterFileContainer aFile =
		// model.getActiveGisFileInformation();
		// try
		// {
		// aFile.writeNodataValueMax();
		// aFile.refresh();
		// } catch (IOException e1)
		// {
		// log.error("UNKNOWN",e1);
		// } catch (GisFileException e1)
		// {
		// log.error("UNKNOWN",e1);
		// }
	}

}

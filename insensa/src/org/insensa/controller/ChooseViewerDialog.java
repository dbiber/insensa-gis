/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;

import org.insensa.extensions.Service;


public class ChooseViewerDialog extends JDialog
{
	private class ChooseIdActionListener implements ActionListener
	{

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			ChooseViewerDialog.this.id = ((JButton) e.getSource()).getText();
			ChooseViewerDialog.this.selectedService = ChooseViewerDialog.this.serviceMap.get(e.getSource());
			ChooseViewerDialog.this.setVisible(false);
		}
	}

	private class EscapeKeyListener extends KeyAdapter
	{
		
		/** 
		 * 
		 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyPressed(KeyEvent e)
		{
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
				ChooseViewerDialog.this.setVisible(false);
		}

		/** 
		 * 
		 * @see java.awt.event.KeyAdapter#keyReleased(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyReleased(KeyEvent e)
		{
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
				ChooseViewerDialog.this.setVisible(false);
		}
	}


	private static final long serialVersionUID = -2521414899563034445L;
	private javax.swing.JButton jButton1;
	private List<Service> serviceList;
	private String id;
	private Map<JButton, Service> serviceMap;

	private Service selectedService;

	/**
	 * @param serviceList
	 * @param owner
	 */
	public ChooseViewerDialog(List<Service> serviceList, Frame owner)
	{
		super(owner, "");
		this.serviceList = serviceList;
		this.serviceMap = new HashMap<JButton, Service>();
		this.initComponents();
	}

	/**
	 * @return
	 */
	public String getId()
	{
		return this.id;
	}

	/**
	 * @return
	 */
	public Service getSelectedService()
	{
		return this.selectedService;
	}


	private void initComponents()
	{

		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		this.setUndecorated(true);
		this.getContentPane().setLayout(new javax.swing.BoxLayout(this.getContentPane(), javax.swing.BoxLayout.PAGE_AXIS));
		Dimension size;
		for (int i = 0; i < this.serviceList.size(); i++)
		{
			this.jButton1 = new javax.swing.JButton();
			this.jButton1.addActionListener(new ChooseIdActionListener());
			this.jButton1.addKeyListener(new EscapeKeyListener());
			this.jButton1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
			this.jButton1.setText(this.serviceList.get(i).getName());
			this.jButton1.setContentAreaFilled(false);
			this.serviceMap.put(this.jButton1, this.serviceList.get(i));
			this.getContentPane().add(this.jButton1);
		}

		this.pack();
		Iterator<JButton> iter = this.serviceMap.keySet().iterator();
		while (iter.hasNext())
		{
			JButton button = iter.next();
			size = button.getPreferredSize();
			button.setMaximumSize(new Dimension(this.getContentPane().getSize().width, size.height));
			button.setPreferredSize(new Dimension(this.getContentPane().getSize().width, size.height));
			button.setSize(new Dimension(this.getContentPane().getSize().width, size.height));
			button.setFocusPainted(false);
		}

	}

}

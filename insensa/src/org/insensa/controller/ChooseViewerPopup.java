/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.insensa.extensions.Service;


public class ChooseViewerPopup extends JPopupMenu
{
	private class ChooseService implements ActionListener
	{

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			ChooseViewerPopup.this.choice = ((JMenuItem) e.getSource()).getText();
		}

	}


	private static final long serialVersionUID = 2017886716733465672L;
	private String choice;


	public ChooseViewerPopup()
	{
	}

	/**
	 * @return
	 */
	public String getChoice()
	{
		return this.choice;
	}

	/**
	 * @param serviceList
	 * @param invoker
	 * @param x
	 * @param y
	 */
	public void show(List<Service> serviceList, Component invoker, int x, int y)
	{
		for (int i = 0; i < serviceList.size(); i++)
		{
			JMenuItem item = new JMenuItem(serviceList.get(i).getName());
			item.addActionListener(new ChooseService());
			this.add(item);
		}
		super.show(invoker, x, y);
	}
}

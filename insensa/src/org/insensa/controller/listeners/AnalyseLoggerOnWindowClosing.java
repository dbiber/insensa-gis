package org.insensa.controller.listeners;

import org.insensa.InsensaErrorLogger;
import org.insensa.helpers.IOHelper;
import org.insensa.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class AnalyseLoggerOnWindowClosing extends WindowAdapter {

    private static final Logger log = LoggerFactory.getLogger(AnalyseLoggerOnWindowClosing.class);


    private View view;

    public AnalyseLoggerOnWindowClosing(View view) {
        this.view = view;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        // If an rrror accured
        if (InsensaErrorLogger.LoggerState.getInstance().isLogged()) {
            boolean desktopIsSupported = false;
            if (Desktop.isDesktopSupported()) {
                if (Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
                    desktopIsSupported = true;
                }
            }

            String message = "WARNING: "
                    + "Some Logs were found \n"
                    + "If you had problems with the program, create an issue:\n"
                    + "https://bitbucket.org/dbiber/insensa-gis/issues\n\n"
                    + "Describe the Problem as detailed as possible and attach the newest log files.\n"
                    + "The files can be found in \"" + IOHelper.getInstance().getLoggingDir() + "\"";
            if (!desktopIsSupported) {
                JOptionPane.showMessageDialog(view,
                        message,
                        "Logs detected", JOptionPane.WARNING_MESSAGE);
            } else {
                message += "\n\n" + "Go to Issue Tracker ?";
                try {
                    int answer = JOptionPane.showConfirmDialog(view, message, "Logs detected", JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE);
                    if (answer == JOptionPane.YES_OPTION) {
                        Desktop desktop = Desktop.getDesktop();
                        URI dennisUri = new URI("https://bitbucket.org/dbiber/insensa-gis/issues");
                        desktop.browse(dennisUri);
                    }

                } catch (URISyntaxException | IOException e1) {
                    log.error("UNKNOWN", e1);
                }
            }
        }
    }
}

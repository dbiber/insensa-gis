package org.insensa.controller.listeners;

import org.insensa.Environment;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ShutdownROnWindowClosing extends WindowAdapter {

    private static final Logger log = LoggerFactory.getLogger(ShutdownROnWindowClosing.class);

    @Override
    public void windowClosing(WindowEvent e) {
        if (Environment.getRootProperties().isRServeRunning()) {
            try {
                //TODO: Listener is only added after Insensa
                // start screen is closed (project opened/created),
                // so wie have to change the time to disconnect
                RConnection connection = new RConnection();
                if (connection.isConnected()) {
                    log.info("Shutting down Rserve");
                    connection.shutdown();
                }
            } catch (RserveException e1) {
                log.error("Error disconnecting Rserve", e1);
            }
        }
    }
}

package org.insensa.controller.listeners;

import org.insensa.Model;
import org.insensa.User;
import org.insensa.view.View;
import org.insensa.view.ViewDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class CloseViewOnWindowClosing extends WindowAdapter {

    private static final Logger log = LoggerFactory.getLogger(CloseViewOnWindowClosing.class);


    private Model model;
    private View view;

    public CloseViewOnWindowClosing(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        User user = model.getActiveUser();
        if (model != null && user != null && user.getActiveProject() != null)
            user.closeProject();
        // Check if there is a Description left to Save
        ViewDescription description = view.getViewProject().getDescriptionView();
        if (!description.getText().isEmpty()
                && description.getRasterFile() != null
                && (description.getRasterFile().getDescription() == null
                || (description.getRasterFile().getDescription() != null
                && !description.getText()
                .equals(description.getRasterFile().getDescription())))) {
            // If Yes, Save If
            try {
                description.getRasterFile().setDescription(description.getText());
            } catch (IOException e1) {
                log.error("UNKNOWN", e1);
            }
        }
    }
}

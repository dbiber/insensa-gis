/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.controller;

import de.bibertech.javahelp.viewer.view.HelpMainGui;

import org.insensa.Environment;
import org.insensa.GenericGisFileSet;
import org.insensa.GenericGisFileSet.RenameErrorException;
import org.insensa.GisFileContainer;
import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.LowPrioException;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.commands.ImportConfigCommand;
import org.insensa.commands.ImportFolderCommand;
import org.insensa.commands.ModelCommand;
import org.insensa.commands.RTranslatorParsingCommand;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.StackConnection;
import org.insensa.connections.StackFilesThreadPools;
import org.insensa.controller.listeners.AnalyseLoggerOnWindowClosing;
import org.insensa.controller.listeners.CloseViewOnWindowClosing;
import org.insensa.controller.listeners.ShutdownROnWindowClosing;
import org.insensa.exceptions.CreateSettingsViewException;
import org.insensa.extensions.ActionFactory;
import org.insensa.extensions.ActionLink;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.GroupPluginExec;
import org.insensa.extensions.IPluginExec;
import org.insensa.extensions.IPluginSettings;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.extensions.MenuExtension;
import org.insensa.extensions.ViewSettingsFactory;
import org.insensa.helpers.AttributeTable;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.model.generic.IScriptConfigurationTranslator;
import org.insensa.model.generic.RScriptConfigurationTranslator;
import org.insensa.optionfilechanger.Normalizing;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.view.ToolBar;
import org.insensa.view.TreeTestTransferHandler;
import org.insensa.view.View;
import org.insensa.view.ViewDescription;
import org.insensa.view.ViewFileSet;
import org.insensa.view.ViewFileSetConnectionPopup;
import org.insensa.view.ViewFileSetFilePopup;
import org.insensa.view.ViewFileSetInfoConnectionPopup;
import org.insensa.view.ViewFileSetInfoReaderPopup;
import org.insensa.view.ViewFileSetPupup;
import org.insensa.view.ViewProject;
import org.insensa.view.dialogs.AboutDialog;
import org.insensa.view.dialogs.ChooseOptionFileChanger;
import org.insensa.view.dialogs.ChooseOutputDialog;
import org.insensa.view.dialogs.CreateUserDialog;
import org.insensa.view.dialogs.DialogAddConnectionTypes;
import org.insensa.view.dialogs.DialogChooseInfoReader;
import org.insensa.view.dialogs.DialogNewProject;
import org.insensa.view.dialogs.DialogOpenProject;
import org.insensa.view.dialogs.DialogProgressInfoReader;
import org.insensa.view.dialogs.DialogSetInfoReaderProperties;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.insensa.view.dialogs.processes.ConnectionProgressTree;
import org.insensa.view.dialogs.sensi.SensiMainGui;
import org.insensa.view.extensions.ExtensionsManagerUi;
import org.insensa.view.generic.swing.GenericSwingDialog;
import org.insensa.view.menubar.MenuBar;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.awt.Window;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TooManyListenersException;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;


/**
 * This class is the main class for setting action.
 * Controller knows the model and View and changes
 * information between them
 */
public class Controller {
  private static final Logger log = LoggerFactory.getLogger(Controller.class);

  private Model model;
  private View view;
  private JFileChooser folderFileChooser;
  private ChooseOutputDialog chooseOutput = null;
  private ExtensionManager exManager;
  private boolean listenerAdded = false;
  private ImageViewToolBarController imageViewToolBarController;
  private List<ITreeSelectionListener> treeSelectionListeners = new ArrayList<>();

  /**
   * @param exManager All actions that should be
   *                  defined at startup are assign here
   */
  public Controller(Model mModel, View mView, ExtensionManager exManager) {
    this.model = mModel;
    this.view = mView;
    this.exManager = exManager;
    this.folderFileChooser = new JFileChooser();
    UIManager.put("OptionPane.cancelButtonText", "Cancel");
    UIManager.put("OptionPane.noButtonText", "No");
    UIManager.put("OptionPane.okButtonText", "Ok");
    UIManager.put("OptionPane.yesButtonText", "Yes");

    this.view.getMenuView().getMenuBar().getMenuFile().getMenuItemNewProject()
        .addActionListener(new NewProjectListener());
    this.view.getMenuView().getMenuBar().getMenuFile().getMenuItemOpenProject()
        .addActionListener(new OpenProjectListener());
    this.view.getMenuView().getMenuBar().getMenuFile().getMenuItemImport()
        .addActionListener(new FolderImportListener());

    this.view.getMenuView().getMenuBar().getMenuHelp().getMenuItemHelp()
        .addActionListener(new OpenHelpActionListener());

    this.view.getStartScreen().getButtonNewProject()
        .addActionListener(new NewProjectListener());
    this.view.getStartScreen().getButtonOpenProject()
        .addActionListener(new OpenProjectListener());
//    this.view.getStartScreen().getButtonImportConfig()
//        .addActionListener(new ImportConfigActionListener());
    this.view.getStartScreen().getButtonAbout().addActionListener(new OpenDialogAboutListener());
    this.view.getStartScreen().getButtonHelp().addActionListener(new OpenHelpActionListener());
    this.view.getStartScreen().getButtonNewUser().addActionListener(new CreateNewUserListener());
    this.view.getMenuView().getMenuBar().getMenuFile().getMenuItemClose()
        .addActionListener(new CloseWindowActionListener(view, model));

    this.view.getMenuView().getMenuBar().getMenuHelp().getMenuItemAbout()
        .addActionListener(new OpenDialogAboutListener());
    // insensa.view.getMenuView().getMenuBar().getMenuEdit().getItemAddAllSensitivityConnections().addActionListener(new
    // AddAllSensitivityConnectionsListener());
    view.getMenuView().getMenuBar().getMenuEdit().getItemAddAllSensitivityConnections()
        .addActionListener(new AddAllSensitivityConnectionsListener());
    this.view.getMenuView().getMenuBar().getMenuEdit().getItemExecuteAllConnections()
        .addActionListener(new ExecuteAllConnections());
    this.view.getMenuView().getMenuBar().getMenuExtensions().getMenuItemManager()
        .addActionListener(e -> new ExtensionsManagerUi().start());

    this.view.getMenuView().getToolBar().getButtonExecuteAllConnections()
        .addActionListener(new ExecuteAllConnections());
    this.view.getMenuView().getToolBar().getButtonProjects()
        .addActionListener(new OpenProjectListener());
    this.view.getMenuView().getToolBar().getButtonNewProject()
        .addActionListener(new NewProjectListener());
    this.view.addWindowListener(new AnalyseLoggerOnWindowClosing(view));
    this.view.addWindowListener(new ShutdownROnWindowClosing());
  }

  /**
   * Defines all actions once a Project is opened.
   */
  public void addNewActionListener() {
    ViewProject newProject = this.view.getViewProject();
//    RMessageStreamMonitor.getInstance().setMessageStreamListener(newProject.getRLogTailView());
    RMessageStreamMonitor.getInstance().addTailerLister(newProject.getRLogTailView());
    ViewFileSet newFileSet = newProject.getFileSetView();
    JTree fileSetTree = newFileSet.getFileSetTree();
    ViewFileSetPupup fileSetPopup = newFileSet.getFileSetPopup();
    ViewFileSetFilePopup filePopup = newFileSet.getFileSetFilePopup();
    ViewFileSetInfoReaderPopup infoReaderPopup = newFileSet.getInfoReaderPopup();
    ViewFileSetConnectionPopup connectionPopup = newFileSet.getConnectionPopup();
    ViewFileSetInfoConnectionPopup infoConnectionPopup = newFileSet.getInfoConnectionPopup();

    fileSetTree.addMouseListener(new MouseOnFileSetTreeListener());
    fileSetTree.addTreeSelectionListener(new TreeChangeListener());

    fileSetTree.addKeyListener(new TreeKeyListener());

    fileSetTree.setTransferHandler(new TreeTestTransferHandler(this.view, this.model));

    filePopup.getItemAddInformationReader()
        .addActionListener(new AddInformationReaderListener());

    filePopup.getItemAddOptionFileChanger()
        .addActionListener(new AddOptionFileChangerListener());
    filePopup.getItemExecuteAllOptions()
        .addActionListener(new ExecuteAllOptionFileChanger());
    filePopup.getItemExecuteAllInfoReader()
        .addActionListener(new ExecuteAllInformationReader());
    filePopup.getItemExecuteConnection()
        .addActionListener(new ExecuteConnectionListener());

    filePopup.getItemExecuteAllInfoConnections()
        .addActionListener(new ExecuteAllInfoConnections());
    filePopup.getItemAddAllSensitivityConnections()
        .addActionListener(new AddAllSensitivityConnectionsListener());

//    filePopup.getItemAddConnection().addActionListener(new AddConnectionMultiListener());
    filePopup.getItemAddConnection()
        .addActionListener(new AddConnectionTypesListener(view, model, DialogAddConnectionTypes.StackType.CONNECTION));
    this.treeSelectionListeners.add((ITreeSelectionListener)
        filePopup.getItemAddConnection().getActionListeners()[0]);

    filePopup.getItemAddInfoConnection()
        .addActionListener(new AddConnectionTypesListener(view, model, DialogAddConnectionTypes.StackType.INFO_CONNECTION));
    this.treeSelectionListeners.add((ITreeSelectionListener)
        filePopup.getItemAddInfoConnection().getActionListeners()[0]);

    filePopup.getItemStackFiles().addActionListener(new AddConnectionTypesListener(view, model, DialogAddConnectionTypes.StackType.STACK));
    this.treeSelectionListeners.add((ITreeSelectionListener)
        filePopup.getItemStackFiles().getActionListeners()[0]);

    filePopup.getItemSetNodataValue()
        .addActionListener(new SetNodataValueActionListener(this.model, this.view));

    //TODO test: Moved to a class file
    filePopup.getItemExportFile()
        .addActionListener(new ExportFileActionListener(this.view, this.model));

    fileSetPopup.getItemImportFolder().addActionListener(new AddFolderToFileSet());
    //TODO Test: new importer listener for plugins
    fileSetPopup.getItemImportFile().addActionListener(new ImportFileActionListener(view, model));
    fileSetPopup.getItemNewFileSet().addActionListener(new NewChildFileSetListener());

    infoReaderPopup.getItemExecute().addActionListener(new ExecuteInfoReader());

    connectionPopup.getItemExecute().addActionListener(new ExecuteConnectionListener());
    infoConnectionPopup.getItemExecute().addActionListener(new ExecuteInfoConnectionListener());

//    newProject.getSplitpane3().addPropertyChangeListener(new ReziseSplitPane());

    DropTarget newDropTarget = new DropTarget();
    try {
      newDropTarget.addDropTargetListener(new DropObjectOnImageListener(this.view, this.model));
    } catch (TooManyListenersException e1) {
      log.error("UNKNOWN", e1);
    }
    newProject.getImageView().setDropTarget(newDropTarget);

    this.view.getViewProject().getDescriptionView()
        .addFocusListener(new DescriptionFocusListener());

    if (!this.listenerAdded) {
      view.addWindowListener(new CloseViewOnWindowClosing(model, view));
      MenuBar menuBar = this.view.getMenuView().getMenuBar();
      menuBar.getMenuEdit().getItemRefresh().addActionListener(new RefreshTreeListener());
      menuBar.getMenuEdit().getItemAddInfoReader()
          .addActionListener(new AddInformationReaderListener());
      menuBar.getMenuEdit().getItemAddOption()
          .addActionListener(new AddOptionFileChangerListener());

      menuBar.getMenuEdit().getItemExecuteInfoReader()
          .addActionListener(new ExecuteAllInformationReader());
      menuBar.getMenuEdit().getItemExecuteOption()
          .addActionListener(new ExecuteAllOptionFileChanger());
      menuBar.getMenuEdit().getItemExecuteConnection()
          .addActionListener(new ExecuteConnectionListener());
      menuBar.getMenuEdit().getItemExecuteAllConnections()
          .addActionListener(new ExecuteAllConnections());
//      menuBar.getMenuEdit().getItemAddConnection().addActionListener(new AddConnectionMultiListener());
      menuBar.getMenuEdit().getItemAddConnection()
          .addActionListener(new AddConnectionTypesListener(view, model, DialogAddConnectionTypes.StackType.CONNECTION));
      this.treeSelectionListeners.add((ITreeSelectionListener)
          menuBar.getMenuEdit().getItemAddConnection().getActionListeners()[0]);

      menuBar.getMenuEdit().getItemRename()
          .addActionListener(new RenameActionListener(view, model));
      menuBar.getMenuEdit().getItemDelete()
          .addActionListener(new DeleteActionListener(view, model));

      menuBar.getMenuFile().getMenuItemNewFileSet()
          .addActionListener(new NewChildFileSetListener());
      menuBar.getMenuFile().getMenuItemImportFolder()
          .addActionListener(new AddFolderToFileSet());
      //TODO Test: new importer listener for plugins
      menuBar.getMenuFile().getMenuItemImportFile()
          .addActionListener(new ImportFileActionListener(view, model));
      menuBar.getMenuFile().getMenuItemClose()
          .addActionListener(new CloseWindowActionListener(view, model));

      //TODO test: Moved to a class file
      menuBar.getMenuFile().getMenuItemExportFile()
          .addActionListener(new ExportFileActionListener(this.view, this.model));

      //TODO: Test Menu Action Extensions:
      //--------------------------------------------------
      Map<String, JMenuItem> itemNameMap = menuBar.getMenuExtensions().getItemNameMap();
      Iterator<String> nameIter = itemNameMap.keySet().iterator();
      ActionFactory actionFactory = new ActionFactory();
      while (nameIter.hasNext()) {
        String itemName = nameIter.next();
        MenuExtension menuExt = exManager.getMenuExtensions().get(itemName);
        for (ActionLink iActionLink : menuExt.getActionLinkList()) {
          String actionName = iActionLink.getTarget();
          if (actionFactory.contains(actionName) && actionFactory.isDialogAction(actionName)) {
//            AbstractImporterSettingDialog iInsensaDialog = actionFactory.getDialogAction(actionName);
//            itemNameMap.get(itemName).addActionListener(new ImplActionViewListener(iInsensaDialog));
          }
        }
      }
      //-------------------------------------------------

      ToolBar toolBar = this.view.getMenuView().getToolBar();
      toolBar.getButtonRefresh().addActionListener(new RefreshTreeListener());
      toolBar.getButtonNewFileSet().addActionListener(new NewChildFileSetListener());
      //TODO Test: new importer listener for plugins
      toolBar.getButtonImportFile().addActionListener(new ImportFileActionListener(view, model));
      toolBar.getButtonImportFolder().addActionListener(new AddFolderToFileSet());
      toolBar.getButtonAddInfoReader().addActionListener(new AddInformationReaderListener());
      toolBar.getButtonAddOption().addActionListener(new AddOptionFileChangerListener());

      toolBar.getButtonExecuteInfoReader().addActionListener(new ExecuteAllInformationReader());
      toolBar.getButtonExecuteOption().addActionListener(new ExecuteAllOptionFileChanger());
      toolBar.getButtonExecuteConnection().addActionListener(new ExecuteConnectionListener());
//      toolBar.getButtonAddConnection().addActionListener(new AddConnectionMultiListener());
      toolBar.getButtonAddConnection()
          .addActionListener(new AddConnectionTypesListener(view, model, DialogAddConnectionTypes.StackType.CONNECTION));
      this.treeSelectionListeners.add((ITreeSelectionListener)
          toolBar.getButtonAddConnection().getActionListeners()[0]);

      this.imageViewToolBarController = new ImageViewToolBarController(this.view);
      this.imageViewToolBarController.initActions();
      this.listenerAdded = true;
    }
  }

  public Controller getController() {
    return this;
  }

  public void addTreeSelectionListener(ITreeSelectionListener listener) {
    this.treeSelectionListeners.add(listener);
  }

  public void removeTreeSelectionListener(ITreeSelectionListener listener) {
    this.treeSelectionListeners.remove(listener);
  }

  public void setChooseOutput(ChooseOutputDialog chooseOutput) {
    this.chooseOutput = chooseOutput;
  }

  private void addPluginsPropertiesToTable(List<? extends IPluginExec> execs,
                                           Vector<Vector<String>> vData) {
    Vector<String> row;
    for (int i = 0; i < execs.size(); i++) {
      row = new Vector<>();
      row.add("");
      row.add(execs.get(i).toString());
      if (execs.get(i).isUsed()) {
        row.add("Used");
      } else {
        row.add("Unused");
      }
      vData.add(row);

      addPluginPropertiesToTable(execs.get(i), vData);
    }
  }

  private void addPluginPropertiesToTable(IPluginExec exec, Vector<Vector<String>> vData) {
    Vector<String> row;
    AttributeTable data = exec.getData();
    for (int j = 0; j < data.size(); j++) {
      String property = data.getKey(j);
      String value = data.getValue(j);
      row = new Vector<>();
      row.add("");
      row.add(property);
      row.add(value);
      vData.add(row);
    }
  }

  private void setOptionProperties(List<? extends IPluginExec> options,
                                   Vector<Vector<String>> vData) {
    if (options != null) {
      Vector row = new Vector<String>();
      row.add("File Options");
      row.add("");
      row.add("");
      vData.add(row);
      addPluginsPropertiesToTable(options, vData);
    }
  }

  private void setConnectionProperties(ConnectionFileChanger conn,
                                       Vector<Vector<String>> vData) {
    if (conn instanceof StackConnection) {
      return;
    }
    if (conn != null) {
      Vector row = new Vector<String>();
      row.add("Connection");
      row.add("");
      row.add("");
      vData.add(row);

      row = new Vector<String>();
      row.add("");
      row.add(conn.toString());
      row.add("");
      vData.add(row);

      addPluginPropertiesToTable(conn, vData);
    }
  }

  private void setInfoConnectionProperties(List<? extends IPluginExec> infoConnections,
                                           Vector<Vector<String>> vData) {
    if (infoConnections != null) {
      Vector row = new Vector<String>();
      row.add("Info Connection");
      row.add("");
      row.add("");
      vData.add(row);
      addPluginsPropertiesToTable(infoConnections, vData);
    }
  }

  private void setFileProperties(IGisFileContainer gisFileInformation,
                                 Vector<Vector<String>> vData) {
    Vector<String> row = new Vector<String>();
    row.add("File");
    row.add("");
    row.add("");
    vData.add(row);

    row = new Vector<>();
    row.add("");
    row.add("Source File");
    row.add(gisFileInformation.getAbsolutePath());
    vData.add(row);

    row = new Vector<>();
    row.add("");
    row.add("Target File");
    row.add(gisFileInformation.getAbsOutputDirectoryPath() + gisFileInformation.getOutputFileName());
    vData.add(row);

    IGisFile gisFile = gisFileInformation.getGisFile();
    gisFile.getGisAttributes().forEach(gisAttribute -> {
      Vector<String> vector = new Vector<>();
      vector.add("");
      vector.add(gisAttribute.getName());
      vector.add(gisAttribute.getValue());
      vData.add(vector);
    });
  }

  private void setInfoReaderProperties(List<? extends IPluginExec> infoReader,
                                       Vector<Vector<String>> vData) {
    if (infoReader != null) {
      Vector row = new Vector<String>();
      row.add("File Informations");
      row.add("");
      row.add("");
      vData.add(row);

      addPluginsPropertiesToTable(infoReader, vData);
    }
  }

  public void setProperties(IGisFileContainer gisFileInformation) {
    // First Check if there is some Description to save;
    ViewDescription description = this.view.getViewProject().getDescriptionView();
    if (description.getText().isEmpty() == false
        && description.getRasterFile() != null
        && (description.getRasterFile().getDescription() == null || (description.getRasterFile().getDescription() != null && !description.getText()
        .equals(description.getRasterFile().getDescription())))) {
      // If Yes, Save If
      try {
        description.getRasterFile().setDescription(description.getText());
      } catch (IOException e) {
        log.error("UNKNOWN", e);
      }
    }

    this.view.getViewProject().getDescriptionView().setGisFile(gisFileInformation);

    Vector<Vector<String>> vData = new Vector<Vector<String>>();
    setFileProperties(gisFileInformation, vData);
    setOptionProperties(gisFileInformation.getOptions(), vData);
    setConnectionProperties(gisFileInformation.getConnectionFileChanger(), vData);
    setInfoConnectionProperties(gisFileInformation.getInfoConnections(), vData);
    setInfoReaderProperties(gisFileInformation.getInfoReaders(), vData);
    this.view.refreshOptionsView(vData);
  }

  public void treePopupFunction(MouseEvent e) {
    if (!e.isPopupTrigger())
      return;
    JTree tree = this.view.getViewProject().getFileSetView().getFileSetTree();
    ViewFileSetPupup fileSetPopup = this.view.getViewProject().getFileSetView().getFileSetPopup();
    ViewFileSetFilePopup fileSetFilePopup = this.view.getViewProject().getFileSetView().getFileSetFilePopup();
    ViewFileSetInfoReaderPopup infoReaderPopup = this.view.getViewProject().getFileSetView().getInfoReaderPopup();
    ViewFileSetConnectionPopup connectionPopup = this.view.getViewProject().getFileSetView().getConnectionPopup();
    ViewFileSetInfoConnectionPopup infoConnectionPopup = this.view.getViewProject().getFileSetView().getInfoConnectionPopup();

    if (tree.getSelectionPaths() == null || tree.getSelectionPaths().length <= 1)
      tree.setSelectionPath(tree.getPathForLocation(e.getX(), e.getY()));

    ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(), this.view, this.model);
    if (tree.getSelectionCount() > 1 && e.isPopupTrigger()) {
      fileSetFilePopup.setEnableMulti(fileEdit.getFileList(), true);
      fileSetFilePopup.show(e.getComponent(), e.getX(), e.getY());
    } else if (e.isPopupTrigger()) {
      TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());
      tree.setSelectionPath(treePath);
      DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

      tree.setSelectionPath(treePath);
      if (selNode != null) {
        if (selNode.getUserObject() instanceof GenericGisFileSet) {
          fileSetPopup.show(e.getComponent(), e.getX(), e.getY());
        } else if (selNode.getUserObject() instanceof IGisFileContainer) {
          IGisFileContainer file = (IGisFileContainer) selNode.getUserObject();
          ConnectionFileChanger con = file.getConnectionFileChanger();
          if (con instanceof InfoConnection) {
            return;
          }
          if (con instanceof StackConnection) {
            fileSetFilePopup.setEnableMulti(fileEdit.getFileList(), true);
          } else {
            fileSetFilePopup.setEnableMulti(fileEdit.getFileList(), false);
          }
          fileSetFilePopup.show(e.getComponent(), e.getX(), e.getY());
        } else if (selNode.getUserObject() instanceof InfoReader) {
          infoReaderPopup.show(e.getComponent(), e.getX(), e.getY());
        } else if (selNode.getUserObject() instanceof ConnectionFileChanger) {
          connectionPopup.show(e.getComponent(), e.getX(), e.getY());
        } else if (selNode.getUserObject() instanceof InfoConnection) {
          infoConnectionPopup.show(e.getComponent(), e.getX(), e.getY());
        }
      }
    }
  }

  private void reselectTreeEntrySelection() {
    JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
    TreePath path = tree.getSelectionPath();
    TreePath parentPath = path.getParentPath();
    tree.setSelectionPath(parentPath);
    tree.setSelectionPath(path);
    view.refreshGui();
  }

  private boolean askForResetPluginUsage(IPluginExec pluginExec) {
    int answer = JOptionPane
        .showConfirmDialog(Controller.this.view,
            "Do you really want to reuse this Reader?", "Reuse InfoReader",
            JOptionPane.YES_NO_OPTION);
    if (answer == JOptionPane.YES_OPTION) {
      pluginExec.setUsed(false);
      try {
        Controller.this.view.refreshTreeNodeByActive(true);
      } catch (IOException e1) {
        log.error("Error", e1);
      }
      return true;
    } else {
      return false;
    }
  }

  private void openOldGenericDialog(IPluginExec pluginExec) {
    DialogSetInfoReaderProperties tmpDia = new DialogSetInfoReaderProperties(pluginExec);
    tmpDia.setModal(true);
    tmpDia.setVisible(true);
  }

  private void openGenericDialog(IPluginSettings pluginSettings,
                                 IPluginExec pluginExec) {
    IScriptConfigurationTranslator translator =
        ((GenericSwingDialog) pluginSettings).getPresenter().getTranslator();
    RTranslatorParsingCommand cmd = new RTranslatorParsingCommand(
        (RScriptConfigurationTranslator) translator, pluginExec);
    DialogSingleProgress progress = new DialogSingleProgress(view, true);
    cmd.setWorkerStatus(progress);
    ViewCommandManager.executeCommands(view, new IProcessFinishedListener() {
      @Override
      public void canceled() {
      }

      @Override
      public void done() {
        openExplicitSettings(pluginExec, pluginSettings, translator);
      }
    }, cmd);
  }

  private void openExplicitSettings(IPluginExec pluginExec,
                                    IPluginSettings settings,
                                    IScriptConfigurationTranslator translator) {
    if (settings != null) {
      settings.setPluginExec(pluginExec);
      if (settings instanceof Component) {
        ((Component) settings).setLocale(view.getLocale());
      }
      if (settings instanceof Window) {
        ((Window) settings).setLocationRelativeTo(view);
      }
      if (settings instanceof ITreeSelectionListener) {
        this.treeSelectionListeners.add((ITreeSelectionListener) settings);
      }
      settings.startView(result -> {
        try {

          if (settings instanceof ITreeSelectionListener) {
            treeSelectionListeners.remove(settings);
          }
          if (result == ViewSettingsCloseListener.Result.BUTTON_OK) {
            try {
              if (settings instanceof GenericSwingDialog) {
                ((IScriptPluginExec) pluginExec)
                    .setVariables(((GenericSwingDialog) settings).getVariables());
                Environment.refreshDefaultPluginVariables((IScriptPluginExec) pluginExec);
              }
              if (pluginExec instanceof GroupPluginExec) {
                ((GroupPluginExec) pluginExec).refreshPlugin();
              } else {
                pluginExec
                    .getParentFileContainer()
                    .getGisFileInformationStorage()
                    .refreshPluginExec(pluginExec);
              }

            } catch (IOException e) {
              log.error("Error resaving plugin data", e);
            }
            reselectTreeEntrySelection();
          }
        } catch (RuntimeException re) {
          re.printStackTrace();
        } finally {
          try {
            if (translator != null) {
              translator.close();
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

      });

    }
  }

  private void openSettings(IPluginExec pluginExec) throws ClassNotFoundException,
      CreateSettingsViewException, InstantiationException, IllegalAccessException {
    if (pluginExec.isUsed()) {
      if (!(pluginExec instanceof InfoConnection) && !(pluginExec instanceof InfoReader)) {
        return;
      }
      if (pluginExec instanceof InfoConnection && pluginExec.getParentFileContainer().isHardCopy()) {
        return;
      }
      if (!askForResetPluginUsage(pluginExec)) {
        return;
      }
    }
    if (!pluginExec.getVariableList().isEmpty()) {
      openOldGenericDialog(pluginExec);
    } else {
      ViewSettingsFactory factory = new ViewSettingsFactory(Controller.this.exManager);
      IPluginSettings settings = factory.getSettingsPlugin(pluginExec);
      if ((settings instanceof GenericSwingDialog) &&
          ((GenericSwingDialog) settings).getPresenter().getTranslator()
              instanceof RScriptConfigurationTranslator) {
        openGenericDialog(settings, pluginExec);
      } else {
        openExplicitSettings(pluginExec, settings, null);
      }
    }
  }

  class AddAllSensitivityConnectionsListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      SensiMainGui sensiGui;
      try {
        sensiGui = new SensiMainGui(Controller.this.view,
            false, Controller.this.model, Controller.this.view,
            (RasterFileContainer) Controller.this.model.getActiveGisFileInformation());
        Controller.this.chooseOutput = sensiGui;
        sensiGui.setVisible(true);
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(Controller.this.view, e1.getMessage());
        log.error("UNKNOWN", e1);
      }
    }
  }

//  class AddConnectionMultiListener implements ActionListener {
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//      GisFileType targetType;
//
//      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
//      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
//          Controller.this.view, Controller.this.model);
//      if (fileEdit.getFileList().size() <= 1 && !fileEdit.getFileList().get(0).isStack()) {
//        JOptionPane.showMessageDialog(Controller.this.view,
//            "You have to select at least two files or a Connection");
//        return;
//      }
//      targetType = fileEdit.getFileList().get(0).getGisFile().getType();
//      DialogChoosePlugin addConnectionDia =
//          new DialogChoosePlugin(Controller.this.view,
//              ExtensionManager.getInstance()
//                  .getUsableConnectionList(ServiceType.EXEC, targetType),
//              "Connection");
//      addConnectionDia.setModalityType(ModalityType.DOCUMENT_MODAL);
//      addConnectionDia.setVisible(true);
//      String connectionName = addConnectionDia.getSelectedConnection();
//
//      if (connectionName != null) {
//        if (fileEdit.getFileList().size() > 1) {
//          Controller.this.chooseOutput = new DialogAddConnectionTypes(Controller.this.view,
//              false, "Add Connection", Controller.this.model,
//              Controller.this.view, connectionName, fileEdit.getFileList());
//          tree.setSelectionPath(new TreePath(Controller.this.view.getActiveTreePath()[0]));
//          Controller.this.chooseOutput.setVisible(true);
//        } else if (fileEdit.getFileList().size() == 1) {
//          IGisFileContainer fileContainer = fileEdit.getFileList().get(0);
//          if (fileContainer.isStack()) {
//            try {
//              model.createAndAddConnection(fileContainer, connectionName);
//              tree.setSelectionPath(new TreePath(Controller.this.view.getActiveTreePath()[0]));
//
//              DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) view.getActiveTreeNode();
//              view.refreshTreeNode(treeNode, true);
//            } catch (IOException e1) {
//              log.error("Error adding Connection", e1);
//            }
//          }
//        }
//      }
//    }
//  }

//  class AddFilesToFileSet implements ActionListener {
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//      DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode();
//      final JFileChooser fileChooser;
//      fileChooser = new JFileChooser();
//
//      File file = null;
//      file = new File(Controller.this.view.getViewProperties().getLastImportedFilePath());
//      if (file != null && file.exists() && file.isDirectory()) {
//        fileChooser.setCurrentDirectory(file);
//      }
//
//      if (activeNode == null || !(activeNode.getUserObject() instanceof GenericGisFileSet)) {
//        return;
//      }
//      new SwingWorker<Object, Object>() {
//        DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode();
//        FileFilter filter = new AsciiAndTiffFileFilter();
//        FileFilter filter2 = new ArcGridFileFilter();
//        FileView fView = new AsciiAndTiffFileView();
//        FileView fView2 = new ArcGridFileView();
//
//        @Override
//        protected Object doInBackground() throws Exception {
//          fileChooser.addPropertyChangeListener(new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//              String prop = evt.getPropertyName();
//              if (prop.equals(JFileChooser.FILE_FILTER_CHANGED_PROPERTY)) {
//                if (evt.getNewValue() == filter) {
//                  ((JFileChooser) evt.getSource()).setFileView(fView);
//                  ((JFileChooser) evt.getSource()).rescanCurrentDirectory();
//                } else if (evt.getNewValue() == filter2) {
//                  ((JFileChooser) evt.getSource()).setFileView(fView2);
//                  ((JFileChooser) evt.getSource()).rescanCurrentDirectory();
//                }
//              }
//            }
//          });
//          fileChooser.removeChoosableFileFilter(fileChooser.getFileFilter());
//          fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
//          fileChooser.addChoosableFileFilter(this.filter);
//          fileChooser.addChoosableFileFilter(this.filter2);
//          fileChooser.setFileFilter(this.filter);
//          fileChooser.setFileView(this.fView);
//          fileChooser.setMultiSelectionEnabled(true);
//
//          int choise = fileChooser.showOpenDialog(Controller.this.view);
//          if (choise == JFileChooser.APPROVE_OPTION) {
//            File file = fileChooser.getSelectedFile();
//            if (file != null)
//              file = file.getParentFile();
//            if (file != null && file.exists() && file.isDirectory()) {
//              Controller.this.view.getViewProperties().setLastImportedFilePath(file.getAbsolutePath());
//            }
//
//            ModelCommand command = new ImportFilesCommand(Controller.this.model.getActiveGisFileSet(), fileChooser.getSelectedFiles(), true);
//            DialogSingleProgress singleProgress = new DialogSingleProgress(Controller.this.view);
//            command.setWorkerStatus(singleProgress);
//            try {
//              command.execute();
//            } catch (LowPrioException e) {
//              JOptionPane.showMessageDialog(Controller.this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
//              singleProgress.setVisible(false);
//            }
//            return this.activeNode;
//          } else
//            return null;
//        }
//
//        @Override
//        protected void done() {
//          try {
//            if (this.get() != null) {
//              DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) this.get();
//
//              TreePath paths[] = new TreePath[1];
//              paths[0] = new TreePath(activeNode.getPath());
//              Controller.this.view.refreshTreeNode(activeNode, true);
//              Controller.this.view.getViewProject().getFileSetView().expandPaths(paths);
//              Controller.this.view.getViewProject().getFileSetView().refreshSelection(paths);
//            }
//          } catch (IOException | ExecutionException | InterruptedException e) {
//            log.error("Error refreshing Tree", e.getCause());
//          }
//        }
//      }.execute();
//    }
//  }

  class AddFileToModulationSet implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
    }
  }

  class AddFolderToFileSet implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode();
      if (node == null || !(node.getUserObject() instanceof GenericGisFileSet))
        return;

      Controller.this.folderFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      File file = null;
      file = new File(Controller.this.view.getViewProperties().getLastImportedFolderPath());

      if (file.exists() && file.isDirectory()) {
        Controller.this.folderFileChooser.setCurrentDirectory(file);
      }

      int choise = Controller.this.folderFileChooser.showOpenDialog(Controller.this.view);

      if (choise == JFileChooser.APPROVE_OPTION) {
        file = Controller.this.folderFileChooser.getSelectedFile();
        if (file != null && file.exists() && file.isDirectory()) {
          Controller.this.view.getViewProperties()
              .setLastImportedFolderPath(file.getAbsolutePath());
        }
        new SwingWorker<Object, Object>() {
          DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode();

          @Override
          protected Object doInBackground() throws Exception {
            if (this.activeNode == null) {
              return null;
            }
            ModelCommand comm = new ImportFolderCommand(
                Controller.this.model.getActiveGisFileSet(),
                Controller.this.folderFileChooser
                    .getSelectedFile().toString(), true);
            DialogSingleProgress singleProgress = new DialogSingleProgress(Controller.this.view);
            comm.setWorkerStatus(singleProgress);
            try {
              comm.execute();
            } catch (LowPrioException e) {
              JOptionPane.showMessageDialog(Controller.this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
              singleProgress.setVisible(false);
            }
            return this.activeNode;
          }

          @Override
          protected void done() {
            try {
              if (this.get() != null) {
                DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) this.get();

                TreePath paths[] = new TreePath[1];
                paths[0] = new TreePath(activeNode.getPath());
                Controller.this.view.refreshTreeNode(activeNode, true);
                Controller.this.view.getViewProject().getFileSetView().expandPaths(paths);
                Controller.this.view.getViewProject().getFileSetView().refreshSelection(paths);
              }
            } catch (IOException e) {
              log.error("UNKNOWN", e);
              JOptionPane.showMessageDialog(Controller.this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            } catch (InterruptedException | ExecutionException e) {
              log.error("UNKNOWN", e);
            }
          }
        }.execute();
      }
    }
  }

  class AddInformationReaderListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
          Controller.this.view, Controller.this.model);
      int size = fileEdit.getFileNodeList().size();
      if (size <= 0) {
        return;
      }
      DialogChooseInfoReader tmpDia = new DialogChooseInfoReader(fileEdit
          .getFileList().get(0).getGisFile().getType(),
          Controller.this.model, Controller.this.view);
      tmpDia.setModal(true);
      tmpDia.setVisible(true);

      TreePath[] selTreePaths = tree.getSelectionPaths();
      for (DefaultMutableTreeNode treeNode : fileEdit.getFileNodeList()) {
        try {
          Controller.this.view.refreshTreeNode(treeNode, true);
        } catch (IOException e) {
          log.error("UNKNOWN", e);
        }
      }
      Controller.this.view.refreshTreeSelection(selTreePaths);
    }

  }

  class AddOptionFileChangerListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {

      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
          Controller.this.view, Controller.this.model);
      int size = fileEdit.getFileNodeList().size();
      if (size <= 0) {
        return;
      }

      TreePath[] selTreePaths = tree.getSelectionPaths();
      ChooseOptionFileChanger tmpDia = new ChooseOptionFileChanger(
          fileEdit.getFileList().get(0).getGisFile().getType(),
          Controller.this.model,
          Controller.this.view);
      tmpDia.setModal(true);
      tmpDia.setVisible(true);
      for (DefaultMutableTreeNode treeNode : fileEdit.getFileNodeList()) {
        try {
          Controller.this.view.refreshTreeNode(treeNode, true);
        } catch (IOException e) {
          log.error("UNKNOWN", e);
        }
      }
      Controller.this.view.refreshTreeSelection(selTreePaths);
    }

  }

  /**
   * @deprecated Use AddConnectionListener instead
   */
  class AddStandardisationListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        Controller.this.model.addOptionToFileByActive(Normalizing.NAME);
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(Controller.this.view, e1.getMessage());
        log.error("UNKNOWN", e1);
      }
    }
  }

  class CreateNewUserListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      CreateUserDialog dialog = new CreateUserDialog(Controller.this.model, Controller.this.view, true);
      dialog.setVisible(true);
    }
  }

  class DescriptionFocusListener extends FocusAdapter {
    private IGisFileContainer rFile;

    @Override
    public void focusGained(FocusEvent e) {
      Object source = e.getSource();
      if (source instanceof ViewDescription) {
        if (((ViewDescription) source).getRasterFile() != null
            && ((DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode()).getUserObject() instanceof RasterFileContainer
            && ((ViewDescription) source).getRasterFile() == ((DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode()).getUserObject()) {
          this.rFile = ((ViewDescription) source).getRasterFile();
          ((ViewDescription) source).setEditable(true);
          ((ViewDescription) source).getCaret().setVisible(true);
        } else {
          ((ViewDescription) source).setEditable(false);
        }

      }
      super.focusGained(e);
    }

    @Override
    public void focusLost(FocusEvent e) {
      Object source = e.getSource();
      if (source instanceof ViewDescription) {
        if (this.rFile != null && this.rFile.getDescription() != null
            && !this.rFile.getDescription().equals(((ViewDescription) source).getText())) {
        }
        ((ViewDescription) source).setEditable(false);
      }
    }
  }

  class ExecuteAllConnections implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      try {
        ConnectionProgressTree progressCon = new ConnectionProgressTree(
            Controller.this.view, (DefaultMutableTreeNode) tree.getModel().getRoot());
//        StackFilesThreadPools threadPool = new StackFilesThreadPools();
        StackFilesThreadPools threadPool = model.getActiveProject().getConnectionThreadPool();
        //TODO: Removed this because of the premise, that the StackFilesThreadPool
        //Stored in the Project class, will always holt the Connections AND the InfoConnections
        //so it should be anough to just execute.
        //This only works if we always add the InfoConnections to this threadPool
//        for (Runnable iRun : threadPool.getRunnableList()) {
//          for (InfoConnection infoConnection :
//              ((ConnectionFileChanger) iRun).getOutputFileContainer().getInfoConnections()) {
//            threadPool.addPluginExec(infoConnection);
//          }
//          threadPool.addPluginExec(iRun);
//        }
        progressCon.setThreadPool(threadPool);
        threadPool.setWorkerList(progressCon);
        threadPool.executeThreads();
      } catch (IOException e) {
        JOptionPane.showMessageDialog(Controller.this.view, e.getMessage());
      }
    }
  }

  class ExecuteAllInformationReader implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
          Controller.this.view, Controller.this.model);
      fileEdit.executeAllInfoReaders();
    }
  }

  class ExecuteAllOptionFileChanger implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
          Controller.this.view, Controller.this.model);
      fileEdit.executeAllOptionFileChanger();
    }
  }

  class ExecuteInfoConnectionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(
          tree.getSelectionPaths(), Controller.this.view, Controller.this.model);
      try {
        fileEdit.executeAllInfoConnections();
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }
    }
  }

  class ExecuteConnectionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(
          tree.getSelectionPaths(), Controller.this.view, Controller.this.model);
      try {
        fileEdit.executeAllConnections();
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }
    }
  }

  class ExecuteAllInfoConnections implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
          Controller.this.view, Controller.this.model);
      try {
        fileEdit.executeAllInfoConnections();
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }
    }
  }

  class ExecuteInfoReader implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      TreeNode activeNode = Controller.this.view.getActiveTreeNode();

      DialogProgressInfoReader dialog;
      try {
        dialog = new DialogProgressInfoReader(Controller.this.view);
        dialog.setModal(false);
        Controller.this.model
            .executeInfoReaderOnActiveFile(((InfoReader) ((DefaultMutableTreeNode) activeNode).getUserObject()).getId(), dialog);
      } catch (IOException e2) {
        JOptionPane.showMessageDialog(null, e2.getMessage());
      }
    }
  }

  class FolderImportListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
      Controller.this.folderFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      int choice = Controller.this.folderFileChooser.showOpenDialog(Controller.this.view);
      if (choice == JFileChooser.APPROVE_OPTION) {
        if (Controller.this.folderFileChooser.getSelectedFile().isDirectory()) {
        }
      }
    }
  }

  class MouseOnFileSetTreeListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      if (e.getClickCount() == 2) {
        JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();
        TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());
        tree.setSelectionPath(treePath);
        DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

        if (selNode != null) {
          Object userObject = selNode.getUserObject();
          if (userObject instanceof IPluginExec) {
            try {
              openSettings((IPluginExec) selNode.getUserObject());
            } catch (ClassNotFoundException
                | CreateSettingsViewException
                | IllegalAccessException
                | InstantiationException e1) {
              log.error("Could not open settings for plugin \"{}\"",
                  ((IPluginExec) selNode.getUserObject()).getId(),e1);
            }
          } else if (userObject instanceof IGisFileContainer
              && ((IGisFileContainer) userObject).getConnectionFileChanger() != null
              && !((IGisFileContainer) userObject).getConnectionFileChanger().isUsed()) {
            IPluginExec pluginExec = ((IGisFileContainer) userObject).getConnectionFileChanger();
            try {
              openSettings(pluginExec);
            } catch (ClassNotFoundException
                | CreateSettingsViewException
                | IllegalAccessException
                | InstantiationException e1) {
              log.error("Could not open settings for plugin \"{}\"",
                  pluginExec.getId(),e1);
            }

          }
        }
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
      Controller.this.treePopupFunction(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      Controller.this.treePopupFunction(e);
    }
  }

//  class MouseOnImage implements MouseListener {
//
//    @Override
//    public void mouseClicked(MouseEvent arg0) {
//    }
//
//    @Override
//    public void mouseEntered(MouseEvent arg0) {
//    }
//
//    @Override
//    public void mouseExited(MouseEvent arg0) {
//    }
//
//    @Override
//    public void mousePressed(MouseEvent arg0) {
//    }
//
//    @Override
//    public void mouseReleased(MouseEvent arg0) {
//    }
//
//  }

  class NewChildFileSetListener implements ActionListener {

    @SuppressWarnings("unused")
    @Override
    public void actionPerformed(ActionEvent arg0) {
      ViewProject viewProject = Controller.this.view.getViewProject();
      if (viewProject == null)
        return;
      ViewFileSet viewFileSet = viewProject.getFileSetView();
      if (viewFileSet == null)
        return;
      JTree tree = viewFileSet.getFileSetTree();
      if (tree == null)
        return;
      String fileSetName = JOptionPane.showInputDialog("Name of the new file set ?");
      if (fileSetName == null)
        return;
      try {
        if (tree.getSelectionPath() == null || Controller.this.view.getActiveTreeNode() == null
            || !(((DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode()).getUserObject() instanceof GenericGisFileSet)) {
          GenericGisFileSet tmpFileSet;
          tmpFileSet = Controller.this.model.createNewFileSet(fileSetName, true);
          Controller.this.view.addRootFileSet(tmpFileSet);
        } else {
          TreePath[] selTreePaths = tree.getSelectionPaths();
          DefaultMutableTreeNode node = (DefaultMutableTreeNode) Controller.this.view.getActiveTreeNode();
          Object obj = node.getUserObject();
          IGisFileSet tmpSet = Controller.this.model
              .createNewChildFileSet(Controller.this.model.getActiveGisFileSet(),
                  fileSetName, true);
          Controller.this.view.refreshTreeNodeByActive(true);
          Controller.this.view.refreshTreeSelection(selTreePaths);
        }
      } catch (IOException | JDOMException exception) {
        log.error("UNKNOWN", exception);
      }
    }
  }

  class NewProjectListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
      DialogNewProject tmpDia;
      try {
        tmpDia = new DialogNewProject(Controller.this.model, Controller.this.view, Controller.this.getController());
        tmpDia.setModal(true);
        tmpDia.setVisible(true);
      } catch (JDOMException | IOException e) {
        JOptionPane.showMessageDialog(Controller.this.view, e.getMessage());
        log.error("UNKNOWN", e);
      }
    }
  }

  class OpenDialogAboutListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      AboutDialog about = new AboutDialog(null, true);
      about.setLocationRelativeTo(null);
      about.setVisible(true);
    }
  }

  class OpenHelpActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
      try {
        HelpMainGui helpGui = new HelpMainGui(Controller.this.view, true);
        helpGui.setTarget("start_screen");
        helpGui.setVisible(true);
      } catch (JDOMException | IOException e) {
        log.error("UNKNOWN", e);
      }
    }
  }

  class OpenProjectListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      DialogOpenProject tmpDia;
      try {
        tmpDia = new DialogOpenProject(Controller.this.model,
            Controller.this.view, Controller.this);
        tmpDia.setModal(true);
        tmpDia.setVisible(true);
      } catch (LowPrioException e1) {
        JOptionPane.showMessageDialog(Controller.this.view,
            e1.getMessage(),
            "Error",
            JOptionPane.ERROR_MESSAGE);
      } catch (JDOMException | IOException e1) {
        JOptionPane.showMessageDialog(Controller.this.view, e1.getMessage());
        log.error("UNKNOWN", e1);
      }
    }
  }

  /**
   * ActionListener for importing an old config file. Add users and workspaces.
   */
  private class ImportConfigActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      JFileChooser configFileChooser = new JFileChooser(".");
      configFileChooser.addChoosableFileFilter(new FileFilter() {
        @Override
        public String getDescription() {
          return "mainConfig.xml";
        }

        @Override
        public boolean accept(File f) {
          if (f.isDirectory() || f.getName().equals("mainConfig.xml")) {
            return true;
          }
          return false;
        }
      });
      if (configFileChooser.showOpenDialog(Controller.this.view) == JFileChooser.APPROVE_OPTION) {
        File selectedFile = configFileChooser.getSelectedFile();
        if (selectedFile != null) {
          try {
            ImportConfigCommand command = new ImportConfigCommand(model,
                selectedFile.getAbsolutePath());
            command.setWorkerStatus(new DialogSingleProgress(Controller.this.view));
            ViewCommandManager.executeCommands(Controller.this.view, null, command);
          } catch (Exception e1) {
            log.error("UNKNOWN", e1);
          }
        }
      }
    }
  }

  class RefreshTreeListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        Controller.this.view.refreshTreeNodeByActive(true);
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }
    }
  }

//  class ReziseSplitPane implements PropertyChangeListener {
//
//    @Override
//    public void propertyChange(PropertyChangeEvent arg0) {
//      JSplitPane sourceSplitPane = (JSplitPane) arg0.getSource();
//      String propertyName = arg0.getPropertyName();
//      if (propertyName.equals(JSplitPane.LAST_DIVIDER_LOCATION_PROPERTY)) {
//        Component rComp = sourceSplitPane.getRightComponent();
//        if (rComp instanceof ViewImageDraw) {
//
//        }
//      }
//    }
//  }

  class TreeChangeListener implements TreeSelectionListener {
    @Override
    public void valueChanged(TreeSelectionEvent e) {
      JTree tree = (JTree) e.getSource();
      DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
      @SuppressWarnings("unused")
      TreePath[] treePath = tree.getSelectionPaths();
      if (selNode != null) {
        Controller.this.view.setActiveTreePath(selNode.getPath());
        for (ITreeSelectionListener treeSelectionListener : treeSelectionListeners) {
          treeSelectionListener.treeSelected(selNode, selNode.getUserObject());
        }
        if (selNode.getUserObject() instanceof GenericGisFileSet) {
          GenericGisFileSet tmpSet = (GenericGisFileSet) selNode.getUserObject();
          Controller.this.model.setActiveRasterFileSet(tmpSet);
          tmpSet.setActiveParentFileSetIter();
          tmpSet.unsetActiveSetsIter();

          if (Controller.this.chooseOutput != null && Controller.this.chooseOutput.isVisible()) {
            Controller.this.chooseOutput.setFileSet((GenericGisFileSet) selNode.getUserObject());
            Controller.this.chooseOutput.setRefreshingTreeNode(selNode);
          }
        } else if (selNode.getUserObject() instanceof IGisFileContainer) {
          GenericGisFileSet tmpSet = (GenericGisFileSet) ((DefaultMutableTreeNode) selNode.getParent()).getUserObject();
          Controller.this.model.setActiveRasterFileSet(tmpSet);
          tmpSet.setActiveParentFileSetIter();
          tmpSet.unsetActiveSetsIter();
          IGisFileContainer tmpFile = (IGisFileContainer) selNode.getUserObject();
          Controller.this.model.setActiveRasterFile(tmpFile);
          Controller.this.setProperties(tmpFile);
          if (Controller.this.chooseOutput != null && Controller.this.chooseOutput.isVisible()) {
            Controller.this.chooseOutput.setFile(tmpFile);
            Controller.this.chooseOutput.setRefreshingTreeNode(selNode);
          }
        } else if (selNode.getUserObject() instanceof InfoReader) {
          GenericGisFileSet tmpSet = (GenericGisFileSet) ((DefaultMutableTreeNode) selNode.getParent().getParent()).getUserObject();
          Controller.this.model.setActiveRasterFileSet(tmpSet);
          tmpSet.setActiveParentFileSetIter();
          tmpSet.unsetActiveSetsIter();
          IGisFileContainer tmpFile =  (IGisFileContainer) ((DefaultMutableTreeNode) selNode.getParent()).getUserObject();
          Controller.this.model.setActiveRasterFile(tmpFile);
        } else if (selNode.getUserObject() instanceof ConnectionFileChanger) {
          GenericGisFileSet tmpSet = (GenericGisFileSet) ((DefaultMutableTreeNode) selNode.getParent().getParent()).getUserObject();
          Controller.this.model.setActiveRasterFileSet(tmpSet);
          tmpSet.setActiveParentFileSetIter();
          tmpSet.unsetActiveSetsIter();
          RasterFileContainer tmpFile = (RasterFileContainer) ((DefaultMutableTreeNode) selNode.getParent()).getUserObject();
          Controller.this.model.setActiveRasterFile(tmpFile);
        } else if (selNode.getUserObject() instanceof OptionFileChanger) {
          DefaultMutableTreeNode fileNode = (DefaultMutableTreeNode) selNode.getParent();
          DefaultMutableTreeNode fileSetNode = (DefaultMutableTreeNode) fileNode.getParent();
          GenericGisFileSet tmpSet = (GenericGisFileSet) fileSetNode.getUserObject();
          GisFileContainer rasterFile = (GisFileContainer) fileNode.getUserObject();
          Controller.this.model.setActiveRasterFileSet(tmpSet);
          tmpSet.setActiveParentFileSetIter();
          tmpSet.unsetActiveSetsIter();
          Controller.this.model.setActiveRasterFile(rasterFile);
        }
      }
    }
  }

  class TreeKeyListener implements KeyListener {

    @Override
    public void keyPressed(KeyEvent arg0) {
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
      JTree tree = Controller.this.view.getViewProject().getFileSetView().getFileSetTree();

      TreePath[] selPaths = tree.getSelectionPaths();
      if (selPaths == null)
        return;
      ControllerFileEditing fileEdit = new ControllerFileEditing(selPaths, Controller.this.view, Controller.this.model);

      if (selPaths.length > 1) {
        if (arg0.getKeyCode() == KeyEvent.VK_DELETE) {
          try {
            fileEdit.removeSelectedItems();
            Controller.this.view.setActiveTreePath(null);
          } catch (IOException e) {
            JOptionPane.showMessageDialog(Controller.this.view, e.getMessage());
            log.error("UNKNOWN", e);
          }
        }
        if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
          List<OptionFileChanger> optionFileChangers = fileEdit.getUnusedOptionFileChanger();
          if (optionFileChangers.isEmpty()) {
            log.info("No Option Selected");
            return;
          }
          OptionFileChanger fileChanger = optionFileChangers.get(0);
          List<IPluginExec> execs = new ArrayList<>();
          for (OptionFileChanger iFileChanger : optionFileChangers) {
            execs.add(iFileChanger);
            if (!iFileChanger.getId().equals(fileChanger.getId())) {
              log.info("Options are different");
              return;
            }
          }
          try {
            openSettings(new GroupPluginExec(execs));
          } catch (ClassNotFoundException | CreateSettingsViewException | IllegalAccessException | InstantiationException e) {
            log.error("Error executing group ", e);
          }
        }
      }
      if (selPaths.length == 1) {
        if (arg0.getKeyCode() == KeyEvent.VK_DELETE) {
          try {
            fileEdit.removeSelectedItem();
            Controller.this.view.setActiveTreePath(null);
          } catch (IOException e) {
            JOptionPane.showMessageDialog(Controller.this.view, e.getMessage());
            log.error("UNKNOWN", e);
          }
        }
        if (arg0.getKeyCode() == KeyEvent.VK_F2) {
          try {
            fileEdit.renameSelected();
          } catch (RenameErrorException e1) {
            StringBuffer errorNamesBuffer = new StringBuffer();
            for (Object iFile : e1.getErrorList()) {
              errorNamesBuffer.append(((RasterFileContainer) iFile).getName());
              errorNamesBuffer.append(" ");
            }
            JOptionPane.showMessageDialog(Controller.this.view, errorNamesBuffer.toString());
          } catch (Exception e) {
            JOptionPane.showMessageDialog(Controller.this.view, e.getMessage());
            log.error("UNKNOWN", e);
          }
        }
        if (arg0.getKeyCode() == KeyEvent.VK_F5) {
          try {
            Controller.this.view.refreshTreeNodeByActive(true);
          } catch (IOException e) {
            log.error("UNKNOWN", e);
          }
        }
      }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
    }
  }
}

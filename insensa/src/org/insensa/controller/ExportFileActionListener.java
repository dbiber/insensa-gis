/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;


import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.exceptions.CreateSettingsViewException;
import org.insensa.exports.ExportManager;
import org.insensa.exports.FileExporter;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.ViewSettingsFactory;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.exports.ChooseFileExporterDialog;
import org.insensa.view.exports.DefaultFileExporterDialog;
import org.insensa.view.exports.IViewFileExporterSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.SwingWorker;
import javax.swing.tree.DefaultMutableTreeNode;


public class ExportFileActionListener implements ActionListener {
  private static final Logger log = LoggerFactory.getLogger(ExportFileActionListener.class);

  private View view;
  private Model model;


  public ExportFileActionListener(View view, Model model) {
    this.view = view;
    this.model = model;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    ExtensionManager exManager = ExtensionManager.getInstance();
    ControllerFileEditing fileEditing = new ControllerFileEditing(view.getViewProject().getFileSetView().getFileSetTree()
        .getSelectionPaths(), view, model);
    List<IGisFileContainer> fileList = fileEditing.getFileList();
    if (fileList == null || fileList.isEmpty())
      return;

    ExportManager exportManager = new ExportManager();

    ChooseFileExporterDialog dialog = new ChooseFileExporterDialog(model, view);
    dialog.setVisible(true);
    String exporterName = dialog.getSelectedExporter();
    if (exporterName == null)
      return;
    final FileExporter exporter = exportManager.getFileExporter(exporterName);
    if (exporter == null)
      return;
    WorkerStatusCallback callback =
        new WorkerStatusCallback(
            new DialogSingleProgress(view));
    exporter.setWorkerStatusCallback(callback);
    exporter.setRasterFileList(fileEditing.getFileList());
    try {
      IViewFileExporterSetting setting = new ViewSettingsFactory(exManager).getViewFileExporter(exporterName);
      if (setting == null) {
        setting = new DefaultFileExporterDialog();
      }
      setting.setFileExporter(exporter);
      if (setting instanceof Component) {
        ((Component) setting).setLocale(view.getLocale());
      }
      setting.startView(result -> {
        if(result == ViewSettingsCloseListener.Result.BUTTON_OK) {
          new SwingWorker<Object, Object>() {
            @Override
            protected Object doInBackground() throws Exception {
              exporter.export();
              return null;
            }
            @Override
            protected void done() {
              super.done();
            }
          }.execute();
        }
      });

    } catch (ClassNotFoundException |
        IllegalAccessException |
        InstantiationException |
        CreateSettingsViewException e1) {
      log.error( "UNKNOWN", e1);
    }

  }

}

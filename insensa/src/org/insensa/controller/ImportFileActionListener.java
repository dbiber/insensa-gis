/*
 * Copyright (C) 2011-2013 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;


import org.insensa.GenericGisFileSet;
import org.insensa.Model;
import org.insensa.commands.RTranslatorParsingCommand;
import org.insensa.exceptions.CreateSettingsViewException;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.IScriptPluginSettings;
import org.insensa.extensions.ViewSettingsFactory;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.imports.IFileImporter;
import org.insensa.imports.ImportManager;
import org.insensa.model.generic.IScriptConfigurationTranslator;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.RScriptConfigurationTranslator;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.insensa.view.generic.swing.GenericSwingDialog;
import org.insensa.view.imports.ChooseFileImporterDialog;
import org.insensa.view.imports.DefaultFileImporterDialog;
import org.insensa.view.imports.IViewFileImporterSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ImportFileActionListener implements ActionListener {
  private static final Logger log = LoggerFactory.getLogger(ImportFileActionListener.class);


  private View view;
  private Model model;


  public ImportFileActionListener(View view, Model model) {
    this.view = view;
    this.model = model;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    //Class that finds selected Elements
    ControllerFileEditing fileEditing = new ControllerFileEditing(view.getViewProject()
        .getFileSetView()
        .getFileSetTree()
        .getSelectionPaths(),
        view, model);
    List<GenericGisFileSet> fileSetList = fileEditing.getFileSetList();
    if (fileSetList == null || fileSetList.size() != 1)
      return;

    ImportManager importManager = new ImportManager();
    ChooseFileImporterDialog dialog = new ChooseFileImporterDialog();
    dialog.startView(result -> {
    });
    String importerName = dialog.getSelectedImporter();
    if (importerName == null)
      return;
    final IFileImporter fileImporter = importManager.getFileImporter(importerName);
    if (fileImporter == null)
      return;
    WorkerStatusCallback callback = null;
//		callback = new WorkerStatusCallback(new DialogSingleProgress(view, 
//				(DefaultMutableTreeNode) fileEditing.getFileNodeList().get(0).getParent()));
    fileImporter.setWorkerStatusCallback(callback);
    fileImporter.setView(view);
    fileImporter.setTargetFileSet(fileSetList.get(0));

    try {
      Optional<IViewFileImporterSetting> oSetting =
          new ViewSettingsFactory(ExtensionManager.getInstance())
              .getViewFileImporter(importerName);
      IViewFileImporterSetting setting = oSetting.orElse(new DefaultFileImporterDialog());
      setting.setFileImporter(fileImporter);
      if (setting instanceof Component) {
        ((Component) setting).setLocale(view.getLocale());
      }

      if (setting instanceof GenericSwingDialog) {
        IScriptConfigurationTranslator translator =
            ((GenericSwingDialog) setting).getPresenter().getTranslator();
        if (translator instanceof RScriptConfigurationTranslator) {
          RTranslatorParsingCommand cmd = new RTranslatorParsingCommand(
              (RScriptConfigurationTranslator) translator, null);
          DialogSingleProgress progress = new DialogSingleProgress(view, true);
          cmd.setWorkerStatus(progress);
          ViewCommandManager.executeCommands(view, new IProcessFinishedListener() {
            @Override
            public void canceled() {
            }

            @Override
            public void done() {
              startSettings(setting, fileImporter);
            }
          }, cmd);
        } else {
          startSettings(setting, fileImporter);
        }
      } else {
        startSettings(setting, fileImporter);
      }
    } catch (CreateSettingsViewException e1) {
      log.error("UNKNOWN", e1);
    }


  }

  private void startSettings(IViewFileImporterSetting setting, IFileImporter fileImporter) {
    setting.startView(result -> {
      if (result == ViewSettingsCloseListener.Result.BUTTON_OK) {
        Map<String, IVariable> variableMap = null;
        if (setting instanceof IScriptPluginSettings) {
          variableMap = ((IScriptPluginSettings) setting).getVariables();
        }
        fileImporter.doImport(variableMap);
      }
    });
  }

}

package org.insensa.controller;

import javax.swing.tree.DefaultMutableTreeNode;

public interface ITreeSelectionListener {
  void treeSelected(DefaultMutableTreeNode selectedTreeNode, Object selectedObject);
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


import org.insensa.GenericGisFileSet;
import org.insensa.LowPrioException;
import org.insensa.Model;
import org.insensa.commands.ImportFilesCommand;
import org.insensa.commands.ModelCommand;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.helper.ArcGridFileFilter;
import org.insensa.view.dialogs.helper.ArcGridFileView;
import org.insensa.view.dialogs.helper.AsciiAndTiffFileFilter;
import org.insensa.view.dialogs.helper.AsciiAndTiffFileView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AddFilesToFileSet implements ActionListener
{
	private static final Logger log = LoggerFactory.getLogger(AddFilesToFileSet.class);

	public static void importFolder(final View view, final Model model, final File[] fileList, final DefaultMutableTreeNode targetNode)
	{
		if (!(targetNode.getUserObject() instanceof GenericGisFileSet))
			return;
		new SwingWorker<Object, Object>()
		{
			@Override
			protected Object doInBackground() throws Exception
			{
				GenericGisFileSet activeFileSet = (GenericGisFileSet) targetNode.getUserObject();
				ModelCommand command = new ImportFilesCommand(activeFileSet, fileList, true);
				DialogSingleProgress singleProgress = new DialogSingleProgress(view);
				command.setWorkerStatus(singleProgress);
				try
				{
					command.execute();
				} catch (LowPrioException e)
				{
					JOptionPane.showMessageDialog(view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					singleProgress.setVisible(false);
				} catch (IOException e)
				{
					log.error( "UNKNOWN", e);
				}
				return targetNode;
			}

			@Override
			protected void done()
			{
				try
				{
					if (this.get() != null)
					{
						DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) this.get();

						TreePath paths[] = new TreePath[1];
						paths[0] = new TreePath(activeNode.getPath());
						view.refreshTreeNode(activeNode, true);
						view.getViewProject().getFileSetView().expandPaths(paths);
						view.getViewProject().getFileSetView().refreshSelection(paths);
					}
				} catch (InterruptedException e)
				{
					log.error( "UNKNOWN", e);
				} catch (ExecutionException e)
				{
					log.error( "UNKNOWN", e);
				} catch (IOException e)
				{
					log.error( "UNKNOWN", e);
				}
			}
		}.execute();

	}

	private View view;

	private Model model;

	public AddFilesToFileSet(View view, Model model)
	{
		this.view = view;
		this.model = model;
	}

	/** 
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) this.view.getActiveTreeNode();
		final JFileChooser fileChooser = new JFileChooser();

		File file = new File(this.view.getViewProperties().getLastImportedFilePath());
		if (file != null && file.exists() && file.isDirectory()) {
			fileChooser.setCurrentDirectory(file);
		}

		if (activeNode == null || !(activeNode.getUserObject() instanceof GenericGisFileSet))
			return;
		new SwingWorker<Object, Object>()
		{
			DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) AddFilesToFileSet.this.view.getActiveTreeNode();
			FileFilter filter = new AsciiAndTiffFileFilter();
			FileFilter filter2 = new ArcGridFileFilter();
			FileView fView = new AsciiAndTiffFileView();
			FileView fView2 = new ArcGridFileView();

			@Override
			protected Object doInBackground() throws Exception
			{
				fileChooser.addPropertyChangeListener(new PropertyChangeListener()
				{
					@Override
					public void propertyChange(PropertyChangeEvent evt)
					{
						String prop = evt.getPropertyName();
						if (prop.equals(JFileChooser.FILE_FILTER_CHANGED_PROPERTY))
						{
							if (evt.getNewValue() == filter)
							{
								((JFileChooser) evt.getSource()).setFileView(fView);
								((JFileChooser) evt.getSource()).rescanCurrentDirectory();
							} else if (evt.getNewValue() == filter2)
							{
								((JFileChooser) evt.getSource()).setFileView(fView2);
								((JFileChooser) evt.getSource()).rescanCurrentDirectory();
							}
						}
					}
				});
				fileChooser.removeChoosableFileFilter(fileChooser.getFileFilter());
				fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				fileChooser.addChoosableFileFilter(this.filter);
				fileChooser.addChoosableFileFilter(this.filter2);
				fileChooser.setFileFilter(this.filter);
				fileChooser.setFileView(this.fView);
				fileChooser.setMultiSelectionEnabled(true);

				int choise = fileChooser.showOpenDialog(AddFilesToFileSet.this.view);
				if (choise == JFileChooser.APPROVE_OPTION)
				{
					File file = fileChooser.getSelectedFile();
					if (file != null)
						file = file.getParentFile();
					if (file != null && file.exists() && file.isDirectory()) {
						AddFilesToFileSet.this.view.getViewProperties().setLastImportedFilePath(file.getAbsolutePath());
					}

					ModelCommand command = new ImportFilesCommand(AddFilesToFileSet.this.model.getActiveGisFileSet(), fileChooser.getSelectedFiles(), true);
					DialogSingleProgress singleProgress = new DialogSingleProgress(AddFilesToFileSet.this.view);
					command.setWorkerStatus(singleProgress);
					try
					{
						command.execute();
					} catch (LowPrioException e)
					{
						JOptionPane.showMessageDialog(AddFilesToFileSet.this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						singleProgress.setVisible(false);
					}
					return this.activeNode;
				} else
					return null;
			}

			@Override
			protected void done()
			{
				try
				{
					if (this.get() != null)
					{
						DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) this.get();

						TreePath paths[] = new TreePath[1];
						paths[0] = new TreePath(activeNode.getPath());
						AddFilesToFileSet.this.view.refreshTreeNode(activeNode, true);
						AddFilesToFileSet.this.view.getViewProject().getFileSetView().expandPaths(paths);
						AddFilesToFileSet.this.view.getViewProject().getFileSetView().refreshSelection(paths);
					}

				} catch (IOException | ExecutionException | InterruptedException e)
				{
					log.error("Error refreshing Tree", e.getCause());
				}
			}
		}.execute();
	}
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.controller;


import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.extensions.ViewerFactory;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.view.View;
import org.insensa.view.extensions.IConnectionView;
import org.insensa.view.extensions.IGisFileView;
import org.insensa.view.extensions.IInfoConnectionView;
import org.insensa.view.extensions.IInfoReaderView;
import org.insensa.view.extensions.IViewer;
import org.insensa.view.extensions.ViewerFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.TreePath;


public class DropObjectOnImageListener extends DropTarget implements DropTargetListener {
  private static final Logger log = LoggerFactory.getLogger(DropObjectOnImageListener.class);

  private static final long serialVersionUID = 8752797768677383838L;
  private View view;
  private ExtensionManager exManager;
  private Model model;

  public DropObjectOnImageListener(View view, Model model) {
    this.view = view;
    this.model = model;
    this.exManager = ExtensionManager.getInstance();
  }

  @Override
  public synchronized void drop(DropTargetDropEvent dtde) {

    JTree fileSetTree = this.view.getViewProject().getFileSetView().getFileSetTree();
    TreePath[] treePaths = fileSetTree.getSelectionPaths();
    ControllerFileEditing edit = new ControllerFileEditing(treePaths, this.view, this.model);
    Point startLocation = dtde.getLocation();
    try {

      if (edit.getNumOfDifferentObjects() > 1 || edit.getNumOfDifferentObjects() <= 0) {
        return;
      }
      if (edit.getReadInfoReaderCount() > 0) {
        if (dtde.getDropAction() == DnDConstants.ACTION_MOVE) {
          List<Service> listOfServices = new ArrayList<Service>();
          listOfServices.addAll(this.getInfoReaderServiceList(edit.getReadInfoReaderList()));

          Service selectedService = this.showSelectedService(listOfServices, this.view,
              ((DropTarget) dtde.getSource()).getComponent());
          this.executeInfoReaderViewer(edit.getReadInfoReaderList(), selectedService,
              dtde.getLocation());
        }
        // ConnectoinFileChanger oder InfoConnection
      } else if (edit.getConnectionList().size() > 0) {
        List<ConnectionFileChanger> connectionFileChangers = new ArrayList<>();

        for (ConnectionFileChanger iterConnection : edit.getConnectionList()) {
          if (iterConnection.isUsed()) {
            connectionFileChangers.add(iterConnection);
          }
        }
        if (connectionFileChangers.isEmpty()) {
          return;
        }
        ;
        List<Service> listOfConnectionServices = new ArrayList<>();
        listOfConnectionServices.addAll(this.getConnectionServiceList(connectionFileChangers));

        Service selectedService = this.showSelectedService(listOfConnectionServices, this.view,
            ((DropTarget) dtde.getSource()).getComponent());
        if (selectedService == null) {
          return;
        }
        this.executeConnectionViewer(connectionFileChangers, selectedService, startLocation);
      } else if (edit.getInfoConnectionList().size() > 0) {
        List<InfoConnection> infoConnections = new ArrayList<>();
        for (InfoConnection infoConnection : edit.getInfoConnectionList()) {
          if (infoConnection.isUsed()) {
            infoConnections.add(infoConnection);
          }
        }
        if (infoConnections.isEmpty()) {
          return;
        }
        List<Service> listOfInfoConnectionServices = new ArrayList<>();
        listOfInfoConnectionServices.addAll(this.getInfoConnectionServiceList(infoConnections));

        Service selectedService = this.showSelectedService(listOfInfoConnectionServices,
            this.view,
            ((DropTarget) dtde.getSource()).getComponent());
        if (selectedService == null) {
          return;
        }
        this.executeInfoConnectionViewer(infoConnections, selectedService, startLocation);

      } else if (edit.getFileList().size() > 0) {
        List<Service> listOfServices = new ArrayList<Service>();
        ServiceList list = this.exManager.getFileServices();
        Service selectedService = null;

        if (list == null) {
          return;// TODO FERHLERMELDUNG;
        } else {
          listOfServices.addAll(list.getServices(
              ServiceType.VIEWER, edit.getFileList().get(0).getGisFile().getType()));
        }
        selectedService = this.showSelectedService(listOfServices, this.view,
            ((DropTarget) dtde.getSource()).getComponent());
        if (selectedService != null) {
          this.executeRasterFileViewer(edit.getFileList(), selectedService, startLocation);
        }
      }
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | IOException exception) {
      log.error("UNKNOWN", exception);
    }
  }

  private void executeConnectionViewer(List<ConnectionFileChanger> connectionList,
                                       Service selectedService, Point startLocation)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
    JTree fileSetTree = this.view.getViewProject().getFileSetView().getFileSetTree();
    Point point = startLocation;
    for (int i = 0; i < connectionList.size(); i++) {
      ConnectionFileChanger con = connectionList.get(i);

      IConnectionView viewer = new ViewerFactory(this.exManager)
          .getConnectionView(con.getId(), selectedService.getName());
      if (viewer != null) {
        ((IViewer) viewer).setTitle(con.getOutputFileName());
        ((IViewer) viewer).setView(this.view);
        viewer.addConnection(con);
        ((IViewer) viewer).init(new Dimension(300, 300));
        ViewerFrame viewerFrame = new ViewerFrame(con.getOutputFileContainer().getName(), (IViewer) viewer);

        viewerFrame.setSize(200, 400);
        viewerFrame.setFileSetTree(fileSetTree);

        Dimension dim = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        ((IViewer) viewer).startView(dim, new Dimension(300, 300));

        viewerFrame.setVisible(true);
        viewerFrame.setLocation(point);
        this.view.getViewProject().getImageView().add(viewerFrame);
        point.x += 15;
        point.y += 15;
      }
    }
  }

  private void executeInfoConnectionViewer(List<InfoConnection> infoConnections,
                                           Service selectedService, Point startLocation)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
    JTree fileSetTree = this.view.getViewProject().getFileSetView().getFileSetTree();
    Point point = startLocation;
    for (int i = 0; i < infoConnections.size(); i++) {
      InfoConnection con = infoConnections.get(i);

      IInfoConnectionView viewer = new ViewerFactory(this.exManager)
          .getInfoConnectionView(con.getId(), selectedService.getName());
      if (viewer != null) {
//        ((IViewer) viewer).setTitle(con.getOutputFileName());
        ((IViewer) viewer).setTitle(con.getId());
        ((IViewer) viewer).setView(this.view);
        viewer.addInfoConnection(con);
        ((IViewer) viewer).init(new Dimension(300, 300));
        ViewerFrame viewerFrame = new ViewerFrame(con.getId(), (IViewer) viewer);

        viewerFrame.setSize(200, 400);
        viewerFrame.setFileSetTree(fileSetTree);

        Dimension dim = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        ((IViewer) viewer).startView(dim, new Dimension(300, 300));

        viewerFrame.setVisible(true);
        viewerFrame.setLocation(point);
        this.view.getViewProject().getImageView().add(viewerFrame);
        point.x += 15;
        point.y += 15;
      }
    }
  }

  public void executeInfoReaderViewer(List<InfoReader> infoReaderList,
                                      Service selectedService,
                                      Point startLocation) throws IOException,
      ClassNotFoundException, InstantiationException, IllegalAccessException {
    Point tmpLocation = startLocation;
    JTree fileSetTree = this.view.getViewProject().getFileSetView().getFileSetTree();
    if (selectedService == null) {
      return;
    }

    for (InfoReader infoReader : infoReaderList) {

      IInfoReaderView viewer = new ViewerFactory(this.exManager)
          .getInfoReaderViewer(infoReader.getId(), selectedService.getName());
      if (viewer != null) {
        viewer.addInfoReader(infoReader);
        ((IViewer) viewer).setTitle(infoReader.getTargetFile().getName());
        ((IViewer) viewer).setView(this.view);
        ((IViewer) viewer).init(new Dimension(300, 300));
        ViewerFrame imageFrame = new ViewerFrame(infoReader.getTargetFile().getName(),
            ((IViewer) viewer));

        imageFrame.setSize(300, 300);
        imageFrame.setFileSetTree(fileSetTree);

        Dimension dim = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        ((IViewer) viewer).startView(dim, new Dimension(300, 300));

        this.view.getViewProject().getImageView().add(imageFrame);
        imageFrame.setLocation(tmpLocation);
        imageFrame.setVisible(true);
        tmpLocation.x += 15;
        tmpLocation.y += 15;
      }
    }
  }

  private void executeRasterFileViewer(List<IGisFileContainer> fileList,
                                       Service selectedService,
                                       Point startLocation) throws ClassNotFoundException,
      InstantiationException, IllegalAccessException, IOException {
    Point point = startLocation;
    JTree fileSetTree = this.view.getViewProject().getFileSetView().getFileSetTree();
    for (IGisFileContainer fileInformation : fileList) {
      IGisFileView viewer = new ViewerFactory(this.exManager)
          .getRasterFileView(selectedService.getName());
      if (viewer != null) {
        ((IViewer) viewer).setTitle(fileInformation.getName());
        // imageView.setTitle("my Title");
        ((IViewer) viewer).setView(this.view);
        viewer.addGisFileContainer(fileInformation);
        ((IViewer) viewer).init(new Dimension(300, 300));
        ViewerFrame imageFrame = new ViewerFrame(fileInformation.getName(), (IViewer) viewer);

        imageFrame.setSize(300, 300);
        imageFrame.setFileSetTree(fileSetTree);

        Dimension dim = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        ((IViewer) viewer).startView(dim, new Dimension(300, 300));

        this.view.getViewProject().getImageView().add(imageFrame);
        imageFrame.setLocation(point);
        imageFrame.setVisible(true);
        point.x += 15;
        point.y += 15;
      }
    }
  }

  private List<Service> getConnectionServiceList(List<ConnectionFileChanger> connectionList) {
    List<Service> serviceList = new ArrayList<Service>();
    List<ServiceList> serviceListList = new ArrayList<ServiceList>();
    for (ConnectionFileChanger iterConnection : connectionList) {
      serviceListList.add(this.exManager.getConnectionServices()
          .get(iterConnection.getId()));
    }
    serviceList.addAll(this.getServiceList(serviceListList));
    ServiceList globalList = this.exManager.getConnectionServices().get("global");
    if (globalList != null) {
      serviceList.addAll(globalList.getServices(ServiceType.VIEWER));
    }
    return serviceList;
  }

  private List<Service> getInfoConnectionServiceList(List<InfoConnection> infoConnections) {
    List<Service> serviceList = new ArrayList<Service>();
    List<ServiceList> serviceListList = new ArrayList<ServiceList>();
    for (InfoConnection infoConnection : infoConnections) {
      serviceListList.add(this.exManager.getInfoConnectionServices()
          .get(infoConnection.getId()));
    }
    serviceList.addAll(this.getServiceList(serviceListList));
    ServiceList globalList = this.exManager.getInfoConnectionServices().get("global");
    if (globalList != null) {
      serviceList.addAll(globalList.getServices(ServiceType.VIEWER));
    }
    return serviceList;
  }

  public List<Service> getFileServiceList(List<RasterFileContainer> fileList) {
    List<Service> serviceList = new ArrayList<Service>();
    return serviceList;
  }

  private List<Service> getInfoReaderServiceList(List<InfoReader> infoReaderList) {
    List<Service> serviceList = new ArrayList<Service>();
    List<ServiceList> serviceListList = new ArrayList<ServiceList>();
    for (InfoReader infoReader : infoReaderList) {
      serviceListList.add(this.exManager.getInfoReaderServices().get(infoReader.getId()));
    }
    serviceList.addAll(this.getServiceList(serviceListList));

    ServiceList globalList = this.exManager.getInfoReaderServices().get("global");
    if (globalList != null) {
      serviceList.addAll(globalList.getServices(ServiceType.VIEWER));
    }
    return serviceList;
  }

  private List<Service> getServiceList(List<ServiceList> serviceListList) {
    List<Service> serviceList = new ArrayList<>();
    List<Service> tmpServiceList = new ArrayList<>();
    for (int i = 0; i < serviceListList.size(); i++) {
      // ConnectionFileChanger iCon = connectionList.get(i);
      ServiceList list = serviceListList.get(i);

      if (list == null) {
        serviceList.clear();
        break;
      } else {
        List<Service> tmpServiceListBoth = list.getServices(ServiceType.VIEWER);
        if (i == 0) {
          tmpServiceList.addAll(tmpServiceListBoth);
          serviceList.addAll(tmpServiceList);
          continue;
        }

        for (int j = 0; j < tmpServiceList.size(); j++) {
          if (!tmpServiceListBoth.contains(tmpServiceList.get(j))) {
            serviceList.remove(tmpServiceList.get(j));
          }
        }
      }
    }
    return serviceList;
  }

  private Service showSelectedService(List<Service> listOfServices, View view,
                                      Component parentComponent) {
    if (listOfServices.size() > 1) {
      ChooseViewerDialog chDialog = new ChooseViewerDialog(listOfServices, view);
      chDialog.setModalityType(ModalityType.APPLICATION_MODAL);
      chDialog.setLocationRelativeTo(parentComponent);
      chDialog.setLocation(MouseInfo.getPointerInfo().getLocation());
      chDialog.setVisible(true);
      return chDialog.getSelectedService();
    } else if (listOfServices.size() == 1) {
      return listOfServices.get(0);
    } else {
      return null;
    }
  }

}

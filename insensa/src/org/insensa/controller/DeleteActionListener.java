/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.TreePath;


import org.insensa.Model;
import org.insensa.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteActionListener implements ActionListener
{
	private static final Logger log = LoggerFactory.getLogger(DeleteActionListener.class);

	private View view;
	private Model model;
	

	public DeleteActionListener(View view, Model model)
	{
		this.view=view;
		this.model=model;
	}
	
	/**
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent event)
	{
		JTree tree = view.getViewProject().getFileSetView().getFileSetTree();
		TreePath[] selPaths = tree.getSelectionPaths();
		if (selPaths == null)
			return;
		ControllerFileEditing fileEdit = new ControllerFileEditing(selPaths, this.view, this.model);
		if (selPaths.length > 1)
		{
			try
			{
				fileEdit.removeSelectedItems();
				this.view.setActiveTreePath(null);
			} catch (IOException e)
			{
				JOptionPane.showMessageDialog(this.view, e.getMessage());
				log.error( "UNKNOWN", e);
			}
		}
		else if(selPaths.length==1)
		{
			try
			{
				fileEdit.removeSelectedItem();
				this.view.setActiveTreePath(null);
			} catch (IOException e)
			{
				JOptionPane.showMessageDialog(this.view, e.getMessage());
				log.error("UNKNOWN",e);
			}
			
		}

	}

}

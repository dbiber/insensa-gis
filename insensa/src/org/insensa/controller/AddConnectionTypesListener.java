package org.insensa.controller;

import org.insensa.GenericGisFileSet;
import org.insensa.GisFileType;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Model;
import org.insensa.connections.StackConnection;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.ServiceType;
import org.insensa.view.View;
import org.insensa.view.ViewFileSet;
import org.insensa.view.dialogs.DialogAddConnectionTypes;
import org.insensa.view.dialogs.connections.DialogChoosePlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class AddConnectionTypesListener implements ActionListener, ITreeSelectionListener {

  private static final Logger log = LoggerFactory.getLogger(AddConnectionTypesListener.class);
  DialogAddConnectionTypes chooseOutput;
  private View view;
  private Model model;
  private DefaultMutableTreeNode selectedTreeNode;
  private Object selectedObject;
  private DialogAddConnectionTypes.StackType stackType;


  public AddConnectionTypesListener(View view, Model model, DialogAddConnectionTypes.StackType stackType) {
    this.view = view;
    this.model = model;
    this.stackType = stackType;
  }

  private String showPluginChooser(List<IGisFileContainer> fileContainerList) {
    String sStackType = "Connection";
    List<String> choiceList;
    GisFileType targetType = fileContainerList.get(0).getGisFile().getType();
    if (stackType == DialogAddConnectionTypes.StackType.INFO_CONNECTION) {
      sStackType = "InfoConnection";
      choiceList = ExtensionManager.getInstance().getUsableInfoConnectionList(ServiceType.EXEC,
          targetType);
    } else {
      choiceList = ExtensionManager.getInstance().getUsableConnectionList(ServiceType.EXEC,
          targetType);
    }

    DialogChoosePlugin addConnectionDia = new DialogChoosePlugin(view,
        choiceList,
        sStackType);
    addConnectionDia.setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
    addConnectionDia.setVisible(true);
    return addConnectionDia.getSelectedConnection();
  }

  private void showOutputChooser(String connectionName,
                                 List<IGisFileContainer> fileContainerList,
                                 JTree tree) {
    chooseOutput = new DialogAddConnectionTypes(view,
        false, "Add Connection", model,
        view, connectionName,
        fileContainerList,
        stackType);
    tree.setSelectionPath(new TreePath(view.getActiveTreePath()[0]));
    chooseOutput.setVisible(true);
  }

  private void addPluginToModel(String connectionName,
                                List<IGisFileContainer> fileContainerList) {
    IGisFileContainer fileContainer = fileContainerList.get(0);
    try {
      if (stackType == DialogAddConnectionTypes.StackType.INFO_CONNECTION) {
        model.createAndAddInfoConnection(fileContainer, connectionName);
      } else {
        model.createAndAddConnection(fileContainer, connectionName);
      }
      DefaultMutableTreeNode activeTreeNode = (DefaultMutableTreeNode) view.getActiveTreeNode();
      if (activeTreeNode != null) {
        TreePath oldPath = new TreePath(activeTreeNode.getPath());
        ViewFileSet fileSet = view.getViewProject().getFileSetView();
        view.refreshTreeNode(activeTreeNode, true);
        fileSet.expandPathChild(oldPath, fileSet);
      }
    } catch (IOException e1) {
      log.error("Error adding InfoConnection", e1);
    }
  }

  private void resolveFileSets(List<IGisFileContainer> fileContainers,
                               List<GenericGisFileSet> fileSets) {
    for (GenericGisFileSet fileSet : fileSets) {
      for (IGisFileContainer iContainer : fileSet.getFileList()) {
        if (!fileContainers.contains(iContainer)) {
          fileContainers.add(iContainer);
        }
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JTree tree = view.getViewProject().getFileSetView().getFileSetTree();
    ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(),
        view, model);
    List<IGisFileContainer> fileContainerList = fileEdit.getFileList();
    resolveFileSets(fileContainerList, fileEdit.getFileSetList());

    if (fileContainerList.size() <= 1 && !fileContainerList.get(0).isStack()) {
      JOptionPane.showMessageDialog(view,
          "You have to select at least two files or a Connection");
      return;
    }

    if (stackType == DialogAddConnectionTypes.StackType.STACK) {
      showOutputChooser(StackConnection.NAME, fileContainerList, tree);
    } else {
      String connectionName = showPluginChooser(fileContainerList);
      if (connectionName != null) {
        if (fileEdit.getFileList().size() > 1) {
          showOutputChooser(connectionName, fileContainerList, tree);
        } else if (fileEdit.getFileList().size() == 1) {
          addPluginToModel(connectionName, fileContainerList);
        }
      }
    }
  }

  @Override
  public void treeSelected(DefaultMutableTreeNode selectedTreeNode,
                           Object selectedObject) {
    this.selectedObject = selectedObject;
    this.selectedTreeNode = selectedTreeNode;
    if (chooseOutput == null) {
      return;
    }
    if (selectedObject instanceof IGisFileSet) {
      chooseOutput.setFileSet((GenericGisFileSet) selectedObject);
    }
    chooseOutput.setRefreshingTreeNode(selectedTreeNode);
  }
}

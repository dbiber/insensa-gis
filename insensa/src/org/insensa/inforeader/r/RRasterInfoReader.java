/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.inforeader.r;


import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceDependency;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.helpers.AttributeTable;
import org.insensa.inforeader.AbstractInfoReader;
import org.insensa.inforeader.InfoReader;
import org.insensa.model.generic.DataVariableConverter;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableImpl;
import org.insensa.model.storage.AttributeTableSaver;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RRasterInfoReader
    extends AbstractInfoReader
    implements IScriptPluginExec {
  private static final Logger log = LoggerFactory.getLogger(RRasterInfoReader.class);

  private String scriptPath;
  private Map<String, IVariable> variables = new LinkedHashMap<>();

  public RRasterInfoReader() {
  }

  @Override
  public AttributeTable getData() {
    AttributeTableSaver saver = new AttributeTableSaver();
    this.save(saver);
    return saver.getTable();
  }

  public void setScriptPath(String filePath) {
    this.scriptPath = filePath;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> dependencies = new ArrayList<>();
    if (getId() == null) {
      log.error("getDependencies", "InfoId not set");
      return dependencies;
    }
    ServiceList serviceList = ExtensionManager
        .getInstance()
        .getInfoReaderServices()
        .get(getId());
    if (serviceList == null) {
      log.error("getDependencies", "No Services found for orderId \""
          + getId()
          + "\"");
      return dependencies;
    }
    Service service = serviceList.getService(ServiceType.EXEC);
    if (service == null) {
      log.error("getDependencies", "No exec service found for orderId \""
          + getId()
          + "\"");
      return dependencies;
    }
    List<ServiceDependency> serviceDependencies = service.getDependencies();
    for (ServiceDependency serviceDependency : serviceDependencies) {
      dependencies.add(serviceDependency.getId());
    }
    return dependencies;
  }

  @Override
  public void readInfos(IGisFileContainer file) {
    RSession rSession = null;
    try {
      rSession = new RSession("127.0.0.1");
      RMessageStream stream = rSession.loadOutput(RMessageType.ALL);
      RMessageStreamMonitor.getInstance().registerMessageStream(stream);

      RasterFileContainer rasterFileContainer = (RasterFileContainer) getTargetFile();
      rSession.assign("rasterFile", rasterFileContainer.getAbsolutePath());

//      rSession.sourceFile(new File(scriptPath));
      rSession.assignInsensaIsActive();
      rSession.assignInsensaWs(file);
      rSession.assignInsensaCurrentFile(rasterFileContainer.getAbsolutePath());

      for (IVariable var : variables.values()) {
        rSession.assign(var);
      }

      DataVariableConverter converter = new DataVariableConverter();
      List<InfoReader> mInfoReaders = file.getInfoReaders();
      for (InfoReader infoReader : mInfoReaders) {
        infoReader.save(converter);
      }
      Map<String, IVariable> tmpVariableMap = converter.getVariableMap();
      for (Map.Entry<String, IVariable> entry : tmpVariableMap.entrySet()) {
        rSession.assign(entry.getValue());
      }

      rSession.sourceFile(new File(scriptPath));

      String str = rSession
          .eval("toString.default(insensa:::getDataXmlRoot())")
          .asString();
      JdomRestorer restorer = new JdomRestorer(str);

      List<IRestorableData> dataCollection = restorer
          .loadCollection("variable",
              VariableImpl.class);

      for (IRestorableData data : dataCollection) {
        variables.put(((IVariable) data).getName(), (IVariable) data);
      }

      rSession.unloadOuput();
      this.used = true;
    } catch (REngineException | REXPMismatchException e) {
      log.error("Error executing R InfoReader", e);
    } finally {
      if (rSession != null) {
        try {
          rSession.closeAndClear();
        } catch (REXPMismatchException | REngineException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    used = restorer.loadBoolean("used").orElse(false);

    List<IRestorableData> data = restorer.loadCollection("variable",
        VariableImpl.class);
    data.forEach(iRestorableData -> variables.put(((IVariable) iRestorableData)
            .getName(),
        (IVariable) iRestorableData));
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("used", used);
    variables.forEach((s, variable) -> saver.save(variable));
  }

  @Override
  public Map<String, IVariable> getVariables() {
    return variables;
  }

  @Override
  public void setVariables(Map<String, IVariable> variableMap) {
    this.variables = variableMap;
  }
}

/*
 * Copyright (C) 2017 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.inforeader.r;

import java.util.Arrays;
import java.util.Map;

public class ScriptDataWrapper {
  
  public static String toString(Object object) {
    if (object instanceof String) {
      return object.toString();
    } else if (object instanceof double[]) {
      return Arrays.toString((double[])object);
    } else if (object instanceof String[]) {
      return Arrays.toString((String[]) object);
    } else if (object instanceof int[]) {
      return Arrays.toString((int[]) object);
    } else if (object instanceof Integer[]) {
      return Arrays.toString((Integer[]) object);
    } else if (object instanceof Map) {
      StringBuilder stringBuffer = new StringBuilder();
      for(Object keyObj : ((Map) object).keySet()) {
        Object valObj = ((Map) object).get(keyObj);
        stringBuffer.append(toString(valObj)).append(":").append(toString(valObj));
      }
      return stringBuffer.toString();
    } else {
      return "UNKNOWN";
    }
  }
}

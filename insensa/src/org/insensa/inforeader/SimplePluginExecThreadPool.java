/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.exceptions.ThreadPoolException;
import org.insensa.extensions.IPluginExec;
import org.insensa.helpers.ExecDependencyController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class SimplePluginExecThreadPool {
  private ExecutorService exeService;
  private List<Runnable> threadList = new ArrayList<Runnable>();
  private ExecDependencyController depChecker;

  public SimplePluginExecThreadPool(ExecDependencyController depChecker) {
    this.depChecker = depChecker;
    // int prozCount = Runtime.getRuntime().availableProcessors();
    int prozCount = 1;
    this.exeService = Executors.newFixedThreadPool(prozCount);
  }

  public void addInfoThread(Runnable threadObject) {
    this.threadList.add(threadObject);
    this.depChecker.setDependency((InfoReader) threadObject);
  }

  public void executeThread(IGisFileContainer rasterFile,
                            IPluginExec iPluginExec,
                            WorkerStatusList workerStatusList) throws IOException {
    List<Runnable> runList = this.getRunnableDepList(rasterFile, iPluginExec);
    if (workerStatusList != null) {
      workerStatusList.startAllProcesses(rasterFile.getName(), runList.size());
    }

    for (int i = 0; i < runList.size(); i++) {
      if (workerStatusList != null) {
        ((InfoReader) runList.get(i)).setWorkerStatus(workerStatusList.getWorkerStatus(i));
      }
      this.exeService.execute(runList.get(i));
    }
  }

  public void executeThreads(IGisFileContainer rasterFile, List<InfoReader> iReader,
                             WorkerStatusList workerStatusList) throws IOException {
    List<Runnable> runList = new ArrayList<>();
    for (InfoReader anIReader: iReader) {
      List<Runnable> tmpRunList = this.getRunnableDepList(rasterFile, anIReader);
      if (tmpRunList != null && !tmpRunList.isEmpty()) {
        for (Runnable tmpRunnable: tmpRunList) {
          if (!runList.contains(tmpRunnable))
            runList.add(tmpRunnable);
        }
      }

    }
    if (workerStatusList != null) {
      workerStatusList.startAllProcesses(rasterFile.getName(), runList.size());
    }

    for (int i = 0; i < runList.size(); i++) {
      if (workerStatusList != null) {
        ((InfoReader) runList.get(i)).setWorkerStatus(workerStatusList.getWorkerStatus(i));
      }
      this.exeService.execute(runList.get(i));
    }
  }

  public void executeThreads(IGisFileContainer rasterFile,
                             WorkerStatusList workerStatusList,
                             boolean waitForCompletion)
      throws IOException {
    executeThreads(rasterFile, workerStatusList);
    if (waitForCompletion) {
      this.exeService.shutdown();
      try {
        this.exeService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        throw new IOException(e.getCause());
      }
    }
  }

  public void executeThreads(IGisFileContainer rasterFile, WorkerStatusList workerStatusList)
      throws IOException {
    List<Runnable> runList = new ArrayList<>();
    for (Runnable aThreadList: this.threadList) {
      if (!((InfoReader) aThreadList).isUsed()) {
        runList.add(aThreadList);
      }
    }
    if (workerStatusList != null) {
      workerStatusList.startAllProcesses(rasterFile.getName(), runList.size());
    }

    for (int i = 0; i < runList.size(); i++) {
      if (workerStatusList != null) {
        ((InfoReader) runList.get(i)).setWorkerStatus(workerStatusList.getWorkerStatus(i));
      }
      this.exeService.execute(runList.get(i));
    }
  }

  private List<Runnable> getRunnableDepList(IGisFileContainer rasterFile, IPluginExec pluginExec) {
    List<Runnable> runList = new ArrayList<>();
    if (pluginExec.isUsed()) {
      return runList;
    }
    List<String> depList = pluginExec.getInfoDependencies();
    for (String iDep: depList) {
      InfoReader tmpReader = rasterFile.getInfoReader(iDep);

      List<Runnable> tmpRunList = this.getRunnableDepList(rasterFile, tmpReader);
      for (Runnable iRun: tmpRunList) {
        if (!runList.contains(iRun)) {
          runList.add(iRun);
        }
      }
    }
    runList.add(pluginExec);
    return runList;
  }

  public void removeInfoReader(IPluginExec pluginExec) {
    if (!this.threadList.remove(pluginExec)) {
      throw new ThreadPoolException("SimplePluginExecThreadPool: removeInfoReader: InfoReader "
          + pluginExec.getOrderId() + "not found");
    }
    this.depChecker.unsetDependency(pluginExec);
  }

  public ExecDependencyController getDependencyController() {
    return depChecker;
  }
}
/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.gdal.gdal.Band;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.VariableValue;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class GetPointInformation extends AbstractInfoReader {

  private static final long serialVersionUID = -4453270097525356515L;
  private final static String NAME = "getPointInformation";
  private String precisionString = "0";
  private double searchValue = Double.MAX_VALUE;
  private int xPos = -1;
  private int yPos = -1;

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(PrecisionValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (!this.isUsed()) {
      return;
    }
    saver.save("searchValue", this.searchValue);
    saver.save("xPos", this.xPos);
    saver.save("yPos", this.yPos);
    saver.save("precision", this.precisionString);
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    table.put("SearchValue", precisionFormat.format(searchValue));
    table.put("xPos", Integer.toString(xPos));
    table.put("yPos", Integer.toString(yPos));

    return table;
  }

//  @Override
//  public Vector<Vector<String>> getInfos() {
//    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
//    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
//    dSymb.setDecimalSeparator('.');
//    precisionFormat.setDecimalFormatSymbols(dSymb);
//    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);
//
//    Vector<Vector<String>> dataVec = new Vector<Vector<String>>();
//    Vector<String> rowVec;
//
//    if (this.read == true) {
//      rowVec = new Vector<String>();
//      rowVec.add("SearchValue");
//      rowVec.add(precisionFormat.format(this.searchValue));
//      dataVec.add(rowVec);
//
//      rowVec = new Vector<String>();
//      rowVec.add("xPos");
//      rowVec.add(Integer.toString(this.xPos));
//      dataVec.add(rowVec);
//
//      rowVec = new Vector<String>();
//      rowVec.add("yPos");
//      rowVec.add(Integer.toString(this.yPos));
//      dataVec.add(rowVec);
//    }
//    return dataVec;
//  }


  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.precisionString = restorer.loadString("precision").get();
    this.xPos = restorer.loadInteger("xPos").get();
    this.yPos = restorer.loadInteger("yPos").get();
    this.searchValue = restorer.loadDouble("searchValue").get();
  }

//  @Override
//  public void setInfos(Element eInfo) throws IOException {
//    if (eInfo.getAttributeValue("read").equals("false")) {
//      this.read = false;
//    } else {
//      this.read = true;
//      try {
//        this.searchValue = eInfo.getAttribute("searchValue").getDoubleValue();
//        this.precisionString = eInfo.getAttributeValue("precision");
//        this.xPos = eInfo.getAttribute("xPos").getIntValue();
//        this.yPos = eInfo.getAttribute("yPos").getIntValue();
//      } catch (DataConversionException e) {
//        throw new IOException(e);
//      }
//    }
//  }

  @Override
  public List<VariableValue> getVariableList() {
    List<VariableValue> variableList = new ArrayList<VariableValue>();
    VariableValue<String> variable;

    variable = new VariableValue<>(Double.toString(this.searchValue));
    variable.setShortDescription("Search Value");
    variable.setiD(0);
    variableList.add(variable);

    variable = new VariableValue<>(Integer.toString(this.xPos));
    variable.setShortDescription("xPos");
    variable.setiD(1);
    variableList.add(variable);

    variable = new VariableValue<>(Integer.toString(this.yPos));
    variable.setShortDescription("yPos");
    variable.setiD(2);
    variableList.add(variable);

    return variableList;
  }

  @Override
  public void setVariableList(List<VariableValue> varList) {
    VariableValue<String> iVal;

    iVal = varList.get(0);
    this.searchValue = Double.valueOf(iVal.getVariable());

    iVal = varList.get(1);
    this.xPos = Integer.valueOf(iVal.getVariable());

    iVal = varList.get(2);
    this.yPos = Integer.valueOf(iVal.getVariable());
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    float factor = (float) (100.0 / ((RasterFile) file.getGisFile()).getNRows());
    float readValue;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);
    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("CFastCountValuesNew:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("CFastCountValuesNew:readInfos: precisionValues not read");

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();
    int precition = ((PrecisionValues) precisionValues).getMaxPrecision();
    Math.pow(10, precition);
    // Dataset ds = file.getDataset();// gdal.Open(file.getAbsolutePath());
    Band band = ((RasterFile) file.getGisFile()).getBand(RasterFileAccess.READ_ONLY);// ds.GetRasterBand(1);

    if (this.xPos > -1 && this.yPos > -1) {
      float[] dData1 = new float[1];
      band.ReadRaster(this.xPos, this.yPos, 1, 1, dData1);
      this.searchValue = dData1[0];
      this.used = true;
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
      return;
    }
    if (!(this.searchValue < Double.MAX_VALUE))
      throw new IOException("Error : you have to set the Attributes");

    int intSearchVal = Math.round(new Double(this.searchValue).floatValue());
    int intReadVal;

    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        if (readValue == ((RasterFile) file.getGisFile()).getNoDataValue()) {

        } else {
          intReadVal = Math.round(new Double(readValue).floatValue());
          if (intReadVal == intSearchVal) {
            this.xPos = i;
            this.yPos = j;
            this.used = true;
            this.processStatus += factor;
            if (this.workerStatus != null)
              this.workerStatus.refreshPercentage(this.processStatus);
            return;
          }
        }
      }
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    ((RasterFile) file.getGisFile()).unlock();
    this.used = true;
  }

}

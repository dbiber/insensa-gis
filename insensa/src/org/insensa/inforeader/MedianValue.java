/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.MathUils;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MedianValue extends AbstractInfoReader {

  public static final String NAME = "MedianValue";
  private static final long serialVersionUID = -6128541089720202723L;
  private long numOfValues = 0;
  private double medianValue = 0;

  private long index1 = -1;
  private long index2 = -1;

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(CountValues.NAME);
    return depList;
  }

  public long getIndex1() {
    return this.index1;
  }

  public long getIndex2() {
    return this.index2;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (!isUsed()) {
      return;
    }
    saver.save("index1", this.index1);
    saver.save("index2", this.index2);
    saver.save("numOfValues", this.numOfValues);
    saver.save("median", saver1 -> saver1.save("value", medianValue));
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (isUsed()) {
      this.index1 = restorer.loadLong("index1").get();
      this.index2 = restorer.loadLong("index2").get();
      this.numOfValues = restorer.loadLong("numOfValues").get();
      restorer.loadRestorable("median", restorer1 -> {
        medianValue = restorer1.loadDouble("value").get();
      });
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    if (isUsed()) {
      table.put("Median", Double.toString(medianValue));
    }
    return table;
  }

  public double getMedianValue() {
    return this.medianValue;
  }

  public long getNumOfValues() {
    return this.numOfValues;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    Float val1;
    Float val2;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader countReader = file.getInfoReader(CountValues.NAME);
    if (countReader == null)
      throw new IOException("CMedianValueNew:readInfos: no fastCountValues InfoReader Found");
    if (!countReader.isUsed())
      throw new IOException("CMedianValueNew:readInfos: fastCountValues not read");
    Map<Float, Long> countList = ((CountValues) countReader).getCountValues().getMap();
    // for(AmountCounter iCnt :countList)
    // numOfValues+=iCnt.getCnt();
    this.numOfValues = ((CountValues) countReader).getNumOfData();

    if ((this.numOfValues % 2) == 0) {
      this.index1 = (this.numOfValues / 2);
      this.index2 = (this.numOfValues / 2) + 1;
      val1 = MathUils.getNthData(countList, this.index1, this.processStatus, this.workerStatus);
      if (val1 == null)
        throw new IOException("Error getting index 1");
      val2 = MathUils.getNthData(countList, this.index2, this.processStatus, this.workerStatus);
      if (val2 == null)
        throw new IOException("Error getting index 2");

      this.medianValue = (val1 + val2) / 2;
    } else {
      this.index1 = ((this.numOfValues + 1) / 2);
      this.index2 = this.index1;
      val1 = MathUils.getNthData(countList, this.index1, this.processStatus, this.workerStatus);
      if (val1 == null)
        throw new IOException("Error getting index");
      this.medianValue = val1;

    }
    if (this.workerStatus != null)
      this.workerStatus.refreshPercentage(100);

    this.used = true;

  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class StandardDeviationValue extends AbstractInfoReader {

  public static final String NAME = "StandardDeviationValue";
  private static final long serialVersionUID = -6328849305543093683L;
  private double standardDeviation = 0;

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<>();
    depList.add(MeanValue.NAME);
    depList.add(CountValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("StandardDeviation", saver1 -> {
      saver1.save("value", Double.toString(this.standardDeviation));
    });

  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    table.put("Standard Deviation", Double.toString(this.standardDeviation));
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    restorer.loadRestorable("StandardDeviation", restorer1 -> {
      standardDeviation = restorer1.loadDouble("value").get();
    });
  }

  public double getStandardDeviationValue() {
    return this.standardDeviation;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    long numOfValues;
    double meanValue;

    float factor;
    Float readValue;
    Long cnt;
    double sqr;

    double tmpDeviationValue = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader meanReader = file.getInfoReader(MeanValue.NAME);
    if (meanReader == null)
      throw new IOException("StandartDeviationValues:ReadInfos: no meanValue InfoReader Found");
    if (!meanReader.isUsed())
      throw new IOException("StandartDeviationValues:ReadInfos: meanValue not read");

    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("StandartDeviationValues:ReadInfos: no countValues InfoReader Found");
    if (!countValues.isUsed())
      throw new IOException("StandartDeviationValues:ReadInfos: precisionValues not read");

    numOfValues = ((MeanValue) meanReader).getNumOfValues();
    meanValue = ((MeanValue) meanReader).getMeanValue();

    Map<Float, Long> countMap = ((CountValues) countValues)
        .getCountValues().getMap();
    factor = (float) (100.0 / countMap.size());
    Iterator<Float> iter = countMap.keySet().iterator();
    while (iter.hasNext()) {
      readValue = iter.next();
      cnt = countMap.get(readValue);
      sqr = (readValue - meanValue) * (readValue - meanValue);
      tmpDeviationValue += ((sqr * cnt) / (numOfValues));
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }

    this.used = true;
    this.standardDeviation = Math.sqrt(tmpDeviationValue);
  }
}

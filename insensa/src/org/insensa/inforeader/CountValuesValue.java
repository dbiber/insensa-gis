package org.insensa.inforeader;

import org.insensa.model.generic.VariableImpl;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.FloatLongValue;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class CountValuesValue extends VariableImpl {

  FloatLongValue mapValue;

  public CountValuesValue() {
    super();
    mapValue = new FloatLongValue();
  }

  @Override
  public Optional<IValue> getValue() {
    return Optional.ofNullable(mapValue);
  }

  @Override
  public String getName() {
    return "CountValues";
  }

  @Override
  public VariableType getType() {
    return VariableType.MAP_FLOAT_LONG;
  }

  public void put(Float key, Long value) {
    mapValue.put(key, value);
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save(getName(), mapValue);
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    Optional<IRestorableData> val = restorer
        .loadRestorable(getName(), FloatLongValue.class);
    mapValue = (FloatLongValue) val.orElse(new FloatLongValue());
  }

  public int size() {
    return mapValue.size();
  }

  public Collection<? extends Long> values() {
    return mapValue.values();
  }

  public Map<Float, Long> getMap() {
    return mapValue;
  }

  public Iterable<Float> keySet() {
    return mapValue.keySet();
  }
}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;


import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatus;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.helpers.VariableValue;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;


public abstract class AbstractInfoReader implements InfoReader, Serializable {
  private static final Logger log = LoggerFactory.getLogger(AbstractInfoReader.class);

  private static final long serialVersionUID = 7267808166881655243L;
  protected boolean used = false;
  protected WorkerStatus workerStatus = null;
  protected IGisFileContainer targetFile = null;
  protected float processStatus = 0;
  protected ExecDependencyController dependencer = null;
  private String errorMassage = "";
  private String infoId;
  private List<String> infoDependencies = new ArrayList<>();

  @Override
  public String getGroupName() {
    return "infos";
  }

  /**
   * Returns a list with names of InfoReaders that have to be executed before this InfoReader can be
   * executed. <br> <br> Implementations of class {@link AbstractInfoReader} should override this
   * method if dependencies are necessary. <b>Example:</b> <br> <code>List&lt;String&gt; depList =
   * super.getDependencies();</code> <br> <code>depList.add("countValues");</code><br>
   * <code>depList.add("equalBreaksValues");</code><br> <code>return depList;</code>
   *
   * @return List&lt;String&gt;List with named of InfoReaders(dependencies) for this InfoReader
   */
  @Override
  public List<String> getInfoDependencies() {
    return infoDependencies;
  }

  @Override
  public void setInfoDependencies(List<String> infoDependencies) {
    this.infoDependencies = infoDependencies;
  }

  @Override
  public String getErrorMessage() {
    return this.errorMassage;
  }

  @Override
  public int getOrderId() {
    return 0;
  }

  @Override
  public void setOrderId(int orderId) {
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    Optional<Boolean> val = restorer.loadBoolean("used");
    if (val.isPresent()) {
      setUsed(val.get());
    } else {
      log.error("Value {} is not present for infoReader {} on file {}",
          "used", getId(), getTargetFile().getName());
      setUsed(false);
    }
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("used", isUsed());
  }

  @Override
  public String getId() {
    return infoId;
  }

  @Override
  public void setId(String id) {
    this.infoId = id;
  }

  protected String getPrecisionString(String floatString) {
    char decimalSeparator = '.';
    String[] stringArray = floatString.split(Pattern.quote(Character.toString(decimalSeparator)));
    int tmpPrecision = stringArray[stringArray.length - 1].length();
    StringBuilder sPrecisionBuilder = new StringBuilder("0.");
    if (tmpPrecision > 0) {
      for (int i = 0; i < tmpPrecision; i++) {
        sPrecisionBuilder.append("#");
      }
    } else {
      sPrecisionBuilder.append("0");
    }

    return sPrecisionBuilder.toString();
  }

  @Override
  public IGisFileContainer getTargetFile() {
    return this.targetFile;
  }

  @Override
  public void setTargetFile(IGisFileContainer targetFile) {
    this.targetFile = targetFile;
  }

  @Override
  public IGisFileContainer getParentFileContainer() {
    return getTargetFile();
  }

  @Override
  public boolean isUsed() {
    return used;
  }

  @Override
  public void setUsed(boolean used) {
    this.used = used;
  }

  @Override
  public List<VariableValue> getVariableList() {
    return Collections.emptyList();
  }

  @Override
  public void setVariableList(List<VariableValue> vaList) {
  }

  @Override
  public void run() {
    if (this.targetFile == null) {
      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(100.0F);
        this.workerStatus.setProgressName("ERROR: Target file == null");
        this.workerStatus.errorProcess();
      }
      if (this.dependencer != null) {
        this.dependencer.errorRunnable(this);
      }
    } else {
      try {
        if (this.workerStatus != null) {
          this.workerStatus.setProgressName(this.getId());
        }
        this.targetFile.setLock(true);
        this.readInfos(this.targetFile);
        //TODO: Workaround for testing
        if (this.targetFile.getGisFileInformationStorage() != null) {
          this.targetFile.getGisFileInformationStorage().refreshPluginExec(this);
        }

        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.endProgress();
        }
        if (this.dependencer != null) {
          this.dependencer.endRunnable(this);
        }
      } catch (Exception exception) {
        log.error("UNKNOWN", exception);
        this.errorMassage = exception.getMessage();
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.setProgressName(exception.getMessage());
          this.workerStatus.errorProcess(exception);
        }
        if (this.dependencer != null) {
          this.dependencer.errorRunnable(this);
        }
      } finally {
        this.targetFile.setLock(false);
      }
    }
  }

  @Override
  public void setDependencyChecker(ExecDependencyController mDep) {
    this.dependencer = mDep;
  }

  @Override
  public void setWorkerStatus(WorkerStatus mWorkerStatus) {
    this.workerStatus = mWorkerStatus;
  }

//  /**
//   * <b>Note</b><br> Can be used to determine the human readable
//   * text for this InfoReader <br> .
//   *
//   * @return a <code>String</code> that returns {@link InfoReader#getOrderId()}
//   * with the fist letter in upper case
//   */
  @Override
  public String toString() {
    return this.getId();
//    String infoName = this.getId();
//    return infoName.substring(0, 1).toUpperCase() + infoName.substring(1);
  }
}

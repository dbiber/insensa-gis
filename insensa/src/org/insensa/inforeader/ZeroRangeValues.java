/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.gdal.gdal.Band;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class ZeroRangeValues extends AbstractInfoReader {


  public static final String NAME = "ZeroRangeValues";
  public static final int PERCENTAGE = 0;
  public static final int VALUE = 1;
  private static final long serialVersionUID = -1220911932608638601L;
  private long count = 0;
  private String precisionString = "";
  private float range = 0.0F;
  private float rangeValue = 0.0F;
  private float relativeCount = 0.0F;
  private int calcMode = 0;

  public int getCalcMode() {
    return this.calcMode;
  }

  public void setCalcMode(int calcMode) {
    this.calcMode = calcMode;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(MinMaxValues.NAME);
    depList.add(PrecisionValues.NAME);
    depList.add(CountValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("rangeValue", rangeValue);
    saver.save("mode", calcMode);
    if (isUsed()) {
      saver.save("range", range);
      saver.save("count", count);
      saver.save("relativeCount", relativeCount);
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();

    if (isUsed()) {
      DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
      DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
      dSymb.setDecimalSeparator('.');
      precisionFormat.setDecimalFormatSymbols(dSymb);
      precisionFormat.setRoundingMode(RoundingMode.HALF_UP);
      table.put("Range (%)", precisionFormat.format(this.range * 100));
      table.put("Range Value", precisionFormat.format(this.rangeValue));
      table.put("Count", Long.toString(count));
      table.put("Count Relative (%)", precisionFormat.format(this.relativeCount));
    }
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.rangeValue = restorer.loadFloat("rangeValue").orElse(0.0F);
    this.calcMode = restorer.loadInteger("mode").orElse(0);

    if (isUsed()) {
      this.count = restorer.loadLong("count").get();
      this.range = restorer.loadFloat("range").get();
      this.relativeCount = restorer.loadFloat("relativeCount").orElse(0.0F);
    }
  }

  public float getRange() {
    return this.range;
  }

  public void setRange(float range) {
    this.range = range;
  }

  public float getRangeValue() {
    return this.rangeValue;
  }

  public void setRangeValue(float rangeValue) {
    this.rangeValue = rangeValue;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    float factor = (float) (100.0 / ((RasterFile) file.getGisFile()).getNRows());
    float readValue;

    this.processStatus = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("ZeroRangeValues:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("ZeroRangeValues:readInfos: precisionValues not read");

    InfoReader minMaxValues = file.getInfoReader(MinMaxValues.NAME);
    if (minMaxValues == null)
      throw new IOException("ZeroRangeValues:readInfos: no minMaxValues InfoReader Found");
    if (!minMaxValues.isUsed())
      throw new IOException("ZeroRangeValues:readInfos: minMaxValues not read");

    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("ZeroRangeValues:readInfos: no countValues InfoReader Found");
    if (!countValues.isUsed()) {
      throw new IOException("ZeroRangeValues:readInfos: countValues not read");
    }

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    float minValue = ((MinMaxValues) minMaxValues).getMinValue();
    float maxValue = ((MinMaxValues) minMaxValues).getMaxValue();
    float tmpRange = maxValue - minValue;

    long numOfData = ((CountValues) countValues).getNumOfData();

    if (this.calcMode == PERCENTAGE) {
      this.rangeValue = this.range * tmpRange;
    } else {
      this.range = this.rangeValue / tmpRange;
    }

    float lowerValue = 0 - this.rangeValue;
    float upperValue = 0 + this.rangeValue;

    Band band = ((RasterFile) file.getGisFile()).getBand(RasterFileAccess.READ_ONLY);// ds.GetRasterBand(1);

    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    // Integer tmpInt=null;
    // int tmpNumOfData=0;
    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (float element : dData) {
        readValue = element;
        if (readValue == ((RasterFile) file.getGisFile()).getNoDataValue()) {
        } else {
          if (readValue >= lowerValue && readValue <= upperValue) {
            if (this.count == Long.MAX_VALUE) {
              throw new IOException("Num of found values is higher then Long-Max");
            }
            this.count++;
          }
        }
      }
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }

    this.relativeCount = (this.count / numOfData) * 100.0F;
    file.getGisFile().unlock();
    this.used = true;
  }

  @Override
  public void setUsed(boolean used) {
    if (!used) {
      this.count = 0;
    }
    super.setUsed(used);
  }

}

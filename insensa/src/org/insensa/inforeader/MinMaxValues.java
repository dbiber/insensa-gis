/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;


public class MinMaxValues extends AbstractInfoReader {

  public static final String NAME = "MinMaxValue";
  private float minValue = Float.MAX_VALUE;//TODO: Use Double instead ?
  private float maxValue = Float.MIN_VALUE;//TODO: Use Double instead ?
  private String precisionString = "";//TODO: Getting Without Precision ?

  @Override
  public String getId() {
    return NAME;
  }

  @Override
  public List<String> getInfoDependencies() {
    return Arrays.asList(PrecisionValues.NAME, CountValues.NAME);
  }

  @Override
  public AttributeTable getData() {
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);
    AttributeTable table = new AttributeTable();
    if (used) {
      table.put("Min Value", precisionFormat.format(this.minValue));
      table.put("Max Value", precisionFormat.format(this.maxValue));
    }
    return table;
  }


  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);
    if (isUsed()) {
      saver.save("precision", this.precisionString);
      saver.save("minimum", saver1 -> {
        saver1.save("value", precisionFormat.format(this.minValue));
      });
      saver.save("maximum", saver1 -> {
        saver1.save("value", precisionFormat.format(this.maxValue));
      });
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (isUsed()) {
      this.precisionString = restorer.loadString("precision").orElse("0");
      restorer.loadRestorable("minimum", restorer1 -> {
        this.minValue = restorer1.loadFloat("value").get();
      });
      restorer.loadRestorable("maximum", restorer1 -> {
        this.maxValue = restorer1.loadFloat("value").get();
      });
    }
  }

  public float getMaxValue() {
    return this.maxValue;
  }

  public float getMinValue() {
    return this.minValue;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    this.used = false;
    this.processStatus = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null) {
      this.dependencer.checkInfoDependencies(this);
    }

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("MinMaxValues:readInfos: no precisionValues InfoReader Found");
    if (precisionValues.isUsed() == false)
      throw new IOException("MinMaxValues:readInfos: precisionValues not read");

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("MinMaxValues:ReadInfos: no countValues InfoReader Found");
    if (countValues.isUsed() == false)
      throw new IOException("MinMaxValues:ReadInfos: precisionValues not read");

    TreeMap<Float, Long> countMap = (TreeMap<Float, Long>) ((CountValues) countValues)
        .getCountValues().getMap();
    if (countMap.isEmpty())
      throw new IOException("MinMaxValues: CountValues are empty");
    this.minValue = countMap.firstKey();
    this.maxValue = countMap.lastKey();
    this.setUsed(true);
  }

  @Override
  public String toString() {
    return getId();
//    String infoName = this.getId();
//    return infoName.substring(0, 1).toUpperCase() + infoName.substring(1);
  }

}

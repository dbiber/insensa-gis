/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.extensions.IPluginExec;
import org.insensa.helpers.ExecDependencyController;

import java.io.IOException;


public interface InfoReader extends IPluginExec {

  /**
   * Returns the file of type {@link IGisFileContainer} that holds this InfoReader.
   * targetFile will only be set if the InfoReader operation was performed, otherwise null
   *
   * @return the file that holds this InfoReader
   */
  IGisFileContainer getTargetFile();

  /**
   * determines the target file for this InfoReader.
   *
   * @param targetFile Target file of type {@link IGisFileContainer} for this
   *                   InfoReader
   */
  void setTargetFile(IGisFileContainer targetFile);

  /**
   * This method will try to receive all informations from dependent InfoReader You can get
   * InfoReader with <b>file.getInfoReader(nameOfInfoReader);</b>
   *
   * @param file File that holds this InfoReader
   */
  void readInfos(IGisFileContainer file) throws IOException;

  /**
   * Set dependency checker
   */
  void setDependencyChecker(ExecDependencyController mDep);

}

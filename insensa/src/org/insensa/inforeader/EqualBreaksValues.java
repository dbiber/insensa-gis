/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.ClassificationRange;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class EqualBreaksValues extends AbstractInfoReader implements BreaksValues {

  public static final String NAME = "EqualBreaksValues";
  private static final long serialVersionUID = 483275816055455467L;
  private int numOfClasses = 5;
  private List<ClassificationRange> rangeList = new ArrayList<>();
  private String precisionString = "0";

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<>();
    depList.add(CountValues.NAME);
    depList.add(MinMaxValues.NAME);
    depList.add(PrecisionValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("numOfClasses", numOfClasses);
    saver.save("precision", precisionString);

    if (isUsed()) {
      this.rangeList.forEach(classificationRange ->
          saver.save("Class", classificationRange));
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    if (isUsed()) {
      for (int i = 0; i < rangeList.size(); i++) {
        table.put("Break " + (i + 1) + " Low",
            precisionFormat.format(this.rangeList.get(i).getLowValue()));
        table.put("Break " + (i + 1) + " High",
            precisionFormat.format(this.rangeList.get(i).getHighValue()));
      }
    }
    return table;
  }


  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (!isUsed()) {
      return;
    }
    List<IRestorableData> data = restorer.loadCollection("Class",
        ClassificationRange.class);
    this.rangeList = new ArrayList<>();
    data.forEach(iRestorableData ->
        rangeList.add((ClassificationRange) iRestorableData));
  }

  public int getNumOfClasses() {
    return this.numOfClasses;
  }

  @Override
  public void setNumOfClasses(int numOfClasses) {
    this.numOfClasses = numOfClasses;
  }

  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  @Override
  public IGisFileContainer getTargetFile() {
    return this.targetFile;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    float minValue;
    float maxValue;
    float range;
    float multi;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }

    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);
    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("EqualBreaksValues:readInfos: no countValues InfoReader Found");
    if (!countValues.isUsed())
      throw new IOException("EqualBreaksValues:readInfos: countValues not read");

    InfoReader minMaxReader = file.getInfoReader(MinMaxValues.NAME);
    if (minMaxReader == null)
      throw new IOException("EqualBreaksValues:readInfos: no minMax InfoReader Found");
    if (!minMaxReader.isUsed())
      throw new IOException("EqualBreaksValues:readInfos: minMaxValues not read");

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("PrecisionValues:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("PrecisionValues:readInfos: precisionValues not read");

    if (((CountValues) countValues).getCountValues().size() < this.numOfClasses)
      throw new IOException("Number of different data " + ((CountValues) countValues)
          .getCountValues().size() + "<" + this.numOfClasses);

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    minValue = ((MinMaxValues) minMaxReader).getMinValue();
    maxValue = ((MinMaxValues) minMaxReader).getMaxValue();

    range = maxValue - minValue;
    multi = range / this.numOfClasses;
    float step = 100 / this.numOfClasses;

    int precVal = ((PrecisionValues) precisionValues).getMaxPrecision();
    Math.pow(10, precVal);

    for (int i = 0; i < this.numOfClasses; i++) {
      this.rangeList.add(new ClassificationRange(minValue + (i * multi), minValue + ((i + 1) * multi)));
      this.processStatus += step;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    this.used = true;
  }

  @Override
  public void setUsed(boolean used) {
    if (!used) {
      this.rangeList.clear();
      this.used = false;
    }
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.ClassificationRange;
import org.insensa.helpers.VariableValue;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class KMeansValues extends AbstractInfoReader {
  private static final Logger log = LoggerFactory.getLogger(KMeansValues.class);

  private static final long serialVersionUID = -7767540008391095066L;
  public List<InfoReader> iReaderList = new ArrayList<InfoReader>();
  public List<InfoReader> iReaderListDep = new ArrayList<InfoReader>();
  private String precisionString = "0.#";
  private int numOfClasses = 5;
  private List<ClassificationRange> rangeList = new ArrayList<ClassificationRange>();

  private long calcClassCount(float data[], long[] dataCounts, int fromIncl, int toIncl) {
    long count = 0;
    for (int i = fromIncl; i <= toIncl; i++) {
      count += dataCounts[i];
    }

    return count;
  }

  private float calcMean(float[] data, long[] dataCounts, long classCounts, int fromIncl, int toIncl) {
    float mean = 0.0F;

    for (int i = fromIncl; i <= toIncl; i++) {
      mean += dataCounts[i] * (data[i] / classCounts);
    }

    return mean;
  }

  private float[] calcMeans(Map<Float, Long> dataMap, int[] counts, float[] ranges) {
    Iterator<Float> iData = dataMap.keySet().iterator();
    int index = 0;
    float[] means = new float[this.numOfClasses];
    means[0] = 0.0F;
    while (iData.hasNext()) {
      Float data = iData.next();
      if (data <= ranges[index]) {
        means[index] += ((data * dataMap.get(data)) / counts[index]);
      } else {
        index++;
        means[index] = 0.0F;
        if (index < this.numOfClasses)
          means[index] += ((data * dataMap.get(data)) / counts[index]);
      }
    }
    return means;
  }

  private float[] calcMeans3(float[] data, long[] dataCounts, int[] classCounts, int[] places) {
    float[] means = new float[this.numOfClasses];
    means[0] = this.calcMean(data, dataCounts, classCounts[0], 0, places[0]);
    for (int i = 1; i < this.numOfClasses; i++) {
      means[i] = this.calcMean(data, dataCounts, classCounts[i], places[i - 1] + 1, places[i]);
    }
    return means;
  }

  private float calcSumOfVariances(float[] data, long[] dataCount, int[] places, int[] classCounts) {
    float[] variances = new float[this.numOfClasses];
    float variance = 0.0F;
    float mean;
    mean = this.calcMean(data, dataCount, classCounts[0], 0, places[0]);
    variances[0] = this.calcVariance(data, dataCount, mean, classCounts[0], 0, places[0]);
    variance = variances[0];
    for (int i = 1; i < this.numOfClasses; i++) {
      mean = this.calcMean(data, dataCount, classCounts[i], places[i - 1] + 1, places[i]);
      variances[i] = this.calcVariance(data, dataCount, mean, classCounts[i], places[i - 1] + 1, places[i]);
      variance += variances[i];
    }
    return variance;
  }

  private float calcSumOfVariances2(float[] data, long[] dataCount, int[] places) {
    float[] variances = new float[this.numOfClasses];
    float variance = 0.0F;
    float mean;
    long classCount;
    classCount = this.calcClassCount(data, dataCount, 0, places[0]);
    mean = this.calcMean(data, dataCount, classCount, 0, places[0]);
    variances[0] = this.calcVariance(data, dataCount, mean, classCount, 0, places[0]);
    variance = variances[0];
    for (int i = 1; i < this.numOfClasses; i++) {
      classCount = this.calcClassCount(data, dataCount, places[i - 1] + 1, places[i]);
      mean = this.calcMean(data, dataCount, classCount, places[i - 1] + 1, places[i]);
      variances[i] = this.calcVariance(data, dataCount, mean, classCount, places[i - 1] + 1, places[i]);
      variance += variances[i];
    }
    return variance;
  }

  private float calcVariance(float[] data, long[] dataCounts, float mean, long classCounts, int fromIncl, int toIncl) {
    float variance = 0.0F;

    float sum = 0.0F;
    for (int i = fromIncl; i <= toIncl; i++) {
      sum = dataCounts[i] * ((data[i] - mean) * (data[i] - mean));
      variance += sum / classCounts;
    }

    return variance;
  }

  private float getBreaks(Map<Float, Long> dataMap, int numOfClasses2, List<ClassificationRange> equalRangeList) {
    Iterator<Float> iData = dataMap.keySet().iterator();
    float[] ranges = new float[this.numOfClasses];
    int[] places = new int[this.numOfClasses];
    int[] placesNew = new int[this.numOfClasses];
    // float[]means = new float[numOfClasses];
    float[] datas = new float[dataMap.size()];
    long[] dataCounts = new long[dataMap.size()];
    int[] counts = new int[this.numOfClasses];
    for (int i = 0; i < this.numOfClasses; i++) {
      // means[i]=0.0F;
      counts[i] = 0;
    }
    // float tmpMean=0.0F;
    int index = 0;
    int index2 = 0;
    // int index3=0;
    while (iData.hasNext()) {
      Float data = iData.next();
      if (data <= equalRangeList.get(index).getHighValue()) {
        ranges[index] = data;
        counts[index] += dataMap.get(data);
        places[index] = index2;
        placesNew[index] = index2;

      } else {
        index++;
        if (index < this.numOfClasses) {
          ranges[index] = data;
          counts[index] += dataMap.get(data);
          places[index] = index2;
          placesNew[index] = index2;
        }
      }
      datas[index2] = data;
      dataCounts[index2] = dataMap.get(data);
      index2++;
    }

    float[] means = this.calcMeans(dataMap, counts, ranges);

    float var = 0;
    int min = 0;
    int change = 0;
    for (int k = 0; k < 50; k++) {
      change = 0;
      for (int i = 0; i < this.numOfClasses - 1; i++) {
        for (int j = places[i]; j > min; j--) {
          if (Math.abs(datas[j] - means[i]) > Math.abs(datas[j] - means[i + 1])) {
            change++;
            counts[i] -= dataCounts[j];
            counts[i + 1] += dataCounts[j];
            placesNew[i]--;
            ranges[i] = datas[placesNew[i]];
          } else
            break;
        }
        for (int j = places[i] + 1; j < places[i + 1]; j++) {
          if (Math.abs(datas[j] - means[i]) < Math.abs(datas[j] - means[i + 1])) {
            change++;
            counts[i] += dataCounts[j];
            counts[i + 1] -= dataCounts[j];
            placesNew[i]++;
            ranges[i] = datas[placesNew[i]];
          } else
            break;
        }
        min = places[i];
      }
      log.debug("Run: " + k + " Changes: " + change);
      for (int l = 0; l < placesNew.length; l++)
        places[l] = placesNew[l];
      means = this.calcMeans(dataMap, counts, ranges);
      var = this.calcSumOfVariances(datas, dataCounts, places, counts);
      log.debug("SSD: " + var);
      if (change == 0)
        break;
    }

    for (int l = 0; l < places.length; l++) {
      log.debug("break " + l + "= " + datas[places[l]]);
    }
    return var;

  }

  private float getBreaks2(Map<Float, Long> dataMap, int numOfClasses2, List<ClassificationRange> equalRangeList) {
    Iterator<Float> iData = dataMap.keySet().iterator();
    float[] ranges = new float[this.numOfClasses];
    int[] places = new int[this.numOfClasses];
    int[] placesNew = new int[this.numOfClasses];
    // float[]means = new float[numOfClasses];
    float[] datas = new float[dataMap.size()];
    long[] dataCounts = new long[dataMap.size()];
    int[] counts = new int[this.numOfClasses];
    for (int i = 0; i < this.numOfClasses; i++) {
      // means[i]=0.0F;
      counts[i] = 0;
    }
    // float tmpMean=0.0F;
    int index = 0;
    int index2 = 0;
    // int index3=0;
    while (iData.hasNext()) {
      Float data = iData.next();
      if (data <= equalRangeList.get(index).getHighValue()) {
        ranges[index] = data;
        counts[index] += dataMap.get(data);
        places[index] = index2;
        placesNew[index] = index2;

      } else {
        index++;
        if (index < this.numOfClasses) {
          ranges[index] = data;
          counts[index] += dataMap.get(data);
          places[index] = index2;
          placesNew[index] = index2;
        }
      }
      datas[index2] = data;
      dataCounts[index2] = dataMap.get(data);
      index2++;
    }

    this.calcMeans(dataMap, counts, ranges);

    float var = 0;
    int min = 0;
    int change = 0;
    int noChange = 0;
    for (int k = 0; k < 50; k++) {
      change = 0;
      for (int i = 0; i < this.numOfClasses - 1; i++) {
        for (int j = places[i]; j > min; j--) {
          placesNew[i]--;
          float var1 = this.calcSumOfVariances2(datas, dataCounts, places);
          float var2 = this.calcSumOfVariances2(datas, dataCounts, placesNew);

          if (var2 < var1) {
            // placesNew[i]--;
            change++;
          } else {
            placesNew[i]++;
            break;
          }
        }
        for (int j = places[i] + 1; j < places[i + 1]; j++) {
          placesNew[i]++;
          float var1 = this.calcSumOfVariances2(datas, dataCounts, places);
          float var2 = this.calcSumOfVariances2(datas, dataCounts, placesNew);
          if (var2 < var1) {
            // places[i]--;
            change++;
          } else {
            placesNew[i]--;
            break;
          }
        }
        min = places[i];
      }
      log.debug("Run: " + k + " Changes: " + change);
      for (int l = 0; l < placesNew.length; l++)
        places[l] = placesNew[l];
      // means = calcMeans(dataMap, counts, ranges);
      var = this.calcSumOfVariances2(datas, dataCounts, places);
      log.debug("SSD: " + this.calcSumOfVariances2(datas, dataCounts, places));
      if (change == 0)
        noChange++;
      else
        noChange = 0;
      if (noChange == this.numOfClasses)
        break;
    }

    for (int l = 0; l < places.length; l++) {
      log.debug("break " + l + "= " + datas[places[l]]);
    }
    return var;
  }

  private float getBreaks3(Map<Float, Long> dataMap, int numOfClasses2, List<ClassificationRange> equalRangeList) {
    Iterator<Float> iData = dataMap.keySet().iterator();
    // float[] ranges = new float[numOfClasses];
    int[] places = new int[this.numOfClasses];
    int[] placesNew = new int[this.numOfClasses];
    // float[]means = new float[numOfClasses];
    float[] datas = new float[dataMap.size()];
    long[] dataCounts = new long[dataMap.size()];
    int[] counts = new int[this.numOfClasses];
    for (int i = 0; i < this.numOfClasses; i++) {
      // means[i]=0.0F;
      counts[i] = 0;
    }
    // float tmpMean=0.0F;
    int index = 0;
    int index2 = 0;
    // int index3=0;
    while (iData.hasNext()) {
      Float data = iData.next();
      if (data <= equalRangeList.get(index).getHighValue()) {
        counts[index] += dataMap.get(data);
        places[index] = index2;
        placesNew[index] = index2;

      } else {
        index++;
        if (index < this.numOfClasses) {
          // ranges[index]=data;
          counts[index] += dataMap.get(data);
          places[index] = index2;
          placesNew[index] = index2;
        }
      }
      datas[index2] = data;
      dataCounts[index2] = dataMap.get(data);
      index2++;
    }

    float[] means = this.calcMeans3(datas, dataCounts, counts, places);

    float var = 0;
    int min = 0;
    int change = 0;
    int noChange = 0;
    for (int k = 0; k < 50; k++) {
      change = 0;
      for (int i = 0; i < this.numOfClasses - 1; i++) {
        for (int j = places[i]; j > min; j--) {
          if (Math.abs(datas[j] - means[i]) > Math.abs(datas[j] - means[i + 1])) {
            change++;
            counts[i] -= dataCounts[j];
            counts[i + 1] += dataCounts[j];
            places[i]--;
            means = this.calcMeans3(datas, dataCounts, counts, places);
          } else
            break;
        }
        for (int j = places[i] + 1; j < places[i + 1]; j++) {
          if (Math.abs(datas[j] - means[i]) < Math.abs(datas[j] - means[i + 1])) {
            change++;
            counts[i] += dataCounts[j];
            counts[i + 1] -= dataCounts[j];
            // placesNew[i]++;
            places[i]++;
            means = this.calcMeans3(datas, dataCounts, counts, places);
          } else
            break;
        }
        min = places[i];
      }
      log.debug("Run: " + k + " Changes: " + change);
      var = this.calcSumOfVariances(datas, dataCounts, places, counts);
      log.debug("SSD: " + this.calcSumOfVariances(datas, dataCounts, places, counts));
      if (change == 0)
        noChange++;
      else
        noChange = 0;
      if (noChange == this.numOfClasses)
        break;
    }

    for (int l = 0; l < places.length; l++) {
      log.debug("break " + l + "= " + datas[places[l]]);
    }
    return var;

  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(CountValues.NAME);
    depList.add(PrecisionValues.NAME);
    depList.add(EqualBreaksValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (!used) {
      return;
    }
    saver.save("numOfClasses", numOfClasses);
    saver.save("precision", precisionString);

    this.rangeList.forEach(classificationRange ->
        saver.save("Class", classificationRange));
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    if (!isUsed()) {
      return table;
    }

    for (int i = 0; i < rangeList.size(); i++) {
      table.put("Break " + (i + 1) + " Low",
          precisionFormat.format(this.rangeList.get(i).getLowValue()));
      table.put("Break " + (i + 1) + " High",
          precisionFormat.format(this.rangeList.get(i).getHighValue()));
    }
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    super.restore(restorer);
    if (!isUsed()) {
      return;
    }
    List<IRestorableData> data = restorer.loadCollection("Class",
        ClassificationRange.class);
    this.rangeList = new ArrayList<>();
    data.forEach(iRestorableData ->
        rangeList.add((ClassificationRange) iRestorableData));
  }

  public int getNumOfClasses() {
    return this.numOfClasses;
  }

  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public List<VariableValue> getVariableList() {
    VariableValue<String> numOfClasses2 = new VariableValue<String>("5");
    List<VariableValue> variableList = new ArrayList<>();
    numOfClasses2.setiD(0);
    numOfClasses2.setShortDescription("Num Of Classes");
    variableList.add(numOfClasses2);
    return variableList;
  }

  public void setVariableList(List<VariableValue> variableList) {
    VariableValue<String> iVal = variableList.get(0);
    int tmpNumOfClasses = Integer.valueOf(iVal.getVariable());
    if (tmpNumOfClasses > 1 && tmpNumOfClasses < Integer.MAX_VALUE)
      this.numOfClasses = tmpNumOfClasses;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {

    this.processStatus = 0.0F;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }

    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("KMeansValues:readInfos: no countValues InfoReader Found");
    if (!countValues.isUsed())
      throw new IOException("KMeansValues:readInfos: countValues not read");

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("KMeansValues:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("KMeansValues:readInfos: precisionValues not read");

    InfoReader equalBreaks = file.getInfoReader(EqualBreaksValues.NAME);
    if (equalBreaks == null)
      throw new IOException("KMeansValues:readInfos: no equalBreaks InfoReader Found");
    if (!equalBreaks.isUsed())
      throw new IOException("KMeansValues:readInfos: equalBreaks not read");

    if (((CountValues) countValues).getCountValues().size() < this.numOfClasses)
      throw new IOException("Number of different data " + ((CountValues) countValues).getCountValues().size() + "<" + this.numOfClasses);

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    Map<Float, Long> dataMap = ((CountValues) countValues).getCountValues().getMap();
    List<ClassificationRange> equalRangeList = ((EqualBreaksValues) equalBreaks).getRangeList();

    float var1 = this.getBreaks(dataMap, this.numOfClasses, equalRangeList);
    float var2 = this.getBreaks3(dataMap, this.numOfClasses, equalRangeList);
    float var3 = this.getBreaks2(dataMap, this.numOfClasses, equalRangeList);

    log.debug("Breaks1 : " + var1);
    log.debug("Breaks3 : " + var2);
    log.debug("Breaks2 : " + var3);
    // read=true;
  }

  @Override
  public void setUsed(boolean used) {
    if (!used) {
      this.rangeList.clear();
      this.used = false;
    }
  }
}

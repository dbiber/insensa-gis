/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.gdal.gdal.Band;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class CountValuesBlocksize extends AbstractInfoReader {
  private static final Logger log = LoggerFactory.getLogger(CountValuesBlocksize.class);
  private static final long serialVersionUID = 7823185364034937704L;
  public int noDataValueCount = 0;
  protected CountValuesValue countValues = new CountValuesValue();
  private int numOfCounts = 0;
  private int numOfData = 0;
  private String precisionString = "0";

  public CountValuesValue getCountValues() throws IOException {
    CountValuesValue tmpCountValues = new CountValuesValue();
    if (this.targetFile.getGisFileInformationStorage() == null) {
      return tmpCountValues;
    }
    ISavableRestorer restorer = this.targetFile
        .getGisFileInformationStorage().getRestorer();
    restorer.loadRestorable("infos", restorer1 -> {
      restorer1.loadRestorable(getId(), restorer2 -> {
        if (restorer2.loadBoolean("used").get()) {
          tmpCountValues.restore(restorer2);
        }
      });
    });
    return tmpCountValues;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(PrecisionValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (!isUsed()) {
      return;
    }
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    saver.save("numOfCounts", this.numOfCounts);
    saver.save("noDataValueCount", this.noDataValueCount);
    saver.save("numOfData", this.numOfData);
    saver.save(this.countValues);
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (!isUsed()) {
      return;
    }
    this.numOfCounts = restorer.loadInteger("numOfCounts").get();
    this.noDataValueCount = restorer.loadInteger("noDataValueCount").get();
    this.numOfData = restorer.loadInteger("numOfData").get();
  }

  public int getNumOfData() {
    return this.numOfData;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {

    float readValue;

    this.processStatus = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null) {
      throw new IOException("CFastCountValuesNew:readInfos: no precisionValues InfoReader Found");
    }
    if (!precisionValues.isUsed()) {
      throw new IOException("CFastCountValuesNew:readInfos: precisionValues not read");
    }

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    Band band = ((RasterFile) file.getGisFile()).getBand(RasterFileAccess.READ_ONLY);

    int xBlocksize = band.GetBlockXSize();
    int yBlocksize = band.GetBlockYSize();

    float factor = (float) (100.0 / ((((RasterFile) file.getGisFile()).getNCols() * ((RasterFile) file.getGisFile()).getNRows()) / (xBlocksize * yBlocksize)));

    int xTmpSize = xBlocksize;
    int yTmpSize = yBlocksize;
    int xSize = band.getXSize();
    int ySize = band.getYSize();
    Long tmpInt = null;
    int tmpNumOfData = 0;

    for (int y = 0; y < ySize; y += yBlocksize) {
      for (int x = 0; x < xSize; x += xBlocksize) {
        if (x + xBlocksize > xSize) {
          xTmpSize = xSize - x;
        } else {
          xTmpSize = xBlocksize;
        }

        if (y + yBlocksize > ySize) {
          yTmpSize = ySize - y;
        } else {
          yTmpSize = yBlocksize;
        }
        float[] dData = new float[xTmpSize * yTmpSize];
        band.ReadRaster(x, y, xTmpSize, yTmpSize, dData);
        for (float element : dData) {
          readValue = element;
          if (readValue == ((RasterFile) file.getGisFile()).getNoDataValue()) {
            this.noDataValueCount++;
          } else {
            tmpNumOfData++;
            tmpInt = this.countValues.getMap().get(readValue);
            if (tmpInt == null) {
              this.countValues.put(readValue, 1L);
            }
            if (tmpInt != null) {
              this.countValues.put(readValue, tmpInt + 1);
            }
          }
        }
        this.processStatus += factor;
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(this.processStatus);
        }
      }
    }

    this.numOfCounts = this.countValues.size();
    this.numOfData = tmpNumOfData;
    file.getGisFile().unlock();
    this.used = true;
  }

  @Override
  public void run() {
    if (this.targetFile == null) {
      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(100.0F);
        this.workerStatus.setProgressName("ERROR: Target file == null");
        this.workerStatus.errorProcess();
      }
      if (this.dependencer != null)
        this.dependencer.errorRunnable(this);
    } else {
      try {
        this.readInfos(this.targetFile);
        this.targetFile.getGisFileInformationStorage().refreshPluginExec(this);
        this.countValues.getMap().clear();
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.endProgress();
        }
        if (this.dependencer != null)
          this.dependencer.endRunnable(this);
      } catch (Exception e) {
        log.error("Error executing Count Values", e);
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.setProgressName(e.getMessage());
          this.workerStatus.errorProcess();
        }
        if (this.dependencer != null)
          this.dependencer.errorRunnable(this);
      }
    }
  }

  @Override
  public void setUsed(boolean used) {
    if (!used) {
      super.setUsed(false);
      this.countValues.getMap().clear();
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable attributeTable = new AttributeTable();
    if (isUsed()) {
      attributeTable.put("Num of Counts", Long.toString(this.numOfCounts));
      attributeTable.put("Num of Data Values", Long.toString(this.numOfData));
      attributeTable.put("Num of noDataValues", Long.toString(this.noDataValueCount));
    }
    return attributeTable;
  }
}

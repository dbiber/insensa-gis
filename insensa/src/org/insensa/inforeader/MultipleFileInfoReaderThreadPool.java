/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.Environment;
import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ExecDependencyController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MultipleFileInfoReaderThreadPool {
  private Map<IGisFileContainer, ExecDependencyController> depMap = new HashMap<IGisFileContainer, ExecDependencyController>();
  private Map<IGisFileContainer, ArrayList<Runnable>> runnableMap = new HashMap<IGisFileContainer, ArrayList<Runnable>>();
  private ExecutorService exeService;


  public MultipleFileInfoReaderThreadPool() {
    // int prozCount = 1;
    int prozCount = Environment.getMaxThreadsAvailable();
    this.exeService = Executors.newFixedThreadPool(prozCount);
  }

  public void addInfoThread(IGisFileContainer rasterFile, Runnable threadObject) {
    if (this.depMap.get(rasterFile) == null) {
      this.depMap.put(rasterFile, rasterFile.getDepChecker());
    }
    if (this.runnableMap.get(rasterFile) == null) {
      this.runnableMap.put(rasterFile, new ArrayList<Runnable>());
    }
    this.runnableMap.get(rasterFile).add(threadObject);
    this.depMap.get(rasterFile).setDependency((InfoReader) threadObject);
  }

  public void executeThreads(List<IGisFileContainer> rasterFileList,
                             List<WorkerStatusList> workerStatusListList) throws IOException {
    for (int j = 0; j < rasterFileList.size(); j++) {
      int unusedCnt = 0;
      List<Runnable> tmpRunList = this.runnableMap.get(rasterFileList.get(j));
      List<Runnable> runList = new ArrayList<Runnable>();
      for (Runnable aTmpRunList: tmpRunList) {
        if (!((InfoReader) aTmpRunList).isUsed()) {
          runList.add(aTmpRunList);
          unusedCnt++;
        }
      }
      if (workerStatusListList.get(j) != null) {
        workerStatusListList.get(j).startAllProcesses(rasterFileList.get(j).getName(), unusedCnt);
      }

      for (int i = 0; i < runList.size(); i++) {
        if (workerStatusListList.get(j) != null)
          ((InfoReader) runList.get(i)).setWorkerStatus(workerStatusListList.get(j).getWorkerStatus(i));
        this.exeService.execute(runList.get(i));
      }
    }
  }

  public ExecutorService getExeService() {
    return this.exeService;
  }

  public void removeInfoReader(RasterFileContainer rasterFile, InfoReader iReader) throws IOException {
    if (!this.runnableMap.get(rasterFile).remove(iReader))
      throw new IOException("SimplePluginExecThreadPool: removeInfoReader: InfoReader "
          + iReader.getId() + "not found");
    this.depMap.get(rasterFile).unsetDependency(iReader);
  }

}

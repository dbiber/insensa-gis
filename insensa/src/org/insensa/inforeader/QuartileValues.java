/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.MathUils;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class QuartileValues extends AbstractInfoReader {
  public static final String NAME = "QuartileValues";
  private static final long serialVersionUID = 8308746677506388559L;
  private long numOfValues = 0;
  private double lowQuartile;
  private double highQuartile;


  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(MedianValue.NAME);
    depList.add(CountValues.NAME);
    return depList;
  }

  public double getHighQuartile() {
    return this.highQuartile;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("numOfValues", numOfValues);
    saver.save("quartile", saver1 -> {
      saver1.save("lowQuartile", lowQuartile);
      saver1.save("highQuartile", highQuartile);
    });
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    table.put("Lower Quartile", Double.toString(lowQuartile));
    table.put("Upper Quartile", Double.toString(highQuartile));
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.numOfValues = restorer.loadInteger("numOfValues").get();
    restorer.loadRestorable("quartiles", restorer1 -> {
      lowQuartile = restorer1.loadDouble("lowQuartile").get();
      highQuartile = restorer1.loadDouble("highQuartile").get();
    });
  }

  public double getLowQuartile() {
    return this.lowQuartile;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    long medianIndex1;
    // int medianIndex2;
    long index1;
    long index2;
    Float val1;
    Float val2;
    long lowCount;
    // int highCount;
    this.processStatus = 0.0F;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);
    InfoReader medianReader = file.getInfoReader(MedianValue.NAME);
    InfoReader countReader = file.getInfoReader(CountValues.NAME);
    if (medianReader == null)
      throw new IOException("QuartileValues: readInfos: no medianValue InfoReader Found");
    if (!medianReader.isUsed())
      throw new IOException("QuartileValues: readInfos: medianValue not read");
    if (countReader == null)
      throw new IOException("QuartileValues: no sortetCountValues InfoReader Found");
    if (!countReader.isUsed())
      throw new IOException("QuartileValues: readInfos: sortetCountValues not read");

    Map<Float, Long> countList = ((CountValues) countReader)
        .getCountValues().getMap();

    medianIndex1 = ((MedianValue) medianReader).getIndex1();
    // medianIndex2 = ((MedianValue)medianReader).getIndex2();
    this.numOfValues = ((MedianValue) medianReader).getNumOfValues();

    lowCount = medianIndex1;
    // highCount=numOfValues-lowCount;

    if ((lowCount % 2) == 0) {
      index1 = (lowCount / 2);
      index2 = (lowCount / 2) + 1;
      val1 = MathUils.getNthData(countList, index1, this.processStatus, this.workerStatus);
      val2 = MathUils.getNthData(countList, index2, this.processStatus, this.workerStatus);
      this.lowQuartile = (val1 + val2) / 2;

      index1 += medianIndex1;
      index2 += medianIndex1;
      val1 = MathUils.getNthData(countList, index1, this.processStatus, this.workerStatus);
      val2 = MathUils.getNthData(countList, index2, this.processStatus, this.workerStatus);
      this.highQuartile = (val1 + val2) / 2;
    } else {
      index1 = ((lowCount + 1) / 2);
      index2 = index1;
      this.lowQuartile = MathUils.getNthData(countList, index1, this.processStatus, this.workerStatus);

      index1 += medianIndex1;
      this.highQuartile = MathUils.getNthData(countList, index1, this.processStatus, this.workerStatus);
    }

    if (this.workerStatus != null)
      this.workerStatus.refreshPercentage(100);
    this.used = true;

  }
}

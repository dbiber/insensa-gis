/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;


public class PrecisionValues extends AbstractInfoReader {
  public static final String NAME = "PrecisionValues";
  private static final Logger log = LoggerFactory.getLogger(PrecisionValues.class);
  private static final long serialVersionUID = -5784812178311164048L;
  private char decimalSeparator = '.';
  private int maxPrecision = 3;
  private String precisionRegex = "#";

  /**
   * Used in plugins ( e.g. StretchLegend)
   */
  public static String createPrecitionPattern(int maxPrecision) {
    StringBuilder precisionPattern = new StringBuilder("0.");
    if (maxPrecision > 0) {
      for (int i = 0; i < maxPrecision; i++) {
        precisionPattern.append("#");
      }
      return precisionPattern.toString();
    } else {
      return "0";
    }
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (isUsed()) {
      saver.save("count", maxPrecision);
      saver.save("regex", precisionRegex);
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    if (isUsed()) {
      table.put("Maximal Precision", Integer.toString(maxPrecision));
    }
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (isUsed()) {
      maxPrecision = restorer.loadInteger("count").orElse(3);
      this.precisionRegex = restorer.loadString("regex").orElse("#");
    }
  }

  public int getMaxPrecision() {
    return this.maxPrecision;
  }

  public void setMaxPrecision(int maxPrecision) {
    if (maxPrecision > 0 && maxPrecision < Integer.MAX_VALUE)
      this.maxPrecision = maxPrecision;
  }

  public String getPrecisionRegex() {
    return this.precisionRegex;
  }

  private void readAscii(IGisFileContainer file) throws IOException {
    String stringLine = new String();
    FileReader fileReader = new FileReader(file.getGisFile().getFileRef());
    BufferedReader bufReader = new BufferedReader(fileReader);
    StringTokenizer stringTokenizer = null;
    String stringToken = null;
    float readValue;
    // int iterVar =0;

    int tmpPrecision = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }

    float factor = (float) (100.0 / ((RasterFile) file.getGisFile()).getNRows());

    for (int i = 0; i < 6; i++)
      stringLine = bufReader.readLine();
    while ((stringLine = bufReader.readLine()) != null) {
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);

      stringTokenizer = new StringTokenizer(stringLine);
      while (stringTokenizer.hasMoreElements() == true) {
        stringToken = stringTokenizer.nextToken();
        try {
          readValue = Float.valueOf(stringToken);
          if (readValue == ((RasterFile) file.getGisFile()).getNoDataValue()) {

          } else {
            String[] stringArray = stringToken.split(Pattern.quote(Character.toString(this.decimalSeparator)));
            if (stringArray == null || stringArray.length <= 1) {
            } else {
              tmpPrecision = stringArray[stringArray.length - 1].length();
              if (tmpPrecision > this.maxPrecision)
                this.maxPrecision = tmpPrecision;
            }
          }
        } catch (NumberFormatException nfe) {

        }
      }
    }

    StringBuilder sPrecision = new StringBuilder("0.");
    if (this.maxPrecision > 0) {
      for (int i = 0; i < this.maxPrecision; i++) {
        sPrecision.append("#");
      }
      this.precisionRegex = sPrecision.toString();
    } else {
      this.precisionRegex = "0";
    }
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }

    String driverName;
    try {
      driverName = ((IRasterGisFile) file.getGisFile()).getDriverShortName();
    } catch (GisFileException exception) {
      log.error("Could not read precision values", exception);
      throw new IOException(exception);
    }
    if (driverName.equals("AAIGrid")) {
      this.readAscii(file);
    } else {
      if (((IRasterGisFile) file.getGisFile()).isTypeFloat()) {
        StringBuilder precisionBuilder = new StringBuilder("0.");
        for (int i = 0; i < this.maxPrecision; i++) {
          precisionBuilder.append("#");
        }
        this.precisionRegex = precisionBuilder.toString();
      } else {
        this.precisionRegex = "#";
      }
    }
    file.getGisFile().unlock();
    this.used = true;
  }

//  @Override
//  public void setVariableList(List<VariableValue> varList) {
//    VariableValue<String> iVal = varList.get(0);
//    int decPlaces = Integer.valueOf(iVal.getVariable());
//    if (decPlaces > 0 && decPlaces < Integer.MAX_VALUE) {
//      this.maxPrecision = decPlaces;
//    }
//  }


}

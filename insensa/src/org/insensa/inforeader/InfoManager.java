/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.Environment;
import org.insensa.GisFileType;
import org.insensa.IGisFile;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.inforeader.r.RRasterInfoReader;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.IStorageCloner;
import org.insensa.model.storage.JdomStorageCloner;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


public class InfoManager {
  private ExtensionManager exManager;
  private IStorageCloner cloner = new JdomStorageCloner();

  public InfoManager() {
    this.exManager = ExtensionManager.getInstance();
  }

  public void cloneInfoReader(InfoReader oldInfo, InfoReader newInfo) {
    cloner.store(oldInfo.getId(), oldInfo);
    newInfo.restore(cloner.getRestorer());
  }

  public InfoReader getInfoReaderCopy(InfoReader info) {
    InfoReader tmpInfo = this.getInfoReader(info.getId());
    cloneInfoReader(info, tmpInfo);
    tmpInfo.setId(info.getId());
    return tmpInfo;
  }

  public InfoReader getInfoReader(String info) {
    ClassLoader cl = this.exManager.getUrlClassLoader();// InsensaGIS.class.getClassLoader();
    ServiceList list = this.exManager.getInfoReaderServices().get(info);
    if (list == null)
      return null;
    Service service = list.getService(ServiceType.EXEC);
    if (service == null) {
      return null;
    } else {
      try {
        if (service.getScript() != null) {
          RRasterInfoReader genericRInfoReader = new RRasterInfoReader();
          Path absFilePath = service.getScript().extractFromFile();
          genericRInfoReader.setScriptPath(absFilePath.toString());
          genericRInfoReader.setId(info);
          Environment.restoreDefaultPluginVariables(genericRInfoReader);
          return genericRInfoReader;
        }
        Class classInfoReader = cl.loadClass(service.getPackageName() + "." + service.getClassName());
        InfoReader infoReader = (InfoReader) classInfoReader.newInstance();
        infoReader.setId(info);
        return infoReader;
      } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  public List<String> getInfoReaderList(GisFileType fileType) {
    List<String> lInfoReader = new ArrayList<String>();
    for (String name: this.exManager.getInfoReaderServices().keySet()) {
      ServiceList serviceList = this.exManager.getInfoReaderServices().get(name);
      Service service = serviceList.getService(ServiceType.EXEC);
      if (service != null && service.getGisType() == fileType) {
        lInfoReader.add(name);
      }
    }
    return lInfoReader;
  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.generic.DataVariableConverter;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.properties.IGisFileInformationStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.List;


public class CountValues extends AbstractInfoReader {

  public static final String NAME = "CountValues";
  private static final Logger log = LoggerFactory.getLogger(CountValues.class);
  private static final long serialVersionUID = -1895139439220248764L;
  public long noDataValueCount = 0;
  protected CountValuesValue countValues = new CountValuesValue();
  private long numOfCounts = 0;
  private long numOfData = 0;
  private RasterFileContainer templateFileContainer = null;
  private String precisionString = "0";

  /**
   * This method was created for testing only so the has to be no xml serialization for retrieving
   * countValues.
   *
   * @return the count values Map
   */
  public CountValuesValue getCountValuesFromCache() {
    return this.countValues;
  }

  public CountValuesValue getCountValues() {
    //TODO: Work around for testing
    if (this.targetFile.getGisFileInformationStorage() == null) {
      return getCountValuesFromCache();
    }
    CountValuesValue tmpCountValues = new CountValuesValue();
    ISavableRestorer restorer = this.targetFile
        .getGisFileInformationStorage().getRestorer();
    restorer.loadRestorable("infos", restorer1 -> {
      restorer1.loadRestorable(getId(), restorer2 -> {
        if (restorer2.loadBoolean("used").get()) {
          tmpCountValues.restore(restorer2);
        }
      });
    });
    return tmpCountValues;
  }

  @Override
  public List<String> getInfoDependencies() {
    return Collections.singletonList(PrecisionValues.NAME);
  }

  @Override
  public AttributeTable getData() {
    AttributeTable attributeTable = new AttributeTable();
    if (isUsed()) {
      attributeTable.put("Num of Counts", Long.toString(this.numOfCounts));
      attributeTable.put("Num of Data Values", Long.toString(this.numOfData));
      attributeTable.put("Num of noDataValues", Long.toString(this.noDataValueCount));
    }
    return attributeTable;
  }

  public long getNumOfCounts() {
    return this.numOfCounts;
  }

  public long getNumOfData() {
    return this.numOfData;
  }

  public IGisFileContainer getTemplateFile() {
    return this.templateFileContainer;
  }

  public void setTemplateFile(RasterFileContainer templateFile) {
    this.templateFileContainer = templateFile;
  }

  @Override
  public void readInfos(IGisFileContainer file)
      throws IOException {
    this.processStatus = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null) {
      this.dependencer.checkInfoDependencies(this);
    }

    this.numOfData = 0;
    if (this.templateFileContainer == null) {
      this.readInfosWithoutTemplate(file);
    } else {
      this.readInfosWithTemplate(file);
    }
  }

  private void countReadValues(float readValue, long prec, int decimalPoints)
      throws IOException {
    Long tmpCount;
    double newReadValue;
    //TODO newReadValue ist double, float evtl nicht ausreichend

    long roundedValue;
    newReadValue = readValue * prec;
    roundedValue = Math.round(newReadValue);
    if (roundedValue == Long.MIN_VALUE || roundedValue == Long.MAX_VALUE) {
      throw new IOException("Number  is to big, use less precision.");
    }
    newReadValue = (double) roundedValue / prec;
    readValue = new Double(newReadValue).floatValue();
    if (numOfData == Long.MAX_VALUE) {
      throw new IOException("Num of data values is higher then " + Long.MAX_VALUE);
    }
    if (numOfData < 0) {
      throw new IOException("Something went wrong when counting the num of data ");
    }
    numOfData++;

    tmpCount = this.countValues.mapValue.putIfAbsent(readValue, 1L);
    if (tmpCount != null) {
      if (tmpCount == Long.MAX_VALUE) {
        throw new IOException("Num of values \"" + readValue + "\" is higher then " + Long.MAX_VALUE);
      }
      this.countValues.put(readValue, tmpCount + 1);
    }
  }

  private void readInfosWithoutTemplate(IGisFileContainer fileContainer)
      throws IOException {
    IRasterGisFile gisFile = (IRasterGisFile) fileContainer.getGisFile();
    float factor = (float) (100.0 / gisFile.getNRows());
    float readValue;
    InfoReader precisionValues = fileContainer
        .getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null) {
      throw new IOException("CFastCountValuesNew:readInfos: no precisionValues InfoReader Found");
    }
    if (!precisionValues.isUsed()) {
      throw new IOException("CFastCountValuesNew:readInfos: precisionValues not read");
    }

    this.precisionString = ((PrecisionValues) precisionValues)
        .getPrecisionRegex();
    int precition = ((PrecisionValues) precisionValues).getMaxPrecision();
    long prec = new Double(Math.pow(10, precition)).longValue();

    int xSize = gisFile.getNCols();
    int ySize = gisFile.getNRows();
    float[] dData;// = new float[xSize];
    for (int j = 0; j < ySize; j++) {
      dData = gisFile.readRaster(0, j, xSize, 1);
      for (float element : dData) {
        readValue = element;
        if (readValue == gisFile.getNoDataValue()) {
          this.noDataValueCount++;
        } else {
          countReadValues(readValue, prec, precition);
        }
      }
      this.processStatus += factor;
      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(this.processStatus);
      }
    }
    this.numOfCounts = this.countValues.mapValue.size();
    gisFile.unlock();
    this.used = true;
  }

  private void readInfosWithTemplate(IGisFileContainer fileContainer)
      throws IOException {
    IRasterGisFile gisFile = (IRasterGisFile) fileContainer.getGisFile();
    float factor = (float) (100.0 / gisFile.getNRows());
    float readValue;
    float readValueTemp;

    InfoReader precisionValues = fileContainer.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("CFastCountValuesNew:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("CFastCountValuesNew:readInfos: precisionValues not read");

    this.precisionString = ((PrecisionValues) precisionValues)
        .getPrecisionRegex();
    int precition = ((PrecisionValues) precisionValues)
        .getMaxPrecision();
    long prec = (long) Math.pow(10, precition);

    int xSize = gisFile.getNCols();
    int ySize = gisFile.getNRows();
    int xSizeTemp = this.templateFileContainer.getGisFile().getNCols();
    float[] dData;// = new float[xSize];
    float[] dDataTemp;// = new float[xSizeTemp];
    for (int j = 0; j < ySize; j++) {
      dData = gisFile.readRaster(0, j, xSize, 1);
      dDataTemp = templateFileContainer.getGisFile().readRaster(0, j, xSizeTemp, 1);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        readValueTemp = dDataTemp[i];
        if (readValue == gisFile.getNoDataValue() || readValueTemp == this.templateFileContainer.getGisFile().getNoDataValue()) {
          this.noDataValueCount++;
        } else {
          countReadValues(readValue, prec, precition);
        }
      }
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    this.numOfCounts = this.countValues.mapValue.size();
    gisFile.unlock();
    this.used = true;
  }

  @Override
  public void run() {
    if (this.targetFile == null) {
      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(100.0F);
        this.workerStatus.setProgressName("ERROR: Target file == null");
        this.workerStatus.errorProcess();
      }
      if (this.dependencer != null)
        this.dependencer.errorRunnable(this);
    } else {
      try {
        this.readInfos(this.targetFile);
        IGisFileInformationStorage storage = this.targetFile
            .getGisFileInformationStorage();
        if (storage != null) {
          storage.refreshPluginExec(this);
          this.countValues.mapValue.clear();
        }
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.endProgress();
        }
        if (this.dependencer != null)
          this.dependencer.endRunnable(this);
      } catch (Exception e) {
        log.error("Error executing Count Values", e);
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.setProgressName(e.getMessage());
          this.workerStatus.errorProcess();
        }
        if (this.dependencer != null)
          this.dependencer.errorRunnable(this);
      }
    }
  }

  @Override
  public void setUsed(boolean used) {
    this.used = used;
    this.countValues.mapValue.clear();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("used", used);
    if (!used) {
      return;
    }

    if (this.targetFile != null && this.countValues.mapValue.isEmpty()) {
      if (saver instanceof DataVariableConverter) {
        saver.save(getCountValues());
      }
      return;
    }
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    saver.save("numOfCounts", this.numOfCounts);
    saver.save("noDataValueCount", noDataValueCount);
    saver.save("numOfData", numOfData);
    saver.save("precision", precisionString);
    saver.save(this.countValues);
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.numOfCounts = restorer.loadLong("numOfCounts")
        .orElse(0L);
    this.noDataValueCount = restorer.loadLong("noDataValueCount")
        .orElse(0L);
    this.numOfData = restorer.loadLong("numOfData")
        .orElse(0L);
    this.precisionString = restorer.loadString("precision")
        .orElse("0");
  }

  @Override
  public String getId() {
    return NAME;
  }
}

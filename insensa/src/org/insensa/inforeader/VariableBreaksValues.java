/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.ClassificationRange;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class VariableBreaksValues extends AbstractInfoReader implements BreaksValues {

  public final static String NAME = "VariableBreaksValues";
  private static final long serialVersionUID = -6982163965279953945L;
  private int numOfClasses = 5;
  private String precisionString = "0";
  private List<ClassificationRange> rangeList = new ArrayList<>();

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<>();
    depList.add(PrecisionValues.NAME);
    depList.add(CountValues.NAME);
    return depList;

  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("numOfClasses", numOfClasses);
    saver.save("precision", precisionString);

    if (isUsed()) {
      this.rangeList.forEach(classificationRange ->
          saver.save("Class", classificationRange));
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    if (isUsed()) {
      for (int i = 0; i < rangeList.size(); i++) {
        table.put("Break " + (i + 1) + " Low",
            precisionFormat.format(this.rangeList.get(i).getLowValue()));
        table.put("Break " + (i + 1) + " High",
            precisionFormat.format(this.rangeList.get(i).getHighValue()));
      }
    }
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (isUsed()) {
      List<IRestorableData> data = restorer.loadCollection("Class",
          ClassificationRange.class);
      this.rangeList = new ArrayList<>();
      data.forEach(iRestorableData ->
          rangeList.add((ClassificationRange) iRestorableData));
    }
  }

  public int getNumOfClasses() {
    return this.numOfClasses;
  }

  @Override
  public void setNumOfClasses(int numOfClasses) {
    this.numOfClasses = numOfClasses;
  }

  @Override
  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public void setRangeList(List<ClassificationRange> rangeList) {
    this.rangeList.clear();
    this.rangeList.addAll(rangeList);
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }

    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("VariableBreaksValues:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("VariableBreaksValues:readInfos: precisionValues not read");

    InfoReader countVal = file.getInfoReader(CountValues.NAME);
    if (countVal == null)
      throw new IOException("VariableBreaksValues:readInfos: no countValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("VariableBreaksValues:readInfos: countValues not read");

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();
    if (this.workerStatus != null)
      this.workerStatus.refreshPercentage(100.0f);

    this.used = true;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.gdal.gdal.Band;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;


public class CountIntervalValues extends AbstractInfoReader {

  public static final int PERCENTAGE = 0;
  public static final int VALUE = 1;
  private static final long serialVersionUID = 1246722104953542207L;
  private static String NAME = "countIntervalValues";
  private long count = 0;
  private String precisionString = "0";
  private float lowerBound = 0.0F;
  private float upperBound = 0.0F;
  private float lowerValue = 0.0F;
  private float upperValue = 0.0F;
  private int calcMode = PERCENTAGE;

  /**
   * @return
   */
  public int getCalcMode() {
    return this.calcMode;
  }

  /**
   * @param calcMode
   */
  public void setCalcMode(int calcMode) {
    this.calcMode = calcMode;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(MinMaxValues.NAME);
    depList.add(PrecisionValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (!this.isUsed()) {
      return;
    }
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    saver.save("count", this.count);
    saver.save("calcMode", this.calcMode);
    saver.save("lowerBound", precisionFormat.format(this.lowerBound));
    saver.save("upperBound", precisionFormat.format(this.upperBound));
    saver.save("lowerValue", precisionFormat.format(this.lowerValue));
    saver.save("upperValue", precisionFormat.format(this.upperValue));
  }


  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.count = restorer.loadInteger("count").get();
    this.lowerBound = restorer.loadFloat("lowerBound").get();
    this.upperBound = restorer.loadFloat("upperBound").get();
    this.lowerValue = restorer.loadFloat("lowerValue").get();
    this.upperValue = restorer.loadFloat("upperValue").get();
    calcMode = restorer.loadInteger("calcMode").orElse(0);
  }

  @Override
  public AttributeTable getData() {
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);
    AttributeTable table = new AttributeTable();
    table.put("Lower Bound (%)", precisionFormat.format(this.lowerBound * 100));
    table.put("Upper Bound (%)", precisionFormat.format(this.lowerBound * 100));
    table.put("Lower Value (%)", precisionFormat.format(this.lowerBound * 100));
    table.put("Upper Value (%)", precisionFormat.format(this.lowerBound * 100));
    table.put("Count", Long.toString(this.count));

    return table;
  }

  public float getLowerBound() {
    return this.lowerBound;
  }

  public void setLowerBound(float lowerBound) {
    this.lowerBound = lowerBound;
  }

  public float getLowerValue() {
    return this.lowerValue;
  }

  public void setLowerValue(float lowerValue) {
    this.lowerValue = lowerValue;
  }

  public float getUpperBound() {
    return this.upperBound;
  }

  public void setUpperBound(float upperBound) {
    this.upperBound = upperBound;
  }

  public float getUpperValue() {
    return this.upperValue;
  }

  public void setUpperValue(float upperValue) {
    this.upperValue = upperValue;
  }


  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    float factor = (float) (100.0 / ((RasterFile) file.getGisFile()).getNRows());
    float readValue;

    this.processStatus = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null) {
      this.dependencer.checkInfoDependencies(this);
    }

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("CCountIntervalValue:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("CCountIntervalValue:readInfos: precisionValues not read");

    InfoReader minMaxValues = file.getInfoReader(MinMaxValues.NAME);
    if (minMaxValues == null)
      throw new IOException("CCountIntervalValue:readInfos: no minMaxValues InfoReader Found");
    if (!minMaxValues.isUsed())
      throw new IOException("CCountIntervalValue:readInfos: minMaxValues not read");

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    float minValue = ((MinMaxValues) minMaxValues).getMinValue();
    float maxValue = ((MinMaxValues) minMaxValues).getMaxValue();
    float range = maxValue - minValue;

    if (this.calcMode == PERCENTAGE) {
      this.lowerValue = (this.lowerBound * range) + minValue;
      this.upperValue = (this.upperBound * range) + minValue;
    } else {
      this.lowerBound = (this.lowerValue - minValue) / range;
      this.upperBound = (this.upperValue - minValue) / range;
    }

    // Dataset ds=file.getDataset();//gdal.Open(file.getAbsolutePath());
    Band band = ((RasterFile) file.getGisFile()).getBand(RasterFileAccess.READ_ONLY);// ds.GetRasterBand(1);

    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (float element : dData) {
        readValue = element;
        if (readValue == ((RasterFile) file.getGisFile()).getNoDataValue()) {
        } else {
          if (readValue >= this.lowerValue && readValue <= this.upperValue) {
            if (this.count == Long.MAX_VALUE) {
              throw new IOException("Num of found values is higher then Long-Max");
            }
            this.count++;
          }
        }
      }
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    file.getGisFile().unlock();
    this.used = true;
  }

  @Override
  public void setUsed(boolean used) {
    if (!used) {
      super.setUsed(false);
      this.count = 0;
    }
  }

}

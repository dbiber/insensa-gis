/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.ClassificationRange;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * Creates Quantile Breaks for a raster based dataset There are 9 possible
 * Calculation methods for Breaks based on the article from XXXXX. Default is 8;
 * If a breaks has the same Value as the last Breaks, this breaks shifts right.
 * If this shift is not possible, an Exception will be thrown
 *
 * @author Dennis Biber
 */
public class QuantileBreaksValues extends AbstractInfoReader implements BreaksValues {


  public static final String QUANTILE_BREAKS_VALUES = "quantileBreaksValues";
  private static final long serialVersionUID = -6926801444118925764L;
  public List<InfoReader> iReaderList = new ArrayList<InfoReader>();
  public List<InfoReader> iReaderListDep = new ArrayList<InfoReader>();
  private String precisionString = "0.#";
  private int numOfClasses = 5;
  private int type = 8;
  private List<ClassificationRange> rangeList = new ArrayList<ClassificationRange>();

  private Float[] getBreakValues(CountValues countValues, int numOfClasses) throws IOException {

    double q = numOfClasses;

    //real value index
    double[] h = new double[numOfClasses];
    //the nth percentile value (e.g. 0.5 for the 0.5 quartile (median))
    //so the percentage values
    double[] p = new double[numOfClasses];
    //The quatile value of
    Float[] Qp = new Float[numOfClasses];
    //The number of all values with data
    long N = countValues.getNumOfData();
    TreeMap<Float, Long> dataMap = countValues
        .getCountValues().mapValue;
    switch (this.type) {
      case 1: {
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = (p[i]) * N + 0.5;
          if (p[i] == 0)
            Qp[i] = dataMap.firstKey();
          else
            Qp[i] = this.getNthData(dataMap, (long) Math.ceil(h[i] - 0.5));
        }
        return Qp;
      }
      case 2: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = (p[i]) * N + 0.5;
          data1 = this.getNthData(dataMap, (long) Math.ceil(h[i] - 0.5));
          data2 = this.getNthData(dataMap, (long) Math.floor(h[i] - 0.5));
          if (p[i] == 0)
            Qp[i] = dataMap.firstKey();
          else if (p[i] == 1.0)
            Qp[i] = dataMap.lastKey();
          else {
            Qp[i] = new Float((data1 + data2) / 2);
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      case 3: {
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = (p[i]) * N;
          if (p[i] <= (0.5 / N))
            Qp[i] = dataMap.firstKey();
          else
            Qp[i] = this.getNthData(dataMap, new Long(Math.round(h[i])).intValue());
        }
        return Qp;
      }
      case 4: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = (p[i]) * N;

          if (p[i] < (1.0 / N)) {
            Qp[i] = dataMap.firstKey();
          } else if (p[i] == 1.0) {
            Qp[i] = dataMap.lastKey();
          } else {
            data1 = this.getNthData(dataMap, (long) Math.floor(h[i]));
            data2 = this.getNthData(dataMap, ((long) Math.floor(h[i])) + 1);
            Qp[i] = new Float(data1 + (h[i] - Math.floor(h[i])) * (data2 - data1));
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      case 5: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = ((p[i]) * N) + 0.5;
          if (p[i] < (0.5 / N)) {
            Qp[i] = dataMap.firstKey();
          } else if (p[i] >= ((N - 0.5) / N)) {
            Qp[i] = dataMap.lastKey();
          } else {
            data1 = this.getNthData(dataMap, (long) Math.floor(h[i]));
            data2 = this.getNthData(dataMap, ((long) Math.floor(h[i])) + 1);
            Qp[i] = new Float(data1 + (h[i] - Math.floor(h[i])) * (data2 - data1));
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      case 6: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = ((p[i]) * (N + 1));

          if (p[i] < (1.0 / (N + 1.0))) {
            Qp[i] = dataMap.firstKey();
          } else if (p[i] >= N / (N + 1.0)) {
            Qp[i] = dataMap.lastKey();
          } else {
            data1 = this.getNthData(dataMap, (long) Math.floor(h[i]));
            data2 = this.getNthData(dataMap, ((long) Math.floor(h[i])) + 1);
            Qp[i] = new Float(data1 + (h[i] - Math.floor(h[i])) * (data2 - data1));
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      case 7: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = ((N - 1) * p[i]) + 1;

          if (p[i] == 1.0) {
            Qp[i] = dataMap.lastKey();
          } else {
            data1 = this.getNthData(dataMap, (long) Math.floor(h[i]));
            data2 = this.getNthData(dataMap, ((long) Math.floor(h[i])) + 1);
            Qp[i] = new Float(data1 + (h[i] - Math.floor(h[i])) * (data2 - data1));
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      case 8: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = ((N + (1.0 / 3.0)) * p[i]) + (1.0 / 3.0);

          if (p[i] < (2.0 / 3.0) / (N + 1.0 / 3.0)) {
            Qp[i] = dataMap.firstKey();
          } else if (p[i] >= (N - 1.0 / 3.0) / (N + 1.0 / 3.0)) {
            Qp[i] = dataMap.lastKey();
          } else {
            data1 = this.getNthData(dataMap, (long) Math.floor(h[i]));
            data2 = this.getNthData(dataMap, ((long) Math.floor(h[i])) + 1);
            Qp[i] = new Float(data1 + (h[i] - Math.floor(h[i])) * (data2 - data1));
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      case 9: {
        Float data1;
        Float data2;
        for (int i = 0; i < q; i++) {
          p[i] = (i + 1) / q;
          h[i] = ((N + (1.0 / 4.0)) * p[i]) + (3.0 / 8.0);

          if (p[i] < (5.0 / 8.0) / (N + 1.0 / 4.0)) {
            Qp[i] = dataMap.firstKey();
          } else if (p[i] >= (N - 3.0 / 8.0) / (N + 1.0 / 4.0)) {
            Qp[i] = dataMap.lastKey();
          } else {
            data1 = this.getNthData(dataMap, (long) Math.floor(h[i]));
            data2 = this.getNthData(dataMap, ((long) Math.floor(h[i])) + 1);
            Qp[i] = new Float(data1 + (h[i] - Math.floor(h[i])) * (data2 - data1));
            Qp[i] = this.getNearestData(Qp[i], dataMap);
          }
        }
        return Qp;
      }
      default:
        break;
    }
    return null;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(CountValues.NAME);
    depList.add(PrecisionValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("numOfClasses", numOfClasses);
    saver.save("type", type);

    if (isUsed()) {
      this.rangeList.forEach(classificationRange ->
          saver.save("Class", classificationRange));
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    DecimalFormat precisionFormat = new DecimalFormat(this.precisionString);
    DecimalFormatSymbols dSymb = precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    precisionFormat.setDecimalFormatSymbols(dSymb);
    precisionFormat.setRoundingMode(RoundingMode.HALF_UP);

    if (isUsed()) {
      for (int i = 0; i < rangeList.size(); i++) {
        table.put("Break " + (i + 1) + " Low",
            precisionFormat.format(this.rangeList.get(i).getLowValue()));
        table.put("Break " + (i + 1) + " High",
            precisionFormat.format(this.rangeList.get(i).getHighValue()));
      }
    }
    return table;
  }


  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (isUsed()) {
      List<IRestorableData> data = restorer.loadCollection("Class",
          ClassificationRange.class);
      this.rangeList = new ArrayList<>();
      data.forEach(iRestorableData ->
          rangeList.add((ClassificationRange) iRestorableData));
    }
  }

  private Float getNearestData(Float data, TreeMap<Float, Long> dataMap) {

    if (dataMap.get(data) == null) {
      Iterator<Float> fIter = dataMap.keySet().iterator();
      Float lowestDifValue = Float.MAX_VALUE;
      Float foundValue = 0.0F;
      while (fIter.hasNext()) {
        Float value = fIter.next();
        if (Math.abs(value - data) < lowestDifValue) {
          lowestDifValue = Math.abs(value - data);
          foundValue = value;
        } else if (Math.abs(value - data) > lowestDifValue) {
          break;
        }
      }
      return foundValue;
    } else
      return data;
  }

  private Float getNthData(Map<Float, Long> dataMap, long index) throws IOException {
    long tmpCounter = 0;
    Iterator<Float> iter = dataMap.keySet().iterator();
    Float val;
    while (iter.hasNext()) {
      val = iter.next();
      tmpCounter += dataMap.get(val);
      if (index <= tmpCounter)
        return val;
    }
    throw new IOException("Could not find data for index " + index + ", counter is: " + tmpCounter);
  }

  public int getNumOfClasses() {
    return this.numOfClasses;
  }

  public void setNumOfClasses(int numOfClasses) {
    this.numOfClasses = numOfClasses;
  }

  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public int getType() {
    return this.type;
  }

  public void setType(int type) {
    this.type = type;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws FileNotFoundException, IOException {

    this.processStatus = 0.0F;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }

    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("QuantileBreaksValues:readInfos: no countValues InfoReader Found");
    if (!countValues.isUsed())
      throw new IOException("QuantileBreaksValues:readInfos: countValues not read");

    InfoReader precisionValues = file.getInfoReader(PrecisionValues.NAME);
    if (precisionValues == null)
      throw new IOException("QuantileBreaksValues:readInfos: no precisionValues InfoReader Found");
    if (!precisionValues.isUsed())
      throw new IOException("QuantileBreaksValues:readInfos: precisionValues not read");

    if (((CountValues) countValues).getCountValues().size() < this.numOfClasses)
      throw new IOException("Number of different data " + ((CountValues) countValues).getCountValues().size() + "<" + this.numOfClasses);

    this.precisionString = ((PrecisionValues) precisionValues).getPrecisionRegex();

    TreeMap<Float, Long> dataMap = ((CountValues) countValues).getCountValues()
        .mapValue;

    ClassificationRange classRange;
    Float[] breakValues = new Float[this.numOfClasses];

    breakValues = this.getBreakValues((CountValues) countValues, this.numOfClasses);

    for (int i = 1; i < breakValues.length; i++) {
      if (breakValues[i].equals(breakValues[i - 1])) {
        Float newValue = dataMap.higherKey(breakValues[i]);
        if (newValue == null) {
          throw new IOException("Can't calculate Quantile-Breaks");
        } else
          breakValues[i] = newValue;
      }
    }

    for (int j = 0; j < breakValues.length; j++) {
      if (j == 0) {
        classRange = new ClassificationRange(dataMap.firstKey(), breakValues[j]);
        this.rangeList.add(classRange);
      } else {
        classRange = new ClassificationRange(breakValues[j - 1], breakValues[j]);
        this.rangeList.add(classRange);
      }

    }
    this.used = true;
  }

  @Override
  public void setUsed(boolean used) {
    if (used) {
      this.rangeList.clear();
      this.used = false;
    }
  }

}

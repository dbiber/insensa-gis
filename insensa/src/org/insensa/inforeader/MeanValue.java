/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.inforeader;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class MeanValue extends AbstractInfoReader// implements InfoReader
{

  public static final String NAME = "MeanValue";
  private static final long serialVersionUID = 1730201103757696250L;
  private double meanValue = 0;
  private long numOfValues = 0;
  private String precisionString = "0";

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(CountValues.NAME);
    return depList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (!isUsed()) {
      return;
    }
    saver.save("arithMeanValue", saver1 -> {
      saver1.save("value", meanValue);
    });
  }

  @Override
  public AttributeTable getData() {
    AttributeTable table = new AttributeTable();
    if (!isUsed()) {
      return table;
    }
    table.put("Mean Value", Double.toString(this.meanValue));
    return table;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (!isUsed()) {
      return;
    }
    restorer.loadRestorable("arithMeanValue", restorer1 -> {
      meanValue = restorer1.loadDouble("value").get();
    });
  }

  public double getMeanValue() {
    return this.meanValue;
  }

  public long getNumOfValues() {
    return this.numOfValues;
  }

  @Override
  public void readInfos(IGisFileContainer file) throws IOException {
    float factor;
    long valueCount;

    Float readValue;
    Long cnt;
    double tmpMeanValue = 0;

    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.getId());
    }
    if (this.dependencer != null)
      this.dependencer.checkInfoDependencies(this);

    InfoReader countValues = file.getInfoReader(CountValues.NAME);
    if (countValues == null)
      throw new IOException("CMeanValueNew:readInfos: no countValues InfoReader Found");
    if (!countValues.isUsed())
      throw new IOException("CMeanValueNew:readInfos: countValues not read");

    valueCount = ((CountValues) countValues).getNumOfData();
    this.numOfValues = valueCount;

    Map<Float, Long> countMap = ((CountValues) countValues).getCountValues().getMap();
    factor = (float) (100.0 / countMap.size());
    Iterator<Float> iter = countMap.keySet().iterator();
    while (iter.hasNext()) {
      readValue = iter.next();
      cnt = countMap.get(readValue);
      tmpMeanValue += ((readValue * cnt) / valueCount);
      this.processStatus += factor;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }

    this.meanValue = tmpMeanValue;
    this.used = true;
  }
}

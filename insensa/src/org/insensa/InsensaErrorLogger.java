package org.insensa;

import org.insensa.helpers.IOHelper;

import java.io.File;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class InsensaErrorLogger extends FileHandler {

  public static class LoggerState {

    private static LoggerState instance = new LoggerState();
    private boolean logged = false;

    private LoggerState() {
    }

    public static LoggerState getInstance() {
      return instance;
    }

    public boolean isLogged() {
      return logged;
    }

    public void setLogged(boolean logged) {
      this.logged = logged;
    }
  }


  public InsensaErrorLogger(String pattern) throws IOException, SecurityException {
    super(pattern);
  }

  public InsensaErrorLogger(String pattern, int limit, int count, boolean append) throws IOException, SecurityException {
    super(pattern, limit, count, append);
  }

  public static boolean isLogged() {
    return LoggerState.getInstance().isLogged();
  }

  public static void config() {
    try {
      Logger logger = Logger.getLogger("");

      Handler[] handlers = logger.getHandlers();
      for (Handler iHandler : handlers) {
        logger.removeHandler(iHandler);
      }

      String logPath = IOHelper.getInstance().getErrorLoggerFile().getParent()
          + File.separator;
      logPath += "errorlog_%g.log";
      InsensaErrorLogger handler = new InsensaErrorLogger(logPath,
          10000000,20,false);

      handler.setFormatter(new SimpleFormatter());

      logger.addHandler(handler);
      logger.setLevel(Level.INFO);


      ConsoleHandler cHandler = new ConsoleHandler();
      cHandler.setLevel(Level.INFO);
      handler.setFormatter(new SimpleFormatter());
      logger.addHandler(cHandler);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public synchronized void publish(LogRecord record) {
    super.publish(record);
    if (record.getLevel() == Level.SEVERE) {
      LoggerState.getInstance().setLogged(true);
    }
  }

}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.gdal.ogr.DataSource;
import org.gdal.ogr.Driver;
import org.gdal.ogr.Layer;
import org.gdal.ogr.ogr;
import org.insensa.exceptions.GisFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ShapeFile implements IGisFile {

  private static final Logger log = LoggerFactory.getLogger(ShapeFile.class);


  private DataSource dataSource;
  private File fileRef;
  private Driver driver;
  private Layer layer;


  public ShapeFile(String fileRef) {
    this.fileRef = new File(fileRef);
    if (this.exists()) {
      this.createVectorInformation();
    }
  }

  public ShapeFile(ShapeFile oldFile, String newFullFileName) throws IOException {
    this.fileRef = new File(newFullFileName);
    if (this.exists()) {
      this.createVectorInformation();
    } else {
      //TODO copy from old
    }

  }

  public ShapeFile(ShapeFile oldFile) throws IOException {
    this(oldFile, oldFile.getAbsolutePath());
  }

  private void createVectorInformation() {
    String name = this.getAbsolutePath();
    if (this.exists()) {

      this.dataSource = ogr.Open(this.getAbsolutePath(),
          RasterFileAccess.READ_ONLY.access());
      if (this.dataSource == null) {
        throw new GisFileException("ShapeFile:createVectorInformation: unable to" +
            " create Dataset for File: " + name);
      }
      this.driver = this.dataSource.GetDriver();
      if (this.driver == null)
        throw new GisFileException("ShapeFile:createVectorInformation: unable to" +
            " create Driver for File: " + name);
      int layers = this.dataSource.GetLayerCount();
      log.info("Num of Layer: " + layers);
      this.layer = this.dataSource.GetLayer(0);
    }
  }

  @Override
  public boolean exists() {
    return fileRef.exists();
  }

  @Override
  public GisFileType getType() {
    return GisFileType.SHAPE;
  }

  @Override
  public boolean delete() {
    this.unlock();
    return this.fileRef.delete();
  }

  @Override
  public String getAbsolutePath() {
    return fileRef.getAbsolutePath();
  }

  @Override
  public File getFileRef() {
    return fileRef;
  }

  @Override
  public String getName() {
    return fileRef.getName();
  }

  @Override
  public void relocateFile(String fullPath) {
    this.fileRef = new File(fullPath);

  }

  private String getNameWithoutExtension(String name) {
    return name.replaceFirst("[.][^.]+$", "");
  }

  private boolean renameBaseFile(File path, String prefix, String suffix, boolean optionalFile) throws IOException {
    File newDest = new File(path, prefix + "." + suffix);
    File oldFile = new File(this.fileRef.getParent(), prefix + "." + suffix);
    if (newDest.exists()) {
      boolean rename = oldFile.renameTo(newDest);
      if (!rename) {
        throw new IOException("rename from " + oldFile.getAbsolutePath() + " to "
            + newDest.getAbsolutePath() + " failed");
      }
    } else if (!optionalFile) {
      throw new IOException(oldFile.getAbsolutePath() + " does not exist");
    }
    return false;
  }

  @Override
  public boolean renameTo(File dest) {
    this.unlock();
    String nameWithoutExt = getNameWithoutExtension(fileRef.getName());
    try {
      renameBaseFile(dest.getParentFile(), nameWithoutExt, "shp", false);
      this.fileRef = dest;
      renameBaseFile(dest.getParentFile(), nameWithoutExt, "dbf", false);
      renameBaseFile(dest.getParentFile(), nameWithoutExt, "shx", false);
    } catch (IOException e) {
      throw new GisFileException("Error renaming file:", e);
    }
    return true;
  }

  @Override
  public void unlock() {

  }

  @Override
  public void refresh() {

  }

  public String getDriverShortName() {
    return this.driver.GetName();
  }

  @Override
  public GisAttributes getGisAttributes() {
    GisAttributes gisAttributes = new GisAttributes();
    gisAttributes.add("Driver Name", this.driver.GetName());
    gisAttributes.add("Layer Name", layer.GetName());
//    layer.ResetReading();
//    Feature feature;
//    while((feature = layer.GetNextFeature()) != null) {
//      FeatureDefn featureDefn = feature.GetDefnRef();
//      for (int i=0 ; i < featureDefn.GetFieldCount(); i++) {
//        FieldDefn fieldDefn = featureDefn.GetFieldDefn(i);
//        if (fieldDefn.GetFieldType() == ogrConstants.OFTInteger) {
//          gisAttributes.add(fieldDefn.GetName(),Integer.toString(feature.GetFieldAsInteger(i)));
//        } else if (fieldDefn.GetFieldType() == ogrConstants.OFTReal) {
//          gisAttributes.add(fieldDefn.GetName(),Double.toString(feature.GetFieldAsDouble(i)));
//        } else if (fieldDefn.GetFieldType() == ogrConstants.OFTString) {
//          gisAttributes.add(fieldDefn.GetName(),feature.GetFieldAsString(i));
//        } else {
//          gisAttributes.add(fieldDefn.GetName(),feature.GetFieldAsString(i));
//        }
//      }
//    }
    return gisAttributes;
  }
}

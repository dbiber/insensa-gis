/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.OptionThreadPool;
import org.insensa.properties.IGisFileInformationStorage;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface IGisFileContainer extends FileInspector {

  IGisFile getGisFile();

  void addFileObserver(FileObserver fileObserver);

  /**
   * Add the info {@link InfoReader} Object to this File.
   *
   * @return true if the new InfoReader could be added, false otherwise
   */
  boolean addInfoReader(InfoReader info, boolean serialize) throws IOException;

  void addInfoReaders(List<InfoReader> infos, boolean serialize) throws IOException;

  void addOption(OptionFileChanger option) throws IOException;

  void changeOutputFileName(String newOutputFileName) throws IOException;

  void changeOutputFilePath(String outputFilePath) throws IOException;

  boolean deleteFile();

  boolean deleteLink();

  void executeAllInfos(WorkerStatusList workerStatusList) throws IOException;

  void executeAllOptions(WorkerStatusList workerStatusList) throws IOException, JDOMException;

  void executeInfo(String infoReaderName, WorkerStatusList workerStatusList) throws IOException;

  void executeInfos(List<String> infoReaderList, WorkerStatusList workerStatusList)
      throws IOException;

  ConnectionFileChanger getConnectionFileChanger();

  void setConnectionFileChanger(ConnectionFileChanger conn) throws IOException;

  void addInfoConnection(InfoConnection infoConnection) throws IOException;

  List<InfoConnection> getInfoConnections();

  ExecDependencyController getDepChecker();

  String getDescription();

  void setDescription(String description) throws IOException;

  List<FileObserver> getFileObservers();

  InfoReader getInfoReader(String infoReaderName);

  List<InfoReader> getInfoReaders();

  List<OptionFileChanger> getOptions();

  OptionThreadPool getOptionThreadPool();

  /**
   * the absolute path TO the output directory of this container.
   * (the directory WITHIN the insensa workspace/project).<br>
   * <ul>Examples:
   * <li>/home/myhome/workspace/project1/fileset2/</li>
   * <li>C:/Users/Documents/workspace/project/fileset2/</li></ul>
   *
   * In Other Words, the ABSOLUTE file path of the output Directory
   * (not necessarily the original file) WITHOUT the file name !
   */
  String getAbsOutputDirectoryPath();

  /**
   * The filename displayed within the Software
   * (Without path, with extension).
   * If the file is just a link instead of a hard copy,
   * this could be different from the original file.
   */
  String getOutputFileName();

  /**
   * Path of this Container relative to the project directory.
   */
  String getPathRelativeToProject();

  void setOutputFileName(String outputFileName) throws IOException;

  ParentModule getParentModule();

  void setParentModule(ParentModule parentModule);

  IGisFileContainer getTargetGisFileContainer();

  void setTargetGisFileContainer(IGisFileContainer targetGisFileContainer);

  int getUnusedInfoReaderCount();

  int getUnusedOptionCount();

  IGisFileInformationStorage getGisFileInformationStorage();

  void setGisFileInformationStorage(IGisFileInformationStorage xmlInformation);

  /**
   * If the file does not link to an external file and has a real physical presence
   */
  boolean isHardCopy();

  void notifyObserversDelete();

  void relocateFile();

  void removeFileObserver(FileObserver fileObserver);

  void removeInfoReader(InfoReader infoReader) throws IOException;

  void removeInfoConnection(InfoConnection infoConnection) throws IOException;

  void removeOptionFileChanger(OptionFileChanger option) throws IOException;

  String getName();

  boolean delete();

  String getAbsolutePath();

  boolean renameTo(File dest) throws IOException;

  boolean isConnection();

  boolean isStack();

//  boolean createNewFile(IGisFileContainer tmpFile2, int gdt_float32, double v);
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.insensa.XMLProperties.XmlProjectProperties;
import org.insensa.connections.CIndexation;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.StackFilesThreadPools;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.SystemSpec;
import org.insensa.helpers.VersionHelper;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.model.storage.ISavableRestorer;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Project implements ParentModule {
  private static final Logger log = LoggerFactory.getLogger(Project.class);


  private String name;
  private String owner;
  private Date date;
  private XmlProjectProperties projectProperties;
  private List<IGisFileSet> fileSetList = new ArrayList<>();
  private IGisFileSet activeFileSet = null;
  private int activeFileSetNumber;
  private StackFilesThreadPools connectionThreadPool = new StackFilesThreadPools();
  private List<ConnectionFileChanger> connectionFileChanger = new ArrayList<>();
  private List<IProjectListener> listener = new ArrayList<>();
  private boolean open = false;
  private int fileCount = 0;
  private User user;
  private String version;

  /**
   * Empty Project.
   */
  public Project(String name, String owner, List<IProjectListener> listener) {
    this.version = VersionHelper.getInstance().getVersion();
    this.name = name;
    this.owner = owner;
    this.date = new Date(System.currentTimeMillis());
    if (listener != null) {
      this.listener.addAll(listener);
    }
  }

  public void addConnection(ConnectionFileChanger conn) {
    if (this.connectionFileChanger.contains(conn)) {
      log.warn("Connection {} already exists", conn.getId());
    } else {
      this.connectionFileChanger.add(conn);
    }
    if (this.connectionThreadPool.getRunnableList().contains(conn)) {
      log.warn("Connection {} already exists in thread pool", conn.getId());
    } else if (!conn.isUsed()) {
      List<InfoConnection> infoConnections = conn.getOutputFileContainer().getInfoConnections();
      for (InfoConnection infoConnection : infoConnections) {
        this.connectionThreadPool.addPluginExec(infoConnection);
      }
      this.connectionThreadPool.addPluginExec(conn);
    }
  }

  public void addFileSet(GenericGisFileSet fileSet) throws IOException {
    if (!this.open) {
      throw new IOException("Project is closed");
    }
    this.fileSetList.add(fileSet);
    fileSet.setParentModule(this);
    this.activeFileSetNumber = this.fileSetList.size() - 1;
    this.activeFileSet = this.fileSetList.get(this.activeFileSetNumber);
    this.projectProperties.addFileSet(fileSet.getName());
  }

  public void addFileSet(String name) throws IOException, JDOMException, GisFileException {
    if (!this.open) {
      throw new IOException("Project is closed");
    }
    GenericGisFileSet tmpSet = new GenericGisFileSet(this, name, null);
    this.fileSetList.add(tmpSet);
    tmpSet.setParentModule(this);
    this.activeFileSetNumber = this.fileSetList.size() - 1;
    this.activeFileSet = this.fileSetList.get(this.activeFileSetNumber);
    this.projectProperties.addFileSet(tmpSet.getName());
  }

  public void addProjectListener(IProjectListener listener) {
    if (!this.listener.contains(listener)) {
      this.listener.add(listener);
    }

  }


  public void close() {
    for (IProjectListener listener : this.listener) {
      listener.startClosingProject(this.name);
    }
    if (this.projectProperties != null && this.listener.contains(this.projectProperties)) {
      this.listener.remove(this.projectProperties);
    }
    this.projectProperties = null;
    if (this.activeFileSet != null) {
      this.activeFileSet = null;
    }
    this.activeFileSetNumber = -1;
    for (IGisFileSet fileSet : this.fileSetList) {
      fileSet.closeIter();
    }
    if (!(this.fileSetList.isEmpty())) {
      this.fileSetList.clear();
    }
    for (IProjectListener listener : this.listener) {
      listener.projectClosed(this.name);
    }
  }

  public boolean create() throws IOException {
    if (!this.createProjectDirs() || this.exists()) {
      throw new IOException("Project: create: Error Create Dirs or to Create New Project");
    }
    this.projectProperties = new XmlProjectProperties(this.getWorkspacePath()
        + this.getName() + File.separator);
    this.projectProperties.setVersion(this.version);
    this.listener.add(this.projectProperties);
    this.open = true;
    for (IProjectListener listener : this.listener) {
      listener.projectOpened(this.name);
    }
    return true;
  }

  private boolean createProjectDirs() throws IOException {
    File pathDir = new File(this.getWorkspacePath() + this.name);
    File fileSetDir = new File(this.getWorkspacePath() + this.name
        + File.separator + "FileSets");
    File modulationSetsDir = new File(this.getWorkspacePath()
        + this.name + File.separator + "ModulationSets");
    if (pathDir.exists()) {
      return false;
    } else {
      if (!pathDir.mkdirs()) {
        throw new IOException("Can't create Project Directory");
      }
      if (!fileSetDir.mkdir()) {
        throw new IOException("Can't create Fileset Directory");
      }
      if (!modulationSetsDir.mkdir()) {
        throw new IOException("Can't create ModulationSets Directory");
      }
      return true;
    }
  }

  public void executeAllConnections(WorkerStatusList workerList) throws IOException {
    this.connectionThreadPool.setWorkerList(workerList);
    this.connectionThreadPool.executeThreads();
  }

  public boolean exists() {
    return new File(this.getWorkspacePath() + this.getName()
        + File.separator + "settings.xml").exists();
  }

  @Override
  public void fireChildFileSetRenamed(GenericGisFileSet fileSet, String oldName, String newName)
      throws IOException {
    log.debug("Project: tell listener childFileSetRenamed");
    for (IProjectListener projectListener : this.listener) {
      projectListener.childFileSetRenamed(fileSet, oldName, newName);
    }
  }

  public IGisFileSet getActiveFileSetRoot() {
    return this.activeFileSet;
  }

  public int getActiveFileSetRootNumber() {
    return this.activeFileSetNumber;
  }

  public StackFilesThreadPools getConnectionThreadPool() {
    return this.connectionThreadPool;
  }

  public Date getDate() {
    return this.date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public IGisFileContainer getFileByRelativePath(String relativefilePath) {
    int lastIndex = relativefilePath.lastIndexOf(SystemSpec.separator);
    String path = relativefilePath.substring(0, lastIndex + 1);
    String name = relativefilePath.substring(lastIndex + 1);
    return getFileByRelativePath(path, name);
  }

  public IGisFileContainer getFileByRelativePath(String fileSetPath, String fileName) {
    IGisFileSet foundSet = null;
    for (IGisFileSet gisFileSet : this.fileSetList) {
      if (gisFileSet.getPathRelativeToProject().equals(fileSetPath)) {
        foundSet = gisFileSet;
        break;
      } else if (fileSetPath.startsWith(gisFileSet.getPathRelativeToProject())) {
        foundSet = this.getFileSetByPath(gisFileSet, fileSetPath);
        break;
      }
    }
    if (foundSet == null) {
      return null;
    }
    for (IGisFileContainer fileContainer : foundSet.getFileList()) {
      if (fileContainer.getName().equals(fileName)) {
        return fileContainer;
      }
    }
    return null;
  }

  public int getFileCount() {
    return this.fileCount;
  }

  public void setFileCount(int fileCount) {
    this.fileCount = fileCount;
  }

  public IGisFileSet getFileSet(int index) {
    return this.fileSetList.get(index);
  }

  public IGisFileSet getFileSetByPath(IGisFileSet parentSet, String relativePath) {
    List<IGisFileSet> fileSets;
    if (parentSet == null) {
      fileSets = getProject().getFileSetList();
    } else {
      fileSets = parentSet.getChildSets();
    }
    for (IGisFileSet fileSet : fileSets) {
      if (fileSet.getPathRelativeToProject().equals(relativePath)) {
        return fileSet;
      } else if (relativePath.startsWith(fileSet.getPathRelativeToProject())) {
        return this.getFileSetByPath(fileSet, relativePath);
      }
    }
    return null;
  }


  public IGisFileSet getFileSetByPath(String relativePath) {
    return getFileSetByPath(null, relativePath);
  }

  public List<IGisFileSet> getFileSetList() {
    return this.fileSetList;
  }

  @Override
  public String getFullPath() {
    return this.getWorkspacePath() + this.name + File.separator;
  }

  public String getPathRelativeToProject(String absolutePath) {
    return SystemSpec.separator
        + (absolutePath.replace(getProjectPath(), ""));
  }

  public boolean isRelativeToProject(String absolutePath) {
    return absolutePath.contains(getProjectPath());
  }

  public IGisFileSet getLastActiveFileSet() {
    if (this.activeFileSet != null) {
      return this.activeFileSet.getLastActiveFileSet();
    } else {
      return null;
    }
  }

  public String getName() {
    return this.name;
  }

  public String getOwner() {
    return this.owner;
  }

  @Override
  public Project getProject() {
    return this;
  }

  @Override
  public String getProjectPath() {
    return this.getFullPath();
  }

  public XmlProjectProperties getProjectProperties() {
    return this.projectProperties;
  }

  public void setProjectProperties(XmlProjectProperties projectProperties) {
    if (this.projectProperties != null && this.listener.contains(this.projectProperties)) {
      this.listener.remove(this.projectProperties);
    }
    this.projectProperties = projectProperties;
    this.listener.add(this.projectProperties);
  }

  @Override
  public String getPathRelativeToProject() {
    return "/";
  }

  public User getUser() {
    return this.user;
  }

  public void setUser(User user) {
    this.user = user;
    this.owner = user.getName();
  }

  @Override
  public String getWorkspacePath() {
    return this.user.getWorkspacePath();
  }

  public boolean open(WorkerStatus workerStatus)
      throws IOException, JDOMException {
    if (workerStatus != null) {
      workerStatus.startProcess();
    }
    GenericGisFileSet tmpFileSet;
    if (this.exists()) {
      this.projectProperties = new XmlProjectProperties(this.getWorkspacePath()
          + this.getName() + File.separator);
      this.listener.add(this.projectProperties);
      for (Object fileSetElement : this.projectProperties.getRasterFileSets()) {
        String nameAttribute = ((Element) fileSetElement).getAttributeValue("name");
        tmpFileSet = new GenericGisFileSet(this, nameAttribute, workerStatus);
        this.fileSetList.add(tmpFileSet);
        tmpFileSet.setParentModule(this);
        this.activeFileSetNumber = this.fileSetList.size() - 1;
        this.activeFileSet = this.fileSetList.get(this.activeFileSetNumber);
      }
      this.solveConnectionDependencies();
      this.open = true;

      if (workerStatus != null) {
        workerStatus.refreshPercentage(100.0f);
        workerStatus.endProgress();
      }
      for (IProjectListener listener : this.listener) {
        listener.projectOpened(this.name);
      }
      return true;
    }
    if (workerStatus != null) {
      workerStatus.refreshPercentage(100.0f);
      workerStatus.endProgress();
    }
    return false;
  }

  public void removeConnection(ConnectionFileChanger conn) throws IOException {
    this.connectionThreadPool.removeConnection(conn);
  }

  public void removeFileSet(GenericGisFileSet fileSet) throws IOException {
    if (this.activeFileSet == fileSet) {
      this.activeFileSet = null;
    }
    fileSet.delete();
    this.projectProperties.removeRasterFileSet(fileSet.getName());
    this.fileSetList.remove(fileSet);

  }

  public void setActiveFileSet(GenericGisFileSet fileSet) {
    int fileSetIndex = this.fileSetList.indexOf(fileSet);
    if (fileSetIndex >= 0) {
      this.setActiveFileSet(fileSetIndex);
    } else {
      for (int i = 0; i < this.fileSetList.size(); i++) {
        fileSetIndex = this.fileSetList.get(i).indexOfIter(fileSet);
        if (fileSetIndex >= 0) {
          this.setActiveFileSet(i);
        }
      }
    }
  }

  public void setActiveFileSet(int activeFileSetIndex) {
    this.activeFileSetNumber = activeFileSetIndex;
    this.activeFileSet = this.fileSetList.get(activeFileSetIndex);
  }

  private void solveInfoConnectionDependencies(IGisFileContainer targetFile,
                                               List<IGisFileContainer> sourceFiles) {

    for (InfoConnection infoConnection : targetFile.getInfoConnections()) {
      if (targetFile.getConnectionFileChanger() != null && targetFile.getConnectionFileChanger().isUsed()) {
//      if (infoConnection.isUsed()) {
        continue;
      }

      ISavableRestorer restorer = targetFile.getGisFileInformationStorage().getRestorer();
      infoConnection.setSourceFileList(sourceFiles);
      restorer.loadRestorable("infoConnections", restorer1 -> {
        restorer.loadRestorable(infoConnection.getId(), restorer2 -> {
          infoConnection.restore(restorer);
        });
      });
    }
  }

  private void solveConnectionDependencies() {
    GisFileContainerFactory factory = new GisFileContainerFactory();

    for (ConnectionFileChanger connectionFileChanger : connectionFileChanger) {
      ISavableRestorer restorer = connectionFileChanger.getOutputFileContainer()
          .getGisFileInformationStorage().getRestorer();
      restorer.loadRestorable("connections", restorer1 -> {
        restorer1.loadRestorable(connectionFileChanger.getId(), restorer2 -> {
          List<IGisFileContainer> fileList = new ArrayList<>();
          restorer2.loadCollection("file", restorer3 -> {
            String outputPath = restorer3.loadString("outputPath").get();
            String outputName = restorer3.loadString("outputName").get();
            IGisFileContainer file = null;
            // TODO: We should find a way to connect source files to used
            // TODO: connections without creating a strong dependency
            if (connectionFileChanger instanceof CIndexation || !connectionFileChanger.isUsed()) {
              //TODO FIX THAT
//              GisFileContainerFactory factory1 =new GisFileContainerFactory();
//              IGisFileContainer tmpFile = this.getFileByRelativePath(outputPath, outputName);
//              file = factory1.create(outputName, tmpFile.getAbsolutePath());
              file = this.getFileByRelativePath(outputPath, outputName);
//              file = factory.create(outputName, tmpFile.getAbsolutePath());
//              file.setParentModule(tmpFile.getParentModule());
              if (file == null) {
                throw new RuntimeException("file " + outputPath + outputName + " not found");
              }
              fileList.add(file);
              file.addFileObserver(connectionFileChanger);
            } else {
              /**
               * TODO: THis is not perfect: If we create a connection that uses a variable dependent
               * TODO: on the sources of other connection (InternalSelection), that the selected
               * TODO: source HAS to be still in the project. Maybe consider saved the absolute path to the file
               * in addition to the outputpath and outputname in a connection save method
               */
              file = this.getFileByRelativePath(outputPath, outputName);
              if (file != null) {
                fileList.add(file);
              } else {
                String tmpAbsPath = Paths.get(getProjectPath(),outputPath,outputName).toFile().getAbsolutePath();
                file = factory.create(outputName,tmpAbsPath);
                fileList.add(file);
              }
            }
          });
          connectionFileChanger.setSourceFileList(fileList);
          connectionFileChanger.restore(restorer2);

          solveInfoConnectionDependencies(connectionFileChanger.getOutputFileContainer(), fileList);
        });
      });
    }
  }

}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.settings;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dennis
 *
 */
public class UpdateCache
{
	private List<PluginCache> pluginCacheList;
	

	public UpdateCache()
	{
		pluginCacheList = new ArrayList<PluginCache>();
	}

	/**
	 * @return the pluginCacheList
	 */
	public List<PluginCache> getPluginCacheList()
	{
		return pluginCacheList;
	}

	/**
	 * @param pluginCacheList the pluginCacheList to set
	 */
	public void setPluginCacheList(List<PluginCache> pluginCacheList)
	{
		this.pluginCacheList = pluginCacheList;
	}
	
	public void addPluginCache(PluginCache pluginCache)
	{
		pluginCacheList.add(pluginCache);
	}
}

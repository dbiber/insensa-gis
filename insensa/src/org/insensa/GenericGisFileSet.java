/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.insensa.XMLProperties.XmlFileSetProperties;
import org.insensa.exceptions.GisFileException;
import org.insensa.exceptions.GisFileSetException;
import org.insensa.helpers.SystemSpec;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.OptionManager;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GenericGisFileSet implements IGisFileSet {
  private static final Logger log = LoggerFactory.getLogger(GenericGisFileSet.class);

  private String name = "unnamed FileSet";
  private String description = "";
  private IGisFileContainer activeGisFile;

  private XmlFileSetProperties fileSetProperties;

  private int activeNumGisFile = 0;
  private List<IGisFileContainer> fileList = new ArrayList<>();
  private List<IGisFileSet> childSets = new ArrayList<>();
  // private GenericGisFileSet parentSet =null;
  private List<FileSetListener> fileSetListeners = new ArrayList<>();
  private IGisFileSet activeFileSet = null;
  private int activeFileSetNum = -1;

  private ParentModule parent;

  public GenericGisFileSet(ParentModule parent,
                           String name,
                           WorkerStatus workerStatus)
      throws IOException, JDOMException {
    this.parent = parent;
    this.name = name;
    if (this.exists()) {
      this.fileSetProperties = new XmlFileSetProperties(this);
      this.addFileSetListener(this.fileSetProperties);
      for (Object fileElement : this.fileSetProperties.getGisFileList()) {
        String nameAttr = ((Element) fileElement).getAttributeValue("name");
        String pathAttr = ((Element) fileElement).getAttributeValue("path");
        Element outputNameElement = ((Element) fileElement).getChild("outputName");
        String outputName = outputNameElement.getText();
        if (pathAttr != null) {
          IGisFileContainer fileInformation = GisFileContainerFactory
              .createGisFileContainer(this,
                  pathAttr + nameAttr,
                  outputName,
                  false,
                  workerStatus);
          this.addGisFileInformation(fileInformation, workerStatus, false);
        } else {
          IGisFileContainer fileInformation = GisFileContainerFactory
              .createGisFileContainer(this,
                  this.getFullPath() + nameAttr,
                  outputName,
                  false,
                  workerStatus);
          this.addGisFileInformation(fileInformation, workerStatus, false);
        }
      }
      for (Object fileSetList : this.fileSetProperties.getGisFileSetList()) {
        String nameValue = ((Element) fileSetList).getAttributeValue("name");
        this.addChildSet(this.getPath() + this.getName(), nameValue, false,
            workerStatus);
      }
    } else {
      this.createFileSetDir();
      this.fileSetProperties = new XmlFileSetProperties(this);
      this.addFileSetListener(this.fileSetProperties);
    }
  }

  @Override
  public IGisFileSet addChildSet(String parentSetPath, String name,
                                 boolean serialize,
                                 WorkerStatus workerStatus)
      throws IOException, JDOMException, GisFileException {
    for (IGisFileSet childSet : this.childSets) {
      if (childSet.getName().equals(name)) {
        return childSet;
      }
    }
    this.createChildFileSetDir(this.getPath() + this.getName());
    GenericGisFileSet tmpSet = new GenericGisFileSet(this, name, workerStatus);
    this.childSets.add(tmpSet);
    tmpSet.setParentModule(this);
    if (serialize) {
      this.fileSetProperties.addChildSet(tmpSet);
    }
    this.setActiveFileSet(this.childSets.size() - 1);
    return tmpSet;
  }

  @Override
  public void addFileSetListener(FileSetListener listener) {
    this.fileSetListeners.add(listener);
  }

  @Override
  public synchronized void addGisFileInformation(IGisFileContainer file,
                                                 boolean serialize) throws IOException {
    String tmpPath = file.getAbsOutputDirectoryPath();
    // das muesste immer gleich sein ????
    // TODO: Why this ? If we remove this, we could remove addGisFile()
    if (!tmpPath.equals(this.getPath() + this.name)
        && !tmpPath.equals(this.getPath() + this.name + File.separator)) {
      log.warn("Should this happen ?");
      file.changeOutputFilePath(this.getPath() + this.name);
    }
    file.setParentModule(this);
    this.fileList.add(file);
    this.activeNumGisFile = this.fileList.size() - 1;
    this.activeGisFile = file;
    if (serialize) {
      this.fileSetProperties.addGisFile(file);
    }
  }


  public void addGisFileInformation(IGisFileContainer newRasterFile,
                                    WorkerStatus workerStatus,
                                    boolean serialize)
      throws IOException, JDOMException, GisFileException {
    newRasterFile.setParentModule(this);

    this.fileList.add(newRasterFile);
    // fileSetListeners.add(newRasterFile);
    if (workerStatus != null) {
      workerStatus.refreshPercentage(workerStatus.getProgress() + workerStatus.getStepSize());
      workerStatus.setProgressName("Load file " + newRasterFile.getName());
    }
    // newRasterFile.setObserver(this);
    this.activeNumGisFile = this.fileList.size() - 1;
    this.activeGisFile = this.fileList.get(this.activeNumGisFile);
    if (serialize) {
      this.fileSetProperties.addGisFile(newRasterFile);
    }
  }


  @Override
  public void addGisFileAt(IGisFileContainer file, boolean serialize, int index)
      throws IOException {
    String tmpPath = file.getAbsOutputDirectoryPath();
    if (!tmpPath.equals(this.getPath() + this.name)
        && !tmpPath.equals(this.getPath() + this.name + File.separator)) {
      file.changeOutputFilePath(this.getPath() + this.name);
    }
    file.setParentModule(this);
    this.fileList.add(index, file);
    // fileSetListeners.add(file);
    // file.setObserver(this);
    this.activeNumGisFile = this.fileList.size() - 1;
    this.activeGisFile = file;
    if (serialize) {
      this.fileSetProperties.addGisFile(file);
    }

  }

  /**
   * TODO should be done with listener.
   */
  @Override
  public void closeIter() {
    if (this.activeFileSet != null) {
      this.activeFileSet = null;
    }
    this.activeFileSetNum = -1;
    if (this.activeGisFile != null) {
      this.activeGisFile = null;
    }
    this.activeNumGisFile = -1;
    if (!this.fileList.isEmpty()) {
      this.fileList.clear();
    }

    this.childSets.forEach(IGisFileSet::closeIter);
    if (!(this.childSets.isEmpty())) {
      this.childSets.clear();
    }
  }

  @Override
  public boolean createChildFileSetDir(String path) throws IOException {
    String childFileSetPath = path;
    if (!(path.endsWith(File.separator))) {
      childFileSetPath += File.separator;
    }
    childFileSetPath += "FileSets";

    File childFileSetFolder = new File(childFileSetPath);
    return !childFileSetFolder.exists() && childFileSetFolder.mkdir();
  }

  /**
   * Creates the File Set- and Info Folder.
   */
  @Override
  public void createFileSetDir() throws IOException {
    File setDir = new File(this.getPath() + this.name);
    if (setDir.exists()) {
      GisFileSetException rfe = new GisFileSetException("Folder: " + this.getPath()
          + this.name + " already exists.",
          GisFileSetException.CREATE_DIR_WHILE_OPEN_PROJECT);
      rfe.setGisFilePath(this.getPath() + this.name);
      throw rfe;
    }
    if (!setDir.mkdir()) {
      throw new IOException("Can't create Folder");
    }
    File setInfoDir = new File(this.getPath()
        + this.name
        + File.separator
        + "infos");
    if (setInfoDir.exists()) {
      throw new IOException("Folder: "
          + setInfoDir.getPath()
          + setInfoDir.getName()
          + " allready exists.");
    }
    if (!setInfoDir.mkdir()) {
      throw new IOException("Can't infos create Folder");
    }

    File setDataDir = new File(this.getPath()
        + this.name
        + File.separator
        + "data");
    if (setDataDir.exists()) {
      throw new IOException("Folder: "
          + setDataDir.getPath()
          + setDataDir.getName()
          + " allready exists.");
    }
    if (!setDataDir.mkdir()) {
      throw new IOException("Can't create data Folder");
    }
  }

  @Override
  public void delete() throws IOException {
    for (IGisFileContainer fileContainer : this.fileList) {

      // fileSetListeners.remove(iFile);
      this.fileSetProperties.removeFile(fileContainer);
      if (fileContainer.getGisFile().getAbsolutePath()
          .equals(fileContainer.getAbsOutputDirectoryPath()
              + fileContainer.getOutputFileName())
          && fileContainer.getGisFile().getFileRef().exists()) {
        if (!fileContainer.delete()) {
          throw new IOException("Error deleting File " + fileContainer.getName());
        }
      } else if (!fileContainer.deleteLink()) {
        throw new IOException("Error deleting File Link " + fileContainer.getName());
      }
    }
    this.fileList.clear();
    for (IGisFileSet fileset : this.childSets) {
      fileset.delete();
      this.fileSetProperties.removeFileSet(fileset);
    }
    this.childSets.clear();
    this.fileSetListeners.remove(this.fileSetProperties);
    if (!this.fileSetProperties.delete()) {
      throw new IOException("Error deleting FileSetXML for " + this.getPath() + this.getName());
    }

    File infoFolder = new File(this.getPath()
        + this.getName()
        + File.separator + "infos");
    if (infoFolder.exists()) {
      if (!infoFolder.delete()) {
        throw new IOException("Error deleting folder " + this.getPath()
            + this.getName() + File.separator + "infos");
      }
    }

    File dataFolder = new File(this.getPath()
        + this.getName()
        + File.separator + "data");
    if (dataFolder.exists()) {
      if (!dataFolder.delete()) {
        throw new IOException("Error deleting folder " + this.getPath()
            + this.getName() + File.separator + "data");
      }
    }

    File childSetsFolder = new File(this.getPath() + this.getName()
        + File.separator + "FileSets");
    if (childSetsFolder.exists()) {
      if (!childSetsFolder.delete()) {
        throw new IOException("Error deleting folder " + this.getPath()
            + this.getName() + File.separator + "FileSets");
      }
    }

    File fsFolder = new File(this.getPath() + this.getName());
    if (fsFolder.exists()) {
      if (!fsFolder.delete()) {
        throw new IOException("Error deleting folder " + this.getPath() + this.getName());
      }
    }
  }

  @Override
  public boolean exists() {
    return new File(this.getPath() + this.getName()
        + File.separator + "FileSetSettings.xml").exists();
  }

  /**
   * @see org.insensa.ParentModule#fireChildFileSetRenamed(GenericGisFileSet, java.lang.String,
   * java.lang.String)
   */
  @Override
  public void fireChildFileSetRenamed(GenericGisFileSet fileSet, String oldName, String newName)
      throws IOException {
    for (FileSetListener fileSetListener : this.fileSetListeners) {
      fileSetListener.childFileSetRenamed(fileSet, oldName, newName);
    }
  }

  @Override
  public void fireChildGisFileOutputNameChanged(IGisFileContainer file, String oldOutputFile,
                                                String newNameTmp) throws IOException {
    for (FileSetListener fileSetListener : this.fileSetListeners) {
      fileSetListener.childFileOutputNameChanged(this, file, oldOutputFile, newNameTmp);
    }
  }

  @Override
  public void fireChildGisFileRenamed(IGisFileContainer file, String oldName, String newNameTmp)
      throws IOException {
    for (FileSetListener fileSetListener : this.fileSetListeners) {
      fileSetListener.childFileRenamed(this, file, oldName, newNameTmp);
    }
  }

  private void setActiveFileSet(GenericGisFileSet fileSet) {
    int fileSetIndex = this.childSets.indexOf(fileSet);
    if (fileSetIndex >= 0) {
      this.setActiveFileSet(fileSetIndex);
    }
  }

  private void setActiveFileSet(int fileSetIndex) {
    this.activeFileSetNum = fileSetIndex;
    this.activeFileSet = this.childSets.get(fileSetIndex);
  }

  @Override
  public IGisFileContainer getActiveGisFile() {
    return this.activeGisFile;
  }

  @Override
  public IGisFileSet getChildSetByName(String name) {
    for (IGisFileSet childSet : this.childSets) {
      if (name.equals(childSet.getName())) {
        return childSet;
      }
    }
    return null;
  }

  @Override
  public List<IGisFileSet> getChildSets() {
    return this.childSets;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public List<IGisFileContainer> getFileList() {
    return this.fileList;
  }

  @Override
  public XmlFileSetProperties getFileSetProperties() {
    return this.fileSetProperties;
  }

  /**
   * @return The absolute path to this fileset, including the FileSets subdirectory, the name and
   * the separators.
   */
  @Override
  public String getFullPath() {
    return this.parent.getFullPath() + "FileSets" + File.separator + this.name + File.separator;
  }

  @Override
  public IGisFileSet getLastActiveFileSet() {
    if (this.activeFileSet != null) {
      return (this.activeFileSet.getLastActiveFileSet());
    } else {
      return this;
    }
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public ParentModule getParent() {
    return this.parent;
  }

  /**
   * @return The path where this file set is located.
   */
  @Override
  public String getPath() {
    return this.parent.getFullPath() + "FileSets" + File.separator;
  }

  /**
   * @see org.insensa.ParentModule#getProject()
   */
  @Override
  public Project getProject() {
    return this.parent.getProject();
  }

  /**
   * @see org.insensa.ParentModule#getProjectPath()
   */
  @Override
  public String getProjectPath() {
    if (this.parent != null) {
      return this.parent.getProjectPath();
    }
    return null;
  }


  /**
   * @return relative path of parent + "FileSets"+name+sep.
   */
  @Override
  public String getPathRelativeToProject() {
    if (this.parent != null) {
      String parentRelPath = this.parent.getPathRelativeToProject();
      parentRelPath += "FileSets" + File.separator + this.getName() + File.separator;
      return parentRelPath;
    }
    return null;
  }

  @Override
  public String getWorkspacePath() {
    if (this.parent != null) {
      return this.parent.getWorkspacePath();
    }
    return null;
  }

  @Override
  public int indexOfIter(IGisFileSet fileSet) {
    int tmpIndex = this.childSets.indexOf(fileSet);
    if (tmpIndex >= 0) {
      return tmpIndex;
    } else {
      for (int i = 0; i < this.childSets.size(); i++) {
        tmpIndex = this.indexOfIter(fileSet);
        if (tmpIndex >= 0) {
          return tmpIndex;
        }
      }
    }
    return -1;
  }

  @Override
  public void removeChildFileSet(IGisFileSet fileSet) throws IOException {
    fileSet.delete();
    this.fileSetProperties.removeFileSet(fileSet);
    this.childSets.remove(fileSet);

  }

  @Override
  public int removeGisFile(IGisFileContainer gisFileInformation) throws IOException {
    this.fileSetProperties.removeFile(gisFileInformation);
    int index = -1;
    for (int i = 0; i < this.fileList.size(); i++) {
      if (gisFileInformation.getName().equals(this.fileList.get(i).getName())
          && gisFileInformation.getOutputFileName()
          .equals(this.fileList.get(i).getOutputFileName())) {
        index = i;
      }
    }
    if (index > -1) {
      if (this.fileList.remove(index) == null) {
        throw new IOException("GenericGisFileSet: removeGisFile: Error removing file");
      } else {
        // TODO kann weg
        return index;
      }
    } else {
      throw new IOException("GenericGisFileSet: removeGisFile: Error removing file");
    }
  }

  @Override
  public void renameTo(String newName) throws IOException {
    String oldFullPath = this.getFullPath();
    String newFullPath = this.getPath() + newName + SystemSpec.separator;
    File oldDir = new File(oldFullPath);
    File newDir = new File(newFullPath);
    String oldName = this.getName();
    // rename the Directory
    if (!oldDir.renameTo(newDir)) {
      throw new IOException("Failed to rename GenericGisFileSet \""
          + oldDir + "\" to \"" + newDir + "\"");
    }
    this.name = newName;
    // the parent File Set Path changed, we have to tell the child FileSets
    for (FileSetListener fileSetListener : this.fileSetListeners) {
      if (fileSetListener != null) {
        fileSetListener.fileSetRenamed(this, oldName, newName);
      }
    }
  }

  @Override
  public synchronized boolean replaceGisFileWithTarget(IGisFileContainer oldGisFile,
                                                       OptionFileChanger option)
      throws IOException {
    if (oldGisFile.getTargetGisFileContainer() == null) {
      return false;
    }

    if (!this.fileList.contains(oldGisFile)) {
      return false;
    }
    IGisFileContainer tmpTargetFile = oldGisFile.getTargetGisFileContainer();
    int index = this.fileList.indexOf(oldGisFile);
    tmpTargetFile.setParentModule(this);
    this.fileList.set(index, oldGisFile.getTargetGisFileContainer());
    this.fileSetProperties.replaceGisFileInformation(oldGisFile, tmpTargetFile);
    oldGisFile.setTargetGisFileContainer(null);
    if (tmpTargetFile.getGisFile().getType() == oldGisFile.getGisFile().getType()) {
      for (OptionFileChanger optionFileChanger : tmpTargetFile.getOptions()) {
        if (!optionFileChanger.isUsed() && optionFileChanger != option) {
          optionFileChanger.setOldGisFile(tmpTargetFile);
        }
      }
    } else {
      if (oldGisFile.isHardCopy()) {
        oldGisFile.delete();
      } else {
        oldGisFile.deleteLink();
      }
    }

    return true;
  }

  @Override
  public void setActiveParentFileSetIter() {
    if (this.parent != null && this.parent instanceof GenericGisFileSet) {
      ((GenericGisFileSet) this.parent).setActiveFileSet(this);
      ((GenericGisFileSet) this.parent).setActiveParentFileSetIter();
    }
  }

  @Override
  public void setParentModule(ParentModule parent) {
    this.parent = parent;
  }

  @Override
  public String toString() {
    return this.getName();
  }

  @Override
  public void unsetActiveSetsIter() {
    this.activeFileSet = null;
    this.activeFileSetNum = -1;
    for (IGisFileSet childSet : this.childSets) {
      childSet.unsetActiveSetsIter();
    }
  }

  /**
   * //TODO: Is THis the same as addGisFileInformation? Remove this ?
   *
   * @param serialize If true, the new file will be saved in the xml properties.
   */
  @Override
  public void addGisFile(IGisFileContainer newGisFile, boolean serialize) throws IOException,
      GisFileException {
    newGisFile.setParentModule(this);
    this.fileList.add(newGisFile);
    this.activeNumGisFile = this.fileList.size() - 1;
    this.activeGisFile = this.fileList.get(this.activeNumGisFile);
    if (serialize) {
      this.fileSetProperties.addGisFile(newGisFile);
    }
  }

  public void addGisFileNameList(List<String> fullNameList, List<String> outputNameList,
                                 String option, boolean serialize) throws JDOMException,
      IOException {
    OptionManager tmpOptionManager = new OptionManager();
    int list = fullNameList.size();
    for (int i = 0; i < list; i++) {
      try {
        if (option != null) {
          IGisFileContainer gisFileInformation = new RasterFileContainer(
              this, outputNameList.get(i),
              fullNameList.get(i),
              tmpOptionManager.getOption(option).orElse(null));
          this.addGisFile(gisFileInformation, serialize);
        } else {
          IGisFileContainer gisFileInformation = new RasterFileContainer(
              this, outputNameList.get(i), fullNameList.get(i),
              null);
          this.addGisFile(gisFileInformation, serialize);
        }
      } catch (GisFileException e) {
        e.printStackTrace();
      }
    }
  }

  public static class RenameErrorException extends IOException {
    private static final long serialVersionUID = 889643985275723795L;
    private List<Object> errorList = null;

    public RenameErrorException(List<Object> errorList) {
      super();
      this.errorList = errorList;
    }

    public List<Object> getErrorList() {
      return this.errorList;
    }
  }


}

package org.insensa;

import org.insensa.exceptions.GisFileException;

import java.io.File;
import java.io.IOException;

public class StackFile implements IGisFile {
  @Override
  public GisFileType getType() {
    return GisFileType.UNKNOWN;
  }

  @Override
  public boolean delete() {
    return false;
  }

  @Override
  public boolean exists() {
    return false;
  }

  @Override
  public String getAbsolutePath() {
    return null;
  }

  @Override
  public File getFileRef() {
    return null;
  }

  @Override
  public String getName() {
    return null;
  }

  @Override
  public void relocateFile(String fullPath) {

  }

  @Override
  public boolean renameTo(File dest) {
    return false;
  }

  @Override
  public void unlock() {

  }

  @Override
  public void refresh() throws GisFileException {

  }

  @Override
  public GisAttributes getGisAttributes() {
    return null;
  }
}

package org.insensa;

import org.insensa.XMLProperties.XmlProperties;
import org.insensa.XMLProperties.XmlViewProperties;
import org.insensa.controller.Controller;
import org.insensa.extensions.IPluginExec;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.view.View;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Environment {
  private static final Logger log = LoggerFactory.getLogger(Environment.class);

  private static Environment instance = new Environment();
  private Model model;
  private View view;
  private Controller controller;
  private ISavableRestorer globalRestorer;
  private ISavableSaver globalSaver;

  private Environment() {
  }

  public static Environment getInstance() {
    return instance;
  }

  public static XmlViewProperties getViewProperties() {
    return instance.getView().getViewProperties();
  }

  public static XmlProperties getRootProperties() {
    return getInstance().getModel().getProperties();
  }

  //FIXME: change threading model to concurrency model
  public static int getMaxThreadsAvailable() {
    //    if (OSInfo.getOSType() == OSInfo.OSType.WINDOWS) {
    //      return 1;
    //    } else {
    //      return Runtime.getRuntime().availableProcessors();
    //    }
    return 1;
  }

  public static ISavableRestorer getUserPluginDefaultValueRestorer() {
    instance.initGlobalRestorer();

    return instance.globalRestorer;
  }

  public static void restoreDefaultPluginExecValues(IPluginExec pluginExec) {
    getRootProperties().restorePluginExec(pluginExec, instance.model.getActiveUser().getName());
  }

  public static void refreshDefaultPluginExecValues(IPluginExec pluginExec) {
    getRootProperties().refreshPluginExec(pluginExec, instance.model.getActiveUser().getName());
  }

  public static void restoreDefaultPluginVariables(IScriptPluginExec pluginExec) {
    getRootProperties().restorePluginVariables(pluginExec, instance.model.getActiveUser().getName());
  }

  public static void refreshDefaultPluginVariables(IScriptPluginExec pluginExec) {
    getRootProperties().refreshPluginVariables(pluginExec, instance.model.getActiveUser().getName());
  }

  public View getView() {
    return view;
  }

  public void setView(View view) {
    this.view = view;
  }

  public Model getModel() {
    return model;
  }

  public void setModel(Model model) {
    this.model = model;
  }

  public Controller getController() {
    return controller;
  }

  public void setController(Controller ctrl) {
    controller = ctrl;
  }

  private void initGlobalRestorer() {
    //    if (this.globalRestorer != null) {
    //      return;
    //    }
    XmlProperties properties = getRootProperties();
    User user = model.getActiveUser();
    if (user == null) {
      throw new RuntimeException("No active user found");
    }
    Element element = properties.getGlobalVariablesElement(user.getName());
    if (element != null) {
      globalRestorer = new JdomRestorer(element);
    }
//    if (element == null) {
//      throw new RuntimeException("Could not find user with with name " + user.getName());
//    }
//    globalRestorer = new JdomRestorer(element);
  }

//  private void initGlobalSaver() {
//    if (this.globalSaver != null) {
//      return;
//    }
//    Element globVar = model.getProperties().getGlobalVariablesElement(model.getActiveUser().getName());
//    globalSaver = new JdomSaver(globVar);
//  }
}

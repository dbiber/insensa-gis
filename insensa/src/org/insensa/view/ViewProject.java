/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import org.insensa.view.image.ImageView;
import org.insensa.view.image.ImageViewToolbar;
import org.insensa.view.r.ViewRLogTail;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;


public class ViewProject {
  private JSplitPane splitpane;
  private JSplitPane splitpane2;
  private JSplitPane splitpane3;
  private ViewFileSet fileSetView;
  private ViewModulationSet modSetView;
  private ViewOptions optionsView;

  private JTabbedPane bottomTabView;
  private ViewDescription descriptionView;
  private ViewRLogTail rLogTail;
  // private ViewHtmlInfo htmlInfo;
  // private ViewImageDraw imageView;
  // private ImageMap imageMap;
  private ImageView viewImage;
  private JPanel rightPanel;
  private ImageViewToolbar imageViewToolbar;


  public ViewProject() {
    // super();
    this.fileSetView = new ViewFileSet();
    this.modSetView = new ViewModulationSet();
    this.optionsView = new ViewOptions();
    this.fileSetView.setMinimumSize(new Dimension(100, 200));

    this.viewImage = new ImageView();
    this.rightPanel = new JPanel();
    this.initPanelRight();
    // rightPanel.add(viewImage);
    // viewImage.setMinimumSize(new Dimension(100,200));
    this.bottomTabView = new JTabbedPane();
    this.descriptionView = new ViewDescription();
    this.rLogTail = new ViewRLogTail();
    this.bottomTabView.addTab("R Log", new JScrollPane(this.rLogTail));
    this.bottomTabView.addTab("Description", new JScrollPane(this.descriptionView));

    // splitpane = new JSplitPane();
    this.initializeProjectView();
  }


  public void close() {
    if (this.optionsView != null && this.optionsView.isVisible()) {
      this.optionsView.close();
      this.optionsView.setVisible(false);
      this.splitpane3.setLeftComponent(null);
      this.optionsView = null;
    }
    if (this.rightPanel != null && this.rightPanel.isVisible())
    // if(viewImage!=null && viewImage.isVisible())
    {
      this.rightPanel.setVisible(false);
      this.splitpane3.setRightComponent(null);
      this.rightPanel = null;
      // viewImage.setVisible(false);
      // splitpane3.setRightComponent(null);
      // viewImage=null;
    }
    if (this.fileSetView != null && this.fileSetView.isVisible()) {
      this.fileSetView.close();
      this.fileSetView.setVisible(false);
      this.splitpane2.setLeftComponent(null);
      this.fileSetView = null;
    }
    if (this.descriptionView != null && this.descriptionView.isVisible()) {//TODO: Why is this? should I create this for ViewRLogTail ?
      // descriptionView.close();
      this.descriptionView.setVisible(false);
      this.splitpane.setBottomComponent(null);
      this.descriptionView = null;
    }
    /*
     * if(modSetView!=null && modSetView.isVisible()) { modSetView.close();
     * modSetView.setVisible(false); splitpane.setBottomComponent(null);
     * modSetView=null; }
     */
    this.splitpane2.setRightComponent(null);
    this.splitpane.setTopComponent(null);
    this.splitpane3.setVisible(false);
    this.splitpane2.setVisible(false);
    this.splitpane.setVisible(false);
    this.splitpane3 = null;
    this.splitpane2 = null;
    this.splitpane = null;
  }

  /**
   * @return
   */
  public ViewDescription getDescriptionView() {
    return this.descriptionView;
  }

  public ViewRLogTail getRLogTailView() {
    return rLogTail;
  }

  /**
   * @return
   */
  public ViewFileSet getFileSetView() {
    return this.fileSetView;
  }

  /**
   * @param fileSetView
   */
  public void setFileSetView(ViewFileSet fileSetView) {
    this.fileSetView = fileSetView;
  }

  /**
   * @return
   */
  public ImageView getImageView() {
    return this.viewImage;
  }

  /**
   * @return
   */
  public ImageViewToolbar getImageViewToolbar() {
    return this.imageViewToolbar;
  }

  /**
   * @return
   */
  public ViewModulationSet getModSetView() {
    return this.modSetView;
  }

  /**
   * @return
   */
  public ViewOptions getOptionsView() {
    return this.optionsView;
  }

  /**
   * @return
   */
  public JSplitPane getSplitpane() {
    return this.splitpane;
  }

  /**
   * @param splitpane
   */
  public void setSplitpane(JSplitPane splitpane) {
    this.splitpane = splitpane;
  }

  /**
   * @return
   */
  public JSplitPane getSplitpane2() {
    return this.splitpane2;
  }

  /**
   * @return
   */
  public JSplitPane getSplitpane3() {
    return this.splitpane3;
  }

  private void initializeProjectView() {
    this.splitpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

    this.splitpane2 = new JSplitPane();
    this.splitpane3 = new JSplitPane();

    this.splitpane2.setLeftComponent(this.fileSetView);
    this.splitpane2.setRightComponent(this.splitpane3);

    this.splitpane3.setLeftComponent(this.splitpane);
    this.splitpane3.setRightComponent(this.rightPanel);

    this.splitpane.setTopComponent(this.optionsView);

    this.splitpane.setBottomComponent(this.bottomTabView);
//		this.splitpane.setBottomComponent(new JScrollPane(this.descriptionView));
  }

  private void initPanelRight() {

    this.imageViewToolbar = new ImageViewToolbar();

    javax.swing.GroupLayout panelRightLayout = new javax.swing.GroupLayout(this.rightPanel);
    this.rightPanel.setLayout(panelRightLayout);
    panelRightLayout.setHorizontalGroup(panelRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(this.imageViewToolbar, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
        .addComponent(this.viewImage, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE));
    panelRightLayout.setVerticalGroup(panelRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelRightLayout.createSequentialGroup()
            .addComponent(this.imageViewToolbar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
            // .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.viewImage, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)));

  }

  public void setDividerLocations() {

    this.splitpane2.setDividerLocation(0.3);
    this.splitpane3.setDividerLocation(0.4);
    this.splitpane.setDividerLocation(0.8);
  }

}

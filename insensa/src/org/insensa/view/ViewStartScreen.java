/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;


import org.insensa.Model;
import org.insensa.helpers.IOHelper;
import org.insensa.settings.PluginCache;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingWorker;

import org.insensa.updater.actions.ActionThreadPool;
import org.insensa.updater.components.InstallerView;
import org.insensa.updater.items.ItemPlugin;
import org.insensa.updater.main.InsensaHttpClient;
import org.insensa.updater.main.UpdateHelper;
import org.insensa.updater.main.VersionChecker;

public class ViewStartScreen extends JPanel {
  private static final Logger log = LoggerFactory.getLogger(ViewStartScreen.class);

  private static final long serialVersionUID = 1L;
  private JPanel startPanel;
  private JButton buttonOpenProject;
  private JButton buttonNewProject;
  private JButton buttonNewUser;
  //    private JButton buttonImportConfig;
  private JButton buttonHelp;
  private JButton buttonAbout;
  private ViewStartScreenLogo logo;
  private Model model;
  private JButton buttonUpdate;


  /**
   * This is the default constructor.
   */
  public ViewStartScreen(Model model) {
    super();
    this.model = model;
    this.initialize();
    this.checkUpdate();
  }

  /**
   *
   */
  public JButton getButtonAbout() {
    return this.buttonAbout;
  }

  /**
   *
   */
  public JButton getButtonHelp() {
    return this.buttonHelp;
  }

  /**
   *
   */
  public JButton getButtonNewProject() {
    return this.buttonNewProject;
  }

  /**
   *
   */
  public JButton getButtonNewUser() {
    return this.buttonNewUser;
  }

  /**
   *
   */
  public JButton getButtonOpenProject() {
    return this.buttonOpenProject;
  }

//    /**
//     * @return the buttonImportConfig
//     */
//    public JButton getButtonImportConfig() {
//        return buttonImportConfig;
//    }

  /**
   * This method initializes this.
   *
   * @return void
   */
  private void initialize() {
    super.setLayout(new GridBagLayout());

    this.buttonUpdate = new JButton("Update Insensa...");
    this.buttonUpdate.setFont(new Font("Tahoma", Font.BOLD, 11));
    this.buttonUpdate.setContentAreaFilled(false);
    this.buttonUpdate.setBorderPainted(false);
    this.buttonUpdate.setVisible(false);

    buttonUpdate.addActionListener(e -> {
      if (new File("InsensaUpdater").exists() || new File("InsensaUpdater.exe").exists()) {
        int archType = 0;

        archType = new VersionChecker().getOsType();
        if (archType == VersionChecker.OS_LINUX || archType == VersionChecker.OS_MAC) {
          try {
            Process process = new ProcessBuilder("./InsensaUpdater").start();
            System.exit(0);
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        } else if (archType == VersionChecker.OS_WIN) {
          try {
            Process process = new ProcessBuilder("InsensaUpdater.exe").start();
            System.exit(0);
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      } else {
        ActionThreadPool.getInstance().setInstallationPath(IOHelper.getInstance().getDocumentsDir().getAbsolutePath());
        VersionChecker vc = new VersionChecker();
        vc.setInsensaDocumentPath(IOHelper.getInstance().getDocumentsDir().getAbsolutePath());
        InstallerView iv = new InstallerView(vc);
        iv.setVisible(true);
      }
    });

    this.buttonNewUser = new JButton("Create New User...");
    this.buttonNewUser.setContentAreaFilled(false);
    this.buttonNewUser.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 11));
    this.buttonNewUser.setBorderPainted(false);

    this.buttonOpenProject = new JButton("Open Existing Project...");
    this.buttonOpenProject.setContentAreaFilled(false);
    this.buttonOpenProject.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 11));
    this.buttonOpenProject.setBorderPainted(false);

    this.buttonNewProject = new JButton("Create New Project...");
    this.buttonNewProject.setContentAreaFilled(false);
    this.buttonNewProject.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 11));
    this.buttonNewProject.setBorderPainted(false);

//        this.buttonImportConfig = new JButton("Import Existing Config...");
//        this.buttonImportConfig.setContentAreaFilled(false);
//        this.buttonImportConfig.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 11));
//        this.buttonImportConfig.setBorderPainted(false);
//        this.buttonImportConfig.setToolTipText("Import MainConfig.xml (Users with Workspaces) from an old version");

    this.buttonHelp = new JButton("Help");
    this.buttonHelp.setFont(new Font(Font.DIALOG, Font.PLAIN, 11));

    this.buttonAbout = new JButton("About");
    this.buttonAbout.setFont(new Font(Font.DIALOG, Font.PLAIN, 11));

    try {
      logo = new ViewStartScreenLogo();
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }

    this.startPanel = new JPanel();
    this.startPanel.setBackground(new Color(255, 255, 255));
    this.startPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

    this.initializeStartPanel();
    this.add(this.startPanel, new GridBagConstraints());
  }


  private void initializeStartPanel() {

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.startPanel);
    this.startPanel.setLayout(jPanel1Layout);

    /**
     * ********************** Horizontal **********************
     */
    /**
     * buttonNewUser buttonNewProject buttonOpenProject
     */
    Group horStarterButtonsParGroup = jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(this.buttonUpdate)
        .addComponent(this.buttonNewUser)
        .addComponent(this.buttonNewProject)
        .addComponent(this.buttonOpenProject);
//                .addComponent(this.buttonImportConfig);


    /**
     * buttonHelp buttonAbout
     */
    Group horHelpButtons = jPanel1Layout
        .createSequentialGroup()
        .addComponent(this.buttonHelp)
        .addGap(10)
        .addComponent(this.buttonAbout);

    /**
     * horStarterButtonsParGroup horHelpButtons
     */
    Group horBottomGroup = jPanel1Layout.createSequentialGroup()
        .addGroup(horStarterButtonsParGroup)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addGroup(horHelpButtons)
        .addContainerGap();

    /**
     * logo horBottomGroup
     */
    Group horAllGroup = jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
//				.addComponent(this.labelVersion, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addComponent(this.logo, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGroup(horBottomGroup);

    Group horAllGroup2 = jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(horAllGroup)
        .addContainerGap();

    jPanel1Layout.setHorizontalGroup(horAllGroup2);

    /**
     * ********************** Vertical **********************
     */
    /**
     * buttonNewUser buttonNewProject buttonOpenProject
     */
    Group verStartButtons = jPanel1Layout.createSequentialGroup();
    verStartButtons
        .addComponent(this.buttonUpdate)
        .addComponent(this.buttonNewUser)
        .addComponent(this.buttonNewProject)
        .addComponent(this.buttonOpenProject);
//                .addComponent(this.buttonImportConfig);

    Group verHelpButtons = jPanel1Layout.createParallelGroup()
        .addComponent(this.buttonHelp)
        .addComponent(this.buttonAbout);

    jPanel1Layout.createSequentialGroup()
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addGroup(verHelpButtons);

    /**
     * buttonHelp buttonAbout
     */
    Group verBottom = jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
        .addGroup(verStartButtons)
        .addGroup(verHelpButtons);

    /**
     * logo verBottom
     */
    Group verAll = jPanel1Layout.createSequentialGroup().addContainerGap()
        .addComponent(this.logo, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
//				.addGap(10)
//				.addComponent(labelVersion)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
        .addGroup(verBottom)
        .addContainerGap();
    jPanel1Layout.setVerticalGroup(verAll);
  }

  public void checkUpdate() {
    new SwingWorker<UpdateHelper, String>() {

      @Override
      protected UpdateHelper doInBackground() {
        VersionChecker versionChecker = new VersionChecker();
        versionChecker.setInsensaDocumentPath(IOHelper.getInstance().getDocumentsDir().getAbsolutePath());

        InsensaHttpClient client = new InsensaHttpClient(versionChecker);

        UpdateHelper updateHelper = new UpdateHelper(client, versionChecker);
        try {
          updateHelper.parse();
          return updateHelper;
        } catch (JDOMException e) {
          log.error("UNKNOWN", e);
        } catch (IOException e) {
          log.error("UNKNOWN", e);
        }

        return null;
      }

      /**
       *
       * @see javax.swing.SwingWorker#done()
       */
      @Override
      protected void done() {
        try {
          UpdateHelper retValue = get();
          if (retValue != null) {
            List<PluginCache> newPluginCacheList = new ArrayList<PluginCache>();

            boolean newPluginsFound = false;
            List<PluginCache> pluginCacheList = model.getProperties().getPluginCacheList();
            Map<String, ItemPlugin> pluginMap = retValue.getNewPluginMap();
            if (pluginMap == null || pluginMap.isEmpty()) {
              // No new Plugins
            } else if (pluginCacheList == null || pluginCacheList.isEmpty()) {
              //New Plugins
              newPluginsFound = true;
              Iterator<String> nameIter = pluginMap.keySet().iterator();
              while (nameIter.hasNext()) {
                String string = (String) nameIter.next();
                ItemPlugin ip = pluginMap.get(string);
                PluginCache pc = new PluginCache();
                pc.setName(ip.getName());
                pc.setVersion(ip.getVersion());
                newPluginCacheList.add(pc);
              }
              model.getProperties().setPluginCacheList(newPluginCacheList);
            } else {
              Iterator<String> nameIter = pluginMap.keySet().iterator();
              boolean found = false;
              while (nameIter.hasNext()) {
                String string = (String) nameIter.next();
                found = false;
                for (PluginCache pluginCache : pluginCacheList) {
                  if (pluginCache.getName().equals(string)) {
                    found = true;
                  } else {
                  }
                }
                if (found == false) {
                  newPluginsFound = true;
                  ItemPlugin ip = pluginMap.get(string);
                  PluginCache pc = new PluginCache();
                  pc.setName(ip.getName());
                  pc.setVersion(ip.getVersion());
                  newPluginCacheList.add(pc);
                } else {

                }
              }
              newPluginCacheList.addAll(pluginCacheList);
              model.getProperties().setPluginCacheList(newPluginCacheList);
            }

//						if(retValue.isCoreUpdatable() || !retValue.getUpdateableMap().isEmpty())
            if (!retValue.getUpdateableMap().isEmpty()) {
              buttonUpdate.setVisible(true);
            }
            if (newPluginsFound == true) {
              buttonUpdate.setVisible(true);
              logo.setNewPluginText(true);
              logo.revalidate();
            } else {
              if (pluginMap != null && !pluginMap.isEmpty() && buttonUpdate.isVisible() == false) {
                buttonUpdate.setText("Install Plugins...");
                buttonUpdate.setVisible(true);
              }
            }

          }
        } catch (InterruptedException e) {
          log.error("UNKNOWN", e);
        } catch (ExecutionException e) {
          log.error("UNKNOWN", e);
        } catch (JDOMException e) {
          log.error("UNKNOWN", e);
        } catch (IOException e) {
          log.error("UNKNOWN", e);
        }

      }
    }.execute();

  }

}

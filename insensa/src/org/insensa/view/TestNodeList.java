/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.tree.TreePath;

/**
 * @author helloworld922
 * <p>
 * @version 1.0
 * <p>
 * copyright 2010 <br>
 * You are welcome to use/modify this code for any purposes you want so
 * long as credit is given to me.
 */
public class TestNodeList implements Transferable, Serializable {

  /**
   * Data flavor that allows a DnDTreeList to be extracted from a transferable
   * object
   */
  public final static DataFlavor TestNodeList_FLAVOR = new DataFlavor(TestNodeList.class, "Drag and drop list");
  private static final long serialVersionUID = 1270874212613332692L;
  /**
   * List of flavors this DnDTreeList can be retrieved as. Currently only
   * supports DnDTreeList_FLAVOR
   */
  protected static DataFlavor[] flavors =
      {TestNodeList.TestNodeList_FLAVOR};

  /**
   * Nodes to transfer
   */
  protected ArrayList<TreePath> nodes;

  /**
   * @param nodes
   */
  public TestNodeList(ArrayList<TreePath> nodes) {
    this.nodes = nodes;
  }

  /**
   * @return
   */
  public ArrayList<TreePath> getNodes() {
    return this.nodes;
  }

  /**
   * @param flavor
   * @return
   * @throws UnsupportedFlavorException
   * @throws IOException
   **/
  @Override
  public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
    if (this.isDataFlavorSupported(flavor)) {
      return this;
    } else {
      throw new UnsupportedFlavorException(flavor);
    }
  }

  /**
   * @see java.awt.datatransfer.Transferable#getTransferDataFlavors()
   */
  @Override
  public DataFlavor[] getTransferDataFlavors() {
    // TODO Auto-generated method stub
    return TestNodeList.flavors;
  }

  /**
   * @param flavor
   * @return
   **/
  @Override
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    DataFlavor[] flavs = this.getTransferDataFlavors();
    for (DataFlavor flav: flavs) {
      if (flav.equals(flavor)) {
        return true;
      }
    }
    return false;
  }

}
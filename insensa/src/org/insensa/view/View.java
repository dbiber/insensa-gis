/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;


import org.insensa.GenericGisFileSet;
import org.insensa.GisFileContainer;
import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Model;
import org.insensa.Project;
import org.insensa.RasterFileContainer;
import org.insensa.XMLProperties.XmlViewProperties;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.exceptions.WriteConfigException;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;


@SuppressWarnings("serial")
public class View extends JFrame {
  private static final Logger log = LoggerFactory.getLogger(View.class);

  private ViewMenu menuView;
  private Model model;
  private List<ViewProject> viewProjectList = new ArrayList<ViewProject>();
  private ViewStartScreen startScreen;
  private TreeNode activeTreePath[];
  private XmlViewProperties viewProperties;

  public View(Model my_model) throws JDOMException, IOException {
    super();
    this.model = my_model;
    this.menuView = new ViewMenu();
    this.startScreen = new ViewStartScreen(model);
    super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

//  // TODO NEVER USED
//  public void addConnectionFileChangerByActive(ConnectionFileChanger Conn, boolean updateNow) throws IOException {
//    DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) this.activeTreePath[this.activeTreePath.length - 1];
//    this.viewProjectList.get(0).getFileSetView().setConnectionFileChanger(Conn, tmpNode, updateNow);
//  }

  public void addFilesToFileSetByActive(List<IGisFileContainer> rasterFileList) {
    DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) this.activeTreePath[this.activeTreePath.length - 1];
    this.viewProjectList.get(0).getFileSetView().addNodes(rasterFileList, tmpNode);
  }

  public Model getModel() {
    return model;
  }


  public void processCloseWindowEvent() {
    super.processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
  }

  // public void addFileSetToFileSetByActive(GenericGisFileSet childSet) throws
  // IOException
  // {
  // DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)
  // activeTreePath[activeTreePath.length - 1];
  // if (treeNode == null)
  // throw new
  // IOException("View: addFileSetToFileSetByActive: NotePath = null");
  // if (!(treeNode.getUserObject() instanceof GenericGisFileSet))
  // throw new
  // IOException("View: addFileSetToFileSetByActive: Note is not of Type GenericGisFileSet");
  // viewProjectList.get(0).getFileSetView().addNode(treeNode, childSet);
  // }

  public void addInfoReaderToFileByActive(InfoReader iReader, boolean updateNow) throws IOException {
    DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) this.activeTreePath[this.activeTreePath.length - 1];
    this.viewProjectList.get(0).getFileSetView().addInfoReader(iReader, tmpNode, updateNow);
  }

  public void addProject() throws IOException {
    if (this.startScreen != null) {
      this.remove(this.startScreen);
      this.startScreen = null;

    }
    Project activeProject = this.model.getActiveProject();
    if (activeProject == null)
      throw new IOException("No active project found");
    try {
      this.viewProperties = new XmlViewProperties(this.model.getActiveProject().getWorkspacePath() +
          this.model.getActiveProject().getName());
      activeProject.addProjectListener(new ViewPropertiesProjectListener(this.viewProperties, this));
    } catch (WriteConfigException e) {
      log.error("", e);
    }
    this.viewProjectList.add(new ViewProject());
    this.add(this.viewProjectList.get(0).getSplitpane2());
    this.setTitle(this.model.getActiveProject().getName() + "-" + this.model.getActiveProject().getOwner());
    this.setVisible(true);
    this.viewProjectList.get(0).setDividerLocations();
  }

  public void addRootFileSet(GenericGisFileSet fileSet) {
    this.viewProjectList.get(0).getFileSetView().addRootFileSet(fileSet);
  }

  /**
   * testFunktion.
   */
  public void addRootFileSetIter(IGisFileSet fileSet, boolean updateNow) throws IOException {
    this.viewProjectList.get(0).getFileSetView().addFileSetIter(-1, null, fileSet, updateNow);
  }


  public void closeProject() {
    if (!(this.viewProjectList.isEmpty())) {
      for (ViewProject ivPro: this.viewProjectList) {
        ivPro.close();
      }
      this.viewProjectList.clear();
    }
  }

  private List<Integer> createExpandList(DefaultMutableTreeNode node) {
    ViewFileSet tmpSet = this.viewProjectList.get(0).getFileSetView();
    JTree tree = tmpSet.getFileSetTree();
    List<Integer> iList = new ArrayList<Integer>();
    if (node.isLeaf()) {
      int index = tree.getRowForPath(new TreePath(node.getPath()));

      if (index > -1 && tree.isExpanded(index))
        iList.add(index);
      return iList;
    } else {
      int index = tree.getRowForPath(new TreePath(node.getPath()));
      if (index > -1 && tree.isExpanded(index))
        iList.add(index);
      int childrenCount = node.getChildCount();
      for (int i = 0; i < childrenCount; i++)
        iList.addAll(this.createExpandList((DefaultMutableTreeNode) node.getChildAt(i)));
      return iList;
    }
  }

  private List<TreePath> createExpandPathList(DefaultMutableTreeNode node) {
    ViewFileSet tmpSet = this.viewProjectList.get(0).getFileSetView();
    JTree tree = tmpSet.getFileSetTree();
    List<TreePath> iList = new ArrayList<TreePath>();
    if (node.isLeaf()) {
      TreePath path = new TreePath(node.getPath());
      if (path != null && tree.isExpanded(path))
        iList.add(path);
      return iList;
    } else {
      TreePath path = new TreePath(node.getPath());
      if (path != null && tree.isExpanded(path))
        iList.add(path);
      int childrenCount = node.getChildCount();
      for (int i = 0; i < childrenCount; i++)
        iList.addAll(this.createExpandPathList((DefaultMutableTreeNode) node.getChildAt(i)));
      return iList;
    }
  }

  public TreeNode getActiveTreeNode() {
    if (this.activeTreePath == null)
      return null;
    if (this.activeTreePath.length <= 0)
      return null;
    return this.activeTreePath[this.activeTreePath.length - 1];
  }

  public TreeNode[] getActiveTreePath() {
    return this.activeTreePath;
  }

  public void setActiveTreePath(TreeNode[] nodePath) {
    this.viewProjectList.get(0).getFileSetView().setActiveTreePath(nodePath);
    this.activeTreePath = nodePath;
  }

  public ViewMenu getMenuView() {
    return this.menuView;
  }

  public List<Integer> getRootExpandetList() {
    return this.createExpandList((DefaultMutableTreeNode) this.getViewProject().getFileSetView().getFileSetTree().getModel().getRoot());
  }

  public ViewStartScreen getStartScreen() {
    return this.startScreen;
  }

  public TreePath getTreePath(Object userObject) {
    return this.viewProjectList.get(0).getFileSetView().getTreePath(userObject);
  }

  public TreePath[] getTreePaths(Object[] userObject) {
    return this.viewProjectList.get(0).getFileSetView().getTreePaths(userObject);
  }

  public ViewProject getViewProject() {
    return this.viewProjectList.get(0);
  }

  public XmlViewProperties getViewProperties() {
    return this.viewProperties;
  }

  public void initialize() {
    this.initializeMenuView();
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setState(Frame.NORMAL);
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension dimension = toolkit.getScreenSize();
    this.setSize(dimension);
    this.setVisible(true);
  }

  private void initializeMenuView() {
    this.setJMenuBar(this.menuView.getMenuBar());
    this.add(this.menuView.getToolBar(), BorderLayout.NORTH);
    this.add(this.startScreen, BorderLayout.CENTER);
    this.add(this.menuView.getStatusLabel(), BorderLayout.SOUTH);
    this.setStatusText("Ready");
  }

  public void openProject(String path) throws IOException {
    // viewProperties = new XmlViewProperties(path);
    this.addProject();
    List<IGisFileSet> rootFileList = this.model.getActiveProject().getFileSetList();
    for (IGisFileSet iFileSet: rootFileList)
      this.addRootFileSetIter(iFileSet, true);

    JTree tree = this.getViewProject().getFileSetView().getFileSetTree();
    List<Integer> iList = new ArrayList<Integer>();
    iList.addAll(this.viewProperties.getExpandList());

    tree.expandPath(new TreePath(tree.getModel().getRoot()));
    for (Integer iter: iList)
      tree.expandRow(iter);
  }

  public void refreshExpand(TreePath[] oldPaths) {
    ViewFileSet tmpSet = this.viewProjectList.get(0).getFileSetView();
    JTree tree = tmpSet.getFileSetTree();
    tree.getRowCount();
    for (TreePath tp: oldPaths) {
      tree.expandPath(tmpSet.getNewTreePath(tp));
    }
  }

  public void refreshGui() {
    this.viewProjectList.get(0).getFileSetView().updateTree();
  }

  /**
   * @param dataVector a X*2 String Vector
   */
  public void refreshOptionsView(Vector<Vector<String>> dataVector) {
    this.viewProjectList.get(0).getOptionsView().setTableContent(dataVector);
  }

  public DefaultMutableTreeNode refreshTreeNode(DefaultMutableTreeNode refNode, boolean updateNow) throws IOException {
    ViewFileSet tmpSet = this.viewProjectList.get(0).getFileSetView();
    JTree tree = tmpSet.getFileSetTree();
    if (refNode != null) {
      if (refNode.equals(tmpSet.getFileSetTree().getModel().getRoot())) {
        return this.refreshTreeRoot(updateNow);
      }

      boolean isExpanded = tree.isExpanded(new TreePath(refNode.getPath()));
      Object tmpObj = refNode.getUserObject();
      if (tmpObj instanceof GenericGisFileSet) {
        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) refNode.getParent();
        if (parentNode != null) {

          List<TreePath> oldTreePaths = new ArrayList<TreePath>();
          oldTreePaths.addAll(this.createExpandPathList(refNode));

          int fileSetNodeIndex = parentNode.getIndex(refNode);
          tmpSet.removeNodeIter(refNode);
          tmpSet.addFileSetIter(fileSetNodeIndex, parentNode, (GenericGisFileSet) tmpObj, updateNow);
          this.refreshExpand(oldTreePaths.toArray(new TreePath[oldTreePaths.size()]));
        }
      } else if (tmpObj instanceof GisFileContainer) {

        DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) refNode.getParent();
        if (parentNode != null) {
          int fileNodeIndex = parentNode.getIndex(refNode);
          tmpSet.removeNodeIter(refNode);
          DefaultMutableTreeNode tmpNode = tmpSet
              .addGisFileAndInfoReader(fileNodeIndex, parentNode, (IGisFileContainer) tmpObj, updateNow);
          if (isExpanded == true)
            tmpSet.expandNode(tmpNode, true);
          return tmpNode;
        }
      } else if (tmpObj instanceof ConnectionFileChanger) {
        if (updateNow == true)
          tmpSet.updateTree();
      } else if (tmpObj instanceof InfoReader) {
        if (updateNow == true)
          tmpSet.updateTree();
      } else if (tmpObj instanceof OptionFileChanger) {
        if (updateNow == true)
          tmpSet.updateTree();
      }
    }
    return null;
  }

  public void refreshTreeNodeByActive(boolean updateNow) throws IOException {
    DefaultMutableTreeNode fileSetNode = (DefaultMutableTreeNode)
        this.activeTreePath[this.activeTreePath.length - 1];
    this.refreshTreeNode(fileSetNode, updateNow);
  }

  public DefaultMutableTreeNode refreshTreeRoot(boolean updateNow) throws IOException {
    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) this.viewProjectList.get(0).getFileSetView().getFileSetTree().getModel().getRoot();
    DefaultMutableTreeNode lastAddedNode = null;
    int childCount = rootNode.getChildCount();
    for (int i = 0; i < childCount; i++) {
      lastAddedNode = this.refreshTreeNode((DefaultMutableTreeNode) rootNode.getChildAt(i), updateNow);
    }
    return lastAddedNode;
  }

  public void refreshTreeSelection(TreePath[] selTreePaths) {
    this.viewProjectList.get(0).getFileSetView().refreshSelection(selTreePaths);
  }

  public void removeAllFilesFromFileSetByActive(boolean updateNow) {
    DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) this.activeTreePath[this.activeTreePath.length - 1];
    this.viewProjectList.get(0).getFileSetView().removeAllNodes(tmpNode, updateNow);
  }

  public void removeAllInfoReaderByActive(boolean updateNow) throws IOException {
    DefaultMutableTreeNode tmpNode = (DefaultMutableTreeNode) this.activeTreePath[this.activeTreePath.length - 1];
    if (tmpNode == null || !(tmpNode.getUserObject() instanceof RasterFileContainer)) {
      throw new IOException("View: removeAllInfoReaderByActive: ERROR");
    }
    this.viewProjectList.get(0).getFileSetView().removeAllInfoReader(tmpNode, updateNow);
  }

  public void removeAllInfoReaderOnFile(int fileIndex, int fileSetIndex, boolean updateNow) throws IOException {
    this.viewProjectList.get(0).getFileSetView().removeAllInfoReader(fileIndex, fileSetIndex, updateNow);
  }

  public DefaultMutableTreeNode replaceRasterFile(DefaultMutableTreeNode oldFileNode,
                                                  IGisFileContainer newFile) throws IOException {
    DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) oldFileNode.getParent();
    Object obj = parentNode.getUserObject();
    ViewFileSet tmpSet = this.viewProjectList.get(0).getFileSetView();
    JTree tree = tmpSet.getFileSetTree();
    boolean isExpanded = tree.isExpanded(new TreePath(oldFileNode.getPath()));

    if (!(obj instanceof GenericGisFileSet)) {
      throw new IOException("Parent User Object is no FileSet");
    }

    GenericGisFileSet fileSet = (GenericGisFileSet) obj;

    if (!(fileSet.getFileList().contains(newFile))) {
      throw new IOException("File: " + newFile + " not Found in FileSet " + fileSet.getName());
    }
    if (parentNode != null) {
      int fileNodeIndex = parentNode.getIndex(oldFileNode);
      tmpSet.removeNodeIter(oldFileNode);
      DefaultMutableTreeNode newFileNode = tmpSet
          .addGisFileAndInfoReader(fileNodeIndex, parentNode, newFile, true);
      if (isExpanded == true)
        tmpSet.expandNode(newFileNode, true);

      return newFileNode;
    }
    return null;
  }

  /**
   * Sets the text in the Status Label.
   */
  public void setStatusText(String text) {
    this.menuView.getStatusLabel().setText(text);
  }


  public void testField() {
    Dimension dim = this.viewProjectList.get(0).getImageView().getSize();
    this.viewProjectList.get(0).getImageView().getBounds().getSize();
  }

}

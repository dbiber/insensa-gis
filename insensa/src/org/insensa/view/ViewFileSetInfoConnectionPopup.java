/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import org.insensa.infoconnections.InfoConnection;

import javax.swing.*;
import java.util.ResourceBundle;


public class ViewFileSetInfoConnectionPopup extends JPopupMenu {
    private JMenuItem itemExecute;

    public ViewFileSetInfoConnectionPopup() {
        super();
        this.initItems();
    }

    public JMenuItem getItemExecute() {
        return this.itemExecute;
    }

    private void initItems() {
        this.itemExecute = new JMenuItem("Execute");
        this.add(this.itemExecute);
        this.itemExecute
                .setIcon(new ImageIcon(ResourceBundle.getBundle("img")
                        .getString("infoConnection.execute")));
    }

    public void setEnable(InfoConnection connection, boolean enable) {
        if (enable) {
            this.itemExecute.setEnabled(!connection.isUsed());
        }
    }

}

/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.extensions;

import org.insensa.view.View;
import org.insensa.view.dialogs.SettingsDialog;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.text.BadLocationException;

public interface IViewer {


  /**
   * This method will be executed by the Controller. All initial functions, e.g. Creating Component
   * and View Components should take place here. This method should never be empty.
   * <p>
   * init is the first method in the creation process and gets called before startView or any
   * resizing method </p>
   *
   * @param componentDim the dimension of the parent ViewerFrame
   * @see ViewerFrame
   * @see java.awt.Dimension
   */
  void init(Dimension componentDim) throws IOException;

  /**
   * This method will be used by the ViewerFrame to put Components in separate Layers. The Image
   * Frame will do this automatically. The order in the array will be the same as the corresponding
   * layer
   *
   * @return any child components that should be put in different layers of an ViewerFrame
   */
//  ImageComponent[] getChildComponents();

  /**
   * returns the main JComponent (parent JComponent). (eg JPanel, JScrollPane...)
   *
   * @return the main (parent) JComponent
   */
  JComponent getComponent();

  /**
   * Return a JComponent that can be used as a target for Drop actions.
   *
   * @return a JComponent uses as DropTarget
   */
  JComponent getDropTargetComponent();

  /**
   * @return Frame icon, displayed in the internal frame.
   */
  ImageIcon getFrameIcon();

  /**
   * This method allows to deal with Objects droped on the JComponent previsiuly defined in {@link
   * #getDropTargetComponent()}. <br> For example, it could be a List of IInfoReader or
   * RasterFileContainer
   */
  void onDropObjects(java.util.List<Object> objectList) throws IOException;

  /**
   * Sets the title for this ViewerFrame.
   */
  void setTitle(String title);

//  /**
//   * Gets called if the refresh button is pressed or got called somewhere .
//   *
//   * @param dim The inner dimension of the ViewerFrame
//   */
//  void refresh(Dimension dim) throws IOException;

  /**
   * Gets called if the refresh button is pressed or got called somewhere .
   */
  void refresh() throws IOException;

  /**
   * This is a secondary starting progress. Could be useful for starting new Threads. Functions that
   * do not need a secondary starting progress, this method can left empty. In that case you only
   * have to use the method {@link IViewer#init(Dimension)}
   *
   * @param dim                The Dimension of the Internal Frame
   * @param componentDimension The Component Dimension
   */
  void startView(Dimension dim, Dimension componentDimension) throws IOException;

  /**
   * Gets called whenever the ViewerFrame is resizing.<br><br> It gets also called <b>ones</b>
   * after this View is placed into the ViewerFrame.<br> <b>(after the init and startView
   * processes)</b>
   *
   * @param dim The inner dimension of the ViewerFrame
   */
  void resizing(Dimension dim) throws IOException;

  /**
   * Reference to the Main View
   *
   * @see org.insensa.view.View
   */
  void setView(View view);

  void saveAsHtml(String filename) throws IOException, BadLocationException;

  /**
   * @return stored ViewerFrame.
   * @deprecated this should't be useful
   */
  ViewerFrame getViewerFrame();

  /**
   * Reference to the ViewerFrame
   *
   * @see org.insensa.view.extensions.ViewerFrame
   * @param parent
   */
  void setViewerFrame(ViewerFrame parent);

  SettingsDialog createSettingsDialog();

  JToolBar getToolBar();
}

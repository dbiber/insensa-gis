/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.extensions;

import org.insensa.view.View;
import org.insensa.view.dialogs.SettingsDialog;

import java.awt.Dimension;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.text.BadLocationException;

public abstract class AbstractImageView implements IViewer {

  protected View view;
  protected String title = "";
  protected ViewerFrame parentViewerFrame;

  /**
   * @see IViewer#setView(org.insensa.view.View)
   */
  @Override
  public void setView(View view) {
    this.view = view;
  }

  /**
   * @see IViewer#setTitle(java.lang.String)
   */
  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public void setViewerFrame(ViewerFrame parent) {
    this.parentViewerFrame = parent;
  }

  @Override
  public ViewerFrame getViewerFrame() {
    return this.parentViewerFrame;
  }

  @Override
  public JComponent getDropTargetComponent() {
    return null;
  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {

  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {

  }

  @Override
  public void saveAsHtml(String filename) throws IOException, BadLocationException {

  }

  @Override
  public SettingsDialog createSettingsDialog() {
    return null;
  }

  @Override
  public JToolBar getToolBar() {
    return null;
  }
}

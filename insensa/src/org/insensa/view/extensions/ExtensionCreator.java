package org.insensa.view.extensions;

import org.apache.commons.io.FilenameUtils;
import org.insensa.model.storage.JdomSaver;
import org.insensa.updater.main.VersionChecker;
import org.insensa.updater.utils.PathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

public class ExtensionCreator {

    private static final Logger log = LoggerFactory.getLogger(ExtensionCreator.class);

    private ExtensionEntity entity;

    public ExtensionCreator() {
    }

    public void setEntity(ExtensionEntity entity) {
        this.entity = entity;
    }

    private boolean checkAndCreateDir(File parentDir, String dir) {
        File newDir = new File(parentDir, dir);
        if (newDir.exists()) {
            log.error("Directory {} already exists", newDir);
            return false;
        }

        if (!newDir.mkdir()) {
            log.error("Error creating directory {}", newDir);
            return false;
        }
        return true;
    }

    public boolean createNewPlugin() {
        File pluginDir = new File(entity.getDirectoryPath());
        if (pluginDir.exists()) {
            log.error("Directory {} alleady exists", entity.getDirectoryPath());
            return false;
        }

        if (!pluginDir.mkdir()) {
            log.error("Error creating directory {}", pluginDir);
            return false;
        }

        if (!checkAndCreateDir(pluginDir, "scripts")) {
            return false;
        }
        if (!checkAndCreateDir(pluginDir, "xml")) {
            return false;
        }
        if (!checkAndCreateDir(pluginDir, "help")) {
            return false;
        }

        File extensions = new File(new File(pluginDir, "xml"), "extensions.xml");
        try {
            boolean create = extensions.createNewFile();
            if (!create) {
                return false;
            }
        } catch (IOException e) {
            log.error("Error creating extensions.xml", e);
            return false;
        }

        JdomSaver saver = new JdomSaver(extensions, "extensions");
//    saver.save("extensions", saver1 -> {
//    });
        saver.write();

        return true;
    }

    public boolean loadPlugin() throws IOException {
        boolean helpExists = true;
        File dir = new File(entity.getDirectoryPath());
        if (!dir.exists()) {
            return false;
        }

        File scriptDir = new File(dir, "scripts");
        if (!scriptDir.exists()) {
            return false;
        }

        File xmlDir = new File(dir, "xml");
        if (!xmlDir.exists()) {
            return false;
        }

        File helpDir = new File(dir, "help");
        if (!helpDir.exists()) {
            log.warn("Plugin {} does not have a help directory");
            helpExists = false;
        }

        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        File pluginsDir = PathUtils.getInstance().getPluginsDir();
        File pluginFile = new File(pluginsDir, entity.getName() + ".zip");


        if (pluginFile.exists()) {
            pluginFile.delete();
        }

        VersionChecker versionChecker = new VersionChecker();
        String fileAbsPath = FilenameUtils.separatorsToUnix(pluginFile.getAbsolutePath());
        if (versionChecker.getOsType() == VersionChecker.OS_WIN) {
            fileAbsPath = "/" + fileAbsPath;
        }
        File fileAbs = new File(fileAbsPath);
        if (fileAbs.exists()) {
            if(!fileAbs.delete()) {
                throw new RuntimeException("Error deleting file " + fileAbsPath);
            }
        }
        URI uri = URI.create("jar:file:" + fileAbsPath);

        try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {

            Files.copy(scriptDir.toPath(), zipfs.getPath("/scripts/"),
                    StandardCopyOption.REPLACE_EXISTING);
            Files.copy(xmlDir.toPath(), zipfs.getPath("/xml/"),
                    StandardCopyOption.REPLACE_EXISTING);
            if (helpExists) {
                Files.copy(helpDir.toPath(), zipfs.getPath("/help/"),
                        StandardCopyOption.REPLACE_EXISTING);
            }

            File[] scriptFiles = scriptDir.listFiles();
            for (File script : scriptFiles) {
                Path targetPath = Paths.get("/scripts/", script.getName());
                Files.copy(script.toPath(), zipfs.getPath(targetPath.toString()),
                        StandardCopyOption.REPLACE_EXISTING);
            }

            File[] xmlFiles = xmlDir.listFiles();
            for (File xmlFile : xmlFiles) {
                Path targetPath = Paths.get("/xml/", xmlFile.getName());
                Files.copy(xmlFile.toPath(), zipfs.getPath(targetPath.toString()),
                        StandardCopyOption.REPLACE_EXISTING);
            }

            if (helpExists) {
                File[] helpFiles = helpDir.listFiles();
                for (File helpFile : helpFiles) {
                    Path targetPath = Paths.get("/help/", helpFile.getName());
                    Files.copy(helpFile.toPath(), zipfs.getPath(targetPath.toString()),
                            StandardCopyOption.REPLACE_EXISTING);
                }
            }
            return true;
        }

    }
}

/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.extensions;



import org.insensa.view.dialogs.SettingsDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TooManyListenersException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class ViewerFrame extends JInternalFrame {

  private static final Logger log = LoggerFactory.getLogger(ViewerFrame.class);

  private IViewer viewer;
  private JTree tree;
  private JButton buttonRefresh;
  private JButton buttonSettings;
  private int numOfLayers = 0;


  public ViewerFrame(String title, IViewer viewer) throws IOException {
    super(title, true, true, true, true);
    this.viewer = viewer;
    viewer.setViewerFrame(this);
    this.initComponents();
    this.setFrameIcon(new ImageIcon(ResourceBundle.getBundle("img")
        .getString("dialog.title.html")));
  }

  public ViewerFrame(IViewer viewer) throws IOException {
    this("", viewer);
    this.viewer = viewer;
    viewer.setViewerFrame(this);
    this.initComponents();
  }

  private void initComponents() throws IOException {
    super.addComponentListener(new ViewerFrame.ComponentResizeListener());
    ViewerFrame.DropObjectListener dtListener = new ViewerFrame.DropObjectListener();
    Component dropComponent = this.viewer.getDropTargetComponent();
    if (dropComponent != null) {
      DropTarget newdt = new DropTarget();
      try {
        newdt.addDropTargetListener(dtListener);
      } catch (TooManyListenersException exception) {
        throw new IOException(exception.getCause());
      }
      dropComponent.setDropTarget(newdt);
    }

    JToolBar toolBar = this.viewer.getToolBar();
    if (toolBar == null) {
      toolBar = createToolbar();
    }


    JComponent componentToAdd;
    if (viewer instanceof JComponent) {
      componentToAdd = (JComponent) viewer;
    } else {
      componentToAdd = viewer.getComponent();
    }

    this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
    this.getContentPane().add(componentToAdd, BorderLayout.CENTER);
    this.pack();
  }

  private JToolBar createToolbar() {
    JToolBar toolBar = new JToolBar();
    toolBar.setRollover(true);
    this.buttonRefresh = new JButton();
    this.buttonRefresh.setIcon(new ImageIcon(ResourceBundle.getBundle("img")
        .getString("menu.refresh")));
    this.buttonRefresh.addActionListener(new ViewerFrame.RefreshActionListener());
    toolBar.add(this.buttonRefresh);

    SettingsDialog settingsDialog = this.viewer.createSettingsDialog();
    if (settingsDialog != null){
      this.buttonSettings = new JButton();
      this.buttonSettings.setIcon(new ImageIcon(ResourceBundle.getBundle("img")
          .getString("toolbar.viewer.settings")));
      this.buttonSettings.addActionListener(e -> {
        settingsDialog.setModal(true);
        settingsDialog.setVisible(true);
      });
      toolBar.add(this.buttonSettings);
    }

    return toolBar;
  }

  public Dimension getPaneDimension() {
    return this.getContentPane().getSize();
  }

  public void setFileSetTree(JTree tree) {
    this.tree = tree;
  }

  private class DropObjectListener extends DropTarget implements DropTargetListener {

    private static final long serialVersionUID = 1683521802088222001L;

    /**
     * @see java.awt.dnd.DropTarget#drop(java.awt.dnd.DropTargetDropEvent)
     */
    @Override
    public synchronized void drop(DropTargetDropEvent dtde) {
      TreePath[] treePaths = ViewerFrame.this.tree.getSelectionPaths();
      List<Object> objects = new ArrayList<Object>();
      for (TreePath treePath : treePaths) {
        Object[] treeObjects = treePath.getPath();
        Object lastObject = treeObjects[treeObjects.length - 1];
        if (lastObject instanceof DefaultMutableTreeNode) {
          objects.add(((DefaultMutableTreeNode) lastObject).getUserObject());
        }
      }
      try {
        ViewerFrame.this.viewer.onDropObjects(objects);
      } catch (IOException exception) {
        exception.printStackTrace();
      }
    }
  }

  private class RefreshActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event) {
      try {
        ViewerFrame.this.viewer.refresh();
        ViewerFrame.this.viewer.resizing(ViewerFrame.this.getContentPane().getSize());
      } catch (IOException exception) {
        log.error( "Error refreshing view", exception);
      }
    }

  }

  private class ComponentResizeListener extends ComponentAdapter {

    /**
     * @see java.awt.event.ComponentAdapter#componentResized(java.awt.event.ComponentEvent)
     */
    @Override
    public void componentResized(ComponentEvent event) {
      try {
        ViewerFrame.this.viewer.resizing(ViewerFrame.this.getContentPane().getSize());
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }
  }

}

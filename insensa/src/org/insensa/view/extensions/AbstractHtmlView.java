/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.extensions;

import org.insensa.view.View;
import org.insensa.view.dialogs.SettingsDialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ResourceBundle;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

public abstract class AbstractHtmlView implements IViewer {
  protected JEditorPane editorPane;
  protected JScrollPane scrollPane;
  protected HTMLDocument doc;
  protected Element body;
  protected JMenu save;
  protected JProgressBar progressBar;
  private HTMLEditorKit kit;
  private StyleSheet styleSheet;
  private JPopupMenu popupMenu;
  private JMenuItem saveAsHtml;

  @Override
  public JComponent getComponent() {
    return this.scrollPane;
  }

  @Override
  public JComponent getDropTargetComponent() {
    return this.editorPane;
  }

  private void initComponents() throws IOException {
    this.editorPane = new JEditorPane();
    this.editorPane.setEditable(false);
    this.scrollPane = new JScrollPane();
    this.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    this.scrollPane.setAutoscrolls(false);
    this.kit = new HTMLEditorKit();
    this.editorPane.setEditorKit(this.kit);
    this.styleSheet = this.kit.getStyleSheet();
    this.styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
    this.styleSheet.addRule("h1 {color: blue;}");
    this.styleSheet.addRule("h2 {color: #ff0000;}");
    this.styleSheet
        .addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");
    this.doc = (HTMLDocument) this.kit.createDefaultDocument();
    this.editorPane.setDocument(this.doc);

    // getting BodyElement
    Element[] roots = this.doc.getRootElements();// #0 is the HTML element,
    // #1
    // the bidi-root
    for (int i = 0; i < roots[0].getElementCount(); i++) {
      Element element = roots[0].getElement(i);
      if (element.getAttributes().getAttribute(StyleConstants.NameAttribute) == HTML.Tag.BODY) {
        this.body = element;
        break;
      }
    }
    if (this.body == null) {
      throw new IOException("can't find <body>");
    }

    this.popupMenu = new JPopupMenu();
    this.save = new JMenu("Save");
    this.saveAsHtml = new JMenuItem("As Html");
    this.saveAsHtml.addActionListener(new SaveAsHtmlActionListener());

    this.save.add(this.saveAsHtml);
    this.save.setIcon(new ImageIcon(ResourceBundle.getBundle("img").getString("menu.save")));

    this.popupMenu.add(this.save);
    this.editorPane.addMouseListener(new MouseOnHtml());

    this.scrollPane.setViewportView(this.editorPane);
  }

  public void initProgressBar() {
    this.progressBar = new JProgressBar();
    JPanel panel = new JPanel();
    GroupLayout layout = new GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            layout.createSequentialGroup().addContainerGap()
                .addComponent(this.progressBar,
                    javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            layout.createSequentialGroup().addContainerGap(105, Short.MAX_VALUE)
                .addComponent(this.progressBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                    28, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(68, 68, 68)));
    this.scrollPane.setViewportView(panel);
  }

  @Override
  public void saveAsHtml(String filename) throws IOException, BadLocationException {
    File outFile = new File(filename);
    if (!outFile.exists()) {
      if (!outFile.createNewFile()) {
        throw new IOException("Error creating file: \"" + filename + "\"");
      }
    }
    try (
        FileOutputStream outStream = new FileOutputStream(outFile);
    ) {
      this.kit.write(outStream, this.doc, 0, this.doc.getLength());
    }
  }

  protected void saveTableAsCsv(String filename, Element tableElement)
      throws IOException, BadLocationException {
    File outFile = new File(filename);
    if (!outFile.exists()) {
      if (!outFile.createNewFile()) {
        throw new IOException("Error creating file: \"" + filename + "\"");
      }
    }
    Writer writer = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8");
    BufferedWriter bufWriter = new BufferedWriter(writer);
    int tableTrCount = tableElement.getElementCount();
    for (int i = 0; i < tableTrCount; i++) {
      Element trElem = tableElement.getElement(i);
      int childCount = trElem.getElementCount();
      for (int j = 0; j < childCount; j++) {
        Element childElem = trElem.getElement(j);
        int start = childElem.getStartOffset();
        int end = childElem.getEndOffset();
        int lenght = end - start;
        String text = this.doc.getText(start, lenght);
        text = text.replaceAll("\n", "");
        if (j == childCount - 1) {
          bufWriter.write("\"" + text + "\"");
        } else {
          bufWriter.write("\"" + text + "\",");
        }
      }
      if (i < tableTrCount - 1) {
        bufWriter.newLine();
      }
    }
    bufWriter.flush();
    bufWriter.close();
  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {
  }

  @Override
  public void init(Dimension componentDim) throws IOException {
    this.initComponents();
  }

  @Override
  public void resizing(Dimension dim) throws IOException {

  }

  @Override
  public ViewerFrame getViewerFrame() {
    return null;
  }

  @Override
  public void setViewerFrame(ViewerFrame parent) {

  }

  @Override
  public void setView(View view) {

  }

  private class MouseOnHtml extends MouseAdapter {

    /**
     * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent event) {
      if (event.isPopupTrigger()) {
        AbstractHtmlView.this.popupMenu.show(event.getComponent(), event.getX(), event.getY());
      }
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent event) {
      if (event.isPopupTrigger()) {
        AbstractHtmlView.this.popupMenu.show(event.getComponent(), event.getX(), event.getY());
      }
    }
  }

  private class SaveAsHtmlActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      try {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        int choise = fileChooser.showSaveDialog(AbstractHtmlView.this.editorPane);
        if (choise == JFileChooser.APPROVE_OPTION) {
          File selFile = fileChooser.getSelectedFile();
          if (selFile.exists()) {
            int option = JOptionPane.showConfirmDialog(AbstractHtmlView.this.editorPane,
                "File already exits"
                + "do you really want do owerwrite this file?\n",
                "File Exists",
                JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
              if (!selFile.delete()) {
                JOptionPane.showMessageDialog(AbstractHtmlView.this.editorPane,
                    "Error Deleting File", "error",
                    JOptionPane.ERROR_MESSAGE);
              }
            } else {
              return;
            }
          }
          String fileName = selFile.getAbsolutePath();
          if (!fileName.endsWith(".html")) {
            fileName += ".html";
          }
          AbstractHtmlView.this.saveAsHtml(fileName);

        }
      } catch (IOException | BadLocationException e1) {
        e1.printStackTrace();
      }
    }
  }
  @Override
  public SettingsDialog createSettingsDialog() {
    return null;
  }

  @Override
  public JToolBar getToolBar() {
    return null;
  }
}

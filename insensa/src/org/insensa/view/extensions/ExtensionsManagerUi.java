package org.insensa.view.extensions;

import org.insensa.Environment;
import org.insensa.XMLProperties.XmlViewProperties;
import org.insensa.view.dialogs.SettingsDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ExtensionsManagerUi extends SettingsDialog {

  private static final Logger log = LoggerFactory.getLogger(ExtensionsManagerUi.class);

  private JPanel mainPanel;
  private JPanel panelButtons;
  private JTable table;
  private JScrollPane scrollPane;
  private DefaultTableModel model;
  private JButton buttonNew;
  private JButton buttonAdd;
  private JButton buttonRemove;
  private Vector<String> headerVec;

  public ExtensionsManagerUi() {
  }

  public ExtensionsManagerUi(Dialog dialog, boolean modal) {
    super(dialog, modal);
  }

  public ExtensionsManagerUi(Frame frame, boolean modal) {
    super(frame, modal);
  }

  private List<ExtensionEntity> getExtensions() {
    List<ExtensionEntity> entities = new ArrayList<>();
    Vector data = model.getDataVector();
    for (Object items : data) {
      Vector<String> sItem = (Vector<String>) items;
      ExtensionEntity entity = new ExtensionEntity(sItem.get(1), "", sItem.get(0));
      entities.add(entity);
    }
    return entities;
  }

  public void start() {
    initComponents(initComponents());
    getButtonOk().addActionListener(e -> {
      List<ExtensionEntity> entities = getExtensions();
      try {
        Environment.getRootProperties().saveLocalExtensions(entities);
      } catch (IOException e1) {
        log.error("Error saving local extensions", e1);
      }
      setVisible(false);
    });
    getButtonCancel().addActionListener(e -> {
      setVisible(false);
    });

    setVisible(true);
  }

  private File getPluginDir() {
    String path = "";
    XmlViewProperties viewProperties = Environment.getViewProperties();
    if (viewProperties != null) {
      path = viewProperties.getLastImportedFolderPath();
    }
    JFileChooser fileChooser = new JFileChooser(path);
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fileChooser.showDialog(null, "Select");
    File selFile = fileChooser.getSelectedFile();
    if (viewProperties != null && selFile != null) {
      viewProperties.setLastImportedFolderPath(selFile.getAbsolutePath());
    }
    return selFile;
  }

  private JPanel initComponents() {
    panelButtons = new JPanel(new FlowLayout());
    buttonAdd = new JButton("Add");
    buttonAdd.addActionListener(e -> {
      File selFile = getPluginDir();
      if (selFile == null) {
        return;
      }
      model.addRow(new String[]{selFile.getName(), selFile.getAbsolutePath()});
//      model.addRow(new String[headerVec.size()]);
    });

    buttonNew = new JButton("New");
    buttonNew.addActionListener(e -> {
      File selDir = getPluginDir();
      if (selDir == null) {
        return;
      }
      String projectName = JOptionPane.showInputDialog(this, "ProjectName:");
      if (projectName == null) {
        return;
      }
      File pluginDir = new File(selDir, projectName);
      ExtensionEntity entity = new ExtensionEntity(pluginDir.getAbsolutePath(), "", projectName);
      ExtensionCreator creator = new ExtensionCreator();
      creator.setEntity(entity);
      if (creator.createNewPlugin()) {
        model.addRow(new String[]{projectName, entity.getDirectoryPath()});
        try {
          Environment.getRootProperties().saveLocalExtensions(getExtensions());
        } catch (IOException e1) {
          log.error("Error saving extensions", e1);
        }
      } else {
        JOptionPane.showMessageDialog(this,
            "Could not create plugin directory, \nCheck log files.",
            "Error creating plugin",
            JOptionPane.ERROR_MESSAGE);
        return;
      }
    });

    buttonRemove = new JButton("Remove");
    buttonRemove.addActionListener(e -> {
      int selRow = table.getSelectedRow();
      model.removeRow(selRow);
    });
    panelButtons.add(buttonNew);
    panelButtons.add(buttonAdd);
    panelButtons.add(buttonRemove);
    mainPanel = new JPanel(new BorderLayout());
    headerVec = new Vector<>();
    headerVec.add("Plugin");
    headerVec.add("Path");
    List<ExtensionEntity> entities = Environment.getRootProperties().getLocalExtensions();
    Vector<Vector<String>> data = new Vector<>();
    for (ExtensionEntity entity : entities) {
      Vector<String> row = new Vector<>();
      row.add(entity.getName());
      row.add(entity.getDirectoryPath());
      data.add(row);
    }

    model = new DefaultTableModel(data, headerVec) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return column == 0;
      }
    };

    table = new JTable(model);
    scrollPane = new JScrollPane(table);
    table.setFillsViewportHeight(true);
    Dimension dim = scrollPane.getPreferredSize();
    table.setPreferredScrollableViewportSize(new Dimension(dim.width, 150));
//    scrollPane.setPreferredSize(new Dimension(dim.width, 100));
//    scrollPane.setMaximumSize(new Dimension(dim.width, 100));
    mainPanel.add(scrollPane, BorderLayout.CENTER);
    mainPanel.add(panelButtons, BorderLayout.PAGE_END);
    mainPanel.setBackground(Color.red);

    table.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent mouseEvent) {
        JTable table = (JTable) mouseEvent.getSource();
        Point point = mouseEvent.getPoint();
        int row1 = table.rowAtPoint(point);
        if (mouseEvent.getClickCount() == 2 && row1 != -1) {
          int col = table.getSelectedColumn();
//          int row = table.getSelectedRow();

          Object object = model.getValueAt(row1, col);
          if (col == 1) {
            File selFile = getPluginDir();
            if (selFile == null) {
              return;
            }
            model.setValueAt(selFile.getAbsolutePath(), row1, col);
          }
        }
      }
    });

    return mainPanel;
//    table.getModel().addTableModelListener(e -> notifyValueChanged());

  }

}

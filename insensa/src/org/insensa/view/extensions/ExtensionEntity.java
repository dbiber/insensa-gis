package org.insensa.view.extensions;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.IStorageData;

public class ExtensionEntity implements IStorageData {

  private String directoryPath;
  private String version;
  private String name;

  public ExtensionEntity() {
  }

  public ExtensionEntity(String directoryPath, String version, String name) {
    this.directoryPath = directoryPath;
    this.version = version;
    this.name = name;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    this.directoryPath = restorer.loadString("directory")
        .orElseThrow(() -> new RuntimeException("Could not read directoryPath"));
    this.name = restorer.loadString("name")
        .orElseThrow(() -> new RuntimeException("Could not read name"));
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("name", name);
    saver.save("directory", directoryPath);
  }

  public String getDirectoryPath() {
    return directoryPath;
  }

  public String getName() {
    return name;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import javax.swing.JLabel;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;


public class StatusLabel extends JLabel implements WorkerStatus
{


	private static final long serialVersionUID = 935642143145655457L;

	/**
	 * @param sText
	 */
	public StatusLabel(String sText)
	{
		super(sText);
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#endPause()
	 */
	@Override
	public void endPause()
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#endProgress()
	 */
	@Override
	public void endProgress()
	{
		super.setText("ProgressEnded");

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#errorProcess()
	 */
	@Override
	public void errorProcess()
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#errorProcess(java.lang.String)
	 */
	@Override
	public void errorProcess(String message)
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#errorProcess(java.lang.Throwable)
	 */
	@Override
	public void errorProcess(Throwable e)
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#getChildWorkerStatusList(int)
	 */
	@Override
	public WorkerStatusList getChildWorkerStatusList(int index)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#getParentWorkerStatusList()
	 */
	@Override
	public WorkerStatusList getParentWorkerStatusList()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#getProgress()
	 */
	@Override
	public float getProgress()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#getStepSize()
	 */
	@Override
	public float getStepSize()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#refreshPercentage(float)
	 */
	@Override
	public void refreshPercentage(float percent)
	{
		super.setText(Float.toString(percent));
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#removeChildrenWorkerStatusLists()
	 */
	@Override
	public void removeChildrenWorkerStatusLists()
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#setChildrenWorkerStatusLists(int)
	 */
	@Override
	public void setChildrenWorkerStatusLists(int numOfLists)
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#setParentWorkerStatusList(org.insensa.WorkerStatusList)
	 */
	@Override
	public void setParentWorkerStatusList(WorkerStatusList mProgress)
	{
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @see org.insensa.WorkerStatus#setProgressName(java.lang.String)
	 */
	@Override
	public void setProgressName(String name)
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#setStepSize(float)
	 */
	@Override
	public void setStepSize(float stepSize)
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#startPause(java.lang.String)
	 */
	@Override
	public void startPause(String text)
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatus#startProcess()
	 */
	@Override
	public void startProcess()
	{

	}

}

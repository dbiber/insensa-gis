/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;


import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RefreshTreeNodeListener implements IProcessFinishedListener {
  private static final Logger log = LoggerFactory.getLogger(RefreshTreeNodeListener.class);

  private List<DefaultMutableTreeNode> nodeList;
  private View view;

  public RefreshTreeNodeListener(DefaultMutableTreeNode node, View view) {
    this.view = view;
    this.nodeList = new ArrayList<DefaultMutableTreeNode>();
    this.nodeList.add(node);
  }

  public RefreshTreeNodeListener(List<DefaultMutableTreeNode> nodeList, View view) {
    this.view = view;
    this.nodeList = new ArrayList<DefaultMutableTreeNode>();
    this.nodeList.addAll(nodeList);
  }

  @Override
  public void canceled() {
    try {
      for (DefaultMutableTreeNode iNode : this.nodeList)
        this.view.refreshTreeNode(iNode, true);
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

  @Override
  public void done() {

    try {
      for (DefaultMutableTreeNode iNode : this.nodeList)
        this.view.refreshTreeNode(iNode, true);
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

}

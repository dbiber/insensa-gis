/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;





public class AboutDialog extends javax.swing.JDialog
{
  private static final Logger log = LoggerFactory.getLogger(AboutDialog.class);

	private static final long serialVersionUID = -439499544828810984L;
	private javax.swing.JButton buttonClose;
	private javax.swing.JEditorPane editorPaneHead;
	private javax.swing.JEditorPane editorPaneFoot;
	private javax.swing.JLabel labelImage;
	private javax.swing.JPanel mainPanel;
	private javax.swing.JScrollPane scrollPaneHead;
	private javax.swing.JScrollPane scrollPaneFoot;




	/** Creates new form AboutDialog */
	public AboutDialog(java.awt.Frame parent, boolean modal)
	{
		super(parent, modal);
		this.initComponents();
		this.setTitle("About");
	}

	private void initComponents()
	{

		this.mainPanel = new javax.swing.JPanel();
		this.labelImage = new javax.swing.JLabel();
		this.scrollPaneHead = new javax.swing.JScrollPane();
		this.editorPaneHead = new javax.swing.JEditorPane();
		this.scrollPaneFoot = new javax.swing.JScrollPane();
		this.editorPaneFoot = new javax.swing.JEditorPane();
		this.buttonClose = new javax.swing.JButton();
		this.buttonClose.addActionListener(new CloseDialogActionListener());
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.labelImage.setIcon(new javax.swing.ImageIcon(ResourceBundle.getBundle("img").getString("about.logo")));

		this.scrollPaneHead.setBorder(null);
		this.scrollPaneHead.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.scrollPaneHead.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.editorPaneHead.setBorder(null);
		this.editorPaneHead.setEditable(false);
		this.editorPaneHead.setOpaque(false);
		HyperlinkListener listener = new LinkListener();
		try
		{
			this.editorPaneHead.setPage("file:about/about.html");
			editorPaneHead.addHyperlinkListener(listener);
		} catch (IOException e)
		{
			log.error("UNKNOWN",e);
		}
		this.scrollPaneHead.setViewportView(this.editorPaneHead);

		this.scrollPaneFoot.setBorder(null);
		this.scrollPaneFoot.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.scrollPaneFoot.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.editorPaneFoot.setBackground(new java.awt.Color(204, 204, 204));
		this.editorPaneFoot.setContentType("text/html");
		this.editorPaneFoot.setEditable(false);
		this.editorPaneFoot.setOpaque(false);
		try
		{
			this.editorPaneFoot.setPage("file:about/foot.html");
			editorPaneFoot.addHyperlinkListener(listener);
		} catch (IOException e)
		{
			log.error("UNKNOWN",e);
		}
		this.scrollPaneFoot.setViewportView(this.editorPaneFoot);

		this.buttonClose.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.buttonClose.setText("Close");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.mainPanel);
		this.mainPanel.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								jPanel1Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(this.labelImage)
																		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(this.scrollPaneHead, javax.swing.GroupLayout.DEFAULT_SIZE, 282,
																				Short.MAX_VALUE).addGap(24, 24, 24))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(this.scrollPaneFoot, javax.swing.GroupLayout.DEFAULT_SIZE, 491,
																				Short.MAX_VALUE).addContainerGap()))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
												jPanel1Layout.createSequentialGroup().addComponent(this.buttonClose).addContainerGap()))));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						jPanel1Layout
								.createSequentialGroup()
								.addGap(12, 12, 12)
								.addGroup(
										jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(this.scrollPaneHead, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
												.addComponent(this.labelImage)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(this.scrollPaneFoot, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE).addGap(12, 12, 12)
								.addComponent(this.buttonClose).addContainerGap()));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
		this.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.mainPanel,
				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.mainPanel,
				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		this.pack();
	}
	
	private class CloseDialogActionListener implements ActionListener
	{

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			setVisible(false);
		}
	}
	
	private class LinkListener implements HyperlinkListener
	{

		/**
		 * @see javax.swing.event.HyperlinkListener#hyperlinkUpdate(javax.swing.event.HyperlinkEvent)
		 */
		@Override
		public void hyperlinkUpdate(HyperlinkEvent e)
		{
			String file = e.getURL().getFile();
			if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
			{
				if(file.contains("about/license.html"))
				{
					LicenseDialog dialog = new LicenseDialog(null, true);
					dialog.setLocationRelativeTo(null);
					dialog.setVisible(true);
				}
				else if(file.contains("about/thirdParty.html"))
				{
					ThirdPartyDialog dialog = new ThirdPartyDialog(null, true);
					dialog.setLocationRelativeTo(null);
					dialog.setVisible(true);
				}
				else if(Desktop.isDesktopSupported())
				{
					Desktop desktop = Desktop.getDesktop();
					if(desktop.isSupported(Desktop.Action.BROWSE))
					{
						try
						{
							desktop.browse(e.getURL().toURI());
						} catch (IOException e1)
						{
						} catch (URISyntaxException e1)
						{
						}
					}
				}
			}
		}
	}

}

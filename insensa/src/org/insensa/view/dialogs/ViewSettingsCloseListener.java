package org.insensa.view.dialogs;


public interface ViewSettingsCloseListener {
  enum Result {
    BUTTON_OK,
    BUTTON_CANCEL,
    WINDOW_CLOSING
  }

  void closed(Result result);
}

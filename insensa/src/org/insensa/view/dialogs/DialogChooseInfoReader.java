/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;


import org.insensa.Environment;
import org.insensa.GisFileType;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.controller.ControllerFileEditing;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.Languages;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;


@SuppressWarnings("serial")
public class DialogChooseInfoReader extends SettingsDialog {
  private static final Logger log = LoggerFactory.getLogger(DialogChooseInfoReader.class);

  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JTable jTable1;
  private Model model;
  private IGisFileContainer rasterFile;
  private View view;
  private GisFileType gisFileType;

  public DialogChooseInfoReader(GisFileType gisFileType, Model model, View view) {
    this.model = model;
    this.view = view;
    super.setTitle("Choose Information");
    this.setSize(600, 400);
    this.gisFileType = gisFileType;
    super.initComponents(this.initComponents());
    super.setHeadTitle("Choose Information");
    super.getButtonOk().addActionListener(new OkButtonListener());
    getButtonCancel().addActionListener(e -> setVisible(false));
  }

  private JPanel initComponents() {
    JPanel panel = new JPanel();

    this.jScrollPane2 = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    List<String> infoList = ExtensionManager.getInstance()
        .getUsableInfoReaderList(ServiceType.EXEC, gisFileType);

    Vector<Vector<Object>> dataVector = new Vector<>();
    Vector<Object> row;
    for (String infoName: infoList) {
//      String upperCaseName = infoName.substring(0, 1).toUpperCase() + infoName.substring(1);
      row = new Vector<>();
      row.add(infoName);
//      row.add(upperCaseName);
      row.add(false);
      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("Information");
    titleVec.add("Selected");
    this.jTable1.setModel(new InfoReaderTable(dataVector, titleVec));
    this.jTable1.setName("jTable1"); // NOI18N
    this.jScrollPane2.setViewportView(this.jTable1);
    this.jTable1.getColumnModel().getColumn(0).setHeaderValue("Information"); // NOI18N
    this.jTable1.getColumnModel().getColumn(1).setHeaderValue("Selected"); // NOI18N

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2,
        javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup().addComponent(this.jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)));

    return panel;
  }

  public class InfoReaderTable extends DefaultTableModel {

    @SuppressWarnings("rawtypes")
    Class[] types = new Class[]
        {java.lang.String.class, java.lang.Boolean.class};

    public InfoReaderTable(Vector<Vector<Object>> dataVector, Vector<String> titleVec) {
      super(dataVector, titleVec);
    }

    @SuppressWarnings("unchecked")
    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0) {
        return false;
      }
      return super.isCellEditable(row, column);
    }

  }

  private class OkButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      int rowCount = DialogChooseInfoReader.this.jTable1.getModel().getRowCount();
      DialogChooseInfoReader.this.rasterFile = DialogChooseInfoReader.this.model.getActiveGisFileInformation();
      JTree tree = DialogChooseInfoReader.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(), DialogChooseInfoReader.this.view,
          DialogChooseInfoReader.this.model);
      int addedInfos = 0;
      for (int i = 0; i < rowCount; i++) {
        try {
          if ((Boolean) (DialogChooseInfoReader.this.jTable1.getModel().getValueAt(i, 1))) {

            if (tree.getSelectionCount() == 1) {
              if (fileEdit.getFileList().get(0).isLocked()) {
                JOptionPane
                    .showMessageDialog(DialogChooseInfoReader.this.view, "This file is locked", "File Error", JOptionPane.ERROR_MESSAGE);
                DialogChooseInfoReader.this.setVisible(false);
                return;
              }

              String infoName = DialogChooseInfoReader.this.jTable1.getModel().getValueAt(i, 0).toString();
//              String lowerCaseName = infoName.substring(0, 1).toLowerCase() + infoName.substring(1);
//              DialogChooseInfoReader.this.model.addInfoToFileByActive(lowerCaseName);
              DialogChooseInfoReader.this.model.addInfoToFileByActive(infoName);
            } else {
              for (IGisFileContainer file: fileEdit.getFileList()) {
                if (!file.isLocked()) {
                  String infoName = DialogChooseInfoReader.this.jTable1.getModel().getValueAt(i, 0).toString();
//                  String lowerCaseName = infoName.substring(0, 1).toLowerCase() + infoName.substring(1);
//                  DialogChooseInfoReader.this.model.addInfoToFileByFile(file, lowerCaseName);
                  DialogChooseInfoReader.this.model.addInfoToFileByFile(file, infoName);
                  addedInfos++;
                }
              }
              if (addedInfos <= 0) {
                JOptionPane.showMessageDialog(DialogChooseInfoReader.this.view, "All files are locked", "InfoReadder Adding Error",
                    JOptionPane.ERROR_MESSAGE);
                DialogChooseInfoReader.this.setVisible(false);
                return;
              } else if (addedInfos < fileEdit.getFileList().size()) {
                JOptionPane.showMessageDialog(DialogChooseInfoReader.this.view, "Some files were locked", "InfoReadder Adding Error",
                    JOptionPane.ERROR_MESSAGE);
                DialogChooseInfoReader.this.setVisible(false);
                return;
              }
            }
          }

        } catch (IOException e1) {
          DialogChooseInfoReader.this.getLabelStatus().setText("ERROR SETTING InfoReader: " + e1);
          log.error("ERROR SETTING InfoReader:", e1);
        }
      }
      DialogChooseInfoReader.this.setVisible(false);
    }

  }

}

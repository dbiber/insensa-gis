/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;


import org.insensa.Model;
import org.insensa.commands.CreateUserCommand;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;


public class CreateUserDialog extends SettingsDialog {
  private static final Logger log = LoggerFactory.getLogger(CreateUserDialog.class);

  private static final long serialVersionUID = -8708089616417109456L;
  private JPanel panelUser;
  private JTextField textFieldUser;
  private JButton buttonUser;
  private JPanel panelWorkspace;
  private JTextField textFieldWorkspace;
  private JButton buttonWorkspace;
  private String userName;
  private String workspacePath;
  private Model model;

  public CreateUserDialog(Model model, Frame frame, boolean modal) {
    super(frame, modal);
    this.model = model;
    this.initComponents(this.initComponents());
    super.setHeadTitle("Create New User");
    super.setTitle("Create New User");
    super.getButtonOk().addActionListener(new OkButtonListener());
  }

  private JPanel initComponents() {
    this.panelUser = new JPanel();
    this.textFieldUser = new JTextField("");
    this.textFieldUser.setEditable(false);
    this.buttonUser = new JButton("Add");
    this.buttonUser.addActionListener(new NewUserListener());

    GroupLayout userLayout = new GroupLayout(this.panelUser);
    this.panelUser.setLayout(userLayout);

    /*
      USER PANEL VERTICAL
     */
    Group verticalGroup = userLayout.createParallelGroup()
        .addComponent(this.textFieldUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(this.buttonUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE);

    Group vertivalGroup2 = userLayout.createSequentialGroup().addContainerGap().addGroup(verticalGroup).addContainerGap();

    userLayout.setVerticalGroup(vertivalGroup2);

    /*
     * USER PANEL HORIZONTAL
     */
    Group horiGroup = userLayout.createSequentialGroup().addContainerGap().addComponent(this.textFieldUser, 150, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addGap(15).addComponent(this.buttonUser).addContainerGap();

    userLayout.setHorizontalGroup(horiGroup);
    this.panelUser.setBorder(BorderFactory.createTitledBorder("New User"));

    this.panelWorkspace = new JPanel();
    this.textFieldWorkspace = new JTextField("");
    this.textFieldWorkspace.setEditable(false);
    this.buttonWorkspace = new JButton("Select");
    this.buttonWorkspace.addActionListener(new SelectWorkspacePath());

    GroupLayout workspaceLayout = new GroupLayout(this.panelWorkspace);
    this.panelWorkspace.setLayout(workspaceLayout);

    /*
     * WORKSPACE PANEL VERTICAL
     */
    Group verticalGroupWS = workspaceLayout.createParallelGroup()
        .addComponent(this.textFieldWorkspace, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(this.buttonWorkspace, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE);

    Group verticalGroupWS2 = workspaceLayout.createSequentialGroup().addContainerGap().addGroup(verticalGroupWS).addContainerGap();
    workspaceLayout.setVerticalGroup(verticalGroupWS2);

    /*
     * WORKSPACE PANEL HORIZONTAL
     */
    Group horiGroupWS = workspaceLayout.createSequentialGroup().addContainerGap()
        .addComponent(this.textFieldWorkspace, 150, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(15).addComponent(this.buttonWorkspace)
        .addContainerGap();

    workspaceLayout.setHorizontalGroup(horiGroupWS);
    this.panelWorkspace.setBorder(BorderFactory.createTitledBorder("Select Workspace"));

    /*
     * MAIN PANEL
     */
    JPanel mainPanel = new JPanel();
    GroupLayout mainLayout = new GroupLayout(mainPanel);
    mainPanel.setLayout(mainLayout);

    /*
     * MAIN PANEL VERTICAL
     */
    Group verMainGroup = mainLayout.createSequentialGroup().addContainerGap().addComponent(this.panelUser).addGap(20).addComponent(this.panelWorkspace)
        .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);

    mainLayout.setVerticalGroup(verMainGroup);

    /*
     * MAIN PANEL HORIZONTAL
     */
    Group horMainParGroup = mainLayout.createParallelGroup().addComponent(this.panelUser).addComponent(this.panelWorkspace);

    Group horMainGroup = mainLayout.createSequentialGroup().addContainerGap().addGroup(horMainParGroup).addContainerGap();

    mainLayout.setHorizontalGroup(horMainGroup);

    return mainPanel;
  }

  private class NewUserListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      CreateUserDialog.this.userName = JOptionPane.showInputDialog(CreateUserDialog.this, "Set user name", "Set user name",
          JOptionPane.INFORMATION_MESSAGE);
      CreateUserDialog.this.textFieldUser.setText(CreateUserDialog.this.userName);
    }
  }

  private class OkButtonListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      CreateUserCommand command = new CreateUserCommand(CreateUserDialog.this.model, CreateUserDialog.this.userName, CreateUserDialog.this.workspacePath);
      try {
        ViewCommandManager.executeCommands(CreateUserDialog.this, new RefreshGuiListener(), command);
      } catch (Exception e1) {
        log.error( "UNKNOWN", e1);
      }
      CreateUserDialog.this.setVisible(false);
    }
  }

  private class RefreshGuiListener implements IProcessFinishedListener {

    /**
     * @see org.insensa.view.dialogs.helper.IProcessFinishedListener#canceled()
     */
    @Override
    public void canceled() {

    }

    /**
     * @see org.insensa.view.dialogs.helper.IProcessFinishedListener#done()
     */
    @Override
    public void done() {

    }

  }

  private class SelectWorkspacePath implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      JFileChooser fc = new JFileChooser();
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      int answer = fc.showOpenDialog(CreateUserDialog.this);
      if (answer == JFileChooser.APPROVE_OPTION) {
        File selFile = fc.getSelectedFile();
        CreateUserDialog.this.workspacePath = selFile.getAbsolutePath();
        CreateUserDialog.this.textFieldWorkspace.setText(CreateUserDialog.this.workspacePath);
      }

    }
  }
}

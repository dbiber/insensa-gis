/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import java.io.File;
import java.util.ResourceBundle;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileView;

import org.insensa.imports.FileDescription;


/**
 * @author dennis
 *
 */
public class FolderFileView extends FileView
{

	private FileDescription fileDescription;
	private int depthCnt = 0;
	

	public FolderFileView(FileDescription fileDescription)
	{
		this.fileDescription=fileDescription;
	}
	
	/**
	 *
	 * @see javax.swing.filechooser.FileView#getIcon(java.io.File)
	 */
	@Override
	public Icon getIcon(File f)
	{
		depthCnt=0;
		ResourceBundle bundle=ResourceBundle.getBundle("img");
		if (f.isDirectory())
		{
			if(depthCnt>=fileDescription.depth)
				return super.getIcon(f);
			depthCnt++;
			if(findInSubfolder(f))
				return new ImageIcon(bundle.getString("dialog.title.map"));
			
//			for (File element : subSubFileArray)
//			{
//				if (element.getName().equals("w001001.adf"))
//				{
//					return new ImageIcon(bundle.getString("dialog.title.map"));
//				}
//			}
		}
		return super.getIcon(f);
	}
	
	private boolean findInSubfolder(File folder)
	{
		File subSubFileArray[] = folder.listFiles();
		for (File element : subSubFileArray)
		{
			if(element.isFile())
			{
				for(String iExt : fileDescription.fileExtensions)
				{
					if(element.getName().endsWith(iExt))
						return true;
				}
			}
			else if(element.isDirectory())
			{
				if(depthCnt>=fileDescription.depth)
					return false;
				else
					return findInSubfolder(element);
			}
		}
		return false;
	}
}

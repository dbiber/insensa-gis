/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * <code>NumberColumnFormat</code> makes available a simple NumberRenderer and
 * NumberEditor for JTable. Usage example:
 *
 * <pre>
 * <code>
 * &NumberColumnFormat numberColumnFormat = new NumberColumnFormat();
 * &table.setDefaultRenderer(Number.class, numberColumnFormat.getRenderer());
 * &table.setDefaultEditor(Number.class, numberColumnFormat.getEditor());
 * </code>
 * </pre>
 */
public class NumberColumnFormat {
  private DecimalFormat formatterR, formatterE;
  private DefaultTableCellRenderer renderer;
  private DecimalFormatSymbols numberSymbols;
  private DefaultCellEditor editor;

  {
    this.numberSymbols = new DecimalFormatSymbols();
    this.numberSymbols.setGroupingSeparator(',');
    this.numberSymbols.setDecimalSeparator('.');
  }

  /**
   * Constructs a <code>NumberColumnFormat</code> using the pattern
   * "###,###,###.##" and the default number format symbols for the default
   * locale.
   */
  public NumberColumnFormat() {
    this(null);
  }

  /**
   * Constructs a <code>NumberColumnFormat</code> using the given pattern and
   * the default number format symbols for the default locale.
   *
   * @param pattern the pattern describing the number format, "###,###,###.##" if
   *                null
   */

  public NumberColumnFormat(String pattern) {
    this(pattern, pattern);
  }

  /**
   * Constructs a <code>NumberColumnFormat</code> using the given patterns and
   * the default number format symbols for the default locale.
   *
   * @param patternR the pattern describing the number format of the renderer,
   *                 "###,###,###.##" if null
   * @param patternE the pattern describing the number format of the editor,
   *                 "#########.##" if null
   */
  public NumberColumnFormat(String patternR, String patternE) {
    if (patternR == null)
      patternR = "###,###,###.##";
    this.formatterR = new DecimalFormat(patternR, this.numberSymbols);
    this.formatterR.setDecimalFormatSymbols(this.numberSymbols);
    int d = patternR.split("\\.")[1].length();
    this.formatterR.setMinimumFractionDigits(d);
    this.formatterR.setMaximumFractionDigits(d);
    if (patternE == null)
      patternE = "#########.##";
    this.formatterE = new DecimalFormat(patternE, this.numberSymbols);

    this.renderer = new NumberRenderer();
    this.editor = new NumberEditor();
  }

  /**
   * Returns the number cell editor.
   *
   * @return the table number cell editor
   */
  public DefaultCellEditor getEditor() {
    return this.editor;
  }

  /**
   * Returns the DecimalFormatSymbols.
   *
   * @return the DecimalFormatSymbols used for the number rendering.
   */
  public DecimalFormatSymbols getNumberSymbols() {
    return this.numberSymbols;
  }

  /**
   * Sets the DecimalFormatSymbols used for the number rendering.
   */
  public void setNumberSymbols(DecimalFormatSymbols numberSymbols) {
    this.numberSymbols = numberSymbols;
    this.formatterR.setDecimalFormatSymbols(numberSymbols);
    this.formatterE.setDecimalFormatSymbols(numberSymbols);
  }

  /**
   * Returns the number cell renderer.
   *
   * @return the table number cell renderer
   */
  public DefaultTableCellRenderer getRenderer() {
    return this.renderer;
  }

  /**
   * Sets the DecimalSeparator used for the number rendering.
   */
  public void setDecimalSeparator(char sep) {
    this.numberSymbols.setDecimalSeparator(sep);
    this.setNumberSymbols(this.numberSymbols);
  }

  /**
   * Sets the GroupingSeparator used for the number rendering.
   */
  public void setGroupingSeparator(char sep) {
    this.numberSymbols.setGroupingSeparator(sep);
    this.setNumberSymbols(this.numberSymbols);
  }

  private class NumberEditor extends DefaultCellEditor {

    private static final long serialVersionUID = 5183055867649727079L;

    /**
     *
     */
    public NumberEditor() {
      super(new JTextField());
      ((JTextField) this.getComponent()).setHorizontalAlignment(SwingConstants.RIGHT);
    }

    /**
     * @see javax.swing.DefaultCellEditor#getCellEditorValue()
     */
    @Override
    public Object getCellEditorValue() {
      try {
        return NumberColumnFormat.this.formatterE.parse(((JTextField) this.getComponent()).getText());
      } catch (ParseException ex) {
        return null;
      }
    }

    /**
     * @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
     */
    @Override
    public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {
      JTextField tf = ((JTextField) this.getComponent());
      tf.setBorder(new LineBorder(Color.black));
      try {
        tf.setText(NumberColumnFormat.this.formatterE.format(value));
      } catch (Exception e) {
        tf.setText("");
      }
      return tf;
    }

    /**
     * @see javax.swing.DefaultCellEditor#stopCellEditing()
     */
    @Override
    public boolean stopCellEditing() {
      String value = ((JTextField) this.getComponent()).getText();
      if (!value.equals("")) {
        try {
          NumberColumnFormat.this.formatterE.parse(value);
        } catch (ParseException e) {
          ((JComponent) this.getComponent()).setBorder(new LineBorder(Color.red));
          return false;
        }
      }
      return super.stopCellEditing();
    }
  }

  private class NumberRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 7312180200433879509L;

    /**
     *
     */
    public NumberRenderer() {
      super();
      this.setHorizontalAlignment(SwingConstants.RIGHT);
    }

    /**
     * @see javax.swing.table.DefaultTableCellRenderer#setValue(java.lang.Object)
     */
    @Override
    public void setValue(Object value) {
      this.setText((value == null) ? "" : NumberColumnFormat.this.formatterR.format(value));
    }
  }
}
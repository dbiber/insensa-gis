/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import org.insensa.commands.CommandManager;
import org.insensa.commands.ICommandProblem;
import org.insensa.commands.ISolution;
import org.insensa.commands.ModelCommand;
import org.insensa.view.dialogs.SolutionsDialog;

import java.awt.Component;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

public class ViewCommandWorker extends SwingWorker<Object, Object> {

  private IProcessFinishedListener listener;
  private Component parentComponent;

  public ViewCommandWorker(Component parentComponent, IProcessFinishedListener listener) {
    this.listener = listener;
    this.parentComponent = parentComponent;
  }

  @Override
  protected Object doInBackground() {
    this.executeCommands(this.parentComponent);
    return null;
  }

  //TODO: WE are not catching exception here, so no point of throwing in doInBackground
  @Override
  protected void done() {
    if (this.listener != null) {
      this.listener.done();
    }
  }

  /**
   * this method will be executed in a background process.
   * It is responsible for executing {@link ModelCommand} commands
   */
  public void executeCommands(Component parentComponent) {
    CommandManager cm = CommandManager.getInstance();
    CommandManager.CommandStatus result = cm.executeCommands();
    if (result == CommandManager.CommandStatus.STATUS_FAIL) {
      ModelCommand mc = cm.getLastCommand();
      String errorMessage = "Error:\n" +
          CommandManager.getInstance().getException().getMessage() + "\n"
          + "you can choose to retry or undo previous changes\n";
      Object[] options =
          {"Retry", "Undo", "Cancel"};
      ICommandProblem cp;
      List<ISolution> solutions = null;
      if (mc.getProblem() != null) {
        cp = mc.getProblem();
        errorMessage = cp.getErrorMessage();
        solutions = cp.getSolutions();
        if (solutions != null && !solutions.isEmpty()) {
          options = new Object[]
              {"Retry", "Solve Problem", "Cancel"};
        }
      }
      int answer = JOptionPane.showOptionDialog(parentComponent,
          errorMessage, "Error", JOptionPane.YES_NO_CANCEL_OPTION,
          JOptionPane.ERROR_MESSAGE,
          null, options, options[0]);
      if (answer == JOptionPane.YES_OPTION) {
        this.executeCommands(parentComponent);
      } else if (answer == JOptionPane.NO_OPTION) {
        if (solutions != null) {
          if (solutions.size() > 0) {
            SolutionsDialog dialog = new SolutionsDialog(solutions);
            dialog.setVisible(true);
            if (dialog.getSelectedSolution() != null) {
              cm.pushCommand(dialog.getSelectedSolution());
              this.executeCommands(parentComponent);
            }
          }
        } else
          this.undoCommands(parentComponent);
      }
    }
  }

  private void undoCommands(Component parentComponent) {
    if (CommandManager.getInstance().undoCommands() == CommandManager.CommandStatus.STATUS_FAIL) {
      String errorMessage = "An error has occurred during undo process:\n" + CommandManager.getInstance().getException().getMessage() + "\n"
          + "WARNUNG: Cancelling could damage the Project\n";
      Object[] options =
          {"Retry undo", "Cancel"};
      int answer = JOptionPane.showOptionDialog(parentComponent, errorMessage, "Error", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null,
          options, options[0]);
      if (answer == JOptionPane.YES_OPTION)
        this.undoCommands(parentComponent);
      else
        throw CommandManager.getInstance().getException();
    }
  }
}

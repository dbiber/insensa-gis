/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class MyChartPanel extends ChartPanel {


  private static final long serialVersionUID = -7211651809379794094L;

  public MyChartPanel(JFreeChart chart) {
    super(chart);
  }

  @Override
  public void mouseDragged(MouseEvent e) {
    XYPlot plot = (XYPlot) this.getChart().getPlot();
    XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) plot.getRenderer();
    xylineandshaperenderer.removeAnnotations();
    Point2D p = this.translateScreenToJava2D(e.getPoint());
    Rectangle2D plotArea = this.getScreenDataArea();

    double chartX = plot.getDomainAxis().java2DToValue(p.getX(), plotArea, plot.getDomainAxisEdge());
    double chartY = plot.getRangeAxis().java2DToValue(p.getY(), plotArea, plot.getRangeAxisEdge());
    XYLineAnnotation lineAnno = new XYLineAnnotation(chartX, 0, chartX, 3000D);
    xylineandshaperenderer.addAnnotation(lineAnno);

  }
}

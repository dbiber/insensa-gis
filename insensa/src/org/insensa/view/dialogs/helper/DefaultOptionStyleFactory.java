/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class DefaultOptionStyleFactory {
  private Style title;
  private Style normal;
  private StyleContext context;


  public DefaultOptionStyleFactory() {
    this.context = new StyleContext();
    this.initStyles();
  }

  public void addStyles(StyledDocument doc) {
    doc.addStyle("title", this.title);
    doc.addStyle("normal", this.normal);
  }

  public StyleContext getContext() {
    return this.context;
  }

  public Style getNormalStyle() {
    return this.normal;
  }

  public Style getTitleStyle() {
    return this.title;
  }

  private void initStyles() {
    this.title = this.context.getStyle(StyleContext.DEFAULT_STYLE);
    StyleConstants.setBold(this.title, true);
    StyleConstants.setAlignment(this.title, StyleConstants.ALIGN_CENTER);
    StyleConstants.setUnderline(this.title, true);

    this.normal = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
  }

}

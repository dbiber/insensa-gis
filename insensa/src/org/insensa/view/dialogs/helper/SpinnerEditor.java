/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.TableCellEditor;

class SpinnerEditor extends AbstractCellEditor implements TableCellEditor
{

	private static final long serialVersionUID = -4368795258837202033L;
	final JSpinner spinner = new JSpinner();
	SpinnerNumberModel spinnerNumberModel;
	JSpinner.NumberEditor editor;


	public SpinnerEditor()
	{
		this.spinnerNumberModel = new SpinnerNumberModel(0.0, 0.0, 100.0, 0.5);
		this.spinner.setModel(this.spinnerNumberModel);
		this.editor = new JSpinner.NumberEditor(this.spinner, "0.##");
		this.spinner.setEditor(this.editor);
	}

	/** 
	 * 
	 * @see javax.swing.CellEditor#getCellEditorValue()
	 */
	public Object getCellEditorValue()
	{
		return this.spinner.getValue();
	}

	/**
	 * @return
	 */
	public JSpinner getSpinner()
	{
		return this.spinner;
	}

	/** 
	 * 
	 * @see javax.swing.table.TableCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
	{
		this.spinner.setValue(value);
		return this.spinner;
	}

	/** 
	 * 
	 * @see javax.swing.AbstractCellEditor#isCellEditable(java.util.EventObject)
	 */
	public boolean isCellEditable(EventObject evt)
	{
		if (evt instanceof MouseEvent)
		{
			return ((MouseEvent) evt).getClickCount() >= 2;
		}
		return true;
	}
}
/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import org.insensa.commands.CommandManager;
import org.insensa.commands.ModelCommand;

import java.awt.Component;
import java.util.List;


public class ViewCommandManager {

  public static void executeCommands(Component parentComponent,
                                     IProcessFinishedListener listener) {
    new ViewCommandWorker(parentComponent, listener).execute();
  }

  public static void executeCommands(Component parentComponent,
                                     IProcessFinishedListener listener,
                                     List<ModelCommand> commandList) {
    CommandManager cManager = CommandManager.getInstance();
    cManager.clearCommands();
    for (ModelCommand iCommand : commandList) {
      cManager.addCommand(iCommand);
      iCommand.createDependendCommands();
    }
    ViewCommandManager.executeCommands(parentComponent, listener);
  }

  public static void executeCommands(Component parentComponent,
                                     IProcessFinishedListener listener,
                                     ModelCommand command) {
    CommandManager cManager = CommandManager.getInstance();
    cManager.clearCommands();
    cManager.addCommand(command);
    command.createDependendCommands();
    ViewCommandManager.executeCommands(parentComponent, listener);
  }

}

/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.helper;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import org.insensa.imports.FileDescription;


/**
 * @author dennis
 *
 */
public class SingleFileFilter extends FileFilter
{

	private FileDescription fileDescription;
	
	public SingleFileFilter(FileDescription fileDescription)
	{
		this.fileDescription=fileDescription;
	}

	/**
	 *
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File f)
	{
		for(String iExt : fileDescription.fileExtensions)
		{
			if(f.getName().endsWith(iExt))
				return true;
		}
		if(f.isDirectory())
			return true;
		return false;
	}

	/**
	 *
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	@Override
	public String getDescription()
	{
		return fileDescription.name;
	}
	
}

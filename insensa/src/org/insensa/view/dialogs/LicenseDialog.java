/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;


import org.insensa.controller.OpenUrlInBrowserHyperlinkListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.swing.JDialog;


public class LicenseDialog extends JDialog {
  private static final Logger log = LoggerFactory.getLogger(LicenseDialog.class);

  private static final long serialVersionUID = 3582996176965613079L;

  private javax.swing.JButton buttonClose;
  private javax.swing.JEditorPane editorPane;
  private javax.swing.JScrollPane scollPane;

  public LicenseDialog(java.awt.Frame parent, boolean modal) {
    super(parent, modal);
    this.initComponents();
    this.setTitle("GPL License");
  }

  private void initComponents() {

    this.scollPane = new javax.swing.JScrollPane();
    this.editorPane = new javax.swing.JEditorPane();
    this.buttonClose = new javax.swing.JButton();

    buttonClose.addActionListener(e -> setVisible(false));

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    this.scollPane.setBorder(null);
    this.scollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    this.scollPane.setOpaque(false);

    this.editorPane.setOpaque(false);
    this.editorPane.setEditable(false);
    this.editorPane.addHyperlinkListener(new OpenUrlInBrowserHyperlinkListener());
    this.scollPane.setViewportView(this.editorPane);
    try {
      this.editorPane.setPage("file:about/license.html");
    } catch (IOException e1) {
      log.error( "UNKNOWN", e1);
    }
    this.buttonClose.setFont(new java.awt.Font("Dialog", 0, 12));
    this.buttonClose.setText("Close");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
    this.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
            layout.createSequentialGroup().addContainerGap(397, Short.MAX_VALUE).addComponent(this.buttonClose).addContainerGap())
        .addComponent(this.scollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup().addComponent(this.scollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.buttonClose).addContainerGap()));

    this.pack();
  }
}

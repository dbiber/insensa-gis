/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.insensa.Model;
import org.insensa.Project;
import org.insensa.User;
import org.insensa.controller.Controller;
import org.insensa.view.View;
import org.jdom.JDOMException;


public class DialogNewProject extends SettingsDialog// extends JDialog
{
  private static final long serialVersionUID = 2416694984948236383L;
  String separator = System.getProperties().getProperty("file.separator");
  private JSeparator separatorButtons;
  private JPanel panelWorkspace;
  private JPanel panelProjectName;
  private JPanel panelOwner;
  private JButton buttonSelectWorkspace;
  private JComboBox comboBoxWorkspace;
  private JComboBox comboBoxOwner;
  private JLabel labelLocation;
  private JLabel labelProjectName;
  private JLabel labelOwnerName;
  private JTextField textFieldProject;
  private JButton buttonNewUser;
  private Model model;
  private View view;
  private Controller controller;
  private JFileChooser selectFolderFC;
  private List<User> users;
  // XmlProperties properties;
  private Map<String, String> userMap = new HashMap<String, String>();

  /**
   * @param m_model
   * @param org.insensa.view
   * @param controller
   * @throws JDOMException
   * @throws IOException
   */
  public DialogNewProject(Model m_model, View view, Controller controller) throws JDOMException, IOException {
    this.model = m_model;
    this.view = view;
    this.controller = controller;
    // properties = model.getProperties();
    super.setTitle("New Project");
    super.setName("New Project");
    JPanel panel = new JPanel();
    javax.swing.GroupLayout jDialogLayout = new javax.swing.GroupLayout(panel);
    panel.setLayout(jDialogLayout);
    this.setSize(600, 400);

    // List<Element> users = properties.getUsers();
    this.users = new ArrayList<User>();
    this.users.addAll(this.model.getUsers());
    String user;
    String workspace;
    // Element eUser;
    for (int i = 0; i < this.users.size(); i++) {
      user = this.users.get(i).getName();// new
      // String(users.get(i).getAttributeValue("name"));
      // eUser = properties.getUser(user);
      if (user != null) {
        workspace = this.users.get(i).getWorkspacePath();// new
        // String(properties.getUser(user).getAttributeValue("workspace"));
        if (user != null && workspace != null)
          this.userMap.put(user, workspace);
      }
    }
    this.initItems();
    this.initWorkspacePanel();
    this.initProjectPanel();
    this.initOwnerPanel();
    this.initMainItems(jDialogLayout);
    super.initComponents(panel);
    super.setHeadTitle("New Project");
    super.getButtonOk().addActionListener(new ButtonFinishActionListener());
  }

  /**
   * @see java.awt.Component#getName()
   */
  public String getName() {
    return this.textFieldProject.getText();
  }

  /**
   * @param userName
   * @return
   */
  public User getUser(String userName) {
    for (User iUser : this.users)
      if (iUser.getName().equals(userName))
        return iUser;
    return null;
  }

  private void initItems() {
    this.separatorButtons = new JSeparator();
    // Panel Workspace Content
    this.panelWorkspace = new JPanel();
    this.labelLocation = new JLabel("Location");
    this.labelLocation.setFont(Font.getFont("Dialog-Plain-12"));
    this.comboBoxWorkspace = new JComboBox();

    this.buttonSelectWorkspace = new JButton("Select");
    this.buttonSelectWorkspace.addActionListener(new SelectWorkspaceActionListener());

    // Panel Project Content
    this.panelProjectName = new JPanel();
    this.labelProjectName = new JLabel("Name");
    this.labelProjectName.setFont(Font.getFont("Dialog-Plain-12"));
    this.textFieldProject = new JTextField();
    this.textFieldProject.addKeyListener(new KeyListener() {
      @Override
      public void keyPressed(KeyEvent e) {

      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
          DialogNewProject.this.getButtonOk().doClick();

      }

      @Override
      public void keyTyped(KeyEvent e) {

      }
    });

    // Panel Owner Content
    this.buttonNewUser = new JButton("New");
    this.buttonNewUser.addActionListener(new NewUserActionListener());

    this.panelOwner = new JPanel();
    this.comboBoxOwner = new JComboBox();
    this.labelOwnerName = new JLabel("Name");
    this.labelOwnerName.setFont(Font.getFont("Dialog-Plain-12"));

    Iterator<String> userIt = this.userMap.keySet().iterator();
    String sName;
    while (userIt.hasNext()) {
      sName = userIt.next();
      this.comboBoxOwner.addItem(sName.toString());
    }
    this.comboBoxWorkspace.addItem(this.userMap.get(this.comboBoxOwner.getSelectedItem()));

    this.comboBoxOwner.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent e) {
        DialogNewProject.this.comboBoxWorkspace.removeAllItems();
        DialogNewProject.this.comboBoxWorkspace.addItem(DialogNewProject.this.userMap.get(DialogNewProject.this.comboBoxOwner.getSelectedItem()));
      }
    });
  }

  /**
   * @param jDialogLayout
   */
  private void initMainItems(GroupLayout jDialogLayout) {
    jDialogLayout.setHorizontalGroup(jDialogLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            jDialogLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.separatorButtons, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE).addContainerGap())
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            jDialogLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.panelOwner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            jDialogLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.panelWorkspace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        .addGroup(
            jDialogLayout
                .createSequentialGroup()
                .addContainerGap()
                .addComponent(this.panelProjectName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                    Short.MAX_VALUE).addContainerGap())

    );
    jDialogLayout.setVerticalGroup(jDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        jDialogLayout
            .createSequentialGroup()
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.panelOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.panelWorkspace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.panelProjectName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
            .addComponent(this.separatorButtons, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)));
  }

  private void initOwnerPanel() {
    this.panelOwner.setBorder(javax.swing.BorderFactory.createTitledBorder("User"));
    javax.swing.GroupLayout ownerPanelLayout = new javax.swing.GroupLayout(this.panelOwner);
    this.panelOwner.setLayout(ownerPanelLayout);
    this.panelOwner.setLayout(ownerPanelLayout);
    ownerPanelLayout.setHorizontalGroup(ownerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        ownerPanelLayout.createSequentialGroup().addContainerGap().addComponent(this.labelOwnerName)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.comboBoxOwner, 0, 293, Short.MAX_VALUE)
            .addGap(12, 12, 12).addComponent(this.buttonNewUser).addGap(12, 12, 12)));
    ownerPanelLayout.setVerticalGroup(ownerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        ownerPanelLayout
            .createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(
                ownerPanelLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.comboBoxOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.buttonNewUser).addComponent(this.labelOwnerName))));
  }

  private void initProjectPanel() {
    this.panelProjectName.setBorder(javax.swing.BorderFactory.createTitledBorder("Project Name"));
    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(this.panelProjectName);
    this.panelProjectName.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(this.labelProjectName).addGap(30, 30, 30)
            .addComponent(this.textFieldProject, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)));
    jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        jPanel2Layout
            .createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(
                jPanel2Layout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelProjectName)
                    .addComponent(this.textFieldProject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))));
  }

  private void initWorkspacePanel() {
    this.panelWorkspace.setBorder(javax.swing.BorderFactory.createTitledBorder("Workspace"));
    javax.swing.GroupLayout worspacePanelLayout = new javax.swing.GroupLayout(this.panelWorkspace);
    this.panelWorkspace.setLayout(worspacePanelLayout);
    this.panelWorkspace.setLayout(worspacePanelLayout);
    worspacePanelLayout.setHorizontalGroup(worspacePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        worspacePanelLayout.createSequentialGroup().addContainerGap().addComponent(this.labelLocation)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.comboBoxWorkspace, 0, 293, Short.MAX_VALUE)
            .addGap(12, 12, 12).addComponent(this.buttonSelectWorkspace).addGap(12, 12, 12)));
    worspacePanelLayout.setVerticalGroup(worspacePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        worspacePanelLayout
            .createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(
                worspacePanelLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.comboBoxWorkspace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.buttonSelectWorkspace)
                    .addComponent(this.labelLocation))));
  }

  public class ButtonFinishActionListener implements ActionListener {

    public void actionPerformed(java.awt.event.ActionEvent e) {
      String selUser = (String) DialogNewProject.this.comboBoxOwner.getSelectedItem();
      DialogNewProject.this.comboBoxWorkspace.getSelectedItem().toString();
      String selProjectName = DialogNewProject.this.textFieldProject.getText();
      Project newProject = null;
      DialogNewProject.this.view.getViewProperties();
      if (DialogNewProject.this.comboBoxOwner.getSelectedItem() == null
          || DialogNewProject.this.textFieldProject.getText().isEmpty()
          || DialogNewProject.this.comboBoxWorkspace.getSelectedItem() == null) {
        DialogNewProject.this.getLabelStatus().setText("You must fill in ALL data");
      }
      User user = DialogNewProject.this.getUser(selUser);
      if (user != null && user.getProject(selProjectName) != null) {
        DialogNewProject.this.getLabelStatus().setText("Project already exists");
      } else {
        try {
          String newWorkspaceName = DialogNewProject.this.comboBoxWorkspace.getSelectedItem().toString();
          if (!newWorkspaceName.endsWith(DialogNewProject.this.separator))
            newWorkspaceName += DialogNewProject.this.separator;
          // USER EXESTIERT NOCH NICHT
          if (user == null) {
            user = DialogNewProject.this.model.addUser(DialogNewProject.this.comboBoxOwner.getSelectedItem().toString(),
                DialogNewProject.this.comboBoxWorkspace.getSelectedItem().toString() + DialogNewProject.this.separator);
            newProject = user.addNewProject(selProjectName, null);
          }
          // USER EXESTIERT BEREITS
          else { // KEIN WORKSPACE GESPEICHERT BISHER
            if (user.getWorkspacePath() == null) {
              user.setWorkspacePath(newWorkspaceName);
              newProject = user.addNewProject(selProjectName, null);

            }
            // WORKSPACE VORHANDEN
            else {
              if (user.getWorkspacePath().equals(newWorkspaceName)) {
                newProject = user.addNewProject(selProjectName, null);
              }
            }
          }
          if (user.getActiveProject() != null)
            user.closeProject();

          if (newProject != null) {
            if (!user.createProject(newProject)) {
              throw new IOException("Error creating new project: " + newProject.getName());
            }
            DialogNewProject.this.model.setActiveUser(user);
            if (user.getActiveProject() != null) {
              DialogNewProject.this.view.closeProject();
              DialogNewProject.this.view.addProject();
              DialogNewProject.this.controller.addNewActionListener();
            }
            DialogNewProject.this.setVisible(false);
          }
        } catch (IOException e1) {
          DialogNewProject.this.getLabelStatus().setText(e1.getMessage());
          e1.printStackTrace();
        }
      }
    }
  }

  public class CancelActionListener implements ActionListener {

    public void actionPerformed(java.awt.event.ActionEvent e) {
      DialogNewProject.this.setVisible(false);
    }
  }

  public class NewUserActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      String userName = JOptionPane.showInputDialog("Your Name?");
      if (userName != null && !userName.isEmpty()) {
        if (DialogNewProject.this.userMap.containsKey(userName)) {
          DialogNewProject.this.getLabelStatus().setText("User already exists");
        } else {
          DialogNewProject.this.userMap.put(userName, null);
          DialogNewProject.this.comboBoxOwner.insertItemAt(userName.toString(), 0);
          DialogNewProject.this.comboBoxOwner.setSelectedIndex(0);
          DialogNewProject.this.comboBoxWorkspace.removeAllItems();
        }
      }
    }
  }

  public class SelectWorkspaceActionListener implements ActionListener {

    public void actionPerformed(java.awt.event.ActionEvent e) {
      if (DialogNewProject.this.comboBoxOwner.getSelectedItem() != null) {
        DialogNewProject.this.selectFolderFC = new JFileChooser();
        DialogNewProject.this.selectFolderFC.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int chose = DialogNewProject.this.selectFolderFC.showOpenDialog(DialogNewProject.this.getParent());
        if (chose == JFileChooser.APPROVE_OPTION) {
          if (DialogNewProject.this.selectFolderFC.getSelectedFile().isDirectory()) {
            DialogNewProject.this.userMap.put((String) DialogNewProject.this.comboBoxOwner.getSelectedItem(),
                DialogNewProject.this.selectFolderFC.getSelectedFile().getAbsolutePath());
            String newFolderName = DialogNewProject.this.selectFolderFC.getSelectedFile().getAbsolutePath();
            DialogNewProject.this.comboBoxWorkspace.addItem(newFolderName);
            DialogNewProject.this.comboBoxWorkspace.setSelectedItem(newFolderName);
          }
        }
      }
    }
  }
}

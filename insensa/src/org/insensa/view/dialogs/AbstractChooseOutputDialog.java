/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.view.View;

import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.List;

import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.tree.DefaultMutableTreeNode;


public abstract class AbstractChooseOutputDialog extends SettingsDialog implements ChooseOutputDialog {

  private static final long serialVersionUID = -8403275360166668334L;
  protected JCheckBox jCheckBox1;
  protected JLabel outputFileSetLabel;
  protected JTextField outputFileSetTextField;
  protected JTextField newSetTextField;
  protected JLabel outputFileNameLabel;
  protected JTextField outputFileNameText;

  protected GenericGisFileSet outputFileSet = null;
  protected boolean isSelected = false;
  protected Model model;
  protected DefaultMutableTreeNode treeNode;
  protected View view;
  String separator = System.getProperties().getProperty("file.separator");
  private String newFileName;

  /**
   * Creates new form DialogAddConnectionTypes.
   */
  public AbstractChooseOutputDialog(Frame parent, boolean modal, Model model, View view) {
    super(parent, modal);
    this.model = model;
    this.view = view;
    super.initComponents(this.initComponents());
    this.setModalityType(ModalityType.MODELESS);
    this.setAlwaysOnTop(true);
  }

  protected boolean checkInputValues() {
    String outputSetPath = "";
    if (this.outputFileNameText.getText().isEmpty()) {
      this.getLabelStatus().setText("File name is missing");
      return false;
    }
    if (this.outputFileSet == null) {
      this.getLabelStatus().setText("No output fileset selected");
      return false;
    }
    if (this.isSelected) {
      outputSetPath = this.outputFileSet.getPath() + this.outputFileSet.getName() + this.separator + "FileSets" + this.separator
          + this.newSetTextField.getText();
      File newFileSet = new File(outputSetPath);
      if (newFileSet.exists()) {
        this.getLabelStatus().setText("Fileset allready exists");
        return false;
      }
    }

    List<IGisFileContainer> fileList = this.outputFileSet.getFileList();
    String newFullFileName = this.outputFileNameText.getText();
    String newFileName;
    if (!newFullFileName.endsWith(".tif")) {
      newFileName = newFullFileName;
      newFullFileName = newFileName + ".tif";
    }
    for (IGisFileContainer aFileList: fileList) {
      String oldFileName = aFileList.getOutputFileName();

      if (oldFileName.equals(newFullFileName)) {
        this.getLabelStatus().setText("File already exists");
        return false;
      }
    }
    this.newFileName = newFullFileName;
    return true;
  }

  protected String getNewFileName() {
    return this.newFileName;
  }

  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.outputFileSetLabel = new JLabel("Output File Set:");
    this.outputFileSetTextField = new JTextField();
    this.newSetTextField = new JTextField();
    this.jCheckBox1 = new JCheckBox();
    this.outputFileNameLabel = new JLabel("Output Filename:");
    this.outputFileNameText = new JTextField();

    this.outputFileSetTextField.setEditable(false);

    this.newSetTextField.setText("New Folder");

    this.jCheckBox1.setText("New Subfolder");
    this.newSetTextField.setEnabled(false);
    this.jCheckBox1.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        AbstractChooseOutputDialog.this.isSelected = true;
        AbstractChooseOutputDialog.this.newSetTextField.setEnabled(true);
      } else {
        AbstractChooseOutputDialog.this.isSelected = false;
        AbstractChooseOutputDialog.this.newSetTextField.setEnabled(false);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(
        // Parallel
        layout.createParallelGroup(Alignment.LEADING)
            .addGroup(
                layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(this.outputFileNameLabel)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(this.outputFileNameText)
                    .addContainerGap())
            .addGroup(
                layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(this.outputFileSetLabel)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(this.outputFileSetTextField)
                    .addContainerGap())
            .addGroup(
                layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(this.jCheckBox1)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(this.newSetTextField)
                    .addContainerGap()));

    layout.setVerticalGroup(

        layout.createParallelGroup(Alignment.LEADING).addGroup(
            layout.createSequentialGroup()
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                    .addComponent(this.outputFileNameLabel)
                    .addComponent(this.outputFileNameText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(
                    layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(this.outputFileSetLabel)
                        .addComponent(this.outputFileSetTextField))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                    .addComponent(this.jCheckBox1).addComponent(this.newSetTextField))
                .addGap(0, 20, Short.MAX_VALUE)));
    return panel;
  }

  @Override
  public boolean isVisible() {
    return super.isVisible();
  }

  @Override
  public void setVisible(boolean b) {
    super.setVisible(b);
  }

  @Override
  public void setFileSet(GenericGisFileSet newSet) {
    this.outputFileSet = newSet;
    this.outputFileSetTextField.setText(newSet.toString());
  }

  @Override
  public void setRefreshingTreeNode(DefaultMutableTreeNode treeNode) {
    this.treeNode = treeNode;
  }
}

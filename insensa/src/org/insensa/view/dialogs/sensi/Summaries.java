/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.GenericGisFileSet;
import org.insensa.connections.CMean;
import org.insensa.connections.CThresholdVolatility;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.EConnection;
import org.jdom.JDOMException;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.SpinnerNumberModel;


public class Summaries extends JPanel {


  private static final long serialVersionUID = 1264068479018197048L;
  String errorMassage = "";
  private javax.swing.JCheckBox checkBoxVol;
  private javax.swing.JCheckBox checkBoxCoeffVar;
  private javax.swing.JCheckBox checkBoxMean;
  private javax.swing.JCheckBox checkBoxStdDev;
  private javax.swing.JScrollPane jScrollPane10;
  private javax.swing.JScrollPane jScrollPane20;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JLabel labelFrom;
  private javax.swing.JLabel labelTo;
  private javax.swing.JSpinner spinnerFrom;
  private javax.swing.JSpinner spinnerTo;
  private javax.swing.JEditorPane editorPaneStats;
  // private javax.swing.JScrollPane scrollPaneVol;
  // private javax.swing.JScrollPane scrollPaneStats;
  private javax.swing.JEditorPane editorPaneVol;
  private boolean isMean = false;
  private boolean isStdDev = false;
  private boolean isCoeffVar = false;
  private boolean isVolatility = false;


  public Summaries() {
    this.initComponents();
  }

  /**
   * @param model
   * @param outputFileSet
   * @param fileList
   * @param outputFileName
   * @return
   * @throws IOException
   * @throws JDOMException
   * @throws GisFileException
   */
  public boolean applySettings(Model model, GenericGisFileSet outputFileSet, List<IGisFileContainer> fileList, String outputFileName) throws IOException,
      JDOMException, GisFileException {
    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
    List<IGisFileContainer> sourceFileList = new ArrayList<>();
    sourceFileList.addAll(fileList);

    int from = ((Integer) this.spinnerFrom.getValue());
    int to = ((Integer) this.spinnerTo.getValue());
    if (this.isVolatility == true) {
      if (from >= to) {
        this.errorMassage = "from must be smaller then to";
      }
      String newMeanName = outputFileNameSplit + "_volatility.tif";
      ConnectionFileChanger thresVol = model.createAndAddConnection(sourceFileList, outputFileSet, CThresholdVolatility.NAME, "summaries", newMeanName);
      ((CThresholdVolatility) thresVol).setThreshold1Perc(from);
      ((CThresholdVolatility) thresVol).setThreshold2Perc(to);
      RasterFileContainer outputFile = (RasterFileContainer) thresVol.getOutputFileContainer();
      if (outputFile != null)
        outputFile.getGisFileInformationStorage().refreshPluginExec(thresVol);

      thresVol.getOutputFileContainer().setDescription("Todo...");
    }
    if (this.isMean == true) {
      String newMeanName = outputFileNameSplit + "_mean.tif";
      ConnectionFileChanger tmpConMean = model.createAndAddConnection(sourceFileList, outputFileSet, CMean.NAME, "summaries", newMeanName);
      tmpConMean.getOutputFileContainer().setDescription("Todo...");
    }
    if (this.isStdDev == true) {
      String newStdDevName = outputFileNameSplit + "_StdDev.tif";
      ConnectionFileChanger tmpConMean = model.createAndAddConnection(sourceFileList, outputFileSet, EConnection.StandardDeviation.toString(), "summaries",
          newStdDevName);
      tmpConMean.getOutputFileContainer().setDescription("Todo...");
    }
    if (this.isCoeffVar == true) {
      String newCoeffVarName = outputFileNameSplit + "_CoeffVar.tif";
      ConnectionFileChanger tmpConMean = model.createAndAddConnection(sourceFileList, outputFileSet, EConnection.CoefficientOfVariation.toString(), "summaries",
          newCoeffVarName);
      tmpConMean.getOutputFileContainer().setDescription("Todo...");
    }

    return true;
  }

  /**
   * @return
   */
  public String getErrorMassage() {
    return this.errorMassage;
  }


  private void initComponents() {

    this.checkBoxMean = new javax.swing.JCheckBox();
    this.checkBoxStdDev = new javax.swing.JCheckBox();
    this.checkBoxCoeffVar = new javax.swing.JCheckBox();
    this.jScrollPane10 = new javax.swing.JScrollPane();
    this.editorPaneStats = new javax.swing.JEditorPane();
    // scrollPaneStats = new javax.swing.JScrollPane();
    this.jScrollPane20 = new javax.swing.JScrollPane();
    this.editorPaneVol = new javax.swing.JEditorPane();
    // scrollPaneVol = new javax.swing.JScrollPane();
    this.checkBoxVol = new javax.swing.JCheckBox();
    this.spinnerFrom = new javax.swing.JSpinner(new SpinnerNumberModel(0, 0, 100, 5));
    this.spinnerTo = new javax.swing.JSpinner(new SpinnerNumberModel(0, 0, 100, 5));
    this.jSeparator1 = new javax.swing.JSeparator();
    this.labelFrom = new javax.swing.JLabel();
    this.labelTo = new javax.swing.JLabel();

    this.checkBoxMean.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkBoxMean.setText("Mean");
    this.checkBoxMean.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
          Summaries.this.isMean = true;
        else
          Summaries.this.isMean = false;
      }
    });

    this.checkBoxStdDev.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkBoxStdDev.setText("Standard Deviation");
    this.checkBoxStdDev.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
          Summaries.this.isStdDev = true;
        else
          Summaries.this.isStdDev = false;
      }
    });
    // checkBoxStdDev.setEnabled(false);

    this.checkBoxCoeffVar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkBoxCoeffVar.setText("Coefficient of Variation");
    this.checkBoxCoeffVar.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
          Summaries.this.isCoeffVar = true;
        else
          Summaries.this.isCoeffVar = false;
      }
    });

    this.jScrollPane10.setBorder(null);
    this.jScrollPane10.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    this.jScrollPane10.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

    this.editorPaneStats.setBackground(new java.awt.Color(238, 238, 238));
    this.editorPaneStats.setEditable(false);
    this.editorPaneStats.setContentType("text/html");
    this.editorPaneStats.setText("Calculation of all standard statistics over all modified index files.");

    this.editorPaneStats.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
    this.jScrollPane10.setViewportView(this.editorPaneStats);

    this.jScrollPane20.setBorder(null);
    this.jScrollPane20.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    this.jScrollPane20.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

    this.editorPaneVol.setBackground(new java.awt.Color(238, 238, 238));
    this.editorPaneVol.setEditable(false);
    this.editorPaneVol.setContentType("text/html");
    this.editorPaneVol.setText("All datasets are counted for which the value falls between the two boundaries.<br>"
        + "The relative values are proportions of the maximum value.");
    this.editorPaneVol.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
    this.jScrollPane20.setViewportView(this.editorPaneVol);

    this.checkBoxVol.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkBoxVol.setText("Volatility");
    this.checkBoxVol.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
          Summaries.this.isVolatility = true;
        else
          Summaries.this.isVolatility = false;
      }
    });

    this.labelFrom.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelFrom.setText("From");

    this.labelTo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelTo.setText("To");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        layout.createSequentialGroup()
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(this.checkBoxMean).addComponent(this.checkBoxStdDev)
                                    .addComponent(this.checkBoxCoeffVar)).addGap(18, 18, 18)
                            .addComponent(this.jScrollPane10, 50, GroupLayout.PREFERRED_SIZE, 280))
                    .addComponent(this.jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                    .addGroup(
                        layout.createSequentialGroup()
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(this.checkBoxVol)
                                    .addGroup(
                                        layout.createSequentialGroup()
                                            .addGroup(
                                                layout.createParallelGroup(
                                                    javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(this.labelFrom)
                                                    .addComponent(this.labelTo))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(
                                                layout.createParallelGroup(
                                                    javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(this.spinnerTo)
                                                    .addComponent(this.spinnerFrom,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE, 124,
                                                        Short.MAX_VALUE))))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(this.jScrollPane20, 50, GroupLayout.PREFERRED_SIZE, 280))).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        layout.createSequentialGroup().addComponent(this.checkBoxMean)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(this.checkBoxStdDev)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(this.checkBoxCoeffVar))
                    .addComponent(this.jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(16, 16, 16)
            .addComponent(this.jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        layout.createSequentialGroup()
                            .addComponent(this.checkBoxVol)
                            .addGap(5, 5, 5)
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(this.labelFrom)
                                    .addComponent(this.spinnerFrom, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(this.labelTo)
                                    .addComponent(this.spinnerTo, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(this.jScrollPane20, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(83, Short.MAX_VALUE)));
  }

  /**
   * @return
   */
  public boolean isCoeffVar() {
    return this.isCoeffVar;
  }

  /**
   * @return
   */
  public boolean isMean() {
    return this.isMean;
  }

  /**
   * @return
   */
  public boolean isStdDev() {
    return this.isStdDev;
  }

  /**
   * @return
   */
  public boolean isVolatility() {
    return this.isVolatility;
  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CIndexation;
import org.insensa.connections.ConnectionManager;
import org.insensa.connections.EConnection;
import org.insensa.view.dialogs.helper.NumberColumnFormat;
import org.jdom.JDOMException;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.swing.AbstractCellEditor;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;


public class ModeRandomVariation extends JPanel implements IMode {
  private static final long serialVersionUID = 7859796057112976889L;
  private CIndexation prioritization;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable jTable1;
  private JLabel labelFileNumber;
  private JLabel labelExe;
  private JRadioButton radioButtonAll;
  private JRadioButton radioButtonOne;
  private ButtonGroup buttonGroup;
  private JSeparator sep;
  private JSpinner spinnerFileNumber;
  private List<IGisFileContainer> tmpConList = new ArrayList<IGisFileContainer>();
  private boolean all = true;

  public ModeRandomVariation(CIndexation prioritization) throws IOException {
    this.prioritization = prioritization;
    this.initComponents();

  }

  @Override
  public boolean applySettings(JLabel labelStatus, Model model,
                               GenericGisFileSet outputFileSet,
                               CIndexation prioritization, String outputFileName)
      throws IOException, JDOMException {
    List<RasterFileContainer> fileList = prioritization.getSourceFileList();
    List<Float> minRangeList = new ArrayList<Float>();
    List<Float> maxRangeList = new ArrayList<Float>();
    List<Float> differenceList = new ArrayList<Float>();
    ConnectionManager connectionManager = new ConnectionManager();

    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
    String outputFileNameNew;
    int fileNameIterator = 0;
    this.tmpConList.clear();

    int numOfVariations = 0;
    for (int i = 0; i < this.jTable1.getRowCount(); i++) {
      minRangeList.add((float) (((Double) this.jTable1.getValueAt(i, 2)) / 100.0));
      maxRangeList.add((float) (((Double) this.jTable1.getValueAt(i, 3)) / 100.0));
      Float diff = maxRangeList.get(i) - minRangeList.get(i);
      if (diff > 0.0F)
        numOfVariations++;
      differenceList.add(maxRangeList.get(i) - minRangeList.get(i));
    }
    Random rand = new Random();
    int numOfFiles = (Integer) this.spinnerFileNumber.getValue();
    // int numOfFilePerVar = Math.round(numOfFiles/numOfVariations);
    if (this.all && numOfVariations > 0) {
      for (int i = 0; i < numOfFiles; i++) {
        fileNameIterator++;
        outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";

        CIndexation tmpCon = (CIndexation) connectionManager.getConnectionFileChanger(prioritization.getId());

        tmpCon.setOutputFileSet(outputFileSet);
        tmpCon.setSourceFileList(fileList);
//        connectionManager.copyConnection(tmpCon, prioritization);
        tmpCon.setUsed(false);
        model.addConnection(fileList,outputFileSet,tmpCon,null,outputFileNameNew);
        connectionManager.copyConnection(prioritization,tmpCon);

        List<Float> weightList = tmpCon.getFileWeightingMap()
            .values()
            .stream()
            .map(CIndexation.Weighting::getWeight)
            .collect(Collectors.toList());

        Float sumOfNewWeights = 0.0f;
        List<Float> newWeightsList = new ArrayList<Float>();
        for (int j = 0; j < differenceList.size(); j++) {
          if (differenceList.get(j) > 0.0F) {
            Float randomWeight = (rand.nextFloat() * differenceList.get(j)) + minRangeList.get(j);
            sumOfNewWeights += randomWeight;
            newWeightsList.add(randomWeight);
            // tmpCon.getWeightList().set(j, randomWeight);
          } else {
            // TODO TEST
            sumOfNewWeights += weightList.get(j);
            newWeightsList.add(weightList.get(j));
          }
        }
        // soll den Uebertrag verteilen
        // TODO Test
        // Float newDiffVal = (1.0f-sumOfNewWeights)/new
        // Float(fileList.size());
        Float newDiffVal = (1.0f - sumOfNewWeights) / numOfVariations;
        List<Float> newWeightsList2 = this.checkRange(newWeightsList, maxRangeList, minRangeList, newDiffVal);
        if (newWeightsList2 == null) {
          continue;
        } else {
          for (int j = 0; j < newWeightsList2.size(); j++) {
            IGisFileContainer tmpContainer = fileList.get(j);
            tmpCon.getWeighting(tmpContainer).setWeight(newWeightsList2.get(j));
//            weightList.set(j, newWeightsList2.get(j));
          }
        }

//        model.addConnection(fileList,outputFileSet,tmpCon,null,outputFileNameNew);
//        model.createAndAddConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);
        tmpCon.getOutputFileContainer().getGisFileInformationStorage().refreshPluginExec(tmpCon);
//        tmpCon.getOutputFile().getGisFileInformationStorage().refreshConnectionFileChanger(tmpCon);

        this.tmpConList.add(tmpCon.getOutputFileContainer());
      }
    }

    if (!this.all) {
      for (int i = 0; i < fileList.size(); i++) {
        if (differenceList.get(i) == 0.0f)
          continue;
        for (int j = 0; j < numOfFiles; j++) {
          Float randomWeight = (rand.nextFloat() * differenceList.get(i)) + minRangeList.get(i);
          fileNameIterator++;
          outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
//          CIndexation tmpCon = (CIndexation) connectionManager.getConnectionFileChanger("Indexation");
          CIndexation tmpCon = (CIndexation) connectionManager.getConnectionFileChanger(prioritization.getId());

          tmpCon.setOutputFileSet(outputFileSet);
          tmpCon.setSourceFileList(fileList);

          tmpCon.setUsed(false);
          model.addConnection(fileList,outputFileSet,tmpCon,null,outputFileNameNew);
          connectionManager.copyConnection(prioritization,tmpCon);

          List<Float> weightList = tmpCon.getFileWeightingMap()
              .values()
              .stream()
              .map(CIndexation.Weighting::getWeight)
              .collect(Collectors.toList());
//          tmpCon.setConnectionElement(prioritization.getConnectionElement());
//          List<Float> weightList = tmpCon.getWeightList();

//          model.createAndAddConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);

          List<IGisFileContainer> tmpFileList = new ArrayList<>();
          tmpFileList.addAll(fileList);
          tmpFileList.remove(i);
          Float oldValue = weightList.get(i);
          Float diffValue = oldValue - randomWeight;
          Float shareValue = diffValue / (float) tmpFileList.size();
          for (int k = 0; k < tmpFileList.size(); k++) {
            int index = fileList.indexOf(tmpFileList.get(k));

//            weightList.set(index, weightList.get(index) + shareValue);
            IGisFileContainer tmpContainer = fileList.get(index);
            float oldWeight = tmpCon.getWeighting(tmpContainer).getWeight();
            tmpCon.getWeighting(tmpContainer).setWeight(oldWeight + shareValue);
          }
          IGisFileContainer tmpContainer = fileList.get(i);
          tmpCon.getWeighting(tmpContainer).setWeight(randomWeight);

//          weightList.set(i, randomWeight);
          tmpCon.getOutputFileContainer().getGisFileInformationStorage().refreshPluginExec(tmpCon);
          this.tmpConList.add(tmpCon.getOutputFileContainer());
        }
      }
    }

    return true;
  }

  private List<Float> checkRange(List<Float> listOfWeights, List<Float> listOfMax, List<Float> listOfMin, Float diffVal) {
    List<Float> newList = new ArrayList<Float>();
    List<Float> listOfWeightsTmp = new ArrayList<Float>();
    listOfWeightsTmp.addAll(listOfWeights);
    int leftFilesSize = 0;
    Float newDiff = 0.0f;
    for (int i = 0; i < listOfWeights.size(); i++)
      newList.add(null);
    for (int i = 0; i < listOfWeightsTmp.size(); i++) {
      if (listOfWeightsTmp.get(i) == null)
        continue;
      Float diffVal2 = listOfWeightsTmp.get(i) + diffVal;
      listOfWeightsTmp.set(i, diffVal2);
      if (listOfWeightsTmp.get(i) > listOfMax.get(i)) {
        newDiff += listOfWeightsTmp.get(i) - listOfMax.get(i);
        listOfWeightsTmp.set(i, listOfMax.get(i));
      } else if (listOfWeightsTmp.get(i) < listOfMin.get(i)) {
        newDiff += listOfWeightsTmp.get(i) - listOfMin.get(i);
        listOfWeightsTmp.set(i, listOfMin.get(i));
      } else {
        newList.set(i, listOfWeightsTmp.get(i));
        leftFilesSize++;
      }
    }
    if (newDiff == 0.0f)
      return listOfWeightsTmp;
    else if (leftFilesSize == 0)
      return null;
    else {
      List<Float> tmpList = this.checkRange(newList, listOfMax, listOfMin, newDiff / (float) leftFilesSize);
      if (tmpList == null)
        return null;
      else {
        for (int i = 0; i < tmpList.size(); i++) {
          if (tmpList.get(i) != null)
            listOfWeightsTmp.set(i, tmpList.get(i));
        }
        return listOfWeightsTmp;
      }
    }

  }

  @Override
  public List<IGisFileContainer> getSensiList() {
    return this.tmpConList;
  }

  @Override
  public String getShortDescription() {
    String description = "The assigned weights are altered randomly within a defined range " +
        "either one at a time or all at a time and as often as defined as the number" +
        " of output files.<br>"
        + "The file number corresponds to the total number of modified index files if the " +
        "weights are altered all at a time and to the number of modified index files per " +
        "variable if altered one at a time.";
    return description;
  }

  private void initComponents() throws IOException {
    this.jScrollPane1 = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();
    this.spinnerFileNumber = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));

    Vector<Vector<Object>> dataVector = new Vector<>();
    Vector<Object> row;
    List<RasterFileContainer> rasterFileList = this.prioritization.getSourceFileList();
    List<Float> weightList = this.prioritization
        .getFileWeightingMap()
        .values()
        .stream()
        .map(CIndexation.Weighting::getWeight)
        .collect(Collectors.toList());

//    List<Float> weightList = this.prioritization.getWeightList();
    if (weightList.isEmpty())
      throw new IOException("Weights not set");
    for (int i = 0; i < rasterFileList.size(); i++) {

      row = new Vector<>();
      row.add(rasterFileList.get(i));
      row.add(weightList.get(i) * 100.0);
      row.add(weightList.get(i) * 100.0);
      row.add(weightList.get(i) * 100.0);

      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<>();
    titleVec.add("File");
    titleVec.add("Weight");
    titleVec.add("Minimum Weight");
    titleVec.add("Maximum Weight");

    PropertiesTableModel tableModel = new PropertiesTableModel(dataVector, titleVec);
    this.jTable1.setModel(tableModel);
    this.jScrollPane1.setViewportView(this.jTable1);

    NumberColumnFormat numberColumn = new NumberColumnFormat("##.##", "##.##");
    this.jTable1.getColumnModel().getColumn(1).setCellEditor(numberColumn.getEditor());
    this.jTable1.getColumnModel().getColumn(1).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(2).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(2).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(3).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(3).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.setRowHeight(24);

    this.labelFileNumber = new JLabel();
    this.labelFileNumber.setFont(new Font("Dialog", 0, 12));
    this.labelFileNumber.setText("File Number:");

    this.labelExe = new JLabel();
    this.labelExe.setFont(new Font("Dialog", 0, 12));
    this.labelExe.setText("Execution Method:");

    this.sep = new JSeparator();

    this.radioButtonOne = new JRadioButton("One at a time");
    this.radioButtonOne.addActionListener(e -> ModeRandomVariation.this.all = false);
    this.radioButtonAll = new JRadioButton("All at a time");
    this.radioButtonAll.setSelected(true);
    this.radioButtonAll.addActionListener(e -> ModeRandomVariation.this.all = true);
    this.buttonGroup = new ButtonGroup();
    this.buttonGroup.add(this.radioButtonAll);
    this.buttonGroup.add(this.radioButtonOne);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.labelFileNumber)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.spinnerFileNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(497, Short.MAX_VALUE))
        .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 679, Short.MAX_VALUE)
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.sep, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                .addContainerGap())
        .addGroup(
            layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(this.labelExe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(this.radioButtonOne, javax.swing.GroupLayout.Alignment.LEADING,
                            javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(this.radioButtonAll, javax.swing.GroupLayout.Alignment.LEADING,
                            javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            javax.swing.GroupLayout.PREFERRED_SIZE)).addContainerGap(430, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
            .addGap(18, 18, 18)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelFileNumber)
                    .addComponent(this.spinnerFileNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.sep, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(this.labelExe)
                    .addComponent(this.radioButtonAll)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.radioButtonOne).addGap(33, 33, 33)));
  }

  @Override
  public String toString() {
    return "Random Weight Variation";
  }

  private static class PropertiesTableModel extends DefaultTableModel {
    private static final long serialVersionUID = -4487353833927280683L;

    boolean[] canEdit = new boolean[]
        {false, false, true, true};

    public PropertiesTableModel(Vector<Vector<Object>> data, Vector<String> columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return this.canEdit[columnIndex];
    }
  }

  @SuppressWarnings("serial")
  private static class SpinnerEditor extends AbstractCellEditor implements TableCellEditor {
    final JSpinner spinner = new JSpinner();
    SpinnerNumberModel spinnerNumberModel;
    JSpinner.NumberEditor editor;

    public SpinnerEditor() {
      this.spinnerNumberModel = new SpinnerNumberModel(0.0, 0.0, 100.0, 0.5);
      this.spinner.setModel(this.spinnerNumberModel);
      this.editor = new JSpinner.NumberEditor(this.spinner, "0.##");
      this.spinner.setEditor(this.editor);
    }

    public Object getCellEditorValue() {
      return this.spinner.getValue();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      this.spinner.setValue(value);
      return this.spinner;
    }

    public boolean isCellEditable(EventObject evt) {
      if (evt instanceof MouseEvent) {
        return ((MouseEvent) evt).getClickCount() >= 2;
      }
      return true;
    }
  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CIndexation;
import org.insensa.connections.ConnectionManager;
import org.insensa.view.dialogs.helper.NumberColumnFormat;
import org.jdom.JDOMException;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.swing.AbstractCellEditor;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;


public class ModeSystematicVariation extends JPanel implements IMode {

  private CIndexation prioritization;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable jTable1;
  private List<IGisFileContainer> tmpConList = new ArrayList<IGisFileContainer>();

  public ModeSystematicVariation(CIndexation prioritization) throws IOException {
    this.prioritization = prioritization;
    this.initComponents();
  }

  float getRounded(float glob) {
    return (float)Math.round(glob * 1000f) / 1000f;
  }

  float getRounded(Double glob) {
    return (float)Math.round(glob.floatValue() * 1000f) / 1000f;
  }

  @Override
  public boolean applySettings(JLabel labelStatus, Model model, GenericGisFileSet outputFileSet, CIndexation prioritization, String outputFileName)
      throws IOException, JDOMException {
    List<RasterFileContainer> fileList = prioritization.getSourceFileList();
    ConnectionManager connectionManager = new ConnectionManager();

    // GenericGisFileSet tmpSet;
    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
    String outputFileNameNew;
    int fileNameIterator = 0;
    this.tmpConList.clear();

    for (int i = 0; i < fileList.size(); i++) {
      IGisFileContainer container = fileList.get(i);
      float origWeight = prioritization.getWeighting(container).getWeight();
      float minWeight = getRounded(((Double)this.jTable1.getValueAt(i, 2)) / 100.0);
      float maxWeight = getRounded(((Double)this.jTable1.getValueAt(i, 3)) / 100.0);
      float stepSize = getRounded(((Double)this.jTable1.getValueAt(i, 4)) / 100.0);
      int numOfSteps = Math.round((maxWeight - minWeight) / stepSize);
      if (stepSize <= 0.0F) {
        continue;
      }
      if (numOfSteps != 0) {
        for (int j = 0; j <= numOfSteps; j++) {
          fileNameIterator++;
          outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
          CIndexation tmpCon = (CIndexation) connectionManager.getConnectionFileChanger("Indexation");
          tmpCon.setOutputFileSet(outputFileSet);
          tmpCon.setSourceFileList(fileList);
          connectionManager.copyConnection(prioritization, tmpCon);
          tmpCon.setUsed(false);
          List<IGisFileContainer> tmpFileList = new ArrayList<>();
          tmpFileList.addAll(fileList);
          tmpFileList.remove(i);
          Float newValue = (j * stepSize) + minWeight;// TODO

          Float diffValue = origWeight - newValue;
          if (diffValue == 0) {
            continue;
          }
          Float shareValue = diffValue / (float) tmpFileList.size();
          shareValue = getRounded(shareValue);

          model.addConnection(fileList, outputFileSet, tmpCon,null , outputFileNameNew);

          for(IGisFileContainer iterContainer : tmpFileList) {
            float oldW = prioritization.getWeighting(iterContainer).getWeight();
            tmpCon.getWeighting(iterContainer).setWeight(oldW + shareValue);
          }
          tmpCon.getWeighting(fileList.get(i)).setWeight(newValue);
          tmpConList.add(tmpCon.getOutputFileContainer());
        }
      }
    }
    return true;
  }

  @Override
  public List<IGisFileContainer> getSensiList() {
    return this.tmpConList;
  }

  @Override
  public String getShortDescription() {
    String description = "The assigned weights of each factor are altered systematically within a defined range and one at a time.<br>"
        + "The step size defines the interval size of the weight differences between the different modified index files.";
    return description;
  }

  private void initComponents() throws IOException {
    this.jScrollPane1 = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();

    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    List<RasterFileContainer> rasterFileList = this.prioritization.getSourceFileList();
    List<Float> weightList = this.prioritization
        .getFileWeightingMap()
        .values()
        .stream()
        .map(CIndexation.Weighting::getWeight)
        .collect(Collectors.toList());
    if (weightList.isEmpty()) {
      throw new IOException("Weights not set");
    }
    if (weightList.size() != rasterFileList.size()) {
      throw new IOException("Not every file has a weight set to it");
    }
    // ButtonGroup buttonGroup= new ButtonGroup();
    for (int i = 0; i < rasterFileList.size(); i++) {
      // JRadioButton rButton = new JRadioButton();
      // rButton.addActionListener(new RadioButtonActionListener());
      row = new Vector<>();
      row.add(rasterFileList.get(i));
      row.add(weightList.get(i) * 100.0);
      row.add(weightList.get(i) * 100.0);
      row.add(weightList.get(i) * 100.0);
      row.add(0.0);
      // buttonGroup.add(rButton);
      // row.add(fileList.get(i));
      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<>();
    titleVec.add("File");
    titleVec.add("Weight");
    titleVec.add("Minimum Weight");
    titleVec.add("Maximum Weight");
    titleVec.add("Step Size");

    PropertiesTableModel tableModel = new PropertiesTableModel(dataVector, titleVec);
    this.jTable1.setModel(tableModel);
    this.jScrollPane1.setViewportView(this.jTable1);

    NumberColumnFormat numberColumn = new NumberColumnFormat("##.##", "##.##");
    this.jTable1.getColumnModel().getColumn(1).setCellEditor(numberColumn.getEditor());
    this.jTable1.getColumnModel().getColumn(1).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(2).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(2).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(3).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(3).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(4).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(4).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.setRowHeight(24);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400, Short.MAX_VALUE)
        .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 200, Short.MAX_VALUE)
        .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE));
  }

  @Override
  public String toString() {
    return "Systematic Weight Variation";
  }

  private static class PropertiesTableModel extends DefaultTableModel {
    private static final long serialVersionUID = -4487353833927280683L;

    boolean[] canEdit = new boolean[]
        {false, false, true, true, true};

    public PropertiesTableModel(Vector<Vector<Object>> data, Vector<String> columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return this.canEdit[columnIndex];
    }
  }

  @SuppressWarnings("serial")
  private static class SpinnerEditor extends AbstractCellEditor implements TableCellEditor {
    final JSpinner spinner = new JSpinner();
    SpinnerNumberModel spinnerNumberModel;
    JSpinner.NumberEditor editor;

    public SpinnerEditor() {
      this.spinnerNumberModel = new SpinnerNumberModel(0.0, 0.0, 100.0, 0.5);
      this.spinner.setModel(this.spinnerNumberModel);
      this.editor = new JSpinner.NumberEditor(this.spinner, "0.##");
      this.spinner.setEditor(this.editor);
    }

    public Object getCellEditorValue() {
      return this.spinner.getValue();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      this.spinner.setValue(value);
      return this.spinner;
    }

    public boolean isCellEditable(EventObject evt) {
      if (evt instanceof MouseEvent) {
        return ((MouseEvent) evt).getClickCount() >= 2;
      }
      return true;
    }
  }
}

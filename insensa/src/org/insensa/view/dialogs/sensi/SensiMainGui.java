/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;


import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CIndexation;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.ConnectionManager;
import org.insensa.view.View;
import org.insensa.view.dialogs.ChooseOutputDialog;
import org.insensa.view.dialogs.SettingsDialog;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;


public class SensiMainGui extends SettingsDialog implements ChooseOutputDialog {
  private static final Logger log = LoggerFactory.getLogger(SensiMainGui.class);
  private static final long serialVersionUID = -2216432862763964108L;
  // private JButton buttonCancel;
  // private JButton buttonOk;
  // private JLabel labelStatus;
  private JScrollPane scrollPaneLocation;
  private JScrollPane scrollPaneMode;
  private JSplitPane splitPane;
  private JTabbedPane tappedPane;
  // private JTextArea textInfo;
  private ModeBase modeBase;
  private Location location;
  private OutputMaps outputMaps;
  private StatisticalOutput statOutput;
  private JPanel panel;
  private Model model;
  private View view;
  private RasterFileContainer activeFile;
  private ConnectionFileChanger connectionCopy;

  public SensiMainGui(java.awt.Frame parent, boolean modal,
                      Model model, View view, RasterFileContainer activeFile) throws IOException {
    super(parent, modal);
    this.model = model;
    this.view = view;
    for (IGisFileContainer iFile : activeFile.getConnectionFileChanger().getSourceFileList()) {
      if (!iFile.getGisFile().getFileRef().exists())
        throw new IOException("One ore more Source Files do not exist");
    }
    this.activeFile = activeFile;
    ConnectionManager cManager = new ConnectionManager();
    try {
      this.connectionCopy = cManager.getConnectionFileChangerCopy(activeFile.getConnectionFileChanger());
    } catch (Exception e) {
      throw new IOException("Error Reading Connection\n" + e.getMessage());
    }
    // super.initComponents();
    this.initComponents();
    super.initComponents(this.layoutComponents());
    super.setHeadTitle("Set Sensitivity Analysis\nSettings");
    super.getButtonOk().addActionListener(new buttonOkAction());
    getButtonCancel().addActionListener(e -> SensiMainGui.this.setVisible(false));
    super.setModalityType(ModalityType.MODELESS);
    // layoutComponents();
    this.setAlwaysOnTop(true);
    super.setSize(750, 600);
  }

  private void initComponents() throws IOException {
    // textInfo = new JTextArea();
    // JPanel panel = new JPanel();
    this.tappedPane = new JTabbedPane();
    this.scrollPaneLocation = new JScrollPane();
    this.scrollPaneMode = new JScrollPane();

    this.splitPane = new JSplitPane();
    // labelStatus = new JLabel();
    // buttonCancel = new JButton();
    // buttonOk = new JButton();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    // textInfo.setColumns(20);
    // textInfo.setEditable(false);
    // textInfo.setRows(5);
    // textInfo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    this.splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    this.splitPane.setEnabled(false);
    this.splitPane.setDividerLocation(0.3);
    this.splitPane.setDividerSize(4);

    this.location = new Location();
    this.outputMaps = new OutputMaps();
    this.statOutput = new StatisticalOutput();
    List<IMode> modeList = new ArrayList<>();
    modeList.add(new ModeJackknifing());

    modeList.add(new ModeHighLow((CIndexation) this.connectionCopy));
    modeList.add(new ModeSystematicVariation((CIndexation) this.connectionCopy));
    modeList.add(new ModeRandomVariation((CIndexation) this.connectionCopy));

    this.modeBase = new ModeBase(modeList, this.splitPane);
    this.splitPane.setTopComponent(this.modeBase);

    this.splitPane.setBottomComponent((Component) modeList.get(0));

    this.tappedPane.addTab("Location", this.scrollPaneLocation);
    this.scrollPaneLocation.setViewportView(this.location);

    this.tappedPane.addTab("Mode", this.splitPane);
    // tappedPane.addTab("Mode", scrollPaneMode);
    // scrollPaneMode.setViewportView(splitPane);

    this.tappedPane.addTab("OutputMaps", this.outputMaps);

    this.tappedPane.addTab("Statistical Output", this.statOutput);
    // panel.add(tappedPane);
    // return panel;
    // labelStatus.setText("jLabel1");
    // labelStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    // buttonCancel.setText("Cancel");
    // buttonCancel.addActionListener(new ActionListener(){
    // @Override
    // public void actionPerformed(ActionEvent e)
    // {
    // setVisible(false);
    // }});

    // buttonOk.setText("Ok");
    // buttonOk.addActionListener(new buttonOkAction());

  }

  /**
   *
   */
  private JPanel layoutComponents() {
    this.panel = new JPanel();
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.panel);
    this.panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).
        // addComponent(tappedPane,javax.swing.GroupLayout.PREFERRED_SIZE,
        // javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE));
            addComponent(this.tappedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        // .addComponent(tappedPane, javax.swing.GroupLayout.PREFERRED_SIZE,
        // javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE));
        .addComponent(this.tappedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE));
    this.pack();
    return this.panel;
  }

  /**
   * @see ChooseOutputDialog#setFile(IGisFileContainer)
   */
  @Override
  public void setFile(IGisFileContainer file) {
    // TODO Auto-generated method stub

  }

  /**
   * @see org.insensa.view.dialogs.ChooseOutputDialog#setFileSet(GenericGisFileSet)
   */
  @Override
  public void setFileSet(GenericGisFileSet fileSet) {
    this.location.setFileSet(fileSet);
  }

  /**
   * @see org.insensa.view.dialogs.ChooseOutputDialog#setRefreshingTreeNode(javax.swing.tree.DefaultMutableTreeNode)
   */
  @Override
  public void setRefreshingTreeNode(DefaultMutableTreeNode treeNode) {
    this.location.setTreeNode(treeNode);
  }

  class buttonOkAction implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        SensiMainGui.this.location
            .applySettings(SensiMainGui.this.model, SensiMainGui.this.getLabelStatus());

        GenericGisFileSet outputFileSet = (GenericGisFileSet) SensiMainGui.this.location.getOutputFileSet();
        CIndexation prioritization = (CIndexation) SensiMainGui.this.connectionCopy;
        String outputFileName = SensiMainGui.this.location.getOutputFileName();

        SensiMainGui.this.modeBase
            .applySettings(SensiMainGui.this.getLabelStatus(),
                SensiMainGui.this.model,
                outputFileSet,
                prioritization,
                outputFileName);

        SensiMainGui.this.outputMaps.getSummaries()
            .applySettings(SensiMainGui.this.model,
                outputFileSet,
                SensiMainGui.this.modeBase.getSensiList(),
                outputFileName);
        SensiMainGui.this.outputMaps.getDifferences()
            .applySettings(SensiMainGui.this.model,
                outputFileSet,
                SensiMainGui.this.modeBase.getSensiList(),
                SensiMainGui.this.activeFile,
                outputFileName);

        SensiMainGui.this.statOutput.getMeanChange()
            .applySettings(SensiMainGui.this.outputMaps.getDifferences());
        SensiMainGui.this.statOutput.getCountingZeroRangeUnits()
            .applySettings(SensiMainGui.this.outputMaps.getDifferences());

        SensiMainGui.this.view.refreshTreeRoot(true);
        SensiMainGui.this.setVisible(false);
      } catch (IOException | JDOMException e1) {
        log.error("Sensi Errir", e1);
      }
    }

  }

}

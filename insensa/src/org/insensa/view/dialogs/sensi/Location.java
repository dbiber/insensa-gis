/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Model;
import org.insensa.exceptions.GisFileException;
import org.jdom.JDOMException;

import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;


public class Location extends javax.swing.JPanel {

  private static final long serialVersionUID = -7764562609088990454L;
  String separator = System.getProperties().getProperty("file.separator");
  private JCheckBox checkBoxNewSub;
  private JPanel panelNewSub;
  private JPanel panelOutName;
  private JPanel panelOutSet;
  private JTextField textNewSubfolder;
  private JTextField textOutputFileSet;

  private JTextField textOutputFilename;
  private IGisFileSet fileSet;
  private DefaultMutableTreeNode treeNode;

  private JLabel statusLabel;
  private IGisFileSet outputFileSet;
  private String outputFileName;
  private boolean isSelected = false;

  /**
   * Creates new form Location.
   */
  public Location() {
    this.initComponents();
    this.layoutComponents();
  }

  public boolean applySettings(Model model, JLabel statusLabel) throws IOException, JDOMException, GisFileException {
    this.statusLabel = statusLabel;
    if (!this.checkInputValues())
      return false;
    if (this.isSelected)
      this.outputFileSet = model.addChildFileSet(this.fileSet, this.textNewSubfolder.getText(), true);
    else
      this.outputFileSet = this.fileSet;
    return true;
  }

  private boolean checkInputValues() {
    String outputSetPath = "";
    if (this.textOutputFilename.getText().isEmpty()) {
      this.statusLabel.setText("File name is missing");
      return false;
    }
    if (this.fileSet == null) {
      this.statusLabel.setText("No output fileset selected");
      return false;
    }
    if (this.isSelected) {
      outputSetPath = this.fileSet.getPath() + this.fileSet.getName() + this.separator + "FileSets" + this.separator + this.textNewSubfolder.getText();
      File newFileSet = new File(outputSetPath);
      if (newFileSet.exists()) {
        this.statusLabel.setText("Fileset allready exists");
        return false;
      }
    }
    List<IGisFileContainer> fileList = this.fileSet.getFileList();
    String newFullFileName = this.textOutputFilename.getText();
    String newFileName;
    if (!newFullFileName.endsWith(".tif")) {
      newFileName = newFullFileName;
      newFullFileName = newFileName + ".tif";
    }
    for (int i = 0; i < fileList.size(); i++) {
      String oldFileName = fileList.get(i).getOutputFileName();

      if (oldFileName.equals(newFullFileName)) {
        this.statusLabel.setText("File already exists");
        return false;
      }
    }
    this.outputFileName = newFullFileName;
    return true;
  }

  public IGisFileSet getFileSet() {
    return this.fileSet;
  }

  public void setFileSet(GenericGisFileSet fileSet) {
    this.fileSet = fileSet;
    this.textOutputFileSet.setText(fileSet.getName());
  }

  public String getOutputFileName() {
    return this.outputFileName;
  }

  public IGisFileSet getOutputFileSet() {
    return this.outputFileSet;
  }

  public DefaultMutableTreeNode getTreeNode() {
    return this.treeNode;
  }

  public void setTreeNode(DefaultMutableTreeNode treeNode) {
    this.treeNode = treeNode;
  }

  private void initComponents() {

    this.panelOutName = new JPanel();
    this.textOutputFilename = new JTextField();
    this.panelOutSet = new JPanel();
    this.textOutputFileSet = new JTextField();
    this.panelNewSub = new JPanel();
    this.textNewSubfolder = new JTextField();
    this.checkBoxNewSub = new JCheckBox();

    Font mainFont = new Font("Dialog", Font.PLAIN, 12);

    this.panelOutName.setBorder(BorderFactory.createTitledBorder(null, "Output Filename", TitledBorder.DEFAULT_JUSTIFICATION,
        TitledBorder.DEFAULT_POSITION, mainFont));
    this.panelOutSet.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Output Fileset", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
        javax.swing.border.TitledBorder.DEFAULT_POSITION, mainFont));
    this.panelNewSub.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "New Subfolder", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
        javax.swing.border.TitledBorder.DEFAULT_POSITION, mainFont));

    this.textOutputFileSet.setEditable(false);
    this.textNewSubfolder.setEditable(false);
    this.textNewSubfolder.setEnabled(false);
    this.checkBoxNewSub.addItemListener(new NewSubfolderCheckBoxStateChange());

  }// </editor-fold>

  private void layoutComponents() {
    javax.swing.GroupLayout panelOutNameLayout = new javax.swing.GroupLayout(this.panelOutName);
    this.panelOutName.setLayout(panelOutNameLayout);
    panelOutNameLayout.setHorizontalGroup(panelOutNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelOutNameLayout.createSequentialGroup().addContainerGap()
            .addComponent(this.textOutputFilename, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE).addContainerGap()));
    panelOutNameLayout
        .setVerticalGroup(panelOutNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            panelOutNameLayout.createSequentialGroup().addContainerGap().addComponent(this.textOutputFilename)));

    javax.swing.GroupLayout panelOutSetLayout = new javax.swing.GroupLayout(this.panelOutSet);
    this.panelOutSet.setLayout(panelOutSetLayout);
    panelOutSetLayout.setHorizontalGroup(panelOutSetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelOutSetLayout.createSequentialGroup().addContainerGap()
            .addComponent(this.textOutputFileSet, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE).addContainerGap()));
    panelOutSetLayout.setVerticalGroup(panelOutSetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING, panelOutSetLayout.createSequentialGroup().addContainerGap().addComponent(this.textOutputFileSet)));

    javax.swing.GroupLayout panelNewSubLayout = new javax.swing.GroupLayout(this.panelNewSub);
    this.panelNewSub.setLayout(panelNewSubLayout);
    panelNewSubLayout.setHorizontalGroup(panelNewSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelNewSubLayout.createSequentialGroup().addContainerGap().addComponent(this.checkBoxNewSub)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.textNewSubfolder, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE).addContainerGap()));
    panelNewSubLayout.setVerticalGroup(panelNewSubLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelNewSubLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                panelNewSubLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(this.textNewSubfolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.checkBoxNewSub)).addGap(12, 12, 12)));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(this.panelNewSub, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)
                    .addComponent(this.panelOutSet, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(this.panelOutName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(this.panelOutName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(this.panelOutSet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
            .addComponent(this.panelNewSub, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap()));
  }

  class NewSubfolderCheckBoxStateChange implements ItemListener {

    @Override
    public void itemStateChanged(ItemEvent e) {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        Location.this.isSelected = true;
        Location.this.textNewSubfolder.setEditable(true);
        Location.this.textNewSubfolder.setEnabled(true);
      } else {
        Location.this.isSelected = false;
        Location.this.textNewSubfolder.setEditable(false);
        Location.this.textNewSubfolder.setEnabled(false);
      }
    }

  }

}

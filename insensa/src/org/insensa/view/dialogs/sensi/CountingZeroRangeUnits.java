/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.IGisFileContainer;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.ZeroRangeValues;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.List;

import javax.swing.SpinnerNumberModel;


public class CountingZeroRangeUnits extends javax.swing.JPanel
{


	private static final long serialVersionUID = -5592785615917454264L;
	// private javax.swing.JCheckBox checkBoxRelVal;
	private javax.swing.JCheckBox checkBoxVal;
	private javax.swing.JScrollPane jScrollPane22;
	private javax.swing.JLabel labelVal1;
	// private javax.swing.JLabel labelVal2;
	private javax.swing.JPanel panelCountChangingUnits;
	private javax.swing.JSpinner spinnerVal1;
	// private javax.swing.JSpinner spinnerVal2;
	private javax.swing.JTextArea textCountChangingUnits;
	private boolean isValue = false;

	// private boolean isRelVal=false;

	/**
	 * Creates new form ChaningUnitsCounts.
	 */
	public CountingZeroRangeUnits()
	{
		this.initComponents();
		this.layoutComponents();
	}

	/**
	 * @param diffs
	 * @return
	 * @throws IOException
	 */
	public boolean applySettings(Differences diffs) throws IOException
	{
		InfoManager iManager = new InfoManager();
		if (this.isValue == true)
		{
			List<IGisFileContainer> fileList = null;
			/*
			 * fileList = diffs.getDiffValueList(); for(RasterFileContainer
			 * iFile : fileList) { ZeroRangeValues zeroRangeValue =
			 * (ZeroRangeValues) iManager.getInfoReader(EInfo.zeroRangeValues);
			 * zeroRangeValue
			 * .setRange(((Double)spinnerVal1.getValue()).floatValue()/100.0F);
			 * iFile.addInfoReader(zeroRangeValue, true); }
			 */
			fileList = diffs.getDiffRelValueList();
			for (IGisFileContainer iFile : fileList)
			{
				ZeroRangeValues zeroRangeValue = (ZeroRangeValues) iManager.getInfoReader(ZeroRangeValues.NAME);
				// zeroRangeValue.setRange(((Double)spinnerVal1.getValue()).floatValue()/100.0F);
				zeroRangeValue.setRangeValue(((Double) this.spinnerVal1.getValue()).floatValue());
				zeroRangeValue.setCalcMode(ZeroRangeValues.VALUE);
				iFile.addInfoReader(zeroRangeValue, true);
			}
			/*
			 * fileList = diffs.getDiffAbsValueList();
			 * for(RasterFileContainer iFile : fileList) { ZeroRangeValues
			 * zeroRangeValue = (ZeroRangeValues)
			 * iManager.getInfoReader(EInfo.zeroRangeValues);
			 * zeroRangeValue.setRange
			 * (((Double)spinnerVal1.getValue()).floatValue()/100.0F);
			 * iFile.addInfoReader(zeroRangeValue, true); }
			 */
			fileList = diffs.getDiffRelAbsValueList();
			for (IGisFileContainer iFile : fileList)
			{
				ZeroRangeValues zeroRangeValue = (ZeroRangeValues) iManager.getInfoReader(ZeroRangeValues.NAME);
				// zeroRangeValue.setRange(((Double)spinnerVal1.getValue()).floatValue()/100.0F);
				zeroRangeValue.setRangeValue(((Double) this.spinnerVal1.getValue()).floatValue());
				zeroRangeValue.setCalcMode(ZeroRangeValues.VALUE);
				iFile.addInfoReader(zeroRangeValue, true);
			}
		}

		return true;
	}


	private void initComponents()
	{

		this.panelCountChangingUnits = new javax.swing.JPanel();
		this.checkBoxVal = new javax.swing.JCheckBox();
		// checkBoxRelVal = new javax.swing.JCheckBox();
		this.labelVal1 = new javax.swing.JLabel();
		this.spinnerVal1 = new javax.swing.JSpinner(new SpinnerNumberModel(0, 0, 100, 0.5));
		// labelVal2 = new javax.swing.JLabel();
		// spinnerVal2 = new javax.swing.JSpinner();
		this.jScrollPane22 = new javax.swing.JScrollPane();
		this.textCountChangingUnits = new javax.swing.JTextArea();

		this.panelCountChangingUnits.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Count Zero Range Units",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog 12 12", 0,
						12))); // NOI18N

		this.checkBoxVal.setFont(new java.awt.Font("Dialog 12 12", 0, 12)); // NOI18N
		this.checkBoxVal.setText("enable");
		this.checkBoxVal.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
					CountingZeroRangeUnits.this.isValue = true;
				else
					CountingZeroRangeUnits.this.isValue = false;
			}
		});

		/*
		 * checkBoxRelVal.setFont(new java.awt.Font("Dialog 12 12", 0, 12)); //
		 * NOI18N checkBoxRelVal.setText("relative value");
		 * checkBoxRelVal.addItemListener(new ItemListener(){
		 * @Override public void itemStateChanged(ItemEvent e) {
		 * if(e.getStateChange()==ItemEvent.SELECTED) isRelVal=true; else
		 * isRelVal=false; }});
		 */

		this.labelVal1.setText("Range (%)");

		// labelVal2.setText("Value 2 (%)");

		this.jScrollPane22.setBorder(null);
		this.jScrollPane22.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.jScrollPane22.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.textCountChangingUnits.setBackground(new java.awt.Color(238, 238, 238));
		this.textCountChangingUnits.setColumns(20);
		this.textCountChangingUnits.setLineWrap(true);
		this.textCountChangingUnits.setRows(5);
		this.textCountChangingUnits.setText("Counts units around zero with distance \"Range\"");
		this.textCountChangingUnits.setWrapStyleWord(true);
		this.jScrollPane22.setViewportView(this.textCountChangingUnits);

	}


	private void layoutComponents()
	{
		javax.swing.GroupLayout panelCountChangingUnitsLayout = new javax.swing.GroupLayout(this.panelCountChangingUnits);
		this.panelCountChangingUnits.setLayout(panelCountChangingUnitsLayout);
		panelCountChangingUnitsLayout.setHorizontalGroup(panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelCountChangingUnitsLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(panelCountChangingUnitsLayout.createSequentialGroup().addComponent(this.checkBoxVal))
										// .addGap(18, 18, 18)
										// .addComponent(checkBoxRelVal))
										.addGroup(
												panelCountChangingUnitsLayout
														.createSequentialGroup()
														.addGroup(
																panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.labelVal1))
														// .addComponent(labelVal2))
														.addGap(18, 18, 18)
														.addGroup(
																panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING,
																		false)
																// .addComponent(spinnerVal2)
																		.addComponent(this.spinnerVal1, javax.swing.GroupLayout.DEFAULT_SIZE, 131,
																				Short.MAX_VALUE)))).addGap(18, 18, 18)
						.addComponent(this.jScrollPane22, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE).addContainerGap()));
		panelCountChangingUnitsLayout.setVerticalGroup(panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelCountChangingUnitsLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelCountChangingUnitsLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(this.jScrollPane22, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 99,
												Short.MAX_VALUE)
										.addGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												panelCountChangingUnitsLayout
														.createSequentialGroup()
														.addGroup(
																panelCountChangingUnitsLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.labelVal1)
																		.addComponent(this.spinnerVal1, javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
														// .addGap(18, 18, 18)
														// .addGroup(panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
														// .addComponent(labelVal2)
														// .addComponent(spinnerVal2,
														// javax.swing.GroupLayout.PREFERRED_SIZE,
														// javax.swing.GroupLayout.DEFAULT_SIZE,
														// javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGap(18, 18, 18)
														.addGroup(
																panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(this.checkBoxVal)
														// .addComponent(checkBoxRelVal)
														))).addContainerGap()));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panelCountChangingUnits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panelCountChangingUnits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}

}

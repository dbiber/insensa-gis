/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.IGisFileContainer;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.MeanValue;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.List;



public class MeanChange extends javax.swing.JPanel
{


	private static final long serialVersionUID = -317370678257955044L;
	private javax.swing.JCheckBox checkBoxAbsRelVal;
	private javax.swing.JCheckBox checkBoxAbsVal;
	private javax.swing.JCheckBox checkBoxRelVal;
	private javax.swing.JCheckBox checkBoxValue;

	private boolean isAbsRelVal = false;
	private boolean isAbsVal = false;
	private boolean isRelVal = false;
	private boolean isValue = false;

	private javax.swing.JScrollPane jScrollPane21;
	private javax.swing.JPanel panelMeanChange;
	private javax.swing.JEditorPane editorPaneMean;

	/**
	 * Creates new form MeanChange.
	 */
	public MeanChange()
	{
		this.initComponents();
		this.layoutComponents();
	}

	/**
	 * @param diffs
	 * @return
	 * @throws IOException
	 */
	public boolean applySettings(Differences diffs) throws IOException
	{
		InfoManager iManager = new InfoManager();
		if (this.isValue == true)
		{
			List<IGisFileContainer> fileList = diffs.getDiffValueList();
			if (fileList.isEmpty() == false)
				for (IGisFileContainer iFile : fileList)
				{
					iFile.addInfoReader(iManager.getInfoReader(MeanValue.NAME), true);
				}
		}
		if (this.isRelVal == true)
		{
			List<IGisFileContainer> fileList = diffs.getDiffRelValueList();
			if (fileList.isEmpty() == false)
				for (IGisFileContainer iFile : fileList)
				{
					iFile.addInfoReader(iManager.getInfoReader(MeanValue.NAME), true);
				}
		}
		if (this.isAbsVal == true)
		{
			List<IGisFileContainer> fileList = diffs.getDiffAbsValueList();
			if (fileList.isEmpty() == false)
				for (IGisFileContainer iFile : fileList)
				{
					iFile.addInfoReader(iManager.getInfoReader(MeanValue.NAME), true);
				}
		}
		if (this.isAbsRelVal == true)
		{
			List<IGisFileContainer> fileList = diffs.getDiffRelAbsValueList();
			if (fileList.isEmpty() == false)
				for (IGisFileContainer iFile : fileList)
				{
					iFile.addInfoReader(iManager.getInfoReader(MeanValue.NAME), true);
				}
		}
		return true;
	}


	private void initComponents()
	{

		this.panelMeanChange = new javax.swing.JPanel();
		this.checkBoxValue = new javax.swing.JCheckBox();
		this.checkBoxRelVal = new javax.swing.JCheckBox();
		this.checkBoxAbsRelVal = new javax.swing.JCheckBox();
		this.checkBoxAbsVal = new javax.swing.JCheckBox();
		this.jScrollPane21 = new javax.swing.JScrollPane();
		this.editorPaneMean = new javax.swing.JEditorPane();

		this.panelMeanChange.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculate Mean Change",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog 12 12 12",
						0, 12))); // NOI18N

		this.checkBoxValue.setFont(new java.awt.Font("Dialog 12 12 12", 0, 12)); // NOI18N
		this.checkBoxValue.setText("value");
		this.checkBoxValue.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
					MeanChange.this.isValue = true;
				else
					MeanChange.this.isValue = false;
			}
		});

		this.checkBoxRelVal.setFont(new java.awt.Font("Dialog 12 12 12", 0, 12)); // NOI18N
		this.checkBoxRelVal.setText("relative value");
		this.checkBoxRelVal.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
					MeanChange.this.isRelVal = true;
				else
					MeanChange.this.isRelVal = false;
			}
		});
		// checkBoxRelVal.setEnabled(false);

		this.checkBoxAbsRelVal.setFont(new java.awt.Font("Dialog 12 12 12 12", 0, 12)); // NOI18N
		this.checkBoxAbsRelVal.setText("absolute relative value");
		this.checkBoxAbsRelVal.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
					MeanChange.this.isAbsRelVal = true;
				else
					MeanChange.this.isAbsRelVal = false;
			}
		});
		// checkBoxAbsRelVal.setEnabled(false);

		this.checkBoxAbsVal.setFont(new java.awt.Font("Dialog 12 12 12 12", 0, 12)); // NOI18N
		this.checkBoxAbsVal.setText("absolute value");
		this.checkBoxAbsVal.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
					MeanChange.this.isAbsVal = true;
				else
					MeanChange.this.isAbsVal = false;
			}
		});
		// checkBoxAbsVal.setEnabled(false);

		this.jScrollPane21.setBorder(null);
		this.jScrollPane21.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.jScrollPane21.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.editorPaneMean.setContentType("text/html");
		this.editorPaneMean.setBackground(new java.awt.Color(238, 238, 238));
		this.editorPaneMean.setText("Calculates the mean difference between the original and the modified index file as value,<br>"
				+ "absolute value, relative value or absolute relative value.");
		this.editorPaneMean.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
		this.editorPaneMean.setEditable(false);
		this.jScrollPane21.setViewportView(this.editorPaneMean);

	}


	private void layoutComponents()
	{

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.panelMeanChange);
		this.panelMeanChange.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								jPanel1Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												jPanel1Layout.createSequentialGroup().addComponent(this.checkBoxValue)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
														.addComponent(this.checkBoxRelVal))
										.addGroup(
												jPanel1Layout.createSequentialGroup().addComponent(this.checkBoxAbsVal)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
														.addComponent(this.checkBoxAbsRelVal)))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(this.jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE).addContainerGap()));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout
						.createSequentialGroup()
						.addGroup(
								jPanel1Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												jPanel1Layout
														.createSequentialGroup()
														.addContainerGap()
														.addGroup(
																jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(this.checkBoxValue).addComponent(this.checkBoxRelVal))
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
														.addGroup(
																jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(this.checkBoxAbsVal).addComponent(this.checkBoxAbsRelVal)))
										.addComponent(this.jScrollPane21, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(39, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap()
						.addComponent(this.panelMeanChange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panelMeanChange, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(274, Short.MAX_VALUE)));
		// javax.swing.GroupLayout panelMeanChangeLayout = new
		// javax.swing.GroupLayout(panelMeanChange);
		// panelMeanChange.setLayout(panelMeanChangeLayout);
		// panelMeanChangeLayout.setHorizontalGroup(
		// panelMeanChangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addGroup(panelMeanChangeLayout.createSequentialGroup()
		// .addContainerGap()
		// .addGroup(panelMeanChangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addComponent(checkBoxRelVal)
		// .addComponent(checkBoxValue))
		// .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
		// .addGroup(panelMeanChangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addComponent(checkBoxAbsVal)
		// .addComponent(checkBoxAbsRelVal))
		// .addGap(18, 18, 18)
		// .addComponent(jScrollPane21, 10, GroupLayout.PREFERRED_SIZE,
		// javax.swing.GroupLayout.PREFERRED_SIZE)
		// .addContainerGap()
		// )
		// );
		// panelMeanChangeLayout.setVerticalGroup(
		// panelMeanChangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addGroup(panelMeanChangeLayout.createSequentialGroup()
		// .addContainerGap()
		// .addGroup(panelMeanChangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
		// .addComponent(checkBoxValue)
		// .addComponent(checkBoxAbsVal))
		// .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
		// .addGroup(panelMeanChangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
		// .addComponent(checkBoxRelVal)
		// .addComponent(checkBoxAbsRelVal))
		// .addContainerGap(24, Short.MAX_VALUE))
		// .addComponent(jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE,
		// 78, Short.MAX_VALUE)
		// );
		//
		// javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		// this.setLayout(layout);
		// layout.setHorizontalGroup(
		// layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addGroup(layout.createSequentialGroup()
		// .addContainerGap()
		// .addComponent(panelMeanChange,
		// javax.swing.GroupLayout.PREFERRED_SIZE,
		// javax.swing.GroupLayout.DEFAULT_SIZE,
		// javax.swing.GroupLayout.PREFERRED_SIZE)
		// .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
		// Short.MAX_VALUE))
		// );
		// layout.setVerticalGroup(
		// layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addGroup(layout.createSequentialGroup()
		// .addContainerGap()
		// .addComponent(panelMeanChange,
		// javax.swing.GroupLayout.PREFERRED_SIZE,
		// javax.swing.GroupLayout.DEFAULT_SIZE,
		// javax.swing.GroupLayout.PREFERRED_SIZE)
		// .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
		// Short.MAX_VALUE))
		// );
	}

}

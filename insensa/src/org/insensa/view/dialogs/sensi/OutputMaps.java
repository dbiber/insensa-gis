/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class OutputMaps extends JTabbedPane {
  private static final long serialVersionUID = 8817169976878344000L;
  private Differences diffs;
  private Summaries summaries;
  private JScrollPane summariesScrollPane;
  private JScrollPane diffsScrollPane;


  public OutputMaps() {
    this.initComponents();
  }

  public Differences getDifferences() {
    return this.diffs;
  }

  public Summaries getSummaries() {
    return this.summaries;
  }


  public void initComponents() {
    this.diffs = new Differences();
    this.summaries = new Summaries();
    this.summariesScrollPane = new JScrollPane();
    this.diffsScrollPane = new JScrollPane();
    this.summariesScrollPane.setViewportView(this.summaries);
    this.diffsScrollPane.setViewportView(this.diffs);

    super.setTabPlacement(super.LEFT);
    this.addTab("Summaries", this.summariesScrollPane);
    this.addTab("Differences", this.diffsScrollPane);
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.RasterFileContainer;
import org.insensa.connections.ErrorIndexation;
import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.connections.CIndexation;
import org.insensa.connections.ConnectionManager;
import org.insensa.view.dialogs.helper.NumberColumnFormat;
import org.jdom.JDOMException;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;


public class ModeHighLow extends JPanel implements IMode {

  private static final long serialVersionUID = 1L;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable jTable1;
  private CIndexation indexation;
  private List<IGisFileContainer> tmpConList = new ArrayList<IGisFileContainer>();

  /**
   * Creates new form BestWorstMode.
   */
  public ModeHighLow(CIndexation indexation) throws IOException {
    this.indexation = indexation;
    this.initComponents();
  }

  @Override
  public boolean applySettings(JLabel labelStatus, Model model,
                               GenericGisFileSet outputFileSet,
                               CIndexation indexation, String outputFileName)
      throws IOException, JDOMException, GisFileException {
    List<RasterFileContainer> fileList = indexation.getSourceFileList();
    List<RasterFileContainer> newFileList = new ArrayList<>(fileList);
    ConnectionManager connectionManager = new ConnectionManager();
    ErrorIndexation tmpCon;
    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
    String outputFileNameNew;
    int fileNameIterator = 0;
    this.tmpConList.clear();

    fileNameIterator++;

    outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
    tmpCon = (ErrorIndexation) connectionManager.getConnectionFileChanger("ErrorIndexation");
    connectionManager.copyConnection(indexation, tmpCon);
    tmpCon.setUsed(false);
    model.addConnection(newFileList, outputFileSet, tmpCon, null, outputFileNameNew);

    for (int i = 0; i < this.jTable1.getRowCount(); i++) {
      Double value1 = (Double) this.jTable1.getValueAt(i, 2);
      Double value2 = (Double) this.jTable1.getValueAt(i, 3);
      value1 /= 100.0;
      value2 /= 100.0;
      tmpCon.getWeighting(newFileList.get(i)).setLowerError(value1.floatValue());
      tmpCon.getWeighting(newFileList.get(i)).setUpperError(value2.floatValue());
    }
    tmpCon.setErrorCase(ErrorIndexation.BEST_CASE);
    tmpCon.setMethod(indexation.getMethod());
    this.tmpConList.add(tmpCon.getOutputFileContainer());

    fileNameIterator++;
    outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
    tmpCon = (ErrorIndexation) connectionManager.getConnectionFileChanger("ErrorIndexation");
    connectionManager.copyConnection(indexation, tmpCon);
    tmpCon.setUsed(false);
    model.addConnection(newFileList, outputFileSet, tmpCon, null, outputFileNameNew);
    tmpCon.getOutputFileContainer().setDescription("File with index values according to your settings\n but with error weighting in LOW CASE.");

    tmpCon.setUsed(false);
    for (int i = 0; i < this.jTable1.getRowCount(); i++) {
      Double value1 = (Double) this.jTable1.getValueAt(i, 2);
      Double value2 = (Double) this.jTable1.getValueAt(i, 3);
      value1 /= 100.0;
      value2 /= 100.0;
      tmpCon.getWeighting(newFileList.get(i)).setLowerError(value1.floatValue());
      tmpCon.getWeighting(newFileList.get(i)).setUpperError(value2.floatValue());
    }
    tmpCon.setErrorCase(ErrorIndexation.WORST_CASE);
    tmpCon.setMethod(indexation.getMethod());
    this.tmpConList.add(tmpCon.getOutputFileContainer());
    return true;
  }

  @Override
  public List<IGisFileContainer> getSensiList() {
    return this.tmpConList;
  }

  @Override
  public String getShortDescription() {
    String description = "New weights are assigned to each indicator according to the distribution of the data.<br>"
        + "Two modified files, one high and one low scenario, are calculated.<br>"
        + "For this procedure a certain range of weight has to be assigned to each factor.";
    return description;
  }

  private void initComponents() throws IOException {

    this.jScrollPane1 = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();

    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    List<RasterFileContainer> rasterFileList = this.indexation.getSourceFileList();
//    List<Float> weightList = this.indexation.getWeightList();
    if (indexation.getFileWeightingMap().isEmpty())
      throw new IOException("Weights not set");
    ButtonGroup buttonGroup = new ButtonGroup();
    for (RasterFileContainer container : rasterFileList) {
      row = new Vector<Object>();
      row.add(container);
      CIndexation.Weighting weighting = indexation.getWeighting(container);
      row.add(weighting.getWeight() * 100.0);
      row.add(weighting.getWeight() * 100.0);
      row.add(weighting.getWeight() * 100.0);
      dataVector.add(row);
    }

//    for (int i = 0; i < rasterFileList.size(); i++) {
//      row = new Vector<Object>();
//      row.add(rasterFileList.get(i));
//      row.add(new Double(weightList.get(i) * 100.0));
//      row.add(new Double(weightList.get(i) * 100.0));
//      row.add(new Double(weightList.get(i) * 100.0));
//      dataVector.add(row);
//    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("File");
    titleVec.add("Weight");
    titleVec.add("Minimum Weight");
    titleVec.add("Maximum Weight");

    PropertiesTableModel tableModel = new PropertiesTableModel(dataVector, titleVec);
    this.jTable1.setModel(tableModel);
    this.jScrollPane1.setViewportView(this.jTable1);

    NumberColumnFormat numberColumn = new NumberColumnFormat("##.##", "##.##");
    this.jTable1.getColumnModel().getColumn(1).setCellEditor(numberColumn.getEditor());
    this.jTable1.getColumnModel().getColumn(1).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(2).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(2).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.getColumnModel().getColumn(3).setCellEditor(new SpinnerEditor());
    this.jTable1.getColumnModel().getColumn(3).setCellRenderer(numberColumn.getRenderer());
    this.jTable1.setRowHeight(24);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400, Short.MAX_VALUE)
        .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 200, Short.MAX_VALUE)
        .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE));
  }

  @Override
  public String toString() {
    return "High/Low Case Szenario";
  }

  public class PropertiesTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -4487353833927280683L;

    boolean[] canEdit = new boolean[]
        {false, false, true, true};

    public PropertiesTableModel() {
      super();
    }

    public PropertiesTableModel(Object[][] objects, String[] strings) {
      super(objects, strings);
    }

    public PropertiesTableModel(Vector<Vector<Object>> data, Vector<String> columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return this.canEdit[columnIndex];
    }
  }

  @SuppressWarnings("serial")
  class SpinnerEditor extends AbstractCellEditor implements TableCellEditor {
    final JSpinner spinner = new JSpinner();
    SpinnerNumberModel spinnerNumberModel;
    JSpinner.NumberEditor editor;

    public SpinnerEditor() {
      this.spinnerNumberModel = new SpinnerNumberModel(0.0, 0.0, 100.0, 0.5);
      this.spinner.setModel(this.spinnerNumberModel);
      this.editor = new JSpinner.NumberEditor(this.spinner, "0.##");
      this.spinner.setEditor(this.editor);
    }

    public Object getCellEditorValue() {
      return this.spinner.getValue();
    }

    public JSpinner getSpinner() {
      return this.spinner;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      this.spinner.setValue(value);
      return this.spinner;
    }

    public boolean isCellEditable(EventObject evt) {
      if (evt instanceof MouseEvent) {
        return ((MouseEvent) evt).getClickCount() >= 2;
      }
      return true;
    }
  }

}

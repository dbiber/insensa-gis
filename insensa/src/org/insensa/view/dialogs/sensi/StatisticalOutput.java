/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class StatisticalOutput extends JTabbedPane
{

	private static final long serialVersionUID = 5229670547997491952L;
	private MeanChange meanChange;
	private CountingZeroRangeUnits countingZeroRangeUnits;
	private JScrollPane scrollPaneCUC;
	private JScrollPane scrollPaneMC;


	public StatisticalOutput()
	{
		this.initComponents();
	}

	/**
	 * @return
	 */
	public CountingZeroRangeUnits getCountingZeroRangeUnits()
	{
		return this.countingZeroRangeUnits;
	}

	/**
	 * @return
	 */
	public MeanChange getMeanChange()
	{
		return this.meanChange;
	}


	public void initComponents()
	{
		this.meanChange = new MeanChange();
		this.countingZeroRangeUnits = new CountingZeroRangeUnits();
		this.scrollPaneCUC = new JScrollPane();
		this.scrollPaneMC = new JScrollPane();

		this.scrollPaneCUC.setViewportView(this.countingZeroRangeUnits);
		this.scrollPaneMC.setViewportView(this.meanChange);

		super.setTabPlacement(super.LEFT);
		super.addTab("Mean Change", this.scrollPaneMC);
		super.addTab("<html>Changing Unit<br>Counts</html>", this.scrollPaneCUC);
	}

}

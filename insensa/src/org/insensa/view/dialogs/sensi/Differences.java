/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CMean;
import org.insensa.connections.CSubtraction;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.EConnection;
import org.insensa.exceptions.GisFileException;
import org.jdom.JDOMException;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;


public class Differences extends javax.swing.JPanel {
  private static final long serialVersionUID = 8341009865576038460L;
  private javax.swing.JPanel panelAbsValue;
  private javax.swing.JPanel panelRelAbs;
  private javax.swing.JPanel panelRelValue;
  private javax.swing.JPanel panelValue;
  private List<IGisFileContainer> diffValueList = new ArrayList<>();
  private List<IGisFileContainer> diffAbsValueList = new ArrayList<>();
  private List<IGisFileContainer> diffRelValueList = new ArrayList<>();
  private List<IGisFileContainer> diffRelAbsValueList = new ArrayList<>();

  /**
   * Creates new form Differences.
   */
  public Differences() {
    this.initComponents();
    this.initLayout();
  }

  public boolean applySettings(Model model, GenericGisFileSet outputFileSet, List<IGisFileContainer> sensiFileList, RasterFileContainer sourceFile,
                               String outputFileName) throws IOException, JDOMException, GisFileException {
    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
    List<IGisFileContainer> sourceFileList = new ArrayList<>();
    sourceFileList.addAll(sensiFileList);
    List<IGisFileContainer> subsList = new ArrayList<>();

    if (((CalcValuePanel) this.panelValue).isSelected()) {
      this.diffValueList.clear();
      String tmpSubName = outputFileNameSplit + "_sub";
      String tmpSubName2;
      int index = 0;
      List<IGisFileContainer> subList = new ArrayList<>();
      IGisFileContainer outFile;
      CSubtraction tmpSubConn;
      for (IGisFileContainer iFile : sourceFileList) {
        index++;
        tmpSubName2 = tmpSubName + index + ".tif";
        outFile = iFile;
        subList.add(sourceFile);
        subList.add(outFile);
        tmpSubConn = (CSubtraction) model
            .createAndAddConnection(subList, outputFileSet, EConnection.Subtraction.toString(), "differences", tmpSubName2);
        tmpSubConn.setMinuend((RasterFileContainer) outFile);
        subsList.add(tmpSubConn.getOutputFileContainer());
        subList.clear();
      }
      if (((CalcValuePanel) this.panelValue).isMean()) {
        String newMeanName = outputFileNameSplit + "_subsMean.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, CMean.NAME, "differences", newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelValue).isStdDev()) {
        String newStdDevName = outputFileNameSplit + "_subsStdDev.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.StandardDeviation.toString(), "differences",
            newStdDevName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelValue).isCoeffVar()) {
        String newCoeffVarName = outputFileNameSplit + "_subsCoeffVar.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.CoefficientOfVariation.toString(), "differences",
            newCoeffVarName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      this.diffValueList.addAll(subsList);
      subsList.clear();
    }
    if (((CalcValuePanel) this.panelRelValue).isSelected()) {
      this.diffRelValueList.clear();
      String tmpSubName = outputFileNameSplit + "_sub_rel";
      String tmpSubName2;
      int index = 0;
      List<IGisFileContainer> subList = new ArrayList<>();
      IGisFileContainer outFile;
      CSubtraction tmpSubConn;
      for (IGisFileContainer iFile : sourceFileList) {
        index++;
        tmpSubName2 = tmpSubName + index + ".tif";
        outFile = iFile;
        subList.add(sourceFile);
        subList.add(outFile);
        tmpSubConn = (CSubtraction) model
            .createAndAddConnection(subList, outputFileSet,
                EConnection.Subtraction.toString(),
                "differences",
                tmpSubName2);
        tmpSubConn.setMinuend((RasterFileContainer) outFile);
        tmpSubConn.setRelative(true);
        subsList.add(tmpSubConn.getOutputFileContainer());
        subList.clear();
      }
      if (((CalcValuePanel) this.panelRelValue).isMean()) {
        String newMeanName = outputFileNameSplit + "_subs_relMean.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, CMean.NAME, "differences", newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelRelValue).isStdDev()) {
        String newMeanName = outputFileNameSplit + "_subs_relStdDev.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.StandardDeviation.toString(), "differences",
            newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelRelValue).isCoeffVar()) {
        String newCoeffVarName = outputFileNameSplit + "_subs_relCoeffVar.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.CoefficientOfVariation.toString(), "differences",
            newCoeffVarName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      this.diffRelValueList.addAll(subsList);
      subsList.clear();
    }
    if (((CalcValuePanel) this.panelAbsValue).isSelected()) {
      this.diffAbsValueList.clear();
      String tmpSubName = outputFileNameSplit + "_sub_abs";
      String tmpSubName2;
      int index = 0;
      List<IGisFileContainer> subList = new ArrayList<>();
      IGisFileContainer outFile;
      CSubtraction tmpSubConn;
      for (IGisFileContainer iFile : sourceFileList) {
        index++;
        tmpSubName2 = tmpSubName + index + ".tif";
        outFile = iFile;
        subList.add(sourceFile);
        subList.add(outFile);
        tmpSubConn = (CSubtraction) model.createAndAddConnection(subList,
            outputFileSet, EConnection.Subtraction.toString(),
            "differences", tmpSubName2);
        tmpSubConn.setMinuend((RasterFileContainer) outFile);
        tmpSubConn.setAbsolute(true);
        subsList.add(tmpSubConn.getOutputFileContainer());
        subList.clear();
      }
      if (((CalcValuePanel) this.panelAbsValue).isMean()) {
        String newMeanName = outputFileNameSplit + "_subs_absMean.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, CMean.NAME, "differences", newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelAbsValue).isStdDev()) {
        String newMeanName = outputFileNameSplit + "_subs_absStdDev.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.StandardDeviation.toString(), "differences",
            newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelAbsValue).isCoeffVar()) {
        String newCoeffVarName = outputFileNameSplit + "_subs_absCoeffVar.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.CoefficientOfVariation.toString(), "differences",
            newCoeffVarName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      this.diffAbsValueList.addAll(subsList);
      subsList.clear();
    }
    if (((CalcValuePanel) this.panelRelAbs).isSelected()) {
      this.diffRelAbsValueList.clear();
      String tmpSubName = outputFileNameSplit + "_sub_rel_abs";
      String tmpSubName2;
      int index = 0;
      List<IGisFileContainer> subList = new ArrayList<>();
      IGisFileContainer outFile;
      CSubtraction tmpSubConn;
      for (IGisFileContainer iFile : sourceFileList) {
        index++;
        tmpSubName2 = tmpSubName + index + ".tif";
        outFile = iFile;
        subList.add(sourceFile);
        subList.add(outFile);
        tmpSubConn = (CSubtraction) model.createAndAddConnection(subList, outputFileSet,
            EConnection.Subtraction.toString(), "differences", tmpSubName2);
        tmpSubConn.setMinuend((RasterFileContainer) outFile);
        tmpSubConn.setAbsolute(true);
        tmpSubConn.setRelative(true);
        subsList.add(tmpSubConn.getOutputFileContainer());
        subList.clear();
      }
      if (((CalcValuePanel) this.panelRelAbs).isMean()) {
        String newMeanName = outputFileNameSplit + "_subs_rel_absMean.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, CMean.NAME, "differences", newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelRelAbs).isStdDev()) {
        String newMeanName = outputFileNameSplit + "_subs_rel_absStdDev.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.StandardDeviation.toString(), "differences",
            newMeanName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      if (((CalcValuePanel) this.panelRelAbs).isCoeffVar()) {
        String newCoeffVarName = outputFileNameSplit + "_subs_rel_absCoeffVar.tif";
        ConnectionFileChanger tmpConMean = model.createAndAddConnection(subsList, outputFileSet, EConnection.CoefficientOfVariation.toString(), "differences",
            newCoeffVarName);
        tmpConMean.getOutputFileContainer().setDescription("Todo...");
      }
      this.diffRelAbsValueList.addAll(subsList);
      subsList.clear();
    }

    return true;
  }

  public List<IGisFileContainer> getDiffAbsValueList() {
    return this.diffAbsValueList;
  }

  public List<IGisFileContainer> getDiffRelAbsValueList() {
    return this.diffRelAbsValueList;
  }

  public List<IGisFileContainer> getDiffRelValueList() {
    return this.diffRelValueList;
  }

  public List<IGisFileContainer> getDiffValueList() {
    return this.diffValueList;
  }

  private void initComponents() {

    this.panelValue = new CalcValuePanel("Value");
    ((CalcValuePanel) this.panelValue)
        .setShortDescription("Calculates positive and negative differences between the original and modified index files.<br>"
            + "The results indicate which areas tend to achieve higher or lower index values.");

    this.panelAbsValue = new CalcValuePanel("Absolute Value");
    ((CalcValuePanel) this.panelAbsValue)
        .setShortDescription("Calculates the absolute differences regardless of the positive or negative signs as a measure of the deviation from the original value.");

    this.panelRelValue = new CalcValuePanel("Relative Value");
    ((CalcValuePanel) this.panelRelValue)
        .setShortDescription("Calculates the relative difference between the original and modified index file as a proportional deviation from the original index.<br>"
            + "The results indicate which areas tend to achieve higher or lower index values as a proportional value to their original index.");

    this.panelRelAbs = new CalcValuePanel("Relative Absolute Value");
    ((CalcValuePanel) this.panelRelAbs)
        .setShortDescription("Calculates the relative difference between the original and modified index file as a proportional deviation from the original index regardless of the positive or negative signs.");

  }

  private void initLayout() {
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 528, Short.MAX_VALUE)
        .addGroup(
            layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(this.panelValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(this.panelAbsValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(this.panelRelValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addComponent(this.panelRelAbs, javax.swing.GroupLayout.Alignment.TRAILING,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap()));
    layout.setVerticalGroup(layout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 482, Short.MAX_VALUE)
        .addGroup(
            layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(this.panelValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.panelAbsValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.panelRelValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.panelRelAbs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
  }

  class CalcValuePanel extends JPanel {
    private static final long serialVersionUID = -3547407652677397072L;
    private javax.swing.JRadioButton radioMean;
    private javax.swing.JRadioButton radioStdDev;
    private javax.swing.JRadioButton radioCoeffVar;
    private javax.swing.JCheckBox checkBoxValue;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JEditorPane editorPaneValue;
    private String checkBoxString;

    private boolean isSelected = false;
    private boolean isMean = false;
    private boolean isStdDev = false;
    private boolean isCoeffVar = false;

    public CalcValuePanel(String checkBoxString) {
      this.checkBoxString = checkBoxString;
      this.initComponents();
      this.layoutComponents();
    }

    private void initComponents() {
      this.radioCoeffVar = new javax.swing.JRadioButton();
      this.radioStdDev = new javax.swing.JRadioButton();
      this.radioMean = new javax.swing.JRadioButton();
      this.checkBoxValue = new javax.swing.JCheckBox();
      this.jScrollPane17 = new javax.swing.JScrollPane();
      this.editorPaneValue = new javax.swing.JEditorPane();

      this.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
      this.radioCoeffVar.setFont(new java.awt.Font("Dialog 12 12 12", 0, 12)); // NOI18N
      this.radioCoeffVar.setText("Coefficient of Variation");
      this.radioCoeffVar.setEnabled(false);
      this.radioCoeffVar.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED)
            CalcValuePanel.this.isCoeffVar = true;
          else
            CalcValuePanel.this.isCoeffVar = false;
        }
      });

      this.radioStdDev.setFont(new java.awt.Font("Dialog 12 12 12", 0, 12)); // NOI18N
      this.radioStdDev.setText("Standard Deviation");
      this.radioStdDev.setEnabled(false);
      this.radioStdDev.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED)
            CalcValuePanel.this.isStdDev = true;
          else
            CalcValuePanel.this.isStdDev = false;
        }
      });

      this.radioMean.setFont(new java.awt.Font("Dialog 12 12 12", 0, 12)); // NOI18N
      this.radioMean.setText("Mean");
      this.radioMean.setEnabled(false);
      this.radioMean.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED)
            CalcValuePanel.this.isMean = true;
          else
            CalcValuePanel.this.isMean = false;
        }
      });
      // checkBoxValue.setText("Value");
      this.checkBoxValue.setText(this.checkBoxString);
      this.checkBoxValue.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED) {
            CalcValuePanel.this.isSelected = true;
            CalcValuePanel.this.radioCoeffVar.setEnabled(true);
            CalcValuePanel.this.radioStdDev.setEnabled(true);
            CalcValuePanel.this.radioMean.setEnabled(true);
          } else {
            CalcValuePanel.this.isSelected = false;
            CalcValuePanel.this.radioCoeffVar.setEnabled(false);
            CalcValuePanel.this.radioStdDev.setEnabled(false);
            CalcValuePanel.this.radioMean.setEnabled(false);
          }
        }
      });
      this.jScrollPane17.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      this.jScrollPane17.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

      this.editorPaneValue.setBackground(new java.awt.Color(238, 238, 238));
      this.editorPaneValue.setContentType("text/html");
      this.jScrollPane17.setViewportView(this.editorPaneValue);
    }

    public boolean isCoeffVar() {
      return this.isCoeffVar;
    }

    public boolean isMean() {
      return this.isMean;
    }

    public boolean isSelected() {
      return this.isSelected;
    }

    public boolean isStdDev() {
      return this.isStdDev;
    }

    private void layoutComponents() {
      javax.swing.GroupLayout panelValueLayout = new javax.swing.GroupLayout(this);
      this.setLayout(panelValueLayout);
      panelValueLayout.setHorizontalGroup(panelValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
          panelValueLayout
              .createSequentialGroup()
              .addContainerGap()
              .addGroup(
                  panelValueLayout
                      .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                      .addComponent(this.checkBoxValue)
                      .addGroup(
                          panelValueLayout
                              .createSequentialGroup()
                              .addGap(21, 21, 21)
                              .addGroup(
                                  panelValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                      .addComponent(this.radioStdDev).addComponent(this.radioMean)
                                      .addComponent(this.radioCoeffVar))))
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(this.jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)));
      panelValueLayout.setVerticalGroup(panelValueLayout
          .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(
              panelValueLayout.createSequentialGroup().addComponent(this.checkBoxValue)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.radioMean)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.radioStdDev)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.radioCoeffVar)
                  .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          .addComponent(this.jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE));
    }

    @Override
    public void setEnabled(boolean enabled) {
      super.setEnabled(enabled);
      this.editorPaneValue.setEnabled(enabled);
      this.jScrollPane17.setEnabled(enabled);
      this.checkBoxValue.setEnabled(enabled);
    }

    public void setShortDescription(String text) {
      this.editorPaneValue.setText(text);
    }

  }
}

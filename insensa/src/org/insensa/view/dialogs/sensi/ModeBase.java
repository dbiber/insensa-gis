/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.connections.CIndexation;
import org.insensa.exceptions.GisFileException;
import org.jdom.JDOMException;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;


public class ModeBase extends javax.swing.JPanel {
  private static final long serialVersionUID = -1685740512771922837L;
  List<IMode> modeList = new ArrayList<IMode>();
  JSplitPane splitPane;
  private javax.swing.JComboBox jComboBox2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JEditorPane editorPane;
  private javax.swing.JScrollPane scrollPane;

  /**
   * Creates new form ModeBase.
   */
  public ModeBase(List<IMode> modeList, JSplitPane splitPane) {
    this.splitPane = splitPane;
    this.modeList = modeList;
    this.initComponents();
    this.layoutComponents();
  }

  /**
   *
   */
  public boolean applySettings(JLabel labelStatus, Model model, GenericGisFileSet outputFileSet, CIndexation prioritization, String outputFileName)
      throws IOException, JDOMException, GisFileException {
    IMode mode = (IMode) this.jComboBox2.getSelectedItem();
    return mode.applySettings(labelStatus, model, outputFileSet, prioritization, outputFileName);
  }

  /**
   *
   */
  public List<IGisFileContainer> getSensiList() {
    IMode mode = (IMode) this.jComboBox2.getSelectedItem();
    return mode.getSensiList();
  }


  private void initComponents() {
    this.jLabel3 = new javax.swing.JLabel();
    this.jComboBox2 = new javax.swing.JComboBox();
    this.jLabel4 = new javax.swing.JLabel();
    this.editorPane = new javax.swing.JEditorPane();
    this.scrollPane = new JScrollPane();
    this.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

    Font mainFont = new Font("Dialog", Font.PLAIN, 12);

    this.jLabel3.setFont(mainFont);
    this.jLabel3.setText("Select Mode");

    this.jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new Object[]
        {this.modeList.get(0), this.modeList.get(1), this.modeList.get(2), this.modeList.get(3)}));
    this.jComboBox2.setName("jComboBox2");
    this.jComboBox2.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ModeBase.this.splitPane.setBottomComponent((Component) ModeBase.this.jComboBox2.getSelectedItem());
        ModeBase.this.editorPane.setText(((IMode) ModeBase.this.jComboBox2.getSelectedItem()).getShortDescription());
      }
    });

    this.jLabel4.setFont(mainFont);
    this.jLabel4.setText("Short Description");

    this.editorPane.setBackground(new Color(238, 238, 238));
    this.editorPane.setEditable(false);
    this.editorPane.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
    this.editorPane.setContentType("text/html");
    this.scrollPane.setViewportView(this.editorPane);
    this.editorPane.setText(((IMode) this.jComboBox2.getSelectedItem()).getShortDescription());
  }


  private void layoutComponents() {
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.jLabel3)
                    .addComponent(this.jComboBox2, 0, 155, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(this.scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(this.jLabel4)).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.jLabel4))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.scrollPane))
            // .addComponent(scrollPane,
            // javax.swing.GroupLayout.PREFERRED_SIZE, 78,
            // javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
  }
}
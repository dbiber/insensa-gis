/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

public class ChangingUnitCounts extends javax.swing.JPanel
{


	private static final long serialVersionUID = 5215831689420228715L;
	private javax.swing.JCheckBox checkBoxRelVal;
	private javax.swing.JCheckBox checkBoxVal;
	private javax.swing.JScrollPane jScrollPane22;
	private javax.swing.JLabel labelVal1;
	private javax.swing.JLabel labelVal2;
	private javax.swing.JPanel panelCountChangingUnits;
	private javax.swing.JSpinner spinnerVal1;
	private javax.swing.JSpinner spinnerVal2;
	private javax.swing.JEditorPane editorPane;

	/**
	 * Creates new form ChaningUnitsCounts.
	 */
	public ChangingUnitCounts()
	{
		this.initComponents();
		this.layoutComponents();
	}


	private void initComponents()
	{

		this.panelCountChangingUnits = new javax.swing.JPanel();
		this.checkBoxVal = new javax.swing.JCheckBox();
		this.checkBoxRelVal = new javax.swing.JCheckBox();
		this.labelVal1 = new javax.swing.JLabel();
		this.spinnerVal1 = new javax.swing.JSpinner();
		this.labelVal2 = new javax.swing.JLabel();
		this.spinnerVal2 = new javax.swing.JSpinner();
		this.jScrollPane22 = new javax.swing.JScrollPane();
		this.editorPane = new javax.swing.JEditorPane();

		this.panelCountChangingUnits.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Count Changing Units",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog 12 12", 0,
						12))); // NOI18N

		this.checkBoxVal.setFont(new java.awt.Font("Dialog 12 12", 0, 12)); // NOI18N
		this.checkBoxVal.setText("value");

		this.checkBoxRelVal.setFont(new java.awt.Font("Dialog 12 12", 0, 12)); // NOI18N
		this.checkBoxRelVal.setText("relative value");

		this.labelVal1.setText("Value 1 (%)");

		this.labelVal2.setText("Value 2 (%)");

		this.jScrollPane22.setBorder(null);
		this.jScrollPane22.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.jScrollPane22.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.editorPane.setBackground(new java.awt.Color(238, 238, 238));
		this.editorPane.setContentType("text/html");
		this.editorPane.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
		this.editorPane.setEditable(false);
		this.editorPane
				.setText("Changing Unit Counts: Counts how many pixels have changed their value between the original and modified index file by less than the defined proportion and calculates the proportion of the total area that has changed not more than specified.");
		this.jScrollPane22.setViewportView(this.editorPane);
	}


	private void layoutComponents()
	{
		javax.swing.GroupLayout panelCountChangingUnitsLayout = new javax.swing.GroupLayout(this.panelCountChangingUnits);
		this.panelCountChangingUnits.setLayout(panelCountChangingUnitsLayout);
		panelCountChangingUnitsLayout.setHorizontalGroup(panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelCountChangingUnitsLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelCountChangingUnitsLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												panelCountChangingUnitsLayout.createSequentialGroup().addComponent(this.checkBoxVal).addGap(18, 18, 18)
														.addComponent(this.checkBoxRelVal))
										.addGroup(
												panelCountChangingUnitsLayout
														.createSequentialGroup()
														.addGroup(
																panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.labelVal1).addComponent(this.labelVal2))
														.addGap(18, 18, 18)
														.addGroup(
																panelCountChangingUnitsLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																		.addComponent(this.spinnerVal2)
																		.addComponent(this.spinnerVal1, javax.swing.GroupLayout.DEFAULT_SIZE, 131,
																				Short.MAX_VALUE)))).addGap(18, 18, 18)
						.addComponent(this.jScrollPane22, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE).addContainerGap()));
		panelCountChangingUnitsLayout
				.setVerticalGroup(panelCountChangingUnitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
						panelCountChangingUnitsLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										panelCountChangingUnitsLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(this.jScrollPane22, javax.swing.GroupLayout.Alignment.LEADING,
														javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
												.addGroup(
														javax.swing.GroupLayout.Alignment.LEADING,
														panelCountChangingUnitsLayout
																.createSequentialGroup()
																.addGroup(
																		panelCountChangingUnitsLayout
																				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(this.labelVal1)
																				.addComponent(this.spinnerVal1, javax.swing.GroupLayout.PREFERRED_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addGap(18, 18, 18)
																.addGroup(
																		panelCountChangingUnitsLayout
																				.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(this.labelVal2)
																				.addComponent(this.spinnerVal2, javax.swing.GroupLayout.PREFERRED_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addGap(18, 18, 18)
																.addGroup(
																		panelCountChangingUnitsLayout
																				.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(this.checkBoxVal).addComponent(this.checkBoxRelVal))))
								.addContainerGap()));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panelCountChangingUnits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panelCountChangingUnits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}
}

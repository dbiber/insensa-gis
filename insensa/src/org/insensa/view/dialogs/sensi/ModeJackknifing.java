/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.sensi;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.RasterFileContainer;
import org.insensa.connections.ConnectionManager;
import org.insensa.exceptions.GisFileException;
import org.insensa.connections.CIndexation;
import org.jdom.JDOMException;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JLabel;
import javax.swing.JPanel;


public class ModeJackknifing extends JPanel implements IMode {

  List<IGisFileContainer> tmpContainerList = new ArrayList<>();
  private javax.swing.JCheckBox checkBoxEqual;
  private javax.swing.JCheckBox checkBoxExFiles;
  private javax.swing.JCheckBox checkBoxExThr;
  private boolean isEqualWeight = false;
  private boolean isExFiles = false;
  private boolean isExThr = false;


  public ModeJackknifing() {
    this.initComponents();
  }

  @Override
  public boolean applySettings(JLabel labelStatus, Model model,
                               GenericGisFileSet outputFileSet,
                               CIndexation prioritization, String outputFileName)
      throws IOException, JDOMException, GisFileException {
    List<RasterFileContainer> fileList = prioritization.getSourceFileList();
    ConnectionManager connectionManager = new ConnectionManager();
    CIndexation tmpCon;
    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
    String outputFileNameNew;
    int fileNameIterator = 0;
    this.tmpContainerList.clear();
    // fileNameIterator++;

    if (!isEqualWeight && !isExFiles && !isExThr) {
      return false;
    }
    if (this.isEqualWeight) {
      fileNameIterator++;
      int listSize = fileList.size();
      float equalValue = 1.0F / listSize;
      equalValue *= 1000;
      equalValue = Math.round(equalValue);
      equalValue /= 1000;

      outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";

//      tmpCon = (CIndexation) connectionManager.getConnectionFileChanger("Indexation");
      tmpCon = (CIndexation) connectionManager.getConnectionFileChangerCopy(prioritization);
      tmpCon.setOutputFileSet(outputFileSet);
      tmpCon.setSourceFileList(fileList);

//      tmpCon.setConnectionElement(prioritization.getConnectionElement());

//      List<Float> weightList = tmpCon.getWeightList();
      List<Float> weightList = tmpCon.getFileWeightingMap()
          .values()
          .stream()
          .map(CIndexation.Weighting::getWeight)
          .collect(Collectors.toList());

      tmpCon.setUsed(false);

      model.addConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);
//      model.createAndAddConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);


      for (int i = 0; i < weightList.size(); i++) {
        weightList.set(i, equalValue);
      }
      tmpCon.getOutputFileContainer()
          .getGisFileInformationStorage().refreshPluginExec(tmpCon);
      tmpCon.getOutputFileContainer()
          .setDescription("File with index values according to your settings\n"
              + " but with equal weighting for all files.");
      this.tmpContainerList.add(tmpCon.getOutputFileContainer());
    }

    if (this.isExFiles) {
      for (int i = 0; i < fileList.size(); i++) {
        fileNameIterator++;
        IGisFileContainer currentContainer = fileList.get(i);
        String weightKey = currentContainer.getPathRelativeToProject() + currentContainer.getOutputFileName();
        outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
//        tmpCon = (CIndexation) connectionManager.getConnectionFileChanger("Indexation");
        tmpCon = (CIndexation) connectionManager.getConnectionFileChangerCopy(prioritization);
        tmpCon.setOutputFileSet(outputFileSet);
        tmpCon.setSourceFileList(fileList);
//        tmpCon.setConnectionElement(prioritization.getConnectionElement());
        tmpCon.setUsed(false);

        tmpCon.getFileWeightingMap().get(weightKey).setWeight(0.0F);
//        model.createAndAddConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);
        model.addConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);
        tmpCon.getOutputFileContainer().getGisFileInformationStorage().refreshPluginExec(tmpCon);
        tmpCon.getOutputFileContainer().setDescription(
            "File with index values according"
                + " to your settings\n but without variable "
                + tmpCon.getSourceFileList().get(i).getName());
        this.tmpContainerList.add(tmpCon.getOutputFileContainer());
      }
    }

    if (this.isExThr) {
      fileNameIterator++;
      outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
//      tmpCon = (CIndexation) connectionManager.getConnectionFileChanger(EConnection.indexation);
      tmpCon = (CIndexation) connectionManager.getConnectionFileChangerCopy(prioritization);
      tmpCon.setOutputFileSet(outputFileSet);
      tmpCon.setSourceFileList(fileList);
//      tmpCon.setConnectionElement(prioritization.getConnectionElement());
      Collection<CIndexation.Weighting> weightings = tmpCon.getFileWeightingMap().values();
      boolean thresholdIsActive = false;
      for (CIndexation.Weighting weighting : weightings) {
        if (weighting.getReward().getActivate()
            || weighting.getPenalty().getActivate()) {
          thresholdIsActive = true;
          break;
        }
      }

      if (thresholdIsActive) {
        tmpCon.setUsed(false);
        model.addConnection(fileList, outputFileSet, tmpCon, null, outputFileNameNew);
        tmpCon.getOutputFileContainer().getGisFileInformationStorage().refreshPluginExec(tmpCon);
        tmpCon.getOutputFileContainer().setDescription("File with index values according to your settings\n but without threshold settings.");

//        tmpCon.getThreshold1List().clear();
//        tmpCon.getThreshold2List().clear();
        this.tmpContainerList.add(tmpCon.getOutputFileContainer());
      }
    }
    return true;
  }

  @Override
  public List<IGisFileContainer> getSensiList() {
    return this.tmpContainerList;
  }

  @Override
  public String getShortDescription() {
    String description = "Each setting and factor can be excluded iteratively from index calculation resulting in modified index files,"
        + "<br> one for each indicator that is excluded, one applying equal weighting to all indicators and one excluding all threshold settings.";
    return description;
  }


  private void initComponents() {

    this.checkBoxExThr = new javax.swing.JCheckBox();
    this.checkBoxExFiles = new javax.swing.JCheckBox();
    this.checkBoxEqual = new javax.swing.JCheckBox();

    this.checkBoxExThr.setFont(new java.awt.Font("Dialog", 0, 12));
    this.checkBoxExThr.setText("Excluding Threshold Settings");
    this.checkBoxExThr.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          ModeJackknifing.this.isExThr = true;
        } else {
          ModeJackknifing.this.isExThr = false;
        }
      }
    });

    this.checkBoxExFiles.setFont(new java.awt.Font("Dialog", 0, 12));
    this.checkBoxExFiles.setText("Excluding Files Iteratively");
    this.checkBoxExFiles.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          ModeJackknifing.this.isExFiles = true;
        } else {
          ModeJackknifing.this.isExFiles = false;
        }
      }
    });

    this.checkBoxEqual.setFont(new java.awt.Font("Dialog", 0, 12));
    this.checkBoxEqual.setText("Apply Equal Weighting");
    this.checkBoxEqual.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          ModeJackknifing.this.isEqualWeight = true;
        } else {
          ModeJackknifing.this.isEqualWeight = false;
        }
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.checkBoxExThr)
                    .addComponent(this.checkBoxExFiles).addComponent(this.checkBoxEqual)).addContainerGap(202, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup().addContainerGap().addComponent(this.checkBoxExThr)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.checkBoxExFiles)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.checkBoxEqual)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
  }

  @Override
  public String toString() {
    return "Jackknifing";
  }
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import de.bibertech.javahelp.viewer.view.HelpMainGui;


import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

//TODO Restructure SettingsDialog/AbstractImporterSettingDialog

/**
 * SettingsDialog has to be treated as just an Insensa-GIS specific version of the JDialog
 * with some additional features like specific buttons, header, help button a.s.o.
 *
 * It is NOT a part of a MVP/MVC pattern has hat so be logically seperated from such designs
 * It can be seen as part of a Swing Extension for Insensa-Gis, nothing more
 * Use it just like you would use a JDialog, but with additional (optional) features!
 */
public class SettingsDialog extends JDialog {
  private static final Logger log = LoggerFactory.getLogger(SettingsDialog.class);

  private static final long serialVersionUID = -4771383948562549058L;
  protected HTMLEditorKit kit;
  protected StyleSheet styleSheet;
  protected HTMLDocument doc;
  protected Element body;
  private javax.swing.JButton buttonCancel;
  private javax.swing.JButton buttonHelp;
  private javax.swing.JButton buttonOk;
  private javax.swing.JEditorPane textPane;
  private javax.swing.JScrollPane scrollPane;
  private javax.swing.JLabel labelStatus;
  private ActionListener cancelActionListener;

  private String helpId = "";


  public SettingsDialog() {
  }

  public SettingsDialog(Dialog dialog, boolean modal) {
    super(dialog, modal);
  }

  public SettingsDialog(Frame frame, boolean modal) {
    super(frame, modal);
  }

  public javax.swing.JButton getButtonCancel() {
    return this.buttonCancel;
  }

  public javax.swing.JButton getButtonHelp() {
    return this.buttonHelp;
  }

  public javax.swing.JButton getButtonOk() {
    return this.buttonOk;
  }

  public ActionListener getCancelActionListener() {
    return this.cancelActionListener;
  }

  public String getHelpId() {
    return this.helpId;
  }

  public void setHelpId(String helpId) {
    this.helpId = helpId;
  }

  public javax.swing.JLabel getLabelStatus() {
    return this.labelStatus;
  }

  protected void initComponents(JPanel panel) {
    this.scrollPane = new javax.swing.JScrollPane();
    this.textPane = new javax.swing.JEditorPane();
    this.labelStatus = new javax.swing.JLabel();
    this.buttonCancel = new javax.swing.JButton();
    this.buttonOk = new javax.swing.JButton();
    this.buttonHelp = new javax.swing.JButton();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    this.scrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    this.scrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
    this.scrollPane.setViewportView(this.textPane);

    this.textPane.setEditable(false);
    this.textPane.setContentType("text/html");
    try {
      this.initHead();
    } catch (IOException e) {
      e.printStackTrace();
    }

    this.labelStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    this.buttonCancel.setFont(new java.awt.Font("Dialog", 0, 12));
    this.buttonCancel.setText("Cancel");
//    this.cancelActionListener = new CancelActionListener();
//    this.buttonCancel.addActionListener(this.cancelActionListener);


    this.buttonOk.setFont(new java.awt.Font("Dialog", 0, 12));
    this.buttonOk.setText("Ok");
    this.buttonHelp.setText("?");
    this.buttonHelp.addActionListener(new HelpActionListener(this));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
    this.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(this.scrollPane)
        .addComponent(this.labelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 423,
            Short.MAX_VALUE)
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            layout.createSequentialGroup().addContainerGap(237, Short.MAX_VALUE)
                .addComponent(this.buttonOk)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.buttonCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.buttonHelp).addContainerGap())
        .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE,
            javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            layout.createSequentialGroup()
                .addComponent(this.scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 78,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(this.buttonHelp)
                        .addComponent(this.buttonCancel).addComponent(this.buttonOk))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.labelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 19,
                    javax.swing.GroupLayout.PREFERRED_SIZE)));

    this.pack();
    this.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
  }

  private void initHead() throws IOException {
    this.kit = new HTMLEditorKit();
    this.textPane.setEditorKit(this.kit);
    this.textPane.setBorder(null);

    this.styleSheet = this.kit.getStyleSheet();
    File file = new File(ResourceBundle.getBundle("img").getString("dialog.settings.head"));
    this.styleSheet.addRule("body {color:#000; font-family:times;" + "margin:5px;" +
        "border:0px;" + " background-image:url(" + file.toURI()
        + ");" + "background-repeat:no-repeat;" + "background-position:right top;" + "}");
    this.styleSheet.addRule("h4 {color: black;" + "align=left;}");
    this.kit.setStyleSheet(this.styleSheet);

    this.doc = (HTMLDocument) this.kit.createDefaultDocument();
    this.textPane.setDocument(this.doc);
    Element[] roots = this.doc.getRootElements();// #0 is the HTML element,
    // #1
    for (int i = 0; i < roots[0].getElementCount(); i++) {
      Element element = roots[0].getElement(i);
      if (element.getAttributes().getAttribute(StyleConstants.NameAttribute) == HTML.Tag.BODY) {
        this.body = element;
        break;
      }
    }
    if (this.body == null)
      throw new IOException("can't find <body>");

    this.styleSheet.removeStyle("body");
    this.kit.setStyleSheet(this.styleSheet);
  }

  public void saveAsHtml(String filename) throws IOException, BadLocationException {
    File outFile = new File(filename);
    if (!outFile.exists())
      outFile.createNewFile();
    FileOutputStream outStream = new FileOutputStream(outFile);
    this.kit.write(outStream, this.doc, 0, this.doc.getLength());
    outStream.close();
  }

  public void setHeadTitle(String title) {
    try {
      this.kit.setStyleSheet(this.styleSheet);
      String newTitle = "<h4>" + title + "</h4>";
      this.doc.insertAfterStart(this.body, newTitle);
      this.styleSheet.removeStyle("body");
      this.kit.setStyleSheet(this.styleSheet);
    } catch (BadLocationException e) {
    } catch (IOException e) {
    }
  }

  public void setStatus(String message) {
    this.labelStatus.setText(message);
  }


//  private class CancelActionListener implements ActionListener {
//    @Override
//    public void actionPerformed(ActionEvent e) {
//      SettingsDialog.this.setVisible(false);
//    }
//  }

  private class HelpActionListener implements ActionListener {
    Dialog dialog;

    public HelpActionListener(Dialog dialog) {
      this.dialog = dialog;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (!SettingsDialog.this.getHelpId().isEmpty()) {
        try {
          HelpMainGui helpGui = new HelpMainGui(this.dialog, false);
          helpGui.setTarget(SettingsDialog.this.getHelpId());
          helpGui.setVisible(true);
        } catch (JDOMException | IOException e1) {
          log.error( "UNKNOWN", e1);
        }
      }
    }
  }
}

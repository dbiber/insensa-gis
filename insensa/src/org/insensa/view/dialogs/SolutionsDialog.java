/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import org.insensa.commands.ISolution;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.UIManager;


public class SolutionsDialog extends SettingsDialog {
  private static final long serialVersionUID = 6061767188487947486L;
  private javax.swing.JComboBox jComboBox1;
  private javax.swing.JLabel jLabel1;
  private ISolution selectedSolution;
  private List<ISolution> solutions = new ArrayList<ISolution>();

  public SolutionsDialog(List<ISolution> solutions) {
    this.solutions.addAll(solutions);
    super.setTitle("Select Solution");
    super.initComponents(this.initComponents());
    super.setModalityType(ModalityType.APPLICATION_MODAL);
    super.setHeadTitle("Select Solution");
    super.getButtonOk().addActionListener(new ButtonOkActionListener());
    this.setLocationRelativeTo(null);
  }

  public ISolution getSelectedSolution() {
    return this.selectedSolution;
  }

  private JPanel initComponents() {
    JPanel mainPanel = new JPanel();
    this.jLabel1 = new javax.swing.JLabel();
    this.jComboBox1 = new javax.swing.JComboBox();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    this.jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.jLabel1.setIcon(UIManager.getIcon("OptionPane.warningIcon")); // NOI18N
    this.jLabel1.setText("Choose a Solution");
    this.jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

    this.jComboBox1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

    this.jComboBox1.setRenderer(new SolutionListRenderer());
    this.jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(this.solutions.toArray(new ISolution[this.solutions.size()])));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(mainPanel);
    mainPanel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(this.jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
                    .addComponent(this.jComboBox1, 0, 420, Short.MAX_VALUE)).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(this.jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

    this.pack();
    return mainPanel;
  }

  private class ButtonOkActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      SolutionsDialog.this.selectedSolution = (ISolution) SolutionsDialog.this.jComboBox1.getSelectedItem();
      SolutionsDialog.this.setVisible(false);
    }
  }

  private class SolutionListRenderer extends DefaultListCellRenderer {
    private static final long serialVersionUID = -214278272582468916L;

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (comp instanceof JLabel) {
        if (value != null && value instanceof ISolution) {
          ((JLabel) comp).setText(((ISolution) value).getMessage());
        }
      }

      return comp;
    }

  }
}

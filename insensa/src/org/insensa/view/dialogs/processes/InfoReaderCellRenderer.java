/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import org.insensa.view.dialogs.ProgressBar;

import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;


public class InfoReaderCellRenderer extends DefaultTreeCellRenderer {


  private static final long serialVersionUID = 8964870847161522250L;

  /**
   * The ProgressBar List, Created earlier;.
   */
  public InfoReaderCellRenderer(List<ProgressBar> progressBarList) {
  }

  @Override
  public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    if (((DefaultMutableTreeNode) value).getUserObject() instanceof ProgressBar) {
      ProgressBar tmpProgressBar = (ProgressBar) ((DefaultMutableTreeNode) value).getUserObject();
      Dimension parentSize = tree.getParent().getSize();
      tmpProgressBar.setPreferredSize(new Dimension(parentSize.width - 44, 30));
      return (Component) ((DefaultMutableTreeNode) value).getUserObject();
    } else {
      return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
    }
  }

}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.insensa.view.dialogs.ProgressBar;



public class OptionFileChangerCellRenderer extends DefaultTreeCellRenderer
{

	private static final long serialVersionUID = -5259820990221102966L;

	/** 
	 * 
	 * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
	{
		if (((DefaultMutableTreeNode) value).getUserObject() instanceof OptionFileChangerProgressBar)
		{
			ProgressBar tmpProgressBar = (ProgressBar) ((DefaultMutableTreeNode) value).getUserObject();
			Dimension parentSize = tree.getParent().getSize();
			tmpProgressBar.setPreferredSize(new Dimension(parentSize.width - 33, 22));
			return (Component) ((DefaultMutableTreeNode) value).getUserObject();
		} else if (((DefaultMutableTreeNode) value).getUserObject() instanceof OptionDependencyProgressBar)
		{
			ProgressBar tmpProgressBar = (ProgressBar) ((DefaultMutableTreeNode) value).getUserObject();
			Dimension parentSize = tree.getParent().getSize();
			tmpProgressBar.setPreferredSize(new Dimension(parentSize.width - 53, 20));
			return (Component) ((DefaultMutableTreeNode) value).getUserObject();
		}
		return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
	}
}

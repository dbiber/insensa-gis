/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.view.View;



public class OptionOptionTreeNode extends DefaultMutableTreeNode implements WorkerStatusList
{

	private static final long serialVersionUID = -1838941908019155019L;
	private OptionFileChangerProgressBar progressBar;
	private List<OptionDependencyProgressBar> progressBarList;
	private List<DefaultMutableTreeNode> childNodeList;
	private View view;
	private OptionFileChangerProgressTree progressTree;
	private int finishedThread = 0;

	/**
	 * @param org.insensa.view
	 * @param wList
	 * @param progressTree
	 */
	public OptionOptionTreeNode(View view, WorkerStatusList wList, OptionFileChangerProgressTree progressTree)
	{
		this.progressBar = new OptionFileChangerProgressBar(view);
		this.setUserObject(this.progressBar);
		this.progressBar.setParentWorkerStatusList(wList);
		this.progressBarList = new ArrayList<OptionDependencyProgressBar>();
		this.childNodeList = new ArrayList<DefaultMutableTreeNode>();
		this.view = view;
		this.progressTree = progressTree;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatusList#endAllProcesses()
	 */
	@Override
	public void endAllProcesses()
	{
		this.finishedThread++;
		if (this.finishedThread == this.childNodeList.size())
			this.progressTree.refreshTreeCollapse(this);
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatusList#getChildWorkerStatusList(org.insensa.WorkerStatus)
	 */
	@Override
	public WorkerStatusList getChildWorkerStatusList(WorkerStatus wStat)
	{
		return null;
	}

	/**
	 * @return
	 */
	public OptionFileChangerProgressBar getProgressBar()
	{
		return this.progressBar;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatusList#getWorkerStatus()
	 */
	@Override
	public WorkerStatus getWorkerStatus()
	{
		return null;
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatusList#getWorkerStatus(int)
	 */
	@Override
	public WorkerStatus getWorkerStatus(int index)
	{
		return this.progressBarList.get(index);
	}


	public void refreshChildNodes()
	{
		for (DefaultMutableTreeNode iNode : this.childNodeList)
		{
			this.progressTree.refreshTree(iNode);
		}
	}

	/**
	 * @param wstat
	 */
	public void refreshTreeNode(WorkerStatus wstat)
	{
		this.progressTree.refreshTree(this.childNodeList.get(this.progressBarList.indexOf(wstat)));
	}

	/**
	 * @param e
	 */
	public void setError(Throwable e)
	{
		this.progressTree.setError(e);
	}

	/** 
	 * 
	 * @see org.insensa.WorkerStatusList#startAllProcesses(java.lang.String, int)
	 */
	@Override
	public void startAllProcesses(String name, int numOfProgress) throws IOException
	{
		this.progressBarList.clear();
		for (int i = 0; i < numOfProgress; i++)
		{
			OptionDependencyProgressBar tmpProgressBar = new OptionDependencyProgressBar(this.view);
			tmpProgressBar.setParentWorkerStatusList(this);
			this.progressBarList.add(tmpProgressBar);
			DefaultMutableTreeNode tmpNode = new DefaultMutableTreeNode(tmpProgressBar);
			this.childNodeList.add(tmpNode);
			this.add(tmpNode);
		}
		this.progressTree.refreshTreeExpand(this);

	}
}

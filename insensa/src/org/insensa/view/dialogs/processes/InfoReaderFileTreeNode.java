/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.view.View;


public class InfoReaderFileTreeNode extends DefaultMutableTreeNode implements WorkerStatusList {


  private static final long serialVersionUID = 127154940126991089L;
  private List<InfoReaderProgressBar> progressBarList;
  private List<DefaultMutableTreeNode> treeNodeList;
  private int finishedThread = 0;
  private InfoReaderProgressTree progressTree;
  private View view;
  private List<Throwable> throwableList;

  public InfoReaderFileTreeNode(InfoReaderProgressTree progressTree, View view) {
    this.progressTree = progressTree;
    this.throwableList = new ArrayList<Throwable>();
    this.progressBarList = new ArrayList<InfoReaderProgressBar>();
    this.treeNodeList = new ArrayList<DefaultMutableTreeNode>();
    this.view = view;
  }

  @Override
  public void endAllProcesses() {
    this.finishedThread++;
    this.progressTree.endProcess();
    if (this.finishedThread == this.treeNodeList.size())
      this.progressTree.endAllProcesses();
  }

  @Override
  public WorkerStatusList getChildWorkerStatusList(WorkerStatus wStat) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public WorkerStatus getWorkerStatus() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public WorkerStatus getWorkerStatus(int index) {
    return this.progressBarList.get(index);
  }


  public void refreshChildNodes() {
    for (DefaultMutableTreeNode iNode : this.treeNodeList) {
      this.progressTree.refreshTree(iNode);
    }
  }

  public void refreshTreeNode(WorkerStatus wstat) {
    this.progressTree.refreshTree(this.treeNodeList.get(this.progressBarList.indexOf(wstat)));
  }

  public void setError(Throwable e) {
    this.progressTree.setError(e);
  }

  @Override
  public void startAllProcesses(String name, int numOfProgress) throws IOException {
    this.setUserObject(name);
    this.progressBarList.clear();
    this.progressTree.addProcesses(numOfProgress);
    for (int i = 0; i < numOfProgress; i++) {
      InfoReaderProgressBar tmpProgressBar = new InfoReaderProgressBar(this.view);
      tmpProgressBar.setParentWorkerStatusList(this);
      this.progressBarList.add(tmpProgressBar);
      DefaultMutableTreeNode tmpNode = new DefaultMutableTreeNode(tmpProgressBar);
      this.treeNodeList.add(tmpNode);
      this.add(tmpNode);
    }
    this.progressTree.startAllProcesses(name);
    this.progressTree.refreshTreeExpand(this);
  }
}

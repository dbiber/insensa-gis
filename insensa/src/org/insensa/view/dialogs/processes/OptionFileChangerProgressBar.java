/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.awt.Color;

import javax.swing.border.MatteBorder;

import org.insensa.view.View;
import org.insensa.view.dialogs.ProgressBar;



public class OptionFileChangerProgressBar extends ProgressBar
{


	private static final long serialVersionUID = -5194628220787163182L;

	/**
	 * @param org.insensa.view
	 */
	public OptionFileChangerProgressBar(View view)
	{
		super(view);
		MatteBorder border = new MatteBorder(2, 0, 2, 0, Color.white);
		super.setBorder(border);
	}

	/** 
	 * 
	 * @see org.insensa.view.dialogs.ProgressBar#errorProcess(java.lang.Throwable)
	 */
	@Override
	public void errorProcess(Throwable e)
	{
		super.errorProcess(e);
		if (this.mProgress instanceof OptionFileChangerFileTreeNode)
			((OptionFileChangerFileTreeNode) this.mProgress).setError(e);
		else if (this.mProgress instanceof OptionOptionTreeNode)
			((OptionOptionTreeNode) this.mProgress).setError(e);
	}

	/** 
	 * 
	 * @see org.insensa.view.dialogs.ProgressBar#refreshPercentage(float)
	 */
	@Override
	public void refreshPercentage(float percent)
	{
		super.refreshPercentage(percent);
		if (this.mProgress instanceof OptionFileChangerFileTreeNode)
			((OptionFileChangerFileTreeNode) this.mProgress).refreshTreeNode(this);
		else if (this.mProgress instanceof OptionOptionTreeNode)
			((OptionOptionTreeNode) this.mProgress).refreshTreeNode(this);
	}

	/** 
	 * 
	 * @see org.insensa.view.dialogs.ProgressBar#setProgressName(java.lang.String)
	 */
	@Override
	public void setProgressName(String name)
	{
		super.setProgressName(name);
		if (this.mProgress instanceof OptionFileChangerFileTreeNode)
			((OptionFileChangerFileTreeNode) this.mProgress).refreshTreeNode(this);
		else if (this.mProgress instanceof OptionOptionTreeNode)
			((OptionOptionTreeNode) this.mProgress).refreshTreeNode(this);
	}

}

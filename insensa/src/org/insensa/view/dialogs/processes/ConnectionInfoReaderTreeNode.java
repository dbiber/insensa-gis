/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.awt.Color;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JProgressBar;
import javax.swing.border.MatteBorder;
import javax.swing.tree.DefaultMutableTreeNode;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;


public class ConnectionInfoReaderTreeNode extends DefaultMutableTreeNode implements WorkerStatus {

  private static final long serialVersionUID = 8874698962396661170L;
  private ConnectionProgressTree progressTree;
  private JProgressBar progressBar;
  private WorkerStatusList parentWorkerStatusList;
  private Date date = new Date();
  private SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss:SS");
  private long time;

  /**
   * @param progressTree
   */
  public ConnectionInfoReaderTreeNode(ConnectionProgressTree progressTree) {
    this.progressTree = progressTree;
    this.progressBar = new JProgressBar();
    // progressBar.setPreferredSize(new Dimension(350,20));
    this.progressBar.setFont(new Font(Font.DIALOG, Font.BOLD, 10));
    MatteBorder border = new MatteBorder(2, 0, 2, 0, Color.white);
    this.progressBar.setBorder(border);
    super.setUserObject(this.progressBar);
  }

  /**
   * @see org.insensa.WorkerStatus#endPause()
   */
  @Override
  public void endPause() {
  }

  /**
   * @see org.insensa.WorkerStatus#endProgress()
   */
  @Override
  public void endProgress() {
    this.time = System.currentTimeMillis() - this.time;
    this.date.setTime(this.time);
    this.progressBar.setString(this.progressBar.getString() + " : " + this.dateFormat.format(this.date));
    if (this.parentWorkerStatusList != null)
      this.parentWorkerStatusList.endAllProcesses();
    this.progressTree.refreshTree(this);
  }

  /**
   * @see org.insensa.WorkerStatus#errorProcess()
   */
  @Override
  public void errorProcess() {
    this.refreshPercentage(100.0F);
    this.progressBar.setForeground(Color.red);
    if (this.parentWorkerStatusList != null)
      this.parentWorkerStatusList.endAllProcesses();
    this.progressTree.refreshTree(this);
  }

  /**
   * @see org.insensa.WorkerStatus#errorProcess(java.lang.String)
   */
  @Override
  public void errorProcess(String message) {
    this.refreshPercentage(100.0F);
    this.progressBar.setForeground(Color.red);
    this.progressBar.setString(message);
    this.progressBar.setStringPainted(true);
    if (this.parentWorkerStatusList != null)
      this.parentWorkerStatusList.endAllProcesses();
    this.progressTree.refreshTree(this);
  }

  /**
   * @see org.insensa.WorkerStatus#errorProcess(java.lang.Throwable)
   */
  @Override
  public void errorProcess(Throwable e) {
    e.printStackTrace();
    this.refreshPercentage(100.0F);
    this.progressBar.setForeground(Color.red);
    if (this.parentWorkerStatusList != null)
      this.parentWorkerStatusList.endAllProcesses();
    this.progressTree.refreshTree(this);
  }

  /**
   * @see org.insensa.WorkerStatus#getChildWorkerStatusList(int)
   */
  @Override
  public WorkerStatusList getChildWorkerStatusList(int index) {
    return null;
  }

  /**
   * @see org.insensa.WorkerStatus#getParentWorkerStatusList()
   */
  @Override
  public WorkerStatusList getParentWorkerStatusList() {
    return this.parentWorkerStatusList;
  }

  /**
   * @see org.insensa.WorkerStatus#setParentWorkerStatusList(org.insensa.WorkerStatusList)
   */
  @Override
  public void setParentWorkerStatusList(WorkerStatusList mProgress) {
    this.parentWorkerStatusList = mProgress;
  }

  /**
   * @see org.insensa.WorkerStatus#getProgress()
   */
  @Override
  public float getProgress() {
    return 0;
  }

  /**
   * @see org.insensa.WorkerStatus#getStepSize()
   */
  @Override
  public float getStepSize() {
    return 0;
  }

  /**
   * @see org.insensa.WorkerStatus#setStepSize(float)
   */
  @Override
  public void setStepSize(float stepSize) {
  }

  /**
   * @see org.insensa.WorkerStatus#refreshPercentage(float)
   */
  @Override
  public void refreshPercentage(float percent) {
    this.progressBar.setValue((int) percent);
    this.progressTree.refreshTree(this);
  }

  /**
   * @see org.insensa.WorkerStatus#removeChildrenWorkerStatusLists()
   */
  @Override
  public void removeChildrenWorkerStatusLists() {
  }

  /**
   * @see org.insensa.WorkerStatus#setChildrenWorkerStatusLists(int)
   */
  @Override
  public void setChildrenWorkerStatusLists(int numOfLists) {
  }


  /**
   * @see org.insensa.WorkerStatus#setProgressName(java.lang.String)
   */
  @Override
  public void setProgressName(String name) {
    this.progressBar.setString(name);
    this.progressBar.setStringPainted(true);
    this.progressTree.refreshTree(this);
  }

  /**
   * @see org.insensa.WorkerStatus#startPause(java.lang.String)
   */
  @Override
  public void startPause(String text) {
  }

  /**
   * @see org.insensa.WorkerStatus#startProcess()
   */
  @Override
  public void startProcess() {
    this.time = System.currentTimeMillis();
  }

}

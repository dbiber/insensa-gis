/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.awt.Color;

import javax.swing.border.MatteBorder;

import org.insensa.view.dialogs.ProgressBar;



public class ConnectionProgressBar extends ProgressBar
{

	private static final long serialVersionUID = 8198494160528459894L;

	/**
	 * @param connectionNode
	 */
	public ConnectionProgressBar(ConnectionTreeNode connectionNode)
	{
		MatteBorder border = new MatteBorder(5, 10, 5, 0, Color.white);
		super.setBorder(border);
	}

	/** 
	 * 
	 * @see org.insensa.view.dialogs.ProgressBar#errorProcess(java.lang.Throwable)
	 */
	@Override
	public void errorProcess(Throwable e)
	{
		super.errorProcess(e);
		// connectionNode.setError(e);
	}

	/** 
	 * 
	 * @see org.insensa.view.dialogs.ProgressBar#refreshPercentage(float)
	 */
	@Override
	public void refreshPercentage(float percent)
	{
		super.refreshPercentage(percent);
		// connectionNode.refreshTreeNode();
	}

	/** 
	 * 
	 * @see org.insensa.view.dialogs.ProgressBar#setProgressName(java.lang.String)
	 */
	public void setProgressName(String name)
	{
		super.setProgressName(name);
		// connectionNode.refreshTreeNode();
	}
}

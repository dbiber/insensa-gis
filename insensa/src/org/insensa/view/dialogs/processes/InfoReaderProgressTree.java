/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.XMLProperties.XmlGisFileInformation;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.MultipleFileInfoReaderThreadPool;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.View;
import org.insensa.view.dialogs.ProgressBar;
import org.insensa.view.dialogs.SettingsDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;


public class InfoReaderProgressTree extends SettingsDialog {
  private static final long serialVersionUID = -1741061767574615657L;
  private View view;
  private List<ProgressBar> progressBarList = new ArrayList<ProgressBar>();
  private List<Throwable> throwableList;
  private javax.swing.JScrollPane treeScrollPane;
  private javax.swing.JTree progressTree;
  private MultipleFileInfoReaderThreadPool threadPool;
  private ExecDependencyController depChecker;
  private DefaultMutableTreeNode rootNode;
  private List<InfoReaderFileTreeNode> fileNodeList;
  private List<IGisFileContainer> fileList;
  private boolean cancelPressed = false;
  private int leftThreads = 0;
  private int singleThreads = 0;
  private int finishedSingleThreads = 0;

  /**
   * Creates new form InfoReaderProgressTree.
   */
  public InfoReaderProgressTree(List<IGisFileContainer> fileList, View view, boolean modal) throws IOException {
    super(view, modal);
    this.throwableList = new ArrayList<>();
    this.fileNodeList = new ArrayList<>();
    this.fileList = fileList;
    this.view = view;

    this.depChecker = new ExecDependencyController();

    super.initComponents(this.initComponents("Execute InfoReader"));
    super.setHeadTitle("Execute InfoReader");
    super.getButtonOk().setText("Close");
    super.getButtonCancel().removeActionListener(this.getCancelActionListener());
    super.getButtonCancel().addActionListener(new CancelWindowActionListener());
    super.getButtonOk().addActionListener(new OkWindowActionListener());
    super.addWindowListener(new CloseWindowListener());
    super.setModalityType(ModalityType.MODELESS);
    super.setResizable(true);
    super.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

  }

  public synchronized void addProcesses(int processes) {
    this.singleThreads += processes;
  }

  private void checkFailure() {
    if (this.throwableList.isEmpty() == false) {
      int answer = JOptionPane.showConfirmDialog(this.view, "A problem accured while executing InfoReader "
          + "should be tried to solve the problem automatically ?");
      if (answer == JOptionPane.YES_OPTION) {
        for (Throwable iThrow : this.throwableList) {
          if (iThrow instanceof FileNotFoundException) {
            for (IGisFileContainer iFile : this.fileList) {
              if (((XmlGisFileInformation) iFile.getGisFileInformationStorage()).getXmlFile().exists() == false) {
                XmlGisFileInformation xmlInfo = new XmlGisFileInformation(iFile);
                for (InfoReader iReader : iFile.getInfoReaders()) {
                  if (iReader instanceof CountValues)
                    ((CountValues) iReader).setUsed(false);
                  xmlInfo.addInfoReader(iReader);
                }
                for (OptionFileChanger iOption : iFile.getOptions())
                  xmlInfo.addOptionFileChanger(iOption);
                if (iFile.getConnectionFileChanger() != null)
                  xmlInfo.setConnectionFileChanger(iFile.getConnectionFileChanger());
                iFile.setGisFileInformationStorage(xmlInfo);
                JOptionPane.showMessageDialog(this.view, "Restored XML Information for File " + iFile.getName());

              }
            }
          }
        }
      }
    }

  }

  private void createTree() throws IOException {
    for (int i = 0; i < this.fileList.size(); i++) {
      if (this.fileList.get(i).getUnusedInfoReaderCount() <= 0) {
        continue;
      }
      InfoReaderFileTreeNode fileNode = new InfoReaderFileTreeNode(this, this.view);
      this.fileNodeList.add(fileNode);
      this.rootNode.add(fileNode);
    }
    if (this.fileNodeList == null || this.fileNodeList.size() <= 0) {
      throw new IOException("No Unused InfoReader found");
    }
  }

  // End of variables declaration

  public synchronized void endAllProcesses() {
  }

  public synchronized void endProcess() {
    this.finishedSingleThreads++;
    if (this.finishedSingleThreads == (this.singleThreads - this.leftThreads)) {
      super.getButtonOk().setEnabled(true);
      super.getLabelStatus().setText("Finished");
    }
  }

  public int getActiveFileCount() {
    return this.fileNodeList.size();
  }

  public List<WorkerStatusList> getWorkerStatusList() {
    List<WorkerStatusList> sList = new ArrayList<WorkerStatusList>();
    sList.addAll(this.fileNodeList);
    return sList;
  }

  public List<InfoReaderFileTreeNode> getWorkerStatusListNodes() {
    return this.fileNodeList;
  }

  /**
   * Init Form an Tree Nodes.
   */
  private JPanel initComponents(String name) throws IOException {

    JPanel panel = new JPanel();
    this.treeScrollPane = new javax.swing.JScrollPane();
    this.treeScrollPane.addComponentListener(new ComponentResizeListener());
    this.progressTree = new javax.swing.JTree();
    this.rootNode = new DefaultMutableTreeNode(name);
    this.progressTree.setRootVisible(false);
    this.createTree();

    DefaultTreeModel treeModel = new DefaultTreeModel(this.rootNode);
    this.progressTree.setModel(treeModel);

    InfoReaderCellRenderer cellRenderer = new InfoReaderCellRenderer(this.progressBarList);
    this.progressTree.setCellRenderer(cellRenderer);
    this.treeScrollPane.setViewportView(this.progressTree);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.treeScrollPane,
        javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.treeScrollPane,
        javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE));
    this.pack();
    return panel;

  }

  /**
   * @param changedNode
   */
  public synchronized void refreshTree(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(() -> ((DefaultTreeModel) InfoReaderProgressTree.this.progressTree.getModel()).nodeChanged(changedNode));

  }

  /**
   * @param changedNode
   */
  public synchronized void refreshTreeExpand(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(() -> {
      ((DefaultTreeModel) InfoReaderProgressTree.this.progressTree.getModel()).nodeChanged(changedNode);
      InfoReaderProgressTree.this.progressTree.expandPath(new TreePath(changedNode.getPath()));
    });
  }

  public void setError(Throwable e) {
    this.throwableList.add(e);
  }

  public void setThreadPool(MultipleFileInfoReaderThreadPool threadPool) {
    this.threadPool = threadPool;
  }

  public void startAllProcesses(String name) throws IOException {

    if (!this.isVisible()) {
      this.setVisible(true);
      super.getButtonOk().setEnabled(false);
    }
  }

  private class CancelWindowActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      List<Runnable> runnableList = InfoReaderProgressTree.this.threadPool.getExeService().shutdownNow();
      if (InfoReaderProgressTree.this.cancelPressed == true)
        return;
      InfoReaderProgressTree.this.leftThreads = runnableList.size();
      InfoReaderProgressTree.this.cancelPressed = true;
      InfoReaderProgressTree.this.getLabelStatus().setText("Cancellation in progress...");
    }
  }

  private class CloseWindowListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent e) {
      InfoReaderProgressTree.this.threadPool.getExeService().shutdownNow();
      String warningMessage = "You are about to close the window although processes might have not been completed.\n\n" + "Are you sure?\n\n"
          + "If yes, you should refresh all used tree branches manually.";
      int answer = JOptionPane.showConfirmDialog(InfoReaderProgressTree.this.view, warningMessage, "Cancel", JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE);

      if (answer == JOptionPane.NO_OPTION)
        return;
      InfoReaderProgressTree.this.checkFailure();
      InfoReaderProgressTree.this.setVisible(false);
      JTree tree = InfoReaderProgressTree.this.view.getViewProject().getFileSetView().getFileSetTree();
      TreePath path = tree.getSelectionPath();
      TreePath pPath = path.getParentPath();

      tree.setSelectionPath(pPath);
      tree.setSelectionPath(path);
      InfoReaderProgressTree.this.view.refreshGui();
    }
  }

  private class ComponentResizeListener extends ComponentAdapter {

    @Override
    public void componentResized(ComponentEvent e) {
      e.getComponent().getSize();
      for (InfoReaderFileTreeNode node : InfoReaderProgressTree.this.fileNodeList) {
        node.refreshChildNodes();
      }
    }
  }

  private class OkWindowActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      InfoReaderProgressTree.this.checkFailure();
      InfoReaderProgressTree.this.setVisible(false);
      JTree tree = InfoReaderProgressTree.this.view.getViewProject().getFileSetView().getFileSetTree();

      // Einmal abwaehlen und wieder anwaehlen, damit die Properties View
      // aktualisiert wird
      TreePath path = tree.getSelectionPath();
      TreePath pPath = path.getParentPath();
      tree.setSelectionPath(pPath);
      tree.setSelectionPath(path);
      InfoReaderProgressTree.this.view.refreshGui();
    }
  }
}

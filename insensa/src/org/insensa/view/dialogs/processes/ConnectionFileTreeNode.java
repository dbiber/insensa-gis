/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;


public class ConnectionFileTreeNode extends DefaultMutableTreeNode implements WorkerStatusList {

  private static final long serialVersionUID = -858753699446388608L;
  private ConnectionProgressTree progressTree;
  private String nodeName = "waiting..";
  private List<ConnectionInfoReaderTreeNode> infoReaderTreeNodeList = new ArrayList<ConnectionInfoReaderTreeNode>();
  private int finishedThreads = 0;

  /**
   * @param progressTree
   */
  public ConnectionFileTreeNode(ConnectionProgressTree progressTree) {
    this.progressTree = progressTree;
    super.setUserObject(this.nodeName);
  }

  /**
   * @see org.insensa.WorkerStatusList#endAllProcesses()
   */
  @Override
  public void endAllProcesses() {
    this.finishedThreads++;
    if (this.finishedThreads >= this.infoReaderTreeNodeList.size())
      this.progressTree.refreshTreeCollapse(this);
  }

  /**
   * @see org.insensa.WorkerStatusList#getChildWorkerStatusList(org.insensa.WorkerStatus)
   */
  @Override
  public WorkerStatusList getChildWorkerStatusList(WorkerStatus wStat) {
    return null;
  }

  /**
   * @see org.insensa.WorkerStatusList#getWorkerStatus()
   */
  @Override
  public WorkerStatus getWorkerStatus() {
    return null;
  }

  /**
   * @see org.insensa.WorkerStatusList#getWorkerStatus(int)
   */
  @Override
  public WorkerStatus getWorkerStatus(int index) {
    return this.infoReaderTreeNodeList.get(index);
  }


  public void refreshChildNodes() {
    for (ConnectionInfoReaderTreeNode node : this.infoReaderTreeNodeList) {
      this.progressTree.refreshTree(node);
    }
  }

  /**
   * @see org.insensa.WorkerStatusList#startAllProcesses(java.lang.String, int)
   */
  @Override
  public void startAllProcesses(String name, int numOfProgress) throws IOException {
    this.nodeName = name;
    for (int i = 0; i < numOfProgress; i++) {
      ConnectionInfoReaderTreeNode infoReaderTreeNode = new ConnectionInfoReaderTreeNode(this.progressTree);
      infoReaderTreeNode.setParentWorkerStatusList(this);
      this.infoReaderTreeNodeList.add(infoReaderTreeNode);
      this.add(infoReaderTreeNode);
    }
    super.setUserObject(name);
    this.progressTree.refreshTreeExpand((DefaultMutableTreeNode) this.getParent());
    this.progressTree.refreshTreeExpand(this);
  }
}

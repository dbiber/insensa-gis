/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.connections.StackFilesThreadPools;
import org.insensa.view.View;
import org.insensa.view.dialogs.SettingsDialog;


public class ConnectionProgressTree extends SettingsDialog implements WorkerStatusList {
  private static final long serialVersionUID = 133843825303385283L;
  private View view;
  private List<ConnectionTreeNode> connectionTreeNodes;
  private JTree tree;
  private JScrollPane scrollPaneTree;
  private DefaultMutableTreeNode rootNode;
  private int finishedThread = 0;
  private List<Throwable> throwableList;
  private DefaultMutableTreeNode fileSetNode;
  private StackFilesThreadPools threadPool;
  private boolean cancelPressed = false;
  private int leftThreads = 0;
  private long time;
  private Date date = new Date();
  private SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss:SS");

  public ConnectionProgressTree(View view, DefaultMutableTreeNode fileSetNode) {
    super(view, false);
    this.view = view;
    this.fileSetNode = fileSetNode;
    this.connectionTreeNodes = new ArrayList<ConnectionTreeNode>();
    this.throwableList = new ArrayList<Throwable>();
    super.initComponents(this.initComponents());
    super.setHeadTitle("Execute Connection");
    super.getButtonOk().setText("Close");
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    super.getButtonCancel().removeActionListener(this.getCancelActionListener());
    super.getButtonCancel().addActionListener(new CancelWindowActionListener());
    super.addWindowListener(new CloseWindowListener());
    super.setModalityType(ModalityType.MODELESS);
    this.setResizable(true);
    super.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
  }

  @Override
  public synchronized void endAllProcesses() {
    this.finishedThread++;
    if (this.finishedThread == (this.connectionTreeNodes.size() - this.leftThreads)) {
      this.getButtonOk().setEnabled(true);
      this.time = System.currentTimeMillis() - this.time;
      this.date.setTime(this.time);
      super.getLabelStatus().setText("Finished: " + this.dateFormat.format(this.date));
    }
  }

  public synchronized void fullRefreshTree(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        ((DefaultTreeModel) ConnectionProgressTree.this.tree.getModel()).nodeStructureChanged(changedNode);

      }
    });
  }

  @Override
  public WorkerStatusList getChildWorkerStatusList(WorkerStatus wStat) {
    return null;
  }

  @Override
  public WorkerStatus getWorkerStatus() {
    return null;
  }

  @Override
  public synchronized WorkerStatus getWorkerStatus(int index) {
    return this.connectionTreeNodes.get(index);
  }

  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.scrollPaneTree = new JScrollPane();
    this.scrollPaneTree.addComponentListener(new ComponentResizeListener());
    this.tree = new JTree();
    this.rootNode = new DefaultMutableTreeNode();
    this.tree.setRootVisible(false);
    DefaultTreeModel treeModel = new DefaultTreeModel(this.rootNode);
    this.tree.setModel(treeModel);
    ConnectionTreeCellRenderer treeCellRenderer = new ConnectionTreeCellRenderer();
    this.tree.setCellRenderer(treeCellRenderer);
    this.scrollPaneTree.setViewportView(this.tree);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.scrollPaneTree,
        javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.scrollPaneTree,
        javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE));
    this.pack();
    return panel;
  }

  private void refreshMainTree() {
    JTree tree = this.view.getViewProject().getFileSetView().getFileSetTree();
    if (!this.fileSetNode.equals(tree.getModel().getRoot())) {
      TreePath path = tree.getSelectionPath();
      TreePath pPath = path.getParentPath();
      TreePath newPath = null;

      try {
        DefaultMutableTreeNode newNode = this.view.refreshTreeNode(this.fileSetNode, true);
        newPath = pPath.pathByAddingChild(newNode);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
      tree.setSelectionPath(newPath);
    } else {
      try {
        this.view.refreshTreeRoot(true);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }
    this.view.refreshGui();
  }

  public synchronized void refreshTree(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        ((DefaultTreeModel) ConnectionProgressTree.this.tree.getModel()).nodeChanged(changedNode);
      }
    });
  }

  public synchronized void refreshTreeCollapse(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        ((DefaultTreeModel) ConnectionProgressTree.this.tree.getModel()).nodeChanged(changedNode);
        ConnectionProgressTree.this.tree.collapsePath(new TreePath(changedNode.getPath()));
      }
    });

  }

  public synchronized void refreshTreeExpand(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        ((DefaultTreeModel) ConnectionProgressTree.this.tree.getModel()).nodeChanged(changedNode);
        ConnectionProgressTree.this.tree.expandPath(new TreePath(changedNode.getPath()));
      }
    });

  }

  public synchronized void removeTreeNodes(final List<DefaultMutableTreeNode> nodes) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        for (int i = 0; i < nodes.size(); i++) {
          DefaultMutableTreeNode parent = (DefaultMutableTreeNode) nodes.get(i).getParent();
          ((DefaultTreeModel) ConnectionProgressTree.this.tree.getModel()).removeNodeFromParent(nodes.get(i));
          ((DefaultTreeModel) ConnectionProgressTree.this.tree.getModel()).nodeStructureChanged(parent);
        }
      }
    });

  }

  public synchronized void setError(Throwable e) {
    this.throwableList.add(e);
  }

  public void setThreadPool(StackFilesThreadPools threadPool) {
    this.threadPool = threadPool;
  }

  /**
   * Adds Nodes for every Connection and Childs for every File in that
   * Connection.
   */
  @Override
  public synchronized void startAllProcesses(String name, int numOfProgress) throws IOException {
    this.connectionTreeNodes.clear();
    if (numOfProgress <= 0)
      return;
    for (int i = 0; i < numOfProgress; i++) {
      ConnectionTreeNode conTreeNode = new ConnectionTreeNode(this);
      this.connectionTreeNodes.add(conTreeNode);
      conTreeNode.setParentWorkerStatusList(this);
      this.rootNode.add(conTreeNode);
    }
    this.tree.expandPath(new TreePath(this.rootNode.getPath()));
    this.time = System.currentTimeMillis();
    super.getButtonOk().setEnabled(false);
    this.setVisible(true);
  }

  public synchronized void updateTree() {
    this.tree.updateUI();
  }

  private class CancelWindowActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      List<Runnable> runnableList = ConnectionProgressTree.this.threadPool.getExeService().shutdownNow();
      if (ConnectionProgressTree.this.cancelPressed == true)
        return;
      ConnectionProgressTree.this.leftThreads = runnableList.size();
      ConnectionProgressTree.this.cancelPressed = true;
      ConnectionProgressTree.this.getLabelStatus().setText("Cancellation in progress...");
    }
  }

  private class CloseWindowListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent e) {
      ConnectionProgressTree.this.threadPool.getExeService().shutdownNow();

      String warningMessage = "You are about to close the window although processes might have not been completed.\n\n" + "Are you sure?\n\n"
          + "If yes, you should refresh all used tree branches manually.";
      int answer = JOptionPane.showConfirmDialog(ConnectionProgressTree.this.view, warningMessage, "Cancel", JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE);

      if (answer == JOptionPane.NO_OPTION)
        return;
      ConnectionProgressTree.this.refreshMainTree();
      ConnectionProgressTree.this.setVisible(false);
    }
  }

  private class ComponentResizeListener extends ComponentAdapter {

    /**
     * @see java.awt.event.ComponentAdapter#componentResized(java.awt.event.ComponentEvent)
     */
    @Override
    public void componentResized(ComponentEvent e) {
      e.getComponent().getSize();
      for (ConnectionTreeNode node : ConnectionProgressTree.this.connectionTreeNodes) {
        node.refreshChildNodes();
        ConnectionProgressTree.this.refreshTree(node);
      }
    }
  }

  private class OkButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      ConnectionProgressTree.this.refreshMainTree();
      ConnectionProgressTree.this.setVisible(false);
    }
  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.processes;


import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.XMLProperties.XmlGisFileInformation;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.MultipleFileOptionThreadPool;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.View;
import org.insensa.view.dialogs.ProgressBar;
import org.insensa.view.dialogs.SettingsDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;


public class OptionFileChangerProgressTree extends SettingsDialog {
  private static final Logger log = LoggerFactory.getLogger(OptionFileChangerProgressTree.class);
  private static final long serialVersionUID = -2811940254348050190L;
  private View view;
  private List<ProgressBar> progressBarList = new ArrayList<ProgressBar>();
  private int finishedSingleThreads = 0;
  private int leftThreads = 0;
  private List<Throwable> throwableList;
  private javax.swing.JScrollPane treeScrollPane;
  private javax.swing.JTree progressTree;
  private ExecDependencyController depChecker;
  private MultipleFileOptionThreadPool threadPool;
  private DefaultMutableTreeNode rootNode;
  private List<OptionFileChangerFileTreeNode> fileNodeList;
  private List<IGisFileContainer> fileList;
  private List<DefaultMutableTreeNode> fileSetNodeList;
  private boolean cancelPressed = false;
  private int singleThreads = 0;
  private long time;
  private Date date = new Date();
  private SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss:SS");

  /**
   * Creates new form InfoReaderProgressTree.
   */
  public OptionFileChangerProgressTree(List<IGisFileContainer> fileList, List<DefaultMutableTreeNode> fileSetNodeList, View view, boolean modal)
      throws IOException {
    super(view, modal);
    this.throwableList = new ArrayList<Throwable>();
    this.fileNodeList = new ArrayList<>();
    this.fileList = fileList;
    this.view = view;
    this.fileSetNodeList = fileSetNodeList;

    this.depChecker = new ExecDependencyController();

    super.initComponents(this.initComponents("Execute Options"));
    super.setHeadTitle("Execute Options");
    super.getButtonOk().setText("Close");
    super.getButtonCancel().removeActionListener(this.getCancelActionListener());
    super.getButtonCancel().addActionListener(new CancelWindowActionListener());
    super.getButtonOk().addActionListener(new OkWindowActionListener());
    super.addWindowListener(new CloseWindowListener());
    super.setModalityType(ModalityType.MODELESS);
    this.setResizable(true);
    // setAlwaysOnTop(true);
    super.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

  }

  synchronized void addProcesses(int processes) {
    this.singleThreads += processes;
  }

  private void checkFailure() {
    if (!this.throwableList.isEmpty()) {
      int answer = JOptionPane.showConfirmDialog(this.view, "A problem accured while executing InfoReader "
          + "should be tried to solve the problem automatically ?");
      if (answer == JOptionPane.YES_OPTION) {
        for (Throwable iThrow : this.throwableList) {
          if (iThrow instanceof FileNotFoundException) {
            for (IGisFileContainer iFile : this.fileList) {
              if (!((XmlGisFileInformation) iFile.getGisFileInformationStorage()).getXmlFile().exists()) {
                XmlGisFileInformation xmlInfo = new XmlGisFileInformation(iFile);
                for (InfoReader iReader : iFile.getInfoReaders()) {
                  if (iReader instanceof CountValues) {
                    iReader.setUsed(false);
                  }
                  xmlInfo.addInfoReader(iReader);
                }
                for (OptionFileChanger iOption : iFile.getOptions())
                  xmlInfo.addOptionFileChanger(iOption);
                if (iFile.getConnectionFileChanger() != null)
                  xmlInfo.setConnectionFileChanger(iFile.getConnectionFileChanger());
                iFile.setGisFileInformationStorage(xmlInfo);
                JOptionPane.showMessageDialog(this.view, "Restored XML Information for File " + iFile.getName());

              }
            }
          }
        }
      }
    }
  }

  private void createTree() throws IOException {
    for (IGisFileContainer aFileList : this.fileList) {
      if (aFileList.getUnusedOptionCount() <= 0)
        continue;
      OptionFileChangerFileTreeNode fileNode = new OptionFileChangerFileTreeNode(this, this.view);
      this.fileNodeList.add(fileNode);
      this.rootNode.add(fileNode);
    }
    if (this.fileNodeList == null || this.fileNodeList.size() <= 0)
      throw new IOException("No Unused Options found");
  }

  synchronized void endProcess() {
    this.finishedSingleThreads++;
    if (this.finishedSingleThreads == (this.singleThreads - this.leftThreads)) {
      super.getButtonOk().setEnabled(true);
      this.time = System.currentTimeMillis() - this.time;
      this.date.setTime(this.time);
      super.getLabelStatus().setText("Finished: " + this.dateFormat.format(this.date));
    }
  }

  public int getActiveFileCount() {
    return this.fileNodeList.size();
  }

  public List<WorkerStatusList> getWorkerStatusList() {
    List<WorkerStatusList> sList = new ArrayList<>();
    sList.addAll(this.fileNodeList);
    return sList;
  }

  public List<OptionFileChangerFileTreeNode> getWorkerStatusListNodes() {
    return this.fileNodeList;
  }

  /**
   * Init Form an Tree Nodes.
   */
  private JPanel initComponents(String name) throws IOException {

    JPanel panel = new JPanel();
    this.treeScrollPane = new javax.swing.JScrollPane();
    this.treeScrollPane.addComponentListener(new ComponentResizeListener());
    this.progressTree = new javax.swing.JTree();

    this.rootNode = new DefaultMutableTreeNode(name);
    this.progressTree.setRootVisible(false);
    this.createTree();

    DefaultTreeModel treeModel = new DefaultTreeModel(this.rootNode);
    this.progressTree.setModel(treeModel);

    OptionFileChangerCellRenderer cellRenderer = new OptionFileChangerCellRenderer();
    this.progressTree.setCellRenderer(cellRenderer);
    this.treeScrollPane.setViewportView(this.progressTree);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.treeScrollPane,
        javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.treeScrollPane,
        javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE));
    this.pack();
    return panel;
  }

  synchronized void refreshTree(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(() -> ((DefaultTreeModel) OptionFileChangerProgressTree.this.progressTree.getModel())
        .nodeChanged(changedNode));
  }

  synchronized void refreshTreeCollapse(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(() -> {
      ((DefaultTreeModel) OptionFileChangerProgressTree.this.progressTree.getModel()).nodeChanged(changedNode);
      OptionFileChangerProgressTree.this.progressTree.collapsePath(new TreePath(changedNode.getPath()));
    });

  }

  synchronized void refreshTreeExpand(final DefaultMutableTreeNode changedNode) {
    SwingUtilities.invokeLater(() -> {
      ((DefaultTreeModel) OptionFileChangerProgressTree.this.progressTree.getModel()).nodeChanged(changedNode);
      OptionFileChangerProgressTree.this.progressTree.expandPath(new TreePath(changedNode.getPath()));
    });

  }

  public synchronized void setError(Throwable e) {
    this.throwableList.add(e);
  }

  public void setThreadPool(MultipleFileOptionThreadPool threadPool) {
    this.threadPool = threadPool;
  }

  public synchronized void startAllProcesses(String name) throws IOException {
    if (!this.isVisible()) {
      this.time = System.currentTimeMillis();
      this.setVisible(true);
      super.getButtonOk().setEnabled(false);
    }
  }

  public void endAllProcesses() {

  }

  private class CancelWindowActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      List<Runnable> runnableList = OptionFileChangerProgressTree.this.threadPool.getExeService().shutdownNow();
      if (OptionFileChangerProgressTree.this.cancelPressed)
        return;
      OptionFileChangerProgressTree.this.leftThreads = runnableList.size();
      OptionFileChangerProgressTree.this.cancelPressed = true;
      OptionFileChangerProgressTree.this.getLabelStatus().setText("Cancellation in progress...");
    }
  }

  private class CloseWindowListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent e) {
      OptionFileChangerProgressTree.this.threadPool.getExeService().shutdownNow();

      String warningMessage = "You are about to close the window although processes might have not been completed.\n\n" + "Are you sure?\n\n"
          + "If yes, you should refresh all used tree branches manually.";
      int answer = JOptionPane.showConfirmDialog(OptionFileChangerProgressTree.this.view, warningMessage, "Cancel", JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE);

      if (answer == JOptionPane.NO_OPTION)
        return;
      OptionFileChangerProgressTree.this.checkFailure();
      try {
        for (DefaultMutableTreeNode iNode : OptionFileChangerProgressTree.this.fileSetNodeList)
          OptionFileChangerProgressTree.this.view.refreshTreeNode(iNode, true);
      } catch (IOException e1) {
        e1.printStackTrace();
      }
      OptionFileChangerProgressTree.this.setVisible(false);
      OptionFileChangerProgressTree.this.view.refreshGui();
    }
  }

  private class ComponentResizeListener extends ComponentAdapter {

    @Override
    public void componentResized(ComponentEvent e) {
      for (OptionFileChangerFileTreeNode node : OptionFileChangerProgressTree.this.fileNodeList) {
        node.refreshChildNodes();
      }
    }
  }

  private class OkWindowActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      OptionFileChangerProgressTree.this.checkFailure();
      JTree tree = OptionFileChangerProgressTree.this.view.getViewProject().getFileSetView().getFileSetTree();
      TreePath[] selTreePaths = tree.getSelectionPaths();
      try {

        for (DefaultMutableTreeNode iNode : OptionFileChangerProgressTree.this.fileSetNodeList)
          OptionFileChangerProgressTree.this.view.refreshTreeNode(iNode, true);

      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }
      OptionFileChangerProgressTree.this.view.refreshTreeSelection(selTreePaths);
      OptionFileChangerProgressTree.this.setVisible(false);
      OptionFileChangerProgressTree.this.view.refreshGui();

    }
  }

}

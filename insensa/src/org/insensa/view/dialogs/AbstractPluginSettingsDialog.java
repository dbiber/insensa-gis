package org.insensa.view.dialogs;

import org.insensa.extensions.IPluginSettings;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.JPanel;

abstract public class AbstractPluginSettingsDialog extends AbstractPluginSettings {
  private static final Logger log = LoggerFactory.getLogger(AbstractPluginSettingsDialog.class);

  ViewSettingsCloseListener closeListener;

  public AbstractPluginSettingsDialog() {
    super();
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
  }

  @Override
  public final void startView(ViewSettingsCloseListener closeListener) {
    this.closeListener = closeListener;
    JPanel childPanel = null;
    try {
      /**
       * This is the main method in the inherited class to create the view.
       */
      childPanel = initComponent();
    } catch (RuntimeException re) {
      log.error("Could not build the Component", re);
      closeListener.closed(ViewSettingsCloseListener.Result.BUTTON_CANCEL);
      this.setVisible(false);
    }
    initComponents(childPanel);
    super.setHeadTitle(getDialogTitle());
    super.setTitle(getDialogTitle());
    getButtonOk().addActionListener(actionEvent -> {
      onButtonOkPressed();
    });
    getButtonCancel().addActionListener(actionEvent -> {
      onButtonCancelPressed();
    });
    this.pack();
    this.setVisible(true);
  }

  protected void notifyDialogClosed(ViewSettingsCloseListener.Result result) {
    closeListener.closed(result);
  }

  public abstract String getDialogTitle();

  /**
   * This method should return the main JPanel object.
   * This methods gets called right within the Constructor,
   * therefore you cannot add dynamically attributes in the
   * separate Constructor
   */
  public abstract JPanel initComponent();

  public void onButtonCancelPressed() {
    this.closeListener.closed(ViewSettingsCloseListener.Result.BUTTON_CANCEL);
    setVisible(false);
  }

  public void onButtonOkPressed() {
    this.closeListener.closed(ViewSettingsCloseListener.Result.BUTTON_OK);
    setVisible(false);
  }

}

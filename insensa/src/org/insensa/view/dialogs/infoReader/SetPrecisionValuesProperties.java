/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.infoReader;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;

import javax.swing.JPanel;


import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;
import org.insensa.view.extensions.ViewChangeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SetPrecisionValuesProperties extends AbstractPluginSettings {
  private static final Logger log = LoggerFactory.getLogger(SetPrecisionValuesProperties.class);
  private static final long serialVersionUID = 5914991835067096160L;
  private static final String HELP_ID = "precison_value_settings";
  private PrecisionValues precisionValues;
  private javax.swing.JLabel labelClasses;
  private javax.swing.JSpinner spinnerClasses;
  private JPanel panel;

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  /**
   * @return
   */
  private JPanel initComponents() {

    this.panel = new JPanel();
    this.spinnerClasses = new javax.swing.JSpinner();
    this.labelClasses = new javax.swing.JLabel();

    this.spinnerClasses.setFont(new java.awt.Font("Dialog", 0, 12));
    this.spinnerClasses.setModel(new javax.swing.SpinnerNumberModel(3, 1, 100, 1));

    this.labelClasses.setFont(new java.awt.Font("Dialog", 0, 12));
    this.labelClasses.setText("Decimal Places: ");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.panel);
    this.panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup().addContainerGap().addComponent(this.labelClasses)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.spinnerClasses, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(142, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.spinnerClasses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.labelClasses)).addContainerGap()));
    return this.panel;
  }

  @Override
  public void setInfoReader(InfoReader infoReader) {
    this.precisionValues = (PrecisionValues) infoReader;
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    super.initComponents(this.initComponents());
    super.setHeadTitle("Precision Values Settings");

    int decimalPlaces = this.precisionValues.getMaxPrecision();
    this.spinnerClasses.setValue(new Integer(decimalPlaces));
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  private class OkButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      SetPrecisionValuesProperties.this.precisionValues.setMaxPrecision(((Integer) SetPrecisionValuesProperties.this.spinnerClasses.getValue())
          .intValue());
      try {
        ((InfoReader) SetPrecisionValuesProperties.this.precisionValues).getTargetFile().getGisFileInformationStorage()
            .refreshPluginExec(SetPrecisionValuesProperties.this.precisionValues);
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }
      SetPrecisionValuesProperties.this.setVisible(false);
    }
  }

}

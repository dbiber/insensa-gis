/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.infoReader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JPanel;

import org.insensa.inforeader.BreaksValues;
import org.insensa.inforeader.EqualBreaksValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;


public class DefaultSetBreaksValuesSettins extends AbstractPluginSettings {

  public static final String helpIdEqualBreaks = "equal_breaks_settings";
  public static final String helpIdJenksBreaks = "jenks_breaks_settings";
  private static final long serialVersionUID = -3326973235277329388L;
  private BreaksValues breaksValues;
  private javax.swing.JLabel labelClasses;
  private javax.swing.JSpinner spinnerClasses;
  private JPanel panel;

  @Override
  public String getHelpId() {
    if (this.breaksValues != null) {
      if (this.breaksValues instanceof EqualBreaksValues)
        return helpIdEqualBreaks;
    }
    return super.getHelpId();
  }

  private JPanel initComponents() {

    this.panel = new JPanel();
    this.spinnerClasses = new javax.swing.JSpinner();
    this.labelClasses = new javax.swing.JLabel();

    this.spinnerClasses.setFont(new java.awt.Font("Dialog", 0, 12));
    this.spinnerClasses.setModel(new javax.swing.SpinnerNumberModel(5, 2, 1000, 1));

    this.labelClasses.setFont(new java.awt.Font("Dialog", 0, 12));
    this.labelClasses.setText("Classes: ");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.panel);
    this.panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup().addContainerGap().addComponent(this.labelClasses)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.spinnerClasses, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(142, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.spinnerClasses, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.labelClasses)).addContainerGap()));
    return this.panel;
  }

  @Override
  public void setInfoReader(InfoReader infoReader) {
    this.breaksValues = (BreaksValues) infoReader;
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    super.initComponents(this.initComponents());
    if (this.breaksValues instanceof EqualBreaksValues) {
      super.setHeadTitle("Equal Breaks Settings");
    }
    int numOfClasses = this.breaksValues.getNumOfClasses();
    this.spinnerClasses.setValue(new Integer(numOfClasses));
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    this.setVisible(true);
  }

  private class OkButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        DefaultSetBreaksValuesSettins.this.breaksValues.setNumOfClasses(((Integer) DefaultSetBreaksValuesSettins.this.spinnerClasses.getValue())
            .intValue());
        ((InfoReader) DefaultSetBreaksValuesSettins.this.breaksValues).getTargetFile().getGisFileInformationStorage()
            .refreshPluginExec((InfoReader) DefaultSetBreaksValuesSettins.this.breaksValues);
      } catch (IOException e1) {
      }
      DefaultSetBreaksValuesSettins.this.setVisible(false);
    }
  }

}

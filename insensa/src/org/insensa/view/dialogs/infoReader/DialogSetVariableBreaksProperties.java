/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.infoReader;


import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ClassificationRange;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.VariableBreaksValues;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;
import org.insensa.view.extensions.ViewChangeType;
import org.insensa.view.image.ImageComponent;
import org.insensa.view.image.ImageVariableBreaks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;


public class DialogSetVariableBreaksProperties extends AbstractPluginSettings {
  private static final Logger log = LoggerFactory.getLogger(DialogSetVariableBreaksProperties.class);

  public static final String helpID = "variable_breaks_settings";
  private static final long serialVersionUID = 6771379907617904863L;
  Vector<Vector<Object>> rowVals = new Vector<Vector<Object>>();
  Vector<String> colNames = new Vector<String>();
  private JButton buttonAdd;
  private JButton buttonDel;
  private javax.swing.JPanel imagePanel;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable tableBreaks;
  private VariableBreaksValues breaks;
  private ImageComponent iComp = null;
  private List<ClassificationRange> rangeList = new ArrayList<ClassificationRange>();

  @Override
  public String getHelpId() {
    return helpID;
  }

  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public void imageClicked(double xPos) {
    int selRow = this.tableBreaks.getSelectedRow();
    if (selRow > -1) {
      this.rowVals.get(selRow).set(1, new Double(xPos));
      this.tableBreaks.setModel(new BreaksTableModel(this.rowVals, this.colNames, this.iComp));
      ((ImageVariableBreaks) this.iComp).markBreakLine(selRow, xPos);
      this.tableBreaks.setRowSelectionInterval(selRow, selRow);
      for (int i = 0; i < this.rowVals.size(); i++) {
        if (i != selRow)
          ((ImageVariableBreaks) this.iComp).unmarkBreakLine(i, (Double) this.rowVals.get(i).get(1));
      }
    }
  }

  public JPanel initComponents() {
    JPanel panel = new JPanel();
    this.jScrollPane1 = new javax.swing.JScrollPane();
    this.tableBreaks = new javax.swing.JTable();
    this.imagePanel = new javax.swing.JPanel();
    this.buttonAdd = new JButton();
    this.buttonDel = new JButton();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    this.colNames.add("Break");
    this.colNames.add("Values");

    this.tableBreaks.setModel(new BreaksTableModel(this.rowVals, this.colNames, this.iComp));
    this.tableBreaks.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent e) {
        int selRow = DialogSetVariableBreaksProperties.this.tableBreaks.getSelectedRow();
        if (selRow > -1) {
          Double value = (Double) DialogSetVariableBreaksProperties.this.rowVals.get(selRow).get(1);
          ((ImageVariableBreaks) DialogSetVariableBreaksProperties.this.iComp)
              .markBreakLine(selRow, value);
          for (int i = 0; i < DialogSetVariableBreaksProperties.this.rowVals.size(); i++) {
            if (i != selRow)
              ((ImageVariableBreaks) DialogSetVariableBreaksProperties.this.iComp).unmarkBreakLine(i,
                  (Double) DialogSetVariableBreaksProperties.this.rowVals.get(i).get(1));
          }
        }
      }
    });

    this.jScrollPane1.setViewportView(this.tableBreaks);

    this.imagePanel.setLayout(new java.awt.BorderLayout());
    this.buttonAdd.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
    this.buttonAdd.setText("Add");
    this.buttonAdd.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        int rowCount = DialogSetVariableBreaksProperties.this.tableBreaks.getModel().getRowCount();
        if (DialogSetVariableBreaksProperties.this.iComp != null) {
          ((ImageVariableBreaks) DialogSetVariableBreaksProperties.this.iComp).addBreakLine(0.0);
          Vector<Object> data = new Vector<Object>();
          data.add(new Integer(rowCount + 1));
          data.add(new Double(0.0));
          DialogSetVariableBreaksProperties.this.rowVals.add(data);
          DialogSetVariableBreaksProperties.this.tableBreaks.setModel(new BreaksTableModel(DialogSetVariableBreaksProperties.this.rowVals,
              DialogSetVariableBreaksProperties.this.colNames, DialogSetVariableBreaksProperties.this.iComp));
        }

      }
    });

    this.buttonDel.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
    this.buttonDel.setText("Delete");
    this.buttonDel.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        int selRow = DialogSetVariableBreaksProperties.this.tableBreaks.getSelectedRow();
        if (selRow > -1) {
          ((ImageVariableBreaks) DialogSetVariableBreaksProperties.this.iComp).deleteBreakLine(selRow);
          DialogSetVariableBreaksProperties.this.rowVals.remove(selRow);
          for (int i = 0; i < DialogSetVariableBreaksProperties.this.rowVals.size(); i++) {
            DialogSetVariableBreaksProperties.this.rowVals.get(i).set(0, new Integer(i + 1));
          }
          DialogSetVariableBreaksProperties.this.tableBreaks.setModel(new BreaksTableModel(DialogSetVariableBreaksProperties.this.rowVals,
              DialogSetVariableBreaksProperties.this.colNames, DialogSetVariableBreaksProperties.this.iComp));
        }
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        layout.createSequentialGroup().addComponent(this.buttonAdd)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.buttonDel))
                    .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.imagePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        javax.swing.GroupLayout.Alignment.TRAILING,
                        layout.createSequentialGroup()
                            .addComponent(this.imagePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(
                        layout.createSequentialGroup()
                            .addComponent(this.jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 272,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(this.buttonAdd).addComponent(this.buttonDel))))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)));

    return panel;
  }

  public void initImage() {
    this.iComp = new ImageVariableBreaks((CountValues) this.breaks.getTargetFile().getInfoReader(CountValues.NAME), "applTitle",
        this.breaks.getTargetFile().getName(), this);
    this.imagePanel.add(this.iComp.getComponent());
    this.invalidate();
    this.validate();
    List<ClassificationRange> rangeList = this.breaks.getRangeList();
    Vector<Object> val;
    if (rangeList != null && rangeList.size() > 0) {
      for (int i = 0; i < rangeList.size() - 1; i++) {
        Double dVal = new Double(rangeList.get(i).getHighValue());
        val = new Vector<Object>();
        val.add(new Integer(i + 1));
        val.add(dVal);
        this.rowVals.add(val);
        ((ImageVariableBreaks) this.iComp).addBreakLine(dVal);
      }
    }

  }

  @Override
  public void setInfoReader(InfoReader infoReader) {
    this.breaks = (VariableBreaksValues) infoReader;
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    super.initComponents(this.initComponents());
    super.setHeadTitle("Set VariableBreaks");
    super.setTitle("Set VariableBreaks");
    super.getButtonOk().addActionListener(new OkButtonListener());
    super.getButtonCancel().addActionListener(e -> setVisible(false));
    IGisFileContainer targetFile = this.breaks.getTargetFile();
    if (!targetFile.getInfoReader(CountValues.NAME).isUsed()) {
      try {
        targetFile.executeInfo(CountValues.NAME, new CountValuesLoaderList(this));
      } catch (IOException e) {
        log.error("Could not execute InfoReader CountValues", e);
        throw new RuntimeException(e);
      }
    } else {
      this.initImage();
    }
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  private class BreaksTableModel extends DefaultTableModel {

    /**
     *
     */
    private static final long serialVersionUID = 2195511650832446782L;

    Class[] types = new Class[]
        {java.lang.Integer.class, java.lang.Double.class};

    boolean[] canEdit = new boolean[]
        {false, true};

    public BreaksTableModel(Vector data, Vector names, ImageComponent iComp) {
      super(data, names);
      this.addTableModelListener(new BreaksTableModelListener(data, (ImageVariableBreaks) iComp));
    }

    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return this.canEdit[columnIndex];
    }

  }

  private class BreaksTableModelListener implements TableModelListener {

    public BreaksTableModelListener(Vector<Vector<Object>> vals, ImageVariableBreaks image) {

    }

    @Override
    public void tableChanged(TableModelEvent e) {
      int selRow = e.getFirstRow();
      BreaksTableModel tableModel = (BreaksTableModel) e.getSource();
      Double val = (Double) tableModel.getValueAt(selRow, 1);
      DialogSetVariableBreaksProperties.this.rowVals.get(selRow).set(1, val);
      ((ImageVariableBreaks) DialogSetVariableBreaksProperties.this.iComp).markBreakLine(selRow, val);
      for (int i = 0; i < DialogSetVariableBreaksProperties.this.rowVals.size(); i++) {
        if (i != selRow)
          ((ImageVariableBreaks) DialogSetVariableBreaksProperties.this.iComp).unmarkBreakLine(i,
              (Double) DialogSetVariableBreaksProperties.this.rowVals.get(i).get(1));
      }
    }

  }

  private class CountValueLoader implements WorkerStatus {
    WorkerStatusList wStatusList = null;

    public CountValueLoader(WorkerStatusList wStatusList) {
      this.wStatusList = wStatusList;
    }

    @Override
    public void endPause() {
    }

    @Override
    public void endProgress() {
      if (this.wStatusList != null)
        this.wStatusList.endAllProcesses();
    }

    @Override
    public void errorProcess() {
    }

    @Override
    public void errorProcess(String message) {
    }

    @Override
    public void errorProcess(Throwable e) {
    }

    @Override
    public WorkerStatusList getChildWorkerStatusList(int index) {
      return null;
    }

    @Override
    public WorkerStatusList getParentWorkerStatusList() {
      return null;
    }

    @Override
    public void setParentWorkerStatusList(WorkerStatusList mProgress) {
    }

    @Override
    public float getProgress() {
      return 0;
    }

    @Override
    public float getStepSize() {
      return 0;
    }

    @Override
    public void setStepSize(float stepSize) {
    }

    @Override
    public void refreshPercentage(float percent) {
    }

    @Override
    public void removeChildrenWorkerStatusLists() {
    }

    @Override
    public void setChildrenWorkerStatusLists(int numOfLists) {
    }

    @Override
    public void setProgressName(String name) {
    }

    @Override
    public void startPause(String text) {
    }

    @Override
    public void startProcess() {
    }

  }

  private class CountValuesLoaderList implements WorkerStatusList {
    private DialogSetVariableBreaksProperties varBreaksDia;
    private List<CountValueLoader> loaderList = new ArrayList<CountValueLoader>();
    private int processes;
    private int finishedProcesses = 0;

    public CountValuesLoaderList(DialogSetVariableBreaksProperties varBreaksDia) {
      this.varBreaksDia = varBreaksDia;
    }

    @Override
    public void endAllProcesses() {
      this.finishedProcesses++;
      if (this.finishedProcesses == this.processes) {
        this.varBreaksDia.getLabelStatus().setText("Loading table done");
        this.varBreaksDia.initImage();
      }

    }

    @Override
    public WorkerStatusList getChildWorkerStatusList(WorkerStatus wStat) {
      return null;
    }

    @Override
    public WorkerStatus getWorkerStatus() {
      return null;
    }

    @Override
    public WorkerStatus getWorkerStatus(int index) {
      return this.loaderList.get(index);
    }

    @Override
    public void startAllProcesses(String name, int numOfProgress) throws IOException {
      this.processes = numOfProgress;
      for (int i = 0; i < numOfProgress; i++) {
        CountValueLoader valLoader = new CountValueLoader(this);
        this.loaderList.add(valLoader);
      }
      this.varBreaksDia.getLabelStatus().setText("Loading table...");
    }

  }

  private class OkButtonListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      CountValues countValues = (CountValues) DialogSetVariableBreaksProperties.this.breaks.getTargetFile()
          .getInfoReader(CountValues.NAME);
      try {
        TreeMap<Float, Long> countMap = (TreeMap<Float, Long>) countValues
            .getCountValues().getMap();
        Vector<Object> tmpVal;

        // Baumliste sortieren
        for (int j = DialogSetVariableBreaksProperties.this.rowVals.size(); j > 0; j--)
          for (int i = 0; i < j - 1; i++) {
            if ((Double) DialogSetVariableBreaksProperties.this.rowVals.get(i).get(1) > (Double) DialogSetVariableBreaksProperties.this.rowVals
                .get(i + 1).get(1)) {
              tmpVal = DialogSetVariableBreaksProperties.this.rowVals.get(i);
              DialogSetVariableBreaksProperties.this.rowVals.set(i, DialogSetVariableBreaksProperties.this.rowVals.get(i + 1));
              DialogSetVariableBreaksProperties.this.rowVals.set(i + 1, tmpVal);
            }
          }
        // Breaks erstellen
        ClassificationRange range;
        for (int i = 0; i < DialogSetVariableBreaksProperties.this.rowVals.size() + 1; i++) {
          if (i == 0)
            range = new ClassificationRange(0, ((Double) DialogSetVariableBreaksProperties.this.rowVals.get(i).get(1)).floatValue());
          else if (i == DialogSetVariableBreaksProperties.this.rowVals.size())
            range = new ClassificationRange(((Double) DialogSetVariableBreaksProperties.this.rowVals.get(i - 1).get(1)).floatValue(),
                countMap.lastKey());
          else
            range = new ClassificationRange(((Double) DialogSetVariableBreaksProperties.this.rowVals.get(i - 1).get(1)).floatValue(),
                ((Double) DialogSetVariableBreaksProperties.this.rowVals.get(i).get(1)).floatValue());
          DialogSetVariableBreaksProperties.this.rangeList.add(range);
        }
        DialogSetVariableBreaksProperties.this.breaks.setRangeList(DialogSetVariableBreaksProperties.this.rangeList);
        DialogSetVariableBreaksProperties.this.breaks.getTargetFile().executeInfo(VariableBreaksValues.NAME, null);
        DialogSetVariableBreaksProperties.this.setVisible(false);
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }

    }
  }
}

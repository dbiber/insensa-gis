/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.infoReader;

import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.QuantileBreaksValues;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;
import org.insensa.view.extensions.ViewChangeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.SpinnerNumberModel;


public class DialogSetQuantileBreaksValues extends AbstractPluginSettings {
  public static final String helpID = "quantile_breaks_settings";
  private static final Logger log = LoggerFactory.getLogger(DialogSetQuantileBreaksValues.class);
  private static final long serialVersionUID = 1847831125049594545L;
  private javax.swing.JComboBox comboBoxMode;
  private javax.swing.JLabel labelMode;
  private javax.swing.JLabel labelValue;
  private javax.swing.JSpinner spinnerValue;
  private QuantileBreaksValues quantileBreaksvalues;
  private int numOfClasses;
  private int type;

  public DialogSetQuantileBreaksValues() {
  }

  @Override
  public String getHelpId() {
    return helpID;
  }

  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.comboBoxMode = new javax.swing.JComboBox();
    this.labelMode = new javax.swing.JLabel();
    this.labelValue = new javax.swing.JLabel();
    this.spinnerValue = new javax.swing.JSpinner();

    this.spinnerValue.setModel(new SpinnerNumberModel(this.numOfClasses, 2, 1000, 1));

    this.comboBoxMode.setModel(new javax.swing.DefaultComboBoxModel(new Integer[]
        {1, 2, 3, 4, 5, 6, 7, 8, 9}));
    this.comboBoxMode.setSelectedIndex(this.type - 1);
    this.labelMode.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelMode.setText("Type:");

    this.labelValue.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelValue.setText("Classes:");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(
                        javax.swing.GroupLayout.Alignment.LEADING,
                        layout.createSequentialGroup().addComponent(this.labelValue)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.spinnerValue))
                    .addGroup(
                        javax.swing.GroupLayout.Alignment.LEADING,
                        layout.createSequentialGroup()
                            .addComponent(this.labelMode)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(this.comboBoxMode, javax.swing.GroupLayout.PREFERRED_SIZE, 142,
                                javax.swing.GroupLayout.PREFERRED_SIZE))).addContainerGap(185, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addGap(18, 18, 18)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelMode)
                    .addComponent(this.comboBoxMode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelValue)
                    .addComponent(this.spinnerValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)));
    this.pack();
    return panel;
  }

  @Override
  public void setInfoReader(InfoReader infoReader) {
    this.quantileBreaksvalues = (QuantileBreaksValues) infoReader;
    this.numOfClasses = this.quantileBreaksvalues.getNumOfClasses();
    this.type = this.quantileBreaksvalues.getType();
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    if (this.quantileBreaksvalues == null)
      throw new RuntimeException("You have to set the InfoReader");
    // super.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
    super.initComponents(this.initComponents());
    super.setHeadTitle("Quantile Breaks Settings");
    super.getButtonOk().addActionListener(new OkButtonListener());
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  private class OkButtonListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      DialogSetQuantileBreaksValues.this.quantileBreaksvalues
          .setType((Integer) DialogSetQuantileBreaksValues.this.comboBoxMode.getSelectedItem());
      DialogSetQuantileBreaksValues.this.quantileBreaksvalues
          .setNumOfClasses((Integer) DialogSetQuantileBreaksValues.this.spinnerValue.getValue());
      try {
        DialogSetQuantileBreaksValues.this.quantileBreaksvalues
            .getTargetFile().getGisFileInformationStorage()
            .refreshPluginExec(DialogSetQuantileBreaksValues.this.quantileBreaksvalues);
      } catch (IOException e1) {

        DialogSetQuantileBreaksValues.this.getLabelStatus().setText(e1.getMessage());
        log.error("Error Saving File", e1);
      }
      DialogSetQuantileBreaksValues.this.setVisible(false);
    }
  }

}

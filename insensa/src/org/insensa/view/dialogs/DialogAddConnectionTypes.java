/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;


import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Model;
import org.insensa.view.View;
import org.insensa.view.ViewFileSet;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreePath;


public class DialogAddConnectionTypes extends AbstractChooseOutputDialog {
  private static final Logger log = LoggerFactory.getLogger(DialogAddConnectionTypes.class);

  private static final long serialVersionUID = -257871315014602070L;
  private List<IGisFileContainer> fileList = new ArrayList<>();
  private String connectionName;
  private StackType stackType;

  /**
   * Creates new form DialogAddConnectionTypes.
   */
  public DialogAddConnectionTypes(Frame parent, boolean modal,
                                  String title,
                                  Model model,
                                  View view,
                                  String connectionName,
                                  List<IGisFileContainer> fileList,
                                  StackType stackType) {
    super(parent, modal, model, view);
    this.stackType = stackType;
    this.fileList.addAll(fileList);
    this.connectionName = connectionName;
    this.setHeadTitle(title);
    this.setTitle(title);
    this.getButtonOk().addActionListener(new OkButtonListener());
    getButtonCancel().addActionListener(e -> setVisible(false));
    setLocationRelativeTo(view);
  }

  public IGisFileSet createAndAdd() throws IOException, JDOMException {
    String newFileName = getNewFileName();
    IGisFileSet tmpFileSet;
    if (stackType == StackType.INFO_CONNECTION) {
      if (isSelected) {
        model.createAndAddInfoConnection(fileList,
            outputFileSet,
            connectionName,
            newSetTextField.getText(),
            newFileName);
        tmpFileSet = outputFileSet.getChildSetByName(newSetTextField.getText());
      } else {
        model.createAndAddInfoConnection(fileList,
            outputFileSet,
            connectionName,
            null,
            newFileName);
        tmpFileSet = outputFileSet;
      }
    } else {
      if (isSelected) {
        model.createAndAddConnection(fileList,
            outputFileSet,
            connectionName,
            newSetTextField.getText(),
            newFileName);
        tmpFileSet = outputFileSet.getChildSetByName(newSetTextField.getText());
      } else {
        model.createAndAddConnection(fileList, outputFileSet,
            connectionName, null, newFileName);
        tmpFileSet = outputFileSet;
      }
    }
    return tmpFileSet;
  }

  @Override
  public void setFile(IGisFileContainer file) {
  }

  public enum StackType {
    CONNECTION,
    STACK, INFO_CONNECTION
  }

  private class OkButtonListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
      if (!checkInputValues()) {
        return;
      }
//      IGisFileSet tmpFileSet;
      try {
        IGisFileSet tmpFileSet = createAndAdd();
        TreePath oldPath1 = new TreePath(treeNode.getPath());

        ViewFileSet fileSetView = view.getViewProject().getFileSetView();
        view.refreshTreeNode(treeNode, true);
        fileSetView.expandPathChild(oldPath1, tmpFileSet);
        setVisible(false);
      } catch (IOException | JDOMException e1) {
        log.error("", e1);
      }
    }
  }
}

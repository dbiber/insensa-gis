/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;


import org.insensa.GisFileType;
import org.insensa.IGisFileContainer;
import org.insensa.Model;
import org.insensa.controller.ControllerFileEditing;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.ServiceType;
import org.insensa.view.View;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;


public class ChooseOptionFileChanger extends SettingsDialog {
  private static final Logger log = LoggerFactory.getLogger(ChooseOptionFileChanger.class);

  private static final long serialVersionUID = 4822752237886099412L;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JTable jTable1;
  private Model model;
  private IGisFileContainer rasterFile;
  private ButtonGroup buttonGroup;
  private String selectedOption;
  private View view;
  private GisFileType gisFileType;

  public ChooseOptionFileChanger(GisFileType gisFileType, Model model, View view) {
    this.model = model;
    this.view = view;
    this.gisFileType = gisFileType;
    super.setTitle("Choose Option");
    this.setSize(600, 400);
    super.initComponents(this.initComponents());
    super.setHeadTitle("Choose Option");
    super.getButtonOk().addActionListener(new OkButtonListener());
    getButtonCancel().addActionListener(e -> setVisible(false));
  }

  /**
   * @return
   */
  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.jScrollPane2 = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    List<String> optionList = ExtensionManager.getInstance()
        .getUsableOptionList(ServiceType.EXEC, gisFileType);


    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    this.buttonGroup = new ButtonGroup();
    for (String optionName: optionList) {
      JPanel parentPanel = new JPanel();
//      String upperCaseName = optionName.substring(0, 1).toUpperCase() + optionName.substring(1);
      JRadioButton rButton = new JRadioButton();
      parentPanel.add(rButton);
      rButton.addActionListener(new RadioButtonActionListener());
      row = new Vector<Object>();
      row.add(parentPanel);
      this.buttonGroup.add(rButton);
//      row.add(upperCaseName);
      row.add(optionName);
      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("Selected");
    titleVec.add("Option");
    TableModel tModel = new OptionsTableModel(dataVector, titleVec);
    tModel.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        ChooseOptionFileChanger.this.repaint();
      }
    });
    this.jTable1.setModel(tModel);
    this.jTable1.getColumnModel().getColumn(0).setCellEditor(new RadioButtonEditor(new JCheckBox()));
    this.jTable1.getColumnModel().getColumn(0).setCellRenderer(new RadioButtonRenderer());
    this.jTable1.setRowHeight(26);
    this.jTable1.setName("jTable1"); // NOI18N
    this.jScrollPane2.setViewportView(this.jTable1);
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2,
        javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup().addComponent(this.jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)));
    return panel;
  }

  private class OkButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      ChooseOptionFileChanger.this.rasterFile = ChooseOptionFileChanger.this.model.getActiveGisFileInformation();
      JTree tree = ChooseOptionFileChanger.this.view.getViewProject().getFileSetView().getFileSetTree();
      ControllerFileEditing fileEdit = new ControllerFileEditing(tree.getSelectionPaths(), ChooseOptionFileChanger.this.view,
          ChooseOptionFileChanger.this.model);
      int addedOptions = 0;
      try {

        if (ChooseOptionFileChanger.this.selectedOption != null) {
          for (IGisFileContainer file: fileEdit.getFileList()) {
            if (!file.isLocked()) {
              ChooseOptionFileChanger.this.model.addOptionToFileByFile(file,
                  ChooseOptionFileChanger.this.selectedOption);
              addedOptions++;
            }
          }
          if (addedOptions <= 0) {
            JOptionPane.showMessageDialog(ChooseOptionFileChanger.this.view, "All files are locked", "OptionFileChanger Adding Error",
                JOptionPane.ERROR_MESSAGE);
            ChooseOptionFileChanger.this.setVisible(false);
            return;
          } else if (addedOptions < fileEdit.getFileList().size()) {
            JOptionPane.showMessageDialog(ChooseOptionFileChanger.this.view, "Some files were locked", "OptionFileChanger Adding Error",
                JOptionPane.ERROR_MESSAGE);
            ChooseOptionFileChanger.this.setVisible(false);
            return;
          }

        }
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(ChooseOptionFileChanger.this.view, e1.getMessage());
        log.error("UNKNOWN", e1);
      }

      ChooseOptionFileChanger.this.setVisible(false);
    }
  }

  private class OptionsTableModel extends DefaultTableModel {


    private static final long serialVersionUID = 3397437031717201384L;

    public OptionsTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0)
        return true;
      else
        return false;
    }
  }

  class RadioButtonActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      int row = ChooseOptionFileChanger.this.jTable1.getSelectedRow();
      String optionName = ChooseOptionFileChanger.this.jTable1.getModel().getValueAt(row, 1).toString();
//      String lowerCaseName = optionName.substring(0, 1).toLowerCase() + optionName.substring(1);
//      ChooseOptionFileChanger.this.selectedOption = lowerCaseName;
      ChooseOptionFileChanger.this.selectedOption = optionName;
    }
  }

  class RadioButtonEditor extends DefaultCellEditor implements ItemListener {

    private static final long serialVersionUID = -2892495723342374495L;
    private JRadioButton button;
    private JPanel panel;

    public RadioButtonEditor(JCheckBox checkBox) {
      super(checkBox);
    }

    public Object getCellEditorValue() {
      this.button.removeItemListener(this);
      return this.panel;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if (value == null)
        return null;
      this.button = (JRadioButton) ((JPanel) value).getComponent(0);
      this.button.addItemListener(this);
      this.panel = (JPanel) value;
      return (Component) value;
    }

    public void itemStateChanged(ItemEvent e) {
      super.fireEditingStopped();
    }
  }

  private class RadioButtonRenderer implements TableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

      if (value == null)
        return null;
      return (Component) value;
    }
  }
}

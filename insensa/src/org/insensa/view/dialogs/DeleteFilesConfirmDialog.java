/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class DeleteFilesConfirmDialog extends javax.swing.JDialog {
  public static final int OPTION_ALL = 1;
  public static final int OPTION_YES = 2;
  public static final int OPTION_NO = 3;
  public static final int OPTION_CANCEL = 0;
  public static final int OPTION_UNKNOWN = -1;
  private static final long serialVersionUID = 1406761501535727429L;
  private javax.swing.JButton buttonAll;
  private javax.swing.JButton buttonCancel;
  private javax.swing.JButton buttonNo;
  private javax.swing.JButton buttonYes;
  private javax.swing.JTextArea jTextArea1;
  private javax.swing.JPanel panelControls;
  private javax.swing.JPanel panelText;
  private javax.swing.JScrollPane jScrollPane1;
  private int option = 0;

  public DeleteFilesConfirmDialog(java.awt.Frame parent, boolean modal) {
    super(parent, modal);
    this.initComponents();
    this.buttonYes.requestFocusInWindow();

  }

  public int getOption() {
    return this.option;
  }

  public javax.swing.JTextArea getTextArea() {
    return this.jTextArea1;
  }

  private void initComponents() {
    this.panelControls = new javax.swing.JPanel();
    this.buttonAll = new javax.swing.JButton();
    this.buttonYes = new javax.swing.JButton();
    this.buttonNo = new javax.swing.JButton();
    this.buttonCancel = new javax.swing.JButton();
    this.jTextArea1 = new javax.swing.JTextArea();
    this.panelText = new JPanel();
    this.jScrollPane1 = new javax.swing.JScrollPane();

    Icon warnIcon = UIManager.getIcon("OptionPane.warningIcon");
    JLabel label = new JLabel(warnIcon);
    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    this.panelControls.setLayout(new javax.swing.BoxLayout(this.panelControls, javax.swing.BoxLayout.LINE_AXIS));
    this.panelControls.add(Box.createHorizontalGlue());
    this.buttonAll.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.buttonAll.setText("All");
    this.buttonAll.addActionListener(e -> {
      DeleteFilesConfirmDialog.this.option = OPTION_ALL;
      DeleteFilesConfirmDialog.this.setVisible(false);
    });
    this.panelControls.add(this.buttonAll);

    this.panelControls.add(Box.createRigidArea(new Dimension(5, 0)));
    this.buttonYes.setFont(new java.awt.Font("Dialog", 0, 12));
    this.buttonYes.setText("Yes");
    this.buttonYes.addActionListener(e -> {
      DeleteFilesConfirmDialog.this.option = OPTION_YES;
      DeleteFilesConfirmDialog.this.setVisible(false);
    });
    this.panelControls.add(this.buttonYes);

    this.panelControls.add(Box.createRigidArea(new Dimension(5, 0)));
    this.buttonNo.setFont(new java.awt.Font("Dialog", 0, 12));
    this.buttonNo.setText("No");
    this.buttonNo.addActionListener(e -> {
      DeleteFilesConfirmDialog.this.option = OPTION_NO;
      DeleteFilesConfirmDialog.this.setVisible(false);
    });
    this.panelControls.add(this.buttonNo);

    this.panelControls.add(Box.createRigidArea(new Dimension(5, 0)));
    this.buttonCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.buttonCancel.setText("Cancel");
    this.buttonCancel.addActionListener(e -> {
      DeleteFilesConfirmDialog.this.option = OPTION_CANCEL;
      DeleteFilesConfirmDialog.this.setVisible(false);
    });
    this.panelControls.add(this.buttonCancel);
    this.panelControls.add(Box.createHorizontalGlue());

    this.jScrollPane1.setBorder(null);
    this.jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    this.jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

    this.jTextArea1.setColumns(20);
    this.jTextArea1.setLineWrap(true);
    this.jTextArea1.setRows(5);
    this.jTextArea1.setBorder(null);
    this.jTextArea1.setOpaque(false);
    this.jTextArea1.setEditable(false);

    this.jScrollPane1.setViewportView(this.jTextArea1);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
    this.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            layout.createSequentialGroup()
                .addGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                            javax.swing.GroupLayout.Alignment.TRAILING,
                            layout.createSequentialGroup().addContainerGap()
                                .addComponent(this.panelControls, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE))
                        .addGroup(
                            layout.createSequentialGroup()
                                .addComponent(label, javax.swing.GroupLayout.PREFERRED_SIZE, 52,
                                    javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)))
                .addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.panelControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap()));

    this.pack();
  }

}

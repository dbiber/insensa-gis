/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.options;

import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;

import java.awt.Frame;
import java.io.IOException;

/**
 * Helper implementation of {@link IOptionSetting}.
 * This is a good starting point for implementing Setting
 * Dialogs for {@link OptionFileChanger}
 */
public abstract class AbstractOptionSetting extends AbstractPluginSettings {

  private static final long serialVersionUID = 257625434431461256L;
  protected OptionFileChanger option = null;

  @Override
  public void setOption(OptionFileChanger option) {
    this.option = option;
  }
}

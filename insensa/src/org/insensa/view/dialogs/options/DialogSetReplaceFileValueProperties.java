/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.options;

import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.ReplaceRasterFileValue;
import org.insensa.view.dialogs.AbstractPluginSettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;
import org.insensa.view.dialogs.helper.DefaultOptionStyleFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;


public class DialogSetReplaceFileValueProperties extends AbstractPluginSettingsDialog {
  public static final String HELP_ID = "replace_value_settings";
  private static final Logger log = LoggerFactory.getLogger(DialogSetReplaceFileValueProperties.class);
  private static final long serialVersionUID = -4771383948562549058L;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JLabel labelFrom;
  private javax.swing.JLabel labelTo;
  private javax.swing.JLabel labelValue;
  private javax.swing.JPanel panelDistance;
  private javax.swing.JPanel panelReplaceValue;
  private javax.swing.JTextField textFieldFrom;
  private javax.swing.JTextField textFieldTo;
  private javax.swing.JTextField textFieldValue;

  private javax.swing.JTextPane textPaneHelp;
  private OptionFileChanger option = null;

  public DialogSetReplaceFileValueProperties() {
  }

  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  private String getValueError(String value) {
    if (value == null || value.isEmpty())
      return "missing value";

    if (value.equals("MIN") || value.equals("MAX") || value.equals("NODATA"))
      return "";

    try {
      Float.valueOf(value);
    } catch (NumberFormatException e) {
      return "\"" + value + "\" is not a valid value";
    }

    return "";
  }


  public JPanel initComponent() {
    this.jPanel1 = new javax.swing.JPanel();
    this.panelDistance = new javax.swing.JPanel();
    this.labelFrom = new javax.swing.JLabel();
    this.textFieldFrom = new javax.swing.JTextField();
    this.labelTo = new javax.swing.JLabel();
    this.textFieldTo = new javax.swing.JTextField();
    this.panelReplaceValue = new javax.swing.JPanel();
    this.labelValue = new javax.swing.JLabel();
    this.textFieldValue = new javax.swing.JTextField();
    this.jScrollPane2 = new javax.swing.JScrollPane();
    this.textPaneHelp = new javax.swing.JTextPane();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    DefaultOptionStyleFactory styleFac = new DefaultOptionStyleFactory();
    StyledDocument doc = this.textPaneHelp.getStyledDocument();
    styleFac.addStyles(doc);

    String insertText = "";
    try {
      doc.insertString(doc.getLength(), "Help:\n", styleFac.getTitleStyle());
      insertText = "Possible values for the distance attributes, in addition" + " to comma-separated numbers are:\n" + "  MIN\n" + "  MAX\n"
          + "  NODATA\n" + "\n" + "Possible values for the distance attributes, in addition" + " to comma-separated numbers are:\n" + "  MIN\n"
          + "  MAX\n" + "  NODATA\n";
      doc.insertString(doc.getLength(), insertText, styleFac.getNormalStyle());
    } catch (BadLocationException e) {
      e.printStackTrace();
    }

    this.panelDistance.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Distance", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
        javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    this.labelFrom.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelFrom.setText("from: ");

    this.textFieldFrom.setText("0.0");

    this.labelTo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelTo.setText("To:");

    this.textFieldTo.setText("0.0");

    if (!((ReplaceRasterFileValue) this.option).getFromValue().isEmpty())
      this.textFieldFrom.setText(((ReplaceRasterFileValue) this.option).getFromValue());
    if (!((ReplaceRasterFileValue) this.option).getToValue().isEmpty())
      this.textFieldTo.setText(((ReplaceRasterFileValue) this.option).getToValue());

    this.panelReplaceValue.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Replace Value",
        javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    this.labelValue.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelValue.setText("Value: ");

    this.textFieldValue.setText("0");
    if (!((ReplaceRasterFileValue) this.option).getReplaceValue().isEmpty())
      this.textFieldValue.setText(((ReplaceRasterFileValue) this.option).getReplaceValue());

    javax.swing.GroupLayout panelDistanceLayout = new javax.swing.GroupLayout(this.panelDistance);
    this.panelDistance.setLayout(panelDistanceLayout);
    panelDistanceLayout.setHorizontalGroup(panelDistanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelDistanceLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                panelDistanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.labelFrom)
                    .addComponent(this.labelTo))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(
                panelDistanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false).addComponent(this.textFieldTo)
                    .addComponent(this.textFieldFrom, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    panelDistanceLayout.setVerticalGroup(panelDistanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelDistanceLayout
            .createSequentialGroup()
            .addGroup(
                panelDistanceLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelFrom)
                    .addComponent(this.textFieldFrom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(
                panelDistanceLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelTo)
                    .addComponent(this.textFieldTo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)).addContainerGap(13, Short.MAX_VALUE)));

    // Replace Value
    javax.swing.GroupLayout panelReplaceValueLayout = new javax.swing.GroupLayout(this.panelReplaceValue);
    this.panelReplaceValue.setLayout(panelReplaceValueLayout);
    panelReplaceValueLayout.setHorizontalGroup(panelReplaceValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelReplaceValueLayout.createSequentialGroup().addContainerGap().addComponent(this.labelValue)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.textFieldValue, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE).addContainerGap()));
    panelReplaceValueLayout.setVerticalGroup(panelReplaceValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            panelReplaceValueLayout
                .createSequentialGroup()
                .addContainerGap()
                .addGroup(
                    panelReplaceValueLayout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(this.labelValue)
                        .addComponent(this.textFieldValue, javax.swing.GroupLayout.PREFERRED_SIZE,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

    this.textPaneHelp.setEditable(false);
    this.jScrollPane2.setViewportView(this.textPaneHelp);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(this.jPanel1);
    this.jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        jPanel1Layout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                jPanel1Layout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(this.panelReplaceValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)
                    .addComponent(this.panelDistance, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)).addGap(18, 18, 18)
            .addComponent(this.jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE).addContainerGap()));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        jPanel1Layout
            .createSequentialGroup()
            .addGroup(
                jPanel1Layout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        jPanel1Layout
                            .createSequentialGroup()
                            .addComponent(this.panelDistance, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(this.panelReplaceValue, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(this.jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)).addContainerGap()));

    return jPanel1;
  }

  @Override
  public void setOption(OptionFileChanger option) {
    this.option = option;
  }

  @Override
  public void onButtonOkPressed() {
    String fromValue = DialogSetReplaceFileValueProperties.this.textFieldFrom.getText();
    String toValue = DialogSetReplaceFileValueProperties.this.textFieldTo.getText();
    String replaceValue = DialogSetReplaceFileValueProperties.this.textFieldValue.getText();

    String error;
    error = DialogSetReplaceFileValueProperties.this.getValueError(fromValue);
    if (!error.isEmpty()) {
      DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(error);
      return;
    }
    error = DialogSetReplaceFileValueProperties.this.getValueError(toValue);
    if (!error.isEmpty()) {
      DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(error);
      return;
    }
    error = DialogSetReplaceFileValueProperties.this.getValueError(replaceValue);
    if (!error.isEmpty()) {
      DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(error);
      return;
    }

    ReplaceRasterFileValue replOption = (ReplaceRasterFileValue) DialogSetReplaceFileValueProperties.this.option;
    replOption.setFromValue(fromValue);
    replOption.setToValue(toValue);
    replOption.setReplaceValue(replaceValue);
    try {
      replOption.getActualFileContainer().getGisFileInformationStorage()
          .refreshPluginExec(DialogSetReplaceFileValueProperties.this.option);
      DialogSetReplaceFileValueProperties.this.setVisible(false);
    } catch (IOException e) {
      DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(e.getMessage());
    }
  }

//  @Override
//  public void startView(ViewSettingsCloseListener listener) {
//    if (this.option == null) {
//      throw new RuntimeException("You have to set an OptionFileChanger");
//    }
////    this.initComponents();
////    super.initComponents(this.jPanel1);
////    super.setHeadTitle("Set ReplaceFileValue Settings");
////    super.getButtonOk().addActionListener(new OkButtonActionListener());
//    this.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
//    this.setVisible(true);
//
//  }

  @Override
  public String getDialogTitle() {
    return "Set ReplaceFileValue Settings";
  }

//  @Override
//  public JPanel initComponent() {
//    return null;
//  }
//
//  private class OkButtonActionListener implements ActionListener {
//
//    @Override
//    public void actionPerformed(ActionEvent e1) {
//      String fromValue = DialogSetReplaceFileValueProperties.this.textFieldFrom.getText();
//      String toValue = DialogSetReplaceFileValueProperties.this.textFieldTo.getText();
//      String replaceValue = DialogSetReplaceFileValueProperties.this.textFieldValue.getText();
//
//      String error;
//      error = DialogSetReplaceFileValueProperties.this.getValueError(fromValue);
//      if (!error.isEmpty()) {
//        DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(error);
//        return;
//      }
//      error = DialogSetReplaceFileValueProperties.this.getValueError(toValue);
//      if (!error.isEmpty()) {
//        DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(error);
//        return;
//      }
//      error = DialogSetReplaceFileValueProperties.this.getValueError(replaceValue);
//      if (!error.isEmpty()) {
//        DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(error);
//        return;
//      }
//
//      ReplaceRasterFileValue replOption = (ReplaceRasterFileValue) DialogSetReplaceFileValueProperties.this.option;
//      replOption.setFromValue(fromValue);
//      replOption.setToValue(toValue);
//      replOption.setReplaceValue(replaceValue);
//      try {
//        replOption.getActualFileContainer().getGisFileInformationStorage()
//            .refreshPluginExec(DialogSetReplaceFileValueProperties.this.option);
//        DialogSetReplaceFileValueProperties.this.setVisible(false);
//      } catch (IOException e) {
//        DialogSetReplaceFileValueProperties.this.getLabelStatus().setText(e.getMessage());
//      }
//    }
//  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.options;

import java.awt.Frame;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.IPluginExec;
import org.insensa.extensions.IPluginSettings;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.extensions.IScriptPluginSettings;
import org.insensa.extensions.ViewSettingsFactory;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.VariableBreaksValues;
import org.insensa.model.generic.IVariable;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.VariableBreaks;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.infoReader.DialogSetVariableBreaksProperties;
import org.insensa.view.extensions.ViewChangeType;

import javax.swing.event.TreeSelectionListener;


public class SetVariableBreaksSettings implements IPluginSettings {
  private OptionFileChanger option;
  private Frame parentFrame;

  @Override
  public void setPluginExec(IPluginExec pluginExec) {
    this.option = (OptionFileChanger) pluginExec;
  }

  @Override
  public void startView(ViewSettingsCloseListener listener) {
    if (this.option != null && !this.option.isUsed()) {
      InfoReader infoReader = null;
      infoReader = this.option.getActualFileContainer()
          .getInfoReader("VariableBreaksValues");
      if (infoReader == null)
        throw new RuntimeException("No Variable Breaks InfoReader found");
      if (infoReader.isUsed())
        infoReader.setUsed(false);

      ExtensionManager exManager = ExtensionManager.getInstance();
      DialogSetVariableBreaksProperties vSettings;
      try {
        vSettings = (DialogSetVariableBreaksProperties)
            new ViewSettingsFactory(exManager)
                .getSettingsPlugin(infoReader);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }

      if (vSettings != null) {
        vSettings.setInfoReader(infoReader);
        vSettings.startView(result -> {
        });
      }
      ((VariableBreaks) this.option).setRangeList(vSettings.getRangeList());
    }
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }
}

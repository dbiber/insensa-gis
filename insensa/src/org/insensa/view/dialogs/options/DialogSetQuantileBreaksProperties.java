/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.options;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.TreeSelectionListener;


import org.insensa.inforeader.BreaksValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.QuantileBreaksValues;
import org.insensa.model.generic.IVariable;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.QuantileBreaks;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DialogSetQuantileBreaksProperties extends AbstractOptionSetting {
  private static final Logger log = LoggerFactory.getLogger(DialogSetQuantileBreaksProperties.class);
  private static final long serialVersionUID = 592553000262138006L;
  private OptionFileChanger option = null;
  private javax.swing.JComboBox comboBoxMode;
  private javax.swing.JLabel labelMode;
  private javax.swing.JLabel labelValue;
  private javax.swing.JSpinner spinnerValue;
  private BreaksValues breaksValues;
  private int numOfClasses;
  private int type;

  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.comboBoxMode = new javax.swing.JComboBox();
    this.labelMode = new javax.swing.JLabel();
    this.labelValue = new javax.swing.JLabel();
    this.spinnerValue = new javax.swing.JSpinner();

    this.spinnerValue.setModel(new SpinnerNumberModel(5, 2, 1000, 1));

    this.comboBoxMode.setModel(new javax.swing.DefaultComboBoxModel(new Integer[]
        {1, 2, 3, 4, 5, 6, 7, 8, 9}));
    // comboBoxMode.setSelectedIndex(type-1);
    this.labelMode.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelMode.setText("Type:");

    this.labelValue.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelValue.setText("Classes:");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(
                        javax.swing.GroupLayout.Alignment.LEADING,
                        layout.createSequentialGroup().addComponent(this.labelValue)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.spinnerValue))
                    .addGroup(
                        javax.swing.GroupLayout.Alignment.LEADING,
                        layout.createSequentialGroup()
                            .addComponent(this.labelMode)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(this.comboBoxMode, javax.swing.GroupLayout.PREFERRED_SIZE, 142,
                                javax.swing.GroupLayout.PREFERRED_SIZE))).addContainerGap(185, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addGap(18, 18, 18)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelMode)
                    .addComponent(this.comboBoxMode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelValue)
                    .addComponent(this.spinnerValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)));
    this.pack();
    return panel;
  }

  @Override
  public void setOption(OptionFileChanger option) {
    this.option = option;
  }

  @Override
  public void startView(ViewSettingsCloseListener listener) {
    this.initComponents(this.initComponents());
    this.breaksValues = (BreaksValues) this.option.getActualFileContainer()
        .getInfoReader(QuantileBreaksValues.QUANTILE_BREAKS_VALUES);
    if (this.breaksValues == null) {
      throw new RuntimeException("Info Reader "
          + QuantileBreaksValues.QUANTILE_BREAKS_VALUES
          + " is missing");
    }

    this.numOfClasses = this.breaksValues.getNumOfClasses();
    this.type = ((QuantileBreaksValues) this.breaksValues).getType();
    this.spinnerValue.setValue(new Integer(this.numOfClasses));
    this.comboBoxMode.setSelectedIndex(this.type - 1);
    super.setHeadTitle("Quantile Breaks Settings");
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    this.setVisible(true);
  }


  private class OkButtonActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if (((InfoReader) DialogSetQuantileBreaksProperties.this.breaksValues).isUsed())
        ((InfoReader) DialogSetQuantileBreaksProperties.this.breaksValues).setUsed(false);
      try {
        DialogSetQuantileBreaksProperties.this.breaksValues
            .setNumOfClasses(((Integer) DialogSetQuantileBreaksProperties.this.spinnerValue.getValue()).intValue());
        ((QuantileBreaksValues) DialogSetQuantileBreaksProperties.this.breaksValues)
            .setType(((Integer) DialogSetQuantileBreaksProperties.this.comboBoxMode.getSelectedItem()));
        ((InfoReader) DialogSetQuantileBreaksProperties.this.breaksValues).getTargetFile().getGisFileInformationStorage()
            .refreshPluginExec((InfoReader) DialogSetQuantileBreaksProperties.this.breaksValues);

        ((QuantileBreaks) DialogSetQuantileBreaksProperties.this.option)
            .setNumOfClasses(((Integer) DialogSetQuantileBreaksProperties.this.spinnerValue.getValue()).intValue());
        ((QuantileBreaks) DialogSetQuantileBreaksProperties.this.option).setType((Integer) DialogSetQuantileBreaksProperties.this.comboBoxMode
            .getSelectedItem());
        ((InfoReader) DialogSetQuantileBreaksProperties.this.breaksValues).getTargetFile().getGisFileInformationStorage()
            .refreshPluginExec(DialogSetQuantileBreaksProperties.this.option);
        DialogSetQuantileBreaksProperties.this.setVisible(false);
      } catch (IOException e1) {
        log.error("UNKNOWN", e1);
      }

    }
  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.options;

import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.TreeSelectionListener;

import org.insensa.model.generic.IVariable;
import org.insensa.optionfilechanger.Normalizing;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.View;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.connections.AbstractPluginSettings;
import org.insensa.view.extensions.ViewChangeType;


public class DialogSetNormalizingProperties extends AbstractPluginSettings {

  public static final String HELP_ID = "normalize_settings";
  private static final long serialVersionUID = -5148853975564155220L;
  private javax.swing.JComboBox comboBoxRange;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JTextPane textPaneDescription;
  private JLabel labelRange;
  private JLabel labelInverse;
  private JCheckBox checkBoxInverse;
  private Normalizing normalizing;
  private View view;
  private boolean inverse = false;

  /**
   * @param evt
   */
  private void comboBoxRangeActionPerformed(java.awt.event.ActionEvent evt) {

  }

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  /**
   * @return
   */
  private JPanel initComponents() {

    JPanel panel = new JPanel();
    this.labelRange = new javax.swing.JLabel();
    this.labelInverse = new javax.swing.JLabel();
    this.comboBoxRange = new javax.swing.JComboBox();
    this.checkBoxInverse = new javax.swing.JCheckBox();
    this.jScrollPane2 = new javax.swing.JScrollPane();
    this.textPaneDescription = new javax.swing.JTextPane();

    this.textPaneDescription.setEditable(false);
    this.textPaneDescription.setFont(new Font("Arial", Font.PLAIN, 10));
    this.textPaneDescription.setText("Transforms all values to a new defined range e.g. 0 to 1000. "
        + "Each value is subtracted by the minimum value and devided by the difference of the range of all values. "
        + "The new values are then multiplied by the defined range (see formula). "
        + "Inverse Min-Max-Normalization additionally inverts all values by subtracting it from the normalized range.");

    Integer[] rangeArray = new Integer[]
        {100, 1000, 10000};
    this.comboBoxRange.setModel(new javax.swing.DefaultComboBoxModel(rangeArray));
    this.comboBoxRange.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        DialogSetNormalizingProperties.this.comboBoxRangeActionPerformed(evt);
      }
    });

    Integer selectedInt = this.normalizing.getRange();
    this.comboBoxRange.setSelectedItem(selectedInt);

    this.checkBoxInverse.addItemListener(e -> {
      if (e.getStateChange() == ItemEvent.SELECTED)
        DialogSetNormalizingProperties.this.inverse = true;
      else
        DialogSetNormalizingProperties.this.inverse = false;

    });

    this.checkBoxInverse.setSelected(this.normalizing.isInverse());

    this.labelRange.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelRange.setText("Range: ");

    this.labelInverse.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.labelInverse.setText("Inverse:");

    this.jScrollPane2.setViewportView(this.textPaneDescription);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                            layout.createSequentialGroup()
                                .addComponent(this.labelRange)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(this.comboBoxRange, javax.swing.GroupLayout.PREFERRED_SIZE, 68,
                                    javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(
                            layout.createSequentialGroup().addComponent(this.labelInverse)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(this.checkBoxInverse))).addGap(18, 18, 18)
                .addComponent(this.jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addGap(18, 18, 18)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        layout.createSequentialGroup()
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(this.labelRange)
                                    .addComponent(this.comboBoxRange, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(this.labelInverse).addComponent(this.checkBoxInverse)))
                    .addComponent(this.jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)));
    return panel;
  }

  @Override
  public void setOption(OptionFileChanger option) {
    if (option != null && option instanceof Normalizing)
      this.normalizing = (Normalizing) option;
  }

  @Override
  public void startView(ViewSettingsCloseListener listener) {
    if (this.normalizing == null || this.view == null)
      throw new RuntimeException("You have to set View and OptionFileChanger");
    super.initComponents(this.initComponents());
    super.setHeadTitle("Set Normalizing Settings");
    super.setTitle("Set Normalizing Settings");
    super.getButtonOk().addActionListener(new OkButtonListener());
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  private class OkButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      try {
        DialogSetNormalizingProperties.this.normalizing
            .setRange((Integer) DialogSetNormalizingProperties.this
                .comboBoxRange.getSelectedItem());
        DialogSetNormalizingProperties.this.normalizing
            .setInverse(DialogSetNormalizingProperties.this.inverse);
        DialogSetNormalizingProperties.this.normalizing
            .getOldRasterFile().getGisFileInformationStorage()
            .refreshPluginExec(DialogSetNormalizingProperties.this.normalizing);
        DialogSetNormalizingProperties.this.setVisible(false);
      } catch (IOException e) {

        e.printStackTrace();
      }
    }
  }
}

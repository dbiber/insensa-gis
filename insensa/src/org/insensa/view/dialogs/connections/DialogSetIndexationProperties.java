/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.connections.CIndexation;
import org.insensa.connections.CIndexation.AggregationMethod;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.view.dialogs.AbstractPluginSettingsDialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;


public class DialogSetIndexationProperties extends AbstractPluginSettingsDialog//SettingsDialog
{

  public static final String HELP_ID = "indexation_and_weighting_settings";
  private static final long serialVersionUID = 1L;
  private JScrollPane jScrollPaneTable;
  private JTable jTable;
  private CIndexation indexation;
  private JSeparator jSep1;
  private JSeparator jSep2;
  private JSeparator jSep3;
  private List<IGisFileContainer> fileList = new ArrayList<IGisFileContainer>();
  private Map<String, CIndexation.Weighting> weightingMap = new HashMap<>();
  //  private List<Float> weightList = new ArrayList<Float>();
//  private List<CIndexation.Weighting> weightings = new ArrayList<>();
  private List<Boolean> isNetList = new ArrayList<Boolean>();
  private JButton rewardButton;
  private JButton penaltyButton;
  private DialogSetThreshold threshold1;
  private DialogSetThreshold threshold2;
  private JPanel mainJPanel;
  private JButton buttonEqualWeight;
  private JLabel labelAggregation;
  private JComboBox comboBoxAggregation;

  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  protected CIndexation.Weighting getWeighting(IGisFileContainer container) {
    return weightingMap.get(container.getPathRelativeToProject() + container.getOutputFileName());
  }

  @Override
  public void setConnection(ConnectionFileChanger connection) {
    this.indexation = (CIndexation) connection;
    this.fileList.addAll(this.indexation.getSourceFileList());
    this.weightingMap.putAll(indexation.getFileWeightingMap());
//    this.weightings.addAll(indexation.getWeightings());
//    int sizeDiff = this.fileList.size() - this.weightings.size();
//    for (int i = 0; i < sizeDiff; i++) {
//      this.weightings.add(new CIndexation.Weighting());
//    }
  }

  @Override
  public void onButtonOkPressed() {
    AggregationMethod method = AggregationMethod.valueOf((String)
        DialogSetIndexationProperties.this.comboBoxAggregation.getSelectedItem());
    if (method == AggregationMethod.GEOMETRIC) {
      int answer = JOptionPane.showConfirmDialog(DialogSetIndexationProperties.this.getParent(),
          "You choose the geometric  aggregation method.\n"
              + "This method is only suitable for positive indicator values.\n"
              + "Normalize your dataset in case of negative values prior geometric indexation.\n" + "Continue?", "", JOptionPane.YES_NO_OPTION,
          JOptionPane.INFORMATION_MESSAGE);
      if (answer == JOptionPane.NO_OPTION)
        return;

    }
    DialogSetIndexationProperties.this.indexation.setMethod(method);
    int rowCount = DialogSetIndexationProperties.this.jTable.getModel().getRowCount();
    for (int i = 0; i < rowCount; i++) {
      String path = (String) jTable.getValueAt(i,0);
      CIndexation.Weighting w = weightingMap.get(path);//getWeighting(fileList.get(i));
      float weight = (((Double) jTable.getValueAt(i, 1)).floatValue()) / 100;
      boolean isNeg = (Boolean) jTable.getValueAt(i, 2);
      w.setWeight(weight);
      w.setNegative(isNeg);
    }
    try {
      IGisFileContainer outputFile = DialogSetIndexationProperties.this.indexation.getOutputFileContainer();
      if (outputFile != null) {
//          outputFile.getGisFileInformationStorage().refreshConnectionFileChanger(DialogSetIndexationProperties.this.indexation);
        outputFile.getGisFileInformationStorage().refreshPluginExec(indexation);
      }
      this.indexation.setFileWeightingMap(weightingMap);
      DialogSetIndexationProperties.this.setVisible(false);
    } catch (IOException e1) {
      JOptionPane.showMessageDialog(null, e1.toString());
      e1.printStackTrace();
    }
  }

  @Override
  public JPanel initComponent() {
    this.threshold1 = new DialogSetThreshold(indexation, DialogSetThreshold.ThresholdMode.REWARD);
    this.threshold2 = new DialogSetThreshold(indexation, DialogSetThreshold.ThresholdMode.PENALTY);

    this.mainJPanel = new JPanel();
    this.buttonEqualWeight = new JButton("Equal");
    this.rewardButton = new JButton("Threshold Set 1");
    this.penaltyButton = new JButton("Threshold Set 2");
    this.jScrollPaneTable = new javax.swing.JScrollPane();
    this.jTable = new javax.swing.JTable();
    this.labelAggregation = new JLabel("AggregationMethod: ");
    this.comboBoxAggregation = new JComboBox(new String[]
        {CIndexation.AggregationMethod.ADDITIVE.toString(),
            CIndexation.AggregationMethod.GEOMETRIC.toString()});

    this.jSep1 = new JSeparator();
    this.jSep2 = new JSeparator();
    this.jSep3 = new JSeparator();
    this.buttonEqualWeight.addActionListener(new EqualButtonActionListener());
    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    this.setName("Form");
    this.rewardButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        DialogSetIndexationProperties.this.threshold1.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
        DialogSetIndexationProperties.this.threshold1.setVisible(true);
//        DialogSetIndexationProperties.this.indexation.setThreshold1List(DialogSetIndexationProperties.this.threshold1.getRewPenList());
      }
    });

    this.penaltyButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        DialogSetIndexationProperties.this.threshold2.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
        DialogSetIndexationProperties.this.threshold2.setVisible(true);
//        DialogSetIndexationProperties.this.indexation.setThreshold2List(DialogSetIndexationProperties.this.threshold2.getRewPenList());
      }
    });

    this.jScrollPaneTable.setName("jScrollPane2");

    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();

    weightingMap.forEach((s, weighting) -> {
      Vector<Object> row = new Vector<Object>();
      row.add(s);
      row.add(weighting.getWeight().doubleValue() * 100);
      row.add(weighting.getNegative());
      dataVector.add(row);
    });

//    for (int i = 0; i < this.fileList.size(); i++) {
//      row = new Vector<Object>();
//      row.add(this.fileList.get(i).getName());

//      if (this.weightings.size() <= i) {
//        row.add(0.0);
//      } else {
//        row.add((this.weightings.get(i).getWeight().doubleValue()) * 100);
//      }
//      if (this.isNetList.size() <= i) {
//        row.add(false);
//      } else {
//        row.add(this.isNetList.get(i));
//      }
//      dataVector.add(row);
//    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("File");
    titleVec.add("Weight ( % )");
    titleVec.add("Inverse");

    if (this.indexation.getMethod() == AggregationMethod.ADDITIVE)
      this.comboBoxAggregation.setSelectedIndex(0);
    else
      this.comboBoxAggregation.setSelectedIndex(1);

    this.jTable.setModel(new PropertiesTableModel(dataVector, titleVec));
    this.jTable.addMouseListener(new MouseListener() {

      @Override
      public void mouseClicked(MouseEvent arg0) {
        int rowCnt = DialogSetIndexationProperties.this.jTable.getModel().getRowCount();
        double momValue = 0.0;
        for (int i = 0; i < rowCnt; i++) {
          momValue += (Double) DialogSetIndexationProperties.this.jTable.getValueAt(i, 1);
        }
        SpinnerEditor tmpEditor = (SpinnerEditor) DialogSetIndexationProperties.this.jTable.getColumnModel().getColumn(1).getCellEditor();
        SpinnerNumberModel tmpModel = (SpinnerNumberModel) tmpEditor.getSpinner().getModel();
        int selRow = DialogSetIndexationProperties.this.jTable.getSelectedRow();
        double oldValue = (Double) DialogSetIndexationProperties.this.jTable.getValueAt(selRow, 1);
        // +0.4 ist nur fuer die rundungsprobleme von Double.
        double valueLeft = (100.0 - momValue) + oldValue + 0.4;
        tmpModel.setMaximum(valueLeft);
      }

      @Override
      public void mouseEntered(MouseEvent e) {
      }

      @Override
      public void mouseExited(MouseEvent e) {
      }

      @Override
      public void mousePressed(MouseEvent e) {
      }

      @Override
      public void mouseReleased(MouseEvent e) {
      }
    });

    NumberColumnFormat numberColumn = new NumberColumnFormat("##.##", "##.##");
    this.jTable.getColumnModel().getColumn(0).setCellRenderer(
        (table, value, isSelected, hasFocus, row, column)
            -> new JLabel(new File(value.toString()).getName()));
    this.jTable.getColumnModel().getColumn(1).setCellRenderer(numberColumn.getRenderer());
    this.jTable.getColumnModel().getColumn(1).setCellEditor(new SpinnerEditor());
    this.jTable.setRowHeight(26);
    this.jTable.setName("jTable1");

    this.jScrollPaneTable.setViewportView(this.jTable);
    GroupLayout layout = new GroupLayout(this.mainJPanel);
    this.mainJPanel.setLayout(layout);
    layout.setHorizontalGroup(
        // Parallel
        layout.createParallelGroup(Alignment.LEADING)
            // .addComponent(jScrollPaneInfoText, Alignment.TRAILING,
            // GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
            // Seriell 1
            /*
             * .addGroup(layout.createSequentialGroup() .addContainerGap()
             * .addComponent(fileNameLabel)
             * .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
             * .addComponent(fileNameTextField) .addContainerGap())//End
             * Seriell 1
             */
            // Seriell 2
            /*
             * .addGroup(layout.createSequentialGroup() .addContainerGap()
             * .addComponent(jSep1) .addContainerGap())
             */// End Seriell 2
            // Seriell 3
            .addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                    .addContainerGap())
            // End Seriell 3
            // Seriel 4
            .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep2).addContainerGap())
            // End
            // Seriell

            // Seriell 5
            .addGroup(
                Alignment.LEADING,
                layout.createSequentialGroup().addContainerGap().addComponent(this.rewardButton).addContainerGap().addComponent(this.penaltyButton)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 100, Short.MAX_VALUE).addComponent(this.buttonEqualWeight)
                    .addContainerGap())
            // Seriell

            .addGroup(
                Alignment.LEADING,
                layout.createSequentialGroup().addContainerGap().addComponent(this.labelAggregation).addContainerGap()
                    .addComponent(this.comboBoxAggregation).addContainerGap())
            .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep3).addContainerGap())
        // 5

        // Seriell 6
        // .addGroup(
        // layout.createSequentialGroup().addContainerGap().addComponent(okButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        // .addComponent(cancelButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
        // .addGap(280, 280, 280)) //End Seriell 6

        // .addComponent(statusLabel, GroupLayout.DEFAULT_SIZE, 448,
        // Short.MAX_VALUE)
        // End Parallel
    );

    layout.setVerticalGroup(
        // Parallel 1
        layout.createParallelGroup(Alignment.LEADING)
            // Seriell 1
            .addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup()
                    // .addComponent(jScrollPaneInfoText,
                    // GroupLayout.PREFERRED_SIZE,
                    // GroupLayout.DEFAULT_SIZE,
                    // GroupLayout.PREFERRED_SIZE)
                    // .addGap(20)
                    // Parallel 2
                    /*
                     * .addGroup(layout.createParallelGroup(GroupLayout
                     * .Alignment.BASELINE)
                     * .addComponent(fileNameLabel)
                     * .addComponent(fileNameTextField))
                     */// End Parallel 2

                    // .addGap(20)
                    // .addComponent(jSep1)
                    // .addGap(20,20,20)
                    .addComponent(this.jScrollPaneTable, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    // .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGap(20)

                    .addGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.rewardButton)
                            .addComponent(this.penaltyButton).addComponent(this.buttonEqualWeight))
                    .addGap(20)
                    .addComponent(this.jSep2)
                    .addGap(20, 20, 20)
                    .addGroup(
                        layout.createParallelGroup(Alignment.BASELINE).addComponent(this.labelAggregation)
                            .addComponent(this.comboBoxAggregation)).addGap(20).addComponent(this.jSep3))
        // Parallel 3
        // .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(okButton).addComponent(cancelButton))
        // .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        // .addComponent(statusLabel, GroupLayout.PREFERRED_SIZE, 19,
        // GroupLayout.PREFERRED_SIZE))
        // End Seriell 1
    );// End Parallel 1
    return this.mainJPanel;
  }

  @Override
  public String getDialogTitle() {
    return "Set Indexation";
  }


//  @Override
//  public void startView(ViewSettingsCloseListener closeListener) {
//    if (this.indexation == null) {
//      throw new RuntimeException("You have to set a connection");
//    }
//    this.fileList.addAll(this.indexation.getRasterFileList());
//    this.weightings.addAll(indexation.getWeightings());
//    this.threshold1 = new DialogSetThreshold(indexation, DialogSetThreshold.ThresholdMode.REWARD);
//    this.threshold2 = new DialogSetThreshold(indexation, DialogSetThreshold.ThresholdMode.PENALTY);
//    super.setTitle("Set Indexation");
//    this.setSize(600, 400);
//    super.initComponents(this.initComponents());
//    super.setHelpId("sec:indexation_and_weighting_settings");
//    super.getButtonOk().addActionListener(new OkButtonActionListener());
//    super.setHeadTitle("Set Indexation Properties");
//    this.setVisible(true);
//  }

  private class EqualButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      int rowCnt = DialogSetIndexationProperties.this.jTable.getRowCount();
      double value = 100.0 / rowCnt;
      for (int i = 0; i < rowCnt; i++)
        DialogSetIndexationProperties.this.jTable.setValueAt(value, i, 1);
    }
  }

  /**
   * <code>NumberColumnFormat</code> makes available a simple NumberRenderer
   * and NumberEditor for JTable. Usage example:
   *
   * <pre>
   * <code>
   * &NumberColumnFormat numberColumnFormat = new NumberColumnFormat();
   * &table.setDefaultRenderer(Number.class, numberColumnFormat.getRenderer());
   * &table.setDefaultEditor(Number.class, numberColumnFormat.getEditor());
   * </code>
   * </pre>
   */
  public class NumberColumnFormat {
    private DecimalFormat formatterR, formatterE;
    private DefaultTableCellRenderer renderer;
    private DecimalFormatSymbols numberSymbols;
    private DefaultCellEditor editor;

    {
      this.numberSymbols = new DecimalFormatSymbols();
      this.numberSymbols.setGroupingSeparator('.');
      this.numberSymbols.setDecimalSeparator(',');
    }

    /**
     * Constructs a <code>NumberColumnFormat</code> using the pattern
     * "###,###,###.##" and the default number format symbols for the
     * default locale.
     */
    public NumberColumnFormat() {
      this(null);
    }

    /**
     * Constructs a <code>NumberColumnFormat</code> using the given pattern
     * and the default number format symbols for the default locale.
     *
     * @param pattern the pattern describing the number format, "###,###,###.##"
     *                if null
     */
    public NumberColumnFormat(String pattern) {
      this(pattern, pattern);
    }

    /**
     * Constructs a <code>NumberColumnFormat</code> using the given patterns
     * and the default number format symbols for the default locale.
     *
     * @param patternR the pattern describing the number format of the renderer,
     *                 "###,###,###.##" if null
     * @param patternE the pattern describing the number format of the editor,
     *                 "#########.##" if null
     */
    public NumberColumnFormat(String patternR, String patternE) {
      if (patternR == null)
        patternR = "###,###,###.##";
      this.formatterR = new DecimalFormat(patternR, this.numberSymbols);
      this.formatterR.setDecimalFormatSymbols(this.numberSymbols);
      int d = patternR.split("\\.")[1].length();
      this.formatterR.setMinimumFractionDigits(d);
      this.formatterR.setMaximumFractionDigits(d);
      if (patternE == null)
        patternE = "#########.##";
      this.formatterE = new DecimalFormat(patternE, this.numberSymbols);
      this.renderer = new NumberRenderer();
      this.editor = new NumberEditor();
    }

    /**
     * Returns the number cell editor.
     *
     * @return the table number cell editor
     */
    public DefaultCellEditor getEditor() {
      return this.editor;
    }

    /**
     * Returns the DecimalFormatSymbols.
     *
     * @return the DecimalFormatSymbols used for the number rendering.
     */
    public DecimalFormatSymbols getNumberSymbols() {
      return this.numberSymbols;
    }

    /**
     * Sets the DecimalFormatSymbols used for the number rendering.
     */
    public void setNumberSymbols(DecimalFormatSymbols numberSymbols) {
      this.numberSymbols = numberSymbols;
      this.formatterR.setDecimalFormatSymbols(numberSymbols);
      this.formatterE.setDecimalFormatSymbols(numberSymbols);
    }

    /**
     * Returns the number cell renderer.
     *
     * @return the table number cell renderer
     */
    public DefaultTableCellRenderer getRenderer() {
      return this.renderer;
    }

    /**
     * Sets the DecimalSeparator used for the number rendering.
     */
    public void setDecimalSeparator(char sep) {
      this.numberSymbols.setDecimalSeparator(sep);
      this.setNumberSymbols(this.numberSymbols);
    }

    /**
     * Sets the GroupingSeparator used for the number rendering.
     */
    public void setGroupingSeparator(char sep) {
      this.numberSymbols.setGroupingSeparator(sep);
      this.setNumberSymbols(this.numberSymbols);
    }

    @SuppressWarnings("serial")
    private class NumberEditor extends DefaultCellEditor {

      /**
       *
       */
      public NumberEditor() {
        super(new JTextField());
        ((JTextField) this.getComponent()).setHorizontalAlignment(SwingConstants.RIGHT);
      }

      /**
       * @see javax.swing.DefaultCellEditor#getCellEditorValue()
       */
      @Override
      public Object getCellEditorValue() {
        try {
          return NumberColumnFormat.this.formatterE.parse(((JTextField) this.getComponent()).getText());
        } catch (ParseException ex) {
          return null;
        }
      }

      /**
       * @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
       */
      @Override
      public Component getTableCellEditorComponent(final JTable table, final Object value,
                                                   final boolean isSelected,
                                                   final int row, final int column) {
        JTextField tf = ((JTextField) this.getComponent());
        tf.setBorder(new LineBorder(Color.black));
        try {
          tf.setText(NumberColumnFormat.this.formatterE.format(value));
        } catch (Exception e) {
          tf.setText("");
        }
        return tf;
      }

      /**
       * @see javax.swing.DefaultCellEditor#stopCellEditing()
       */
      @Override
      public boolean stopCellEditing() {
        String value = ((JTextField) this.getComponent()).getText();
        if (!value.equals("")) {
          try {
            NumberColumnFormat.this.formatterE.parse(value);
          } catch (ParseException e) {
            ((JComponent) this.getComponent()).setBorder(new LineBorder(Color.red));
            return false;
          }
        }
        return super.stopCellEditing();
      }
    }

    @SuppressWarnings("serial")
    private class NumberRenderer extends DefaultTableCellRenderer {

      /**
       *
       */
      public NumberRenderer() {
        super();
        this.setHorizontalAlignment(SwingConstants.RIGHT);
      }

      /**
       * @see javax.swing.table.DefaultTableCellRenderer#setValue(java.lang.Object)
       */
      @Override
      public void setValue(Object value) {
        this.setText((value == null) ? "" : NumberColumnFormat.this.formatterR.format(value));
      }
    }
  }

//  private class OkButtonActionListener implements ActionListener {
//
//    /**
//     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
//     */
//    @Override
//    public void actionPerformed(ActionEvent e) {
//      List<Float> weights = new ArrayList<Float>();
//      List<Boolean> isNegs = new ArrayList<Boolean>();
//      AggregationMethod method = AggregationMethod.valueOf((String)
//          DialogSetIndexationProperties.this.comboBoxAggregation.getSelectedItem());
//      if (method == AggregationMethod.GEOMETRIC) {
//        int answer = JOptionPane.showConfirmDialog(DialogSetIndexationProperties.this.getParent(),
//            "You choose the geometric  aggregation method.\n"
//                + "This method is only suitable for positive indicator values.\n"
//                + "Normalize your dataset in case of negative values prior geometric indexation.\n" + "Continue?", "", JOptionPane.YES_NO_OPTION,
//            JOptionPane.INFORMATION_MESSAGE);
//        if (answer == JOptionPane.NO_OPTION)
//          return;
//
//      }
//      DialogSetIndexationProperties.this.indexation.setMethod(method);
//      int rowCount = DialogSetIndexationProperties.this.jTable.getModel().getRowCount();
//      for (int i = 0; i < rowCount; i++) {
//        CIndexation.Weighting w = indexation.getWeightings().get(i);
//        float weight = (((Double) DialogSetIndexationProperties.this.jTable.getValueAt(i, 1)).floatValue()) / 100;
//        boolean isNeg = (Boolean) DialogSetIndexationProperties.this.jTable.getValueAt(i, 2);
//        w.setWeight(weight);
//        w.setNegative(isNeg);
//      }
//      try {
//        IGisFileContainer outputFile = DialogSetIndexationProperties.this.indexation.getOutputFile();
//        if (outputFile != null) {
////          outputFile.getGisFileInformationStorage().refreshConnectionFileChanger(DialogSetIndexationProperties.this.indexation);
//          outputFile.getGisFileInformationStorage().refreshPluginExec(indexation);
//        }
//        DialogSetIndexationProperties.this.setVisible(false);
//      } catch (IOException e1) {
//        JOptionPane.showMessageDialog(null, e.toString());
//        e1.printStackTrace();
//      }
//    }
//  }

  @SuppressWarnings("serial")
  public class PropertiesTableModel extends DefaultTableModel {
    @SuppressWarnings("rawtypes")
    Class[] types = new Class[]
        {java.lang.String.class, Double.class, Boolean.class};
    boolean[] canEdit = new boolean[]
        {false, true, true};

    @SuppressWarnings("rawtypes")
    public PropertiesTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    @SuppressWarnings(
        {"unchecked", "rawtypes"})
    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0)
        return false;
      else
        return true;
    }
  }

  @SuppressWarnings("serial")
  class SpinnerEditor extends AbstractCellEditor implements TableCellEditor {
    final JSpinner spinner = new JSpinner();
    SpinnerNumberModel spinnerNumberModel;
    JSpinner.NumberEditor editor;

    /**
     *
     */
    public SpinnerEditor() {
      this.spinnerNumberModel = new SpinnerNumberModel(0.0, 0.0, 100.0, 0.5);
      this.spinner.setModel(this.spinnerNumberModel);
      this.editor = new JSpinner.NumberEditor(this.spinner, "0.##");
      this.spinner.setEditor(this.editor);
    }

    /**
     * @see javax.swing.CellEditor#getCellEditorValue()
     */
    public Object getCellEditorValue() {
      return this.spinner.getValue();
    }

    /**
     * @return
     */
    public JSpinner getSpinner() {
      return this.spinner;
    }

    /**
     * @see javax.swing.table.TableCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
     */
    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
      this.spinner.setValue(value);
      return this.spinner;
    }

    /**
     * @see javax.swing.AbstractCellEditor#isCellEditable(java.util.EventObject)
     */
    public boolean isCellEditable(EventObject evt) {
      if (evt instanceof MouseEvent) {
        return ((MouseEvent) evt).getClickCount() >= 2;
      }
      return true;
    }
  }
}
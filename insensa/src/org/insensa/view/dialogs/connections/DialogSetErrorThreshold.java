/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.connections.ErrorIndexation;
import org.insensa.connections.CIndexation;
import org.insensa.view.dialogs.helper.NumberColumnFormat;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;


public class DialogSetErrorThreshold extends javax.swing.JDialog {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private javax.swing.JButton okButton;
  private javax.swing.JButton cancelButton;
  private javax.swing.JLabel statusLabel;
  private javax.swing.JScrollPane jScrollPaneInfoText;
  private javax.swing.JScrollPane jScrollPaneTable;
  private javax.swing.JTable jTable;
  private javax.swing.JEditorPane jEditorPane;
  private ErrorIndexation prioritization;
  private javax.swing.JLabel warningLabel;
  // private JTextField fileNameTextField;
  private JSeparator jSep1;
  private List<IGisFileContainer> fileList = new ArrayList<IGisFileContainer>();
  private List<ErrorIndexation.Threshold> rewPenList = new ArrayList<ErrorIndexation.Threshold>();

  /**
   * @param targetPrio
   * @param list
   */
  public DialogSetErrorThreshold(ErrorIndexation targetPrio, List<ErrorIndexation.Threshold> list) {
    this.prioritization = targetPrio;
    this.fileList.addAll(this.prioritization.getSourceFileList());
    this.rewPenList.addAll(list);
    int sizeDiff = this.fileList.size() - this.rewPenList.size();
    if (sizeDiff > 0)
      for (int i = 0; i < sizeDiff; i++) {
        this.rewPenList.add(new CIndexation.Threshold());
      }
    super.setTitle("Set Prioritization");
    this.setSize(600, 400);
    this.initComponents();
    this.initTable();
  }

  /**
   * @return
   */
  public List<ErrorIndexation.Threshold> getRewPenList() {
    return this.rewPenList;
  }

  // private Model model;

  @SuppressWarnings(
      {"unchecked", "serial"})
  private void initComponents() {

    this.jScrollPaneInfoText = new javax.swing.JScrollPane();
    this.jEditorPane = new javax.swing.JEditorPane();
    this.statusLabel = new javax.swing.JLabel();
    this.okButton = new javax.swing.JButton();
    this.cancelButton = new javax.swing.JButton();
    this.jScrollPaneTable = new javax.swing.JScrollPane();
    this.jTable = new javax.swing.JTable();
    this.warningLabel = new JLabel("Warning: The previous \"negative weight\" setting will be ignored");
    Font newFont = new Font(null, Font.PLAIN, 12);

    this.warningLabel.setFont(newFont);
    this.warningLabel.setForeground(Color.red);

    this.jSep1 = new JSeparator();
    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    this.setName("Form");

    this.jScrollPaneInfoText.setName("jScrollPane1");
    this.jEditorPane.setContentType("text/html");
    this.jEditorPane.setEditable(false);
    this.jEditorPane.setName("jTextArea1");
    this.jEditorPane.setText("<html>\n<body>\n&nbsp;&nbsp;&nbsp; <span style=\"font-weight: bold;\">" + "Set Properties"
        + "</span><br>\n&nbsp;&nbsp;&nbsp;" + "set properties for Prioritization ID:" + this.prioritization.getOrderId() + "\n "
        + "&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <br>\n</body>\n</html>");
    this.jScrollPaneInfoText.setViewportView(this.jEditorPane);

    this.statusLabel.setText("");
    this.statusLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    this.statusLabel.setName("jLabel1");

    this.okButton.setText("Ok");
    this.okButton.setName("jButton1");
    this.okButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        int rowCnt = DialogSetErrorThreshold.this.jTable.getModel().getRowCount();
        for (int i = 0; i < rowCnt; i++) {
          // Object obj = jTable.getValueAt(i, 0);
          ErrorIndexation.Threshold rewPen = DialogSetErrorThreshold.this.rewPenList.get(i);
          if (((Boolean) DialogSetErrorThreshold.this.jTable.getValueAt(i, 0)) == false) {
            rewPen.setActivate(false);
          } else {
            rewPen.setActivate(true);
            rewPen.setCondition((String) DialogSetErrorThreshold.this.jTable.getValueAt(i, 2));
            rewPen.setValuePerc(((Number) DialogSetErrorThreshold.this.jTable.getValueAt(i, 3)).floatValue());
            if (DialogSetErrorThreshold.this.jTable.getValueAt(i, 4).equals("%"))
              rewPen.setMode(CIndexation.Mode.percentage);
            else
              rewPen.setMode(CIndexation.Mode.value);
            if (DialogSetErrorThreshold.this.jTable.getValueAt(i, 5).equals(CIndexation.Assignment.max.toString()))
              rewPen.setAssignment(CIndexation.Assignment.max);
            else
              rewPen.setAssignment(CIndexation.Assignment.min);
          }
        }
        DialogSetErrorThreshold.this.setVisible(false);
      }
    });

    this.cancelButton.setText("Cancel");
    this.cancelButton.setName("jButton2");
    this.cancelButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent e) {
        DialogSetErrorThreshold.this.setVisible(false);
      }
    });
    this.jScrollPaneTable.setName("jScrollPane2");

    this.jScrollPaneTable.setViewportView(this.jTable);
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
    this.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout
        .createParallelGroup(Alignment.LEADING)
        .addComponent(this.jScrollPaneInfoText, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
        // NEW
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.warningLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
        // .addComponent(fileNameTextField)
        // .addContainerGap())

        .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep1).addContainerGap())
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                .addContainerGap())
        // NEW-END
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.okButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.cancelButton, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE).addGap(280, 280, 280))
        .addComponent(this.statusLabel, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)

    );
    layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(
        GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup()
            .addComponent(this.jScrollPaneInfoText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            // .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGap(20)
            // .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
            .addComponent(this.warningLabel)
            // .addComponent(fileNameTextField))
            .addGap(20)
            .addComponent(this.jSep1)
            .addGap(20)
            // .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.okButton).addComponent(this.cancelButton))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.statusLabel, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)));

  }

  private void initTable() {
    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    for (int i = 0; i < this.fileList.size(); i++) {
      if (this.rewPenList.get(i).getActivate() == false) {
        row = new Vector<Object>();
        row.add(false);
        row.add(this.fileList.get(i).getName());
        row.add("<");
        row.add(0.0);
        row.add("%");
        row.add(CIndexation.Assignment.max.toString());
        dataVector.add(row);
      } else {
        row = new Vector<Object>();
        row.add(true);
        row.add(this.fileList.get(i).getName());
        row.add(this.rewPenList.get(i).getCondition());
        row.add(this.rewPenList.get(i).getValuePerc());
        if (this.rewPenList.get(i).getMode() == CIndexation.Mode.percentage)
          row.add("%");
        else
          row.add("value");
        row.add(this.rewPenList.get(i).getAssignment().toString());
        dataVector.add(row);
      }
    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("Enable");
    titleVec.add("File");
    titleVec.add("Condition");
    titleVec.add("Condition Value");
    titleVec.add("Mode");
    titleVec.add("Assignment");

    this.jTable.setModel(new PropertiesTableModel(dataVector, titleVec));
    String[] conditionString =
        {"<", ">", "<=", ">="};
    String[] valueString =
        {"value", "%"};
    String[] minMaxString =
        {"min", "max"};

    // jTable.getColumnModel().getColumn(2).setCellEditor(new
    // ComboBoxEditor(conditionString));
    this.jTable.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox(conditionString)));
    this.jTable.getColumnModel().getColumn(2).setCellRenderer(new CenterTextRenderer());

    NumberColumnFormat numberColumn = new NumberColumnFormat("##.##", "##.##");
    this.jTable.getColumnModel().getColumn(3).setCellEditor(numberColumn.getEditor());
    this.jTable.getColumnModel().getColumn(3).setCellRenderer(numberColumn.getRenderer());

    // jTable.getColumnModel().getColumn(4).setCellEditor(new
    // ComboBoxEditor(valueString));
    this.jTable.getColumnModel().getColumn(4).setCellEditor(new DefaultCellEditor(new JComboBox(valueString)));
    // jTable.getColumnModel().getColumn(5).setCellEditor(new
    // ComboBoxEditor(minMaxString));
    this.jTable.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(new JComboBox(minMaxString)));

    this.jTable.setRowHeight(26);
    this.jTable.setName("jTable1");
  }

  @SuppressWarnings("serial")
  class CenterTextRenderer extends DefaultTableCellRenderer {

    /**
     *
     */
    public CenterTextRenderer() {
      super();
      this.setHorizontalAlignment(SwingConstants.CENTER);
    }
  }

  public class PropertiesTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -4828648138963193886L;
    Class[] types = new Class[]
        {Boolean.class, String.class, String.class, Double.class, String.class, String.class};
    @SuppressWarnings("unused")
    boolean[] canEdit = new boolean[]
        {false, true, true};

    /**
     * @param data
     * @param columnNames
     */
    public PropertiesTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    /**
     * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
     */
    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    /**
     * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 1)
        return false;
      else
        return true;
    }
  }

}
package org.insensa.view.dialogs.connections;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.extensions.IPluginExec;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.infoConnections.IInfoConnectionSetting;
import org.insensa.view.dialogs.infoReader.IInfoReaderSetting;
import org.insensa.view.dialogs.options.IOptionSetting;
import org.insensa.view.extensions.ViewChangeType;

public abstract class AbstractPluginSettings
    extends SettingsDialog
    implements IOptionSetting,
    IInfoReaderSetting,
    IConnectionSetting,
    IInfoConnectionSetting {


  @Override
  public void setVisible(boolean b) {
    super.setVisible(b);
    if (!b) {
      dispose();
    }
  }

  @Override
  public void setPluginExec(IPluginExec pluginExec) {
    if (pluginExec instanceof InfoReader) {
      setInfoReader((InfoReader) pluginExec);
    } else if (pluginExec instanceof OptionFileChanger) {
      setOption((OptionFileChanger) pluginExec);
    } else if (pluginExec instanceof ConnectionFileChanger) {
      setConnection((ConnectionFileChanger) pluginExec);
    } else if (pluginExec instanceof InfoConnection) {
      setInfoConnection((InfoConnection) pluginExec);
    }
  }

  @Override
  public void setOption(OptionFileChanger option) {
  }

  @Override
  public void setConnection(ConnectionFileChanger connection) {
  }

  @Override
  public void setInfoConnection(InfoConnection infoConnection) {

  }

  @Override
  public void setInfoReader(InfoReader infoReader) {
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.connections.CIndexation;
import org.insensa.connections.CIndexation.Assignment;
import org.insensa.connections.CIndexation.Mode;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.helper.NumberColumnFormat;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;


public class DialogSetThreshold extends SettingsDialog {

  public static final String HELP_ID = "threshold_settings_index";
  private static final long serialVersionUID = 1L;
  private javax.swing.JScrollPane jScrollPaneTable;
  private javax.swing.JTable jTable;
  private CIndexation indexation;
  private javax.swing.JLabel warningLabel;
  private JSeparator jSep1;
  private List<IGisFileContainer> fileList = new ArrayList<>();
  private ThresholdMode mode;
  private Map<String, CIndexation.Weighting> weightingMap = new LinkedHashMap<>();

  public DialogSetThreshold(CIndexation targetPrio, ThresholdMode mode) {
    this.mode = mode;
    this.indexation = targetPrio;
    this.fileList.addAll(this.indexation.getSourceFileList());
    this.weightingMap.putAll(this.indexation.getFileWeightingMap());
    super.setTitle("Set Indexation Threshold");
    this.setSize(600, 400);
    super.initComponents(this.initComponents());
    super.getButtonOk().addActionListener(new OkButtonListener());
    getButtonCancel().addActionListener(e -> DialogSetThreshold.this.setVisible(false));
    super.setHeadTitle("Set Indexation Threshold");
    this.initTable();
  }

  protected CIndexation.Weighting getWeighting(IGisFileContainer container) {
    return weightingMap.get(container.getPathRelativeToProject() + container.getOutputFileName());
  }

  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.jScrollPaneTable = new javax.swing.JScrollPane();
    this.jTable = new javax.swing.JTable();

    this.warningLabel = new JLabel("Warning: Inverse settings will not be considered. "
        + "Conditions must be formulated accordingly.");
    Font newFont = new Font(null, Font.PLAIN, 12);

    this.warningLabel.setFont(newFont);
    this.warningLabel.setForeground(Color.red);

    this.jSep1 = new JSeparator();

    this.jScrollPaneTable.setViewportView(this.jTable);
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout
        .createParallelGroup(Alignment.LEADING)
        // NEW
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.warningLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))
        .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep1).addContainerGap())
        .addGroup(
            layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                .addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(
        GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup().addComponent(this.warningLabel).addGap(20).addComponent(this.jSep1).addGap(20)
            .addComponent(this.jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)));
    return panel;
  }

  private void initTable() {
    Vector<Vector<Object>> dataVector = new Vector<>();
    weightingMap.forEach((s, weighting) -> {
      CIndexation.Threshold threshold;
      switch (mode) {
        case REWARD:
          threshold = weighting.getReward();
          break;
        case PENALTY:
          threshold = weighting.getPenalty();
          break;
        default:
          throw new RuntimeException("Mode Error");
      }
      if (!threshold.getActivate()) {
        Vector<Object> row = new Vector<>();
        row.add(false);
        row.add(s);
        row.add("<");
        row.add(0.0);
        row.add("%");
        row.add(Assignment.max.toString());
        dataVector.add(row);
      } else {
        Vector<Object> row = new Vector<>();
        row.add(true);
        row.add(s);
        row.add(threshold.getCondition());
        row.add(threshold.getValuePerc());
        if (threshold.getMode() == Mode.percentage) {
          row.add("%");
        } else {
          row.add("value");
        }
        row.add(threshold.getAssignment().toString());
        dataVector.add(row);
      }
    });
    Vector<String> titleVec = new Vector<>();
    titleVec.add("Enable");
    titleVec.add("File");
    titleVec.add("Condition");
    titleVec.add("Condition Value");
    titleVec.add("Mode");
    titleVec.add("Assignment");

    this.jTable.setModel(new PropertiesTableModel(dataVector, titleVec));
    String[] conditionString =
        {"<", ">", "<=", ">="};
    String[] valueString =
        {"value", "%"};
    String[] minMaxString =
        {"min", "max"};

    this.jTable.getColumnModel().getColumn(2)
        .setCellEditor(new DefaultCellEditor(new JComboBox(conditionString)));
    this.jTable.getColumnModel().getColumn(2)
        .setCellRenderer(new CenterTextRenderer());

    NumberColumnFormat numberColumn = new NumberColumnFormat("##.##", "##.##");
    this.jTable.getColumnModel().getColumn(3)
        .setCellEditor(numberColumn.getEditor());
    this.jTable.getColumnModel().getColumn(3)
        .setCellRenderer(numberColumn.getRenderer());

    this.jTable.getColumnModel().getColumn(4)
        .setCellEditor(new DefaultCellEditor(new JComboBox(valueString)));
    this.jTable.getColumnModel().getColumn(5)
        .setCellEditor(new DefaultCellEditor(new JComboBox(minMaxString)));

    this.jTable.setRowHeight(26);
    this.jTable.setName("jTable1");
  }

  enum ThresholdMode {
    PENALTY,
    REWARD
  }

  @SuppressWarnings("serial")
  class CenterTextRenderer extends DefaultTableCellRenderer {

    public CenterTextRenderer() {
      super();
      this.setHorizontalAlignment(SwingConstants.CENTER);
    }
  }

  private class OkButtonListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
      int rowCnt = DialogSetThreshold.this.jTable.getModel().getRowCount();
      for (int i = 0; i < rowCnt; i++) {
        CIndexation.Threshold rewPen;
        switch (mode) {
          case PENALTY:
            rewPen = getWeighting(fileList.get(i)).getPenalty();
            break;
          case REWARD:
            rewPen = getWeighting(fileList.get(i)).getReward();
            break;
          default:
            throw new RuntimeException("Mode not set");
        }

        if (!((Boolean) DialogSetThreshold.this.jTable.getValueAt(i, 0))) {
          rewPen.setActivate(false);
        } else {
          rewPen.setActivate(true);
          rewPen.setCondition((String) DialogSetThreshold.this.jTable.getValueAt(i, 2));
          rewPen.setValuePerc(((Number) DialogSetThreshold.this.jTable.getValueAt(i, 3)).floatValue());
          if (DialogSetThreshold.this.jTable.getValueAt(i, 4).equals("%"))
            rewPen.setMode(Mode.percentage);
          else {
            rewPen.setMode(Mode.value);
          }
          if (DialogSetThreshold.this.jTable.getValueAt(i, 5)
              .equals(Assignment.max.toString())) {
            rewPen.setAssignment(Assignment.max);
          } else {
            rewPen.setAssignment(Assignment.min);
          }
        }
      }
      DialogSetThreshold.this.setVisible(false);
    }
  }

  public class PropertiesTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -640240456525907001L;
    Class[] types = new Class[]
        {Boolean.class, String.class, String.class, Double.class, String.class, String.class};
    @SuppressWarnings("unused")
    boolean[] canEdit = new boolean[]
        {false, true, true};

    public PropertiesTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 1) {
        return false;
      } else {
        return true;
      }
    }
  }

}
/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;

import javax.swing.JPanel;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.infoconnections.PearsonCorrelation;
import org.insensa.infoconnections.SpearmanRankCorrelation;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.AbstractPluginSettingsDialog;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.extensions.ViewChangeType;


public class DialogSetCovAndCorProperties extends AbstractPluginSettingsDialog {


  public static final String HELP_ID = "correlation_settings";
  private static final long serialVersionUID = -4421494221129163665L;
  private javax.swing.JCheckBox checkboxPartialCor;
  private javax.swing.JCheckBox checkboxTTest;
  private javax.swing.JComboBox comboBoxNodataOption;

  private javax.swing.JLabel labelNodataOption;
  private PearsonCorrelation covAndCor = null;
  private SpearmanRankCorrelation spearRank = null;

  public DialogSetCovAndCorProperties() {

  }

  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  /**
   * @return
   */
  private JPanel initComponents() {
    if (this.covAndCor == null && this.spearRank == null) {
      throw new RuntimeException("You have to set the ConnectionFileChanger");
    }
    super.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);

    JPanel panel = new JPanel();
    this.comboBoxNodataOption = new javax.swing.JComboBox();
    this.labelNodataOption = new javax.swing.JLabel();
    this.checkboxPartialCor = new javax.swing.JCheckBox();
    this.checkboxTTest = new javax.swing.JCheckBox();

    this.comboBoxNodataOption.setModel(new javax.swing.DefaultComboBoxModel(new String[]
        {"NODATA_SKIP", "MEAN_INITIATION", "ZERO_INITIATION"}));
    this.labelNodataOption.setText("NoData Value Option:");
    this.checkboxPartialCor.setText("Partial Correlation");
    this.checkboxTTest.setText("t-test");

    if (this.covAndCor != null) {
      this.comboBoxNodataOption.setSelectedIndex(this.covAndCor.getOption() - 1);
      this.checkboxPartialCor.setSelected(this.covAndCor.getPartial());
      this.checkboxTTest.setSelected(this.covAndCor.getT_test());
    } else if (this.spearRank != null) {
      this.comboBoxNodataOption.setSelectedIndex(this.spearRank.getOption() - 1);
      this.checkboxPartialCor.setSelected(this.spearRank.getPartial());
      this.checkboxTTest.setSelected(this.spearRank.getT_test());
    }

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(this.checkboxTTest)
                        .addComponent(this.checkboxPartialCor)
                        .addComponent(this.labelNodataOption)
                        .addComponent(this.comboBoxNodataOption, javax.swing.GroupLayout.PREFERRED_SIZE,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(this.labelNodataOption)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.comboBoxNodataOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18).addComponent(this.checkboxPartialCor)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.checkboxTTest)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

    this.pack();
    return panel;
  }

  @Override
  public void setInfoConnection(InfoConnection infoConnection) {
    if (infoConnection instanceof PearsonCorrelation)
      this.covAndCor = (PearsonCorrelation) infoConnection;
    if (infoConnection instanceof SpearmanRankCorrelation)
      this.spearRank = (SpearmanRankCorrelation) infoConnection;
  }

  @Override
  public void onButtonOkPressed() {
    try {
      if (DialogSetCovAndCorProperties.this.covAndCor != null) {
        DialogSetCovAndCorProperties.this.covAndCor.setOption(DialogSetCovAndCorProperties.this.comboBoxNodataOption.getSelectedIndex() + 1);
        DialogSetCovAndCorProperties.this.covAndCor.setPartial(DialogSetCovAndCorProperties.this.checkboxPartialCor.isSelected());
        DialogSetCovAndCorProperties.this.covAndCor.setT_test(DialogSetCovAndCorProperties.this.checkboxTTest.isSelected());
        DialogSetCovAndCorProperties.this.covAndCor.getParentFileContainer().getGisFileInformationStorage()
            .refreshPluginExec(DialogSetCovAndCorProperties.this.covAndCor);
      } else if (DialogSetCovAndCorProperties.this.spearRank != null) {
        DialogSetCovAndCorProperties.this.spearRank.setOption(DialogSetCovAndCorProperties.this.comboBoxNodataOption.getSelectedIndex() + 1);
        DialogSetCovAndCorProperties.this.spearRank.setPartial(DialogSetCovAndCorProperties.this.checkboxPartialCor.isSelected());
        DialogSetCovAndCorProperties.this.spearRank.setT_test(DialogSetCovAndCorProperties.this.checkboxTTest.isSelected());
        DialogSetCovAndCorProperties.this.spearRank.getParentFileContainer().getGisFileInformationStorage()
            .refreshPluginExec(DialogSetCovAndCorProperties.this.spearRank);
      }
      DialogSetCovAndCorProperties.this.setVisible(false);
    } catch (IOException e1) {
      e1.printStackTrace();
    }
  }

  @Override
  public String getDialogTitle() {
    return "Set CovAndCor Properties";
  }

  @Override
  public JPanel initComponent() {
    return initComponents();
  }

//  private class OkButtonActionListener implements ActionListener {
//    @Override
//    public void actionPerformed(ActionEvent e) {
//      try {
//        if (DialogSetCovAndCorProperties.this.covAndCor != null) {
//          DialogSetCovAndCorProperties.this.covAndCor.setOption(DialogSetCovAndCorProperties.this.comboBoxNodataOption.getSelectedIndex() + 1);
//          DialogSetCovAndCorProperties.this.covAndCor.setPartial(DialogSetCovAndCorProperties.this.checkboxPartialCor.isSelected());
//          DialogSetCovAndCorProperties.this.covAndCor.setT_test(DialogSetCovAndCorProperties.this.checkboxTTest.isSelected());
//          DialogSetCovAndCorProperties.this.covAndCor.getParentFileContainer().getGisFileInformationStorage()
//              .refreshPluginExec(DialogSetCovAndCorProperties.this.covAndCor);
//        } else if (DialogSetCovAndCorProperties.this.spearRank != null) {
//          DialogSetCovAndCorProperties.this.spearRank.setOption(DialogSetCovAndCorProperties.this.comboBoxNodataOption.getSelectedIndex() + 1);
//          DialogSetCovAndCorProperties.this.spearRank.setPartial(DialogSetCovAndCorProperties.this.checkboxPartialCor.isSelected());
//          DialogSetCovAndCorProperties.this.spearRank.setT_test(DialogSetCovAndCorProperties.this.checkboxTTest.isSelected());
//          DialogSetCovAndCorProperties.this.spearRank.getParentFileContainer().getGisFileInformationStorage()
//              .refreshPluginExec(DialogSetCovAndCorProperties.this.spearRank);
//        }
//        DialogSetCovAndCorProperties.this.setVisible(false);
//      } catch (IOException e1) {
//        e1.printStackTrace();
//      }
//    }
//  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CSubtraction;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.extensions.ViewChangeType;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;


public class DialogSetSubtractionProperties extends AbstractPluginSettings {
  public static final String HELP_ID = "subtraction_settings";
  private static final long serialVersionUID = -3140341551579173083L;
  private CSubtraction subtraction = null;
  private List<IGisFileContainer> fileList = new ArrayList<IGisFileContainer>();
  private RasterFileContainer minuend = null;
  private boolean relative = false;
  private boolean absolute = false;
  private boolean ignoreNodata = false;
  private String divByZeroOption = "";
  private boolean wasEnabled = true;
  private javax.swing.ButtonGroup buttonGroup1;
  private javax.swing.JCheckBox checkBoxIgnoreNDV;
  private javax.swing.JCheckBox checkboxRelValues;
  private javax.swing.JCheckBox checkBoxAbsValues;
  private javax.swing.JTable jTable1;
  private javax.swing.JLabel labelValue;
  private javax.swing.JPanel panelInternal;
  private javax.swing.JPanel panelDivByZero;
  private javax.swing.JPanel panelOptions;
  private javax.swing.JPanel panelTable;
  private javax.swing.JRadioButton radioSetDummy;
  private javax.swing.JRadioButton radioSetMaxPrec;
  private javax.swing.JRadioButton radioSetNDV;
  private javax.swing.JScrollPane scrollPaneTable;
  private javax.swing.JTextField textFieldValue;

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  /**
   * @return
   */
  private JPanel initComponents() {
    this.buttonGroup1 = new javax.swing.ButtonGroup();
    this.panelInternal = new javax.swing.JPanel();
    this.panelTable = new javax.swing.JPanel();
    this.scrollPaneTable = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();
    this.panelOptions = new javax.swing.JPanel();
    this.checkboxRelValues = new javax.swing.JCheckBox();
    this.checkBoxAbsValues = new javax.swing.JCheckBox();
    this.checkBoxIgnoreNDV = new javax.swing.JCheckBox();
    this.panelDivByZero = new javax.swing.JPanel();
    this.radioSetNDV = new javax.swing.JRadioButton();
    this.radioSetMaxPrec = new javax.swing.JRadioButton();
    this.radioSetDummy = new javax.swing.JRadioButton();
    this.labelValue = new javax.swing.JLabel();
    this.textFieldValue = new javax.swing.JTextField();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    this.initPanelInternal();
    this.pack();
    return this.panelInternal;
  }

  private void initData() {
    switch (this.subtraction.getDivByZeroOption()) {
      case CSubtraction.SET_NODATA:
        this.radioSetNDV.doClick();
        break;
      case CSubtraction.SET_DUMMY:
        this.radioSetDummy.doClick();
        break;
      case CSubtraction.SET_MAX_PRECISION:
        this.radioSetMaxPrec.doClick();
      default:
        break;
    }

    if (this.subtraction.getIgnoreNodata() == true)
      this.checkBoxIgnoreNDV.doClick();

    if (this.subtraction.getSourceFileList().size() > 2)
      this.checkboxRelValues.setEnabled(false);
    else
      this.checkBoxIgnoreNDV.setEnabled(false);

    if (this.subtraction.getRelative() == false)
      this.setPanelDivByZeroOptionEnabled(false);
    else
      this.checkboxRelValues.doClick();
    if (this.subtraction.getAbsolute() == true)
      this.checkBoxAbsValues.doClick();
  }

  private void initPabelDivByZero() {
    this.buttonGroup1.add(this.radioSetDummy);
    this.buttonGroup1.add(this.radioSetMaxPrec);
    this.buttonGroup1.add(this.radioSetNDV);

    this.panelDivByZero.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Division By Zero",
        javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    this.radioSetNDV.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.radioSetNDV.setText("Set NoDataValue");
    this.radioSetNDV.setActionCommand("setNDV");
    this.radioSetNDV.addActionListener(new SetDivByZeroStateActionListener());

    this.radioSetMaxPrec.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.radioSetMaxPrec.setText("Set By Maximal Precision");
    this.radioSetMaxPrec.setActionCommand("setMaxPrex");
    this.radioSetMaxPrec.addActionListener(new SetDivByZeroStateActionListener());

    this.radioSetDummy.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.radioSetDummy.setText("Set Dummy Value");
    this.radioSetDummy.setActionCommand("setDummy");
    this.radioSetDummy.addActionListener(new SetDivByZeroStateActionListener());

    this.labelValue.setText("Value:");

    javax.swing.GroupLayout panelDivByZeroLayout = new javax.swing.GroupLayout(this.panelDivByZero);
    this.panelDivByZero.setLayout(panelDivByZeroLayout);
    panelDivByZeroLayout.setHorizontalGroup(panelDivByZeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelDivByZeroLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                panelDivByZeroLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        panelDivByZeroLayout
                            .createSequentialGroup()
                            .addGap(21, 21, 21)
                            .addComponent(this.labelValue)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(this.textFieldValue, javax.swing.GroupLayout.PREFERRED_SIZE, 67,
                                javax.swing.GroupLayout.PREFERRED_SIZE)).addComponent(this.radioSetNDV)
                    .addComponent(this.radioSetMaxPrec).addComponent(this.radioSetDummy)).addContainerGap(154, Short.MAX_VALUE)));
    panelDivByZeroLayout.setVerticalGroup(panelDivByZeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            panelDivByZeroLayout
                .createSequentialGroup()
                .addContainerGap()
                .addComponent(this.radioSetNDV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.radioSetMaxPrec)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.radioSetDummy)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(
                    panelDivByZeroLayout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(this.labelValue)
                        .addComponent(this.textFieldValue, javax.swing.GroupLayout.PREFERRED_SIZE,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
  }

  private void initPanelInternal() {
    this.initPanelTable();
    this.initPabelDivByZero();
    this.initPanelOptions();

    javax.swing.GroupLayout paneInternalLayout = new javax.swing.GroupLayout(this.panelInternal);
    this.panelInternal.setLayout(paneInternalLayout);
    paneInternalLayout.setHorizontalGroup(paneInternalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        paneInternalLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                paneInternalLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, true)
                    .addComponent(this.panelDivByZero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)
                    .addComponent(this.panelOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)
                    .addComponent(this.panelTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)).addContainerGap()));
    paneInternalLayout.setVerticalGroup(paneInternalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        paneInternalLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                paneInternalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(
                    paneInternalLayout
                        .createSequentialGroup()
                        .addComponent(this.panelTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(this.panelOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                            javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(this.panelDivByZero, javax.swing.GroupLayout.PREFERRED_SIZE,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))).addContainerGap()));
  }

  /**
   *
   */
  private void initPanelOptions() {
    this.panelOptions.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Options", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
        javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    this.checkboxRelValues.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkboxRelValues.setText("Relative Values ( in % )");
    this.checkboxRelValues.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          DialogSetSubtractionProperties.this.relative = true;
          DialogSetSubtractionProperties.this.setPanelDivByZeroOptionEnabled(true);
        } else {
          DialogSetSubtractionProperties.this.relative = false;
          DialogSetSubtractionProperties.this.setPanelDivByZeroOptionEnabled(false);
        }
      }
    });
    this.checkBoxIgnoreNDV.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkBoxIgnoreNDV.setText("Ingore NoDataValue");
    this.checkBoxIgnoreNDV.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
          DialogSetSubtractionProperties.this.ignoreNodata = true;
        else
          DialogSetSubtractionProperties.this.ignoreNodata = false;
      }
    });

    this.checkBoxAbsValues.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.checkBoxAbsValues.setText("Absolute Values");
    this.checkBoxAbsValues.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
          DialogSetSubtractionProperties.this.absolute = true;
        else
          DialogSetSubtractionProperties.this.absolute = false;
      }
    });

    javax.swing.GroupLayout panelOptionsLayout = new javax.swing.GroupLayout(this.panelOptions);
    this.panelOptions.setLayout(panelOptionsLayout);
    panelOptionsLayout.setHorizontalGroup(panelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelOptionsLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                panelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.checkboxRelValues)
                    .addComponent(this.checkBoxAbsValues).addComponent(this.checkBoxIgnoreNDV)).addContainerGap(170, Short.MAX_VALUE)));
    panelOptionsLayout.setVerticalGroup(panelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelOptionsLayout.createSequentialGroup().addContainerGap().addComponent(this.checkboxRelValues)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.checkBoxAbsValues)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.checkBoxIgnoreNDV)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
  }

  /**
   * Init Panel with Table.
   */
  private void initPanelTable() {

    this.initTable();
    this.scrollPaneTable.setViewportView(this.jTable1);

    javax.swing.GroupLayout panelTableLayout = new javax.swing.GroupLayout(this.panelTable);
    this.panelTable.setLayout(panelTableLayout);
    panelTableLayout.setHorizontalGroup(panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.scrollPaneTable,
        javax.swing.GroupLayout.PREFERRED_SIZE, 334, Short.MAX_VALUE));
    panelTableLayout.setVerticalGroup(panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.scrollPaneTable,
        javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE));
  }

  private void initTable() {
    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    ButtonGroup buttonGroup = new ButtonGroup();
    for (int i = 0; i < this.fileList.size(); i++) {
      JRadioButton rButton = new JRadioButton();
      rButton.addActionListener(new RadioButtonActionListener());
      row = new Vector<Object>();
      row.add(rButton);
      buttonGroup.add(rButton);
      row.add(this.fileList.get(i));
      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("Minuend");
    titleVec.add("File");

    TableModel tModel = new PropertiesTableModel(dataVector, titleVec);
    tModel.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        DialogSetSubtractionProperties.this.repaint();
      }
    });

    this.jTable1.setModel(tModel);
    for (int i = 0; i < this.jTable1.getRowCount(); i++) {
      if (this.subtraction.getMinuend() != null)
        if (this.subtraction.getMinuend().equals(this.jTable1.getValueAt(i, 1))) {
          ((AbstractButton) this.jTable1.getValueAt(i, 0)).setSelected(true);
          this.minuend = (RasterFileContainer) this.jTable1.getValueAt(i, 1);
        }
    }
    this.jTable1.getColumnModel().getColumn(0).setCellEditor(new RadioButtonEditor(new JCheckBox()));
    this.jTable1.getColumnModel().getColumn(0).setCellRenderer(new RadioButtonRenderer());
    this.jTable1.setRowHeight(26);
    this.jTable1.setName("jTable1");

  }

  //TODO: Remove logic from setter, throw somewhere else
  @Override
  public void setConnection(ConnectionFileChanger connection) {
    if (connection instanceof CSubtraction) {
      this.subtraction = (CSubtraction) connection;
      this.fileList.addAll(this.subtraction.getSourceFileList());
    } else {
      throw new RuntimeException("connection is not of type CSubtraction");
    }
  }

  private void setPanelDivByZeroOptionEnabled(boolean enable) {
    Component[] compArray = this.panelDivByZero.getComponents();
    for (Component element : compArray) {
      if (enable == false) {
        element.setEnabled(enable);
      } else {
        if (element == this.labelValue || element == this.textFieldValue) {
          if (this.wasEnabled == true)
            element.setEnabled(enable);
        } else {
          element.setEnabled(enable);
        }
      }

    }
    this.panelDivByZero.setEnabled(enable);
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    if (this.subtraction == null)
      throw new RuntimeException("You have to set a connection");
    super.setTitle("Set Subtraction Properties");
    super.initComponents(this.initComponents());
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    super.setHeadTitle("Set Subtraction Properties");
    this.initData();
    this.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  private class OkButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      if (DialogSetSubtractionProperties.this.minuend != null)
        DialogSetSubtractionProperties.this.subtraction.setMinuend(DialogSetSubtractionProperties.this.minuend);
      else {
        DialogSetSubtractionProperties.this.getLabelStatus().setText("Set Minuend File");
        return;
      }
      DialogSetSubtractionProperties.this.subtraction.setRelative(DialogSetSubtractionProperties.this.relative);
      DialogSetSubtractionProperties.this.subtraction.setAbsolute(DialogSetSubtractionProperties.this.absolute);
      DialogSetSubtractionProperties.this.subtraction.setIgnoreNodata(DialogSetSubtractionProperties.this.ignoreNodata);
      if (DialogSetSubtractionProperties.this.relative == true) {
        if (DialogSetSubtractionProperties.this.divByZeroOption.equals("setNDV"))
          DialogSetSubtractionProperties.this.subtraction.setDivByZeroOption(CSubtraction.SET_NODATA);
        else if (DialogSetSubtractionProperties.this.divByZeroOption.equals("setMaxPrex"))
          DialogSetSubtractionProperties.this.subtraction.setDivByZeroOption(CSubtraction.SET_MAX_PRECISION);
        else if (DialogSetSubtractionProperties.this.divByZeroOption.equals("setDummy")) {
          float dummyVal = 0.0F;
          String sVal = DialogSetSubtractionProperties.this.textFieldValue.getText();
          sVal = sVal.replace(",", ".");
          DialogSetSubtractionProperties.this.subtraction.setDivByZeroOption(CSubtraction.SET_DUMMY);
          try {
            dummyVal = Float.valueOf(sVal);
            DialogSetSubtractionProperties.this.subtraction.setDummyValue(dummyVal);
          } catch (Exception e1) {
            DialogSetSubtractionProperties.this.getLabelStatus().setText("Insert Valid Number");
            e1.printStackTrace();
            return;
          }

        }
      }
      try {
        IGisFileContainer outputFile = DialogSetSubtractionProperties.this.subtraction.getOutputFileContainer();
        if (outputFile != null)
          outputFile.getGisFileInformationStorage().refreshPluginExec(DialogSetSubtractionProperties.this.subtraction);
        DialogSetSubtractionProperties.this.setVisible(false);
      } catch (IOException e1) {
        DialogSetSubtractionProperties.this.getLabelStatus().setText(e1.getMessage());
        e1.printStackTrace();
      }
    }
  }

  private class PropertiesTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -4487353833927280683L;

    /**
     * @param data
     * @param columnNames
     */
    @SuppressWarnings("rawtypes")
    public PropertiesTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    /**
     * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0)
        return true;
      else
        return false;
    }
  }

  private class RadioButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      int row = DialogSetSubtractionProperties.this.jTable1.getSelectedRow();
      RasterFileContainer file = (RasterFileContainer) DialogSetSubtractionProperties.this.jTable1.getValueAt(row, 1);
      DialogSetSubtractionProperties.this.minuend = file;
    }

  }

  private class RadioButtonEditor extends DefaultCellEditor implements ItemListener {
    /**
     *
     */
    private static final long serialVersionUID = -6043787145735846016L;
    private JRadioButton button;

    /**
     * @param checkBox
     */
    public RadioButtonEditor(JCheckBox checkBox) {
      super(checkBox);
    }

    /**
     * @see javax.swing.DefaultCellEditor#getCellEditorValue()
     */
    public Object getCellEditorValue() {
      this.button.removeItemListener(this);
      return this.button;
    }

    /**
     * @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
     */
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if (value == null)
        return null;
      this.button = (JRadioButton) value;
      this.button.addItemListener(this);
      return (Component) value;
    }

    /**
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    public void itemStateChanged(ItemEvent e) {
      super.fireEditingStopped();
    }
  }

  private class RadioButtonRenderer implements TableCellRenderer {

    /**
     * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
     */
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      if (value == null)
        return null;
      return (Component) value;
    }
  }

  private class SetDivByZeroStateActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent arg0) {
      DialogSetSubtractionProperties.this.divByZeroOption = arg0.getActionCommand();
      if (DialogSetSubtractionProperties.this.divByZeroOption.equals("setDummy")) {
        DialogSetSubtractionProperties.this.labelValue.setEnabled(true);
        DialogSetSubtractionProperties.this.textFieldValue.setEnabled(true);
        DialogSetSubtractionProperties.this.textFieldValue.setText(Float.toString(DialogSetSubtractionProperties.this.subtraction.getDummyValue()));
        DialogSetSubtractionProperties.this.wasEnabled = true;
      } else {
        DialogSetSubtractionProperties.this.textFieldValue.setText(Float.toString(DialogSetSubtractionProperties.this.subtraction.getDummyValue()));
        DialogSetSubtractionProperties.this.labelValue.setEnabled(false);
        DialogSetSubtractionProperties.this.textFieldValue.setEnabled(false);
        DialogSetSubtractionProperties.this.wasEnabled = false;
      }
    }

  }

}

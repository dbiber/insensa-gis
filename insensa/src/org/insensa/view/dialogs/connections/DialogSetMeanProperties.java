/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CMean;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.extensions.ViewChangeType;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;


public class DialogSetMeanProperties extends AbstractPluginSettings {

  public static final String HELP_ID = "mean_settings";
  private static final long serialVersionUID = -7288781758251638513L;
  // private javax.swing.JButton okButton;
  // private javax.swing.JButton cancelButton;
  // private javax.swing.JLabel statusLabel;
  // private javax.swing.JScrollPane jScrollPaneInfoText;
  private javax.swing.JScrollPane jScrollPaneTable;
  private javax.swing.JTable jTable;
  // private javax.swing.JEditorPane jEditorPane;
  private JCheckBox checkboxRelative;
  private JSeparator jSep3;
  private RasterFileContainer relativeSource = null;
  private boolean relative = false;
  private JSeparator jSep1;
  private JSeparator jSep2;
  private List<IGisFileContainer> fileList = new ArrayList<IGisFileContainer>();
  private ButtonGroup buttonGroup;
  private CMean mean;

  // public DialogSetMeanProperties(CMean mean)
  // {
  //
  // }

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  private void initComponents() {
    // jScrollPaneInfoText = new JScrollPane();
    // jEditorPane = new JEditorPane();
    // statusLabel = new JLabel();
    // okButton = new JButton();
    // cancelButton = new JButton();
    this.jScrollPaneTable = new JScrollPane();
    this.jTable = new JTable();
    this.checkboxRelative = new JCheckBox("Relative Values (in %)");
    // fileNameDescriptionLabel = new JLabel("Output File Name:");
    // fileNameDescriptionLabel.setFont(new Font(null, Font.PLAIN, 12));

    // fileNameLabel = new JLabel("");

    this.jSep1 = new JSeparator();
    this.jSep2 = new JSeparator();
    this.jSep3 = new JSeparator();

    // setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    // jScrollPaneInfoText.setName("jScrollPane1");
    // jEditorPane.setContentType("text/html");
    // jEditorPane.setEditable(false);
    // jEditorPane.setContentType("text/html");
    // try
    // {
    // jEditorPane.setPage("file:html/" + "set"+ EConnection.mean.toString()
    // + ".html");
    // }
    // catch (IOException e1)
    // {
    //
    // }
    // jScrollPaneInfoText.setViewportView(jEditorPane);
    //
    // statusLabel.setText("");
    // statusLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    //
    // okButton.setText("Ok");

    if (this.mean.getRelative() == true)
      this.checkboxRelative.doClick();

    /*
     * checkboxIgnoreNodata.setToolTipText(
     * "<html><span style=\"font-weight: bold;\">If Checked:</span> Result is NoData if<br>"
     * + "&nbsp;&nbsp;all Subtrahends or minuend are NoData<br>"+
     * "<span style=\"font-weight: bold;\">If not Check:</span> Result is NoData if<br>"
     * + "&nbsp;&nbsp;any Value is NoData</html>");
     */

    // cancelButton.setText("Cancel");

    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    this.buttonGroup = new ButtonGroup();
    for (int i = 0; i < this.fileList.size(); i++) {
      JRadioButton rButton = new JRadioButton();
      rButton.addActionListener(new RadioButtonActionListener());
      row = new Vector<Object>();
      row.add(rButton);
      rButton.setEnabled(false);
      this.buttonGroup.add(rButton);
      row.add(this.fileList.get(i));
      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("Relative Source");
    titleVec.add("File");

    TableModel tModel = new PropertiesTableModel(dataVector, titleVec);
    tModel.addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(TableModelEvent e) {
        DialogSetMeanProperties.this.repaint();
      }
    });

    this.jTable.setModel(tModel);

    for (int i = 0; i < this.jTable.getRowCount(); i++) {
      if (this.mean.getRelativeSource() != null)
        if (this.mean.getRelativeSource().equals(this.jTable.getValueAt(i, 1))) {
          ((AbstractButton) this.jTable.getValueAt(i, 0)).setSelected(true);
          this.relativeSource = (RasterFileContainer) this.jTable.getValueAt(i, 1);
          // fileNameLabel.setText(relativeSource.toString());
        }
    }
    this.jTable.getColumnModel().getColumn(0).setCellEditor(new RadioButtonEditor(new JCheckBox()));
    this.jTable.getColumnModel().getColumn(0).setCellRenderer(new RadioButtonRenderer());
    this.jTable.setRowHeight(26);
    this.jTable.setName("jTable1");

    this.jScrollPaneTable.setViewportView(this.jTable);

  }

  /**
   * @return
   */
  public JPanel initGroupLayout() {
    JPanel panel = new JPanel();
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(
        // Parallel
        layout.createParallelGroup(Alignment.LEADING)
            .addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                    .addContainerGap())// End
            .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep3).addContainerGap())// End

            .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.checkboxRelative).addContainerGap())// End
            .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep2).addContainerGap())// End

    );

    layout.setVerticalGroup(
        // Parallel 1
        layout.createParallelGroup(Alignment.LEADING)
            // Seriell 1
            .addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup()

                    .addComponent(this.jScrollPaneTable, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(20).addComponent(this.jSep3)
                    .addGap(20).addComponent(this.checkboxRelative).addGap(20).addComponent(this.jSep2).addGap(20, 20, 20)

            )
        // End Seriell 1
    );// End Parallel 1
    return panel;
  }

  public void initListener() {
    this.checkboxRelative.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          DialogSetMeanProperties.this.relative = true;
          Enumeration<AbstractButton> buttonEnum = DialogSetMeanProperties.this.buttonGroup.getElements();
          while (buttonEnum.hasMoreElements()) {
            buttonEnum.nextElement().setEnabled(true);
          }
          DialogSetMeanProperties.this.jTable.repaint();
        } else {
          DialogSetMeanProperties.this.relative = false;
          Enumeration<AbstractButton> buttonEnum = DialogSetMeanProperties.this.buttonGroup.getElements();
          while (buttonEnum.hasMoreElements()) {
            buttonEnum.nextElement().setEnabled(false);
          }
          DialogSetMeanProperties.this.jTable.repaint();
        }
      }
    });
  }

  @Override
  public void setConnection(ConnectionFileChanger connection) {
    if (connection instanceof CMean)
      this.mean = (CMean) connection;
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    if (this.mean == null)
      throw new RuntimeException("You have to set the ConnectionFileChanger");
    this.fileList.addAll(this.mean.getSourceFileList());
    super.setTitle("Set Mean Properties");
    this.setSize(600, 400);
    this.initComponents();
    this.initListener();
    super.initComponents(this.initGroupLayout());
    super.setHeadTitle("Set Mean Properties");
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    super.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {
  }

  private class OkButtonActionListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
      if (DialogSetMeanProperties.this.relative == true) {
        if (DialogSetMeanProperties.this.relativeSource != null) {
          DialogSetMeanProperties.this.mean.setRelative(DialogSetMeanProperties.this.relative);
          DialogSetMeanProperties.this.mean.setRelativeSource(DialogSetMeanProperties.this.relativeSource);
        } else {
          DialogSetMeanProperties.this.getLabelStatus().setText("Set  File");
        }
      }
      try {
        IGisFileContainer outputFile = DialogSetMeanProperties.this.mean.getOutputFileContainer();
        if (outputFile != null)
          outputFile.getGisFileInformationStorage().refreshPluginExec(DialogSetMeanProperties.this.mean);
        DialogSetMeanProperties.this.setVisible(false);
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(null, e.toString());
        e1.printStackTrace();
      }
    }
  }

  public class PropertiesTableModel extends DefaultTableModel {
    private static final long serialVersionUID = -4487353833927280683L;

    public PropertiesTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0)
        return true;
      else
        return false;
    }
  }

  class RadioButtonActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      int row = DialogSetMeanProperties.this.jTable.getSelectedRow();
      RasterFileContainer file = (RasterFileContainer) DialogSetMeanProperties.this.jTable.getValueAt(row, 1);
      DialogSetMeanProperties.this.relativeSource = file;
    }
  }

  class RadioButtonEditor extends DefaultCellEditor implements ItemListener {
    /**
     *
     */
    private static final long serialVersionUID = -879641167415189715L;
    private JRadioButton button;

    public RadioButtonEditor(JCheckBox checkBox) {
      super(checkBox);
    }

    public Object getCellEditorValue() {
      this.button.removeItemListener(this);
      return this.button;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if (value == null)
        return null;
      this.button = (JRadioButton) value;
      this.button.addItemListener(this);
      return (Component) value;
    }

    public void itemStateChanged(ItemEvent e) {
      super.fireEditingStopped();
    }
  }

  class RadioButtonRenderer implements TableCellRenderer {
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      if (value == null)
        return null;
      return (Component) value;
    }
  }
}
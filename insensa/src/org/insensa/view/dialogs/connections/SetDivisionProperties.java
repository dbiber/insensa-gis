/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.connections.CDivision;
import org.insensa.extensions.IPluginExec;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.extensions.ViewChangeType;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JPanel;


public class SetDivisionProperties extends AbstractPluginSettings implements IConnectionSetting {
  private static final long serialVersionUID = -3140341551579173083L;
  private CDivision division = null;
  private List<IGisFileContainer> fileList = new ArrayList<IGisFileContainer>();
  private RasterFileContainer divisor = null;
  private RasterFileContainer dividend = null;
  private String divByZeroOption = "";
  private boolean wasEnabled = true;
  private javax.swing.ButtonGroup buttonGroup1;
  private javax.swing.JTable jTable1;
  private javax.swing.JLabel labelValue;
  private javax.swing.JPanel panelInternal;
  private javax.swing.JPanel panelDivByZero;
  // private javax.swing.JPanel panelOptions;
  private javax.swing.JPanel panelTable;
  private javax.swing.JRadioButton radioSetDummy;
  // private javax.swing.JRadioButton radioSetMaxPrec;
  private javax.swing.JRadioButton radioSetNDV;
  private javax.swing.JScrollPane scrollPaneTable;
  private javax.swing.JTextField textFieldValue;
  private javax.swing.JComboBox comboBoxDividend;
  private javax.swing.JComboBox comboBoxDivisor;
  private javax.swing.JPanel panelDividend;
  private javax.swing.JPanel panelDivisor;

  private void initComboBoxPanels() {
    this.panelDivisor = new javax.swing.JPanel();
    this.comboBoxDivisor = new javax.swing.JComboBox();
    this.panelDividend = new javax.swing.JPanel();
    this.comboBoxDividend = new javax.swing.JComboBox();

    this.panelDivisor.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Divisor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
        javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    Vector<Object> divisorVec = new Vector<Object>();
    divisorVec.addAll(this.division.getSourceFileList());

    this.comboBoxDivisor.setFont(new java.awt.Font("Dialog", 0, 12));
    this.comboBoxDivisor.setModel(new DefaultComboBoxModel(divisorVec));
    this.comboBoxDivisor.setSelectedIndex(0);

    javax.swing.GroupLayout panelDivisorLayout = new javax.swing.GroupLayout(this.panelDivisor);
    this.panelDivisor.setLayout(panelDivisorLayout);
    panelDivisorLayout.setHorizontalGroup(panelDivisorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
        this.comboBoxDivisor, 0, 358, Short.MAX_VALUE));
    panelDivisorLayout.setVerticalGroup(panelDivisorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
        this.comboBoxDivisor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE));

    this.panelDividend.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dividend", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
        javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    Vector<Object> dividendVec = new Vector<Object>();
    dividendVec.addAll(this.division.getSourceFileList());
    // dividendVec.remove(1);

    this.comboBoxDividend.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.comboBoxDividend.setModel(new DefaultComboBoxModel(dividendVec));
    this.comboBoxDividend.setSelectedIndex(1);
    javax.swing.GroupLayout panelDividendLayout = new javax.swing.GroupLayout(this.panelDividend);
    this.panelDividend.setLayout(panelDividendLayout);
    panelDividendLayout.setHorizontalGroup(panelDividendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
        this.comboBoxDividend, 0, 358, Short.MAX_VALUE));
    panelDividendLayout.setVerticalGroup(panelDividendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
        this.comboBoxDividend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
    this.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(this.panelDivisor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(this.panelDividend, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addComponent(this.panelDivisor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.panelDividend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(463, Short.MAX_VALUE)));
  }

  private JPanel initComponents() {
    this.buttonGroup1 = new javax.swing.ButtonGroup();
    this.panelInternal = new javax.swing.JPanel();
    this.panelTable = new javax.swing.JPanel();
    this.scrollPaneTable = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();
    this.panelDivByZero = new javax.swing.JPanel();
    this.radioSetNDV = new javax.swing.JRadioButton();
    this.radioSetDummy = new javax.swing.JRadioButton();
    this.labelValue = new javax.swing.JLabel();
    this.textFieldValue = new javax.swing.JTextField();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    this.initPanelInternal();
    this.pack();
    return this.panelInternal;
  }

  private void initData() {
    switch (this.division.getDivByZeroOption()) {
      case CDivision.SET_NODATA:
        this.radioSetNDV.doClick();
        break;
      case CDivision.SET_DUMMY:
        this.radioSetDummy.doClick();
        break;
      default:
        break;
    }
  }

  private void initPabelDivByZero() {
    this.buttonGroup1.add(this.radioSetDummy);
    this.buttonGroup1.add(this.radioSetNDV);

    this.panelDivByZero.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Division By Zero",
        javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION,
        new java.awt.Font("Dialog", 0, 12))); // NOI18N

    this.radioSetNDV.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.radioSetNDV.setText("Set NoDataValue");
    this.radioSetNDV.setActionCommand("setNDV");
    this.radioSetNDV.addActionListener(new SetDivByZeroStateActionListener());

    this.radioSetDummy.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    this.radioSetDummy.setText("Set Dummy Value");
    this.radioSetDummy.setActionCommand("setDummy");
    this.radioSetDummy.addActionListener(new SetDivByZeroStateActionListener());

    this.labelValue.setText("Value:");

    javax.swing.GroupLayout panelDivByZeroLayout = new javax.swing.GroupLayout(this.panelDivByZero);
    this.panelDivByZero.setLayout(panelDivByZeroLayout);
    panelDivByZeroLayout.setHorizontalGroup(panelDivByZeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelDivByZeroLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                panelDivByZeroLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        panelDivByZeroLayout
                            .createSequentialGroup()
                            .addGap(21, 21, 21)
                            .addComponent(this.labelValue)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(this.textFieldValue, javax.swing.GroupLayout.PREFERRED_SIZE, 67,
                                javax.swing.GroupLayout.PREFERRED_SIZE)).addComponent(this.radioSetNDV)
                    .addComponent(this.radioSetDummy)).addContainerGap(154, Short.MAX_VALUE)));
    panelDivByZeroLayout.setVerticalGroup(panelDivByZeroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            panelDivByZeroLayout
                .createSequentialGroup()
                .addContainerGap()
                .addComponent(this.radioSetNDV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.radioSetDummy)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(
                    panelDivByZeroLayout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(this.labelValue)
                        .addComponent(this.textFieldValue, javax.swing.GroupLayout.PREFERRED_SIZE,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
  }

  private void initPanelInternal() {
    this.initComboBoxPanels();
    this.initPabelDivByZero();

    javax.swing.GroupLayout paneInternalLayout = new javax.swing.GroupLayout(this.panelInternal);
    this.panelInternal.setLayout(paneInternalLayout);
    paneInternalLayout.setHorizontalGroup(paneInternalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        paneInternalLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                paneInternalLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, true)
                    .addComponent(this.panelDividend, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(this.panelDivisor, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(this.panelDivByZero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)).addContainerGap()));
    paneInternalLayout.setVerticalGroup(paneInternalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        paneInternalLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                paneInternalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(
                    paneInternalLayout
                        .createSequentialGroup()
                        .addComponent(this.panelDividend, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(this.panelDivisor, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(this.panelDivByZero, javax.swing.GroupLayout.PREFERRED_SIZE,
                            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))).addContainerGap()));
  }

  @Override
  public void setPluginExec(IPluginExec pluginExec) {
    if (pluginExec instanceof CDivision) {
      this.division = (CDivision) pluginExec;
      this.fileList.addAll(this.division.getSourceFileList());
    } else
      throw new RuntimeException("connection is not of type CDivision");
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    if (this.division == null)
      throw new RuntimeException("You have to set a connection");
    super.setTitle("Set Division Properties");
    super.initComponents(this.initComponents());
    super.getButtonOk().addActionListener(new OkButtonActionListener());
    super.setHeadTitle("Set Division Properties");
    this.initData();
    this.setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {
  }

  private class OkButtonActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      SetDivisionProperties.this.divisor = (RasterFileContainer) SetDivisionProperties.this.comboBoxDivisor.getSelectedItem();
      SetDivisionProperties.this.dividend = (RasterFileContainer) SetDivisionProperties.this.comboBoxDividend.getSelectedItem();
      if (SetDivisionProperties.this.divisor == null) {
        SetDivisionProperties.this.getLabelStatus().setText("Set Divisor");
        return;
      }
      if (SetDivisionProperties.this.dividend == null) {
        SetDivisionProperties.this.getLabelStatus().setText("Set Dividend");
        return;
      }
      if (SetDivisionProperties.this.dividend == SetDivisionProperties.this.divisor) {
        SetDivisionProperties.this.getLabelStatus().setText("dividend is the same file as divisor");
        return;
      }

      SetDivisionProperties.this.division.setDivisor(SetDivisionProperties.this.divisor);
      SetDivisionProperties.this.division.setDividend(SetDivisionProperties.this.dividend);

      if (SetDivisionProperties.this.divByZeroOption.equals("setNDV"))
        SetDivisionProperties.this.division.setDivByZeroOption(CDivision.SET_NODATA);
      else if (SetDivisionProperties.this.divByZeroOption.equals("setDummy")) {
        float dummyVal = 0.0F;
        String sVal = SetDivisionProperties.this.textFieldValue.getText();
        sVal = sVal.replace(",", ".");
        SetDivisionProperties.this.division.setDivByZeroOption(CDivision.SET_DUMMY);
        try {
          dummyVal = Float.valueOf(sVal);
          SetDivisionProperties.this.division.setDummyValue(dummyVal);
        } catch (Exception e1) {
          SetDivisionProperties.this.getLabelStatus().setText("Insert Valid Number");
          e1.printStackTrace();
          return;
        }
      }
      try {
        IGisFileContainer outputFile = SetDivisionProperties.this.division.getOutputFileContainer();
        if (outputFile != null)
          outputFile.getGisFileInformationStorage().refreshPluginExec(SetDivisionProperties.this.division);
        SetDivisionProperties.this.setVisible(false);
      } catch (IOException e1) {
        SetDivisionProperties.this.getLabelStatus().setText(e1.getMessage());
        e1.printStackTrace();
      }
    }
  }

  private class SetDivByZeroStateActionListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent arg0) {
      SetDivisionProperties.this.divByZeroOption = arg0.getActionCommand();
      if (SetDivisionProperties.this.divByZeroOption.equals("setDummy")) {
        SetDivisionProperties.this.labelValue.setEnabled(true);
        SetDivisionProperties.this.textFieldValue.setEnabled(true);
        SetDivisionProperties.this.textFieldValue.setText(Float.toString(SetDivisionProperties.this.division.getDummyValue()));
        SetDivisionProperties.this.wasEnabled = true;
      } else {
        SetDivisionProperties.this.textFieldValue.setText(Float.toString(SetDivisionProperties.this.division.getDummyValue()));
        SetDivisionProperties.this.labelValue.setEnabled(false);
        SetDivisionProperties.this.textFieldValue.setEnabled(false);
        SetDivisionProperties.this.wasEnabled = false;
      }
    }

  }
}

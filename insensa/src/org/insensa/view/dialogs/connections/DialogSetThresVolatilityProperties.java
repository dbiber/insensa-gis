/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs.connections;

import org.insensa.IGisFileContainer;
import org.insensa.connections.CThresholdVolatility;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.extensions.ViewChangeType;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;


public class DialogSetThresVolatilityProperties extends AbstractPluginSettings {

  public static final String HELP_ID = "threshold_settings";
  private static final long serialVersionUID = -7288781758251638513L;
  private JSpinner spinnerFrom;
  private JSpinner spinnerTo;
  private JLabel labelFrom;
  private JLabel labelTo;
  private JSeparator jSep3;
  private JSeparator jSep1;

  private JSeparator jSep2;
  private CThresholdVolatility thresVol;

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return HELP_ID;
  }

  private void initComponents() {
    this.spinnerFrom = new JSpinner(new SpinnerNumberModel(0, 0, 100, 5));
    this.spinnerTo = new JSpinner(new SpinnerNumberModel(0, 0, 100, 5));

    this.labelFrom = new JLabel("From (in %)");
    this.labelTo = new JLabel("To (in %)");

    this.jSep1 = new JSeparator();
    this.jSep2 = new JSeparator();
    this.jSep3 = new JSeparator();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    this.spinnerFrom.setValue(this.thresVol.getThreshold1Perc());
    this.spinnerTo.setValue(this.thresVol.getThreshold2Perc());

  }

  /**
   * @return
   */
  public JPanel initGroupLayout() {
    JPanel panel = new JPanel();
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(
        // Parallel
        layout.createParallelGroup(Alignment.LEADING)
            // Seriell 1
            .addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(this.labelFrom, 40, 40, 40)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.spinnerFrom).addContainerGap())// End
            // Seriell 2
            .addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(this.labelTo, 40, 40, 40)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.spinnerTo).addContainerGap())// END
            // Seriell 3
            .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jSep1).addContainerGap())// End
        // End Parallel
    );

    layout.setVerticalGroup(
        // Parallel 1
        layout.createParallelGroup(Alignment.LEADING)
            // Seriell 1
            .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                // .addComponent(jScrollPaneInfoText,GroupLayout.PREFERRED_SIZE,
                // 80,GroupLayout.PREFERRED_SIZE)
                // .addGap(20)
                // Parallel 2
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.labelFrom).addComponent(this.spinnerFrom))// End
                // Parallel
                // 3
                .addGap(20)
                // Parallel 3
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.labelTo).addComponent(this.spinnerTo))// End
                // Parallel
                // 3
                .addGap(20).addComponent(this.jSep1).addGap(20, 20, 20))
        // End Seriell 1
    );// End Parallel 1
    return panel;
  }

  @Override
  public void setConnection(ConnectionFileChanger connection) {
    if (connection instanceof CThresholdVolatility)
      this.thresVol = (CThresholdVolatility) connection;
  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    if (this.thresVol == null)
      throw new RuntimeException("You have to set the ConnectionFileChanger");
    super.setTitle("Set Threshold Volatility Properties");
    this.setSize(600, 400);
    this.initComponents();
    super.initComponents(this.initGroupLayout());
    super.setHeadTitle("Set Threshold Volatility Properties");
    super.getButtonOk().addActionListener(new OkButtonListener());
    this.setVisible(true);
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  private class OkButtonListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent arg0) {
      int from = ((Integer) DialogSetThresVolatilityProperties.this.spinnerFrom.getValue());
      int to = ((Integer) DialogSetThresVolatilityProperties.this.spinnerTo.getValue());
      if (from >= to) {
        DialogSetThresVolatilityProperties.this.getLabelStatus().setText("from must be smaller then to");
        return;
      }
      try {
        DialogSetThresVolatilityProperties.this.thresVol.setThreshold1Perc(from);
        DialogSetThresVolatilityProperties.this.thresVol.setThreshold2Perc(to);
        IGisFileContainer outputFile = DialogSetThresVolatilityProperties.this.thresVol.getOutputFileContainer();
        if (outputFile != null)
          outputFile.getGisFileInformationStorage().refreshPluginExec(DialogSetThresVolatilityProperties.this.thresVol);
        DialogSetThresVolatilityProperties.this.setVisible(false);
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(null, e1.toString());
      }
    }
  }
}
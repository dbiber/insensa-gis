/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import org.gdal.gdalconst.gdalconstConstants;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JPanel;


public class DialogSetNodataValue extends SettingsDialog {
  public static final String helpID = "no_data_value";
  private static final Logger log = LoggerFactory.getLogger(DialogSetNodataValue.class);
  private static final long serialVersionUID = 2139135745940548069L;
  private final Double[] FLOAT32_FIELD = new Double[]
      {new Float(-Float.MAX_VALUE).doubleValue(), new Float(Float.MAX_VALUE).doubleValue(), new Float(Float.MIN_VALUE).doubleValue(),
          new Float(Float.MIN_NORMAL).doubleValue()};
  private final Double[] BYTE_FIELD = new Double[]
      {-128.0, 127.0, 128.0, 255.0};
  private final Double[] UINT16_FIELD = new Double[]
      {65535.0};
  private final Double[] INT16_FIELD = new Double[]
      {32767.0, -32768.0};
  private final Double[] INT32_FIELD = new Double[]
      {new Integer(Integer.MAX_VALUE).doubleValue(), new Integer(Integer.MIN_VALUE).doubleValue()};
  private javax.swing.JButton buttonTestValue;
  private javax.swing.JComboBox dropDownValues;
  private javax.swing.JLabel labelFileType;
  private javax.swing.JLabel labelNoDataValue;
  private javax.swing.JTextField textFieldFileType;
  private JPanel panelMain;
  private RasterFileContainer rasterFile;

  /**
   * @param rasterFile
   * @throws IOException
   */
  public DialogSetNodataValue(RasterFileContainer rasterFile) throws IOException {
    this.rasterFile = rasterFile;
    super.initComponents(this.initPanel(rasterFile));
    super.getButtonOk().addActionListener(new OkButtonListener());
    super.setHeadTitle("Set NoData Value");
  }

  /**
   * @param rasterFile
   * @return
   * @throws IOException
   */
  private String getFileTypeString(RasterFileContainer rasterFile) throws IOException {
    return DataTypeHelper.getFileTypeString(rasterFile.getGisFile());
  }

  // private int getFileType(RasterFileContainer rasterFile) throws
  // IOException
  // {
  // String name = rasterFile.getAbsolutePath();
  // Dataset dataset = gdal.Open(name,gdalconst.GA_Update);
  // if(dataset==null)
  // throw new
  // IOException("RasterFile:refresh: unable to create Dataset for File: "+name);
  // Driver driver = dataset.GetDriver();
  // if(driver==null)
  // throw new
  // IOException("RasterFile:refresh: unable to create Driver for File: "+name);
  // Band band = dataset.GetRasterBand(1);
  // return band.getDataType();
  // }
  //

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return helpID;
  }

  /**
   * @param rasterFile
   * @return
   * @throws IOException
   */
  private Double[] getNodataValueArray(RasterFileContainer rasterFile) throws IOException {
    int type = rasterFile.getDataType();
    if (type == gdalconstConstants.GDT_Float32)
      return this.FLOAT32_FIELD;
    else if (type == gdalconstConstants.GDT_Byte)
      return this.BYTE_FIELD;
    else if (type == gdalconstConstants.GDT_UInt16)
      return this.UINT16_FIELD;
    else if (type == gdalconstConstants.GDT_Int16)
      return this.INT16_FIELD;
    else if (type == gdalconstConstants.GDT_Int32)
      return this.INT32_FIELD;
    else
      return new Double[1];
  }

  /**
   * @param rasterFile
   * @return
   * @throws IOException
   */
  public JPanel initPanel(RasterFileContainer rasterFile) throws IOException {
    this.panelMain = new JPanel();
    this.labelFileType = new javax.swing.JLabel();
    this.textFieldFileType = new javax.swing.JTextField();
    this.labelNoDataValue = new javax.swing.JLabel();
    this.dropDownValues = new javax.swing.JComboBox();
    this.buttonTestValue = new javax.swing.JButton();

    this.labelFileType.setFont(new java.awt.Font("Dialog", 0, 12));
    this.labelFileType.setText("File Type: ");

    this.textFieldFileType.setEditable(false);
    this.textFieldFileType.setText(this.getFileTypeString(rasterFile));

    this.labelNoDataValue.setFont(new java.awt.Font("Dialog", 0, 12));
    this.labelNoDataValue.setText("New NoDataValue: ");

    this.dropDownValues.setEditable(true);
    this.dropDownValues.setFont(new java.awt.Font("Dialog", 0, 12));
    this.dropDownValues.setModel(new javax.swing.DefaultComboBoxModel(this.getNodataValueArray(rasterFile)));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.panelMain);
    this.panelMain.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(
                        layout.createSequentialGroup().addComponent(this.labelFileType)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(this.textFieldFileType, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE))
                    .addGroup(
                        layout.createSequentialGroup().addComponent(this.labelNoDataValue)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(this.dropDownValues, 0, 237, Short.MAX_VALUE))
            ).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelFileType)
                    .addComponent(this.textFieldFileType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(18, 18, 18)
            .addGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(this.labelNoDataValue)
                    .addComponent(this.dropDownValues, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)).addGap(18, 18, 18)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    return this.panelMain;
  }

  private class OkButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        Object obj = DialogSetNodataValue.this.dropDownValues.getSelectedItem();
        Double newValue = Double.valueOf(obj.toString());
        DialogSetNodataValue.this.rasterFile.writeNodataValue(newValue);
        DialogSetNodataValue.this.rasterFile.refresh();
        DialogSetNodataValue.this.setVisible(false);
      } catch (IOException e1) {
        log.error("", e1);
      }
    }

  }
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.view.View;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JProgressBar;


public class ProgressBar extends JProgressBar implements WorkerStatus {

  private static final long serialVersionUID = -7148764703186170275L;
  protected WorkerStatusList mProgress = null;
  protected View view;
  private Date date = new Date();
  private SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss:SS");

  private long time;


  public ProgressBar() {
  }

  public ProgressBar(View view) {
    this.view = view;
  }

  @Override
  public void endPause() {
    this.setIndeterminate(false);
  }

  @Override
  public void endProgress() {
    this.time = System.currentTimeMillis() - this.time;
    this.date.setTime(this.time);
    this.setString(this.getString() + " : " + this.dateFormat.format(this.date));
    if (this.mProgress != null) {
      this.mProgress.endAllProcesses();
    }
  }

  @Override
  public void errorProcess() {
    if (this.isIndeterminate())
      this.setIndeterminate(false);
    this.refreshPercentage(100.0F);
    this.setForeground(Color.red);
    if (this.mProgress != null) {
      this.mProgress.endAllProcesses();
    }
  }

  @Override
  public void errorProcess(String message) {
    if (this.isIndeterminate())
      this.setIndeterminate(false);

    this.refreshPercentage(100.0F);
    this.setForeground(Color.red);
    this.setString(message);
    this.setStringPainted(true);

    if (this.mProgress != null) {
      this.mProgress.endAllProcesses();
    }
  }

  @Override
  public void errorProcess(Throwable e) {
    if (this.isIndeterminate())
      this.setIndeterminate(false);

    e.printStackTrace();
    this.refreshPercentage(100.0F);
    this.setForeground(Color.red);
    if (this.mProgress != null) {
      this.mProgress.endAllProcesses();
    }
  }

  @Override
  public WorkerStatusList getChildWorkerStatusList(int index) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public WorkerStatusList getParentWorkerStatusList() {
    return this.mProgress;
  }

  /**
   * @see org.insensa.WorkerStatus#setParentWorkerStatusList(org.insensa.WorkerStatusList)
   */
  @Override
  public void setParentWorkerStatusList(WorkerStatusList mProgress) {
    this.mProgress = mProgress;
  }

  @Override
  public float getProgress() {

    return 0;
  }

  @Override
  public float getStepSize() {

    return 0;
  }

  @Override
  public void setStepSize(float stepSize) {


  }

  @Override
  public void refreshPercentage(float percent) {
    this.setValue((int) percent);

  }

  @Override
  public void removeChildrenWorkerStatusLists() {


  }

  @Override
  public void setChildrenWorkerStatusLists(int numOfLists) {


  }

  @Override
  public void setProgressName(String name) {
    this.setString(name);
    this.setStringPainted(true);
  }

  @Override
  public void startPause(String text) {
    this.setString(text);
    this.setStringPainted(true);
    this.setIndeterminate(true);
  }

  @Override
  public void startProcess() {
    this.time = System.currentTimeMillis();
  }

}

package org.insensa.view.dialogs;
/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */


import org.insensa.IProjectListener;
import org.insensa.LowPrioException;
import org.insensa.Model;
import org.insensa.Project;
import org.insensa.exceptions.GisFileSetException;
import org.insensa.User;
import org.insensa.XMLPropertyException;
import org.insensa.commands.CommandManager;
import org.insensa.commands.DeleteProjectCommand;
import org.insensa.commands.ImportProjectCommand;
import org.insensa.commands.ModelCommand;
import org.insensa.commands.OpenProjectCommand;
import org.insensa.controller.Controller;
import org.insensa.helpers.SystemSpec;
import org.insensa.view.View;
import org.insensa.view.ViewPropertiesProjectListener;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.insensa.view.dialogs.processes.MainProcessDialog;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;


public class DialogOpenProject extends SettingsDialog {
  private static final Logger log = LoggerFactory.getLogger(DialogOpenProject.class);

  private static final long serialVersionUID = 1L;
  // Labels
  private JLabel labelProjects;
  private JLabel labelOwner;
  // panels
  private JPanel panelProject;
  private JPanel panelOwner;
  private JScrollPane scrollPaneProject;
  private JTable tableProjects;
  private JPopupMenu popupMenu;
  private JComboBox comboBoxOwner;
  private JSeparator separatorButtons;
  private JButton buttonImportProject;
  private Model model;
  private Controller controller;
  private List<User> users;
  private View view;

  public DialogOpenProject(Model model, View view, Controller controller)
      throws JDOMException, IOException {
    this.model = model;
    this.view = view;
    this.controller = controller;
    this.users = new ArrayList<>();
    this.users.addAll(this.model.getUsers());
    super.setTitle("Open Project");
    super.setName("Open Project");

    this.initItems();
    this.initProjectPanel();
    this.initPopupMenu();
    this.initOwnerPanel();

    super.initComponents(this.initComponents());
    super.setHeadTitle("Open Project");
    super.getButtonOk().addActionListener(new ButtonFinishActionListener());
    getButtonCancel().addActionListener(e -> {
      setVisible(false);
      dispose();
    });
  }

  public boolean deleteAllFiles(File folder) {
    if (folder.exists() && folder.isDirectory()) {
      File[] children = folder.listFiles();
      if (children == null) {
        return false;
      }
      for (File element: children) {
        if (element.isDirectory()) {
          if (!this.deleteAllFiles(element)) {
            return false;
          }
        } else if (!element.delete()) {
          return false;
        }
      }
      if (!folder.delete()) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  public void deleteProject(Dialog parent) throws IOException {
    int selectedRow = this.tableProjects.getSelectedRow();
    String userName = (String) this.comboBoxOwner.getSelectedItem();
    String projectName = (String) this.tableProjects.getValueAt(selectedRow, 0);
    Project project = this.model.getActiveProject();
    if (project != null) {
      String activeProjectName = project.getName();
      if (activeProjectName.equals(projectName)) {
        JOptionPane.showMessageDialog(parent, "You can not delete an open project");
        return;
      }
    }
    int answer = JOptionPane.showConfirmDialog(parent, "Do you really want to delete project \""
        + projectName + "\"");
    if (answer == JOptionPane.YES_OPTION) {
      for (User user: this.users) {
        if (user.getName().equals(userName)) {
          new DeleteProjectSwingWorker(user, projectName, parent).execute();
        }
      }
    }
  }

  private User getUser(String userName) {
    for (User user: this.users) {
      if (userName.equals(user.getName())) {
        return user;
      }
    }
    return null;
  }

  private JPanel initComponents() {
    JPanel panel = new JPanel();
    this.buttonImportProject = new JButton("Import Project");
    this.buttonImportProject.addActionListener(new ImportProjectListener());
    javax.swing.GroupLayout groupLayout = new javax.swing.GroupLayout(panel);
    panel.setLayout(groupLayout);
    groupLayout.setHorizontalGroup(groupLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            groupLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.separatorButtons, javax.swing.GroupLayout.DEFAULT_SIZE, 474,
                    Short.MAX_VALUE).addContainerGap())
        .addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            groupLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.panelOwner, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        .addGroup(
            groupLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.panelProject, javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        .addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup()
            .addComponent(this.buttonImportProject).addContainerGap()));
    groupLayout.setVerticalGroup(groupLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
            groupLayout
                .createSequentialGroup()
                .addComponent(this.panelOwner, javax.swing.GroupLayout.PREFERRED_SIZE,
                    javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.panelProject, javax.swing.GroupLayout.PREFERRED_SIZE,
                    javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(this.buttonImportProject)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(this.separatorButtons, javax.swing.GroupLayout.PREFERRED_SIZE,
                    8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)));
    return panel;
  }

  private void initItems() throws IOException {
    this.separatorButtons = new JSeparator();

    // Panel Project Content
    this.panelProject = new JPanel();
    this.scrollPaneProject = new JScrollPane();

    this.labelProjects = new JLabel("Name");
    this.labelProjects.setFont(Font.getFont("Dialog-Plain-12"));
    Vector<String> vectorNames = new Vector<>();
    vectorNames.add("Name");
    vectorNames.add("Created");

    this.tableProjects = new JTable(new ProjectTableModel(vectorNames, 1));

    this.tableProjects.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
          DialogOpenProject.this.getButtonOk().doClick();
        }
      }
    });

    this.tableProjects.addKeyListener(new DeleteProjectActionListener(this));
    this.scrollPaneProject.setViewportView(this.tableProjects);

    this.comboBoxOwner = new JComboBox();
    this.panelOwner = new JPanel();
    this.labelOwner = new JLabel("Name");
    this.labelOwner.setFont(Font.getFont("Dialog-Plain-12"));

    if (this.users == null || this.users.isEmpty()) {
      throw new LowPrioException("No existing users found."
          + "\nYou have to create a new user and project");
    }
    this.setTableModel(this.users.get(0));
    for (int i = 0; i < this.users.size(); i++) {
      this.comboBoxOwner.addItem(this.users.get(i).getName());
    }

    this.comboBoxOwner.addActionListener(event -> {
      for (int i = 0; i < DialogOpenProject.this.users.size(); i++) {
        if (DialogOpenProject.this.users.get(i).getName()
            .equals(DialogOpenProject.this.comboBoxOwner.getSelectedItem().toString())) {
          DialogOpenProject.this.setTableModel(DialogOpenProject.this.users.get(i));
        }
      }

    });
  }

  private void initOwnerPanel() {
    this.panelOwner.setBorder(javax.swing.BorderFactory.createTitledBorder("User"));
    javax.swing.GroupLayout ownerPanelLayout = new javax.swing.GroupLayout(this.panelOwner);
    this.panelOwner.setLayout(ownerPanelLayout);
    this.panelOwner.setLayout(ownerPanelLayout);
    ownerPanelLayout.setHorizontalGroup(ownerPanelLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
            ownerPanelLayout.createSequentialGroup().addContainerGap().addComponent(this.labelOwner)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(this.comboBoxOwner, 0, 293, Short.MAX_VALUE)
                .addGap(12, 12, 12)));
    ownerPanelLayout
        .setVerticalGroup(ownerPanelLayout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                ownerPanelLayout
                    .createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(
                        ownerPanelLayout
                            .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(this.comboBoxOwner, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(this.labelOwner))));
  }

  private void initPopupMenu() {
    this.popupMenu = new JPopupMenu();
    JMenuItem item = new JMenuItem("Delete");
    item.addActionListener(new DeleteProjectActionListener(this));
    this.popupMenu.add(item);

    this.tableProjects.addMouseListener(new MouseOnTableListener());
  }

  private void initProjectPanel() {
    this.panelProject.setBorder(javax.swing.BorderFactory.createTitledBorder("Project Name"));
    javax.swing.GroupLayout groupLayout = new javax.swing.GroupLayout(this.panelProject);
    this.panelProject.setLayout(groupLayout);
    groupLayout.setHorizontalGroup(groupLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
            groupLayout.createSequentialGroup().addContainerGap()
                .addComponent(this.labelProjects).addGap(30, 30, 30)
                .addComponent(this.scrollPaneProject, javax.swing.GroupLayout.PREFERRED_SIZE, 250,
                    javax.swing.GroupLayout.DEFAULT_SIZE)));
    groupLayout.setVerticalGroup(groupLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
            javax.swing.GroupLayout.Alignment.TRAILING,
            groupLayout
                .createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(
                    groupLayout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(this.labelProjects)
                        .addComponent(this.scrollPaneProject,
                            javax.swing.GroupLayout.PREFERRED_SIZE, 150,
                            javax.swing.GroupLayout.PREFERRED_SIZE))));
  }

  public void setTableModel(User user) {
    Vector<String> vectorNames = new Vector<String>();
    vectorNames.add("Name");
    vectorNames.add("Created");
    Vector<Vector<String>> vectorData = new Vector<Vector<String>>();
    for (Project project: user.getProjects()) {
      Vector<String> row = new Vector<String>();

      row.add(project.getName());
      row.add(DateFormat.getDateInstance().format(project.getDate()));
      vectorData.add(row);
    }
    this.tableProjects.setModel(new ProjectTableModel(vectorData, vectorNames));
  }

  @SuppressWarnings("rawtypes")
  public static class ProjectTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 3171718278649073690L;

    Class[] types = new Class[]
        {String.class, String.class};
    boolean[] canEdit = new boolean[]
        {false, false};

    public ProjectTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    public ProjectTableModel(Vector<String> names, int i) {
      super(names, i);
    }

    @SuppressWarnings("unchecked")
    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      return false;
    }
  }

  public class ButtonFinishActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      {
        if (DialogOpenProject.this.tableProjects.getSelectedRow() < 0) {
          DialogOpenProject.this.getLabelStatus().setText("You must select a Project");
        } else {
          final User user = DialogOpenProject.this.getUser(DialogOpenProject
              .this.comboBoxOwner.getSelectedItem().toString());
          if (user == null) {
          } else {
            for (final Project iProject: user.getProjects()) {
              int iRow = DialogOpenProject.this.tableProjects.getSelectedRow();
              int iColumn = DialogOpenProject.this.tableProjects.getSelectedColumn();
              String name = DialogOpenProject.this.tableProjects.getModel()
                  .getValueAt(iRow, iColumn).toString();
              if (iProject.getName().equals(name)) {
                final DialogSingleProgress singleProgress = new DialogSingleProgress(null);
                singleProgress.setModalityType(ModalityType.MODELESS);
                singleProgress.setTitle("Open Project");
                new SwingWorker<Object, Object>() {

                  @Override
                  protected Object doInBackground() throws Exception {

                    try {
                      OpenProjectCommand opCommand = new OpenProjectCommand(
                          DialogOpenProject.this.model, user, iProject, null);
                      opCommand.setWorkerStatus(singleProgress);
                      opCommand.execute();
                    } catch (XMLPropertyException exception) {
                      if (exception.getErrorType() == XMLPropertyException.FILE_EMPTY) {
                        String errorMessage = "The project  could not be opened because a "
                            + "file was not saved properly.\n"
                            + "The configuration file is empty, which normally should "
                            + "not occur\n"
                            + "One possible solution would be to delete the file completely "
                            + "and try again.\n" + "\n"
                            + "Do you want to delete the file and reopen the project?\n";
                        int answer = JOptionPane.showConfirmDialog(singleProgress, errorMessage,
                            "Error Openning Project",
                            JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
                        if (answer == JOptionPane.YES_OPTION &&
                            exception.getConfigFileName() != null) {
                          File cFile = new File(exception.getConfigFileName());
                          if (cFile.exists() && cFile.delete() == true) {
                            List<IProjectListener> list = new ArrayList<IProjectListener>();
                            list.add(new ViewPropertiesProjectListener(
                                DialogOpenProject.this.view.getViewProperties(),
                                DialogOpenProject.this.view));
                            OpenProjectCommand opCommand = new OpenProjectCommand(
                                DialogOpenProject.this.model, user, iProject,
                                list);
                            opCommand.setWorkerStatus(singleProgress);
                            opCommand.execute();
                          } else {
                            throw exception;
                          }
                        } else {
                          throw exception;
                        }
                      }
                    } catch (GisFileSetException exception) {
                      if (exception.getErrorCause()
                          == GisFileSetException.CREATE_DIR_WHILE_OPEN_PROJECT) {
                        String errorMessage = "The project could not be opened "
                            + "because a FileSet is damaged:\n" + exception.getRasterFilePath()
                            + "\n"
                            + "Whould you like to delete this folder and all files in it "
                            + "from your harddisk and try again?\n\n"
                            + "WARNING: This cannot be undone!!";
                        int answer = JOptionPane.showConfirmDialog(singleProgress,
                            errorMessage, "Error Openning Project",
                            JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
                        if (answer == JOptionPane.YES_OPTION) {
                          File fs = new File(exception.getRasterFilePath());

                          if (!DialogOpenProject.this.deleteAllFiles(fs))
                            throw new IOException(
                                "ERROR DELETING: You have to delete the folder \n"
                                    + exception.getRasterFilePath() + "\n"
                                    + "yourself");
                          List<IProjectListener> list = new ArrayList<>();
                          list.add(new ViewPropertiesProjectListener(
                              DialogOpenProject.this.view.getViewProperties(),
                              DialogOpenProject.this.view));
                          OpenProjectCommand opCommand = new OpenProjectCommand(
                              DialogOpenProject.this.model, user, iProject, list);
                          opCommand.setWorkerStatus(singleProgress);
                          opCommand.execute();
                        } else
                          throw exception;
                      }
                    }
                    return null;
                  }

                  @Override
                  protected void done() {
                    try {
                      this.get();
                      String path = user.getWorkspacePath();
                      String projectName = iProject.getName();
                      DialogOpenProject.this.view.closeProject();
                      DialogOpenProject.this.view.openProject(path + projectName
                          + SystemSpec.separator);
                      DialogOpenProject.this.controller.addNewActionListener();

                    } catch (InterruptedException | ExecutionException | IOException exception) {
                      DialogOpenProject.this.view.closeProject();
                      JOptionPane.showMessageDialog(DialogOpenProject.this.view,
                          exception.getMessage());
                      singleProgress.setVisible(false);
                      log.error("UNKNOWN", exception);
                    }
                  }

                }.execute();
                DialogOpenProject.this.setVisible(false);
                DialogOpenProject.this.dispose();
              }
            }
          }
        }
      }
    }
  }

  public class DeleteProjectActionListener implements ActionListener, KeyListener {
    private Dialog parent;

    public DeleteProjectActionListener(Dialog parent) {
      this.parent = parent;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
      if (DialogOpenProject.this.tableProjects.getSelectedRowCount() >= 1)
        try {
          DialogOpenProject.this.deleteProject(this.parent);
        } catch (IOException e1) {
          log.error("UNKNOWN", e1);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
      if (e.getKeyCode() == KeyEvent.VK_DELETE && DialogOpenProject.this.tableProjects.getSelectedRowCount() >= 1)
        try {
          DialogOpenProject.this.deleteProject(this.parent);
        } catch (IOException e1) {
          log.error("UNKNOWN", e1);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

  }

  public class DeleteProjectSwingWorker extends SwingWorker<Object, Object> {
    private User user;
    private String projectName;
    private Dialog parentDialog;

    public DeleteProjectSwingWorker(User user, String projectName, Dialog parentDialog) {
      this.parentDialog = parentDialog;
      this.user = user;
      this.projectName = projectName;
    }

    @Override
    protected Object doInBackground() {
      ModelCommand command = new DeleteProjectCommand();
      command.setWorkerStatus(new DialogSingleProgress(null, this.parentDialog));
      ((DeleteProjectCommand) command).setFolderName(this.user.getWorkspacePath() + this.projectName);
      try {
        command.execute();
        this.user.removeProject(this.projectName);
      } catch (IOException e) {
        log.error("Error Deleting", e);
      }
      return null;
    }

    /**
     * @see javax.swing.SwingWorker#done()
     */
    @Override
    protected void done() {
      SwingUtilities.invokeLater(() -> DialogOpenProject.this.setTableModel(DeleteProjectSwingWorker.this.user));

    }
  }

  private class ImportProjectListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      JFileChooser fc = new JFileChooser();
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      if (fc.showOpenDialog(DialogOpenProject.this) == JFileChooser.CANCEL_OPTION)
        return;

      File selFile = fc.getSelectedFile();
      if (!selFile.exists() || !selFile.isDirectory()) {
        DialogOpenProject.this.setStatus("Import Error: You have to select a Directory");
      } else {
        String selUser = (String) DialogOpenProject.this.comboBoxOwner.getSelectedItem();
        final User user = DialogOpenProject.this.getUser(selUser);
        if (user == null) {
          DialogOpenProject.this.setStatus("No user selected");
          return;
        }
        ImportProjectCommand command = new ImportProjectCommand(user, selFile);
        command.setWorkerStatus(new MainProcessDialog(DialogOpenProject.this.view, DialogOpenProject.this, null));
        CommandManager cManager = CommandManager.getInstance();
        cManager.clearCommands();
        cManager.addCommand(command);
        command.createDependendCommands();
        ViewCommandManager.executeCommands(DialogOpenProject.this.view, new IProcessFinishedListener() {

          @Override
          public void canceled() {
          }

          @Override
          public void done() {
            DialogOpenProject.this.setTableModel(user);
            SwingUtilities.invokeLater(() -> DialogOpenProject.this.setTableModel(user));
          }
        });
      }
    }
  }

  public class MouseOnTableListener extends MouseAdapter {

    @Override
    public void mousePressed(MouseEvent e) {
      if (e.isPopupTrigger())
        DialogOpenProject.this.popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      if (e.isPopupTrigger())
        DialogOpenProject.this.popupMenu.show(e.getComponent(), e.getX(), e.getY());
    }
  }
}

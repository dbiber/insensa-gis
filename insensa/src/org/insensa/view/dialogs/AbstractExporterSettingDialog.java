/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.dialogs;

import org.insensa.view.exports.IViewFileExporterSetting;
import org.insensa.view.imports.IViewFileImporterSetting;

import javax.swing.JPanel;


public abstract class AbstractExporterSettingDialog extends SettingsDialog implements IViewFileExporterSetting {

  private ViewSettingsCloseListener closeListener;

//  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    this.closeListener = closeListener;
    initComponents(initComponent());
    super.setHeadTitle(getDialogTitle());
    super.setTitle(getDialogTitle());
    getButtonOk().addActionListener(actionEvent -> onButtonOkPressed());
    this.pack();
    this.setVisible(true);
  }

  protected abstract String getDialogTitle();

  protected void notifyDialogClosed(ViewSettingsCloseListener.Result result) {
    closeListener.closed(result);
  }

  /**
   * This method should return the main JPanel object.
   * This methods gets called right within the Constructor,
   * therefore you cannot add dynamically attributes in the separate Constructor
   */
  public abstract JPanel initComponent();

  public abstract void onButtonOkPressed();

}

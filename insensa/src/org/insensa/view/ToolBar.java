/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class ToolBar extends JToolBar {

  private static final long serialVersionUID = -3816496290515824841L;
  private JButton buttonSave;
  private JButton buttonRefresh;
  private JButton buttonProjects;
  private JButton buttonNewProject;
  private JButton buttonNewFileSet;
  private JButton buttonImportFile;
  private JButton buttonImportFolder;
  private JButton buttonAddInfoReader;
  private JButton buttonAddOption;
  private JButton buttonAddConnection;
  private JButton buttonExecuteInfoReader;
//  private JButton buttonExecuteInfoConnection;
  private JButton buttonExecuteOption;
  private JButton buttonExecuteConnection;

  private JPanel jPanel;

  private JButton testButton;
  private JButton testButton2;
  private JButton buttonExecuteAllConnections;

  private String IMAGE_PATH = "images/";


  public ToolBar() {
    super();
    JSeparator sep;
    ResourceBundle bundle = ResourceBundle.getBundle("img");
    this.jPanel = new JPanel();
    ((FlowLayout) this.jPanel.getLayout()).setAlignment(FlowLayout.LEFT);
    this.jPanel.setOpaque(false);
    this.add(this.jPanel);

    this.buttonSave = new JButton(new ImageIcon(bundle.getString("menu.save")));
    this.buttonSave.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonSave.setOpaque(false);
    this.buttonSave.setContentAreaFilled(false);
    this.buttonSave.setToolTipText("save");
    this.jPanel.add(this.buttonSave);

    this.buttonRefresh = new JButton(new ImageIcon(bundle.getString("menu.refresh")));
    this.buttonRefresh.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonRefresh.setOpaque(false);
    this.buttonRefresh.setContentAreaFilled(false);
    this.buttonRefresh.setToolTipText("refresh");
    this.jPanel.add(this.buttonRefresh);

    sep = new JSeparator(SwingConstants.VERTICAL);
    sep.setPreferredSize(new Dimension(1, 20));
    this.jPanel.add(sep);

    this.buttonProjects = new JButton(new ImageIcon(bundle.getString("project.open.delete.import")));
    this.buttonProjects.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonProjects.setOpaque(false);
    this.buttonProjects.setContentAreaFilled(false);
    this.buttonProjects.setToolTipText("open/manage projects");
    this.jPanel.add(this.buttonProjects);

    this.buttonNewProject = new JButton(new ImageIcon(bundle.getString("project.new")));
    this.buttonNewProject.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonNewProject.setOpaque(false);
    this.buttonNewProject.setContentAreaFilled(false);
    this.buttonNewProject.setToolTipText("create new project");
    this.jPanel.add(this.buttonNewProject);

    this.buttonNewFileSet = new JButton(new ImageIcon(bundle.getString("fileset.new")));
    this.buttonNewFileSet.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonNewFileSet.setOpaque(false);
    this.buttonNewFileSet.setContentAreaFilled(false);
    this.buttonNewFileSet.setToolTipText("add new FileSet");
    this.jPanel.add(this.buttonNewFileSet);

    sep = new JSeparator(SwingConstants.VERTICAL);
    sep.setPreferredSize(new Dimension(1, 20));
    this.jPanel.add(sep);

    this.buttonImportFile = new JButton(new ImageIcon(bundle.getString("menu.file.import")));
    this.buttonImportFile.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonImportFile.setOpaque(false);
    this.buttonImportFile.setContentAreaFilled(false);
    this.buttonImportFile.setToolTipText("import file");
    this.jPanel.add(this.buttonImportFile);

    this.buttonImportFolder = new JButton(new ImageIcon(bundle.getString("menu.folder.import")));
    this.buttonImportFolder.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonImportFolder.setOpaque(false);
    this.buttonImportFolder.setContentAreaFilled(false);
    this.buttonImportFolder.setToolTipText("import folder");
    this.jPanel.add(this.buttonImportFolder);

    sep = new JSeparator(SwingConstants.VERTICAL);
    sep.setPreferredSize(new Dimension(1, 20));
    this.jPanel.add(sep);

    this.buttonAddInfoReader = new JButton(new ImageIcon(bundle.getString("inforeader.add")));
    this.buttonAddInfoReader.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonAddInfoReader.setOpaque(false);
    this.buttonAddInfoReader.setContentAreaFilled(false);
    this.buttonAddInfoReader.setToolTipText("add InfoReader");
    this.jPanel.add(this.buttonAddInfoReader);

    this.buttonAddOption = new JButton(new ImageIcon(bundle.getString("option.add")));
    this.buttonAddOption.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonAddOption.setOpaque(false);
    this.buttonAddOption.setContentAreaFilled(false);
    this.buttonAddOption.setToolTipText("add OptionFileChanger");
    this.jPanel.add(this.buttonAddOption);

    this.buttonAddConnection = new JButton(new ImageIcon(bundle.getString("connection.add")));
    this.buttonAddConnection.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonAddConnection.setOpaque(false);
    this.buttonAddConnection.setContentAreaFilled(false);
    this.buttonAddConnection.setToolTipText("add Connection");
    this.jPanel.add(this.buttonAddConnection);

    sep = new JSeparator(SwingConstants.VERTICAL);
    sep.setPreferredSize(new Dimension(1, 20));
    this.jPanel.add(sep);

    this.buttonExecuteInfoReader = new JButton(new ImageIcon(bundle.getString("inforeader.execute")));
    this.buttonExecuteInfoReader.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonExecuteInfoReader.setOpaque(false);
    this.buttonExecuteInfoReader.setContentAreaFilled(false);
    this.buttonExecuteInfoReader.setToolTipText("execute all InfoReader\n on selected file");
    this.jPanel.add(this.buttonExecuteInfoReader);

    this.buttonExecuteOption = new JButton(new ImageIcon(bundle.getString("option.execute")));
    this.buttonExecuteOption.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonExecuteOption.setOpaque(false);
    this.buttonExecuteOption.setContentAreaFilled(false);
    this.buttonExecuteOption.setToolTipText("execute all OptionFileChanger\n on selected files");
    this.jPanel.add(this.buttonExecuteOption);

    this.buttonExecuteConnection = new JButton(new ImageIcon(bundle.getString("connection.execute")));
    this.buttonExecuteConnection.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonExecuteConnection.setOpaque(false);
    this.buttonExecuteConnection.setContentAreaFilled(false);
    this.buttonExecuteConnection.setToolTipText("execute selected Connection");
    this.jPanel.add(this.buttonExecuteConnection);

//    this.buttonExecuteInfoConnection = new JButton(new ImageIcon(bundle.getString("infoConnection.execute")));
//    this.buttonExecuteInfoConnection.setBorder(new EmptyBorder(5, 5, 5, 5));
//    this.buttonExecuteInfoConnection.setOpaque(false);
//    this.buttonExecuteInfoConnection.setContentAreaFilled(false);
//    this.buttonExecuteInfoConnection.setToolTipText("execute InfoConnection on selected file");
//    this.jPanel.add(this.buttonExecuteInfoConnection);

    this.buttonExecuteAllConnections = new JButton(new ImageIcon(bundle.getString("connection.execute.all")));
    this.buttonExecuteAllConnections.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.buttonExecuteAllConnections.setOpaque(false);
    this.buttonExecuteAllConnections.setContentAreaFilled(false);
    this.buttonExecuteAllConnections.setToolTipText("execute all Connections");

    this.jPanel.add(this.buttonExecuteAllConnections);

    sep = new JSeparator(SwingConstants.VERTICAL);
    sep.setPreferredSize(new Dimension(1, 20));
    this.jPanel.add(sep);

    this.testButton = new JButton("TEST");
    // testButton2 = new JButton("TEST2(Set NoDataValue)");

    // jPanel.add(testButton);
    // jPanel.add(testButton2);
  }

  public JButton getButtonAddConnection() {
    return this.buttonAddConnection;
  }

  public JButton getButtonAddInfoReader() {
    return this.buttonAddInfoReader;
  }

  public JButton getButtonAddOption() {
    return this.buttonAddOption;
  }

  public JButton getButtonExecuteAllConnections() {
    return this.buttonExecuteAllConnections;
  }

//  public JButton getButtonExecuteInfoConnection() {
//    return buttonExecuteInfoConnection;
//  }

  public JButton getButtonExecuteConnection() {
    return this.buttonExecuteConnection;
  }

  public JButton getButtonExecuteInfoReader() {
    return this.buttonExecuteInfoReader;
  }

  public JButton getButtonExecuteOption() {
    return this.buttonExecuteOption;
  }

  public JButton getButtonImportFile() {
    return this.buttonImportFile;
  }

  public JButton getButtonImportFolder() {
    return this.buttonImportFolder;
  }

  public JButton getButtonNewFileSet() {
    return this.buttonNewFileSet;
  }

  public JButton getButtonNewProject() {
    return this.buttonNewProject;
  }

  public JButton getButtonProjects() {
    return this.buttonProjects;
  }

  public JButton getButtonRefresh() {
    return this.buttonRefresh;
  }

  public JButton getButtonSave() {
    return this.buttonSave;
  }

  public JButton getTestButton() {
    return this.testButton;
  }

  public JButton getTestButton2() {
    return this.testButton2;
  }

}

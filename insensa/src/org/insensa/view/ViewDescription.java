/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import org.insensa.IGisFileContainer;

import javax.swing.JEditorPane;


public class ViewDescription extends JEditorPane
{

	private static final long serialVersionUID = 8612226728775772408L;
	private IGisFileContainer rasterFile;


	public ViewDescription()
	{
		super.setEditable(false);
	}

	/**
	 * @return
	 */
	public IGisFileContainer getRasterFile()
	{
		return this.rasterFile;
	}

	/**
	 * @param rasterFile
	 */
	public void setGisFile(IGisFileContainer rasterFile)
	{
		if (rasterFile == null)
			return;
		this.rasterFile = rasterFile;
		if (rasterFile.getDescription() == null)
			this.setText("");
		else
			this.setText(rasterFile.getDescription());
	}

	/** 
	 * 
	 * @see javax.swing.JEditorPane#setText(java.lang.String)
	 */
	@Override
	public void setText(String t)
	{
		super.setText(t);
	}

}

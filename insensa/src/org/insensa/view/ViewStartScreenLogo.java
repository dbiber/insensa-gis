/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.JComponent;


import org.insensa.helpers.VersionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author dennis
 */
public class ViewStartScreenLogo extends JComponent {
  private static final Logger log = LoggerFactory.getLogger(ViewStartScreenLogo.class);

  private boolean newPlugin;

  /**
   * @throws IOException
   *
   */
  public ViewStartScreenLogo() throws IOException {
    newPlugin = false;
  }

  /**
   * @see javax.swing.JComponent#paint(java.awt.Graphics)
   */
  @Override
  public void paint(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;
    try {
      g2d.drawImage(getImage(), 0, 0, 454, 250, null);
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

  private BufferedImage getImage() throws IOException {
    BufferedImage image = new BufferedImage(454, 250, BufferedImage.TYPE_INT_RGB);
    Graphics2D ig = image.createGraphics();

    BufferedImage logoImage = ImageIO.read(new File(ResourceBundle.getBundle("img").getString("start.screen.logo")));
    ig.drawImage(logoImage, 0, 0, 454, 250, null);

    ig.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
    ig.setFont(new Font("Tomaha", Font.BOLD, 10));
    ig.drawString("( Version: " + VersionHelper.getInstance().getVersion() + " )", 5, 15);

    if (newPlugin) {
      ig.setColor(Color.RED);
      //	    ig.setColor(new Color(247, 56, 38));
      ig.setFont(new Font("Tomaha", Font.BOLD, 12));
      ig.drawString("NEWS", 5, 250 - 30);
      ig.setFont(new Font("Tomaha", Font.BOLD, 10));
      ig.drawString("New plugins available", 5, 250 - 15);
    }
    return image;
  }

  public void setNewPluginText(boolean newPlugin) {
    this.newPlugin = newPlugin;
  }

}

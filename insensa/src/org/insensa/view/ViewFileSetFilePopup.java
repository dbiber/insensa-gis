/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import java.awt.Dimension;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.insensa.IGisFileContainer;
import org.insensa.connections.CIndexation;
import org.insensa.connections.ConnectionFileChanger;


public class ViewFileSetFilePopup extends JPopupMenu {

  private static final long serialVersionUID = 1L;

  private JMenu itemExecute;
  private JMenuItem itemExecuteAllOptions;
  private JMenuItem itemExecuteAllInfoReader;
  private JMenuItem itemExecuteConnection;
  private JMenuItem itemExecuteAllInfoConnections;
  private JMenuItem itemAddAllSensitivityConnections;

  private JMenu itemAdd;

  private JMenuItem itemAddStadardisation;
  private JMenuItem itemAddInformationReader;
  private JMenuItem itemAddOptionFileChanger;

  private JMenuItem itemAddInfoConnection;
  private JMenuItem itemAddConnection;

  private JMenuItem itemSetNodataValue;

  private JMenuItem itemStackFiles;

  private JMenu menuExport;
  private JMenuItem itemExportFile;

  private String IMAGE_PATH = "images/";


  public ViewFileSetFilePopup() {
    super();
    this.initItems();
  }

  public JMenuItem getItemAddAllSensitivityConnections() {
    return this.itemAddAllSensitivityConnections;
  }

  public JMenuItem getItemAddConnection() {
    return this.itemAddConnection;
  }

  public JMenuItem getItemAddInfoConnection() {
    return this.itemAddInfoConnection;
  }

  public JMenuItem getItemAddInformationReader() {
    return this.itemAddInformationReader;
  }

  public JMenuItem getItemAddOptionFileChanger() {
    return this.itemAddOptionFileChanger;
  }

  public JMenuItem getItemAddStandardisation() {
    return this.itemAddStadardisation;
  }

  public JMenuItem getItemExecuteAllInfoReader() {
    return this.itemExecuteAllInfoReader;
  }

  public JMenuItem getItemExecuteAllOptions() {
    return this.itemExecuteAllOptions;
  }

  public JMenuItem getItemExecuteConnection() {
    return this.itemExecuteConnection;
  }

  public JMenuItem getItemExecuteAllInfoConnections() {
    return this.itemExecuteAllInfoConnections;
  }

  public JMenuItem getItemExportFile() {
    return this.itemExportFile;
  }

  public JMenuItem getItemSetNodataValue() {
    return this.itemSetNodataValue;
  }

  public JMenuItem getItemStackFiles() {return this.itemStackFiles;}


  private void initItems() {
    ResourceBundle bundle = ResourceBundle.getBundle("img");
    this.itemAdd = new JMenu("Add");
    this.itemAddInformationReader = new JMenuItem("Information Reader");
    this.itemAddOptionFileChanger = new JMenuItem("Option File Changer");
    this.itemAddConnection = new JMenuItem("Connection");
    this.itemAddInfoConnection = new JMenuItem("InfoConnection");
    this.itemAddAllSensitivityConnections = new JMenuItem("Add Sensitivity Analysis");
    this.itemSetNodataValue = new JMenuItem("Set NodataValue");
    this.itemStackFiles = new JMenuItem("Stack Files");

    this.itemAdd.add(this.itemAddInformationReader);
    this.itemAdd.add(this.itemAddOptionFileChanger);
    this.itemAdd.add(this.itemAddConnection);
    this.itemAdd.add(this.itemAddInfoConnection);
    this.itemAdd.add(this.itemAddAllSensitivityConnections);

    this.itemAdd.setIcon(new ImageIcon(bundle.getString("menu.add")));
    this.itemAddInformationReader.setIcon(new ImageIcon(bundle.getString("inforeader.add")));
    this.itemAddOptionFileChanger.setIcon(new ImageIcon(bundle.getString("option.add")));
    this.itemAddConnection.setIcon(new ImageIcon(bundle.getString("connection.add")));
    this.itemAddInfoConnection.setIcon(new ImageIcon(bundle.getString("infoConnection.add")));
    this.itemAddAllSensitivityConnections.setIcon(new ImageIcon(bundle.getString("menu.sensi.add")));

    this.itemExecute = new JMenu("Execute");
    this.itemExecuteAllOptions = new JMenuItem("Option File Changer");
    this.itemExecuteAllInfoReader = new JMenuItem("All Information Reader");
    this.itemExecuteConnection = new JMenuItem("Connection");
    this.itemExecuteAllInfoConnections = new JMenuItem("All InfoConnections");
    this.itemExecute.add(this.itemExecuteAllInfoReader);
    this.itemExecute.add(this.itemExecuteAllOptions);
    this.itemExecute.add(this.itemExecuteConnection);
    this.itemExecute.add(this.itemExecuteAllInfoConnections);

    this.itemExecuteAllInfoReader.setIcon(new ImageIcon(bundle.getString("inforeader.execute")));
    this.itemExecuteConnection.setIcon(new ImageIcon(bundle.getString("connection.execute")));
    this.itemExecuteAllInfoConnections.setIcon(new ImageIcon(bundle.getString("infoConnection.execute")));
    this.itemExecute.setIcon(new ImageIcon(bundle.getString("menu.execute")));
    this.itemExecuteAllOptions.setIcon(new ImageIcon(bundle.getString("option.execute")));

    this.menuExport = new JMenu("Export");
    this.menuExport.setIcon(new ImageIcon(bundle.getString("menu.export")));

    this.itemExportFile = new JMenuItem("File");
    this.itemExportFile.setIcon(new ImageIcon(bundle.getString("menu.file.export")));
    this.menuExport.add(this.itemExportFile);

    this.itemStackFiles.setIcon(new ImageIcon(bundle.getString("file.stack")));

    this.add(this.itemAdd);
    this.addSeparator();
    this.add(this.itemExecute);
    this.addSeparator();
    this.add(itemStackFiles);
    this.addSeparator();
    this.add(this.itemSetNodataValue);
    this.addSeparator();
    this.add(this.menuExport);

    Dimension dim = this.itemExecute.getPreferredSize();
    dim.setSize(dim.getWidth() + 15, dim.getHeight());
    this.itemExecute.setPreferredSize(dim);
  }

  public void setEnableMulti(List<IGisFileContainer> fileList, boolean enable) {
    this.itemAddConnection.setEnabled(enable);
    this.itemAddInfoConnection.setEnabled(enable);
    if (fileList.size() > 1) {
      this.itemAddAllSensitivityConnections.setEnabled(false);
    } else if (fileList.size() == 1) {
      ConnectionFileChanger con = fileList.get(0).getConnectionFileChanger();
      if (con != null && con instanceof CIndexation) {
        this.itemAddAllSensitivityConnections.setEnabled(true);
      } else {
        this.itemAddAllSensitivityConnections.setEnabled(false);
      }
    }
  }
}

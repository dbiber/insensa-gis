/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.insensa.view.menubar.MenuBar;



public class ViewMenu
{
	private MenuBar menuBar;
	private ToolBar toolBar;
	private StatusLabel statusLabel;


	public ViewMenu()
	{
		this.menuBar = new MenuBar();
		this.menuBar.createMenuItems();
		this.toolBar = new ToolBar();
		this.statusLabel = new StatusLabel("stat");
		this.statusLabel.setBorder(new EmptyBorder(4, 4, 4, 4));
		this.statusLabel.setHorizontalAlignment(SwingConstants.LEADING);
		this.statusLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));

	}

	/**
	 * @return
	 */
	public MenuBar getMenuBar()
	{
		return this.menuBar;
	}

	/**
	 * @return
	 */
	public StatusLabel getStatusLabel()
	{
		return this.statusLabel;
	}

	/**
	 * @return
	 */
	public ToolBar getToolBar()
	{
		return this.toolBar;
	}

	/**
	 * @param menuBar
	 */
	public void setMenuBar(MenuBar menuBar)
	{
		this.menuBar = menuBar;
	}

	/**
	 * @param statusLabel
	 */
	public void setStatusLabel(StatusLabel statusLabel)
	{
		this.statusLabel = statusLabel;
	}

	/**
	 * @param toolBar
	 */
	public void setToolBar(ToolBar toolBar)
	{
		this.toolBar = toolBar;
	}

}

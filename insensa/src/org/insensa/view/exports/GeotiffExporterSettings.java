/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.exports;

import org.insensa.exports.FileExporter;
import org.insensa.exports.GeotiffExporter;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.AbstractExporterSettingDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;

import java.awt.Component;
import java.awt.Frame;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;


public class GeotiffExporterSettings extends AbstractExporterSettingDialog {

  private FileExporter exporter;
  private javax.swing.JFileChooser fileChooser;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JPanel panelMain;
  private javax.swing.JPanel panelTable;
  private javax.swing.JTable table;

  private void initComponents() {

    this.panelMain = new javax.swing.JPanel();
    this.fileChooser = new javax.swing.JFileChooser();
    this.panelTable = new javax.swing.JPanel();
    this.jScrollPane1 = new javax.swing.JScrollPane();
    this.table = new javax.swing.JTable();
    this.jSeparator1 = new javax.swing.JSeparator();

    this.fileChooser.setControlButtonsAreShown(false);
    this.fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);


    JComboBox TFWComboBox = new JComboBox(new String[]
        {"YES", "NO"});
    TFWComboBox.setSelectedIndex(1);
    JComboBox compressComboBox = new JComboBox(new String[]
        {"LZW", "DEFLATE", "PACKBITS", "NONE"});
    compressComboBox.setSelectedIndex(1);
    JComboBox predictorComboBox = new JComboBox(new String[]
        {"1", "2", "3"});
    predictorComboBox.setSelectedIndex(0);
    JComboBox zlevelComboBox = new JComboBox(new String[]
        {"1", "2", "3", "4", "5", "6", "7", "8", "9"});
    zlevelComboBox.setSelectedIndex(5);

    ExportOptionsModel model = new ExportOptionsModel();
    model.addColumn("Option", new Object[]
        {"TFW", "COMPRESS", "PREDICTOR", "ZLEVEL"});
    model.addColumn("Value", new Object[]
        {TFWComboBox, compressComboBox, predictorComboBox, zlevelComboBox});
    // String[]values = new String[]{"item1","item2","item3"};
    this.table.setModel(model);
    this.table.getColumnModel().getColumn(1).setCellEditor(new ComboBoxCellEditor());
    this.table.getColumnModel().getColumn(1).setCellRenderer(new ComboBoxCellRenderer());
    this.table.setRowHeight(20);

    this.jScrollPane1.setViewportView(this.table);

    javax.swing.GroupLayout panelTableLayout = new javax.swing.GroupLayout(this.panelTable);
    this.panelTable.setLayout(panelTableLayout);
    panelTableLayout.setHorizontalGroup(panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        panelTableLayout
            .createSequentialGroup()
            .addContainerGap()
            .addGroup(
                panelTableLayout
                    .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(this.jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 542,
                        Short.MAX_VALUE)
                    .addComponent(this.jSeparator1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 542,
                        Short.MAX_VALUE)).addContainerGap()));
    panelTableLayout.setVerticalGroup(panelTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelTableLayout
            .createSequentialGroup()
            .addContainerGap()
            .addComponent(this.jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE).addContainerGap()));

    javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(this.panelMain);
    this.panelMain.setLayout(panelMainLayout);
    panelMainLayout.setHorizontalGroup(panelMainLayout
        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(this.fileChooser, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)
        .addComponent(this.panelTable, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE,
            javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
    panelMainLayout.setVerticalGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        panelMainLayout
            .createSequentialGroup()
            .addComponent(this.fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(this.panelTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.getContentPane());
    this.getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.panelMain,
        javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.panelMain,
        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

    this.pack();
  }

  public void setFileExporter(FileExporter fileExporter) {
    this.exporter = fileExporter;
  }

  @Override
  public JPanel initComponent() {
    this.initComponents();

    return panelMain;
  }

  @Override
  public String getDialogTitle() {
    return "Set Export Setting";
  }

  @Override
  public void onButtonOkPressed() {
    Vector<String> optionVector = new Vector<String>();
    String valueTFW = (String) ((JComboBox) GeotiffExporterSettings.this.table.getValueAt(0, 1)).getSelectedItem();
    String valueCompress = (String) ((JComboBox) GeotiffExporterSettings.this.table.getValueAt(1, 1)).getSelectedItem();
    String valuePredictor = (String) ((JComboBox) GeotiffExporterSettings.this.table.getValueAt(2, 1)).getSelectedItem();
    String valueZLevel = (String) ((JComboBox) GeotiffExporterSettings.this.table.getValueAt(3, 1)).getSelectedItem();
    if (GeotiffExporterSettings.this.fileChooser.getSelectedFile() == null)
      return;
    String outputFilePath = GeotiffExporterSettings.this.fileChooser.getSelectedFile().getAbsolutePath();
    GeotiffExporterSettings.this.exporter.setOutputFilePath(outputFilePath);
    if (valueTFW.equals("YES"))
      optionVector.add("TFW=YES");
    optionVector.add("COMPRESS=" + valueCompress);
    if (valueCompress.equals("LZW") || valueCompress.equals("DEFLATE"))
      optionVector.add("PREDICTOR=" + valuePredictor);
    if (valueCompress.equals("DEFLATE"))
      optionVector.add("ZLEVEL=" + valueZLevel);

    ((GeotiffExporter) GeotiffExporterSettings.this.exporter).setOptionVector(optionVector);
    notifyDialogClosed(ViewSettingsCloseListener.Result.BUTTON_OK);
    GeotiffExporterSettings.this.setVisible(false);
  }

  private class ComboBoxCellEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 2304010344237569832L;
    JComboBox selBox = new JComboBox();

    @Override
    public Object getCellEditorValue() {
      return this.selBox;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      this.selBox = (JComboBox) value;
      return this.selBox;
    }
  }

  private class ComboBoxCellRenderer implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      return new JLabel((String) ((JComboBox) value).getSelectedItem());
    }
  }

  private class ExportOptionsModel extends DefaultTableModel {
    private static final long serialVersionUID = 7273782997130275119L;

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0)
        return false;
      else
        return true;
    }
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.exports;

import java.awt.Frame;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.insensa.exports.FileExporter;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.AbstractExporterSettingDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;

public class DefaultFileExporterDialog extends AbstractExporterSettingDialog {

  private JFileChooser fileChooser;

  private FileExporter exporter;


  @Override
  public void setFileExporter(FileExporter fileExporter) {

  }

  @Override
  public void startView(ViewSettingsCloseListener closeListener) {
    super.setHeadTitle("Choose Export Directory");
    super.setTitle("File-Exporter Dialog");
    this.setVisible(true);
  }

  @Override
  protected String getDialogTitle() {
    return "Export Settings";
  }

  @Override
  public void onButtonOkPressed() {
    String outputDirectory = fileChooser.getSelectedFile().getAbsolutePath();
    if (outputDirectory == null)
      return;
    exporter.setOutputFilePath(outputDirectory);
    setVisible(false);
  }

  @Override
  public JPanel initComponent() {
    JPanel panel = new JPanel();
    fileChooser = new JFileChooser();
    fileChooser.setControlButtonsAreShown(false);
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    panel.add(fileChooser);
    return panel;
  }

}

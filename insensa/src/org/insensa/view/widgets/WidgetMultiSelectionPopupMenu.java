package org.insensa.view.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;

public class WidgetMultiSelectionPopupMenu<E> extends JPanel {

  private Vector<E> vector;
  private List<E> elements;
  private Map<E, PopupMenuCheckbox<E>> checkboxes = new LinkedHashMap<>();
  private JPopupMenu popupMenu;
  private ActionListener listener;


  public WidgetMultiSelectionPopupMenu(String buttonText) {
    super();
    elements = new ArrayList<>();
    setLayout(new BorderLayout());
    setBackground(Color.blue);
    vector = new Vector<>();
    JToggleButton button = new JToggleButton(buttonText);
    popupMenu = new JPopupMenu();
    TogglePopupHandler handler = new TogglePopupHandler(popupMenu, button);
    popupMenu.addPopupMenuListener(handler);
    button.addActionListener(handler);
    setOpaque(true);
    setComponentPopupMenu(popupMenu);
    add(button);
  }

  public void setListener(ActionListener listener) {
    this.listener = listener;
  }

  public void addItem(String text, E element) {
    elements.add(element);
    PopupMenuCheckbox<E> checkbox = new PopupMenuCheckbox<>(text, element);
    checkboxes.put(checkbox.getElement(),checkbox);
    checkbox.addItemListener(e -> {
      @SuppressWarnings("unchecked")
      PopupMenuCheckbox<E> selCheckbox = (PopupMenuCheckbox<E>) e.getSource();
      if(listener==null) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        vector.add(selCheckbox.getElement());
        listener.actionPerformed(new ActionEvent(this,
            ItemEvent.SELECTED,""));
      } else {
        vector.remove(selCheckbox.getElement());
        listener.actionPerformed(new ActionEvent(this,
            ItemEvent.DESELECTED,""));
      }
    });
    popupMenu.add(checkbox);
  }

  public Vector<E> getSelectedElements() {
    return vector;
  }

  public void changeSelection(int index) {
    @SuppressWarnings("unchecked")
    PopupMenuCheckbox<E> popupMenuCheckbox = (PopupMenuCheckbox<E>) popupMenu.getComponent(index);
    if (popupMenuCheckbox.isSelected()) {
      popupMenuCheckbox.setSelected(false);
    } else {
      popupMenuCheckbox.setSelected(true);
    }
  }

  public List<E> getElements() {
    return elements;
  }

  public void setSelectedObjects(List<E> defaultVal) {
    defaultVal.forEach(e -> {
      checkboxes.get(e).setSelected(true);
//      vector.add(e);
    });
  }
}

package org.insensa.view.widgets;

import java.awt.BorderLayout;
import java.awt.event.ItemListener;

import javax.accessibility.Accessible;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.event.MouseInputListener;

public class PopupMenuCheckbox<E> extends JMenuItem implements Accessible {

  private E element;
  private JCheckBox checkBox;

  public PopupMenuCheckbox(String text, E element) {
    super(" ");
    this.element = element;
    setLayout(new BorderLayout());
    checkBox = new JCheckBox(text){
      private transient MouseInputListener handler;

      @Override
      public void updateUI() {
        removeMouseListener(handler);
        removeMouseMotionListener(handler);
        super.updateUI();
        handler = new DispatchParentHandler();
        addMouseListener(handler);
        addMouseMotionListener(handler);
        setFocusable(false);
        setOpaque(false);
      }
    };
    setPreferredSize(checkBox.getPreferredSize());
    add(checkBox);
  }

  @Override
  public boolean isSelected() {
    return checkBox.isSelected();
  }

  @Override
  public void setSelected(boolean b) {
    super.setSelected(b);
    checkBox.setSelected(b);
  }

  @Override
  public void doClick() {
    super.doClick();
    checkBox.doClick();
  }

  @Override
  public void addItemListener(ItemListener l) {
    checkBox.addItemListener(e -> {
      e.setSource(PopupMenuCheckbox.this);
      l.itemStateChanged(e);
    });
  }

  public E getElement() {
    return element;
  }
}

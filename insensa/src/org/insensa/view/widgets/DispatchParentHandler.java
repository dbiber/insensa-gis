package org.insensa.view.widgets;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

class DispatchParentHandler extends MouseInputAdapter {
  private void dispatchEvent(MouseEvent e) {
    Component src = e.getComponent();
    Container tgt = SwingUtilities.getUnwrappedParent(src);
    tgt.dispatchEvent(SwingUtilities.convertMouseEvent(src, e, tgt));
  }
  @Override public void mouseEntered(MouseEvent e) {
    dispatchEvent(e);
  }
  @Override public void mouseExited(MouseEvent e) {
    dispatchEvent(e);
  }
  @Override public void mouseMoved(MouseEvent e) {
    dispatchEvent(e);
  }
  @Override public void mouseDragged(MouseEvent e) {
    dispatchEvent(e);
  }
}
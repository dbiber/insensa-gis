package org.insensa.view.widgets;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

class TogglePopupHandler implements PopupMenuListener, ActionListener {
  private final JPopupMenu popup;
  private final AbstractButton button;
  protected TogglePopupHandler(JPopupMenu popup, AbstractButton button) {
    this.popup = popup;
    this.button = button;
  }
  @Override public void actionPerformed(ActionEvent e) {
    AbstractButton b = (AbstractButton) e.getSource();
    if (b.isSelected()) {
      Container p = SwingUtilities.getUnwrappedParent(b);
      Rectangle r = b.getBounds();
      popup.show(p, r.x, r.y + r.height);
    } else {
      popup.setVisible(false);
    }
  }
  @Override public void popupMenuCanceled(PopupMenuEvent e) { /* not needed */ }
  @Override public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    EventQueue.invokeLater(() -> {
      button.getModel().setArmed(false);
      button.getModel().setSelected(false);
    });
  }
  @Override public void popupMenuWillBecomeVisible(PopupMenuEvent e) { /* not needed */ }
}
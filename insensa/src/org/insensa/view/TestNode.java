/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Eine Dummy Klasse die Als Transferable dient
 *
 * @author dennis
 */
public class TestNode extends DefaultMutableTreeNode implements Transferable, Serializable, Observer {

  public static final DataFlavor TestNodes = new DataFlavor(TestNode.class, "test node");
  public static final DataFlavor TestNodesList = new DataFlavor(ArrayList.class, "TestNodes List");
  protected static final DataFlavor[] flavors =
      {TestNodes, TestNodesList};
  private static final long serialVersionUID = 8916525758005729916L;


  public TestNode() {
    super();
  }

  /**
   * @param data
   */
  public TestNode(Serializable data) {
    super(data);
  }

  /**
   * @see java.awt.datatransfer.Transferable#getTransferData(java.awt.datatransfer.DataFlavor)
   */
  @Override
  public Object getTransferData(DataFlavor arg0) throws UnsupportedFlavorException, IOException {
    if (this.isDataFlavorSupported(arg0))
      return this;
    else
      throw new UnsupportedFlavorException(arg0);
  }

  /**
   * @see java.awt.datatransfer.Transferable#getTransferDataFlavors()
   */
  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return flavors;
  }

  /**
   * @see java.awt.datatransfer.Transferable#isDataFlavorSupported(java.awt.datatransfer.DataFlavor)
   */
  @Override
  public boolean isDataFlavorSupported(DataFlavor arg0) {
    DataFlavor[] flavs = this.getTransferDataFlavors();
    for (DataFlavor flav: flavs) {
      if (flav.equals(arg0)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
   */
  @Override
  public void update(Observable o, Object arg) {

  }

}
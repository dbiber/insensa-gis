/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class ViewFileSetInfoReaderPopup extends JPopupMenu
{

	private static final long serialVersionUID = -750121801161359399L;
	JMenuItem itemExecute;


	public ViewFileSetInfoReaderPopup()
	{
		super();
		this.initItems();
	}

	/**
	 * @return
	 */
	public JMenuItem getItemExecute()
	{
		return this.itemExecute;
	}


	private void initItems()
	{
		this.itemExecute = new JMenuItem("execute");
		this.add(this.itemExecute);
		this.itemExecute.setIcon(new ImageIcon(ResourceBundle.getBundle("img").getString("inforeader.used")));
	}

}

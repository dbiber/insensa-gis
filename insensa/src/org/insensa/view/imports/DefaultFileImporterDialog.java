/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.imports;

import org.insensa.Environment;
import org.insensa.exceptions.WriteConfigException;
import org.insensa.imports.FileDescription;
import org.insensa.imports.IFileImporter;
import org.insensa.view.dialogs.AbstractPluginSettingsDialog;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.dialogs.helper.FolderFileFilter;
import org.insensa.view.dialogs.helper.FolderFileView;
import org.insensa.view.dialogs.helper.SingleFileFilter;
import org.insensa.view.dialogs.helper.SingleFileView;
import org.insensa.view.extensions.ViewChangeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileView;

public class DefaultFileImporterDialog extends AbstractPluginSettingsDialog implements IViewFileImporterSetting {
  private static final Logger log = LoggerFactory.getLogger(DefaultFileImporterDialog.class);

  private JFileChooser fileChooser;
  private IFileImporter fileImporter;

  public DefaultFileImporterDialog() {
  }


  /**
   * @see org.insensa.view.imports.IViewFileImporterSetting#setFileImporter(IFileImporter)
   */
  @Override
  public void setFileImporter(IFileImporter fileImporter) {
    this.fileImporter = fileImporter;
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {
  }

  @Override
  public String getDialogTitle() {
    return "Import Settings";
  }

  @Override
  public void onButtonOkPressed() {
    List<File> selFileList = new ArrayList<File>();
    File[] selFileArray = fileChooser.getSelectedFiles();
    if (selFileArray != null && selFileArray.length > 0) {
      for (int i = 0; i < selFileArray.length; i++)
        selFileList.add(selFileArray[i]);
      fileImporter.setFileList(selFileList);
      try {
        Environment.getViewProperties().setLastImportedFilePath(
            selFileList.get(0).getAbsolutePath());
      } catch (WriteConfigException e) {
        log.error("UNKNOWN", e);
      }
      super.notifyDialogClosed(ViewSettingsCloseListener.Result.BUTTON_OK);

    } else {
      super.notifyDialogClosed(ViewSettingsCloseListener.Result.BUTTON_CANCEL);
    }
    setVisible(false);
  }

  @Override
  public JPanel initComponent() {

    JPanel panel = new JPanel();
    fileChooser = new JFileChooser();
    fileChooser.setControlButtonsAreShown(false);
//		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    fileChooser.setMultiSelectionEnabled(true);
    createFileFilter();
    File tmpFile = new File(Environment.getViewProperties().getLastImportedFilePath());
    if (tmpFile.exists() && tmpFile.getParentFile().exists()) {
      fileChooser.setCurrentDirectory(tmpFile.getParentFile());
    }

    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    panel.add(fileChooser);

    return panel;
  }

  private void createFileFilter() {
    FileDescription[] fileDescriptions = fileImporter.getFileDescriptions();

    if (fileDescriptions != null && fileDescriptions.length > 0) {
      final List<FileFilter> fileFilterList = new ArrayList<FileFilter>();
      final List<FileView> fileViewList = new ArrayList<FileView>();
      for (int i = 0; i < fileDescriptions.length; i++) {
        if (fileDescriptions[i].isFile) {
          FileFilter filter = new SingleFileFilter(fileDescriptions[i]);
          FileView fView = new SingleFileView(fileDescriptions[i]);
          fileFilterList.add(filter);
//					fileChooser.addChoosableFileFilter(filter);
          fileViewList.add(fView);
        } else {
          FileFilter filter = new FolderFileFilter(fileDescriptions[i]);
          FileView fileView = new FolderFileView(fileDescriptions[i]);
          fileFilterList.add(filter);
//					fileChooser.addChoosableFileFilter(filter);
          fileViewList.add(fileView);
        }
      }
      if (!fileFilterList.isEmpty()) {
        fileChooser.removeChoosableFileFilter(fileChooser.getFileFilter());
        for (int j = 0; j < fileFilterList.size(); j++) {
          fileChooser.addChoosableFileFilter(fileFilterList.get(j));
        }
        fileChooser.setFileFilter(fileFilterList.get(0));
        fileChooser.setFileView(fileViewList.get(0));

        fileChooser.addPropertyChangeListener(JFileChooser.FILE_FILTER_CHANGED_PROPERTY, new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            if (fileFilterList.contains(evt.getNewValue())) {
              int index = fileFilterList.indexOf(evt.getNewValue());
              ((JFileChooser) evt.getSource()).setFileView(fileViewList.get(index));
              ((JFileChooser) evt.getSource()).rescanCurrentDirectory();
            }
          }
        });
      }
    }
  }

}

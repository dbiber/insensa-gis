/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.imports;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.insensa.imports.IFileImporter;
import org.insensa.imports.ImportManager;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.AbstractPluginSettingsDialog;
import org.insensa.view.extensions.ViewChangeType;


/**
 * Dialog to choose between a FileImporter
 *
 * @author dennis
 */
public class ChooseFileImporterDialog extends AbstractPluginSettingsDialog implements IViewFileImporterSetting {
  public static final String helpID = "export_files";
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JTable jTable1;
  private ButtonGroup buttonGroup;
  private String selectedImporter = null;

  /**
   * @see org.insensa.view.dialogs.SettingsDialog#getHelpId()
   */
  @Override
  public String getHelpId() {
    return helpID;
  }

  @Override
  public void setFileImporter(IFileImporter fileImporter) {

  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {

  }

  @Override
  public String getDialogTitle() {
    return "Choose FileImporter";
  }

  public String getSelectedImporter() {
    return this.selectedImporter;
  }

  @Override
  public JPanel initComponent() {
    JPanel panel = new JPanel();
    this.jScrollPane2 = new javax.swing.JScrollPane();
    this.jTable1 = new javax.swing.JTable();

    this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    List<String> exporterList = new ImportManager().getFileImporterList();

    Vector<Vector<Object>> dataVector = new Vector<Vector<Object>>();
    Vector<Object> row;
    this.buttonGroup = new ButtonGroup();
    for (String exportName : exporterList) {
      JPanel parentPanel = new JPanel();
//      String upperCaseName = exportName.substring(0, 1).toUpperCase() + exportName.substring(1);
      JRadioButton rButton = new JRadioButton();
      parentPanel.add(rButton);
      rButton.addActionListener(new RadioButtonActionListener());
      row = new Vector<Object>();
      row.add(parentPanel);
      this.buttonGroup.add(rButton);
      row.add(exportName);
//      row.add(upperCaseName);
      dataVector.add(row);
    }
    Vector<String> titleVec = new Vector<String>();
    titleVec.add("Selected");
    titleVec.add("Importer");
    TableModel tModel = new OptionsTableModel(dataVector, titleVec);
    tModel.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        ChooseFileImporterDialog.this.repaint();
      }
    });
    this.jTable1.setModel(tModel);
    this.jTable1.getColumnModel().getColumn(0).setCellEditor(new RadioButtonEditor(new JCheckBox()));
    this.jTable1.getColumnModel().getColumn(0).setCellRenderer(new RadioButtonRenderer());
    this.jTable1.setRowHeight(26);
    this.jTable1.setName("jTable1");
    this.jScrollPane2.setViewportView(this.jTable1);
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane2,
        javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup().addComponent(this.jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)));
    return panel;
  }

  @Override
  public void onButtonOkPressed() {
    ChooseFileImporterDialog.this.setVisible(false);
  }

  @Override
  public void onButtonCancelPressed() {
    ChooseFileImporterDialog.this.selectedImporter = null;
    super.onButtonCancelPressed();
  }

  private class OptionsTableModel extends DefaultTableModel {
    public OptionsTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      if (column == 0)
        return true;
      else
        return false;
    }
  }

  class RadioButtonActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      int row = ChooseFileImporterDialog.this.jTable1.getSelectedRow();
      String optionName = ChooseFileImporterDialog.this.jTable1.getModel().getValueAt(row, 1).toString();
//      String lowerCaseName = optionName.substring(0, 1).toLowerCase() + optionName.substring(1);
//      ChooseFileImporterDialog.this.selectedImporter = lowerCaseName;
      ChooseFileImporterDialog.this.selectedImporter = optionName;
    }
  }

  class RadioButtonEditor extends DefaultCellEditor implements ItemListener {
    private JRadioButton button;
    private JPanel panel;

    public RadioButtonEditor(JCheckBox checkBox) {
      super(checkBox);
    }

    public Object getCellEditorValue() {
      this.button.removeItemListener(this);
      return this.panel;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if (value == null)
        return null;
      this.button = (JRadioButton) ((JPanel) value).getComponent(0);
      this.button.addItemListener(this);
      this.panel = (JPanel) value;
      return (Component) value;
    }

    public void itemStateChanged(ItemEvent e) {
      super.fireEditingStopped();
    }
  }

  private class RadioButtonRenderer implements TableCellRenderer {
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

      if (value == null)
        return null;
      return (Component) value;
    }
  }
}

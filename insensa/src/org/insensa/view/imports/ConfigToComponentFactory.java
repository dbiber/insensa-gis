package org.insensa.view.imports;

import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.tree.ImmutableNode;
import org.insensa.Environment;

import org.insensa.exceptions.WriteConfigException;
import org.insensa.extensions.Languages;
import org.insensa.extensions.Script;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPFactor;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class ConfigToComponentFactory {

  private static final Logger log = LoggerFactory.getLogger(ConfigToComponentFactory.class);



  public static Configuration parseScript(Script script) {
    RSession rSession = null;
    try {
      if (script.getLanguage() == Languages.R) {
        rSession = new RSession("127.0.0.1");
        RMessageStream stream = rSession.loadOutput(RMessageType.OUTPUT);
        RMessageStreamMonitor.getInstance().registerMessageStream(stream);
        rSession.sourceFile(script.extractFromFile().toFile());
        REXP ret = rSession.eval("insensa.data");
        List<String> nameList = Arrays.asList(((REXPFactor) ret.asList().get("name")).asStrings());
        List<String> typeList = Arrays.asList(((REXPFactor) ret.asList().get("type")).asStrings());
        Configuration conf = new BaseConfiguration();
        conf.addProperty("name", nameList.toArray());
        conf.addProperty("type", typeList.toArray());
        rSession.unloadOuput();
        rSession.getRawConnection().close();
        return conf;
      } else if (script.getLanguage() == Languages.XML) {
        return new Configurations()
            .xml(script.extractFromFile().toString());
      }
    } catch (ConfigurationException | IOException | REngineException | REXPMismatchException e) {
      log.error( "", e);
    } finally {
      if(rSession != null) {
        try {
          rSession.closeAndClear();
        } catch (REXPMismatchException | REngineException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }


  public static Map<JLabel, JComponent> getComponents(Configuration conf) {
    if (conf instanceof HierarchicalConfiguration) {
      return createFromHierarchical((HierarchicalConfiguration) conf);
    } else {
      return createFromBase((BaseConfiguration) conf);
    }
  }

  private static Map<JLabel, JComponent>
  createFromHierarchical(HierarchicalConfiguration settingsConf) {
    Map<JLabel, JComponent> cMap = new LinkedHashMap<>();
    List<HierarchicalConfiguration<ImmutableNode>> varConfs =
        settingsConf.configurationsAt("variable");

    for (HierarchicalConfiguration conf : varConfs) {
      String name = conf.getString("[@name]");
      String rType = conf.getString("[@r-type]");
      Map.Entry<JLabel, JComponent> entry = null;
      if (conf.isEmpty()) {
        entry = createComponent(name, rType);
      } else {
        entry = createComponent(conf);
      }
      cMap.put(entry.getKey(), entry.getValue());
    }
    return cMap;
  }

  private static Map<JLabel, JComponent>
  createFromBase(BaseConfiguration conf) {
    Map<JLabel, JComponent> cMap = new LinkedHashMap<>();
    List<String> names = Arrays.asList(conf.getStringArray("name"));
    List<String> types = Arrays.asList(conf.getStringArray("type"));
    for (int i = 0; i < names.size(); i++) {
      Map.Entry<JLabel, JComponent> entry = createComponent(names.get(i), types.get(i));
      cMap.put(entry.getKey(), entry.getValue());
    }
    return cMap;
  }


//  private static Map.Entry<JLabel, JComponent>
//  createComponentFromHierarchical(HierarchicalConfiguration variableConf) {
//    Map<JLabel, JComponent> cMap = new LinkedHashMap<>();
//  }

  public static Map.Entry<JLabel, JComponent>
  createComponent(HierarchicalConfiguration<ImmutableNode> varConf) {
    String name = varConf.getString("[@name]");
    String rType = varConf.getString("[@r-type]");
    Map.Entry<JLabel, JComponent> entry = createComponent(name, rType);
    if (!varConf.configurationsAt("spinner").isEmpty()) {
      Configuration conf = varConf.configurationsAt("spinner").get(0);
      JSpinner spinner = (JSpinner) entry.getValue();
      if (rType.equals("numeric")) {
        int fromVal = conf.getInt("[@from]");
        int toVal = conf.getInt("[@to]");
        SpinnerModel model = new SpinnerNumberModel();
        ((SpinnerNumberModel) model).setMinimum(fromVal);
        ((SpinnerNumberModel) model).setMaximum(toVal);
        model.setValue(fromVal);
        spinner.setModel(model);
      }
    } else if (!varConf.configurationsAt("filechooser").isEmpty()) {
      Configuration conf = varConf.configurationsAt("filechooser").get(0);
      entry = new AbstractMap.SimpleEntry<>(new JLabel(name),
          createSelectDir(conf));
    }
    return entry;
  }

  public static Map.Entry<JLabel, JComponent> createComponent(String name, String type) {
    JLabel label = new JLabel(name);
    JComponent comp = null;
    switch (type) {
      case "string":
        comp = new JTextField("");
        comp.setName(name);
        break;
      case "numeric":
        comp = new JSpinner();
        comp.setName(name);
        break;
      case "directory":
        comp = new JTextField("");
        comp.setName(name);
        break;
      default:
        comp = new JTextField("UNKNOWN");
        comp.setName(name);
        break;
    }
    return new AbstractMap.SimpleEntry<JLabel, JComponent>(label, comp);
  }

  public static JComponent createSelectDir(Configuration fileChooserConf) {
    String fcType = fileChooserConf.getString("[@type]");
    JPanel panel = new JPanel();
    JButton button = new JButton("Select Dir");
    JTextField textField = new JTextField();
    textField.setColumns(30);
    button.addActionListener(e -> {
      JFileChooser fileChooser = new JFileChooser(new File(Environment
          .getViewProperties().getLastImportedFolderPath()));
      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        textField.setText(fileChooser.getSelectedFile().getAbsolutePath());
        Environment.getViewProperties().setLastImportedFolderPath(fileChooser
            .getSelectedFile().getAbsolutePath());
      }

    });
    FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
    panel.setLayout(layout);
    panel.add(textField);
    panel.add(button);
    return panel;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class ViewFileSetPupup extends JPopupMenu {

  private static final long serialVersionUID = 1945426640480105561L;
  private JMenu itemImport;
  private JMenu itemNew;

  private JMenuItem itemImportFolder;
  private JMenuItem itemImportFile;
  private JMenuItem itemNewFileSet;
  private JMenuItem itemNewCustomFile;

  private String IMAGE_PATH = "images/";


  public ViewFileSetPupup() {
    super();
    this.initItems();
  }

  public JMenuItem getItemImportFile() {
    return this.itemImportFile;
  }

  public JMenuItem getItemImportFolder() {
    return this.itemImportFolder;
  }

  public JMenuItem getItemNewFileSet() {
    return this.itemNewFileSet;
  }

  public JMenuItem getItemNewCustomFile() {
    return itemNewCustomFile;
  }

  private void initItems() {
    this.itemNew = new JMenu("New");
    this.itemImport = new JMenu("Import");

    ResourceBundle bundle = ResourceBundle.getBundle("img");
    this.itemImportFolder = new JMenuItem("Folder", new ImageIcon(bundle.getString("menu.folder.import")));
    this.itemImportFile = new JMenuItem("File", new ImageIcon(bundle.getString("menu.file.import")));
    this.itemNewFileSet = new JMenuItem("File Set", new ImageIcon(bundle.getString("fileset.new")));
    this.itemNewCustomFile = new JMenuItem("Custom File");

    this.itemNew.add(this.itemNewFileSet);
    this.itemNew.add(this.itemNewCustomFile);
    this.itemImport.add(this.itemImportFolder);
    this.itemImport.add(this.itemImportFile);

    this.add(this.itemNew);
    this.addSeparator();
    this.add(this.itemImport);

    this.itemImport.setIcon(new ImageIcon(bundle.getString("menu.import")));

    Dimension dim = this.itemImport.getPreferredSize();
    dim.setSize(dim.getWidth() + 15, dim.getHeight());
    this.itemImport.setPreferredSize(dim);
  }

}

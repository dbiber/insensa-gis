/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import org.apache.commons.io.FilenameUtils;
import org.insensa.GenericGisFileSet;
import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.StackConnection;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;

import java.awt.Component;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;


public class ViewFileSet extends JScrollPane {
  private static final long serialVersionUID = 1L;
  private JTree tree;
  private ViewFileSetPupup fileSetPopup;
  private ViewFileSetFilePopup fileSetFilePopup;
  private ViewFileSetInfoReaderPopup infoReaderPopup;
  private ViewFileSetConnectionPopup connectionPopup;
  private ViewFileSetInfoConnectionPopup infoConnectionPopup;
  private TreeNode[] activeTreePath;

  public ViewFileSet() {
    super();
    this.fileSetPopup = new ViewFileSetPupup();
    this.fileSetFilePopup = new ViewFileSetFilePopup();
    this.infoReaderPopup = new ViewFileSetInfoReaderPopup();
    this.connectionPopup = new ViewFileSetConnectionPopup();
    this.infoConnectionPopup = new ViewFileSetInfoConnectionPopup();
    this.initTree();
  }

  public void addConnectionFileChanger(ConnectionFileChanger connection,
                                       DefaultMutableTreeNode treeNode,
                                       boolean updateNow) throws IOException {
    if (treeNode != null) {
      treeNode.add(new DefaultMutableTreeNode(connection));
      if (updateNow) {
        this.tree.updateUI();
      }
    } else {
      throw new IOException("setConnectionFileChanger: File Tree Node exestiert nicht");
    }
  }

  public void addInfoConnections(InfoConnection infoConnection,
                                 DefaultMutableTreeNode parentTreeNode,
                                 boolean updateNow) {
    Objects.requireNonNull(parentTreeNode);
    parentTreeNode.add(new DefaultMutableTreeNode(infoConnection));
    if (updateNow) {
      this.tree.updateUI();
    }
  }

  /**
   * @param index The index for the new Child or -1 for adding to the End
   */
  public DefaultMutableTreeNode addFileSetIter(int index, DefaultMutableTreeNode parentNode, IGisFileSet fileSet, boolean updateNow) throws IOException {
    DefaultMutableTreeNode selNode;
    DefaultMutableTreeNode newNode;
    if (parentNode == null)
      selNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
    else
      selNode = parentNode;

    newNode = new DefaultMutableTreeNode(fileSet);
    if (index == -1)
      selNode.add(newNode);
    else
      selNode.insert(newNode, index);

    for (IGisFileSet iFileSet: fileSet.getChildSets()) {
      this.addFileSetIter(-1, newNode, iFileSet, updateNow);
    }
    for (IGisFileContainer iFile: fileSet.getFileList()) {
      ConnectionFileChanger con = iFile.getConnectionFileChanger();
      this.addGisFileAndInfoReader(-1, newNode, iFile, updateNow);
    }
    if (updateNow)
      this.tree.updateUI();

    return newNode;
  }

  public void addInfoReader(InfoReader iReader,
                            DefaultMutableTreeNode treeNode,
                            boolean updateNow) throws IOException {
    if (treeNode != null) {
      treeNode.add(new DefaultMutableTreeNode(iReader));
      if (updateNow)
        this.tree.updateUI();
    } else
      throw new IOException("File Tree Node exestiert nicht");
  }

  public void addNode(DefaultMutableTreeNode parentNode, Object childNode) throws IOException {
    if (parentNode != null) {
      parentNode.add(new DefaultMutableTreeNode(childNode));
      this.tree.updateUI();
    } else
      throw new IOException("Tree Node exestiert nicht");
  }

  public void addNode(String fileName, int fileSetIndex) throws IOException {
    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
    DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) rootNode.getChildAt(fileSetIndex);
    if (activeNode != null) {
      activeNode.add(new DefaultMutableTreeNode(fileName));
      this.tree.updateUI();
    } else
      throw new IOException("Tree Node exestiert nicht");
  }

  public void addNodes(List<IGisFileContainer> oList, DefaultMutableTreeNode treeNode) {
    DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) treeNode.getParent();
    if (parentNode != null)
      parentNode.getIndex(treeNode);
    if (treeNode != null)
      for (int i = 0; i < oList.size(); i++) {
        treeNode.add(new DefaultMutableTreeNode(oList.get(i)));
      }
    // tree.expandPath(tree.getPathForRow(fileSetIndex));
    this.tree.expandPath(new TreePath(treeNode.getPath()));
    this.tree.updateUI();
  }

  public void addNodes(List<String> nameList) {
    // DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode)
    // tree.getModel().getRoot().getChildAt(activeFileSet);
    DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) this.tree.getLastSelectedPathComponent();
    for (int i = 0; i < nameList.size(); i++) {
      selNode.add(new DefaultMutableTreeNode(nameList.get(i).toString()));
    }
    this.tree.expandPath(this.tree.getSelectionPath());
    this.tree.updateUI();
  }

  public void addOptionFileChanger(OptionFileChanger option, DefaultMutableTreeNode treeNode, boolean updateNow) throws IOException {
    if (treeNode != null) {
      treeNode.add(new DefaultMutableTreeNode(option));
      if (updateNow == true)
        this.tree.updateUI();
    } else
      throw new IOException("setConnectionFileChanger: File Tree Node exestiert nicht");
  }

  /**
   * @param index The index for the new Child or -1 for adding to the end
   */
  public DefaultMutableTreeNode addGisFileAndInfoReader(int index,
                                                        DefaultMutableTreeNode parentNode,
                                                        IGisFileContainer file,
                                                        boolean updateNow)
      throws IOException {
    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(file);
    if (index == -1) {
      parentNode.add(newNode);
    } else {
      parentNode.insert(newNode, index);
    }
    if (file.isConnection() &&
        !(((ConnectionFileChanger) file.getConnectionFileChanger())
            instanceof StackConnection)) {
      this.addConnectionFileChanger(file.getConnectionFileChanger(), newNode, updateNow);
    }
    for (InfoConnection iterInfoConnection: file.getInfoConnections()) {
      addInfoConnections(iterInfoConnection, newNode, updateNow);
    }
    for (OptionFileChanger iterOption: file
        .getOptions()) {
      this.addOptionFileChanger(iterOption, newNode, updateNow);
    }
    for (InfoReader iterReader: file.getInfoReaders()) {
      this.addInfoReader(iterReader, newNode, updateNow);
    }

    if (updateNow) {
      this.tree.updateUI();
    }
    return newNode;
  }

  /**
   * @param fileSet
   */
  /*
   * public void addTreeRoot( String fileSetName ) { DefaultMutableTreeNode
   * selNode = (DefaultMutableTreeNode)tree.getModel().getRoot();
   * selNode.add(new DefaultMutableTreeNode(fileSetName)); tree.updateUI(); }
   */
  public void addRootFileSet(GenericGisFileSet fileSet) {
    DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
    selNode.add(new DefaultMutableTreeNode(fileSet));
    expandNode(selNode, true);
    this.tree.updateUI();
  }

  /*
   * public void addChildToParentNode(Object parentNode, Object childNode) {
   * DefaultMutableTreeNode rootNode =
   * (DefaultMutableTreeNode)tree.getModel().getRoot(); int nodeCnt =
   * rootNode.getChildCount(); DefaultMutableTreeNode activeNode = rootNode;
   * for(int i=0;i<nodeCnt;i++) { if(((DefaultMutableTreeNode)
   * activeNode.getChildAt(i)).getUserObject().equals(parentNode)) { } } }
   */

  public void close() {
    ((DefaultMutableTreeNode) this.tree.getModel().getRoot()).removeAllChildren();
    this.tree.removeAll();
    this.fileSetFilePopup = null;
    this.fileSetPopup = null;
    this.tree = null;
    this.tree = null;
  }

  public void collapseNode(DefaultMutableTreeNode treeNode, boolean updateNow) {
    this.tree.collapsePath(new TreePath(treeNode.getPath()));
    if (updateNow == true)
      this.tree.updateUI();
  }

  public void expandNode(DefaultMutableTreeNode treeNode, boolean updateNow) {
    this.tree.expandPath(new TreePath(treeNode.getPath()));
    if (updateNow == true)
      this.tree.updateUI();
  }

  public void expandPathChild(TreePath selTreePath, Object child) {
    int rowCnt = this.tree.getRowCount();
    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) selTreePath.getLastPathComponent();
    Object obj = treeNode.getUserObject();
    for (int j = 0; j < rowCnt; j++) {
      TreePath tmpTp = this.tree.getPathForRow(j);
      DefaultMutableTreeNode tmpTreeNode = (DefaultMutableTreeNode) tmpTp.getLastPathComponent();
      Object tmpObj = tmpTreeNode.getUserObject();
      if (tmpObj == obj) {
        if (!this.tree.isExpanded(tmpTp)) {
          this.tree.expandPath(tmpTp);
          rowCnt = this.tree.getRowCount();
          for (int k = 0; k < tmpTreeNode.getChildCount(); k++) {
            DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) tmpTreeNode.getChildAt(k);
            if (childNode.getUserObject() == child) {
              this.tree.expandPath(new TreePath(childNode.getPath()));
              return;
            }
          }
          return;
        }
      }
    }
  }

  /**
   * TODO Nicht Ideal.. Da hiet nur mit toString des letzten Elements
   * verglichen wird Besser waere die Methode refreshExpand in View
   */
  public void expandPaths(TreePath[] selTreePath) {

    for (TreePath tp: selTreePath) {
      int rowCnt = this.tree.getRowCount();
      DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) tp.getLastPathComponent();
      Object obj = treeNode.getUserObject();
      for (int j = 0; j < rowCnt; j++) {
        TreePath tmpTp = this.tree.getPathForRow(j);
        DefaultMutableTreeNode tmpTreeNode = (DefaultMutableTreeNode) tmpTp.getLastPathComponent();
        Object tmpObj = tmpTreeNode.getUserObject();
        if (tmpObj.toString().equals(obj.toString())) {
          if (!this.tree.isExpanded(tmpTp)) {
            this.tree.expandPath(tmpTp);
            break;
          }
        }
      }
    }
  }

  public TreeNode[] getActiveTreePath() {
    return this.activeTreePath;
  }

  public void setActiveTreePath(TreeNode[] activeTreePath) {
    this.activeTreePath = activeTreePath;
  }

  public ViewFileSetConnectionPopup getConnectionPopup() {
    return this.connectionPopup;
  }

  public ViewFileSetInfoConnectionPopup getInfoConnectionPopup() {
    return infoConnectionPopup;
  }

  public ViewFileSetFilePopup getFileSetFilePopup() {
    return this.fileSetFilePopup;
  }

  public ViewFileSetPupup getFileSetPopup() {
    return this.fileSetPopup;
  }

  public JTree getFileSetTree() {
    return this.tree;
  }

  public ViewFileSetInfoReaderPopup getInfoReaderPopup() {
    return this.infoReaderPopup;
  }

  public TreePath getNewTreePath(TreePath treePath) {
    int rowCnt = this.tree.getRowCount();
    if (treePath == null || treePath.getPathCount() <= 0 || !(treePath.getPathComponent(0) instanceof DefaultMutableTreeNode))
      return null;
    for (int j = 0; j < rowCnt; j++) {
      TreePath tmpTp = this.tree.getPathForRow(j);
      if (tmpTp.getPathCount() == treePath.getPathCount()) {
        for (int i = 0; i < tmpTp.getPathCount(); i++) {
          Object userObject = ((DefaultMutableTreeNode) treePath.getPathComponent(i)).getUserObject();
          Object tmpObj = ((DefaultMutableTreeNode) tmpTp.getPathComponent(i)).getUserObject();
          if (userObject.toString().equals(tmpObj.toString())) {
            if (i == tmpTp.getPathCount() - 1)
              return tmpTp;
          } else
            break;
        }
      }
    }
    return null;
  }

  public TreePath getTreePath(Object userObject) {
    int rCnt = this.tree.getRowCount();
    for (int i = 0; i < rCnt; i++) {
      TreePath tmpPath = this.tree.getPathForRow(i);
      if (((DefaultMutableTreeNode) tmpPath.getLastPathComponent()).getUserObject() == userObject)
        return tmpPath;
    }
    return null;
  }

  public TreePath[] getTreePaths(Object[] userObjects) {
    TreePath[] tps = new TreePath[userObjects.length];

    for (int i = 0; i < userObjects.length; i++) {
      tps[i] = this.getTreePath(userObjects[i]);
    }
    return tps;
  }

  private void initTree() {
    this.tree = new JTree(new DefaultMutableTreeNode("test"));
    ToolTipManager.sharedInstance().registerComponent(this.tree);
    this.tree.setCellRenderer(new ViewFileSetRenderer());
    this.tree.setRootVisible(false);
    this.tree.setDragEnabled(true);
    this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    this.tree.setDropMode(DropMode.ON);
    super.setViewportView(this.tree);
  }

  public void refreshSelection(TreePath[] selTreePath) {
    TreePath[] newTreePath = new TreePath[selTreePath.length];
    this.tree.getRowCount();
    for (int i = 0; i < selTreePath.length; i++) {
      newTreePath[i] = this.getNewTreePath(selTreePath[i]);
    }
    this.tree.setSelectionPaths(newTreePath);
  }

  public void removeAllInfoReader(DefaultMutableTreeNode treeNode, boolean updateNow) throws IOException {
    if (treeNode != null) {
      treeNode.removeAllChildren();
      if (updateNow == true)
        this.tree.updateUI();
    } else
      throw new IOException("File Tree Node exestiert nicht");
  }

  public void removeAllInfoReader(int fileIndex, int fileSetIndex, boolean updateNow) throws IOException {
    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
    DefaultMutableTreeNode activeFileSetNode = (DefaultMutableTreeNode) rootNode.getChildAt(fileSetIndex);
    DefaultMutableTreeNode activeFileNode;
    if (activeFileSetNode != null)
      activeFileNode = (DefaultMutableTreeNode) activeFileSetNode.getChildAt(fileIndex);
    else
      throw new IOException("FileSet Tree Node exestiert nicht");

    if (activeFileNode != null) {
      activeFileNode.removeAllChildren();
      if (updateNow == true)
        this.tree.updateUI();
    } else
      throw new IOException("File Tree Node exestiert nicht");
  }

  public void removeAllNodes(DefaultMutableTreeNode treeNode, boolean updateNow) {
    treeNode.removeAllChildren();
    if (updateNow == true)
      this.tree.updateUI();
  }

  public void removeAllNodes(int fileSetIndex, boolean updateNow) {
    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
    DefaultMutableTreeNode fileSetNode = (DefaultMutableTreeNode) rootNode.getChildAt(fileSetIndex);

    fileSetNode.removeAllChildren();
    if (updateNow == true)
      this.tree.updateUI();
  }

  public void removeNodeIter(DefaultMutableTreeNode child) {
    if (child != null) {
      child.removeAllChildren();
      child.removeFromParent();
    }
  }

  public void setEnableConnection(ConnectionFileChanger connection, boolean enable) {
    this.connectionPopup.setEnable(connection, enable);
  }

  public void updateTree() {
    this.tree.updateUI();
  }

  class ViewFileSetRenderer extends DefaultTreeCellRenderer {
    private static final long serialVersionUID = -7128932476107613835L;
    ResourceBundle bundle = ResourceBundle.getBundle("img");
    ImageIcon closedIcon = new ImageIcon(bundle.getString("fileset.closed"));
    ImageIcon openIcon = new ImageIcon(bundle.getString("fileset.opened"));
    ImageIcon rasterFileIcon = new ImageIcon(bundle.getString("file.hardcopy"));
    ImageIcon rasterFileStack = new ImageIcon(bundle.getString("file.stack"));
    ImageIcon rasterFileGrayIcon = new ImageIcon(bundle.getString("file.softlink"));
    ImageIcon rasterFileErrorIcon = new ImageIcon(bundle.getString("file.error"));
    ImageIcon infoReaderReadIcon = new ImageIcon(bundle.getString("inforeader.used"));
    ImageIcon infoReaderIcon = new ImageIcon(bundle.getString("inforeader.unused"));
    ImageIcon rasterFileLocked = new ImageIcon(bundle.getString("file.locked"));
    ImageIcon connectionUsed = new ImageIcon(bundle.getString("connection.used"));
    ImageIcon connectionUnused = new ImageIcon(bundle.getString("connection.unused"));
    ImageIcon infoConnection = new ImageIcon(bundle.getString("infoConnection.used"));
    ImageIcon infoConnectionUnused = new ImageIcon(bundle.getString("infoConnection.unused"));
    ImageIcon optionUnused = new ImageIcon(bundle.getString("option.unused"));
    ImageIcon optionUsed = new ImageIcon(bundle.getString("option.used"));
    private String IMAGE_PATH = "images/";

    public ViewFileSetRenderer() {
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
                                                  boolean expanded, boolean leaf,
                                                  int row, boolean hasFocus) {
      super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
      if (node.getUserObject() instanceof GenericGisFileSet) {
        this.setLeafIcon(this.closedIcon);
        this.setClosedIcon(this.closedIcon);
        this.setOpenIcon(this.openIcon);
        String description = ((GenericGisFileSet) node.getUserObject()).getDescription();
        if (description != null) {
          if (!(description.isEmpty())) {
            this.setToolTipText(description);
          }
        }
      } else if (node.getUserObject() instanceof IGisFileContainer) {
        IGisFileContainer file = (IGisFileContainer) node.getUserObject();
        ConnectionFileChanger conn = file.getConnectionFileChanger();
        if (file.isLocked()) {
          this.setIcon(this.rasterFileLocked);
          return this;
        }
        if (file.getAbsolutePath().equals(file.getAbsOutputDirectoryPath()
            + file.getOutputFileName())) {
          if (file.getGisFile().exists()) {
            this.setIcon(this.rasterFileIcon);
          } else {
            if (file.isConnection()) {
              setIcon(rasterFileStack);
            } else {
              this.setIcon(this.rasterFileGrayIcon);
            }
          }
        } else {
          if (file.getGisFile().exists()) {
            this.setIcon(this.rasterFileGrayIcon);
          } else {
            this.setIcon(this.rasterFileErrorIcon);
          }
        }
      } else if (node.getUserObject() instanceof InfoReader) {
        if (((InfoReader) node.getUserObject()).isUsed()) {
          this.setIcon(this.infoReaderReadIcon);
        } else {
          this.setIcon(this.infoReaderIcon);
        }
      } else if (node.getUserObject() instanceof ConnectionFileChanger) {
        if (node.getUserObject() instanceof StackConnection) {
          setText(FilenameUtils.removeExtension(getText()));
        }
        if (!((ConnectionFileChanger) node.getUserObject()).isUsed()) {
          this.setIcon(this.connectionUnused);
        } else {
          this.setIcon(this.connectionUsed);
        }
      } else if (node.getUserObject() instanceof InfoConnection) {
        if (((InfoConnection)node.getUserObject()).isUsed()) {
          this.setIcon(this.infoConnection);
        } else {
          this.setIcon(this.infoConnectionUnused);
        }
      } else if (node.getUserObject() instanceof OptionFileChanger) {
        if (!((OptionFileChanger) node.getUserObject()).isUsed()) {
          this.setIcon(this.optionUnused);
        } else {
          this.setIcon(this.optionUsed);
        }
      } else {
        this.setToolTipText(null);
      }
      return this;
    }
  }
}

/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.menubar;

import javax.swing.JMenuItem;

/**
 * @author dennis
 *
 */
public class DefaultExtensionItem implements IExtensionItem
{


	private String name;
	

	public DefaultExtensionItem(String name)
	{
		this.name=name;
	}
	/**
	 *
	 * @see org.insensa.view.menubar.IExtensionItem#getMenuItem()
	 */
	@Override
	public JMenuItem getMenuItem()
	{
		return new JMenuItem(name);
	}

}

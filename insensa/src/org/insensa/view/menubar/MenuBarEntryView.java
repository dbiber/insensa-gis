/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.menubar;

import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MenuBarEntryView extends JMenu
{

	private static final long serialVersionUID = 4823647248940459138L;
	private JMenuItem itemShowImage;

	/**
	 * @param name
	 */
	public MenuBarEntryView(String name)
	{
		super(name);

		this.itemShowImage = new JMenuItem("Show Map", new ImageIcon(ResourceBundle.getBundle("img").getString("menu.view.map")));
		// this.add(itemShowImage);

		// Dimension dim = itemShowImage.getPreferredSize();
		// dim.setSize(dim.getWidth()+20, dim.getHeight());
		// itemShowImage.setPreferredSize(dim);

		// itemShowImage.setEnabled(false);
	}

	/**
	 * @return
	 */
	public JMenuItem getItemShowImage()
	{
		return this.itemShowImage;
	}
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.menubar;

import org.insensa.extensions.MenuItemFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MenuBarEntryExtensions extends JMenu {

  private JMenuItem menuItemManager;
  private Map<String, JMenuItem> itemNameMap;

  public MenuBarEntryExtensions(String name) {
    super(name);
    ResourceBundle bundle = ResourceBundle.getBundle("img");

    menuItemManager = new JMenuItem("Manager...");
    add(menuItemManager);

    itemNameMap = new HashMap<>();
    loadExtensionItems();
  }

  public Map<String, JMenuItem> getItemNameMap() {
    return itemNameMap;
  }

  private void loadExtensionItems() {
    MenuItemFactory factory = new MenuItemFactory();
    List<String> itemNames = factory.getExtensionItemNames();
    for (String iName : itemNames) {
      IExtensionItem item = factory.getExtensionItem(iName);
      if (item != null) {
        JMenuItem mItem = item.getMenuItem();
        itemNameMap.put(iName, mItem);
        add(mItem);
      }
    }
  }

  public JMenuItem getMenuItemManager() {
    return menuItemManager;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.menubar;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

public class MenuBarEntryEdit extends JMenu {
  private static final long serialVersionUID = -2501117718672942605L;
  private static String IMAGE_PATH = "images/";
  private JMenuItem itemRefresh;
  private JMenuItem itemRename;
  private JMenuItem itemDelete;
  private JMenu menuAdd;
  private JMenuItem itemAddInfoReader;
  private JMenuItem itemAddOption;
  private JMenuItem itemAddConnection;
  private JMenuItem itemAddAllSensitivityConnections;
  private JMenu menuExecute;
  private JMenuItem itemExecuteInfoReader;
  private JMenuItem itemExecuteOption;
  private JMenuItem itemExecuteConnection;
  private JMenuItem itemExecuteAllConnections;

  public MenuBarEntryEdit(String name) {
    super(name);
    this.initComponents();
  }

  public JMenuItem getItemAddAllSensitivityConnections() {
    return this.itemAddAllSensitivityConnections;
  }

  public JMenuItem getItemAddConnection() {
    return this.itemAddConnection;
  }

  public JMenuItem getItemAddInfoReader() {
    return this.itemAddInfoReader;
  }

  public JMenuItem getItemAddOption() {
    return this.itemAddOption;
  }

  public JMenuItem getItemDelete() {
    return this.itemDelete;
  }

  public JMenuItem getItemExecuteAllConnections() {
    return this.itemExecuteAllConnections;
  }

  public JMenuItem getItemExecuteConnection() {
    return this.itemExecuteConnection;
  }

  public JMenuItem getItemExecuteInfoReader() {
    return this.itemExecuteInfoReader;
  }

  public JMenuItem getItemExecuteOption() {
    return this.itemExecuteOption;
  }

  public JMenuItem getItemRefresh() {
    return this.itemRefresh;
  }

  public JMenuItem getItemRename() {
    return this.itemRename;
  }

  private void initComponents() {
    ResourceBundle bundle = ResourceBundle.getBundle("img");
    this.itemRefresh = new JMenuItem("Refresh", new ImageIcon(bundle.getString("menu.refresh")));
    this.itemRefresh.setMnemonic('R');
    this.itemRefresh.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    this.add(this.itemRefresh);

    this.itemRename = new JMenuItem("Rename", new ImageIcon(bundle.getString("menu.rename")));
    this.itemRename.setMnemonic('n');
    this.itemRename.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
    this.add(this.itemRename);

    this.itemDelete = new JMenuItem("Delete", new ImageIcon(bundle.getString("menu.delete")));
    this.itemDelete.setMnemonic('D');
    //This should not be necesarry because there is also a KeyTreeListener installed which reacts on delete

//		this.itemDelete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
    this.add(this.itemDelete);

    this.add(new JSeparator());

    this.menuAdd = new JMenu("Add");
    this.menuAdd.setIcon(new ImageIcon(bundle.getString("menu.add")));
    this.menuAdd.setMnemonic('A');
    this.add(this.menuAdd);
    this.itemAddInfoReader = new JMenuItem("InfoReader", new ImageIcon(bundle.getString("inforeader.add")));
    this.itemAddInfoReader.setAccelerator(KeyStroke.getKeyStroke('I', InputEvent.CTRL_DOWN_MASK));
    this.menuAdd.add(this.itemAddInfoReader);

    this.itemAddOption = new JMenuItem("Option", new ImageIcon(bundle.getString("option.add")));
    this.itemAddOption.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_DOWN_MASK));
    this.menuAdd.add(this.itemAddOption);

    this.itemAddConnection = new JMenuItem("Connection", new ImageIcon(bundle.getString("connection.add")));
    this.itemAddConnection.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.CTRL_DOWN_MASK));
    this.menuAdd.add(this.itemAddConnection);

    this.itemAddAllSensitivityConnections = new JMenuItem("Add Sensitivity Analysis", new ImageIcon(bundle.getString("menu.sensi.add")));
    this.add(this.itemAddAllSensitivityConnections);

    this.add(new JSeparator());

    this.menuExecute = new JMenu("Execute");
    this.menuExecute.setIcon(new ImageIcon(bundle.getString("menu.execute")));
    this.menuExecute.setMnemonic('E');
    this.add(this.menuExecute);

    this.itemExecuteInfoReader = new JMenuItem("InfoReader", new ImageIcon(bundle.getString("inforeader.execute")));
    this.itemExecuteInfoReader.setAccelerator(KeyStroke.getKeyStroke('I', InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
    this.menuExecute.add(this.itemExecuteInfoReader);

    this.itemExecuteOption = new JMenuItem("Option", new ImageIcon(bundle.getString("option.execute")));
    this.itemExecuteOption.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
    this.menuExecute.add(this.itemExecuteOption);

    this.itemExecuteConnection = new JMenuItem("Connection", new ImageIcon(bundle.getString("connection.execute")));
    this.itemExecuteConnection.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
    this.menuExecute.add(this.itemExecuteConnection);

    this.itemExecuteAllConnections = new JMenuItem("Execute All Connections", new ImageIcon(bundle.getString("connection.execute.all")));
    this.itemExecuteAllConnections.setMnemonic('A');
    this.itemExecuteAllConnections.setAccelerator(KeyStroke.getKeyStroke('A', InputEvent.CTRL_DOWN_MASK));
    this.add(this.itemExecuteAllConnections);
  }

  public void setEnableFileItems(boolean enable) {
    this.itemRename.setEnabled(enable);
    this.itemAddInfoReader.setEnabled(enable);
    this.itemAddOption.setEnabled(enable);
    this.itemAddConnection.setEnabled(enable);
    this.itemExecuteInfoReader.setEnabled(enable);
    this.itemExecuteOption.setEnabled(enable);
  }

  public void setEnableFileSetItems(boolean enable) {
    this.itemRename.setEnabled(enable);
  }

  public void setEnableFilesItems(boolean enable) {
    this.itemAddConnection.setEnabled(enable);
  }

}

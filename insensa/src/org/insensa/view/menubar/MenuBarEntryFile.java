/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.menubar;

import org.insensa.RasterFileContainer;
import org.insensa.view.ViewListener;

import java.awt.event.InputEvent;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTree;
import javax.swing.KeyStroke;


public class MenuBarEntryFile extends JMenu implements ViewListener {

  private static final long serialVersionUID = -3947923386715178492L;
  final JFileChooser folderFileChooser = new JFileChooser();
  private JMenu menuNew;
  private JMenuItem menuItemNewProject;
  private JMenuItem menuItemNewFileSet;
  private JMenuItem menuItemProjects;
  private JMenu menuImport;
  private JMenuItem menuItemImportFolder;
  private JMenuItem menuItemImportFile;
  private JMenu menuExport;
  private JMenuItem menuItemExportFile;
  private JMenuItem menuItemSaveSettings;
  private JMenuItem menuItemClose;

  public MenuBarEntryFile(String name) {
    super(name);
    ResourceBundle bundle = ResourceBundle.getBundle("img");
    this.menuNew = new JMenu("New");
    this.menuNew.setMnemonic('N');
    this.add(this.menuNew);
    this.menuItemNewProject = new JMenuItem("Project");
    this.menuItemNewProject.setIcon(new ImageIcon(bundle.getString("project.new")));
    this.menuItemNewProject.setAccelerator(KeyStroke.getKeyStroke('P', InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
    this.menuNew.add(this.menuItemNewProject);

    this.menuItemNewFileSet = new JMenuItem("FileSet");
    this.menuItemNewFileSet.setIcon(new ImageIcon(bundle.getString("fileset.new")));
    this.menuItemNewFileSet.setAccelerator(KeyStroke.getKeyStroke('N', InputEvent.CTRL_DOWN_MASK));
    this.menuNew.add(this.menuItemNewFileSet);

    this.menuItemProjects = new JMenuItem("Projects...");
    this.menuItemProjects.setMnemonic('P');
    this.menuItemProjects.setIcon(new ImageIcon(bundle.getString("project.open.delete.import")));
    this.menuItemProjects.setAccelerator(KeyStroke.getKeyStroke('P', InputEvent.CTRL_DOWN_MASK));
    this.add(this.menuItemProjects);

    this.add(new JSeparator());

    this.menuImport = new JMenu("Import");
    this.menuImport.setMnemonic('I');
    this.menuImport.setIcon(new ImageIcon(bundle.getString("menu.import")));
    this.add(this.menuImport);

    this.menuItemImportFolder = new JMenuItem("Folder");
    this.menuItemImportFolder.setIcon(new ImageIcon(bundle.getString("menu.folder.import")));
    this.menuItemImportFolder.setAccelerator(KeyStroke.getKeyStroke('F', InputEvent.CTRL_DOWN_MASK));
    this.menuImport.add(this.menuItemImportFolder);

    this.menuItemImportFile = new JMenuItem("File");
    this.menuItemImportFile.setIcon(new ImageIcon(bundle.getString("menu.file.import")));
    this.menuItemImportFile.setAccelerator(KeyStroke.getKeyStroke('F', InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
    this.menuImport.add(this.menuItemImportFile);

    this.menuExport = new JMenu("Export");
    this.menuExport.setMnemonic('E');
    this.menuExport.setIcon(new ImageIcon(bundle.getString("menu.export")));
    this.add(this.menuExport);

    this.menuItemExportFile = new JMenuItem("File");
    this.menuItemExportFile.setIcon(new ImageIcon(bundle.getString("menu.file.export")));
    this.menuExport.add(this.menuItemExportFile);

    this.add(new JSeparator());

    this.menuItemSaveSettings = new JMenuItem("Save Setting", new ImageIcon(bundle.getString("menu.save")));
    this.menuItemSaveSettings.setMnemonic('S');
    this.menuItemSaveSettings.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_DOWN_MASK));
    this.add(this.menuItemSaveSettings);

    this.menuItemClose = new JMenuItem("Close", new ImageIcon(bundle.getString("insensa.view.close")));
    this.menuItemClose.setMnemonic('C');
    this.add(this.menuItemClose);

  }

  public JMenuItem getMenuItemClose() {
    return this.menuItemClose;
  }

  public JMenuItem getMenuItemExportFile() {
    return this.menuItemExportFile;
  }

  public JMenuItem getMenuItemImport() {
    return this.menuImport;
  }

  public JMenuItem getMenuItemImportFile() {
    return this.menuItemImportFile;
  }

  public JMenuItem getMenuItemImportFolder() {
    return this.menuItemImportFolder;
  }

  public JMenuItem getMenuItemNewFileSet() {
    return this.menuItemNewFileSet;
  }

  public JMenuItem getMenuItemNewProject() {
    return this.menuItemNewProject;
  }

  public JMenuItem getMenuItemOpenProject() {
    return this.menuItemProjects;
  }

  public JMenuItem getMenuItemProjects() {
    return this.menuItemProjects;
  }

  public JMenuItem getMenuItemSaveSettings() {
    return this.menuItemSaveSettings;
  }

  @Override
  public void projectClosed() {
  }

  @Override
  public void projectOpened() {
  }

  @Override
  public void treeDeselected() {
  }

  @Override
  public void treeSelected(JTree tree) {
    Object object = tree.getSelectionCount();
    if (object instanceof RasterFileContainer) {
    }
  }

}

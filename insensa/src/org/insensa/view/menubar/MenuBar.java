/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.menubar;

import javax.swing.JMenuBar;

public class MenuBar extends JMenuBar {

  private static final long serialVersionUID = -4236987183611465527L;
  private MenuBarEntryFile menuFile;
  private MenuBarEntryHelp menuHelp;
  private MenuBarEntryEdit menuEdit;
  private MenuBarEntryView menuView;
  private MenuBarEntryExtensions menuExtensions;


  public MenuBar() {
    super();
    this.menuFile = new MenuBarEntryFile("File");
    this.menuEdit = new MenuBarEntryEdit("Edit");
    this.menuView = new MenuBarEntryView("View");
    this.menuHelp = new MenuBarEntryHelp("Help");
    this.menuExtensions = new MenuBarEntryExtensions("Extensions");

    this.add(this.menuFile);
    this.add(this.menuEdit);
    this.add(this.menuView);
    this.add(this.menuExtensions);
    this.add(this.menuHelp);

    this.menuFile.setMnemonic('F');
    this.menuHelp.setMnemonic('H');
    this.menuEdit.setMnemonic('E');
    this.menuExtensions.setMnemonic('x');
    this.menuView.setMnemonic('V');
  }

  public void createMenuItems() {
  }

  public MenuBarEntryEdit getMenuEdit() {
    return this.menuEdit;
  }

  public MenuBarEntryFile getMenuFile() {
    return this.menuFile;
  }

  public MenuBarEntryExtensions getMenuExtensions() {
    return menuExtensions;
  }

  public MenuBarEntryHelp getMenuHelp() {
    return this.menuHelp;
  }

  public MenuBarEntryView getMenuWindow() {
    return this.menuView;
  }

}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.menubar;

import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MenuBarEntryHelp extends JMenu
{

	private static final long serialVersionUID = -7787983608605200107L;
	private JMenuItem menuItemHelp;
	private JMenuItem menuItemAbout;
	private static String IMAGE_PATH = "images/";

	/**
	 * @param name
	 */
	public MenuBarEntryHelp(String name)
	{
		super(name);
		this.menuItemHelp = new JMenuItem("Help");
		this.menuItemHelp.setIcon(new ImageIcon(ResourceBundle.getBundle("img").getString("menu.help")));
		this.menuItemHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		this.add(this.menuItemHelp);

		this.menuItemAbout = new JMenuItem("About");
		this.add(this.menuItemAbout);
	}

	/**
	 * @return
	 */
	public JMenuItem getMenuItemAbout()
	{
		return this.menuItemAbout;
	}

	/**
	 * @return
	 */
	public JMenuItem getMenuItemHelp()
	{
		return this.menuItemHelp;
	}

}

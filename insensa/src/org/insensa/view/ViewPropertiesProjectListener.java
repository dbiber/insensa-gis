/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.io.IOException;


import org.insensa.GenericGisFileSet;
import org.insensa.IProjectListener;
import org.insensa.XMLProperties.XmlViewProperties;
import org.insensa.exceptions.WriteConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ViewPropertiesProjectListener implements IProjectListener
{
	private static final Logger log = LoggerFactory.getLogger(ViewPropertiesProjectListener.class);

	private View view;

	/**
	 * @param properties
	 * @param org.insensa.view
	 */
	public ViewPropertiesProjectListener(XmlViewProperties properties, View view)
	{
		this.view = view;
	}

	/** 
	 * 
	 * @see org.insensa.IProjectListener#childFileSetRenamed(GenericGisFileSet, java.lang.String, java.lang.String)
	 */
	@Override
	public void childFileSetRenamed(GenericGisFileSet fileSet, String oldName, String newName) throws IOException
	{
		// TODO Auto-generated method stub

	}

	/** 
	 * 
	 * @see org.insensa.IProjectListener#fileSetRenamed(GenericGisFileSet, java.lang.String, java.lang.String)
	 */
	@Override
	public void fileSetRenamed(GenericGisFileSet fileSet, String oldName, String newName)
	{

	}

	/** 
	 * 
	 * @see org.insensa.IProjectListener#projectClosed(java.lang.String)
	 */
	@Override
	public void projectClosed(String projectName)
	{

	}

	/** 
	 * 
	 * @see org.insensa.IProjectListener#projectOpened(java.lang.String)
	 */
	@Override
	public void projectOpened(String projectName)
	{

	}

	/** 
	 * 
	 * @see org.insensa.IProjectListener#startClosingProject(java.lang.String)
	 */
	@Override
	public void startClosingProject(String projectName) {
		try {
			this.view.getViewProperties().addExpandList(this.view.getRootExpandetList());
		} catch (WriteConfigException e2) {
			log.error( "", e2);
		}
	}

}

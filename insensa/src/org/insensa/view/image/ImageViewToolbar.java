/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image;

import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class ImageViewToolbar extends JToolBar
{


	private static final long serialVersionUID = 6139553813907753241L;
	private JButton buttonHorizontal;
	private JButton buttonVertical;
	private JButton buttonAll;
	private JButton buttonCollapseAll;
	private JButton buttonExpandAll;
	private JButton buttonCloseAll;

	private JPanel panelToolBar;
	private String IMAGE_PATH = "images/";


	public ImageViewToolbar()
	{
		super();
		this.initToolBar();
	}

	/**
	 * @return
	 */
	public JButton getButtonAll()
	{
		return this.buttonAll;
	}

	/**
	 * @return
	 */
	public JButton getButtonCloseAll()
	{
		return this.buttonCloseAll;
	}

	/**
	 * @return
	 */
	public JButton getButtonCollapseAll()
	{
		return this.buttonCollapseAll;
	}

	/**
	 * @return
	 */
	public JButton getButtonExpandAll()
	{
		return this.buttonExpandAll;
	}

	/**
	 * @return
	 */
	public JButton getButtonHorizontal()
	{
		return this.buttonHorizontal;
	}

	/**
	 * @return
	 */
	public JButton getButtonVertical()
	{
		return this.buttonVertical;
	}


	private void initToolBar()
	{
		this.panelToolBar = new JPanel();
		// ((FlowLayout)panelToolBar.getLayout()).setAlignment(FlowLayout.LEFT);
		this.panelToolBar.setOpaque(false);
		ResourceBundle bundle = ResourceBundle.getBundle("img");
		this.buttonHorizontal = new JButton(new ImageIcon(bundle.getString("toolbar.viewer.arrange.horizontal")));
		this.buttonHorizontal.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.buttonHorizontal.setOpaque(false);
		this.buttonHorizontal.setBorderPainted(false);
		this.buttonHorizontal.setContentAreaFilled(false);
		this.buttonHorizontal.setToolTipText("Arrange Horizontal");

		this.buttonVertical = new JButton(new ImageIcon(bundle.getString("toolbar.viewer.arrange.vertical")));
		this.buttonVertical.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.buttonVertical.setOpaque(false);
		this.buttonVertical.setBorderPainted(false);
		this.buttonVertical.setContentAreaFilled(false);
		this.buttonVertical.setToolTipText("Arrange Vertical");

		this.buttonAll = new JButton(new ImageIcon(bundle.getString("toolbar.viewer.arrange.all")));
		this.buttonAll.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.buttonAll.setOpaque(false);
		this.buttonAll.setBorderPainted(false);
		this.buttonAll.setContentAreaFilled(false);
		this.buttonAll.setToolTipText("Arrange Uniform");

		JSeparator sep;
		sep = new JSeparator(SwingConstants.VERTICAL);
		sep.setPreferredSize(new Dimension(1, 20));

		this.buttonCollapseAll = new JButton(new ImageIcon(bundle.getString("toolbar.viewer.collapse.all")));
		this.buttonCollapseAll.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.buttonCollapseAll.setOpaque(false);
		this.buttonCollapseAll.setBorderPainted(false);
		this.buttonCollapseAll.setContentAreaFilled(false);
		this.buttonCollapseAll.setToolTipText("Collapse All");

		this.buttonExpandAll = new JButton(new ImageIcon(bundle.getString("toolbar.viewer.expand.all")));
		this.buttonExpandAll.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.buttonExpandAll.setOpaque(false);
		this.buttonExpandAll.setBorderPainted(false);
		this.buttonExpandAll.setContentAreaFilled(false);
		this.buttonExpandAll.setToolTipText("Expand All");

		JSeparator sep2;
		sep2 = new JSeparator(SwingConstants.VERTICAL);
		sep2.setPreferredSize(new Dimension(1, 20));

		this.buttonCloseAll = new JButton(new ImageIcon(bundle.getString("toolbar.viewer.close.all")));
		this.buttonCloseAll.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.buttonCloseAll.setOpaque(false);
		this.buttonCloseAll.setBorderPainted(false);
		this.buttonCloseAll.setContentAreaFilled(false);
		this.buttonCloseAll.setToolTipText("Close All");

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(this.panelToolBar);
		this.panelToolBar.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout.createSequentialGroup().addComponent(this.buttonHorizontal).addComponent(this.buttonVertical).addComponent(this.buttonAll)
						.addComponent(sep, 1, 1, 1).addComponent(this.buttonCollapseAll).addComponent(this.buttonExpandAll).addComponent(sep2, 1, 1, 1)
						.addComponent(this.buttonCloseAll).addContainerGap(516, Short.MAX_VALUE)));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout
						.createSequentialGroup()
						.addGroup(
								jPanel2Layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(this.buttonVertical, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(this.buttonHorizontal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(this.buttonAll, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(sep, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(this.buttonCollapseAll, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(this.buttonExpandAll, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(sep2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(this.buttonCloseAll, javax.swing.GroupLayout.Alignment.LEADING)).addContainerGap()));

		this.add(this.panelToolBar);
		this.setFloatable(false);

	}

}

package org.insensa.view.image;


import org.insensa.IGisFileContainer;
import org.insensa.RasterFile;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.view.extensions.AbstractImageView;
import org.insensa.view.extensions.IGisFileView;
import org.insensa.view.extensions.IInfoReaderView;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class RBoxplotTest extends AbstractImageView implements
    IInfoReaderView, IGisFileView {
  private static final Logger log = LoggerFactory.getLogger(RBoxplotTest.class);

  private List<InfoReader> infoReaders;
  private IGisFileContainer rasterFileInformation;
  private Image image;
  private JPanel panel;
  private String rFile = "/home/dennis/dl/Projekte/insensa-gis/r/boxplot.R";

  @Override
  public JComponent getComponent() {
    return panel;
  }

  @Override
  public void init(Dimension componentDim) throws IOException {
    createPanelImage();
    panel = new RImagePanel();
  }

  @Override
  public void refresh() throws IOException {
    createPanelImage();
    resizing(this.parentViewerFrame.getContentPane().getSize());
    panel.invalidate();
    panel.revalidate();
  }

  @Override
  public void resizing(Dimension dim) throws IOException {
    this.panel.setBounds(0, 0, dim.width, dim.height);
  }

  @Override
  public void addInfoReader(InfoReader infoReader) {
    if (infoReaders == null) {
      infoReaders = new ArrayList<>();
    }
    this.infoReaders.add(infoReader);
  }

  @Override
  public void addInfoReader(List<InfoReader> infoReaders) {

  }

  @Override
  public JComponent getDropTargetComponent() {
    return panel;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
    int listCnt = this.infoReaders.size();
    for (Object iObj : objectList)
      if (iObj instanceof InfoReader)
        this.addInfoReader((InfoReader) iObj);
    if (listCnt < this.infoReaders.size())
      this.refresh();
  }

  private void createPanelImage() {
    String device = "png"; // device we'll call (this would work with pretty much any bitmap device)
    String extension = "png";
    // connect to Rserve (if the user specified a server at the command line, use it, otherwise connect locally)
    RConnection rConnection = null;
    try {
      rConnection = new RConnection("127.0.0.1");
      // if Cairo is installed, we can get much nicer graphics, so try to load it
      if (rConnection.parseAndEval("suppressWarnings(require('Cairo',quietly=TRUE))").asInteger() > 0) {
        device = "CairoPNG"; // great, we can use Cairo device
      } else {
        log.warn("(consider installing Cairo package for better bitmap output)");
      }

      // we are careful here - not all R binaries support jpeg
      // so we rather capture any failures
      REXP xp = rConnection.parseAndEval("try(" + device + "('test." + extension + "',res=300,units='px',width=1200,height=1200))");

      if (xp.inherits("try-error")) { // if the result is of the class try-error then there was a problem
        log.error("Can't open " + device + " graphics device:\n" + xp.asString());
        // this is analogous to 'warnings', but for us it's sufficient to get just the 1st warning
        REXP warning = rConnection.eval("if (exists('last.warning') && length(last.warning)>0) names(last.warning)[1] else 0");
        if (warning.isString()) {
          log.error(warning.asString());
        }
        return;
      }

      for (int i = 0; i < infoReaders.size(); i++) {
        CountValues countValues = (CountValues) infoReaders.get(i);
        Map<Float, Long> countMAp = countValues.getCountValues().getMap();
        double[] doubleArray = new double[(int) countValues.getNumOfData()];
        log.debug("Num = " + countValues.getNumOfData());

        int index = 0;
        for (Map.Entry<Float, Long> entry : countMAp.entrySet()) {
          Long count = entry.getValue();
          Float value = entry.getKey();
          for (int j = 0; j < count; j++) {
            doubleArray[index++] = value.doubleValue();
          }
        }

        rConnection.assign("row" + i, doubleArray);
      }
      String[] namesArray = new String[infoReaders.size()];
      StringBuilder vectorToList = new StringBuilder("rowList <- list(");
      for (int i = 0; i < infoReaders.size(); i++) {
        namesArray[i] = infoReaders.get(i).getTargetFile().getName();
        vectorToList.append("row").append(i);
        if (i < infoReaders.size() - 1) {
          vectorToList.append(",");
        }
      }
      vectorToList.append(")");
      log.debug(vectorToList.toString());
      rConnection.eval(vectorToList.toString());
      rConnection.assign("rowNames",namesArray);



      evalAndPrint(rConnection, "source('" + rFile + "')");
      evalAndPrint(rConnection, "dev.off()");
      String wd = rConnection.parseAndEval("getwd()").asString();
      log.debug("working dir = " + wd);
      File file = new File(wd + "/test." + extension + "");
      image = ImageIO.read(file);

      rConnection.close();
    } catch (REngineException | REXPMismatchException | IOException exceptions) {
      try {
        rConnection.parseAndEval("geterrmessage()").toString();
      } catch (REngineException | REXPMismatchException e) {
        e.printStackTrace();
      }
      exceptions.printStackTrace();
    }

  }

  private void evalAndPrint(RConnection rConnection, String command) throws REXPMismatchException, REngineException {
    REXP r = rConnection.parseAndEval("try(" + command + ",silent=TRUE)");
    if (r.inherits("try-error")) {
      log.error("Error: " + r.asString());
    } else { // success ...
    }
  }

  @Override
  public void addGisFileContainer(IGisFileContainer rasterFile) {
    this.rasterFileInformation = rasterFile;
  }

  @Override
  public void addGisFileContainers(List<IGisFileContainer> rasterFileList) {

  }


  public class RImagePanel extends JPanel {
    @Override
    protected void paintComponent(Graphics graphics) {
      super.paintComponent(graphics);
      graphics.drawImage(image, 0, 0, getWidth(), getHeight(), null);
    }
  }
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image;

import org.insensa.view.extensions.ViewerFrame;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JDesktopPane;



public class ImageView extends JDesktopPane// JPanel
{
	private static final long serialVersionUID = -5986170949371190373L;

	private java.util.List<ViewerFrame> imageFrameList;


	public ImageView()
	{
		this.imageFrameList = new ArrayList<ViewerFrame>();
	}

	/** 
	 * 
	 * @see java.awt.Container#add(java.awt.Component)
	 */
	@Override
	public Component add(Component comp)
	{
		return super.add(comp);
	}

	/**
	 * @param imageFrame
	 */
	public void add(ViewerFrame imageFrame)
	{
		this.imageFrameList.add(imageFrame);
		super.add(imageFrame);
	}

}

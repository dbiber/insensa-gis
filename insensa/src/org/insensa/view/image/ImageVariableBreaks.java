/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image;

import org.insensa.helpers.ClassificationRange;
import org.insensa.inforeader.CountValues;
import org.insensa.view.dialogs.infoReader.DialogSetVariableBreaksProperties;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;


public class ImageVariableBreaks extends JComponent implements ImageComponent {

  double[][] data;
  double maxCount = Double.MIN_VALUE;
  double minCount = Double.MAX_VALUE;
  List<XYLineAnnotation> lineList = new ArrayList<>();
  List<ClassificationRange> breakList;
  DialogSetVariableBreaksProperties dialog;
  private int startX;
  private int maxWidth;
  private float maxHeight;
  private float startY;
  private long maxCnt;
  private int widthPerBar;
  private float stepHeight;
  private Dimension pDim;
  private BufferedImage image;
  private CountValues counts;
  private ChartPanel chartPanel;
  private XYPlot plot;

  public ImageVariableBreaks(CountValues lcounts, String applicationTitle,
                             String chartTitle, DialogSetVariableBreaksProperties dialog) {
    this.counts = lcounts;
    this.dialog = dialog;
    this.breakList = new ArrayList<>();
    XYDataset dataset = this.createXYDataset();
    JFreeChart chart = this.createXYChart(dataset, chartTitle);
    this.chartPanel = new ChartPanel(chart);
    this.chartPanel.addChartMouseListener(new myMouseListener());
    this.chartPanel.setPreferredSize(new java.awt.Dimension(500, 500));
    this.chartPanel.setDomainZoomable(false);
    this.chartPanel.setRangeZoomable(false);

  }

  public void addBreakLine(double xVal) {
    XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) this.plot.getRenderer();
    XYLineAnnotation lineAnno = new XYLineAnnotation(xVal, 0, xVal, this.maxCount);
    this.lineList.add(lineAnno);
    xylineandshaperenderer.addAnnotation(lineAnno);
  }

  public void checkPosition() {
    this.createHistogramm(this.pDim);

    int xPos = super.getMousePosition().x;
    int numOfVals = this.counts.getCountValues().size();
    int graphWidht = numOfVals * this.widthPerBar;

    float nthBar = ((float) (xPos - this.startX) * (float) numOfVals) / graphWidht;
    List<Long> cntList = new ArrayList<>();
    cntList.addAll(this.counts.getCountValues().values());
    if ((int) nthBar <= (numOfVals - 1) && (int) nthBar >= 0) {
      Graphics2D graphics = this.image.createGraphics();
      graphics.setColor(Color.magenta);
      float fHeight = (cntList.get((int) nthBar) * this.stepHeight);
      graphics.fillRect(this.startX + (((int) nthBar) * this.widthPerBar) + 1, Math.round(this.startY - fHeight) + 1, this.widthPerBar - 1,
          Math.round(fHeight) - 1);
      this.setImage(this.image);
    }
  }

  public void createHistogramm(Dimension dim) {
    this.pDim = dim;
    Map<Float, Long> countMap = this.counts.getCountValues()
        .getMap();
    this.startX = (dim.width / 100) * 5;
    this.maxWidth = dim.width - (this.startX * 2);
    this.maxHeight = (dim.height / 2);
    this.startY = ((dim.height) / 2) + (this.maxHeight / 2);
    this.maxCnt = Long.MIN_VALUE;
    Iterator<Float> iData = countMap.keySet().iterator();

    // groesste Anzahl eines Wertes
    while (iData.hasNext()) {
      Float data = iData.next();
      if (countMap.get(data) > this.maxCnt)
        this.maxCnt = countMap.get(data);
    }

    this.widthPerBar = this.maxWidth / countMap.size();
    this.stepHeight = this.maxHeight / this.maxCnt;

    BufferedImage tmpImage = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = tmpImage.createGraphics();
    graphics.setBackground(Color.white);
    graphics.clearRect(0, 0, dim.width, dim.height);

    iData = countMap.keySet().iterator();
    int index = 0;
    while (iData.hasNext()) {
      Float data = iData.next();
      long cnt = countMap.get(data);
      float fHeight = cnt * this.stepHeight;
      graphics.setColor(Color.red);
      graphics.fillRect(this.startX + (index * this.widthPerBar), Math.round(this.startY - fHeight), this.widthPerBar, Math.round(fHeight));
      graphics.setColor(Color.black);
      graphics.drawRect(this.startX + (index * this.widthPerBar), Math.round(this.startY - fHeight), this.widthPerBar, Math.round(fHeight));
      index++;
    }
    graphics.setColor(Color.black);
    graphics.drawLine(this.startX, Math.round(this.startY), this.startX + this.maxWidth, Math.round(this.startY));
    graphics.drawLine(this.startX, Math.round(this.startY), this.startX, Math.round(this.startY - this.maxHeight));
    graphics.drawString("Count Values of " + this.counts.getTargetFile().getName(), this.startX, (((dim.height) / 2) - (this.maxHeight / 2)) - 5);

    this.setImage(tmpImage);
  }

  @Override
  public BufferedImage createNewImage(Dimension dim) {
    return null;
  }

  private JFreeChart createXYChart(XYDataset dataset, String title) {
    JFreeChart chart = ChartFactory.createXYLineChart(title,
        "xAchse", "yAchse", dataset, PlotOrientation.VERTICAL,
        true, false, false);
    this.plot = chart.getXYPlot();
    this.plot.setForegroundAlpha(0.5f);
    return chart;
  }

  private XYDataset createXYDataset() {
    DefaultXYDataset result = new DefaultXYDataset();

    this.data = new double[2][this.counts.getCountValues().size()];
    Map<Float, Long> dataMap = this.counts.getCountValues().getMap();
    Iterator<Float> dataF = dataMap.keySet().iterator();
    int index = 0;
    while (dataF.hasNext()) {
      Float valueF = dataF.next();
      double value = valueF.doubleValue();
      double count = dataMap.get(valueF).doubleValue();
      if (count > this.maxCount) {
        this.maxCount = count;
      }
      if (count < this.minCount) {
        this.minCount = count;
      }
      this.data[0][index] = value;
      this.data[1][index] = count;
      index++;
    }
    result.addSeries("val", this.data);
    return result;
  }

  public void deleteBreakLine(int index) {
    XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) this.plot.getRenderer();
    xylineandshaperenderer.removeAnnotation(this.lineList.get(index));
    this.lineList.remove(index);
  }

  @Override
  public JComponent getComponent() {
    return this.chartPanel;
  }

  @Override
  public BufferedImage getImage() {
    return null;
  }

  public void setImage(BufferedImage image) {
    this.image = image;
    this.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
    this.repaint();
    this.invalidate();
  }

  public JComponent getPanel() {
    return this.chartPanel;
  }

  public void markBreakLine(int index, double xVal) {
    XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) this.plot.getRenderer();
    XYLineAnnotation lineAnno = new XYLineAnnotation(xVal, 0, xVal, this.maxCount, new BasicStroke(), Color.gray);
    xylineandshaperenderer.removeAnnotation(this.lineList.get(index));
    xylineandshaperenderer.addAnnotation(lineAnno);
    this.lineList.set(index, lineAnno);
  }

//  public void moveBreakLine(int index, double xVal) {
//    XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) this.plot.getRenderer();
//    XYLineAnnotation lineAnno = new XYLineAnnotation(xVal, 0, xVal, this.maxCount, new BasicStroke(), Color.gray);
//    xylineandshaperenderer.removeAnnotation(this.lineList.get(index));
//    xylineandshaperenderer.addAnnotation(lineAnno);
//    this.lineList.set(index, lineAnno);
//  }

  @Override
  protected void paintComponent(Graphics g) {
    if (this.image != null)
      g.drawImage(this.image, 0, 0, this);
  }

  @Override
  public void refresh(Dimension dim) throws IOException {
  }

  @Override
  public void resizingImage(Dimension d) throws IOException {
  }

  public void unmarkBreakLine(int index, double xVal) {
    XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) this.plot.getRenderer();
    XYLineAnnotation lineAnno = new XYLineAnnotation(xVal, 0, xVal, this.maxCount);
    xylineandshaperenderer.removeAnnotation(this.lineList.get(index));
    xylineandshaperenderer.addAnnotation(lineAnno);
    this.lineList.set(index, lineAnno);
  }

  public class DropInfoListener extends DropTarget implements DropTargetListener {
    private static final long serialVersionUID = 1812093032412534950L;

    @Override
    public synchronized void drop(DropTargetDropEvent dtde) {
    }
  }
//
  class myMouseListener implements ChartMouseListener {

    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
      ChartEntity entity = event.getEntity();
      ImageVariableBreaks.this.plot.getRenderer();
      if (entity == null)
        return;
      if (entity instanceof XYItemEntity) {

        int item = ((XYItemEntity) entity).getItem();
        int series = ((XYItemEntity) entity).getSeriesIndex();
        double xValue = ((XYItemEntity) entity).getDataset().getXValue(series, item);
        ((XYItemEntity) entity).getDataset().getYValue(series, item);
        ImageVariableBreaks.this.dialog.imageClicked(xValue);
      } else {
        Point2D p = ImageVariableBreaks.this.chartPanel.translateScreenToJava2D(event.getTrigger().getPoint());
        Rectangle2D plotArea = ImageVariableBreaks.this.chartPanel.getScreenDataArea();
        XYPlot plot = (XYPlot) event.getChart().getPlot(); // your plot
        double chartX = plot.getDomainAxis().java2DToValue(p.getX(), plotArea, plot.getDomainAxisEdge());
        plot.getRangeAxis().java2DToValue(p.getY(), plotArea, plot.getRangeAxisEdge());

        ImageVariableBreaks.this.dialog.imageClicked(chartX);
      }
    }

    @Override
    public void chartMouseMoved(ChartMouseEvent event) {
    }
  }
}

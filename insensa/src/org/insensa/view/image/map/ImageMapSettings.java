/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * CDialogTemplate.java Created on 13.04.2011, 08:56:42
 */

package org.insensa.view.image.map;

import org.insensa.view.dialogs.SettingsDialog;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;




public class ImageMapSettings extends SettingsDialog// JDialog
{

	private static final long serialVersionUID = 8945374258741683338L;
	private JScrollPane jScrollPane1;
	private JTabbedPane jTabbedPane1;
	private JPanel paneInternal;

	private ImageMapSettingsChooseColorList chooseColorList;
	private IImageMapSettings legendSettings;
	private IImageMap imageMap;
	private JPanel childPanel;

	/**
	 * Creates new form CDialogTemplate.
	 * 
	 * @param parent
	 * @param modal
	 * @param imageMap
	 * @param namesList
	 */
	public ImageMapSettings(Frame parent, boolean modal, IImageMap imageMap, List<String> namesList)
	{
		super(parent, modal);
		this.imageMap = imageMap;
		this.chooseColorList = new ImageMapSettingsChooseColorList(imageMap, namesList);
		this.legendSettings = imageMap.getLegend().getSettingsPanel();
		// legendSettings = new ImageMapSettingsLegend(imageMap.getLegend());
		super.initComponents(this.initComponents());
		super.setHeadTitle("Map Settings");
		super.getButtonOk().addActionListener(ImageMapSettings.this::OkActionListener);
		super.getButtonCancel().addActionListener(ImageMapSettings.this::CancelActionListener);
		super.getButtonHelp().addActionListener(ImageMapSettings.this::HelpActionListener);

		this.jTabbedPane1.addTab("Color", this.chooseColorList);
		if (this.legendSettings != null)
		{
			this.legendSettings.setImageMapSettings(this);
			this.jTabbedPane1.addTab("Legend", this.legendSettings.getPanel());
		}
		List<IImageMapSettings> settingList = imageMap.getImageMapSettings();
		if (settingList != null)
			for (IImageMapSettings iset : settingList)
			{
				iset.setImageMapSettings(this);
				this.jTabbedPane1.add(iset.getSettingsName(), iset.getPanel());
			}
		this.pack();

	}

	private void CancelActionListener(ActionEvent evt)
	{
		this.setVisible(false);
	}

	private void HelpActionListener(ActionEvent evt)
	{
		// TODO add your handling code here:
	}

	private JPanel initComponents()
	{
		this.childPanel = new JPanel();
		this.jScrollPane1 = new javax.swing.JScrollPane();
		this.paneInternal = new javax.swing.JPanel();
		this.jTabbedPane1 = new javax.swing.JTabbedPane();

		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		javax.swing.GroupLayout paneInternalLayout = new javax.swing.GroupLayout(this.childPanel);
		this.childPanel.setLayout(paneInternalLayout);
		paneInternalLayout.setHorizontalGroup(paneInternalLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(this.jTabbedPane1,
						javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE));
		paneInternalLayout.setVerticalGroup(paneInternalLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(this.jTabbedPane1,
						javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE));

		return this.childPanel;
	}

	private void OkActionListener(ActionEvent evt)
	{
		if (this.legendSettings != null)
			this.legendSettings.finish();
		List<Color> colorList = this.chooseColorList.getColorList();
		this.imageMap.setInvert(this.chooseColorList.isInverse());
		this.imageMap.setColorList(colorList);

		try
		{
			((AbstractInfoReaderImageMap)this.imageMap)
					.refresh();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setVisible(false);
	}

	public void setLegendSettings(IImageMapSettings legendSettings)
	{
		if (this.legendSettings != null)
			this.jTabbedPane1.remove(this.legendSettings.getPanel());

		this.legendSettings = legendSettings;
		legendSettings.setImageMapSettings(this);
		this.jTabbedPane1.add(legendSettings.getSettingsName(), legendSettings.getPanel());
	}

}

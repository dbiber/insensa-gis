/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.image.map;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class ColorEditor extends AbstractCellEditor implements TableCellEditor, ActionListener
{

	private static final long serialVersionUID = 3930151182795183239L;
	Color currentColor;
	JButton button;
	JColorChooser colorChooser;
	JDialog dialog;
	protected static final String EDIT = "edit";


	public ColorEditor()
	{
		// Set up the editor (from the table's point of org.insensa.view),
		// which is a button.
		// This button brings up the color chooser dialog,
		// which is the editor from the user's point of org.insensa.view.
		this.button = new JButton();
		this.button.setActionCommand(EDIT);
		this.button.addActionListener(this);
		this.button.setBorderPainted(false);

		// Set up the dialog that the button brings up.
		this.colorChooser = new JColorChooser();
		this.dialog = JColorChooser.createDialog(this.button, "Pick a Color", true, // modal
				this.colorChooser, this, // OK button handler
				null); // no CANCEL button handler
	}

	/**
	 * Handles events from the editor button and from the dialog's OK button.
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (EDIT.equals(e.getActionCommand()))
		{
			// The user has clicked the cell, so
			// bring up the dialog.
			this.button.setBackground(this.currentColor);
			this.colorChooser.setColor(this.currentColor);
			this.dialog.setVisible(true);

			// Make the renderer reappear.
			this.fireEditingStopped();

		} else
		{ // User pressed dialog's "OK" button.
			this.currentColor = this.colorChooser.getColor();
		}
	}

	// Implement the one CellEditor method that AbstractCellEditor doesn't.
	/** 
	 * 
	 * @see javax.swing.CellEditor#getCellEditorValue()
	 */
	public Object getCellEditorValue()
	{
		return this.currentColor;
	}

	// Implement the one method defined by TableCellEditor.
	/** 
	 * 
	 * @see javax.swing.table.TableCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
	 */
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
	{
		this.currentColor = (Color) value;
		return this.button;
	}
}

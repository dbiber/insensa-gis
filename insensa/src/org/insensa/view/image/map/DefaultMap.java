/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.image.map;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdalconst.gdalconstConstants;
import org.insensa.IGisFileContainer;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.helpers.ClassificationRange;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.view.image.ImageComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JProgressBar;

public class DefaultMap extends JComponent implements ImageComponent {
  private static final Logger log = LoggerFactory.getLogger(DefaultMap.class);
  protected List<Color> colorList;
  protected List<ClassificationRange> rangeList;
  protected IGisFileContainer rasterFile;
  protected InfoReader iReader;
  protected BufferedImage origImage;
  protected JProgressBar progressBar;
  private BufferedImage image;

  public DefaultMap(List<Color> colorList,
                    List<ClassificationRange> rangeList,
                    IGisFileContainer rasterFile,
                    InfoReader infoReader,
                    JProgressBar progressBar) {
    this.colorList = colorList;
    this.rangeList = rangeList;
    this.rasterFile = rasterFile;
    this.iReader = infoReader;
    this.progressBar = progressBar;
  }

  /**
   * @param img
   * @param comp
   * @return
   */
  protected Dimension calcOptimalSize(Dimension img, Dimension comp) {
    float duration;
    Dimension optDim = new Dimension();

    if (img.width < comp.width && img.height < comp.height) {
      optDim.width = comp.width;
      duration = (float) comp.width / (float) img.width;
      optDim.height = Math.round(img.height * duration);
      optDim = this.calcOptimalSize(optDim, comp);
    } else if (img.width > comp.width) {
      duration = (float) comp.width / (float) img.width;
      optDim.width = comp.width;
      optDim.height = Math.round(img.height * duration);
      optDim = this.calcOptimalSize(optDim, comp);
    } else if (img.height > comp.height) {
      duration = (float) comp.height / (float) img.height;
      optDim.height = comp.height;
      optDim.width = Math.round(img.width * duration);
      optDim = this.calcOptimalSize(optDim, comp);
    } else
      return img;

    return optDim;
  }

  public void render(Dimension dim) throws IOException {
    BufferedImage image = createNewImage(dim);
    this.origImage = new BufferedImage(image.getWidth(),
        image.getHeight(),
        image.getType());
    this.origImage.getGraphics().drawImage(image, 0, 0, null);
    setImage(image);
  }

  @Override
  public BufferedImage createNewImage(Dimension dim) throws IOException {
    int xSize = ((RasterFile) this.rasterFile.getGisFile()).getNCols();
    int ySize = ((RasterFile) this.rasterFile.getGisFile()).getNRows();
    Dimension optDim = this.calcOptimalSize(new Dimension(xSize, ySize), dim);
    int imgXSize = optDim.width;
    int imgYSize = optDim.height;

    BufferedImage bufImage = new BufferedImage(optDim.width, optDim.height, BufferedImage.TYPE_INT_RGB);

    Band band = ((RasterFile) this.rasterFile.getGisFile()).getBand(RasterFileAccess.READ_ONLY);
    Dataset dataset = ((RasterFile) this.rasterFile.getGisFile()).getDataset(RasterFileAccess.READ_ONLY);

    int xValue = 0;
    int yValue = 0;
    float readValue;
    boolean valueFound = false;
    int fullSize = imgXSize * imgYSize;
    float factor = (float) (100.0 / fullSize);
    float process = 0.0F;
    float dData[] = new float[imgXSize];
    float mul = 1000.0F;
    int maxPrec = 3;
    double newReadValue;
    PrecisionValues precValues = (PrecisionValues) iReader.getTargetFile().getInfoReader(PrecisionValues.NAME);
    if (precValues != null) {
      if (DataTypeHelper.isFloatingDataType(((RasterFile) iReader.getTargetFile().getGisFile())
          .getDataType())) {
        maxPrec = ((PrecisionValues) precValues).getMaxPrecision();
        mul = (float) Math.pow(10.0, maxPrec);
      } else {
        mul = 1.0F;
      }
    }

    float ratioY = (float) band.getYSize() / (float) imgYSize;
    int yNthValue;
    for (int l = 0; l < imgYSize; l++) {
      if (ratioY < 0)
        yNthValue = l;
      else
        yNthValue = (int) Math.floor((float) l * ratioY);
      if (yNthValue > band.getYSize() - 1)
        break;
      dataset.ReadRaster(0, yNthValue, band.getXSize(), 1, imgXSize, 1, gdalconstConstants.GDT_Float32, dData, null);
      try {
        for (float element : dData) {
          process += factor;
          progressBar.setValue(Math.round(process));
          if (xValue >= imgXSize) {
            xValue = 0;
            yValue++;
          }
          readValue = element;

          if (readValue == ((RasterFile) this.rasterFile.getGisFile()).getNoDataValue()) {
            bufImage.setRGB(xValue, yValue, new Color(255, 255, 255).getRGB());
            valueFound = true;
          } else {
            newReadValue = readValue * mul;
            newReadValue = Math.round(newReadValue);
            newReadValue /= mul;
            readValue = (float) newReadValue;

            if (readValue >= this.rangeList.get(0).getLowValue() && readValue <= this.rangeList.get(0).getHighValue()) {
              bufImage.setRGB(xValue, yValue, this.colorList.get(0).getRGB());
              valueFound = true;
            }
            for (int k = 1; k < this.rangeList.size(); k++) {
              if (readValue > this.rangeList.get(k).getLowValue() && readValue <= this.rangeList.get(k).getHighValue()) {
                bufImage.setRGB(xValue, yValue, this.colorList.get(k).getRGB());
                valueFound = true;
              }
            }
          }
          if (!valueFound) {
            log.error("ERROR, point with Value: " + readValue);
          }
          valueFound = false;
          xValue++;
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

    }
    this.rasterFile.getGisFile().unlock();

    return bufImage;
  }

  /**
   * @param width
   * @param height
   */
  public void scaleImage(int width, int height) {
    if (origImage == null)
      return;
    int xSize = origImage.getWidth();
    int ySize = origImage.getHeight();
    Dimension optDim = this.calcOptimalSize(new Dimension(xSize, ySize), new Dimension(width, height));

    BufferedImage newImage2 = new BufferedImage(optDim.width, optDim.height, BufferedImage.TYPE_INT_RGB);
    newImage2.getGraphics().drawImage(origImage, 0, 0, optDim.width, optDim.height, this);
    this.setImage(newImage2);
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    if (this.image != null) {
      graphics.drawImage(this.image, 0, 0, this);
    } else
      super.paintComponent(graphics);
  }

//  /**
//   * @param width
//   * @param height
//   */
//  public void scaleImage(int width, int height)
//  {
//    if (this.origImage == null)
//      return;
//    int xSize = this.origImage.getWidth();
//    int ySize = this.origImage.getHeight();
//    Dimension optDim = this.calcOptimalSize(new Dimension(xSize, ySize), new Dimension(width, height));
//
//    BufferedImage newImage2 = new BufferedImage(optDim.width, optDim.height, BufferedImage.TYPE_INT_RGB);
//    newImage2.getGraphics().drawImage(this.origImage, 0, 0, optDim.width, optDim.height, this);
//    if (this.legend != null && this.rangeList != null && this.colorList != null)
//      try
//      {
//        this.legend.setInvert(this.invert);
//        this.legend.refreshLegend(this.rangeList, this.colorList);
//      } catch (IOException e)
//      {
//        e.printStackTrace();
//      }
//    this.setImage(newImage2);
//  }

  @Override
  public String getToolTipText(MouseEvent event) {
    int x = event.getX();
    int y = event.getY();
    String tooltipText = null;
    if (this.image == null)
      return tooltipText;
    int imgWith = this.image.getWidth();
    int imgHeight = this.image.getHeight();

    if (x > imgWith || x < 0 || y > imgHeight || y < 0) {
      tooltipText = null;
      return tooltipText;
    }
    Color selectedColor = new Color(this.image.getRGB(x, y));
    int indexOf = this.colorList.indexOf(selectedColor);
    if (indexOf > -1)
      tooltipText = "Class: " + indexOf + " ; Low: " + this.rangeList.get(indexOf).getLowValue() + " ; High: "
          + this.rangeList.get(indexOf).getHighValue();
    else
      tooltipText = null;
    return tooltipText;
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public BufferedImage getImage() {
    return image;
  }

  /**
   * @param image
   */
  public void setImage(BufferedImage image) {
    this.image = image;
    this.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
    this.repaint();
    this.invalidate();
  }

  @Override
  public void refresh(Dimension dim) throws IOException {

  }

  @Override
  public void resizingImage(Dimension dim) throws IOException {

  }

  public void setColorList(List<Color> colorList) {
    this.colorList = colorList;
  }
}

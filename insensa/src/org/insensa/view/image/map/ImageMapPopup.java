/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

public class ImageMapPopup extends JPopupMenu
{


	private static final long serialVersionUID = 3594468028630663412L;
	private JMenu save;
	private JMenuItem itemSaveAsPNG;
	private JMenuItem settings;
	private JMenuItem getPointInformation;


	public ImageMapPopup()
	{
		super();
		this.initItems();
	}

	/**
	 * @return
	 */
	public JMenuItem getGetPointInformation()
	{
		return this.getPointInformation;
	}

	/**
	 * @return
	 */
	public JMenuItem getItemSaveAsPNG()
	{
		return this.itemSaveAsPNG;
	}

	/**
	 * @return
	 */
	public JMenuItem getItemSettings()
	{
		return this.settings;
	}


	private void initItems()
	{
		this.save = new JMenu("Save");
		this.itemSaveAsPNG = new JMenuItem("As PNG");
		ResourceBundle bundle = ResourceBundle.getBundle("img");
		this.save.add(this.itemSaveAsPNG);
		this.save.setIcon(new ImageIcon(bundle.getString("menu.save")));
		this.itemSaveAsPNG.setIcon(new ImageIcon(bundle.getString("dialog.title.map")));

		this.add(this.save);
		this.add(new JSeparator());
		this.settings = new JMenuItem("Settings");
		this.add(this.settings);

		this.getPointInformation = new JMenuItem("Get Point Information");
		this.add(this.getPointInformation);
		Dimension dim = this.settings.getPreferredSize();
		dim.setSize(dim.getWidth() + 15, dim.getHeight());
		this.settings.setPreferredSize(dim);
	}

	/**
	 * @param enable
	 */
	public void setEnable(boolean enable)
	{

	}
}

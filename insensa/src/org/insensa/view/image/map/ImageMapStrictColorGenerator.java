/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


/**
 * @author dennis
 *
 */
public class ImageMapStrictColorGenerator extends AbstractColorListGenerator
{

	/**
	 *
	 * @see org.insensa.view.image.map.IColorListGenerator#createColorList()
	 */
	@Override
	public List<Color> createColorList()
	{
		float[] hsbStartColor = Color.RGBtoHSB(startColor.getRed(), startColor.getGreen(), startColor.getBlue(), null);
		float[] hsbEndColor = Color.RGBtoHSB(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), null);
		
		float startHue = hsbStartColor[0]*360.0F;
		float startSaturation = hsbStartColor[1]*100.0F;
		float startBrightness = hsbStartColor[2]*100.0F;
		
		float endHue = hsbEndColor[0]*360.0F;
		float endSaturation = hsbEndColor[1]*100.0F;
		float endBrightness = hsbEndColor[2]*100.0F;
		
		int colorCnt = rangeList.size();
		
		List<Color> colorList = new ArrayList<Color>();

		float percentageHue = 0.0F;
		float percentageSaturation = 0.0F;
		float percentageBrightness = 0.0F;

		percentageSaturation = (endSaturation - startSaturation) / (colorCnt - 1);
		percentageBrightness = (endBrightness - startBrightness) / (colorCnt - 1);
		percentageHue = Math.abs(endHue - startHue) / (colorCnt - 1);
		float tmpStartColor = startHue;
		float tmpStartsat = startSaturation;
		float tmpStartBrit = startBrightness;

		if (inverse == false)
		{
			if (startHue > endHue)
				percentageHue = (endHue + (360.F - startHue)) / (colorCnt - 1);
		}
		if (inverse == true)
		{
			if (startHue < endHue)
				percentageHue = (startHue + (360.F - endHue)) / colorCnt - 1;
		}

		colorList.add(Color.getHSBColor(tmpStartColor / 360.0F, startSaturation / 100.0F, startBrightness / 100.0F));
		for (int i = 1; i < colorCnt - 1; i++)
		{
			if (inverse == true)
			{
				tmpStartColor -= percentageHue;
				if (tmpStartColor < 0)
					tmpStartColor = (360.0F - Math.abs(tmpStartColor));
			} else
			{
				tmpStartColor += percentageHue;
				if (tmpStartColor > 360.0F)
					tmpStartColor = (tmpStartColor - 360.0F);
			}
			tmpStartsat += percentageSaturation;
			tmpStartBrit += percentageBrightness;

			colorList.add(Color.getHSBColor(tmpStartColor / 360.0F, tmpStartsat / 100.0F, tmpStartBrit / 100.0F));
		}
		colorList.add(Color.getHSBColor(endHue / 360.0F, endSaturation / 100.0F, endBrightness / 100.0F));
		return colorList;
		
	}
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import java.awt.Color;
import java.io.IOException;
import java.util.List;

import org.insensa.helpers.ClassificationRange;
import org.insensa.view.image.ImageComponent;


public interface ILegendComponent extends ImageComponent {

  void createColorLegend();

  int getBorderSize();

  void setBorderSize(int borderSize);

  int getPrecision();

  void setPrecision(int maxPrecision);

  IImageMapSettings getSettingsPanel();

  boolean isInvert();

  void setInvert(boolean invert);

  void refreshLegend(List<ClassificationRange> rangeList, List<Color> colorList) throws IOException;

  void setColorList(List<Color> colorList);

  void setRangeList(List<ClassificationRange> rangeList);
}

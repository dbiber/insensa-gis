/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * ChooseColor.java Created on 21.04.2011, 12:12:49
 */

package org.insensa.view.image.map;


import org.insensa.inforeader.PrecisionValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;



public class ImageMapSettingsChooseColorList extends JPanel
{
	private static final Logger log = LoggerFactory.getLogger(ImageMapSettingsChooseColorList.class);

	private class ChooseColorActionListener implements ActionListener
	{
		private JButton button;
		private JColorChooser colorChooser;

		/**
		 * @param button
		 * @param colorChooser
		 */
		public ChooseColorActionListener(JButton button, JColorChooser colorChooser)
		{
			this.button = button;
			this.colorChooser = colorChooser;
		}

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			this.button.setBackground(this.colorChooser.getColor());
		}

	}

	private class SetColorActionListener implements ActionListener
	{
		private JButton button;

		/**
		 * @param button
		 */
		public SetColorActionListener(JButton button)
		{
			this.button = button;
		}

		/** 
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			JColorChooser colorChooser = new JColorChooser();
			JDialog dialog = JColorChooser.createDialog(ImageMapSettingsChooseColorList.this, "Pick a Color", true,  // modal
					colorChooser, new ChooseColorActionListener(this.button, colorChooser),  // OK
																							// button
																							// handler
					null); // no CANCEL button handler
			colorChooser.setColor(this.button.getBackground());
			dialog.setVisible(true);
		}

	}

	class SettingsColorModel extends AbstractTableModel
	{
		private static final long serialVersionUID = 1645357896609832054L;
		private String[] columnNames =
		{ "Range", "Color" };
		private Object[][] data;

		/**
		 * @param data
		 */
		public SettingsColorModel(Object[][] data)
		{
			this.data = data;
		}

		/** 
		 * 
		 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
		 */
		@Override
		public Class<?> getColumnClass(int arg0)
		{
			return this.getValueAt(0, arg0).getClass();
		}

		/** 
		 * 
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		@Override
		public int getColumnCount()
		{
			return this.columnNames.length;
		}

		/** 
		 * 
		 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
		 */
		@Override
		public String getColumnName(int arg0)
		{
			return this.columnNames[arg0];
		}

		/**
		 * @return
		 */
		public String[] getColumnNames()
		{
			return this.columnNames;
		}

		/** 
		 * 
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		@Override
		public int getRowCount()
		{
			return this.data.length;
		}

		/** 
		 * 
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		@Override
		public Object getValueAt(int row, int col)
		{
			if (this.data != null && this.data[row][col] != null)
				return this.data[row][col];
			else
				return null;
		}

		/** 
		 * 
		 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
		 */
		public boolean isCellEditable(int row, int col)
		{
			// Note that the data/cell address is constant,
			// no matter where the cell appears onscreen.
			if (col < 1)
			{
				return false;
			} else
			{
				return true;
			}
		}

		/**
		 * @param data
		 */
		public void setData(Object[][] data)
		{
			this.data = data;
		}

		/** 
		 * 
		 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
		 */
		public void setValueAt(Object value, int row, int col)
		{
			this.data[row][col] = value;
			this.fireTableCellUpdated(row, col);
		}
	}


	private static final long serialVersionUID = -6127863764379740390L;
	private JButton buttonGenerate;
	private JCheckBox checkBoxInverse;
	private JComboBox comboBoxType;
	private JButton buttonStartColor;
	private JButton buttonEndColor;
	private JLabel labelEndColor;
	private JLabel labelStartColor;
	private JLabel labelType;
	private JPanel panelGradient;
	private JScrollPane scrollPaneTable;
	private JTable tableColor;
	private IImageMap imageMap;

	private List<String> namesList;

	private List<Color> colorList;

	private boolean inverse = true;

	/**
	 * Creates new form ChooseColor.
	 * 
	 * @param imageMap
	 * @param namesList
	 */
	public ImageMapSettingsChooseColorList(IImageMap imageMap, List<String> namesList)
	{
		this.imageMap = imageMap;
		this.namesList = namesList;
		this.colorList = imageMap.getColorList();
		this.inverse = imageMap.isInvert();
		this.initComponents();
	}

//	/**
//	 * @param startColor
//	 * @param endColor
//	 * @param startSaturaion
//	 * @param endSaturation
//	 * @param startBrightness
//	 * @param endBrightness
//	 * @param negative
//	 * @return
//	 */
//	public List<Color> createColorListHSB(float startColor, float endColor, float startSaturaion, float endSaturation, float startBrightness,
//			float endBrightness, boolean negative)
//	{
//		List<ClassificationRange> rangeList = this.imageMap.getRangeList();
//		List<Color> colorList = new ArrayList<Color>();
//
//		float percentageHue = 0.0F;
//		float percentageSaturation = 0.0F;
//		float percentageBrightness = 0.0F;
//
//		percentageSaturation = (endSaturation - startSaturaion) / (rangeList.size() - 1);
//		percentageBrightness = (endBrightness - startBrightness) / (rangeList.size() - 1);
//		percentageHue = Math.abs(endColor - startColor) / (rangeList.size() - 1);
//		float tmpStartColor = startColor;
//		float tmpStartsat = startSaturaion;
//		float tmpStartBrit = startBrightness;
//
//		if (negative == false)
//		{
//			if (startColor > endColor)
//				percentageHue = (endColor + (360.F - startColor)) / (rangeList.size() - 1);
//		}
//		if (negative == true)
//		{
//			if (startColor < endColor)
//				percentageHue = (startColor + (360.F - endColor)) / rangeList.size() - 1;
//		}
//
//		colorList.add(Color.getHSBColor(tmpStartColor / 360.0F, startSaturaion / 100.0F, startBrightness / 100.0F));
//		for (int i = 1; i < rangeList.size() - 1; i++)
//		{
//			if (negative == true)
//			{
//				tmpStartColor -= percentageHue;
//				if (tmpStartColor < 0)
//					tmpStartColor = (360.0F - Math.abs(tmpStartColor));
//			} else
//			{
//				tmpStartColor += percentageHue;
//				if (tmpStartColor > 360.0F)
//					tmpStartColor = (tmpStartColor - 360.0F);
//			}
//			tmpStartsat += percentageSaturation;
//			tmpStartBrit += percentageBrightness;
//
//			colorList.add(Color.getHSBColor(tmpStartColor / 360.0F, tmpStartsat / 100.0F, tmpStartBrit / 100.0F));
//		}
//		colorList.add(Color.getHSBColor(endColor / 360.0F, endSaturation / 100.0F, endBrightness / 100.0F));
//		return colorList;
//	}

	/**
	 * @return
	 */
	public List<Color> getColorList()
	{
		List<Color> colorList = new ArrayList<Color>();
		SettingsColorModel model = ((SettingsColorModel) this.tableColor.getModel());
		int count = model.getRowCount();
		for (int i = 0; i < count; i++)
		{
			colorList.add((Color) model.getValueAt(i, 1));
		}
		return colorList;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	private void initComponents()
	{
		this.scrollPaneTable = new javax.swing.JScrollPane();
		this.tableColor = new javax.swing.JTable();
		this.panelGradient = new javax.swing.JPanel();
		this.labelStartColor = new javax.swing.JLabel();
		this.buttonStartColor = new JButton();
		this.labelEndColor = new javax.swing.JLabel();
		this.buttonEndColor = new JButton();
		this.buttonGenerate = new javax.swing.JButton();
		this.labelType = new javax.swing.JLabel();
		this.comboBoxType = new javax.swing.JComboBox();
		this.checkBoxInverse = new javax.swing.JCheckBox();

		// List<Color> colorList = imageMap.getColorList();
		Object[][] data = new Object[this.colorList.size()][2];
		for (int i = 0; i < this.colorList.size(); i++)
		{
			data[i][0] = this.namesList.get(i);
			data[i][1] = this.colorList.get(i);
		}

		this.tableColor.setModel(new SettingsColorModel(data));
		this.tableColor.setDefaultRenderer(Color.class, new ColorRenderer(true));
		this.tableColor.setDefaultEditor(Color.class, new ColorEditor());

		this.scrollPaneTable.setViewportView(this.tableColor);

		this.panelGradient.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Auto Gradient", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

		this.labelStartColor.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelStartColor.setText("Start Color:");

		this.buttonStartColor.setBackground(this.colorList.get(0));
		this.buttonStartColor.setOpaque(true);
		this.buttonStartColor.addActionListener(new SetColorActionListener(this.buttonStartColor));

		this.labelEndColor.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelEndColor.setText("End Color:");

		// labelEndColorPicker.setText(" ");
		this.buttonEndColor.setBackground(this.colorList.get(this.colorList.size() - 1));
		this.buttonEndColor.setOpaque(true);
		this.buttonEndColor.addActionListener(new SetColorActionListener(this.buttonEndColor));

		this.buttonGenerate.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.buttonGenerate.setText("Generate");
		this.buttonGenerate.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				Color backColor = ImageMapSettingsChooseColorList.this.buttonStartColor.getBackground();
				Color endColor = ImageMapSettingsChooseColorList.this.buttonEndColor.getBackground();
				// Color backColor = Color.green;
				// Color endColor = Color.yellow;
				IColorListGenerator colorListGenerator = imageMap.getColorListGenerator();
				if(colorListGenerator==null)
					return;
				colorListGenerator.setEndColor(endColor);
				colorListGenerator.setStartColor(backColor);
				colorListGenerator.setInverse(inverse);
				colorListGenerator.setRangeList(imageMap.getRangeList());
				colorListGenerator.setPrecitionValues((PrecisionValues) imageMap.getInfoReader().getTargetFile().getInfoReader(PrecisionValues.NAME));
				List<Color> colorList;
				try
				{
					colorList = colorListGenerator.createColorList();

//				float[] hsbStartColor = Color.RGBtoHSB(backColor.getRed(), backColor.getGreen(), backColor.getBlue(), null);
//				float[] hsbEndColor = Color.RGBtoHSB(endColor.getRed(), endColor.getGreen(), endColor.getBlue(), null);
//				List<Color> colorList = ImageMapSettingsChooseColorList.this.createColorListHSB(hsbStartColor[0] * 360.0F, hsbEndColor[0] * 360.0F,
//						hsbStartColor[1] * 100.0F, hsbEndColor[1] * 100.0F, hsbStartColor[2] * 100.0F, hsbEndColor[2] * 100.0F,
//						ImageMapSettingsChooseColorList.this.inverse);
				Object[][] data = new Object[colorList.size()][2];
				for (int i = 0; i < colorList.size(); i++)
				{
					data[i][0] = ImageMapSettingsChooseColorList.this.namesList.get(i);
					data[i][1] = colorList.get(i);
				}
				ImageMapSettingsChooseColorList.this.tableColor.setModel(new SettingsColorModel(data));
				} catch (IOException e)
				{
					log.error("UNKNOWN",e);
				}
				// ImageMapSettingsChooseColorList.this.colorList=colorList;
			}
		});

		this.labelType.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelType.setText("Type:");

		this.comboBoxType.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.comboBoxType.setModel(new javax.swing.DefaultComboBoxModel(new String[]
		{ "HSV", "RGB" }));

		// if(inverse=true)
		// checkBoxInverse.setSelected(true);
		this.checkBoxInverse.setSelected(this.inverse);
		this.checkBoxInverse.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.checkBoxInverse.setText("Inverse");
		this.checkBoxInverse.addItemListener(new ItemListener()
		{

			@Override
			public void itemStateChanged(ItemEvent arg0)
			{
				if (arg0.getStateChange() == ItemEvent.DESELECTED)
					ImageMapSettingsChooseColorList.this.inverse = false;
				else
					ImageMapSettingsChooseColorList.this.inverse = true;
			}
		});
		javax.swing.GroupLayout panelGradientLayout = new javax.swing.GroupLayout(this.panelGradient);
		this.panelGradient.setLayout(panelGradientLayout);
		panelGradientLayout.setHorizontalGroup(panelGradientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelGradientLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelGradientLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(this.checkBoxInverse)
										.addComponent(this.buttonGenerate)
										.addGroup(
												panelGradientLayout
														.createSequentialGroup()
														.addGroup(
																panelGradientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.labelStartColor).addComponent(this.labelEndColor)
																		.addComponent(this.labelType))
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
														.addGroup(
																panelGradientLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.comboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(this.buttonEndColor, 99, 99, Short.MAX_VALUE)
																		.addComponent(this.buttonStartColor, 99, 99, Short.MAX_VALUE)))).addContainerGap()));
		panelGradientLayout.setVerticalGroup(panelGradientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelGradientLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelGradientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(this.labelStartColor)
										.addComponent(this.buttonStartColor, GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(
								panelGradientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(this.labelEndColor)
										.addComponent(this.buttonEndColor, GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(
								panelGradientLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(this.labelType)
										.addComponent(this.comboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(this.checkBoxInverse).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
						.addComponent(this.buttonGenerate).addContainerGap()));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.scrollPaneTable, javax.swing.GroupLayout.PREFERRED_SIZE, 250, Short.MAX_VALUE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(this.panelGradient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(this.panelGradient, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(this.scrollPaneTable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												224, Short.MAX_VALUE)).addContainerGap()));
	}

	/**
	 * @return
	 */
	public boolean isInverse()
	{
		return this.inverse;
	}

}

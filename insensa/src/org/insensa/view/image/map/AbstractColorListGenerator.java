/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import java.awt.Color;
import java.util.List;

import org.insensa.helpers.ClassificationRange;
import org.insensa.inforeader.PrecisionValues;


/**
 * @author dennis
 *
 */
public abstract class AbstractColorListGenerator implements IColorListGenerator
{

	protected Color startColor;
	protected Color endColor;
	protected boolean inverse;
	protected List<ClassificationRange> rangeList;
	protected PrecisionValues precisionValues;
	
	/**
	 *
	 * @see org.insensa.view.image.map.IColorListGenerator#setStartColor(java.awt.Color)
	 */
	@Override
	public void setStartColor(Color startColor)
	{
		this.startColor=startColor;
	}

	/**
	 *
	 * @see org.insensa.view.image.map.IColorListGenerator#setEndColor(java.awt.Color)
	 */
	@Override
	public void setEndColor(Color endColor)
	{
		this.endColor=endColor;
	}

	/**
	 *
	 * @see org.insensa.view.image.map.IColorListGenerator#setInverse(boolean)
	 */
	@Override
	public void setInverse(boolean inverse)
	{
		this.inverse=inverse;
	}

	/**
	 *
	 * @see org.insensa.view.image.map.IColorListGenerator#setRangeList(java.util.List)
	 */
	@Override
	public void setRangeList(List<ClassificationRange> rangeList)
	{
		this.rangeList=rangeList;
	}

	/**
	 *
	 * @see org.insensa.view.image.map.IColorListGenerator#setPrecitionValues(org.insensa.inforeader.PrecisionValues)
	 */
	@Override
	public void setPrecitionValues(PrecisionValues precisionValues)
	{
		this.precisionValues=precisionValues;
	}
}

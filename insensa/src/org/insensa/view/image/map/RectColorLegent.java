/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import org.insensa.helpers.ClassificationRange;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLayeredPane;


public class RectColorLegent extends JComponent implements ILegendComponent// ImageComponent
{
  private static final long serialVersionUID = 1511375880045973889L;
  List<ClassificationRange> rangeList;
  private BufferedImage image;
  private Point position = new Point(0, 0);
  private Point offset;
  private Dimension legendSize = new Dimension(0, 0);
  private Dimension size = new Dimension(0, 0);
  private List<Color> colorList;
  private DecimalFormat precisionFormat;
  private String title = null;
  private int borderSize = 5;
  private int hGap = 4;
  private int vGap = 2;
  private int precision = 3;
  private int fontSize = 12;
  private int textWidth;
  private Font font = new Font("Arial", Font.PLAIN, 12);
  private boolean enableRangeLabel = true;
  private Dimension rectColorSize = new Dimension(20, 15);
  private Color backgroundColor = Color.white;

  public RectColorLegent() {
    super.addMouseListener(new legendMouseAdapter());
    super.addMouseMotionListener(new legendMotionListener());
  }

  private BufferedImage addBorder(BufferedImage image) {
    BufferedImage borderedImage = new BufferedImage(this.size.width, this.size.height, image.getType());
    Graphics2D bImageGraph = borderedImage.createGraphics();
    bImageGraph.setBackground(this.backgroundColor);
    bImageGraph.clearRect(0, 0, borderedImage.getWidth(), borderedImage.getHeight());
    bImageGraph.drawImage(image, this.borderSize, this.borderSize, null);
    return borderedImage;
  }

  private void calcSize() {
    FontMetrics fontMetrics = this.getFontMetrics(this.font);

    String rangeString;
    int tmpTextWidth = 0, tmpTextWidth2;
    for (ClassificationRange iRange : this.rangeList) {
      if (this.precisionFormat == null) {
        if (this.isEnableRangeLabel())
          rangeString = new Float(iRange.getLowValue()).toString() + " - " + new Float(iRange.getHighValue()).toString();
        else
          rangeString = new Float(iRange.getHighValue()).toString();
      } else {
        if (this.isEnableRangeLabel())
          rangeString = this.precisionFormat.format(new Float(iRange.getLowValue()).doubleValue()) + " - "
              + this.precisionFormat.format(new Float(iRange.getHighValue()).doubleValue());
        else
          rangeString = this.precisionFormat.format(new Float(iRange.getHighValue()).doubleValue());
      }
      tmpTextWidth2 = fontMetrics.stringWidth(rangeString);
      if (tmpTextWidth2 > tmpTextWidth)
        tmpTextWidth = tmpTextWidth2;
    }
    // sMaxValue=Float.toString(Math.round(maxValue));
    this.textWidth = tmpTextWidth;// fontMetrics.charWidth('_')*(sMaxValue.length()+precision)*2+fontMetrics.stringWidth(" - ");
    this.legendSize.width = this.rectColorSize.width + this.hGap + this.textWidth;
    this.size.width = this.legendSize.width + 2 * this.borderSize;

    this.legendSize.height = this.colorList.size() * (this.rectColorSize.height + this.vGap) - this.vGap;
    this.size.height = this.legendSize.height + (2 * this.borderSize);

  }

  public void createColorLegend() {
    this.calcSize();
    BufferedImage tmpImage = new BufferedImage(this.legendSize.width + 1, this.legendSize.height + 1, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = tmpImage.createGraphics();
    graphics.setBackground(this.backgroundColor);
    graphics.clearRect(0, 0, tmpImage.getWidth(), tmpImage.getHeight());
    int colorIter = 0;
    int y;
    String rangeString = "";
    for (int i = this.colorList.size() - 1; i >= 0; i--) {
      y = (i * (this.rectColorSize.height + this.vGap));
      graphics.setColor(this.colorList.get(colorIter));
      graphics.fillRect(1, y + 1, this.rectColorSize.width - 1, this.rectColorSize.height - 1);

      graphics.setColor(Color.black);
      graphics.drawRect(0, y, this.rectColorSize.width, this.rectColorSize.height);
      graphics.setFont(this.font);
      if (this.precisionFormat == null) {
        if (this.isEnableRangeLabel()) {
          rangeString = new Float(this.rangeList.get(colorIter).getLowValue()).toString() + " - "
              + new Float(this.rangeList.get(colorIter).getHighValue()).toString();
          graphics.drawString(rangeString, this.rectColorSize.width + this.hGap, y + this.rectColorSize.height);
        } else {
          rangeString = new Float(this.rangeList.get(colorIter).getHighValue()).toString();
          graphics.drawString(rangeString, this.rectColorSize.width + this.hGap, y + this.rectColorSize.height);
        }
      } else {
        if (this.isEnableRangeLabel()) {
          rangeString = this.precisionFormat.format(new Float(this.rangeList.get(colorIter).getLowValue()).doubleValue()) + " - "
              + this.precisionFormat.format(new Float(this.rangeList.get(colorIter).getHighValue()).doubleValue());
          graphics.drawString(rangeString, this.rectColorSize.width + this.hGap, y + this.rectColorSize.height);
        } else {
          rangeString = this.precisionFormat.format(new Float(this.rangeList.get(colorIter).getHighValue()).doubleValue());
          graphics.drawString(rangeString, this.rectColorSize.width + this.hGap, y + this.rectColorSize.height);
        }
      }
      colorIter++;
    }

    if (this.borderSize > 0)
      tmpImage = this.addBorder(tmpImage);

    this.setPreferredSize(new Dimension(this.size.width, this.size.height));
    this.setMinimumSize(new Dimension(this.size.width, this.size.height));
    this.image = tmpImage;
  }

  @Override
  public BufferedImage createNewImage(Dimension dim) throws IOException {
    return null;

  }

  public Color getBackgroundColor() {
    return this.backgroundColor;
  }

  public void setBackgroundColor(Color backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  public int getBorderSize() {
    return this.borderSize;
  }

  public void setBorderSize(int borderSize) {
    this.borderSize = borderSize;
  }

  public List<Color> getColorList() {
    return this.colorList;
  }

  public void setColorList(List<Color> colorList) {
    this.colorList = colorList;
  }

  public JComponent getComponent() {
    return this;
  }

  public Font getFont() {
    return this.font;
  }

  public void setFont(Font font) {
    this.font = font;
  }

  public int getFontSize() {
    return this.fontSize;
  }

  public void setFontSize(int fontSize) {
    this.fontSize = fontSize;
  }

  public int gethGap() {
    return this.hGap;
  }

  public void sethGap(int hGap) {
    this.hGap = hGap;
  }

  public BufferedImage getImage() {
    return this.image;
  }

  public Dimension getLegendSize() {
    return this.legendSize;
  }

  public int getPrecision() {
    return this.precision;
  }

  public void setPrecision(int maxPrecision) {
    String sPrecision = "0.";
    String pattern;
    if (maxPrecision > 0) {
      for (int i = 0; i < maxPrecision; i++)
        sPrecision += "#";
      pattern = sPrecision;
    } else
      pattern = "0";

    this.precisionFormat = new DecimalFormat(pattern);
    DecimalFormatSymbols dSymb = this.precisionFormat.getDecimalFormatSymbols();
    dSymb.setDecimalSeparator('.');
    this.precisionFormat.setDecimalFormatSymbols(dSymb);
    this.precisionFormat.setRoundingMode(RoundingMode.HALF_UP);
  }

  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public void setRangeList(List<ClassificationRange> rangeList) {
    this.rangeList = rangeList;
  }

  public Dimension getRectColorSize() {
    return this.rectColorSize;
  }

  public void setRectColorSize(Dimension rectColorSize) {
    this.rectColorSize = rectColorSize;
  }

  @Override
  public IImageMapSettings getSettingsPanel() {
    return new RectColorLegendSettings(this);
  }

  public Dimension getSize() {
    return this.size;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getvGap() {
    return this.vGap;
  }

  public void setvGap(int vGap) {
    this.vGap = vGap;
  }

  public boolean isEnableRangeLabel() {
    return this.enableRangeLabel;
  }

  public void setEnableRangeLabel(boolean enableRangeLabel) {
    this.enableRangeLabel = enableRangeLabel;
  }

  @Override
  public boolean isInvert() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void setInvert(boolean invert) {
    // TODO Auto-generated method stub

  }

  @Override
  protected void paintComponent(Graphics g) {
    if (this.image != null) {
      g.drawImage(this.image, 0, 0, this);
    } else
      super.paintComponent(g);
  }

  @Override
  public void refresh(Dimension dim) throws IOException {
    this.createColorLegend();
    this.resizingImage(dim);
  }

  public void refreshLegend(List<ClassificationRange> rangeList, List<Color> colorList) throws IOException {
    this.rangeList = rangeList;
    this.colorList = colorList;
    // calcSize();
    this.createColorLegend();
    this.resizingImage(null);
  }

  @Override
  public void resizingImage(Dimension dim) throws IOException {
    this.setBounds(this.position.x, this.position.y, this.size.width, this.size.height);
  }

  public class legendMotionListener extends MouseMotionAdapter {

    @Override
    public void mouseDragged(MouseEvent e) {
      if (RectColorLegent.this.getParent() instanceof JLayeredPane) {
        if (RectColorLegent.this.offset != null) {
          Point tmpPoint = new Point(RectColorLegent.this.getX() + (e.getX()
              - RectColorLegent.this.offset.x), RectColorLegent.this.getY()
              + (e.getY() - RectColorLegent.this.offset.y));

          if (tmpPoint.x >= 0 && (tmpPoint.x + RectColorLegent.this.size.width)
              <= RectColorLegent.this.getParent().getSize().width)
            RectColorLegent.this.position.x = tmpPoint.x;
          if (tmpPoint.y >= 0 && (tmpPoint.y + RectColorLegent.this.size.height)
              <= RectColorLegent.this.getParent().getSize().height)
            RectColorLegent.this.position.y = tmpPoint.y;
          RectColorLegent.this.setLocation(RectColorLegent.this.position);
        }

      }
    }
  }

  public class legendMouseAdapter extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      if (RectColorLegent.this.getParent() instanceof JLayeredPane) {
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
      RectColorLegent.this.offset = new Point(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {

      RectColorLegent.this.offset = null;
    }
  }

}

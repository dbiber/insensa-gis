/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * LegendSettings.java Created on 24.06.2011, 14:35:39
 */

package org.insensa.view.image.map;

import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;

import org.insensa.helpers.ClassificationRange;



public class RectColorLegendSettings extends javax.swing.JPanel implements IImageMapSettings
{


	private static final long serialVersionUID = -9029150568606831859L;
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JCheckBox checkBoxBold;
	private javax.swing.JCheckBox checkBoxItalic;
	private javax.swing.JComboBox comboSize;
	private javax.swing.JComboBox comboType;
	private javax.swing.JCheckBox jCheckBox1;
	private javax.swing.JLabel labelBorderSize;
	// private javax.swing.JLabel labelExample;
	private javax.swing.JLabel labelHSpace;
	private javax.swing.JLabel labelSize;
	private javax.swing.JLabel labelStyle;
	private javax.swing.JLabel labelType;
	private javax.swing.JLabel labelVSpace;
	private javax.swing.JPanel panelExample;
	private javax.swing.JPanel panelFont;
	private javax.swing.JPanel panelGeneral;
	private javax.swing.JSpinner spinnerBorderSize;
	private javax.swing.JSpinner spinnerHSpace;
	private javax.swing.JSpinner spinnerVSpace;

	private Integer size;
	private boolean bold;
	private boolean italic;
	private String famlilyName;
	private Font newFont;
	private Integer hSize;
	private Integer vSize;
	private Integer borderSize;
	private boolean enableRangeText;

	private RectColorLegent legend;
	private RectColorLegent exampleLegend;

	/**
	 * Creates new form LegendSettings.
	 * 
	 * @param legend
	 */
	public RectColorLegendSettings(RectColorLegent legend)
	{
		this.legend = legend;
		this.initComponents();
		this.initDefaults();
	}

	/**
	 * @param evt
	 */
	private void checkBoxBoldActionPerformed(java.awt.event.ActionEvent evt)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}

	/**
	 * @param evt
	 */
	private void checkBoxItalicActionPerformed(java.awt.event.ActionEvent evt)
	{
		this.setSettings();
		this.refreshExampleLegend();

	}

	/**
	 * @param e2
	 */
	private void comboSizeActionPerformed(ItemEvent e2)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}

	/**
	 * @param e2
	 */
	private void comboTypeActionPerformed(ItemEvent e2)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}

	/** 
	 * 
	 * @see org.insensa.view.image.map.IImageMapSettings#finish()
	 */
	public void finish()
	{
		this.legend.setFont(this.newFont);
		this.legend.setvGap(this.vSize);
		this.legend.sethGap(this.hSize);
		this.legend.setBorderSize(this.borderSize);
		this.legend.setEnableRangeLabel(this.enableRangeText);
	}

	/** 
	 * 
	 * @see org.insensa.view.image.map.IImageMapSettings#getPanel()
	 */
	@Override
	public JPanel getPanel()
	{
		return this;
	}

	/** 
	 * 
	 * @see org.insensa.view.image.map.IImageMapSettings#getSettingsName()
	 */
	@Override
	public String getSettingsName()
	{
		return "Legend";
	}

	// <editor-fold defaultstate="collapsed" desc="Generated Code">

	private void initComponents()
	{

		this.buttonGroup1 = new javax.swing.ButtonGroup();
		this.panelFont = new javax.swing.JPanel();
		this.comboType = new javax.swing.JComboBox();
		this.labelType = new javax.swing.JLabel();
		this.labelStyle = new javax.swing.JLabel();
		this.checkBoxBold = new javax.swing.JCheckBox();
		this.checkBoxItalic = new javax.swing.JCheckBox();
		this.labelSize = new javax.swing.JLabel();
		this.comboSize = new javax.swing.JComboBox();
		this.panelGeneral = new javax.swing.JPanel();
		this.jCheckBox1 = new javax.swing.JCheckBox();
		this.labelHSpace = new javax.swing.JLabel();
		this.labelVSpace = new javax.swing.JLabel();
		this.spinnerHSpace = new javax.swing.JSpinner();
		this.spinnerVSpace = new javax.swing.JSpinner();
		this.labelBorderSize = new javax.swing.JLabel();
		this.spinnerBorderSize = new javax.swing.JSpinner();
		this.panelExample = new javax.swing.JPanel();
		// labelExample = new javax.swing.JLabel();

		this.panelFont.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Font", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

		this.comboType.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.comboType.setModel(new javax.swing.DefaultComboBoxModel(new String[]
		{ "Arial", "Item 2", "Item 3", "Item 4" }));
		this.comboType.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				RectColorLegendSettings.this.comboTypeActionPerformed(e);
			}
		});

		this.labelType.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelType.setText("Type:");

		this.labelStyle.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelStyle.setText("Style:");

		this.checkBoxBold.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
		this.checkBoxBold.setText("Bold");
		this.checkBoxBold.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				RectColorLegendSettings.this.checkBoxBoldActionPerformed(evt);
			}
		});

		this.checkBoxItalic.setFont(new java.awt.Font("Dialog", 2, 10)); // NOI18N
		this.checkBoxItalic.setText("Italic");
		this.checkBoxItalic.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				RectColorLegendSettings.this.checkBoxItalicActionPerformed(evt);
			}
		});

		this.labelSize.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelSize.setText("Size:");

		this.comboSize.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.comboSize.setModel(new javax.swing.DefaultComboBoxModel(new Integer[]
		{ 5, 8, 10, 12, 14 }));
		this.comboSize.addItemListener(new ItemListener()
		{

			@Override
			public void itemStateChanged(ItemEvent e)
			{
				RectColorLegendSettings.this.comboSizeActionPerformed(e);

			}
		});

		javax.swing.GroupLayout panelFontLayout = new javax.swing.GroupLayout(this.panelFont);
		this.panelFont.setLayout(panelFontLayout);
		panelFontLayout.setHorizontalGroup(panelFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelFontLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelFontLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												panelFontLayout.createSequentialGroup().addComponent(this.labelType)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(this.comboType, 0, 381, Short.MAX_VALUE))
										.addGroup(
												panelFontLayout.createSequentialGroup().addComponent(this.labelStyle)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.checkBoxBold)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(this.checkBoxItalic))
										.addGroup(
												panelFontLayout
														.createSequentialGroup()
														.addComponent(this.labelSize)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(this.comboSize, javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));
		panelFontLayout.setVerticalGroup(panelFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						panelFontLayout
								.createSequentialGroup()
								.addGroup(
										panelFontLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(this.labelType)
												.addComponent(this.comboType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										panelFontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(this.labelStyle)
												.addComponent(this.checkBoxBold).addComponent(this.checkBoxItalic))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										panelFontLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(this.labelSize)
												.addComponent(this.comboSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		this.panelGeneral.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "General", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

		this.jCheckBox1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.jCheckBox1.setText("Enable Range Text");
		this.jCheckBox1.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				RectColorLegendSettings.this.jCheckBox1ActionPerformed(evt);
			}
		});

		this.labelHSpace.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelHSpace.setText("Hor Space:");

		this.labelVSpace.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelVSpace.setText("Ver Space:");

		this.spinnerHSpace.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.spinnerHSpace.setModel(new javax.swing.SpinnerNumberModel(4, 0, 10, 1));
		this.spinnerHSpace.addChangeListener(new javax.swing.event.ChangeListener()
		{
			public void stateChanged(javax.swing.event.ChangeEvent evt)
			{
				RectColorLegendSettings.this.spinnerHSpaceStateChanged(evt);
			}
		});

		this.spinnerVSpace.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.spinnerVSpace.setModel(new javax.swing.SpinnerNumberModel(2, 0, 10, 1));
		this.spinnerVSpace.addChangeListener(new javax.swing.event.ChangeListener()
		{
			public void stateChanged(javax.swing.event.ChangeEvent evt)
			{
				RectColorLegendSettings.this.spinnerVSpaceStateChanged(evt);
			}
		});

		this.labelBorderSize.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.labelBorderSize.setText("Border Size:");

		this.spinnerBorderSize.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
		this.spinnerBorderSize.setModel(new javax.swing.SpinnerNumberModel(5, 0, 20, 1));
		this.spinnerBorderSize.addChangeListener(new javax.swing.event.ChangeListener()
		{
			public void stateChanged(javax.swing.event.ChangeEvent evt)
			{
				RectColorLegendSettings.this.spinnerBorderSizeStateChanged(evt);
			}
		});

		javax.swing.GroupLayout panelGeneralLayout = new javax.swing.GroupLayout(this.panelGeneral);
		this.panelGeneral.setLayout(panelGeneralLayout);
		panelGeneralLayout.setHorizontalGroup(panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelGeneralLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelGeneralLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(this.jCheckBox1)
										.addGroup(
												panelGeneralLayout
														.createSequentialGroup()
														.addGroup(
																panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.labelVSpace).addComponent(this.labelBorderSize)
																		.addComponent(this.labelHSpace))
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
														.addGroup(
																panelGeneralLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(this.spinnerHSpace, javax.swing.GroupLayout.DEFAULT_SIZE, 50,
																				Short.MAX_VALUE)
																		.addComponent(this.spinnerVSpace, javax.swing.GroupLayout.DEFAULT_SIZE, 50,
																				Short.MAX_VALUE)
																		.addComponent(this.spinnerBorderSize, javax.swing.GroupLayout.DEFAULT_SIZE, 50,
																				Short.MAX_VALUE)))).addContainerGap()));
		panelGeneralLayout.setVerticalGroup(panelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelGeneralLayout
						.createSequentialGroup()
						.addComponent(this.jCheckBox1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(
								panelGeneralLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(this.labelHSpace)
										.addComponent(this.spinnerHSpace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(
								panelGeneralLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(this.labelVSpace)
										.addComponent(this.spinnerVSpace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(
								panelGeneralLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(this.labelBorderSize)
										.addComponent(this.spinnerBorderSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)).addContainerGap(29, Short.MAX_VALUE)));

		this.panelExample.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Example", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

		// javax.swing.GroupLayout panelExampleLayout = new
		// javax.swing.GroupLayout(panelExample);
		// panelExample.setLayout(panelExampleLayout);
		// panelExampleLayout.setHorizontalGroup(
		// panelExampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addGap(0, 268, Short.MAX_VALUE)
		// );
		// panelExampleLayout.setVerticalGroup(
		// panelExampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		// .addGap(0, 126, Short.MAX_VALUE)
		// );

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(this.panelFont, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										// .addComponent(labelExample,
										// javax.swing.GroupLayout.DEFAULT_SIZE,
										// 460, Short.MAX_VALUE)
										.addGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												layout.createSequentialGroup()
														.addComponent(this.panelGeneral, javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(this.panelExample, javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.panelFont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(
								layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(this.panelGeneral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(this.panelExample, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						// .addComponent(labelExample)
						.addContainerGap()));
	}


	private void initDefaults()
	{

		Font font = this.legend.getFont();
		String familyName = font.getFamily();
		Integer size = font.getSize();

		GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		this.comboType.setModel(new DefaultComboBoxModel(gEnv.getAvailableFontFamilyNames()));

		this.famlilyName = familyName;
		this.size = size;
		this.bold = font.isBold();
		this.italic = font.isItalic();
		this.borderSize = this.legend.getBorderSize();
		this.hSize = this.legend.gethGap();
		this.vSize = this.legend.getvGap();
		this.enableRangeText = this.legend.isEnableRangeLabel();

		List<ClassificationRange> rangeList = new ArrayList<ClassificationRange>();
		rangeList.addAll(this.legend.getRangeList());

		List<Color> colorList = new ArrayList<Color>();
		colorList.addAll(this.legend.getColorList());

		if (rangeList.size() > 3)
		{
			for (int i = rangeList.size() - 1; i > 3; i--)
			{
				rangeList.remove(i);
				colorList.remove(i);
			}
		}
		this.exampleLegend = new RectColorLegent();
		this.exampleLegend.setColorList(colorList);
		this.exampleLegend.setRangeList(rangeList);
		this.exampleLegend.setEnableRangeLabel(this.enableRangeText);
		this.exampleLegend.createColorLegend();
		this.panelExample.add(this.exampleLegend.getComponent());

		this.jCheckBox1.setSelected(this.enableRangeText);
		this.comboType.setSelectedItem(familyName);
		this.comboSize.setSelectedItem(size);
		this.checkBoxBold.setSelected(font.isBold());
		this.checkBoxItalic.setSelected(font.isItalic());

		// setSettings();
		// refreshExampleLegend();
		// labelExample.setFont(font);

	}

	/**
	 * @param evt
	 */
	private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}


	private void refreshExampleLegend()
	{
		this.exampleLegend.setFont(this.newFont);
		this.exampleLegend.setvGap(this.vSize);
		this.exampleLegend.sethGap(this.hSize);
		this.exampleLegend.setBorderSize(this.borderSize);
		this.exampleLegend.setEnableRangeLabel(this.enableRangeText);
		try
		{
			this.exampleLegend.refresh(this.exampleLegend.getSize());
			this.panelExample.invalidate();
			this.panelExample.revalidate();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/** 
	 * 
	 * @see org.insensa.view.image.map.IImageMapSettings#setImageMapSettings(org.insensa.view.image.map.ImageMapSettings)
	 */
	@Override
	public void setImageMapSettings(ImageMapSettings settings)
	{
		// TODO Auto-generated method stub

	}


	private void setSettings()
	{
		this.famlilyName = (String) this.comboType.getSelectedItem();
		this.size = (Integer) this.comboSize.getSelectedItem();
		this.bold = this.checkBoxBold.isSelected();
		this.italic = this.checkBoxItalic.isSelected();
		if (this.bold == true && this.italic == true)
			this.newFont = new Font(this.famlilyName, Font.BOLD | Font.ITALIC, this.size);
		else if (this.bold == true)
			this.newFont = new Font(this.famlilyName, Font.BOLD, this.size);
		else if (this.italic == true)
			this.newFont = new Font(this.famlilyName, Font.ITALIC, this.size);
		else
			this.newFont = new Font(this.famlilyName, Font.PLAIN, this.size);
		// labelExample.setFont(newFont);

		this.borderSize = (Integer) this.spinnerBorderSize.getValue();
		this.vSize = (Integer) this.spinnerVSpace.getValue();
		this.hSize = (Integer) this.spinnerHSpace.getValue();
		this.enableRangeText = this.jCheckBox1.isSelected();
	}

	/**
	 * @param evt
	 */
	private void spinnerBorderSizeStateChanged(javax.swing.event.ChangeEvent evt)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}

	/**
	 * @param evt
	 */
	private void spinnerHSpaceStateChanged(javax.swing.event.ChangeEvent evt)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}

	/**
	 * @param evt
	 */
	private void spinnerVSpaceStateChanged(javax.swing.event.ChangeEvent evt)
	{
		this.setSettings();
		this.refreshExampleLegend();
	}

}

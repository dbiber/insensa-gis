/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFile;
import org.insensa.helpers.ClassificationRange;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSaveImagePNG;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.extensions.IInfoReaderView;
import org.insensa.view.extensions.IViewer;
import org.insensa.view.extensions.ViewerFrame;
import org.insensa.view.image.ImageComponent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;


public abstract class AbstractInfoReaderImageMap implements IInfoReaderView,
    IViewer, IImageMap {
  private static final long serialVersionUID = -3559400984857844256L;
  protected View view;
  protected InfoReader iReader;
  protected String title;
  protected ILegendComponent legend;
  protected List<Color> colorList;
  protected List<ClassificationRange> rangeList;
  protected IGisFileContainer rasterFile;
  protected DefaultMap map;
  protected JPanel panel;
  protected boolean invert = true;
  protected JProgressBar progressBar;
  protected float startColor = 240.0F;
  protected float endColor = 0.0F;
  protected ImageMapPopup imageMapPopup;
  protected ViewerFrame parent;

  @Override
  public void addInfoReader(InfoReader iReader) {
    if (this.iReader != null)
      return;
    this.iReader = iReader;
    this.rasterFile = iReader.getTargetFile();
  }

  @Override
  public void addInfoReader(List<InfoReader> iReader) {
    return;
  }

  @Override
  public InfoReader getInfoReader() {
    return iReader;
  }

  @Override
  public List<Color> getColorList() {
    return this.colorList;
  }

  @Override
  public void setColorList(List<Color> colorList) {
    this.colorList = colorList;
    map.setColorList(colorList);
  }

  @Override
  public JComponent getComponent() {
    return panel;
  }

  @Override
  public JComponent getDropTargetComponent() {
    return null;
  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

  public ViewerFrame getViewerFrame() {
    return this.parent;
  }

  @Override
  public void setViewerFrame(ViewerFrame parent) {
    this.parent = parent;
  }

  @Override
  public ILegendComponent getLegend() {
    return this.legend;
  }

  public void setLegend(ILegendComponent legend) {
    this.legend = legend;
  }

  @Override
  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public void imagePopupFunction(MouseEvent e, ImageComponent iComp) throws IOException {
    if (e.isPopupTrigger()) {
      this.imageMapPopup.show(e.getComponent(), e.getX(), e.getY());
    }
  }

  @Override
  public void init(Dimension componentDim) throws IOException {
    panel = new JPanel();
    panel.setBackground(Color.WHITE);
    this.progressBar = new JProgressBar();
    this.rangeList = this.createRangeList();
    IColorListGenerator clGenerator = getColorListGenerator();
    clGenerator.setEndColor(Color.getHSBColor(this.endColor / 360.0F, 1.0F, 1.0F));
    clGenerator.setStartColor(Color.getHSBColor(this.startColor / 360.0F, 1.0F, 1.0F));
    clGenerator.setRangeList(rangeList);
    clGenerator.setPrecitionValues((PrecisionValues) iReader.getTargetFile().getInfoReader(PrecisionValues.NAME));
    clGenerator.setInverse(invert);
    this.colorList = clGenerator.createColorList();

    this.map = new DefaultMap(colorList, rangeList, rasterFile, iReader, progressBar);
    this.imageMapPopup = new ImageMapPopup();
    map.addMouseListener(new MouseOnImage());
    map.setToolTipText("text");
    this.imageMapPopup.getItemSaveAsPNG().addActionListener(new SaveImageActionListener());
    this.imageMapPopup.getItemSettings().addActionListener(new OpenSettingsActionListener());

    if (this.legend != null) {
      this.legend.setInvert(this.invert);
      this.legend.setRangeList(this.rangeList);
      this.legend.setColorList(this.colorList);

      InfoReader prec = this.iReader.getTargetFile().getInfoReader(PrecisionValues.NAME);
      if (prec != null) {
        this.legend.setPrecision(((PrecisionValues) prec).getMaxPrecision());
      }

      this.legend.setBorderSize(10);
      this.legend.createColorLegend();
    }
  }

  @Override
  public boolean isInvert() {
    return this.invert;
  }

  @Override
  public void setInvert(boolean invert) {
    this.invert = invert;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {

  }

  @Override
  public void refresh() throws IOException {
    Dimension dimension = this.parent.getPaneDimension();
    this.startView(dimension, dimension);
  }

  @Override
  public void resizing(Dimension dim) throws IOException {
    this.scaleImage(dim.width - ((Component) getLegend()).getWidth(), dim.height);
  }

  public void saveImage(String outputFileName, Dimension dim) throws IOException {
    String legendName;
    if (outputFileName.lastIndexOf(".") <= 0) {
      legendName = outputFileName + "_legend.png";
      outputFileName = outputFileName + ".png";
    } else {
      legendName = outputFileName.substring(0, outputFileName.lastIndexOf(".")) + "_legend.png";
      outputFileName = outputFileName.substring(0, outputFileName.lastIndexOf(".")) + ".png";
    }
    BufferedImage bufImage = map.createNewImage(dim);
    File outputFile = new File(outputFileName);

    ImageIO.write(bufImage, "png", outputFile);
    this.saveLegend(legendName, null);
  }

  public void saveLegend(String outputFileName, Dimension dim) throws IOException {
    BufferedImage legendImage = this.legend.getImage();
    legendImage.createGraphics().setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    File outputFile = new File(outputFileName);
    ImageIO.write(legendImage, "png", outputFile);
  }

  public void scaleImage(int width, int height) {
    map.scaleImage(width, height);
    if (this.legend != null && this.rangeList != null && this.colorList != null)
      try {
        this.legend.setInvert(this.invert);
        this.legend.refreshLegend(this.rangeList, this.colorList);
      } catch (IOException e) {
        e.printStackTrace();
      }
  }

  public DefaultMap getMap() {
    return map;
  }

  @Override
  public void setView(View view) {
    this.view = view;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {
    final Dimension newDim = dim;
    GroupLayout layout = new GroupLayout(panel);
    this.panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        layout.createSequentialGroup().addContainerGap().addComponent(this.progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
            .addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
        javax.swing.GroupLayout.Alignment.TRAILING,
        layout.createSequentialGroup().addContainerGap(105, Short.MAX_VALUE)
            .addComponent(this.progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(68, 68, 68)));

    new SwingWorker<Object, Object>() {
      @Override
      protected Object doInBackground() throws Exception {
        map.render(newDim);
        return null;
      }

      protected void done() {
        AbstractInfoReaderImageMap.this.panel.removeAll();
        panel.setLayout(new BorderLayout());
        panel.add((Component) getLegend(), BorderLayout.WEST);
        panel.add(map, BorderLayout.CENTER);
        AbstractInfoReaderImageMap.this.scaleImage(
            AbstractInfoReaderImageMap.this.parent.getPaneDimension().width
                - ((Component) legend).getWidth(),
            AbstractInfoReaderImageMap.this.parent.getPaneDimension().height);
        panel.revalidate();
      }
    }.execute();
  }

  @Override
  public void saveAsHtml(String filename) throws IOException, BadLocationException {

  }

  @Override
  public IColorListGenerator getColorListGenerator() {
    return new ImageMapStrictColorGenerator();
  }

  @Override
  public SettingsDialog createSettingsDialog() {
    return null;
  }

  @Override
  public JToolBar getToolBar() {
    return null;
  }

  public class GetPointInformationListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {

    }
  }

  public class MouseHoverListener implements MouseMotionListener {

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

  }

  public class MouseOnImage implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent arg0) {

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
      try {
        AbstractInfoReaderImageMap.this.imagePopupFunction(arg0, (ImageComponent) arg0.getSource());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
      try {
        AbstractInfoReaderImageMap.this.imagePopupFunction(arg0, (ImageComponent) arg0.getSource());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  public class OpenSettingsActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      List<String> namesList = new ArrayList<String>();
      for (ClassificationRange iRange : AbstractInfoReaderImageMap.this.rangeList)
        namesList.add(Float.toString(iRange.getLowValue()) + " - " + Float.toString(iRange.getHighValue()));
      new ImageMapSettings(AbstractInfoReaderImageMap.this.view,
          false,
          AbstractInfoReaderImageMap.this,
          namesList)
          .setVisible(true);
    }

  }

  public class SaveImageActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      DialogSaveImagePNG dia = new DialogSaveImagePNG(null, true, new Dimension(0, 0), new Dimension(
          ((RasterFile) AbstractInfoReaderImageMap.this.rasterFile.getGisFile()).getNCols(),
          ((RasterFile) AbstractInfoReaderImageMap.this.rasterFile.getGisFile()).getNRows()));
      dia.setDimension(new Dimension(AbstractInfoReaderImageMap.this.map.getWidth(),
          AbstractInfoReaderImageMap.this.map.getHeight()));
      dia.setVisible(true);
      String fileName = dia.getFileName();
      if (fileName != null) {
        try {
          AbstractInfoReaderImageMap.this.saveImage(fileName, dia.getValDim());
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
    }

  }
}

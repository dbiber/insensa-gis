/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import org.insensa.helpers.ClassificationRange;
import org.insensa.inforeader.InfoReader;

import java.awt.Color;
import java.io.IOException;
import java.util.List;



public interface IImageMap// extends ImageComponent
{
	// ImageMapLegend getLegend( );
	// ImageMapStrechLegend getStrechLegend( );
	/**
	 * @return
	 * @throws IOException
	 */
	List<ClassificationRange> createRangeList() throws IOException;

	/**
	 * @return
	 */
	List<Color> getColorList();

	/**
	 * @return
	 */
	List<IImageMapSettings> getImageMapSettings();

	/**
	 * @return
	 */
	ILegendComponent getLegend();

	/**
	 * @return
	 */
	List<ClassificationRange> getRangeList();

	/**
	 * @return
	 */
	boolean isInvert();

	// void initImageMap( Dimension dim, Dimension componentDimension ) throws
	// IOException;
	/**
	 * @param colorList
	 */
	void setColorList(List<Color> colorList);

	/**
	 * @param invert
	 */
	void setInvert(boolean invert);
	
	/**
	 * 
	 * @return
	 */
	IColorListGenerator getColorListGenerator();
	
	/**
	 * 
	 * @return
	 */
	InfoReader getInfoReader();
}

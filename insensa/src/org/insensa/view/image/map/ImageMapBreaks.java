/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image.map;

import org.insensa.helpers.ClassificationRange;
import org.insensa.inforeader.BreaksValues;

import java.util.List;


public class ImageMapBreaks extends AbstractInfoReaderImageMap
{


	private static final long serialVersionUID = -4668577033781840759L;


	public ImageMapBreaks()
	{
		ILegendComponent legend = new RectColorLegent();
		super.setLegend(legend);
	}

	/** 
	 * 
	 * @see org.insensa.view.image.map.IImageMap#createRangeList()
	 */
	@Override
	public List<ClassificationRange> createRangeList()
	{
		if (this.iReader instanceof BreaksValues)
		{
			return ((BreaksValues) this.iReader).getRangeList();
		}
		return null;
	}

	/** 
	 * 
	 * @see org.insensa.view.image.map.IImageMap#getImageMapSettings()
	 */
	@Override
	public List<IImageMapSettings> getImageMapSettings()
	{
		return null;
	}


}

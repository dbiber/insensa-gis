/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image;

import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.view.View;
import org.insensa.view.dialogs.SettingsDialog;
import org.insensa.view.extensions.IInfoReaderView;
import org.insensa.view.extensions.IViewer;
import org.insensa.view.extensions.ViewerFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TooManyListenersException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JToolBar;


public class ImageChartTestPlugin implements IInfoReaderView, IViewer {

  double[][] data;

  double maxCount = Double.MIN_VALUE;
  private CountValues counts = null;
  private ChartPanel chartPanel;
  private XYPlot plot;
  private String title;
  private List<InfoReader> infoReaderList = new ArrayList<InfoReader>();

  public ImageChartTestPlugin() {
  }

  @Override
  public void addInfoReader(InfoReader iReader) {
    if (this.infoReaderList == null) {
      this.infoReaderList = new ArrayList<>();
    }
    if (!this.infoReaderList.contains(iReader)) {
      this.infoReaderList.add(iReader);
    }
  }

  @Override
  public void addInfoReader(List<InfoReader> iReader) {
    for (InfoReader iIterReader : iReader) {
      this.addInfoReader(iIterReader);
    }
  }

  /**
   * Creates a chart.
   */
  private JFreeChart createXYChart(XYDataset dataset, String title) {

    JFreeChart chart = ChartFactory.createXYLineChart(title,
        "Value", "Count",
        dataset, PlotOrientation.VERTICAL,
        true, false, false);

    this.plot = chart.getXYPlot();
    this.plot.setForegroundAlpha(0.5f);
    return chart;
  }

  private XYDataset createXYDataset() {
    DefaultXYDataset result = new DefaultXYDataset();

    this.data = new double[2][this.counts.getCountValues().size()];
    Map<Float, Long> dataMap = this.counts.getCountValues().getMap();
    Iterator<Float> dataF = dataMap.keySet().iterator();
    int index = 0;
    while (dataF.hasNext()) {
      Float valueF = dataF.next();
      double value = valueF.doubleValue();
      double count = dataMap.get(valueF).doubleValue();
      if (count > this.maxCount)
        this.maxCount = count;
      this.data[0][index] = value;
      this.data[1][index] = count;
      index++;
    }
    result.addSeries(this.title, this.data);
    return result;
  }

  @Override
  public JComponent getComponent() {
    return this.chartPanel;
  }

  @Override
  public JComponent getDropTargetComponent() {
    return null;
  }

  @Override
  public ImageIcon getFrameIcon() {
    return new ImageIcon(ResourceBundle.getBundle("img").getString("dialog.title.chart"));
  }

  @Override
  public ViewerFrame getViewerFrame() {
    return null;
  }

  @Override
  public void setViewerFrame(ViewerFrame parent) {
  }

  public JComponent getPanel() {
    return this.chartPanel;
  }

  @Override
  public void init(Dimension componentDim) throws IOException {
    for (InfoReader iReader : this.infoReaderList) {
      if (iReader instanceof CountValues) {
        this.counts = (CountValues) iReader;
      }
    }
    if (this.counts == null) {
      throw new IOException("No CountValues Found");
    }
    XYDataset dataset = this.createXYDataset();

    JFreeChart chart = this.createXYChart(dataset, "CountValues");

    this.chartPanel = new ChartPanel(chart);
    this.chartPanel.addChartMouseListener(new myMouseListener());

    this.chartPanel.setPreferredSize(new java.awt.Dimension(500, 500));
    this.chartPanel.setBounds(0, 0, 500, 500);

    DropTarget newDropTarget = new DropTarget();
    try {
      newDropTarget.addDropTargetListener(new DropInfoListener());
    } catch (TooManyListenersException e1) {
      e1.printStackTrace();
    }
    this.chartPanel.setDropTarget(newDropTarget);
  }

  @Override
  public void onDropObjects(List<Object> objectList) {
  }

  @Override
  public void resizing(Dimension d) {
    this.chartPanel.setBounds(0, 0, d.width, d.height);
  }

  @Override
  public void setView(View view) {
  }

  @Override
  public void saveAsHtml(String filename) {
  }

  @Override
  public SettingsDialog createSettingsDialog() {
    return null;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) {
  }

  @Override
  public void refresh() {
  }

  @Override
  public JToolBar getToolBar() {
    return null;
  }

  public class DropInfoListener extends DropTarget implements DropTargetListener {
    private static final long serialVersionUID = 1812093032412534950L;

    @Override
    public synchronized void drop(DropTargetDropEvent dtde) {
    }
  }

  class myMouseListener implements ChartMouseListener {

    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
      ChartEntity entity = event.getEntity();
      ImageChartTestPlugin.this.plot.getRenderer();
      if (entity == null)
        return;
      if (entity instanceof XYItemEntity) {

        int item = ((XYItemEntity) entity).getItem();
        int series = ((XYItemEntity) entity).getSeriesIndex();
        double xValue = ((XYItemEntity) entity).getDataset().getXValue(series, item);
        ((XYItemEntity) entity).getDataset().getYValue(series, item);
        new XYLineAnnotation(xValue, 0, xValue, ImageChartTestPlugin.this.maxCount);
      }
    }

    @Override
    public void chartMouseMoved(ChartMouseEvent event) {
    }
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.image;

import org.gdal.gdal.Dataset;
import org.gdal.gdalconst.gdalconstConstants;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.view.extensions.AbstractImageView;
import org.insensa.view.extensions.IGisFileView;
import org.insensa.view.extensions.IViewer;
import org.insensa.view.extensions.ViewerFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.function.LineFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.statistics.Regression;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ExtensionFileFilter;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Paint;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;


public class ScatterPlotViewer extends AbstractImageView implements IGisFileView {
  private List<RasterFileContainer> rasterFileList = new ArrayList<>();
  private int pointCount = 100000;
  private Point position = new Point(0, 0);
  private Point offset;
  private ChartPanel chartPanel;
  private JPanel panel = new JPanel();
  private JLabel label = new JLabel("Two Files Required");
  private JProgressBar progressBar = new JProgressBar();
  private Dimension dimension;
  private double minValue = Double.MAX_VALUE, maxValue = Double.MIN_VALUE;
  private XYDataset regDataset;
  private XYPlot plot;
  private JComponent formularComponent;
  private ViewerFrame imageFrame;
  private JCheckBoxMenuItem menuItemRegLine;
  private JCheckBoxMenuItem menuItemFormular;
  private double[] regValues;

  @Override
  public void addGisFileContainers(List<IGisFileContainer> rasterFileList) {
    for (IGisFileContainer iFile : rasterFileList)
      this.addGisFileContainer(iFile);
  }

  @Override
  public void addGisFileContainer(IGisFileContainer gisFileContainer) {
    RasterFileContainer rasterFile = (RasterFileContainer)gisFileContainer;
    if (this.rasterFileList.isEmpty())
      this.rasterFileList.add(rasterFile);
    else if (this.rasterFileList.size() == 1) {
      if (this.rasterFileList.get(0).getGisFile().getNCols() == rasterFile.getGisFile().getNCols() && this.rasterFileList.get(0).getGisFile().getNRows() == rasterFile.getGisFile().getNRows())
        this.rasterFileList.add(rasterFile);
    }

  }

  public BufferedImage createBufferedImage(JComponent component, int imageType) {
    Dimension componentSize = component.getPreferredSize();
    component.setDoubleBuffered(false);
    component.setSize(componentSize);
    component.addNotify();
    component.validate();

    BufferedImage img = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration()
        .createCompatibleImage(component.getSize().width, component.getSize().height);
    Graphics2D grap = img.createGraphics();
    grap.setColor(Color.WHITE);
    grap.fillRect(0, 0, img.getWidth(), img.getHeight());
    component.print(grap);
    grap.dispose();
    return img;
  }

  private synchronized void createChart() {
    this.progressBar.setStringPainted(true);
    this.progressBar.setString("Loading Chart");
    this.panel.remove(this.label);
    this.panel.add(this.progressBar);
    this.panel.revalidate();
    new SwingWorker<Object, Object>() {
      @Override
      protected Object doInBackground() throws Exception {
        XYDataset dataset = ScatterPlotViewer.this.createData();
        return dataset;
      }

      @Override
      protected void done() {
        XYDataset dataset;
        try {
          dataset = (XYDataset) this.get();

          NumberAxis xAxis = new NumberAxis("X");
          xAxis.setAutoRangeIncludesZero(true);
          NumberAxis yAxis = new NumberAxis("Y");
          yAxis.setAutoRangeIncludesZero(true);

          XYDotRenderer dotRenderer = new XYDotRenderer();
          dotRenderer.setDotWidth(4);
          dotRenderer.setDotHeight(4);
          Paint p = new Color(255, 0, 0, 50);
          dotRenderer.setSeriesPaint(0, p);
          ScatterPlotViewer.this.plot = new XYPlot(dataset, xAxis, yAxis, dotRenderer);

          ScatterPlotViewer.this.regValues = Regression.getOLSRegression(dataset, 0);
          LineFunction2D lineFunc = new LineFunction2D(ScatterPlotViewer.this.regValues[0], ScatterPlotViewer.this.regValues[1]);
          ScatterPlotViewer.this.regDataset = DatasetUtilities.sampleFunction2D(lineFunc, ScatterPlotViewer.this.minValue,
              ScatterPlotViewer.this.maxValue, 100, "RegLine");
          ScatterPlotViewer.this.plot.setDataset(1, ScatterPlotViewer.this.regDataset);
          XYLineAndShapeRenderer lineRenderer = new XYLineAndShapeRenderer(true, false);
          lineRenderer.setSeriesPaint(0, Color.black);
          ScatterPlotViewer.this.plot.setRenderer(1, lineRenderer);

          ScatterPlotViewer.this.plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
          final JFreeChart chart = new JFreeChart(ScatterPlotViewer.this.title, JFreeChart.DEFAULT_TITLE_FONT, ScatterPlotViewer.this.plot, true);
          ScatterPlotViewer.this.chartPanel = new ChartPanel(chart, true) {
            @Override
            public void doSaveAs() throws IOException {
              JFileChooser fileChooser = new JFileChooser();
              String filename;
              String fullFileName = null;
              String formularFileName = null;
              // fileChooser.setCurrentDirectory();
              ExtensionFileFilter filter = new ExtensionFileFilter(localizationResources.getString("PNG_Image_Files"), ".png");
              fileChooser.addChoosableFileFilter(filter);

              int option = fileChooser.showSaveDialog(this);
              if (option == JFileChooser.APPROVE_OPTION) {
                filename = fileChooser.getSelectedFile().getPath();
                if (this.isEnforceFileExtensions()) {
                  if (!filename.endsWith(".png")) {
                    fullFileName = filename + ".png";
                  } else {
                    fullFileName = filename;
                    filename = filename.substring(0, filename.lastIndexOf("."));
                  }
                }
                if (fullFileName == null) {
                  throw new IOException("could not create filename");
                }
                ChartUtilities.saveChartAsPNG(new File(fullFileName), chart, this.getWidth(), this.getHeight());

                if (ScatterPlotViewer.this.formularComponent != null) {
                  formularFileName = filename + "_formular.png";
                  BufferedImage image = ScatterPlotViewer.this.createBufferedImage(ScatterPlotViewer.this.formularComponent,
                      BufferedImage.TYPE_INT_RGB);
                  image.createGraphics().setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                  File outputFile = new File(formularFileName);
                  ImageIO.write(image, "png", outputFile);
                }
              }
            }
          };
          JPopupMenu menu = ScatterPlotViewer.this.chartPanel.getPopupMenu();
          ScatterPlotViewer.this.menuItemRegLine = new JCheckBoxMenuItem("Regression Line");
          ScatterPlotViewer.this.menuItemRegLine.setSelected(true);
          ScatterPlotViewer.this.menuItemRegLine.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              if (ScatterPlotViewer.this.plot.getDataset(1) != null) {
                ScatterPlotViewer.this.plot.setDataset(1, null);
                ScatterPlotViewer.this.menuItemFormular.setEnabled(false);
                if (ScatterPlotViewer.this.formularComponent != null) {
                  ScatterPlotViewer.this.formularComponent = null;
                }
              } else {
                ScatterPlotViewer.this.plot.setDataset(1, ScatterPlotViewer.this.regDataset);
                ScatterPlotViewer.this.menuItemFormular.setEnabled(true);
              }
            }
          });
          menu.add(ScatterPlotViewer.this.menuItemRegLine);

          ScatterPlotViewer.this.menuItemFormular = new JCheckBoxMenuItem("Formular");
          ScatterPlotViewer.this.menuItemFormular.setSelected(false);
          ScatterPlotViewer.this.menuItemFormular.addActionListener(e -> {
            if (ScatterPlotViewer.this.formularComponent == null) {
              ScatterPlotViewer.this.formularComponent = new FormularComponent();
              ((FormularComponent) ScatterPlotViewer.this.formularComponent).setValues(ScatterPlotViewer.this.regValues[0],
                  ScatterPlotViewer.this.regValues[1]);
              ScatterPlotViewer.this.formularComponent.addMouseMotionListener(new LegendMotionListener());
              ScatterPlotViewer.this.formularComponent.addMouseListener(new LegendMouseAdapter());
            } else {
              ScatterPlotViewer.this.formularComponent = null;
            }
          });
          menu.add(ScatterPlotViewer.this.menuItemFormular);
          ScatterPlotViewer.this.panel.remove(ScatterPlotViewer.this.progressBar);
          ScatterPlotViewer.this.panel.add(ScatterPlotViewer.this.chartPanel);
          ScatterPlotViewer.this.panel.revalidate();
        } catch (InterruptedException | ExecutionException e) {
          e.printStackTrace();
        }

      }
    }.execute();
  }

  private synchronized XYDataset createData() throws IOException {
    if (this.rasterFileList.size() < 2) {
      return null;
    }
    DefaultXYDataset result = new DefaultXYDataset();
    XYSeries series = new XYSeries("Series1");

    RasterFileContainer fileX = this.rasterFileList.get(0);
    RasterFileContainer fileY = this.rasterFileList.get(1);

    int xSize = fileX.getGisFile().getNCols();
    int ySize = fileX.getGisFile().getNRows();
    int fullSize = xSize * ySize;
    if (this.pointCount > fullSize)
      this.pointCount = fullSize;
    float rel = (float) (Math.sqrt((double) this.pointCount / (double) fullSize));
    rel = 1.0F / rel;
    int dataXSize = (int) Math.floor((float) xSize / rel);
    int dataYSize = (int) Math.floor((float) ySize / rel);
    int imgSize = dataXSize * dataYSize;
    float[][] tmpData = new float[2][dataXSize];
    Dataset datasetX = ((RasterFile) fileX.getGisFile()).getDataset(RasterFileAccess.READ_ONLY);
    Dataset datasetY = ((RasterFile) fileY.getGisFile()).getDataset(RasterFileAccess.READ_ONLY);
    float process = 0.0F;
    float factor = (float) (100.0 / imgSize);

    int xValue = 0;
    float readValue1, readValue2;
    double dVal1;
    boolean valueFound = false;

    int yNthValue;
    for (int l = 0; l < dataYSize; l++) {
      if (rel < 0)
        yNthValue = l;
      else
        yNthValue = (int) Math.floor((float) l * rel);
      if (yNthValue > ySize - 1)
        break;
      datasetX.ReadRaster(0, yNthValue, xSize, 1, dataXSize, 1, gdalconstConstants.GDT_Float32, tmpData[0], null);
      datasetY.ReadRaster(0, yNthValue, xSize, 1, dataXSize, 1, gdalconstConstants.GDT_Float32, tmpData[1], null);
      try {
        for (int i = 0; i < tmpData[0].length; i++) {
          process += factor;
          this.progressBar.setValue(Math.round(process));

          if (xValue >= dataXSize) {
            xValue = 0;
          }
          readValue1 = tmpData[0][i];
          readValue2 = tmpData[1][i];
          if (readValue1 == fileX.getGisFile().getNoDataValue() || readValue2 == fileY.getGisFile().getNoDataValue()) {
            valueFound = true;
          } else {
            dVal1 = (double) readValue1;
            if (dVal1 < this.minValue)
              this.minValue = dVal1;
            if (dVal1 > this.maxValue)
              this.maxValue = dVal1;
            series.add(dVal1, (double) readValue2);
            valueFound = true;
          }
          valueFound = false;
          xValue++;
        }

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    result.addSeries("result1", series.toArray());
    fileX.getGisFile().unlock();
    return result;
  }

  /**
   * @see IViewer#getComponent()
   */
  @Override
  public JComponent getComponent() {
    return this.panel;
  }

  /**
   * @see IViewer#getDropTargetComponent()
   */
  @Override
  public JComponent getDropTargetComponent() {
    return this.panel;
  }

  /**
   * @see IViewer#getFrameIcon()
   */
  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

//	/**
//	 *
//	 * @see IViewer#getChildComponents()
//	 */
//	@Override
//	public ImageComponent[] getChildComponents()
//	{
//		if (this.formularComponent == null)
//			return null;
//		ImageComponent[] children = new ImageComponent[1];
//		children[0] = (ImageComponent) this.formularComponent;
//		return children;
//	}

  /**
   * @see IViewer#getViewerFrame()
   */
  @Override
  public ViewerFrame getViewerFrame() {
    return this.imageFrame;
  }

  /**
   * @see IViewer#setViewerFrame(ViewerFrame)
   */
  @Override
  public void setViewerFrame(ViewerFrame parent) {
    this.imageFrame = parent;
  }

  /**
   * @see IViewer#init(java.awt.Dimension)
   */
  @Override
  public void init(Dimension componentDim) throws IOException {
    this.dimension = componentDim;
    this.panel.setBounds(0, 0, componentDim.width, componentDim.height);

    if (this.rasterFileList.size() != 2) {
      this.panel.add(this.label);
      return;
    } else
      this.panel.remove(this.label);
    this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.LINE_AXIS));

    this.createChart();
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
    for (Object iObject : objectList)
      if (iObject instanceof RasterFileContainer)
        this.addGisFileContainer((RasterFileContainer) iObject);
    this.init(this.dimension);
  }

  @Override
  public void refresh() throws IOException {

  }

  @Override
  public void resizing(Dimension dim) throws IOException {
    this.dimension = dim;
    this.panel.setBounds(0, 0, dim.width, dim.height);
    this.panel.revalidate();
  }

  /**
   * @see IViewer#startView(java.awt.Dimension, java.awt.Dimension)
   */
  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {

  }

//	/**
//	 *
//	 * @see IViewer#refresh(java.awt.Dimension)
//	 */
//	@Override
//	public void refresh(Dimension dim) throws IOException
//	{
//		// init(dimension);
//	}

  private static class FormularComponent extends JLabel implements ImageComponent {

    /**
     *
     */
    private static final long serialVersionUID = 3495310676589505228L;

    /**
     *
     */
    public FormularComponent() {
      super("y = a + b * x");
      super.setOpaque(false);
      super.setSize(super.getPreferredSize());
    }

    /**
     * @see org.insensa.view.image.ImageComponent#createNewImage(java.awt.Dimension)
     */
    @Override
    public BufferedImage createNewImage(Dimension dim) throws IOException {
      return null;
    }

    /**
     * @see org.insensa.view.image.ImageComponent#getComponent()
     */
    @Override
    public JComponent getComponent() {
      return this;
    }

    /**
     * @see org.insensa.view.image.ImageComponent#getImage()
     */
    @Override
    public BufferedImage getImage() {
      return null;
    }

    /**
     * @see org.insensa.view.image.ImageComponent#refresh(java.awt.Dimension)
     */
    @Override
    public void refresh(Dimension dim) throws IOException {
    }

    /**
     * @see org.insensa.view.image.ImageComponent#resizingImage(java.awt.Dimension)
     */
    @Override
    public void resizingImage(Dimension dim) throws IOException {
    }

    /**
     * @param a
     * @param b
     */
    public void setValues(double a, double b) {
      DecimalFormat format = new DecimalFormat("#.###");
      DecimalFormatSymbols formatSymbols = format.getDecimalFormatSymbols();
      formatSymbols.setDecimalSeparator('.');
      format.setDecimalFormatSymbols(formatSymbols);
      format.setRoundingMode(RoundingMode.HALF_UP);

      String sA = format.format(a);
      String sB = format.format(b);
      int x = 183;
      this.setText("y = " + sB + " " + Character.toString((char) x) + " x + " + sA);
      super.setSize(super.getPreferredSize());
    }
  }

  public class LegendMotionListener extends MouseMotionAdapter {

    /**
     * @see java.awt.event.MouseMotionAdapter#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent e) {
      Dimension size = ScatterPlotViewer.this.formularComponent.getPreferredSize();
      if (ScatterPlotViewer.this.formularComponent.getParent() instanceof JLayeredPane) {
        if (ScatterPlotViewer.this.offset != null) {
          Point tmpPoint = new Point(ScatterPlotViewer.this.formularComponent.getX() + (e.getX() - ScatterPlotViewer.this.offset.x),
              ScatterPlotViewer.this.formularComponent.getY() + (e.getY() - ScatterPlotViewer.this.offset.y));

          if (tmpPoint.x >= 0 && (tmpPoint.x + size.width) <= ScatterPlotViewer.this.formularComponent.getParent().getSize().width)
            ScatterPlotViewer.this.position.x = tmpPoint.x;
          if (tmpPoint.y >= 0 && (tmpPoint.y + size.height) <= ScatterPlotViewer.this.formularComponent.getParent().getSize().height)
            ScatterPlotViewer.this.position.y = tmpPoint.y;
          ScatterPlotViewer.this.formularComponent.setLocation(ScatterPlotViewer.this.position);
        }
      }
    }
  }

  public class LegendMouseAdapter extends MouseAdapter {

    /**
     * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent e) {
      ScatterPlotViewer.this.offset = new Point(e.getPoint());
    }

    /**
     * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e) {

      ScatterPlotViewer.this.offset = null;
    }
  }

}

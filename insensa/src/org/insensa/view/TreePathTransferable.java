/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class TreePathTransferable implements Transferable
{
	public static final DataFlavor TREE_PATH_TRANS = new DataFlavor(TreePath.class, "treePath");
	protected static final DataFlavor[] flavors =
	{ TREE_PATH_TRANS };
	Object treePath = null;
	List<Object> asd;

	/**
	 * @param tree
	 */
	public TreePathTransferable(JTree tree)
	{
		this.asd = new ArrayList<Object>();
		TreePath[] treePaths = tree.getSelectionPaths();

		for (TreePath treePath2 : treePaths)
		{
			Object[] treeObjects = treePath2.getPath();
			Object lastObject = treeObjects[treeObjects.length - 1];
			if (lastObject instanceof DefaultMutableTreeNode)
				this.asd.add(((DefaultMutableTreeNode) lastObject).getUserObject());
		}

	}

	/**
	 * @return
	 */
	public List<Object> getObjects()
	{
		return this.asd;
	}

	/** 
	 * 
	 * @see java.awt.datatransfer.Transferable#getTransferData(java.awt.datatransfer.DataFlavor)
	 */
	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
	{
		if (this.isDataFlavorSupported(flavor))
			return this.asd.toArray();
		else
			throw new UnsupportedFlavorException(flavor);
	}

	/** 
	 * 
	 * @see java.awt.datatransfer.Transferable#getTransferDataFlavors()
	 */
	@Override
	public DataFlavor[] getTransferDataFlavors()
	{
		return flavors;
	}

	/** 
	 * 
	 * @see java.awt.datatransfer.Transferable#isDataFlavorSupported(java.awt.datatransfer.DataFlavor)
	 */
	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor)
	{
		DataFlavor[] flavs = this.getTransferDataFlavors();
		for (DataFlavor flav : flavs)
		{
			if (flav.equals(flavor))
				return true;
		}
		return false;
	}
}

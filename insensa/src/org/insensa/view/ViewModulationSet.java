/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view;

import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

@SuppressWarnings("serial")
public class ViewModulationSet extends JScrollPane
{
	private JTree tree;
	ViewModulationSetPopup modSetPopup;


	public ViewModulationSet()
	{
		super();
		this.modSetPopup = new ViewModulationSetPopup();
		this.initTree();
	}

	/**
	 * @param nameList
	 */
	public void addNodes(List<String> nameList)
	{
		DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) this.tree.getLastSelectedPathComponent();
		for (int i = 0; i < nameList.size(); i++)
		{
			selNode.add(new DefaultMutableTreeNode(nameList.get(i).toString()));
		}
		this.tree.expandPath(this.tree.getSelectionPath());
	}

	/**
	 * @param nameList
	 * @param activeModSet
	 */
	public void addNodesByIndex(List<String> nameList, int activeModSet)
	{
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
		DefaultMutableTreeNode activeNode = (DefaultMutableTreeNode) rootNode.getChildAt(activeModSet);
		if (activeNode != null)
		{
			for (int i = 0; i < nameList.size(); i++)
			{
				activeNode.add(new DefaultMutableTreeNode(nameList.get(i).toString()));
			}
			this.tree.expandPath(this.tree.getPathForRow(activeModSet));
		}
	}

	/**
	 * @param fileSetName
	 */
	public void addTreeRoot(String fileSetName)
	{
		DefaultMutableTreeNode selNode = (DefaultMutableTreeNode) this.tree.getModel().getRoot();
		selNode.add(new DefaultMutableTreeNode(fileSetName));
		((DefaultTreeModel) this.tree.getModel()).reload();
	}


	public void close()
	{
		((DefaultMutableTreeNode) this.tree.getModel().getRoot()).removeAllChildren();
		this.tree.removeAll();
		this.tree = null;
		this.modSetPopup = null;
	}

	/**
	 * @return
	 */
	public ViewModulationSetPopup getModSetPopup()
	{
		return this.modSetPopup;
	}

	/**
	 * @return
	 */
	public JTree getModulationSetTree()
	{
		return this.tree;
	}


	private void initTree()
	{
		this.tree = new JTree(new DefaultMutableTreeNode("test"));
		this.tree.setRootVisible(false);
		super.setViewportView(this.tree);
	}
	/**
	 * @param projectName
	 * @param fileSetName
	 */
	/*
	 * public void addTree( String modSetName ) { DefaultMutableTreeNode
	 * rootNode = new DefaultMutableTreeNode(modSetName); tree=new
	 * JTree(rootNode); tree.addMouseListener(new java.awt.event.MouseAdapter()
	 * {
	 * @Override public void mousePressed(MouseEvent e) { if(e.isPopupTrigger())
	 * { TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());
	 * tree.setSelectionPath(treePath); DefaultMutableTreeNode selNode
	 * =(DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
	 * if(selNode!=null) if(selNode.isRoot()) {
	 * modSetPopup.show(e.getComponent(), e.getX(), e.getY()); }
	 * //popupMenu.show(e.getComponent(),e.getX(),e.getY()); } }
	 * @Override public void mouseReleased(MouseEvent e) {
	 * if(e.isPopupTrigger()) { TreePath treePath =
	 * tree.getPathForLocation(e.getX(), e.getY());
	 * tree.setSelectionPath(treePath); DefaultMutableTreeNode selNode
	 * =(DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
	 * if(selNode!=null) if(selNode.isRoot()) {
	 * modSetPopup.show(e.getComponent(), e.getX(), e.getY()); }
	 * //popupMenu.show(e.getComponent(),e.getX(),e.getY()); } } });
	 * super.addTab(modSetName, tree); }
	 */
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

@SuppressWarnings("serial")
public class TreeTransferHandler extends TransferHandler {
  private static final Logger log = LoggerFactory.getLogger(TreeTransferHandler.class);

  @Override
  public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
    return super.canImport(comp, transferFlavors);
  }

  @Override
  public boolean canImport(TransferSupport support) {

    support.getDataFlavors();
    return support.isDataFlavorSupported(TestNode.TestNodes)
        || support.isDataFlavorSupported(DataFlavor.stringFlavor);
  }

  /**
   * Because I do not use {@link Transferable} objects
   * we just have to return something that is not null
   */
  @Override
  protected Transferable createTransferable(JComponent c) {
    return new TestNode("test");
  }

  @Override
  public int getSourceActions(JComponent c) {
    return TransferHandler.COPY_OR_MOVE;
  }

  @Override
  public boolean importData(TransferSupport support) {
    Transferable transferable = support.getTransferable();
    log.debug("Transverable: " + transferable.getTransferDataFlavors().toString());
    return true;
  }
}
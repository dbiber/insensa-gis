package org.insensa.view.generic;

import org.insensa.exceptions.ScriptParseException;

public interface IViewChangedListener {
  void viewChanged();
}

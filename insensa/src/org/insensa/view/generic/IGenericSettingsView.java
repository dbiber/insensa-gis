package org.insensa.view.generic;

public interface IGenericSettingsView {
  void addVariableObject(IVariableObject obj);
  Object getDrawableObject();
  void clear();
}

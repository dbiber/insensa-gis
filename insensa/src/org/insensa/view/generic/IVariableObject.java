package org.insensa.view.generic;

import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.IVariable;

import java.util.Optional;

public interface IVariableObject {
  void build();

  Optional<IValue> getValue();

  void setValue(IValue value);

  String getName();

  VariableType getType();

  void setVariable(IVariable variable);

  void addValueListener(IVariableValueListener listener);

  Object getDrawableObject();

  String getDisplay();
}

package org.insensa.view.generic.swing;

import org.insensa.view.generic.IGenericSettingsView;
import org.insensa.view.generic.IGroupObject;
import org.insensa.view.generic.IVariableObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingGenericSettingsView implements IGenericSettingsView {

  private JPanel panel;
  private List<IVariableObject> objects;
  private Map<JLabel, JComponent> cMap;
  private GroupLayout layout;


  public SwingGenericSettingsView() {
    cMap = new LinkedHashMap<>();
    objects = new ArrayList<>();

    panel = new JPanel();
    layout = new GroupLayout(panel);
    panel.setLayout(layout);
  }


  @Override
  public void clear() {
    objects.clear();
    cMap.clear();
    panel.removeAll();
  }

  @Override
  public void addVariableObject(IVariableObject obj) {
    objects.add(obj);
  }

  private void initMap() {
    for (IVariableObject obj : objects) {
      cMap.put(new JLabel(obj.getDisplay()), (JComponent) obj.getDrawableObject());
    }
  }

  @Override
  public Object getDrawableObject() {
    initMap();
    layout.setHorizontalGroup(createHorizontalGroup(layout));
    layout.setVerticalGroup(createVerticalGroup(layout));
    return panel;
  }



  private GroupLayout.Group createHorizontalGroup(GroupLayout layout){
    GroupLayout.ParallelGroup leftGroup = layout.createParallelGroup();
    GroupLayout.ParallelGroup rightGroup = layout.createParallelGroup();

    for(Map.Entry<JLabel,JComponent> e : cMap.entrySet()) {
      leftGroup.addComponent(e.getKey());
      rightGroup.addComponent(e.getValue());
    }
    return layout.createSequentialGroup()
        .addGap(10)
        .addGroup(leftGroup)
        .addGap(10)
        .addGroup(rightGroup)
        .addGap(10);
  }

  private GroupLayout.Group createVerticalGroup(GroupLayout layout){

    GroupLayout.SequentialGroup group = layout.createSequentialGroup();


    for(Map.Entry<JLabel,JComponent> e : cMap.entrySet()) {
      group.addGroup(
          layout.createParallelGroup()
              .addComponent(e.getKey())
              .addComponent(e.getValue())
      ).addGap(10);
    }
    return group;
  }
}

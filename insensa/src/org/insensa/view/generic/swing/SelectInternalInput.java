package org.insensa.view.generic.swing;

import org.insensa.Environment;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Project;
import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.view.generic.AbstractVariableObject;

import java.awt.event.ItemEvent;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import javax.swing.JComboBox;

public class SelectInternalInput
    extends AbstractVariableObject {

  private JComboBox<String> filePathsComboBox;
  private Vector<String> vector = new Vector<>();

  @Override
  protected void buildView() {
    Project project = Environment.getInstance().getModel().getActiveProject();
    Optional<IParameter> filterParam = getParameter("filter");
    vector.add("Select One...");
    if (filterParam.isPresent()) {
      switch (filterParam.get().getValue()) {
        case "stack":
          createBoxWithStack(project);
          break;
        case "connection":
          createBoxWithConnection(project);
          break;
        case "file":
          createBoxWithAll(project);
          break;
      }
    } else {
      createBoxWithAll(project);
    }
  }

  private void createBoxWithAll(Project project) {
//    vector = new Vector<>();
    List<IGisFileSet> fileSets = project.getFileSetList();
    for (IGisFileSet fileset : fileSets) {
      checkFileSet(fileset, container -> true);
    }
    filePathsComboBox = new JComboBox<>(vector);
  }

//  private void createBoxWithFile(Project project) {
//    vector = new Vector<>();
//    List<IGisFileSet> fileSets = project.getFileSetList();
//    for (IGisFileSet fileSet : fileSets) {
//      checkFileSet(fileSet, container -> true);
//    }
//
//    filePathsComboBox = new JComboBox<>(vector);
//
//  }

  private void createBoxWithStack(Project project) {
//    vector = new Vector<>();
    List<IGisFileSet> fileSets = project.getFileSetList();
    for (IGisFileSet fileset : fileSets) {
      checkFileSet(fileset, container -> (container.isConnection() && !container.getConnectionFileChanger().isUsed()));
    }
    filePathsComboBox = new JComboBox<>(vector);
  }

  private void createBoxWithConnection(Project project) {
    Optional<IParameter> idParam = getParameter("id");
//    vector = new Vector<>();
    List<IGisFileSet> fileSets = project.getFileSetList();
    for (IGisFileSet fileset : fileSets) {
      checkFileSet(fileset, container -> {
        if (idParam.isPresent()) {
          return container.isConnection() && container.getConnectionFileChanger()
              .getId().toLowerCase().contains(idParam.get().getValue().toLowerCase());
        } else {
          return container.isConnection();
        }
      });
    }
    filePathsComboBox = new JComboBox<>(vector);
  }

  private void checkFileSet(IGisFileSet fileSet,
                            IGisFileContainerFilter fileContainerFilter) {
    for (IGisFileSet child : fileSet.getChildSets()) {
      checkFileSet(child, fileContainerFilter);
    }
    for (IGisFileContainer fileContainer : fileSet.getFileList()) {
      if (fileContainerFilter.filter(fileContainer)) {
        vector.add(fileContainer.getPathRelativeToProject() + fileContainer.getOutputFileName());
      }
    }
  }

  @Override
  protected void createInternalListener() {
    filePathsComboBox.addItemListener(event -> {
      if (event.getStateChange() == ItemEvent.SELECTED) {
        if (getValue().isPresent()) {
          notifyValueChanged();
        }
      }
    });
  }

  @Override
  public Optional<IValue> getValue() {
    return Optional.of(new StringValue((String) filePathsComboBox.getSelectedItem()));
  }

  @Override
  public void setValue(IValue value) {
    String sval = ((StringValue) value).getValue();
    filePathsComboBox.setSelectedItem(sval);
  }

  @Override
  public Object getDrawableObject() {
    return filePathsComboBox;
  }


  private interface IGisFileContainerFilter {
    boolean filter(IGisFileContainer container);
  }
}

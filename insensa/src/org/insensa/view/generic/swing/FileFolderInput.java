package org.insensa.view.generic.swing;

import org.insensa.model.generic.IEvent;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.view.generic.AbstractVariableObject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FileFolderInput extends AbstractVariableObject {

  private JPanel panel;
  private JFileChooser fileChooser;
  private JTextField textField;
  private JButton button;
  private NotificationDocument document;

  @Override
  protected void buildView() {
    //Copy elements
    panel = new JPanel();
    panel.setLayout(new BorderLayout());
    button = new JButton("Select Dir");
    textField = new JTextField();
    document = new NotificationDocument();
    textField.setDocument(document);
    fileChooser = new JFileChooser();
    if (getType() == VariableType.FILE) {
      fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    } else if (getType() == VariableType.DIRECTORY) {
      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
    panel.add(textField, BorderLayout.CENTER);
    panel.add(button, BorderLayout.LINE_END);
    panel.setBackground(Color.blue);
  }

  @Override
  protected void createInternalListener() {
    button.addActionListener(e -> {
      if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        textField.setText(fileChooser.getSelectedFile().getAbsolutePath());
      }
    });
    boolean hasOnEnter = false;
    for (IEvent event : getVariable().getEvents()) {
      //Do not add listener
      if (event.getType().equals("onEnter")) {
        hasOnEnter = true;
        break;
      }
    }
    if (hasOnEnter) {
      document.setChangeListener(() -> {
        String fText = textField.getText();
        if (fText == null || fText.isEmpty() || !new File(fText).exists()) {
          return;
        }
        notifyValueChanged();
      });
    } else {
      document.setChangeListener(this::notifyValueChanged);
    }
  }

  @Override
  public Object getDrawableObject() {
    return panel;
  }

  @Override
  public Optional<IValue> getValue() {
    return Optional.of(new StringValue(textField.getText()));
  }

  @Override
  public void setValue(IValue value) {
    textField.setText(((StringValue) value).getValue());
  }

  public JFileChooser getFileChooser() {
    return fileChooser;
  }

  public JTextField getTextField() {
    return textField;
  }
}

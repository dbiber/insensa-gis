package org.insensa.view.generic.swing;

import org.insensa.model.generic.value.BooleanValue;
import org.insensa.model.generic.value.IValue;
import org.insensa.view.generic.AbstractVariableObject;

import java.util.Optional;

import javax.swing.JCheckBox;

public class BooleanInput extends AbstractVariableObject {

  private JCheckBox checkBox;

  @Override
  protected void buildView() {
    checkBox = new JCheckBox();
  }

  @Override
  protected void createInternalListener() {
    checkBox.addItemListener(e -> notifyValueChanged());
  }

  @Override
  public Optional<IValue> getValue() {
    return Optional.of(new BooleanValue(checkBox.isSelected()));
  }

  @Override
  public void setValue(IValue value) {
    checkBox.setSelected(((BooleanValue) value).getValue());
  }

  @Override
  public Object getDrawableObject() {
    return checkBox;
  }
}

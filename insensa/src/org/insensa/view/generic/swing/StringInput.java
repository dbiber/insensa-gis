package org.insensa.view.generic.swing;

import org.insensa.model.generic.IEvent;
import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.view.generic.AbstractVariableObject;

import java.util.Optional;

import javax.swing.JTextField;


public class StringInput extends AbstractVariableObject {
  private JTextField textField;
  private NotificationDocument document;

  @Override
  protected void buildView() {
    this.textField = new JTextField();
    document = new NotificationDocument();
    this.textField.setDocument(document);
    this.textField.addActionListener(e -> notifyValueChanged());
  }

  @Override
  protected void createInternalListener() {
    for (IEvent event: getVariable().getEvents()) {
      //Do not add listener
      if (event.getType().equals("onEnter")) {
        return;
      }
    }
    document.setChangeListener(this::notifyValueChanged);
  }

  /**
   * @return an optional of type @{@link IValue} retrieved from the Input view field
   */
  @Override
  public Optional<IValue> getValue() {
    return Optional.of(new StringValue(textField.getText()));
  }

  /**
   * Change the view with the value @{@link IValue}
   */
  @Override
  public void setValue(IValue value) {
    textField.setText(((StringValue) value).getValue());
  }

  /**
   * @return The view object
   */
  @Override
  public Object getDrawableObject() {
    return textField;
  }
}

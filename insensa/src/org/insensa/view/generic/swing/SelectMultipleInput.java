package org.insensa.view.generic.swing;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.ParameterListValue;
import org.insensa.view.generic.AbstractVariableObject;
import org.insensa.view.widgets.WidgetMultiSelectionPopupMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Vector;

public class SelectMultipleInput extends AbstractVariableObject {

  private WidgetMultiSelectionPopupMenu<IParameter> widget;

  @Override
  protected void buildView() {
    //Should not be null, checked by parser
    IParameter oParam = Objects
        .requireNonNull(getParameter("collection").orElse(null));

    widget = new WidgetMultiSelectionPopupMenu<>(oParam.getValue());
    oParam.getParameter().forEach(parameter -> widget.addItem(parameter.getName(),parameter));
  }

  @Override
  public void setValue(IValue value) {
    Objects.requireNonNull(value);
    List<IParameter> devaultList = ((ParameterListValue) value).getDataList();
    List<IParameter> selectionList = new ArrayList<>();
    widget.getElements().forEach(parameter -> {
      devaultList.forEach(parameter1 -> {
        if(parameter.getValue().equals(parameter1.getValue()) &&
            parameter.getName().equals(parameter1.getName())){
          selectionList.add(parameter);
        }
      });
    });
    widget.setSelectedObjects(selectionList);
  }

  @Override
  protected void createInternalListener() {
    widget.setListener(e -> notifyValueChanged());
  }

  @Override
  public Optional<IValue> getValue() {
    Vector<IParameter> selParams = widget.getSelectedElements();
    return Optional.of(new ParameterListValue(new ArrayList<>(selParams)));
  }

  @Override
  public Object getDrawableObject() {
    return widget;
  }
}

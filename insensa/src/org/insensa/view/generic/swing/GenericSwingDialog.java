package org.insensa.view.generic.swing;

import org.insensa.commands.RScriptCommand;
import org.insensa.exceptions.ScriptParseException;
import org.insensa.exports.FileExporter;
import org.insensa.extensions.IPluginExec;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.extensions.IScriptPluginSettings;
import org.insensa.imports.IFileImporter;
import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.AbstractPluginSettingsDialog;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.insensa.view.exports.IViewFileExporterSetting;
import org.insensa.view.extensions.ViewChangeType;
import org.insensa.view.generic.IScriptExecutionListener;
import org.insensa.view.generic.VariableViewPresenter;
import org.insensa.view.imports.IViewFileImporterSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.JPanel;

public class GenericSwingDialog extends AbstractPluginSettingsDialog
    implements IViewFileImporterSetting, IViewFileExporterSetting, IScriptPluginSettings {
  private static final Logger log = LoggerFactory.getLogger(GenericSwingDialog.class);
  VariableViewPresenter presenter;
  JPanel panel;
  JPanel subPanel;
  RCommandProgressBarWorkerStatus workerStatus =
      new RCommandProgressBarWorkerStatus();
  RScriptCommand command = new RScriptCommand("asd");

  public GenericSwingDialog(VariableViewPresenter presenter) {
    this.presenter = presenter;
    panel = new JPanel();
    panel.setLayout(new BorderLayout());
    this.presenter.setViewChangedListener(() -> {
      panel.remove(subPanel);
      try {
        subPanel = (JPanel) presenter.buildSettingsView().getDrawableObject();
      } catch (ScriptParseException e) {
        log.error("Could not refresh view", e);
      }
      panel.add(subPanel, BorderLayout.CENTER);
      super.pack();
    });
  }

  public VariableViewPresenter getPresenter() {
    return presenter;
  }

  @Override
  public String getDialogTitle() {
    return "Generic Dialog";
  }

  @Override
  public JPanel initComponent() {
    try {
      subPanel = (JPanel) presenter.buildSettingsView().getDrawableObject();
      panel.add(subPanel, BorderLayout.CENTER);
      return panel;
    } catch (ScriptParseException e) {
      throw new RuntimeException("Could not build the GenericSettingView", e);
    }
  }

  @Override
  public Map<String, IVariable> getVariables() {
    return presenter.getVariableMap();
  }

  @Override
  public void notifyViewChange(ViewChangeType type, Object payload) {
  }

  @Override
  public void setPluginExec(IPluginExec pluginExec) {
    super.setPluginExec(pluginExec);
    Map<String, IVariable> vars = ((IScriptPluginExec) pluginExec).getVariables();
    if (vars != null) {
      presenter.setDefaultValues(vars);
    }
  }

  @Override
  public void setFileImporter(IFileImporter fileImporter) {
  }

  @Override
  public void setFileExporter(FileExporter fileExporter) {
  }

}

package org.insensa.view.generic.swing;

import org.insensa.model.generic.IVariable;
import org.insensa.view.generic.IVariableObject;
import org.insensa.view.generic.IVariableObjectFactory;

import java.util.Objects;

public class SwingObjectFactory implements IVariableObjectFactory {

  @Override
  public IVariableObject createObject(IVariable variable) {
    IVariableObject object = null;
    switch (variable.getType()) {
      case NUMERIC:
        object = new NumericInput();
        break;
      case DIRECTORY:
        object = new FileFolderInput();
        break;
      case FILE:
        object = new FileFolderInput();
        break;
      case STRING:
        object = new StringInput();
        break;
      case PASSWORD:
        object = new PasswordInput();
        break;
      case SELECT_MULTIPLE:
        object = new SelectMultipleInput();
        break;
      case SELECT_ONE:
        object = new SelectOneInput();
        break;
      case BOOLEAN:
        object = new BooleanInput();
        break;
      case SELECT_INTERNAL:
        object = new SelectInternalInput();
        break;
      case TABLE:
        object = new TableInput();
        break;
      default:
        break;
    }

    Objects.requireNonNull(object);
    object.setVariable(variable);
    object.build();
    return object;
  }

}

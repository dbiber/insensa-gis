package org.insensa.view.generic.swing;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.ParameterImpl;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.ParameterListValue;
import org.insensa.view.generic.AbstractVariableObject;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;
import java.util.StringJoiner;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class TableInput extends AbstractVariableObject {

  private JPanel panelTable;
  private JPanel panelButtons;
  private JTable table;
  private JScrollPane scrollPane;
  private DefaultTableModel model;
  private JButton buttonAdd;
  private JButton buttonRemove;
  private Vector<String> headerVec;

  @Override
  protected void buildView() {
    panelButtons = new JPanel(new FlowLayout());
    buttonAdd = new JButton("Add");
    buttonAdd.addActionListener(e -> {
      model.addRow(new String[headerVec.size()]);
    });
    buttonRemove = new JButton("Remove");
    buttonRemove.addActionListener(e -> {
      int selRow = table.getSelectedRow();
      model.removeRow(selRow);
    });
    panelButtons.add(buttonAdd);
    panelButtons.add(buttonRemove);

    panelTable = new JPanel(new BorderLayout());
    headerVec = new Vector<>();
    Optional<IParameter> headerParam = getParameter("header");
    if (headerParam.isPresent()) {
      for (IParameter hParam : headerParam.get().getParameter()) {
        headerVec.add(hParam.getValue());
      }
    }

    model = new DefaultTableModel(headerVec, 0) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return true;
      }
    };
    table = new JTable(model);
    scrollPane = new JScrollPane(table);
    table.setFillsViewportHeight(true);
    Dimension dim = scrollPane.getPreferredSize();
    scrollPane.setPreferredSize(new Dimension(dim.width, 100));
    scrollPane.setMaximumSize(new Dimension(dim.width, 100));
    panelTable.add(scrollPane, BorderLayout.PAGE_START);
    panelTable.add(panelButtons, BorderLayout.PAGE_END);

    table.getModel().addTableModelListener(e -> notifyValueChanged());
  }

  @Override
  protected void createInternalListener() {

  }

  @Override
  public Optional<IValue> getValue() {
    Vector<IParameter> params = new Vector<>();
    for (int count = 0; count < model.getRowCount(); count++) {
      StringJoiner joiner = new StringJoiner(",");
      for (int col = 0; col < model.getColumnCount(); col++) {
        Object colObj = model.getValueAt(count, col);
        if (colObj != null) {
          joiner.add(colObj.toString());
        } else {
          joiner.add("");
        }
      }
      params.add(new ParameterImpl(Integer.toString(count), joiner.toString()));
    }
    return Optional.of(new ParameterListValue(new ArrayList<>(params)));
  }

  @Override
  public void setValue(IValue value) {
    List<IParameter> content = ((ParameterListValue) value).getDataList();
    for (IParameter parameter : content) {
      String csvVal = parameter.getValue();
      String[] columns = csvVal.split(",");
      ((DefaultTableModel) table.getModel()).addRow(columns);
    }
  }

  @Override
  public Object getDrawableObject() {
    return panelTable;
  }
}

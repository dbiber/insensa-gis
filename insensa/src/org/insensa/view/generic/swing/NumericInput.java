package org.insensa.view.generic.swing;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.IntegerValue;
import org.insensa.view.generic.AbstractVariableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class NumericInput extends AbstractVariableObject {
  private static final Logger log = LoggerFactory.getLogger(NumericInput.class);

  private JComponent spinner;
  private SpinnerModel spinnerModel;

  @Override
  protected void buildView() {
    //copy elements
    List<IParameter> parameters = getParameter();
    spinner = new JSpinner();
    if (getType() == VariableType.NUMERIC) {
      spinnerModel = new SpinnerNumberModel();
      for (IParameter parameter : parameters) {
        switch (parameter.getName()) {
          case "min":
            Integer val = getIntValue(parameter, 0);
            ((SpinnerNumberModel) spinnerModel)
                .setMinimum(val);
            spinnerModel.setValue(val);
            break;
          case "max":
            ((SpinnerNumberModel) spinnerModel)
                .setMaximum(getIntValue(parameter, 0));
            break;
          case "stepsize":
            ((SpinnerNumberModel) spinnerModel)
                .setStepSize(getIntValue(parameter, 1));
            break;
          case "default":
            spinnerModel
                .setValue(getIntValue(parameter, 0));
            break;
        }
      }
      ((JSpinner) spinner).setModel(spinnerModel);
    }
  }

  @Override
  protected void createInternalListener() {
    spinnerModel.addChangeListener(e -> notifyValueChanged());
  }

  private Integer getIntValue(IParameter parameter, Integer fallback) {
    try {
      return Integer.parseInt(parameter.getValue());
    } catch (NumberFormatException e) {
      log.error("Value of param " + parameter.getName()
          + " is not a number but \""
          + parameter.getValue() + "\"");
      return fallback;
    }
  }

  @Override
  public Optional<IValue> getValue() {
    return Optional.of(
        new IntegerValue(((SpinnerNumberModel) spinnerModel).getNumber().intValue()));
  }

  @Override
  public void setValue(IValue value) {
    spinnerModel.setValue(((IntegerValue) value).getValue());
  }

  @Override
  public Object getDrawableObject() {
    return spinner;
  }
}

package org.insensa.view.generic.swing;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class NotificationDocument extends PlainDocument {

  IChangeListener listener;

  public NotificationDocument() {
  }

  public void setChangeListener(IChangeListener listener) {
    this.listener = listener;
    addDocumentListener(new DocumentListener() {
      @Override
      public void insertUpdate(DocumentEvent e) {
        listener.changed();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        listener.changed();
      }
    });
  }

  @Override
  public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
    // fields don't want to have multiple lines.  We may provide a field-specific
    // model in the future in which case the filtering logic here will no longer
    // be needed.
    Object filterNewlines = getProperty("filterNewlines");
    if ((filterNewlines instanceof Boolean) && filterNewlines.equals(Boolean.TRUE)) {
      if ((str != null) && (str.indexOf('\n') >= 0)) {
        StringBuilder filtered = new StringBuilder(str);
        int n = filtered.length();
        for (int i = 0; i < n; i++) {
          if (filtered.charAt(i) == '\n') {
            filtered.setCharAt(i, ' ');
          }
        }
        str = filtered.toString();
      }
    }
    super.insertString(offs, str, a);
  }

  @Override
  public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
    if (length == 0 && (text == null)) {
      return;
    }
    writeLock();
    try {
      if (length > 0) {
        remove(offset, length);
      }
      if (text != null) {
        if (text.isEmpty()) {
          listener.changed();
        } else {
          insertString(offset, text, attrs);
        }
      }
    } finally {
      writeUnlock();
    }
  }

  @Override
  protected void postRemoveUpdate(DefaultDocumentEvent chng) {
    super.postRemoveUpdate(chng);


  }


  interface IChangeListener {
    void changed();
  }
}

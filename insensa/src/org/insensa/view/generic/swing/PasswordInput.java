package org.insensa.view.generic.swing;

import org.insensa.model.generic.IEvent;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.view.generic.AbstractVariableObject;

import java.util.Optional;

import javax.swing.JPasswordField;

public class PasswordInput extends AbstractVariableObject {
  private JPasswordField textField;
  private NotificationDocument document;

  @Override
  protected void buildView() {
    this.textField = new JPasswordField();
    document = new NotificationDocument();
    this.textField.setDocument(document);
    this.textField.addActionListener(e -> notifyValueChanged());
  }

  /**
   * executed by NotificationDocument when text changed
   */
  @Override
  protected void createInternalListener() {
    for (IEvent event : getVariable().getEvents()) {
      //Do not add listener
      if (event.getType().equals("onEnter")) {
        return;
      }
    }
    document.setChangeListener(this::notifyValueChanged);
  }

  @Override
  public Optional<IValue> getValue() {
    return Optional.of(new StringValue(textField.getText()));
  }

  @Override
  public void setValue(IValue value) {
    textField.setText(((StringValue) value).getValue());
  }

  @Override
  public Object getDrawableObject() {
    return textField;
  }
}
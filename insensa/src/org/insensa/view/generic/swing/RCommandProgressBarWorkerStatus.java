package org.insensa.view.generic.swing;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.insensa.view.dialogs.ProgressBar;

public class RCommandProgressBarWorkerStatus extends ProgressBar implements TailerListener {
  @Override
  public void init(Tailer tailer) {}

  @Override
  public void fileNotFound() {}

  @Override
  public void fileRotated() {}

  @Override
  public void handle(String line) {
    setProgressName(line);
  }

  @Override
  public void handle(Exception ex) {}
}

package org.insensa.view.generic.swing;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.ParameterImpl;
import org.insensa.model.generic.value.IValue;
import org.insensa.view.generic.AbstractVariableObject;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

public class SelectOneInput extends AbstractVariableObject {

  private JComboBox<IParameter> parameterJComboBox;

  @Override
  protected void buildView() {
    IParameter parameter = getParameter("collection").get();
    ComboBoxModel<IParameter> paramModel = new ParameterComboBoxModel(parameter);

    parameterJComboBox = new JComboBox<>();
    parameterJComboBox.setModel(paramModel);
    parameterJComboBox.setRenderer(new ParameterComboBoxRenderer());
//    parameter.getParameter().forEach(parameterJComboBox::addItem);
  }

  @Override
  public void setValue(IValue value) {
    ParameterImpl param = (ParameterImpl) value;
    List<IParameter> params = ((ParameterComboBoxModel) parameterJComboBox.getModel())
        .getParameters()
        .stream()
        .filter(parameter -> parameter.getName().equals(param.getName()) &&
            parameter.getValue().equals(param.getValue()))
        .collect(Collectors.toList());
    if(!params.isEmpty()) {
      parameterJComboBox.setSelectedItem(params.get(0));
    }
  }

  @Override
  protected void createInternalListener() {
    parameterJComboBox.addItemListener(event -> {
      if (event.getStateChange() == ItemEvent.SELECTED) {
        if (getValue().isPresent()) {
          notifyValueChanged();
        }
      }
    });
  }

  @Override
  public Optional<IValue> getValue() {
    Object selItem = parameterJComboBox.getSelectedItem();
    if (selItem == null || ((IParameter) selItem).getValue().equals("none")) {
      return Optional.empty();
    } else {
      return Optional.of((IParameter) selItem);
    }
  }

  @Override
  public Object getDrawableObject() {
    return parameterJComboBox;
  }

  public class ParameterComboBoxModel extends DefaultComboBoxModel<IParameter> {

    List<IParameter> parameters;

    public ParameterComboBoxModel(IParameter parameter) {
      super();
      this.parameters = parameter.getParameter();
      addElement(new ParameterImpl(parameter.getValue(), "none"));
      parameters.forEach(this::addElement);
    }

    public List<IParameter> getParameters() {
      return parameters;
    }
  }

  public class ParameterComboBoxRenderer extends BasicComboBoxRenderer {
    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(
          list, value, index, isSelected, cellHasFocus);
      IParameter param = (IParameter) value;
      ((JLabel) comp).setText(param.getName());
      return comp;
    }
  }
}

package org.insensa.view.generic;

import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.IEvent;
import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IScriptConfigurationTranslator;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.ParameterListValue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class VariableViewPresenter {

  private Map<String, IVariable> variableMap;
  private IScriptConfigurationTranslator translator;
  private IGenericSettingsView settingsView;
  private IVariableObjectFactory factory;
  private IVariableValueListener listener;
  private IViewChangedListener viewChangedListener;
  private IScriptExecutionListener scriptExecutionListener;

  public VariableViewPresenter(IScriptConfigurationTranslator translator,
                               IGenericSettingsView genericSettingsView,
                               IVariableObjectFactory factory) {
    variableMap = new LinkedHashMap<>();
    this.translator = translator;
    this.settingsView = genericSettingsView;
    this.factory = factory;
    listener = new ConfigValueListener();
  }

  public void setViewChangedListener(IViewChangedListener viewChangedListener) {
    this.viewChangedListener = viewChangedListener;
  }

  public IGenericSettingsView buildSettingsView() throws ScriptParseException {
    variableMap.clear();
    settingsView.clear();
    List<IVariable> variables = this.translator.getVariables();
    if (variables == null || variables.isEmpty()) {
      throw new ScriptParseException("No variables available, "
          + "forgot to parse with translator ?");
    }
    for (IVariable variable : variables) {
      variableMap.put(variable.getName(), variable);
      IVariableObject object = factory.createObject(variable);
      object.addValueListener(listener);
      variable.getEvents().forEach(event -> object.addValueListener(new EventValueListener(event)));
      settingsView.addVariableObject(object);
    }
    return settingsView;
  }

  public IScriptConfigurationTranslator getTranslator() {
    return translator;
  }

  public Map<String, IVariable> getVariableMap() {
    return variableMap;
  }

  public void setDefaultValues(Map<String, IVariable> vars) {
    translator.setDefaultValues(vars);
  }

  public void setExecutionListener(Object p0) {

  }

  class ConfigValueListener implements IVariableValueListener {
    @Override
    public void valueChanged(String name, IValue value, VariableType type) {
      variableMap.get(name).setValue(value);
    }
  }

  class EventValueListener implements IVariableValueListener {

    private IEvent event;

    public EventValueListener(IEvent event) {
      this.event = event;
    }

    @Override
    public void valueChanged(String name, IValue value, VariableType type) {
      boolean uiHasChanged = false;
      if (event.getType() != null && event.getType().equals("onEnter")) {
        VariableViewPresenter.this.translator.executeFunction(event.getFunction(), value);
        uiHasChanged = true;
      } else if (!event.getOnValue().isEmpty()) {
        boolean valueMatch =
            event.getOnValue()
                .stream()
                .map(Object::toString)
                .anyMatch(s -> {
                  if (value instanceof IParameter) {
                    return s.equals(((IParameter) value).getValue());
                  } else if (value instanceof ParameterListValue) {
                    for (IParameter param : ((ParameterListValue) value).getDataList()) {
                      if (param.getValue().equalsIgnoreCase(s)) {
                        return true;
                      }
                    }
                  } else {
                    return value.toScriptString().equalsIgnoreCase(s);
                  }
                  return false;
                });
        if (valueMatch) {
          if (event.getShowGroup() != null && !event.getShowGroup().isEmpty()) {
            VariableViewPresenter.this.translator.setActiveGroup(event.getShowGroup());
            uiHasChanged = true;
          } else if (event.getFunction() != null && !event.getFunction().isEmpty()) {
            VariableViewPresenter.this.translator.executeFunction(event.getFunction(), value);
            uiHasChanged = true;
          } else {
            uiHasChanged = false;
          }
        }
      }
      if (uiHasChanged) {
        if (VariableViewPresenter.this.viewChangedListener != null) {
          VariableViewPresenter.this.viewChangedListener.viewChanged();
        }
      }
    }
  }
}

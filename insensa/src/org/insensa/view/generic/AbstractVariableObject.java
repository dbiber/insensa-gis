package org.insensa.view.generic;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractVariableObject implements IVariableObject {

  private IVariable variable;
  private List<IVariableValueListener> listeners;

  public AbstractVariableObject() {
    listeners = new ArrayList<>();
  }

  protected IVariable getVariable() {
    return variable;
  }

  @Override
  public void setVariable(IVariable variable) {
    this.variable = variable;
  }

  @Override
  public String getName() {
    return variable.getName();
  }

  @Override
  public VariableType getType() {
    return variable.getType();
  }

  @Override
  public void addValueListener(IVariableValueListener listener) {
    this.listeners.add(listener);
  }

  @Override
  public String getDisplay() {
    return variable.getDisplay();
  }

  /**
   * Split build into three substeps so the listeners are added after
   * setting the default value because settings the default value should not trigger events!
   */
  @Override
  public void build() {
    buildView();
    createInternalListener();
    if (variable.getValue().isPresent()) {
      setValue(variable.getValue().get());
    }
  }

  public void notifyValueChanged() {
    this.listeners.forEach(iVariableValueListener -> {
      iVariableValueListener.valueChanged(getName(),
          getValue().get(),
          getType());
    });
  }

  protected List<IParameter> getParameter() {
    return variable.getParameter();
  }

  protected Optional<IParameter> getParameter(String name) {
    return variable.getParameter(name);
  }

  /**
   * Create the object to return to in {@link #getDrawableObject()}
   * You can use abstract methods {@link #getType()}, {@link #getName()},
   * {@link #getVariable()}
   * for a more detailed configuration.
   *
   * @apiNote It is important to set the default value here because setValue is meant to
   * trigger {@link #notifyValueChanged()} (registered in {@link #createInternalListener()})
   */
  protected abstract void buildView();

  /**
   * Create a listener that ultimately calles {@link #notifyValueChanged()}
   * as soon as the view changes,  e.g. if the text changes in a TextView obj
   * or if a button got pressed.
   * {@link #notifyValueChanged()} will itself notify the model of the variables
   * to trigger moden event if any is registered.
   *
   * For Example, there could be a search field that calles
   * {@link #notifyValueChanged()}
   * every time sombody presses the ENTER key within the TextField.
   * {@link #notifyValueChanged()}
   * will then call a "search" method in the model which themself
   * could change the view again
   */
  protected abstract void createInternalListener();
}

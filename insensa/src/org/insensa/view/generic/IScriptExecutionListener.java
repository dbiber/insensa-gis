package org.insensa.view.generic;

import org.insensa.commands.AbstractModelCommand;

public interface IScriptExecutionListener {
  void executeScript(AbstractModelCommand cmd);
}

package org.insensa.view.generic;

import org.insensa.model.generic.IVariable;

public interface IVariableObjectFactory {
  IVariableObject createObject(IVariable variable);
}

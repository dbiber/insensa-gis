package org.insensa.view.generic;

import org.insensa.model.generic.VariableType;
import org.insensa.model.generic.value.IValue;

public interface IVariableValueListener {
  void valueChanged(String name, IValue value, VariableType type);
}

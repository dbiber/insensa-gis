/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;


import org.insensa.GisFileType;
import org.insensa.connections.ConnectionManager;
import org.insensa.infoconnections.InfoConnectionManager;
import org.insensa.inforeader.InfoManager;
import org.insensa.optionfilechanger.OptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;


public class ViewOptions extends JScrollPane {
  private static final Logger log = LoggerFactory.getLogger(ViewOptions.class);
  private static final long serialVersionUID = -206847686181271871L;
  Vector<String> vNames;
  private JTable optionsTable;
  CustomTableCellRenderer cellRenderer = new CustomTableCellRenderer();

  public ViewOptions() {
    super();
    this.initTable();
  }

  public void close() {
    if (this.optionsTable != null) {
      this.optionsTable.removeAll();
      this.optionsTable.setVisible(false);
      this.optionsTable = null;
    }
    if (this.vNames != null) {
      this.vNames.clear();
      this.vNames = null;
    }
  }

  /**
   * @return
   */
  public JTable getOptionsTable() {
    return this.optionsTable;
  }

  private void initTable() {
    this.vNames = new Vector<String>();
    this.vNames.add("Name");
    this.vNames.add("Property");
    this.vNames.add("Value");
    this.optionsTable = new JTable(new PropertiesTableModel(this.vNames, 1));
    this.optionsTable.getColumnModel().getColumn(0).setCellRenderer(cellRenderer);
    this.optionsTable.getColumnModel().getColumn(1).setCellRenderer(cellRenderer);
    this.optionsTable.getColumnModel().getColumn(2).setCellRenderer(cellRenderer);
    this.setViewportView(this.optionsTable);
  }

  /**
   * @param dataVector a new X*2 String Vector
   */
  public void setTableContent(Vector<Vector<String>> dataVector) {
    ((PropertiesTableModel)optionsTable.getModel()).setDataVector(dataVector,vNames);
    this.optionsTable.getColumnModel().getColumn(0).setCellRenderer(cellRenderer);
    this.optionsTable.getColumnModel().getColumn(1).setCellRenderer(cellRenderer);
    this.optionsTable.getColumnModel().getColumn(2).setCellRenderer(cellRenderer);
  }

  public class CustomTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -2010365868679410400L;


    //TODO: FIXME: This is terrible, please fix !
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      String oldString = (String) table.getValueAt(row, 0);
      String oldString2 = (String) table.getValueAt(row, 1);
      if (oldString != null) {
        if (!oldString.isEmpty()) {
          cell.setBackground(Color.LIGHT_GRAY);
          return cell;
        } else {

          Object[] sInfoArray;
          Object[] sConnectionArray;
          Object[] sOptionArray;
          Object[] sInfoConnectionArray;
          sConnectionArray = new ConnectionManager().getConnectionList().toArray();
          sInfoArray = new InfoManager().getInfoReaderList(GisFileType.GEO_TIFF).toArray();
          sOptionArray = new OptionManager().getOptionFileChangerList(GisFileType.GEO_TIFF).toArray();
          sInfoConnectionArray = new InfoConnectionManager().getInfoConnectionList(GisFileType.GEO_TIFF).toArray();
          String[] nameArray = new String[sInfoArray.length + sConnectionArray.length + sOptionArray.length + sInfoConnectionArray.length];
          int j = 0;
          for (Object element: sInfoArray) {
            nameArray[j] = element.toString();//.substring(0, 1).toUpperCase() + element.toString().substring(1);
            j++;
          }
          for (Object element: sConnectionArray) {
            nameArray[j] = element.toString();//.substring(0, 1).toUpperCase() + element.toString().substring(1);
            j++;
          }
          for (Object element: sOptionArray) {
            nameArray[j] = element.toString();//.substring(0, 1).toUpperCase() + element.toString().substring(1);
            j++;
          }
          for (Object element: sInfoConnectionArray) {
            nameArray[j] = element.toString();//.substring(0, 1).toUpperCase() + element.toString().substring(1);
            j++;
          }
          for (String element: nameArray) {
            if (oldString2.equals(element)) {
              if (column != 0) {
                cell.setBackground(Color.LIGHT_GRAY);
                return cell;
              } else {
                cell.setBackground(Color.white);
                return cell;
              }
            }
          }
          cell.setBackground(Color.white);
          return cell;
        }
      }
      return cell;
    }
  }

  public class PropertiesTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 4337820792615775373L;
    Class[] types = new Class[]
        {String.class, String.class, String.class};
    @SuppressWarnings("unused")
    boolean[] canEdit = new boolean[]
        {false, true, true};

    public PropertiesTableModel(Vector columnNames, int rowCount) {
      super(columnNames, rowCount);
    }

    public PropertiesTableModel(Object[] columnNames, int rowCount) {
      super(columnNames, rowCount);
    }

    public PropertiesTableModel(Vector data, Vector columnNames) {
      super(data, columnNames);
    }

    public void setDataVector(Vector data, Vector columnNames) {
      super.setDataVector(data, columnNames);
    }

    public Class getColumnClass(int columnIndex) {
      return this.types[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      return false;
    }
  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view;

import org.insensa.Model;
import org.insensa.controller.AddFilesToFileSet;
import org.insensa.controller.ControllerFileEditing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


public class TreeTestTransferHandler extends TransferHandler {
  private static final Logger log = LoggerFactory.getLogger(TreeTestTransferHandler.class);

  private static final long serialVersionUID = 1639768966259784310L;
  private View view;
  private Model model;

  public TreeTestTransferHandler(View view, Model model) {
    this.view = view;
    this.model = model;
  }

  @Override
  public boolean canImport(TransferSupport support) {
    return true;
  }

  @Override
  protected Transferable createTransferable(JComponent c) {
    if (c instanceof JTree) {
      return new TestNode();
    } else {
      log.debug("no tree");
      return null;
    }
  }

  public void drop(Point dropLocation, int dndMode) {
    JTree fileSetTree = this.view.getViewProject().getFileSetView().getFileSetTree();
    TreePath[] treePaths = fileSetTree.getSelectionPaths();
    ControllerFileEditing edit = new ControllerFileEditing(treePaths, this.view, this.model);

    TreePath path = this.view.getViewProject().getFileSetView().getFileSetTree().getClosestPathForLocation(dropLocation.x, dropLocation.y);
    DefaultMutableTreeNode targetNode = (DefaultMutableTreeNode) path.getLastPathComponent();
    edit.setTargetTreePath(targetNode);

    if (dndMode == DnDConstants.ACTION_COPY) {
      try {
        edit.copyFiles();
      } catch (Exception e) {
        JOptionPane.showMessageDialog(this.view, e.toString());
        log.error("UNKNOWN", e);
      }
    } else if (dndMode == DnDConstants.ACTION_MOVE) {
      try {
        edit.moveFiles();
      } catch (Exception e) {
        JOptionPane.showMessageDialog(this.view, e.toString());
        log.error("UNKNOWN", e);
      }
    }

  }

  @Override
  public int getSourceActions(JComponent c) {
    return TransferHandler.COPY_OR_MOVE;
  }

  @Override
  public boolean importData(TransferSupport support) {

    if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
      try {
        Object fileTransferable = support.getTransferable()
            .getTransferData(DataFlavor.javaFileListFlavor);
        if (fileTransferable instanceof List<?> && !(((List<?>) fileTransferable).isEmpty())
            && (((List<?>) fileTransferable).get(0) instanceof File)) {
          List<File> fileList = (List<File>) fileTransferable;
          File[] fileArray = fileList.toArray(new File[fileList.size()]);
          TreePath path;
          if (support.isDrop()) {
            path = ((JTree.DropLocation) support.getDropLocation()).getPath();
          } else {
            path = this.view.getViewProject().getFileSetView().getFileSetTree().getSelectionPath();
          }
          DefaultMutableTreeNode targetNode = (DefaultMutableTreeNode) path.getLastPathComponent();
          AddFilesToFileSet.importFolder(this.view, this.model, fileArray, targetNode);
        }
      } catch (UnsupportedFlavorException | IOException e) {
        log.error("UNKNOWN", e);
      }
    } else if (support.isDataFlavorSupported(TestNode.TestNodes)) {
      this.drop(support.getDropLocation().getDropPoint(), support.getDropAction());
    } else {
      log.warn("Unknown Flavor");
    }
    return true;
  }
}

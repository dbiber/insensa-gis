package org.insensa.view.r;

import org.insensa.inforeader.InfoReader;
import org.insensa.view.extensions.AbstractImageView;
import org.insensa.view.extensions.IInfoReaderView;
import org.insensa.view.extensions.ViewerFrame;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
//import org.rosuda.javaGD.GDCanvas;
//import org.rosuda.javaGD.GDInterface;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class RGraph extends AbstractImageView implements IInfoReaderView {

    JPanel rPanel;
    JComponent component;
//    private GDCanvas gdCanvas;

    @Override
    public void addInfoReader(InfoReader iReader) {

    }

    @Override
    public void addInfoReader(List<InfoReader> iReader) {

    }

//    @Override
//    public ImageComponent[] getChildComponents() {
//        return new ImageComponent[0];
//    }

    @Override
    public JComponent getComponent() {
        return rPanel;
    }

    @Override
    public JComponent getDropTargetComponent() {
        return null;
    }

    @Override
    public ImageIcon getFrameIcon() {
        return null;
    }

    @Override
    public ViewerFrame getViewerFrame() {
        return null;
    }

    @Override
    public void init(Dimension componentDim) throws IOException {
        rPanel = new JPanel();
        try {
            rPanel = new JPanel(new BorderLayout());
//            gdCanvas = new GDCanvas(componentDim.width,componentDim.height);
//            rPanel.add(gdCanvas,BorderLayout.CENTER);

            RConnection connection = new RConnection("localhost");
//            connection.eval("Sys.setenv('JAVAGD_CLASS_NAME'='/org/insensa/view/r/RGraph')");
            connection.eval("library(JavaGD)");
            connection.eval("JavaGD()");
            connection.eval("a <- c(1,2,3,2,4)");
            connection.eval("plot(a,type=\"l\")");
            connection.close();
//            gdCanvas.setSize(componentDim.width,componentDim.height);
//            gdCanvas.initRefresh();
        } catch (RserveException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDropObjects(List<Object> objectList) throws IOException {

    }

    @Override
    public void refresh() throws IOException {

    }

    @Override
    public void resizing(Dimension dim) throws IOException {
        rPanel.setBounds(0, 0, dim.width, dim.height);

    }

    @Override
    public void setViewerFrame(ViewerFrame parent) {

    }

    @Override
    public void startView(Dimension dim, Dimension componentDimension) throws IOException {

    }

//    public class RCanvas extends GDInterface {
//        @Override
//        public void gdOpen(double v, double v1) {
//            c=gdCanvas;
//        }
//    }
}

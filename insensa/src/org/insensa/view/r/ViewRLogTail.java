package org.insensa.view.r;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.insensa.r.IMessageStreamListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;

import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;

public class ViewRLogTail extends JTextPane implements IMessageStreamListener, TailerListener {

  private static final Logger log = LoggerFactory.getLogger(ViewRLogTail.class);

  BufferedReader reader = null;
  int lastReadLine = 0;
  StyledDocument doc;
  SimpleAttributeSet keyWord;

  public ViewRLogTail() {
    doc = super.getStyledDocument();
    keyWord = new SimpleAttributeSet();
  }

  @Override
  public void fileChanged(File file) {
  }

  @Override
  public void init(Tailer tailer) {
  }

  @Override
  public void fileNotFound() {
  }

  @Override
  public void fileRotated() {
  }

  @Override
  public void handle(String line) {
    log.debug("Line: " + line);
    SwingUtilities.invokeLater(() -> {
      try {
        doc.insertString(doc.getLength(),
            "\n" + ":" + line,
            keyWord);
      } catch (BadLocationException e) {
        e.printStackTrace();
      }
    });
  }

  @Override
  public void handle(Exception ex) {
  }
}

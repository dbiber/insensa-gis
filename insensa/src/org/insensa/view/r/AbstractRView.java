/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.r;


import org.apache.commons.io.FilenameUtils;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.insensa.view.extensions.*;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public abstract class AbstractRView extends AbstractImageView implements
    IGisFileView, IInfoReaderView, IConnectionView, IInfoConnectionView {

  private static final Logger log = LoggerFactory.getLogger(AbstractRView.class);
  protected List<ConnectionFileChanger> connectionFileChangers = new ArrayList<>();
  protected List<InfoConnection> infoConnections = new ArrayList<>();
  protected List<InfoReader> infoReaders = new ArrayList<>();
  protected List<IGisFileContainer> rasterFileInformations = new ArrayList<>();
  Dimension dimension = new Dimension(300, 300);
  private List<Image> images = new ArrayList<>();
  private JComboBox comboBox = new JComboBox();
  private JPanel parentPanel;
  private JPanel panel;
  private RImagePanel imagePanel;
  //  private RRasterConnection rConnection;
  private String device = "png";
  private String extension = "png";
  private String address = "172.0.0.1";
  private JProgressBar progressBar;

  @Override
  public JComponent getComponent() {
    return this.parentPanel;
  }

  @Override
  public void init(Dimension componentDim) {
    this.dimension = componentDim;
    progressBar = new JProgressBar();
    progressBar.setIndeterminate(true);
    parentPanel = new JPanel();
    parentPanel.setLayout(new BoxLayout(this.parentPanel, BoxLayout.LINE_AXIS));
    initRImagePanel();
//    panel = new RImagePanel();
    parentPanel.add(progressBar);

    run();
  }

  private void initRImagePanel() {
    panel = new JPanel(new BorderLayout());
    imagePanel = new RImagePanel();
    JToolBar toolBar = new JToolBar();

    toolBar.add(comboBox);
    panel.add(toolBar, BorderLayout.PAGE_START);
    panel.add(imagePanel, BorderLayout.CENTER);
    panel.addMouseWheelListener(e -> {
      int currentIndex = comboBox.getSelectedIndex();
      int length = images.size();
      int notches = e.getWheelRotation();
      if (notches < 0) {
        if (currentIndex < length - 1) {
          currentIndex++;
        }
      } else {
        if (currentIndex > 0) {
          currentIndex--;
        }
      }
      comboBox.setSelectedIndex(currentIndex);
    });
    comboBox.addActionListener(e -> {
      int i = comboBox.getSelectedIndex();
      imagePanel.setImage(images.get(i));
      imagePanel.revalidate();
      imagePanel.repaint();
    });
  }

  private void run() {
    new SwingWorker<Object, Object>() {

      @Override
      protected Object doInBackground() {


        RSession session = null;
        try {
          RConnection connection = prepareConnection(address);
          session = new RSession(connection);
          RMessageStream stream = session.loadOutput(RMessageType.ALL);
          RMessageStreamMonitor.getInstance().registerMessageStream(stream);

          assignVariables(session);
          executeScript(session);
          loadRImage(session);

          session.unloadOuput();
        } catch (REngineException | REXPMismatchException | IOException e) {
          log.error("Error", e);
        } finally {
          if (session != null) {
            try {
              session.closeAndClear();
            } catch (REXPMismatchException | REngineException e) {
              e.printStackTrace();
            }
          }
        }

        return session;
      }

      @Override
      protected void done() {
        //          rConnection = (RRasterConnection) get();
        parentPanel.remove(progressBar);
        parentPanel.add(panel);
        parentPanel.revalidate();
        parentPanel.repaint();
      }
    }.execute();
  }

  private RConnection prepareConnection(String address) throws REngineException, REXPMismatchException {

    RConnection rConnection = new RConnection("127.0.0.1");
    // if Cairo is installed, we can get much nicer graphics, so try to load it
    if (rConnection.parseAndEval("suppressWarnings(require('Cairo',quietly=TRUE))").asInteger() > 0) {
      device = "CairoPNG"; // great, we can use Cairo device
    } else {
      log.error("(consider installing Cairo package for better bitmap output)");
    }
    REXP xp = rConnection.parseAndEval("try(" + device + "('test%03d." + extension
        + "',res=NA,units='px',width="
        + dimension.width
        + ",height=" + dimension.height + ",pointsize = 12))");
    if (xp.inherits("try-error")) { // if the result is of the class try-error then there was a problem
      log.error("Can't open " + device + " graphics device:\n" + xp.asString());
      // this is analogous to 'warnings', but for us it's sufficient to get just the 1st warning
      REXP warning = rConnection.eval("if (exists('last.warning') && length(last.warning)>0) names(last.warning)[1] else 0");
      if (warning.isString()) {
        log.error(warning.asString());
      }
      return null;
    }
    return rConnection;
  }


  private void loadRImage(RSession session) throws REXPMismatchException, REngineException, IOException {
    session.eval("dev.off()");
    String wd = session.eval("getwd()").asString();
    File wdDir = new File(wd);
    File[] files = wdDir.listFiles();
    for (int i = 0; i < files.length; i++) {
      if (files[i].getName().endsWith(extension)) {
        images.add(ImageIO.read(files[i]));
        comboBox.addItem(files[i].getName());
      }
    }
    session.eval("unlink('" + FilenameUtils.separatorsToUnix(wd) + "/*." + extension + "')");
  }

  public void setImage(Image image) {
    images.add(image);
  }

  protected String getErrorMessage(RSession session) throws REXPMismatchException, REngineException {
    return session.eval("geterrmessage()").asString();

  }

  @Override
  public void refresh() throws IOException {

    this.dimension = parentPanel.getSize();
    run();
    log.debug("panel.getSize(): " + parentPanel.getSize());
    log.debug("this.parentViewerFrame.getContentPane().getSize(): "
        + this.parentViewerFrame.getContentPane().getSize());
  }

  @Override
  public void resizing(Dimension dim) {
  }

  @Override
  public void addInfoReader(List<InfoReader> infoReaders) {
    infoReaders.forEach(this::addInfoReader);
  }

  @Override
  public void addInfoReader(InfoReader infoReader) {
    this.infoReaders.add(infoReader);
  }

  @Override
  public void addInfoConnection(List<InfoConnection> connectionList) {
    infoConnections.addAll(connectionList);
  }

  @Override
  public void addInfoConnection(InfoConnection connection) {
    infoConnections.add(connection);
  }

  @Override
  public void addGisFileContainers(List<IGisFileContainer> rasterFileList) {
    rasterFileList.forEach(this::addGisFileContainer);
  }

  @Override
  public void addGisFileContainer(IGisFileContainer rasterFile) {
    rasterFileInformations.add(rasterFile);
  }

  protected List<InfoReader> getInfoReaders() {
    return infoReaders;
  }

  protected List<IGisFileContainer> getRasterFileInformations() {
    return rasterFileInformations;
  }

  @Override
  public JComponent getDropTargetComponent() {
    return panel;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
    int infoReaderCount = this.infoReaders.size();
    int rasterFileInformationCount = this.rasterFileInformations.size();
    for (Object iObj : objectList) {
      if (iObj instanceof InfoReader) {
        this.addInfoReader((InfoReader) iObj);
      } else if (iObj instanceof RasterFileContainer) {
        this.addGisFileContainer((RasterFileContainer) iObj);
      }
    }

    if (infoReaderCount < this.infoReaders.size()
        || rasterFileInformationCount < this.rasterFileInformations.size()) {
      this.refresh();
    }
  }

  @Override
  public void addConnection(ConnectionFileChanger connection) {
    connectionFileChangers.add(connection);
  }

  @Override
  public void addConnections(List<ConnectionFileChanger> connectionList) {
    connectionFileChangers.addAll(connectionList);
  }

  abstract void assignVariables(RSession session) throws IOException, REngineException, REXPMismatchException;

  abstract void executeScript(RSession session) throws REXPMismatchException,
      REngineException, IOException;

  public class RImagePanel extends JPanel {
    private Image image;

    public RImagePanel(Image image) {
      this.image = image;
    }

    public RImagePanel() {
    }

    public void setImage(Image image) {
      this.image = image;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
      super.paintComponent(graphics);
      if (image != null)
        graphics.drawImage(image, 0, 0, getWidth(), getHeight(), null);
    }
  }
}

/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.r;

import org.apache.commons.io.FilenameUtils;
import org.insensa.IGisFileContainer;
import org.insensa.connections.RRasterConnection;
import org.insensa.infoconnections.RRasterInfoConnection;
import org.insensa.inforeader.r.RRasterInfoReader;
import org.insensa.r.RSession;
import org.insensa.view.dialogs.SettingsDialog;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenericRView extends AbstractRView {
  private static final Logger log = LoggerFactory.getLogger(GenericRView.class);

  private String filePath = "/home/dennis/dl/Projekte/insensa-gis/r/generic.R";

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  @Override
  void assignVariables(RSession session) throws IOException, REngineException, REXPMismatchException {
    session.assignInsensaIsActive();
    if (rasterFileInformations != null && !rasterFileInformations.isEmpty()) {
      session.assignInsensaWs(rasterFileInformations.get(0));
      session.assignSourceFiles(rasterFileInformations);
    }
    infoReaders.forEach(infoReader -> {
      if (infoReader instanceof RRasterInfoReader) {
        ((RRasterInfoReader) infoReader).getVariables().forEach((s, variable) -> {
          session.assign(variable);
        });
      }
    });
    connectionFileChangers.forEach(connectionFileChanger -> {
      if (connectionFileChanger instanceof RRasterConnection) {
        ((RRasterConnection) connectionFileChanger).getVariables().forEach((s, variable) -> {
          session.assign(variable);
        });
      }
      //TODO: only makes sence when ONE option
      session.assignSourceFiles(connectionFileChanger.getSourceFileList());
    });
    infoConnections.forEach(infoConnection -> {
      if (infoConnection instanceof RRasterInfoConnection) {
        ((RRasterInfoConnection) infoConnection).getVariables().forEach((s, variable) -> {
          session.assign(variable);
        });
      }
      //TODO: only makes sence when ONE infoConnection
      session.assignSourceFiles(infoConnection.getSourceFileList());
    });


  }

  @Override
  void executeScript(RSession session) throws REXPMismatchException, REngineException, IOException {
    REXP rxp = session.eval("source('" + FilenameUtils.separatorsToUnix(filePath) + "')");
    String wd = session.eval("getwd()").asString();
    log.debug("working dir = " + wd);
  }

  @Override
  public SettingsDialog createSettingsDialog() {
    return super.createSettingsDialog();
  }
}

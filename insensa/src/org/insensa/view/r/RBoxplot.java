/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.view.r;

import org.insensa.helpers.r.RCommandHelper;
import org.insensa.inforeader.CountValues;
import org.insensa.r.RSession;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;

import java.io.IOException;
import java.util.Map;

public class RBoxplot extends AbstractRView {

  private String rFile = "/home/dennis/dl/Projekte/insensa-gis/r/boxplot.R";


//  @Override
//  void assignVariables(RConnection connection) throws REngineException, IOException {
//    for (int i = 0; i < infoReaders.size(); i++) {
//      CountValues countValues = (CountValues) infoReaders.get(i);
//      Map<Float, Long> countMAp = countValues.getCountValues().getMap();
//      double[] doubleArray = new double[(int) countValues.getNumOfData()];
//
//      int index = 0;
//      for (Map.Entry<Float, Long> entry : countMAp.entrySet()) {
//        Long count = entry.getValue();
//        Float value = entry.getKey();
//        for (int j = 0; j < count; j++) {
//          doubleArray[index++] = value.doubleValue();
//        }
//      }
//
//      connection.assign("row" + i, doubleArray);
//    }
//    String[] namesArray = new String[infoReaders.size()];
//    StringBuilder vectorToList = new StringBuilder("rowList <- list(");
//    for (int i = 0; i < infoReaders.size(); i++) {
//      namesArray[i] = infoReaders.get(i).getTargetFile().getName();
//      vectorToList.append("row").append(i);
//      if (i < infoReaders.size() - 1) {
//        vectorToList.append(",");
//      }
//    }
//    vectorToList.append(")");
//    connection.eval(vectorToList.toString());
//    connection.assign("rowNames", namesArray);
//  }

//  @Override
//  void executeScript(RConnection connection) throws REXPMismatchException, REngineException {
//    RCommandHelper.evalAndPrint(connection, "source('" + rFile + "')");
//  }

  @Override
  void assignVariables(RSession session) throws IOException, REngineException, REXPMismatchException {

  }

  @Override
  void executeScript(RSession session) throws REXPMismatchException, REngineException, IOException {

  }
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.html;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.infoconnections.CorrelationData;
import org.insensa.infoconnections.PearsonCorrelation;
import org.insensa.view.extensions.AbstractHtmlView;
import org.insensa.view.extensions.IConnectionView;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;


public class HtmlCovAndCorTable extends AbstractHtmlView implements IConnectionView {

  private List<PearsonCorrelation> covAndCorList = new ArrayList<PearsonCorrelation>();

  /**
   *
   */
  public HtmlCovAndCorTable() {
  }

  /**
   * @see IConnectionView#addConnection(org.insensa.connections.ConnectionFileChanger)
   */
  @Override
  public void addConnection(ConnectionFileChanger connection) {
    if (connection instanceof PearsonCorrelation)
      if (!this.covAndCorList.contains(connection))
        this.covAndCorList.add((PearsonCorrelation) connection);
  }

  @Override
  public void addConnections(List<ConnectionFileChanger> connectionList) {

  }

  /**
   * @throws IOException
   */
  public void initComponents() throws IOException {
    PearsonCorrelation covAndCor = this.covAndCorList.get(0);
    List<String> fileNames = covAndCor.getFileNameList();
    String[][] sCovMatrix = new String[fileNames.size() + 1][fileNames.size() + 1];
    String[][] sCorMatrix = new String[fileNames.size() + 1][fileNames.size() + 1];
    String[][] sPValueMatrix = new String[fileNames.size() + 1][fileNames.size() + 1];
    for (int i = 0; i < fileNames.size(); i++) {
      sCovMatrix[0][i + 1] = Integer.toString(i + 1);
      sCovMatrix[i + 1][0] = Integer.toString(i + 1);
      sCorMatrix[0][i + 1] = Integer.toString(i + 1);
      sCorMatrix[i + 1][0] = Integer.toString(i + 1);
    }
    if (covAndCor.getT_test() == true) {
      for (int i = 0; i < fileNames.size(); i++) {
        sPValueMatrix[0][i + 1] = Integer.toString(i + 1);
        sPValueMatrix[i + 1][0] = Integer.toString(i + 1);
      }
    }

    List<CorrelationData> covCorList = covAndCor.getCorrelationData();
    sCovMatrix[0][0] = "";
    sCorMatrix[0][0] = "";
    sPValueMatrix[0][0] = "";
    String pValue = "";
    for (int i = 0; i < covCorList.size(); i++) {
      CorrelationData data = covAndCor.getCorrelationData().get(i);
      int index1 = data.file1;
      int index2 = data.file2;
      fileNames.get(index1);
      fileNames.get(index2);
      String cov = Float.toString(data.covariance);
      String cor = Float.toString(data.correlation);
      if (covAndCor.getT_test()) {
        pValue = Float.toString(data.pValue);
      }

      sCovMatrix[index1 + 1][index2 + 1] = cov;
      sCovMatrix[index2 + 1][index1 + 1] = cov;

      sCorMatrix[index1 + 1][index2 + 1] = cor;
      sCorMatrix[index2 + 1][index1 + 1] = cor;

      if (covAndCor.getT_test() == true) {
        sPValueMatrix[index1 + 1][index2 + 1] = pValue;
        sPValueMatrix[index2 + 1][index1 + 1] = pValue;
      }
    }
    StringBuilder htmlStringBuilder = new StringBuilder();
    htmlStringBuilder.append("<div style=\"text-align: center;\"><span style=\"font-weight: bold;\">");
    htmlStringBuilder.append("Covariance	and	Correlation (Pearson)");
    htmlStringBuilder.append("</span><br></div><br>option = ");
    htmlStringBuilder.append("<span style=\"color: rgb(255, 0, 0);\">");
    htmlStringBuilder.append(covAndCor.getOptionName());
    htmlStringBuilder.append("</span><br><br><span style=\"text-decoration: underline;\">Files:");
    htmlStringBuilder.append("</span><br>");
    htmlStringBuilder.append("<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"");
    htmlStringBuilder.append("border=\"1\" cellpadding=\"2\" cellspacing=\"2\">");
    htmlStringBuilder.append("<tbody>");

    for (int i = 0; i < fileNames.size(); i++) {
      htmlStringBuilder.append("<tr><td style=\"vertical-align: top;\">");
      htmlStringBuilder.append(Integer.toString(i + 1));
      htmlStringBuilder.append("<br></td><td style=\"vertical-align: top;\">");
      htmlStringBuilder.append(fileNames.get(i));
      htmlStringBuilder.append("<br></td></tr>");
    }
    htmlStringBuilder.append("</tbody></table><br><br>");

    // Covariance Matrix
    htmlStringBuilder.append("<span style=\"text-decoration: underline;\">" + "Corvariance Matrix:" + "</span><br>");
    htmlStringBuilder.append("<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"");
    htmlStringBuilder.append("border=\"1\" cellpadding=\"2\" cellspacing=\"2\">");
    htmlStringBuilder.append("<tbody>");
    for (String[] element : sCovMatrix) {
      htmlStringBuilder.append("<tr>");
      for (int k = 0; k < element.length; k++) {
        htmlStringBuilder.append("<td style=\"vertical-align: top;\">");
        htmlStringBuilder.append(element[k]);
        htmlStringBuilder.append("<br></td>");
      }
      htmlStringBuilder.append("</tr>");
    }
    htmlStringBuilder.append("</tbody></table><br><br>");
    // Correlation Matrix
    htmlStringBuilder.append("<span style=\"text-decoration: underline;\">Correlation Matrix:</span><br>");
    htmlStringBuilder.append("<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"");
    htmlStringBuilder.append("border=\"1\" cellpadding=\"2\" cellspacing=\"2\">");
    htmlStringBuilder.append("<tbody>");

    for (String[] element : sCorMatrix) {
      htmlStringBuilder.append("<tr>");
      for (int k = 0; k < element.length; k++) {
        htmlStringBuilder.append("<td style=\"vertical-align: top;\">");
        htmlStringBuilder.append(element[k]);
        htmlStringBuilder.append("<br></td>");
      }
      htmlStringBuilder.append("</tr>");
    }
    htmlStringBuilder.append("</tbody></table><br><br>");

    // PValue Matrix
    if (covAndCor.getT_test() == true) {
      htmlStringBuilder.append("<span style=\"text-decoration: underline;\">P-Value Matrix:</span><br>");
      htmlStringBuilder.append("<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"");
      htmlStringBuilder.append("border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody>");
      for (String[] element : sPValueMatrix) {
        htmlStringBuilder.append("<tr>");
        for (int k = 0; k < element.length; k++) {
          htmlStringBuilder.append("<td style=\"vertical-align: top;\">");
          htmlStringBuilder.append(element[k]);
          htmlStringBuilder.append("<br></td>");
        }
        htmlStringBuilder.append("</tr>");
      }
      htmlStringBuilder.append("</tbody></table><br><br>");
    }
    if (covAndCor.getPartial() == true) {
      htmlStringBuilder.append(this.initPartialCorrelationMatrices());
    }
    try {
      this.doc.insertBeforeEnd(this.body, htmlStringBuilder.toString());
    } catch (BadLocationException e) {
    }
  }

  private String initPartialCorrelationMatrices() {
    String htmlString = "";
    PearsonCorrelation covAndCor = this.covAndCorList.get(0);
    List<String> fileNames = covAndCor.getFileNameList();
    int size = fileNames.size();
    String sPartialMatrix[][][] = new String[size][size + 1][size + 1];

    for (int i = 0; i < size; i++) {
      sPartialMatrix[i][0][0] = "";
      for (int j = 0; j < size; j++) {
        if (j == i)
          continue;
        sPartialMatrix[i][0][j + 1] = Integer.toString(j + 1);
        sPartialMatrix[i][j + 1][0] = Integer.toString(j + 1);
      }
    }
    List<CorrelationData> partialLists = covAndCor.getPartCorList();

    for (int i = 0; i < partialLists.size(); i++) {
      CorrelationData data = partialLists.get(i);
      String sConstant = Integer.toString(data.constantIndex);
      String sIndex1 = Integer.toString(data.file1);
      String sIndex2 = Integer.toString(data.file2);
      String sCor = Float.toString(data.correlation);
      int constant = Integer.valueOf(sConstant);
      int index1 = Integer.valueOf(sIndex1);
      int index2 = Integer.valueOf(sIndex2);
      sPartialMatrix[constant][index1 + 1][index2 + 1] = sCor;
      sPartialMatrix[constant][index2 + 1][index1 + 1] = sCor;
    }

    for (int i = 0; i < sPartialMatrix.length; i++) {
      htmlString += "<span style=\"text-decoration: underline;\">" + "Partial Correlation - Constant:File " + (i + 1) + " = " + fileNames.get(i)
          + "</span><br>" + "<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\""
          + "border=\"1\" cellpadding=\"2\" cellspacing=\"2\">" + "<tbody>";

      for (int j = 0; j < sPartialMatrix[i].length; j++) {
        if (sPartialMatrix[i][j][0] == null)
          continue;
        htmlString += "<tr>";
        for (int k = 0; k < sPartialMatrix[i][j].length; k++) {
          if (sPartialMatrix[i][j][k] == null)
            continue;
          htmlString += "<td style=\"vertical-align: top;\">";
          htmlString += sPartialMatrix[i][j][k];
          htmlString += "<br></td>";
        }
        htmlString += "</tr>";
      }

      htmlString += "</tbody></table><br><br>";
    }

    return htmlString;
  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
  }

  @Override
  public void setTitle(String title) {

  }

  @Override
  public void refresh() throws IOException {

  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {
    super.startView(dim, componentDimension);
    this.initComponents();
  }

}

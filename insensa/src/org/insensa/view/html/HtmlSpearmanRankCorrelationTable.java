/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.html;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.infoconnections.CorrelationData;
import org.insensa.infoconnections.SpearmanRankCorrelation;
import org.insensa.view.extensions.AbstractHtmlView;
import org.insensa.view.extensions.IConnectionView;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;


public class HtmlSpearmanRankCorrelationTable extends AbstractHtmlView implements IConnectionView {

  private List<SpearmanRankCorrelation> spearmanList = new ArrayList<SpearmanRankCorrelation>();

  /**
   * @throws IOException
   */
  public HtmlSpearmanRankCorrelationTable() throws IOException {
  }

  /**
   * @see IConnectionView#addConnection(org.insensa.connections.ConnectionFileChanger)
   */
  @Override
  public void addConnection(ConnectionFileChanger connection) {
    if (connection instanceof SpearmanRankCorrelation)
      if (!this.spearmanList.contains(connection))
        this.spearmanList.add((SpearmanRankCorrelation) connection);
  }

  @Override
  public void addConnections(List<ConnectionFileChanger> connectionList) {
  }

  /**
   * @throws IOException
   */
  private void initComponents() throws IOException {
    // scrollPane=new JScrollPane();
    SpearmanRankCorrelation spearman = this.spearmanList.get(0);
    List<String> fileNames = spearman.getFileNameList();
    String[][] sCovMatrix = new String[fileNames.size() + 1][fileNames.size() + 1];
    String[][] sCorMatrix = new String[fileNames.size() + 1][fileNames.size() + 1];
    String[][] sPValueMatrix = new String[fileNames.size() + 1][fileNames.size() + 1];
    for (int i = 0; i < fileNames.size(); i++) {
      sCovMatrix[0][i + 1] = Integer.toString(i + 1);
      sCovMatrix[i + 1][0] = Integer.toString(i + 1);
      sCorMatrix[0][i + 1] = Integer.toString(i + 1);
      sCorMatrix[i + 1][0] = Integer.toString(i + 1);
    }
    if (spearman.getT_test() == true) {
      for (int i = 0; i < fileNames.size(); i++) {
        sPValueMatrix[0][i + 1] = Integer.toString(i + 1);
        sPValueMatrix[i + 1][0] = Integer.toString(i + 1);
      }
    }
    List<CorrelationData> correlationData = spearman.getCorrelationData();
    sCovMatrix[0][0] = "";
    sCorMatrix[0][0] = "";
    sPValueMatrix[0][0] = "";
    String pValue = "";
    for (int i = 0; i < correlationData.size(); i++) {
      int index1 = correlationData.get(i).file1;
      int index2 = correlationData.get(i).file2;
      String cov = Float.toString(correlationData.get(i).covariance);
      String cor = Float.toString(correlationData.get(i).correlation);
      if (spearman.getT_test()) {
        pValue = Float.toString(correlationData.get(i).pValue);
      }

      sCovMatrix[index1 + 1][index2 + 1] = cov;
      sCovMatrix[index2 + 1][index1 + 1] = cov;

      sCorMatrix[index1 + 1][index2 + 1] = cor;
      sCorMatrix[index2 + 1][index1 + 1] = cor;

      if (spearman.getT_test()) {
        sPValueMatrix[index1 + 1][index2 + 1] = pValue;
        sPValueMatrix[index2 + 1][index1 + 1] = pValue;
      }
    }

    String htmlString = "<div style=\"text-align: center;\"><span style=\"font-weight: bold;\">" + "Covariance	and	Correlation (Spearman)"
        + // NAME
        "</span><br></div>" + "<br>" + "option = "
        + // NAME
        "<span style=\"color: rgb(255, 0, 0);\">" + spearman.getOptionName()
        + // NAME
        "</span><br>" + "<br>" + "<span style=\"text-decoration: underline;\">" + "Files:"
        + // NAME
        "</span><br>" +

        "<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"" + "border=\"1\" cellpadding=\"2\" cellspacing=\"2\">"
        + "<tbody>";
    for (int i = 0; i < fileNames.size(); i++) {
      htmlString += "<tr>" + "<td style=\"vertical-align: top;\">" + Integer.toString(i + 1) + "<br></td>" + "<td style=\"vertical-align: top;\">"
          + fileNames.get(i) + "<br></td></tr>";

    }
    htmlString += "</tbody></table><br><br>";

    // Covariance Matrix
    htmlString += "<span style=\"text-decoration: underline;\">" + "Corvariance Matrix:" + "</span><br>"
        + "<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"" + "border=\"1\" cellpadding=\"2\" cellspacing=\"2\">"
        + "<tbody>";
    for (String[] element: sCovMatrix) {
      htmlString += "<tr>";
      for (int k = 0; k < element.length; k++) {
        htmlString += "<td style=\"vertical-align: top;\">";
        htmlString += element[k];
        htmlString += "<br></td>";
      }
      htmlString += "</tr>";
    }
    htmlString += "</tbody></table><br><br>";

    // Correlation Matrix
    htmlString += "<span style=\"text-decoration: underline;\">" + "Correlation Matrix:" + "</span><br>"
        + "<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\"" + "border=\"1\" cellpadding=\"2\" cellspacing=\"2\">"
        + "<tbody>";
    for (String[] element: sCorMatrix) {
      htmlString += "<tr>";
      for (int k = 0; k < element.length; k++) {
        htmlString += "<td style=\"vertical-align: top;\">";
        htmlString += element[k];
        htmlString += "<br></td>";
      }
      htmlString += "</tr>";
    }
    htmlString += "</tbody></table><br><br>";

    // PValue Matrix
    if (spearman.getT_test() == true) {
      htmlString += "<span style=\"text-decoration: underline;\">" + "P-Value Matrix:" + "</span><br>"
          + "<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\""
          + "border=\"1\" cellpadding=\"2\" cellspacing=\"2\">" + "<tbody>";
      for (String[] element: sPValueMatrix) {
        htmlString += "<tr>";
        for (int k = 0; k < element.length; k++) {
          htmlString += "<td style=\"vertical-align: top;\">";
          htmlString += element[k];
          htmlString += "<br></td>";
        }
        htmlString += "</tr>";
      }
      htmlString += "</tbody></table><br><br>";
    }

    if (spearman.getPartial() == true)
      htmlString += this.initPartialCorrelationMatrices();
    try {
      this.doc.insertBeforeEnd(this.body, htmlString);
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  private String initPartialCorrelationMatrices() {
    String htmlString = "";
    SpearmanRankCorrelation spearman = this.spearmanList.get(0);
    List<String> fileNames = spearman.getFileNameList();
    int size = fileNames.size();
    String sPartialMatrix[][][] = new String[size][size + 1][size + 1];

    for (int i = 0; i < size; i++) {
      sPartialMatrix[i][0][0] = "";
      for (int j = 0; j < size; j++) {
        if (j == i)
          continue;
        sPartialMatrix[i][0][j + 1] = Integer.toString(j + 1);
        sPartialMatrix[i][j + 1][0] = Integer.toString(j + 1);
      }
    }

    List<CorrelationData> partialLists = spearman.getPartCorList();

    for (int i = 0; i < partialLists.size(); i++) {
      String sCor = partialLists.get(i).correlation.toString();
      int constant = partialLists.get(i).constantIndex;
      int index1 = partialLists.get(i).file1;
      int index2 = partialLists.get(i).file2;
      sPartialMatrix[constant][index1 + 1][index2 + 1] = sCor;
      sPartialMatrix[constant][index2 + 1][index1 + 1] = sCor;
    }

    for (int i = 0; i < sPartialMatrix.length; i++) {
      htmlString += "<span style=\"text-decoration: underline;\">" + "Partial Correlation - Constant:File " + (i + 1) + " = " + fileNames.get(i)
          + "</span><br>" + "<table style=\"text-align: left; width: 100%; margin-left: 0px; margin-right: auto;\""
          + "border=\"1\" cellpadding=\"2\" cellspacing=\"2\">" + "<tbody>";

      for (int j = 0; j < sPartialMatrix[i].length; j++) {
        if (sPartialMatrix[i][j][0] == null)
          continue;
        htmlString += "<tr>";
        for (int k = 0; k < sPartialMatrix[i][j].length; k++) {
          if (sPartialMatrix[i][j][k] == null)
            continue;
          htmlString += "<td style=\"vertical-align: top;\">";
          htmlString += sPartialMatrix[i][j][k];
          htmlString += "<br></td>";
        }
        htmlString += "</tr>";
      }

      htmlString += "</tbody></table><br><br>";
    }

    return htmlString;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {

  }

  @Override
  public void refresh() throws IOException {

  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {
    super.startView(dim, componentDimension);
    this.initComponents();
  }

  @Override
  public void setTitle(String title) {

  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.html;

import org.gdal.osr.SpatialReference;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.view.extensions.AbstractHtmlView;
import org.insensa.view.extensions.IGisFileView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;


public class SpatialReferenceTable extends AbstractHtmlView implements IGisFileView {
  private static final Logger log = LoggerFactory.getLogger(SpatialReferenceTable.class);

  private List<IGisFileContainer> rasterFileList = new ArrayList<>();
  private Map<IGisFileContainer, List<String>> fileMap = new TreeMap<>();
  private Element mainTable;
  private String[] dataArray = new String[]
      {"LOCAL_CS", "GEOGSC", "PROJCS", "PROJECTION", "DATUM"};
  private List<String> tableRowDataArray = new ArrayList<String>();

  @Override
  public void addGisFileContainer(IGisFileContainer rasterFile) {
    if (rasterFile != null && !this.rasterFileList.contains(rasterFile))
      this.rasterFileList.add(rasterFile);
  }

  @Override
  public void addGisFileContainers(List<IGisFileContainer> rasterFileList) {
    for (IGisFileContainer iFile : rasterFileList)
      this.addGisFileContainer(iFile);
  }

  private void addTableContent() throws IOException {
    this.sortMember();
    String htmlString = "";
    htmlString += "<tr><th></th>";
    for (IGisFileContainer iFile : this.rasterFileList) {
      htmlString += "<th>" + iFile.getName() + "</th>";
    }
    htmlString += "</tr>";

    for (int i = 0; i < this.tableRowDataArray.size(); i++) {
      htmlString += "<tr>";
      htmlString += "<th>";
      htmlString += this.tableRowDataArray.get(i);
      htmlString += "</th>";
      for (IGisFileContainer iFile : this.rasterFileList) {
        htmlString += "<td>";
        htmlString += this.fileMap.get(iFile).get(i);//Todo: Out of bounds Exception (Bei Drainage.asc)
        htmlString += "</td>";
      }
      htmlString += "</tr>";
    }

    try {
      this.doc.insertBeforeEnd(this.mainTable, htmlString);
    } catch (BadLocationException e) {
    }
  }

  private void initDocument() throws IOException {
    String htmlString = "<br><h2 align=\"center\">File Information Table</h2>"
        + "<table id=\"mainTable\" style=\"text-align:right;width:100%;table-layout:fixed;word-wrap:break-word; \" border=\"1\" "
        + "cellpadding=\"5\" cellspacing=\"0\">" + "</table>";
    try {
      this.doc.insertAfterStart(this.body, htmlString);
    } catch (BadLocationException ignored) {
    }
    Element table = this.doc.getElement("mainTable");
    this.mainTable = table;
    this.addTableContent();
  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
    int lstCnt = this.rasterFileList.size();
    for (Object iObj : objectList)
      if (iObj instanceof RasterFileContainer)
        this.addGisFileContainer((RasterFileContainer) iObj);
    if (lstCnt < this.rasterFileList.size())
      this.refresh();
  }

  @Override
  public void setTitle(String title) {
  }

  @Override
  public void refresh() throws IOException {
    this.editorPane.setText("");
    this.fileMap.clear();
    this.initDocument();
    this.editorPane.setCaretPosition(0);
  }

  private void sortMember() {
    this.tableRowDataArray.clear();
    for (String element : this.dataArray) {
      this.tableRowDataArray.add(element);
    }

    for (IGisFileContainer iterFile : this.rasterFileList) {
      RasterFileContainer iFile = (RasterFileContainer)iterFile;
      List<String> dataList;
      log.debug(((RasterFile)iFile.getGisFile()).getDataset(RasterFileAccess.READ_ONLY).GetProjection());
      SpatialReference spatialReference = iFile.getGisFile().getSpatialReference();
      if (spatialReference != null) {
        dataList = new ArrayList<String>();
        for (String element : this.dataArray) {
          String sData = spatialReference.GetAttrValue(element);
          if (sData != null)
            dataList.add(sData);
          else
            dataList.add("");
        }
      } else {
        dataList = new ArrayList<String>();
        for (String element : this.dataArray) {
          this.tableRowDataArray.add(element);
          dataList.add("");
        }
      }

      double[] geoTransform = iFile.getGisFile().getGeoTransform();
      int xSize = iFile.getGisFile().getNCols();
      int ySize = iFile.getGisFile().getNRows();
      double cellsize1 = geoTransform[1];
      double cellsize2 = geoTransform[5];
      double extentLeft = geoTransform[0];
      double extentTop = geoTransform[3];
      double extentRight = extentLeft + (xSize * geoTransform[1]);
      double extentBottom = extentTop + (ySize * geoTransform[5]);
      String dataType = DataTypeHelper.getDataTypeInternalName(iFile.getGisFile().getDataType());
      float noDataValue = iFile.getGisFile().getNoDataValue();

      dataList.add(Integer.toString(xSize));
      dataList.add(Integer.toString(ySize));
      dataList.add(Double.toString(cellsize1));
      dataList.add(Double.toString(cellsize2));
      dataList.add(Double.toString(extentLeft));
      dataList.add(Double.toString(extentRight));
      dataList.add(Double.toString(extentTop));
      dataList.add(Double.toString(extentBottom));
      dataList.add(dataType);
      dataList.add(Float.toString(noDataValue));
      this.fileMap.put(iFile, dataList);
    }
    this.tableRowDataArray.add("X-Size");
    this.tableRowDataArray.add("Y-Size");
    this.tableRowDataArray.add("Cellsize 1");
    this.tableRowDataArray.add("Cellsize 2");
    this.tableRowDataArray.add("Extend Left");
    this.tableRowDataArray.add("Extend Right");
    this.tableRowDataArray.add("Extend Top");
    this.tableRowDataArray.add("Extend Bottom");
    this.tableRowDataArray.add("Data Type");
    this.tableRowDataArray.add("NoDataValue");
  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {
    super.startView(dim, componentDimension);
    this.initDocument();
  }
}

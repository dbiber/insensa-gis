/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.html;

import org.insensa.inforeader.InfoReader;
import org.insensa.view.extensions.AbstractHtmlView;
import org.insensa.view.extensions.IInfoReaderView;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;


public class HtmlInfoReaderDataTable extends AbstractHtmlView implements IInfoReaderView {
  private List<InfoReader> infoReader = new ArrayList<InfoReader>();
  private Element mainTr;

  @Override
  public void addInfoReader(InfoReader infoReader) {
    if (!this.infoReader.contains(infoReader))
      this.infoReader.add(infoReader);

  }

  @Override
  public void addInfoReader(List<InfoReader> infoReaderList) {
    for (InfoReader iReader : infoReaderList)
      this.addInfoReader(iReader);
  }

  private void addTableContent(InfoReader infoReader) throws IOException {
    StringBuilder htmlStringBuilder = new StringBuilder();
    htmlStringBuilder.append("<td valign=\"top\"><h5 align=\"center\">");
    htmlStringBuilder.append(infoReader.getTargetFile().getName());
    htmlStringBuilder.append("<br>");
    htmlStringBuilder.append(infoReader.getId());
    htmlStringBuilder.append("</h5><table style=\"text-align: left; width: 100%;\" border=\"1\"cellpadding=\"5\" cellspacing=\"0\"><tbody>");
    htmlStringBuilder.append("<tr><TH>Property</TH><TH>Value</TH></tr>");

    Vector<Vector<String>> dataVec = infoReader.getData().getAttibutes();
    for (int i = 0; i < dataVec.size(); i++) {
      Vector<String> theVec = dataVec.get(i);
      htmlStringBuilder.append("<tr>");
      for (int j = 0; j < theVec.size(); j++) {
        String data = theVec.get(j);
        htmlStringBuilder.append("<td>");
        htmlStringBuilder.append(data);
        htmlStringBuilder.append("</td>");

      }
      htmlStringBuilder.append("</tr>");
    }
    htmlStringBuilder.append("</tbody></table></td>");
    try {
      this.doc.insertBeforeEnd(this.mainTr, htmlStringBuilder.toString());
    } catch (BadLocationException e) {
    }
  }

  private void initDocument() throws IOException {
    String htmlString = "<br><h2 align=\"center\">Count Values</h2>" + "<table id=\"mainTable\" ><tr id=\"mainTr\">" + "</tr></table>";
    try {
      this.doc.insertAfterStart(this.body, htmlString);
    } catch (BadLocationException e) {
    }
    Element table = this.doc.getElement("mainTable");
    Element tr = this.doc.getElement("mainTr");
    this.mainTr = tr;
    for (InfoReader iReader : this.infoReader)
      this.addTableContent(iReader);

  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
    for (Object iObj : objectList)
      if (iObj instanceof InfoReader)
        this.addInfoReader((InfoReader) iObj);
    this.refresh();

  }

  @Override
  public void setTitle(String title) {

  }

  @Override
  public void refresh() throws IOException {
    this.editorPane.setText("");
    this.initDocument();
    this.editorPane.setCaretPosition(0);
  }

  @Override
  public void startView(Dimension dim, Dimension componentDimension) throws IOException {
    super.startView(dim, componentDimension);
    this.initDocument();
    this.editorPane.setCaretPosition(0);
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.view.html;

import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoReader;
import org.insensa.view.extensions.AbstractHtmlView;
import org.insensa.view.extensions.IInfoReaderView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;


public class HtmlCountValuesTable extends AbstractHtmlView implements IInfoReaderView {

  private static final Logger log = LoggerFactory.getLogger(HtmlCountValuesTable.class);


  private Element mainTr;
  private List<CountValues> countValues;

  /**
   *
   */
  public HtmlCountValuesTable() {
  }

  /**
   * @see IInfoReaderView#addInfoReader(org.insensa.inforeader.InfoReader)
   */
  @Override
  public void addInfoReader(InfoReader infoReader) {
    if (this.countValues == null)
      this.countValues = new ArrayList<CountValues>();
    if (infoReader instanceof CountValues && !this.countValues.contains(infoReader))
      this.countValues.add((CountValues) infoReader);
  }

  /**
   * @see IInfoReaderView#addInfoReader(java.util.List)
   */
  @Override
  public void addInfoReader(List<InfoReader> infoReaderList) {
    if (this.countValues == null)
      this.countValues = new ArrayList<CountValues>();
    for (InfoReader iReader : infoReaderList)
      if (iReader instanceof CountValues && !this.countValues.contains(iReader))
        this.countValues.add((CountValues) iReader);
  }

  /**
   *
   */
  private void addTableContent(CountValues countValues) throws IOException {
    this.doc.getLength();
    Iterator<Float> iData = countValues.getCountValues().keySet().iterator();
    Map<Float, Long> cMap = countValues.getCountValues().getMap();
    StringBuilder htmlStringBuilder = new StringBuilder();
    htmlStringBuilder.append("<td valign=\"top\">");
    htmlStringBuilder.append("<h5 align=\"center\">");
    htmlStringBuilder.append(countValues.getTargetFile().getName());
    htmlStringBuilder.append("</h5>");
    htmlStringBuilder.append("<table style=\"text-align: left; width: 100%;\" border=\"1\"");
    htmlStringBuilder.append("cellpadding=\"5\" cellspacing=\"0\">");
    htmlStringBuilder.append("<tbody>");
    htmlStringBuilder.append("<tr><TH>Value</TH><TH>Count</TH></tr>");

    while (iData.hasNext()) {
      Float data = iData.next();
      Long cnt = cMap.get(data);
      htmlStringBuilder.append("<tr><td>");
      htmlStringBuilder.append(data.toString());
      htmlStringBuilder.append("</td><td>");
      htmlStringBuilder.append(cnt.toString());
      htmlStringBuilder.append("</td></tr>");
    }
    htmlStringBuilder.append("</tbody></table></td>");
    try {
      this.doc.insertBeforeEnd(this.mainTr, htmlStringBuilder.toString());
    } catch (BadLocationException e) {
      log.error("Error creating html", e);
    }
  }

  /**
   * @see AbstractHtmlView#getComponent()
   */
  @Override
  public JComponent getComponent() {
    return this.scrollPane;
  }

  /**
   * @see AbstractHtmlView#getDropTargetComponent()
   */
  @Override
  public JComponent getDropTargetComponent() {
    return this.editorPane;
  }

  /**
   *
   */
  private synchronized void initDocument() throws IOException {
    String htmlString = "<br><h2 align=\"center\">Count Values</h2>"
        + "<table id=\"mainTable\" ><tr id=\"mainTr\">"
        + "</tr></table>";

    try {
      this.doc.insertAfterStart(this.body, htmlString);
    } catch (BadLocationException e) {
      log.error("Error Adding html string to body", e);
    }
    Element tr = this.doc.getElement("mainTr");
    this.mainTr = tr;
    for (CountValues iCountValues : this.countValues) {
      this.addTableContent(iCountValues);
    }
  }

  @Override
  public void onDropObjects(List<Object> objectList) throws IOException {
    int prevSize = this.countValues.size();
    for (Object iObj : objectList)
      if (iObj instanceof CountValues && !this.countValues.contains(iObj))
        this.countValues.add((CountValues) iObj);
    if (prevSize < this.countValues.size())
      this.refresh();
  }

  /**
   *
   */
  public void saveAsCsv(String filename) throws IOException {
    File outFile = new File(filename);
    if (!outFile.exists())
      outFile.createNewFile();
    FileWriter fWriter = new FileWriter(outFile);
    BufferedWriter bufWriter = new BufferedWriter(fWriter);
    for (CountValues iCounts : this.countValues) {
      Map<Float, Long> dataMap = iCounts.getCountValues().getMap();
      Iterator<Float> fIter = dataMap.keySet().iterator();
      bufWriter.write("\"" + iCounts.getTargetFile().getName() + "\",\n");

      bufWriter.write("\"Value\",\"Count\"\n");
      while (fIter.hasNext()) {
        Float nextVal = fIter.next();
        Long count = dataMap.get(nextVal);
        bufWriter.write(nextVal.toString() + "," + count.toString() + "\n");
      }
      bufWriter.newLine();
    }
    bufWriter.flush();
    bufWriter.close();
  }

  @Override
  public void setTitle(String title) {

  }

  @Override
  public void refresh() throws IOException {
    this.editorPane.setText("");
    // scrollPane.setViewportView(scrollPane);
    new SwingWorker<Object, Object>() {
      @Override
      protected Object doInBackground() throws Exception {
        HtmlCountValuesTable.this.initDocument();
        return null;
      }

      @Override
      protected void done() {
        HtmlCountValuesTable.this.editorPane.setCaretPosition(0);
      }
    }.execute();
  }

  @Override
  public void init(Dimension componentDim) throws IOException {
    super.init(componentDim);
    JMenuItem saveAsCSV = new JMenuItem("As CSV");
    this.save.add(saveAsCSV);
    saveAsCSV.addActionListener(new SaveAsCSVListener());

    // initProgressBar();
    new SwingWorker<Object, Object>() {
      @Override
      protected Object doInBackground() throws Exception {
        HtmlCountValuesTable.this.initDocument();
        return null;
      }

      @Override
      protected void done() {
        HtmlCountValuesTable.this.editorPane.setCaretPosition(0);
        // scrollPane.setViewportView(editorPane);
      }
    }.execute();
    // initComponents();
  }

  @Override
  public ImageIcon getFrameIcon() {
    return null;
  }

  private class SaveAsCSVListener implements ActionListener {

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
      int choise = fileChooser.showSaveDialog(HtmlCountValuesTable.this.editorPane);
      if (choise == JFileChooser.APPROVE_OPTION) {
        File selFile = fileChooser.getSelectedFile();
        if (selFile.exists()) {
          int option = JOptionPane.showConfirmDialog(HtmlCountValuesTable.this.editorPane, "File already exits"
              + "do you really want do owerwrite this file?\n", "File Exists", JOptionPane.YES_NO_OPTION);
          if (option == JOptionPane.YES_OPTION) {
            if (!selFile.delete()) {
              JOptionPane.showMessageDialog(HtmlCountValuesTable.this.editorPane, "Error Deleting File", "error", JOptionPane.ERROR_MESSAGE);
            }
          } else {
            return;
          }
        }
        String fileName = selFile.getAbsolutePath();
        if (!fileName.endsWith(".csv"))
          fileName += ".csv";
        try {
          HtmlCountValuesTable.this.saveAsCsv(fileName);
        } catch (IOException e1) {
          e1.printStackTrace();
        }

      }
    }

  }
}

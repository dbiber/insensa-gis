/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.insensa.XMLProperties.XmlProperties;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.ConnectionManager;
import org.insensa.connections.StackConnection;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.IOHelper;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.infoconnections.InfoConnectionManager;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.OptionManager;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Main Model implementation
 */
public class Model {
  private User activeUser;
  private List<User> userList;
  private OptionManager optionManager = new OptionManager();
  private InfoManager infoManager = new InfoManager();
  private ConnectionManager connectionManager = new ConnectionManager();
  private InfoConnectionManager infoConnectionManager = new InfoConnectionManager();
  private IGisFileSet activeFileSet = null;
  private IGisFileContainer activeRasterFile = null;
  private XmlProperties properties;

  public Model() throws IOException, JDOMException {
    File config = IOHelper.getInstance().getXmlDir();
    if (!config.exists()) {
      if (!config.mkdirs()) {
        throw new IOException("Config folder can not be created");
      }
    }
    // First create all User
    // properties will open the Users an add the projects to them
    this.userList = new ArrayList<User>();
    config = IOHelper.getInstance().getMainConfigFile();
    this.properties = new XmlProperties(config.getAbsolutePath());
    this.userList.addAll(this.properties.getUsers());
    // after creating the User Objects add the properties as Listener
    // so that it can listen to User changes
    for (User user : this.userList) {
      user.addUserListener(this.properties);
    }
  }

  /**
   * TODO Das ist NUR fuer das speichern des PluginCache
   * entstanden (wird in ViewStartScreen gemacht)...
   * besser waehre eine Loesung mit Listener (MVC Violation)
   *
   * @return the properties
   */
  public XmlProperties getProperties() {
    return properties;
  }

  public IGisFileSet addChildFileSet(IGisFileSet targetFileSet,
                                     String outputFileSetName,
                                     boolean serialization)
      throws IOException, JDOMException,
      GisFileException {
    IGisFileSet tmpSet = targetFileSet
        .addChildSet(targetFileSet.getPath()
                + targetFileSet.getName(), outputFileSetName, serialization,
            null);
    return tmpSet;
  }

//  public IGisFileSet createAndAddConnection(List<IGisFileContainer> sourceFileList,
//                                   IGisFileSet outputFileSet,
//                                   ConnectionFileChanger connectionFileChanger,
//                                   String outputFileSetName, String outputFileName)
//      throws IOException, JDOMException, GisFileException {
//
//    IGisFileSet tmpSet;
//    if (outputFileSetName != null) {
//      tmpSet = outputFileSet.addChildSet(outputFileSet.getPath()
//              + outputFileSet.getName(), outputFileSetName,
//          true, null);
//    } else {
//      tmpSet = outputFileSet;
//    }
//
//    // eCon.setSourceFileList(sourceFileList);
//    RasterFileContainer newFile = new RasterFileContainer(tmpSet,
//        outputFileName, tmpSet.getPath() + tmpSet.getName() + File.separator
//        + outputFileName, null);
//    tmpSet.addGisFileInformation(newFile, true);
//    // ConnectionFileChanger tmpCon =
//    // connectionManager.getConnectionFileChanger(eCon);
//    connectionFileChanger.setSourceFileList(sourceFileList);
//    // TODO TEST BUG1002
//    // Observer Pattern
//    for (IGisFileContainer iterFile : sourceFileList) {
//      iterFile.addFileObserver(connectionFileChanger);
//    }
//
//    connectionFileChanger.setOutputFileSet(tmpSet);
//    connectionFileChanger.setOutputFile(newFile);
//    newFile.setConnectionFileChanger(connectionFileChanger);
//
//    // getActiveProject().createAndAddConnection(eCon,true);
//    this.getActiveProject().createAndAddConnection(connectionFileChanger);
//    return tmpSet;
//  }


  private void removeConnection(ConnectionFileChanger con) throws IOException {
    if (con != null) {
      for (IGisFileContainer iterFile : con.getSourceFileList()) {
        iterFile.removeFileObserver(con);
      }
      if (!con.isUsed()) {
        getActiveProject().removeConnection(con);
      }
    }
  }

  public InfoConnection createAndAddInfoConnection(IGisFileContainer targetStackFile,
                                                   String infoConnectionName)
      throws IOException {
    if (!targetStackFile.isConnection()) {
      throw new IOException("Target File Container does not have a connection");
    }
    ConnectionFileChanger oldConnection = targetStackFile.getConnectionFileChanger();
    List<? extends IGisFileContainer> oldSourceFiles = oldConnection.getSourceFileList();

    InfoConnection newInfoConnection = this.infoConnectionManager
        .getInfoConnection(infoConnectionName,
            oldSourceFiles,
            oldConnection.getOutputFileSet(),
            targetStackFile);

    for (IGisFileContainer iterFile: oldSourceFiles) {
      iterFile.addFileObserver(newInfoConnection);
    }
    List<String> icDeps = newInfoConnection.getInfoConnectionDependencies();
    for (String dep : icDeps) {
      createAndAddInfoConnection(targetStackFile,dep);
    }
    targetStackFile.addInfoConnection(newInfoConnection);

    return newInfoConnection;
  }

  public InfoConnection createAndAddInfoConnection(List<IGisFileContainer> sourceFileList,
                                                   IGisFileSet outputFileSet,
                                                   String infoConnectionName,
                                                   String outputFileSetName,
                                                   String outputFileName)
      throws IOException, JDOMException {

    ConnectionFileChanger connectionFileChanger =
        createAndAddConnection(sourceFileList, outputFileSet,
            StackConnection.NAME, outputFileSetName, outputFileName);

    IGisFileContainer targetFile = connectionFileChanger.getOutputFileContainer();

    InfoConnection newInfoConnection = this.infoConnectionManager
        .getInfoConnection(infoConnectionName,
            sourceFileList,
            outputFileSet,
            targetFile);
    for (IGisFileContainer iterFile: sourceFileList) {
      iterFile.addFileObserver(newInfoConnection);
    }
    List<String> icDeps = newInfoConnection.getInfoConnectionDependencies();
    for (String dep : icDeps) {
      createAndAddInfoConnection(targetFile,dep);
    }
    targetFile.addInfoConnection(newInfoConnection);
    return newInfoConnection;
  }

  /**
   * Creates a new Connection and add's it to the targetStackFile.
   *
   * @param targetStackFile the fileContainer to add the connection to.
   *                        There should already a connection available
   *                        on that container, otherwise this method should not be called.
   * @param connectionName  the name of the connection to create.
   * @return the newly created connection
   */
  public ConnectionFileChanger createAndAddConnection(IGisFileContainer targetStackFile,
                                                      String connectionName) throws IOException {
    if (!targetStackFile.isConnection()) {
      throw new IOException("Target File Container does not have a connection");
    }
    ConnectionFileChanger oldConnection = targetStackFile.getConnectionFileChanger();
    List<? extends IGisFileContainer> oldSourceFiles = oldConnection.getSourceFileList();

    ConnectionFileChanger newConnection = this.connectionManager
        .getConnectionFileChanger(connectionName,
            oldSourceFiles,
            oldConnection.getOutputFileSet(),
            targetStackFile);
    removeConnection(oldConnection);
    for (IGisFileContainer iterFile: oldSourceFiles) {
      iterFile.addFileObserver(newConnection);
    }
    targetStackFile.setConnectionFileChanger(newConnection);
    List<String> icDeps = newConnection.getInfoConnectionDependencies();
    for (String dep : icDeps) {
      createAndAddInfoConnection(targetStackFile,dep);
    }
    this.getActiveProject().addConnection(newConnection);
    return newConnection;
  }


  /**
   * Creates a new FileContainer and Connection, add the Connection to the FileContainer and adds the
   * FileContainer to a FileSet
   *
   * @param sourceFileList    List of {@link IGisFileContainer} to add to connection
   * @param outputFileSet     the target fileSet to store the new File(Connection)
   * @param connectionName    the name of the connection to create
   * @param outputFileSetName if != null, create a new Fileset within the outputFileSet
   * @param outputFileName    the name of the newly created file
   * @return the newly created connection
   */
  public ConnectionFileChanger createAndAddConnection(List<IGisFileContainer> sourceFileList,
                                                      IGisFileSet outputFileSet, String connectionName,
                                                      String outputFileSetName, String outputFileName)
      throws IOException, JDOMException {
    IGisFileSet tmpSet = outputFileSet;
    if (outputFileSetName != null) {
      tmpSet = outputFileSet.addChildSet(outputFileSet.getPath()
              + outputFileSet.getName(), outputFileSetName, true,
          null);
    }
    //TODO: Use Factory instead ?
    IGisFileContainer newFile = new RasterFileContainer(tmpSet, outputFileName,
        tmpSet.getPath() + tmpSet.getName() + File.separator
            + outputFileName, null);
    tmpSet.addGisFileInformation(newFile, true);

    return addConnection(connectionName,
        sourceFileList,
        outputFileSet,
        newFile);
  }

  public IGisFileSet addConnection(List<? extends IGisFileContainer> sourceFileList,
                                   IGisFileSet outputFileSet,
                                   ConnectionFileChanger eCon,
                                   String outputFileSetName,
                                   String outputFileName) throws IOException, JDOMException {
    IGisFileSet tmpSet = outputFileSet;
    if (outputFileSetName != null) {
      tmpSet = outputFileSet.addChildSet(outputFileSet.getPath()
              + outputFileSet.getName(), outputFileSetName,
          true, null);
    }

    IGisFileContainer newFile = new RasterFileContainer(tmpSet,
        outputFileName, tmpSet.getPath() + tmpSet.getName() + File.separator
        + outputFileName, null);
    tmpSet.addGisFileInformation(newFile, true);
    eCon.setSourceFileList(sourceFileList);
    for (IGisFileContainer iterFile : sourceFileList) {
      iterFile.addFileObserver(eCon);
    }

    eCon.setOutputFileSet(tmpSet);
    eCon.setOutputFileContainer(newFile);
    newFile.setConnectionFileChanger(eCon);

    this.getActiveProject().addConnection(eCon);
    return tmpSet;
  }

  private ConnectionFileChanger addConnection(String connectionName,
                                              List<IGisFileContainer> sourceFileList,
                                              IGisFileSet targetFileSet,
                                              IGisFileContainer targetFileContainer) throws IOException {
    ConnectionFileChanger tmpCon;
    if (connectionName.equalsIgnoreCase(StackConnection.NAME)) {
      tmpCon = new StackConnection();
      tmpCon.setSourceFileList(sourceFileList);
      tmpCon.setOutputFileSet(targetFileSet);
      tmpCon.setOutputFileContainer(targetFileContainer);
    } else {
      tmpCon = this.connectionManager
          .getConnectionFileChanger(connectionName, sourceFileList, targetFileSet, targetFileContainer);

    }
    for (IGisFileContainer iterFile: sourceFileList) {
      iterFile.addFileObserver(tmpCon);
    }
    targetFileContainer.setConnectionFileChanger(tmpCon);
    List<String> icDeps = tmpCon.getInfoConnectionDependencies();
    for (String dep : icDeps) {
      createAndAddInfoConnection(targetFileContainer,dep);
    }
    this.getActiveProject().addConnection(tmpCon);
    return tmpCon;
  }

  public void addFolderToFileSetByActive(String path, boolean serialize)
      throws IOException, JDOMException {
    File folder = new File(path);
    File[] fileArray = folder.listFiles();
    List<IGisFileContainer> activeFileList = this.activeFileSet.getFileList();

    List<String> filePathList = new ArrayList<String>();
    List<String> fileNameList = new ArrayList<String>();

    String tmpName;

    for (File element: fileArray) {
      if (element.isFile()) {
        tmpName = element.getName();
        if (tmpName.endsWith(".asc") || tmpName.endsWith("tif") || tmpName.endsWith("TIF")) {
          filePathList.add(element.getAbsolutePath());
          fileNameList.add(element.getName());
        }
      } else {
        File[] subFileArray = element.listFiles();
        for (File element2: subFileArray) {
          if (element2.isDirectory()) {
            File[] subSubFileArray = element2.listFiles();
            for (File element3: subSubFileArray) {
              if (element3.getName().equals("w001001.adf")) {
                filePathList.add(element2.getAbsolutePath());
                fileNameList.add(element2.getName());
              }
            }
          }
        }
      }
    }

    for (int i = 0; i < activeFileList.size(); i++) {
      for (int j = 0; j < filePathList.size(); ) {
        if (fileNameList.get(j).equals(activeFileList.get(i).getName())
            || fileNameList.get(j).equals(activeFileList.get(i).getOutputFileName())) {
          filePathList.remove(j);
          fileNameList.remove(j);
          // break;
        } else {
          j++;
        }
      }
    }

    if (filePathList.isEmpty()) {
      throw new IOException("no new file was found");
    }
    this.activeFileSet.addGisFileNameList(filePathList, fileNameList, null, serialize);
  }

  /**
   * Adds a new InfoReader Onject to the active Raster File and solves the
   * Dependencies by iterating this method.
   */
  public void addInfoToFileByActive(String info) throws IOException {
    InfoReader infoNew = this.infoManager.getInfoReader(info);
    this.getActiveGisFileInformation().addInfoReader(infoNew, true);
  }

  public void addInfoToFileByFile(IGisFileContainer file, String info) throws IOException {
    InfoReader infoNew = this.infoManager.getInfoReader(info);
    file.addInfoReader(infoNew, true);
  }

  public void addOptionToFileByActive(String option) throws IOException {
    Optional<OptionFileChanger> optionNew = this.optionManager.getOption(option);
    if (!optionNew.isPresent()) {
      throw new IOException("No Option with name " + option + "found");
    }
    IGisFileContainer myRasterFile = this.getActiveGisFileInformation();
    myRasterFile.addOption(optionNew.get());
  }

  public void addOptionToFileByFile(IGisFileContainer file, String option)
      throws IOException {
    Optional<OptionFileChanger> optionNew = this.optionManager.getOption(option);
    if (!optionNew.isPresent()) {
      throw new IOException("No Option with name " + option + " found");
    }

    file.addOption(optionNew.get());
  }

//  public IGisFileSet addSensitivityAnalysis(IGisFileSet outputFileSet, String newFileSetName,
//                                            String outputFileName, CIndexation prioritization)
//      throws IOException, JDOMException {
//    List<IGisFileContainer<RasterFile>> fileList = prioritization.getRasterFileList();
//
//    String outputFileNameSplit = outputFileName.substring(0, outputFileName.indexOf("."));
//    String outputFileNameNew;
//    int fileNameIterator = 0;
//
//    IGisFileSet tmpSet;
//    int listSize = fileList.size();
//    float equalValue = 1.0F / listSize;
//    equalValue *= 1000;
//    equalValue = Math.round(equalValue);
//    equalValue /= 1000;
//    CIndexation tmpCon;
//
//    fileNameIterator++;
//    outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
//    tmpCon = (CIndexation) this.connectionManager.getConnectionFileChanger(EConnection.indexation);
//    tmpCon.setOutputFileSet(outputFileSet);
//    tmpCon.setSourceFileList(fileList);
//
//    tmpCon.setConnectionElement(prioritization.getConnectionElement());
//    List<Float> weightList = tmpCon.getWeightList();
//
//    tmpCon.setUsed(false);
//    tmpSet = this.createAndAddConnection(fileList, outputFileSet, tmpCon, newFileSetName, outputFileNameNew);
//    for (int i = 0; i < weightList.size(); i++) {
//      tmpCon.getWeightList().set(i, equalValue);
//    }
//    tmpCon.getOutputFile().getGisFileInformationStorage().refreshConnectionFileChanger(tmpCon);
//
//    for (int i = 0; i < fileList.size(); i++) {
//      fileNameIterator++;
//      outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
//      tmpCon = (CIndexation) this.connectionManager
//          .getConnectionFileChanger(EConnection.indexation);
//      tmpCon.setOutputFileSet(outputFileSet);
//      tmpCon.setSourceFileList(fileList);
//      tmpCon.setConnectionElement(prioritization.getConnectionElement());
//      tmpCon.setUsed(false);
//      tmpCon.getWeightList().set(i, 0.0F);
//      tmpSet = this.createAndAddConnection(fileList, outputFileSet, tmpCon, newFileSetName,
//          outputFileNameNew);
//      tmpCon.getOutputFile().getGisFileInformationStorage().refreshConnectionFileChanger(tmpCon);
//
//    }
//
//    fileNameIterator++;
//    outputFileNameNew = outputFileNameSplit + fileNameIterator + ".tif";
//    tmpCon = (CIndexation) this.connectionManager.getConnectionFileChanger(EConnection.indexation);
//    tmpCon.setOutputFileSet(outputFileSet);
//    tmpCon.setSourceFileList(fileList);
//    tmpCon.setConnectionElement(prioritization.getConnectionElement());
//    boolean thresholdIsActive = false;
//    for (Threshold threshold : tmpCon.getThreshold1List()) {
//      if (threshold.getActivate()) {
//        thresholdIsActive = true;
//        break;
//      }
//    }
//    for (Threshold threshold : tmpCon.getThreshold2List()) {
//      if (threshold.getActivate()) {
//        thresholdIsActive = true;
//        break;
//      }
//    }
//
//    if (thresholdIsActive) {
//      tmpCon.setUsed(false);
//      tmpSet = this.createAndAddConnection(fileList, outputFileSet, tmpCon, newFileSetName,
//          outputFileNameNew);
//      tmpCon.getOutputFile().getGisFileInformationStorage().refreshConnectionFileChanger(tmpCon);
//      tmpCon.getThreshold1List().clear();
//      tmpCon.getThreshold2List().clear();
//    }
//
//    return tmpSet;
//  }

  /**
   * Adds an empty User (without Projects) has has defined a name and a workspace.
   */
  public void addUser(User user) throws IOException {
    this.properties.addUser(user);
    user.addUserListener(this.properties);
    this.userList.add(user);
  }

  public User addUser(String user, String workspace) throws IOException {
    User newUser = new User(user, workspace);
    this.properties.addUser(workspace, user);
    newUser.addUserListener(this.properties);
    this.userList.add(newUser);
    return newUser;
  }

  public IGisFileSet createNewChildFileSet(IGisFileSet parentFileSet, String newFileSetName,
                                           boolean serialize) throws IOException, JDOMException,
      GisFileException {
    return parentFileSet.addChildSet(parentFileSet.getPath() + parentFileSet.getName(),
        newFileSetName, true, null);
  }

  public GenericGisFileSet createNewFileSet(String name, boolean serialize)
      throws IOException, JDOMException, GisFileException {
    GenericGisFileSet tmpFileSet = null;
    if (!this.activeUser.getProjects().isEmpty()) {
      for (IGisFileSet fileSet: this.getActiveProject().getFileSetList()) {
        if (fileSet.getName().equals(name)) {
          throw new IOException("Model: createNewFileSet: FileSet already exists");
        }
      }
      Project activeProject = this.getActiveProject();
      tmpFileSet = new GenericGisFileSet(activeProject, name, null);
      this.getActiveProject().addFileSet(tmpFileSet);
      return tmpFileSet;
    }
    return null;
  }

  public void executeAllConnection(WorkerStatusList workerStatus) throws IOException {
    this.getActiveProject().executeAllConnections(workerStatus);
  }


  /**
   * Execute All File Options on Active File. WARNING, this function ignores
   * the dependencies of options
   */
  public void executeFileOptionsOnActiveFile(WorkerStatusList workerStatusList)
      throws IOException, JDOMException {
    IGisFileContainer activeRasterFile = this.getActiveGisFileInformation();
    activeRasterFile.executeAllOptions(workerStatusList);
  }

  public void executeInfoReaderOnActiveFile(String infoReaderName,
                                            WorkerStatusList workerStatusList)
      throws IOException {
    IGisFileContainer activeFile = this.getActiveGisFileInformation();
    activeFile.executeInfo(infoReaderName, workerStatusList);

  }

  public void executeInformationReadersOnActiveFile(WorkerStatusList workerStatusList)
      throws IOException {
    IGisFileContainer activeFile = this.getActiveGisFileInformation();
    activeFile.executeAllInfos(workerStatusList);
  }

  public Project getActiveProject() {
    if (this.activeUser == null) {
      return null;
    }
    if (this.activeUser.getActiveProject() == null) {
      return null;
    }
    return this.activeUser.getActiveProject();
  }

  public IGisFileContainer getActiveGisFileInformation() {
    return this.activeRasterFile;
  }

  public IGisFileSet getActiveGisFileSet() {
    return this.activeFileSet;
  }

  public void setActiveRasterFileSet(GenericGisFileSet fileSet) {
    this.activeFileSet = fileSet;
  }

  public User getActiveUser() {
    return this.activeUser;
  }

  public void setActiveUser(User user) {
    this.activeUser = user;
  }

  public ConnectionManager getConnectionManager() {
    return this.connectionManager;
  }

  public InfoConnectionManager getInfoConnectionManager() {
    return infoConnectionManager;
  }

  public InfoManager getInfoManager() {
    return this.infoManager;
  }

  public OptionManager getOptionManager() {
    return this.optionManager;
  }

  /**
   * @return the existing users or an empty list if no user is defined.
   */
  public List<User> getUsers() throws IOException {
    return this.userList;
  }

  public boolean openProject(User user, Project project, WorkerStatus workerStatus,
                             List<IProjectListener> projectListener)
      throws IOException, JDOMException,
      GisFileException {
    String path = user.getWorkspacePath();
    int fileCnt = project.getFileCount();
    if (fileCnt > 0) {
      if (workerStatus != null) {
        float stepsize = 100.0f / (float) fileCnt;
        workerStatus.setStepSize(stepsize);
      }
    }

    String tmpPath = path;
    if (!(tmpPath.endsWith(File.separator))) {
      tmpPath += File.separator;
    }

    this.activeUser = user;
    this.activeUser.openProject(project, workerStatus);
    return true;
  }

  /**
   * Adds required InfoReader for all Options to the active File This function
   * should be used before the options are executed.
   *
   * @return number of newly added InfoReaders
   */
  public int resolveOptionDependenciesOnActiveFile() throws IOException {
    IGisFileContainer fileContainer = this.getActiveGisFileInformation();
    int infoCnt = 0;
    if (fileContainer.getOptions() != null) {
      for (OptionFileChanger optionFileChanger: fileContainer.getOptions()) {
        if (optionFileChanger.getInfoDependencies() != null) {
          for (String optionDependency: optionFileChanger.getInfoDependencies()) {
            if (fileContainer
                .addInfoReader(this.infoManager.getInfoReader(optionDependency), true)) {
              infoCnt++;
            }
          }
        }
      }
    }
    return infoCnt;
  }

  public void setActiveRasterFile() {
    if (this.activeFileSet != null) {
      this.activeRasterFile = this.activeFileSet.getActiveGisFile();
    }
  }

  public void setActiveRasterFile(IGisFileContainer activeRasterFile) {
    this.activeRasterFile = activeRasterFile;
  }

  public void writeNewFile(RasterFileContainer file) {

  }

}

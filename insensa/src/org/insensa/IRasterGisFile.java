package org.insensa;

import org.gdal.osr.SpatialReference;
import org.insensa.helpers.DataTypeHelper;

import java.io.IOException;
import java.util.Map;

public interface IRasterGisFile extends IGisFile {
  float getCellSize();

  float getXllCorner();

  float getYllCorner();

  /**
   * @return ySize
   */
  int getNRows();

  /**
   * @return xSize
   */
  int getNCols();

  float getNoDataValue();

  int getDataType();

  double[] getGeoTransform();

  Map<String, String> getSpatialReferenceAttr();

  void readRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException;

  float[] readRaster(int xOff, int yOff, int xSize, int ySize) throws IOException;

  void createNewFile(IRasterGisFile gisFile, DataTypeHelper.RasterDataType rasterDataType, double maxValue) throws IOException;

  void writeNodataValue(double newValue) throws IOException;

  SpatialReference getSpatialReference();

  void writeNodataValueMax() throws IOException;

  String getProjectionWKT();

  String getDriverShortName() throws IOException;

  String getParent();

  boolean isTypeFloat();

  void writeRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException;
}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.insensa.XMLProperties.XmlGisFileInformation;
import org.insensa.connections.ConnectionManager;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.connections.StackConnection;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.infoconnections.InfoConnectionManager;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.SimplePluginExecThreadPool;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.optionfilechanger.OptionManager;
import org.insensa.optionfilechanger.OptionThreadPool;
import org.insensa.properties.IGisFileInformationStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Contains information about a RasterFile and all using optionFileChanger and infoReader.
 */
public class GisFileContainer implements
    Comparable<GisFileContainer>,
    IGisFileContainer {
  private static final Logger log = LoggerFactory.getLogger(GisFileContainer.class);

  protected List<InfoReader> infoReaders = new ArrayList<>();
  protected SimplePluginExecThreadPool infoReaderExecPool =
      new SimplePluginExecThreadPool(new ExecDependencyController());
  protected SimplePluginExecThreadPool infoConnectionReaderExecPool =
      new SimplePluginExecThreadPool(new ExecDependencyController());
  private List<OptionFileChanger> options = new ArrayList<>();
  private List<InfoConnection> infoConnections = new ArrayList<>();
  private ConnectionFileChanger connectionFileChanger;
  private String description = "";
  private IGisFileInformationStorage xmlInformation;
  private IGisFileContainer targetGisFileContainer = null;
  private List<FileObserver> fileObservers = new ArrayList<>();
  /**
   * @see IGisFileContainer#getOutputFileName()
   */
  private String outputFileName;
  private int lockedCnt;
  private ParentModule parent;
  private OptionThreadPool optionThreadPool = new OptionThreadPool();
  private IGisFile gisFile;

  /**
   * <p>
   * Creates a new RasterFileContainer as a copy of another, with new
   * OutputFilePath and OutputFileName.
   * </p>
   * <p>
   * All OptionFileChanger, InfoReader and ConnectionFileChanger will be copied
   * and added to the new GisFileContainer.
   * The Difference here is that the Copies are really new Instances
   * </p>
   * //TODO: Check the difference between this Constructor and the other Constructor
   * //TODO: Maybe we can remove one of them
   */
  public GisFileContainer(IGisFileContainer oldFile,
                          ParentModule parent,
                          String newOutputFileName,
                          boolean keepOpen) throws IOException,
      GisFileException {
    this.gisFile = GisFileFactory.create(oldFile.getGisFile(),
        oldFile.getGisFile().getAbsolutePath());
    this.parent = parent;

    //TODO: siehe Variablenbeschreibung
    this.setOutputFileName(newOutputFileName);

    // TODO leave the file open until everything is finished writing
    IGisFileInformationStorage oldInformation = oldFile.getGisFileInformationStorage();
    oldInformation.openGlobal();

    this.xmlInformation =
        new XmlGisFileInformation((XmlGisFileInformation) oldFile.getGisFileInformationStorage(),
            this);

    // TODO leave the file open until everything is finished writing
    this.xmlInformation.openGlobal();

    OptionManager optionManager = new OptionManager();
    for (OptionFileChanger option : oldFile.getOptions()) {
      this.addOption(optionManager.getFileChangerCopy(option));
    }

    InfoManager infoManager = new InfoManager();
    for (InfoReader info : oldFile.getInfoReaders()) {
      this.addInfoReader(infoManager.getInfoReaderCopy(info), true);
    }

    ConnectionManager connectionManager = new ConnectionManager();
    ConnectionFileChanger con = oldFile.getConnectionFileChanger();
    if (con != null) {
      this.connectionFileChanger = connectionManager
          .getConnectionFileChangerCopy(con);
      this.connectionFileChanger.setOutputFileContainer(this);
    }

    this.setDescription(oldFile.getDescription());
    if (!keepOpen) {
      this.xmlInformation.closeGlobal();
    }
    oldInformation.closeGlobal();
  }

  /**
   * <ul>
   * <li>1. Creates an instance of IGisFile with (fullName).</li>
   * <li>2. Stores outputFileName, appends extension automatically if necessary</li>
   * <li>3. Creates a new {@link XmlGisFileInformation} instance</li>
   * <li>4. If xml storage file exists, restores all
   * {@link org.insensa.extensions.IPluginExec} insances</li>
   * <li>5. Add the option if not null</li>
   * </ul>
   *
   * @param parent         a link to the parent module. Content of
   *                       {@link #getAbsOutputDirectoryPath()}
   *                       is determined by this parameter.
   * @param outputFileName the new file name this file is supposed to have (without path).
   *                       {@link OptionFileChanger}s use this (and others), to determine the target
   *                       file name.
   *                       If outputFileName does not have an extension,
   *                       it will be created automatically.
   * @param fullName       the file to use for the {@link IGisFile} creation.
   *                       calls {@link GisFileFactory#create(String)}.
   *                       The file in fullName, does not have to exist already!
   */
  public GisFileContainer(ParentModule parent,
                          String outputFileName,
                          String fullName,
                          OptionFileChanger option)
      throws GisFileException {

    gisFile = GisFileFactory.create(fullName)
        .orElseThrow(() ->
            new GisFileException("File extension could not determine the file type"));
    this.parent = parent;
    this.setOutputFileName(outputFileName);
    this.xmlInformation = new XmlGisFileInformation(this);
    this.xmlInformation.openGlobal();
    this.restoreAllStoredPluginExecInstancesIfAvailable();
    if (option != null) {
      this.addOption(option);
    }
    this.xmlInformation.closeGlobal();
  }

//  public GisFileContainer(String outputFileName, String name) {
//    gisFile = GisFileFactory.create(new File(outputFileName,name).getAbsolutePath())
//        .orElseThrow(() ->
//            new GisFileException("File extension could not determine the file type"));;
//  }

  /**
   * This Constructor was solely created for testing.
   */
  public GisFileContainer(String outputFileName, String fullName)
      throws GisFileException {
    this.gisFile = GisFileFactory.create(fullName).orElseThrow(() ->
        new GisFileException("File extension could not determine the file type"));
    this.setOutputFileName(outputFileName);
  }

  /**
   * Create a new {@link IGisFile} Instance and copy all {@link OptionFileChanger}
   * and {@link InfoReader} from <i>old</i> and RasterFileContainer The Path of the new File instance
   * is the path of newAbsoluteFilePath The XML Information for the new File
   * is the same as for the old file, no need to change this.
   *
   * @apiNote No physical file creation will be done
   */
  public GisFileContainer(String newAbsoluteFilePath,
                          IGisFileContainer oldFileContainer,
                          ParentModule parentModule,
                          boolean copyInfos,
                          boolean copyOptions,
                          boolean keepOpen) throws IOException, GisFileException {
    this.gisFile = GisFileFactory.create(oldFileContainer.getGisFile(), newAbsoluteFilePath);
    this.parent = parentModule;

    this.setOutputFileName(oldFileContainer.getOutputFileName());

    this.xmlInformation = new XmlGisFileInformation((XmlGisFileInformation) oldFileContainer
        .getGisFileInformationStorage(), this);
    this.xmlInformation.openGlobal();

    // OptionManager oManager = new OptionManager();
    if (copyOptions) {
      this.optionThreadPool = oldFileContainer.getOptionThreadPool();
      this.options.addAll(oldFileContainer.getOptions());
    } else {
      this.xmlInformation.removeAllOptionFileChangers();
    }

    if (copyInfos) {
      InfoManager infoManager = new InfoManager();
      for (InfoReader info : oldFileContainer.getInfoReaders()) {
        InfoReader infoNew = infoManager.getInfoReaderCopy(info);
        this.addInfoReader(infoNew, true);
      }
    } else {
      // TODO TEST ausklammern, hier kann ein neuer erstellt werden!
      // this.depChecker=oldFileContainer.getDepChecker();
      this.xmlInformation.removeInfoReaders();
    }

    this.setDescription(oldFileContainer.getDescription());
    // FIXME TEST BUG 1007
    if (oldFileContainer.getConnectionFileChanger() != null) {
      this.connectionFileChanger = oldFileContainer.getConnectionFileChanger();
      // this.connectionTreadPool.setConnectionFileChanger(connectionFileChanger);
      this.connectionFileChanger.setOutputFileContainer(this);
    }
    // END

    if (!keepOpen) {
      this.xmlInformation.closeGlobal();
    }
  }

  @Override
  public IGisFile getGisFile() {
    return gisFile;
  }

  @Override
  public void addFileObserver(FileObserver fileObserver) {
    this.fileObservers.add(fileObserver);
  }

  /**
   * Add the info Inforeader Object to this File.
   *
   * @return true if the new InfoReader could be added, false otherwise
   */
  @Override
  public boolean addInfoReader(InfoReader info, boolean serialize) {
    if (info == null) {
      return false;
    }

    List<String> infoDependenyNames = info.getInfoDependencies();
    InfoManager manager = new InfoManager();

    if (!info.isUsed()) {
      for (String infoDependencyName : infoDependenyNames) {
        this.addInfoReader(manager.getInfoReader(infoDependencyName), serialize);
      }
    }
    // getActiveGisFileInformation().addInfoReader(infoNew,true);

    for (InfoReader iterInfo : this.infoReaders) {
      if (iterInfo.getId().equals(info.getId())) {
        if (info.isUsed()) {
          // TODO TEST Wenn der gleiche InfoReader schon exestiert,
          // beim neuen allerdings
          // read = true ist, dann muss auch der ThreadPool
          // aktualisiert werden.
          // erster test: scheint zu gehn
          this.infoReaderExecPool.removeInfoReader(iterInfo);
          new InfoManager().cloneInfoReader(info, iterInfo);
          infoReaderExecPool.addInfoThread(iterInfo);
//          iterInfo.setInfos(info.getInfoElement());
//          this.infoReaderExecPool.addInfoThread(iterInfo);
        }
        return false;
      }

    }

    info.setTargetFile(this);
    this.infoReaders.add(info);
    this.infoReaderExecPool.addInfoThread(info);
    if (serialize) {
      this.xmlInformation.addInfoReader(info);
    }
    return true;
  }

  /**
   * Add all InfoReader Objects in infos to this File.
   */
  @Override
  public void addInfoReaders(List<InfoReader> infos, boolean serialize) throws IOException {
    if (this.infoReaders != null) {
      if (infos.size() > 0) {
        for (InfoReader infoReader : infos) {
          this.addInfoReader(infoReader, serialize);
        }
      }
    }
  }

  @Override
  public void addOption(OptionFileChanger option) {

    // TODO Hier muss eine Anfrage rein, wenn die Option schon ausgefuehrt
    // wurde
    // , also read == true, dann brauchen die Abhaengigkeiten nicht
    // aufgeloest werden

    //TODO: Why do we need this, is this still necessary ?
    //    if (!option.checkApproval(this)) {
    //      throw new IOException(option.getErrorMessage());
    //    }
    int lastId = 0;
    InfoManager infoManager = new InfoManager();
    for (OptionFileChanger optionFileChanger : this.options) {
      if (optionFileChanger.getOrderId() > lastId) {
        lastId = optionFileChanger.getOrderId();
      }
    }
    if (!option.isUsed()) {
      for (String infoDependency : option.getInfoDependencies()) {
        this.addInfoReader(infoManager.getInfoReader(infoDependency), true);
      }
    }
    option.setOrderId(lastId + 1);
    option.setOldGisFile(this);
    this.options.add(option);
    this.optionThreadPool.addOptionThread(option);
    this.xmlInformation.addOptionFileChanger(option);
  }

  /**
   * set the new output file name and change the name of the xml config.
   */
  @Override
  public void changeOutputFileName(String newOutputFileName) throws IOException {
    this.setOutputFileName(newOutputFileName);
    ((XmlGisFileInformation) this.xmlInformation)
        .renameTo(this, this.getAbsOutputDirectoryPath());
  }

  /**
   * Deletes the xml information an create a new one in the new path.
   */
  @Override
  public void changeOutputFilePath(String outputFilePath) throws IOException {
    String newPath;
    if (outputFilePath.endsWith(File.separator)) {
      newPath = outputFilePath;
    } else {
      newPath = outputFilePath + File.separator;
    }
    ((XmlGisFileInformation) this.xmlInformation).renameTo(this, newPath);
  }

  @Override
  public boolean delete() {
    boolean deleted;
    deleted = gisFile.delete();
    ((XmlGisFileInformation) this.xmlInformation).delete();
    File auxFile = new File(gisFile.getAbsolutePath() + ".aux.xml");
    if (auxFile.exists()) {
      auxFile.delete();
    }
    // TODO TEST BUG1002
    this.notifyObserversDelete();
    return deleted;
  }

  @Override
  public String getAbsolutePath() {
    return getGisFile().getAbsolutePath();
  }

  @Override
  public boolean deleteFile() {
    this.notifyObserversDelete();
    return gisFile.delete();
  }

  @Override
  public boolean deleteLink() {
    if (this.xmlInformation == null) {
      return false;
    }
    boolean deleted2 = ((XmlGisFileInformation) this.xmlInformation).delete();
    this.notifyObserversDelete();

    return deleted2;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && (obj instanceof GisFileContainer)) {
      GisFileContainer tmpFile = (GisFileContainer) obj;
      return super.equals(obj) && this.getAbsOutputDirectoryPath()
          .equals(tmpFile.getAbsOutputDirectoryPath())
          && this.outputFileName.equals(tmpFile.getOutputFileName());
    }
    return super.equals(obj);
  }

  /**
   * This compares the file names of the file reference first.
   * If the file references are equal, then the output file names and paths will be compared.
   *
   * @see Comparable#compareTo(Object)
   */
  @Override
  public int compareTo(GisFileContainer o) {
    if (o == null) {
      throw new NullPointerException("compare object is null");
    }
    int fileRefReturn = gisFile.getFileRef().compareTo(o.gisFile.getFileRef());

    if (fileRefReturn == 0) {
      if (this.getAbsOutputDirectoryPath().equals(o.getAbsOutputDirectoryPath())
          && this.outputFileName.equals(o.getOutputFileName())) {
        return 0;
      } else {
        return ((this.getAbsOutputDirectoryPath() + this.outputFileName)
            .compareTo(o.getAbsOutputDirectoryPath() + o.getOutputFileName()));
      }
    } else {
      return fileRefReturn;
    }
  }

  /**
   * Execute all Unused InformationReader.
   *
   * @param waitForCompletion if true wait for completion for MAX_LONG amount of seconds
   */
  public void executeAllInfos(WorkerStatusList workerStatusList,
                              boolean waitForCompletion) throws IOException {
    this.infoReaderExecPool.executeThreads(this, workerStatusList, waitForCompletion);
  }

  /**
   * Execute all Unused InformationReader.
   */
  @Override
  public void executeAllInfos(WorkerStatusList workerStatusList) throws IOException {
    this.infoReaderExecPool.executeThreads(this, workerStatusList);
  }

  /**
   * Execute all Options in the Option List.
   */
  @Override
  public void executeAllOptions(WorkerStatusList workerStatusList)
      throws IOException {
    this.optionThreadPool.executeThreads(workerStatusList, this);
  }

  @Override
  public void executeInfo(String infoReaderName, WorkerStatusList workerStatusList)
      throws IOException {
    InfoReader infoReader = this.getInfoReader(infoReaderName);
    if (infoReader == null) {
      throw new IOException("File: " + this.getName() + "InfoReader "
          + infoReaderName + " not found");
    }
    if (infoReader.isUsed()) {
      return;
    }
    this.infoReaderExecPool.executeThread(this, infoReader, workerStatusList);
  }

  /**
   * Executes the read method on the first InfoReader Object.
   *
   * @return The executed InfoReader instance.
   */
  @Override
  public void executeInfos(List<String> infoReaderList, WorkerStatusList workerStatusList)
      throws IOException {
    List<InfoReader> runList = new ArrayList<>();
    for (InfoReader infoReader : this.infoReaders) {
      if (infoReaderList.contains(infoReader.getId())) {
        runList.add(infoReader);
      }
    }
    if (!runList.isEmpty()) {
      this.infoReaderExecPool.executeThreads(this, runList, workerStatusList);
    }

  }

  @Override
  public String getAbsOutputDirectoryPath() {
    return this.parent.getFullPath();
  }

  @Override
  public ConnectionFileChanger getConnectionFileChanger() {
    return this.connectionFileChanger;
  }

  @Override
  public void setConnectionFileChanger(ConnectionFileChanger conn) throws IOException {
    this.connectionFileChanger = conn;
    this.xmlInformation.setConnectionFileChanger(conn);
  }

  @Override
  public void addInfoConnection(InfoConnection infoConnection) throws IOException {
    this.infoConnections.add(infoConnection);
    this.xmlInformation.addInfoConnection(infoConnection);
  }

  @Override
  public List<InfoConnection> getInfoConnections() {
    return infoConnections;
  }

  @Override
  public ExecDependencyController getDepChecker() {
    return infoReaderExecPool.getDependencyController();
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  @Override
  public void setDescription(String description) throws IOException {
    this.description = description;
    this.xmlInformation.addDescription(description);
  }

  @Override
  public List<FileObserver> getFileObservers() {
    return this.fileObservers;
  }

  @Override
  public InfoReader getInfoReader(String infoReaderName) {
    for (InfoReader infoReader : this.infoReaders) {
      if (infoReader.getId().equals(infoReaderName)) {
        return infoReader;
      }
    }
    return null;
  }

  /**
   * @return The List of InfoReader Object for this Files.
   */
  @Override
  public List<InfoReader> getInfoReaders() {
    return this.infoReaders;
  }

  @Override
  public List<OptionFileChanger> getOptions() {
    return this.options;
  }

  @Override
  public OptionThreadPool getOptionThreadPool() {
    return this.optionThreadPool;
  }

  @Override
  public String getOutputFileName() {
    return this.outputFileName;
  }

  /**
   * FIXME: Do this in the factory
   */
  @Override
  public void setOutputFileName(String outputFileName) {
    if (getGisFile().getType() == GisFileType.GEO_TIFF) {
      if (outputFileName.lastIndexOf(".") <= 0) {
        this.outputFileName = outputFileName + ".tif";
      } else {
        this.outputFileName = outputFileName.substring(0, outputFileName.lastIndexOf(".")) + ".tif";
      }
    } else if (getGisFile().getType() == GisFileType.SHAPE) {
      if (outputFileName.lastIndexOf(".") <= 0) {
        this.outputFileName = outputFileName + ".shp";
      } else {
        this.outputFileName = outputFileName.substring(0, outputFileName.lastIndexOf(".")) + ".shp";
      }
    } else {
      this.outputFileName = outputFileName;
    }
  }

  @Override
  public ParentModule getParentModule() {
    return this.parent;
  }

  @Override
  public void setParentModule(ParentModule parentModule) {
    this.parent = parentModule;
    if (this.connectionFileChanger != null && (this.parent instanceof GenericGisFileSet)) {
      this.connectionFileChanger.setOutputFileSet((GenericGisFileSet) parentModule);
    }
    for (InfoReader iterInfoReader : infoReaders) {
      if (iterInfoReader instanceof InfoConnection) {
        ((ConnectionFileChanger) iterInfoReader).setOutputFileSet((GenericGisFileSet) parentModule);
      }
    }
  }

  @Override
  public String getPathRelativeToProject() {
    if (this.parent != null) {
      return this.parent.getPathRelativeToProject();
    }
    return null;
  }

  @Override
  public IGisFileContainer getTargetGisFileContainer() {
    return this.targetGisFileContainer;
  }

  @Override
  public void setTargetGisFileContainer(IGisFileContainer targetGisFileContainer) {
    this.targetGisFileContainer = targetGisFileContainer;
  }


  // public boolean renameTo(RasterFileContainer dest) throws IOException
  // {
  // boolean rename = super.renameTo(dest);
  // //boolean rename2 = xmlInformation.delete();
  // xmlInformation.renameTo(dest,dest.getAbsOutputDirectoryPath());
  //
  // return rename;
  // }

  @Override
  public int getUnusedInfoReaderCount() {
    int count = 0;
    if (this.infoReaders == null) {
      return 0;
    }
    for (InfoReader infoReader : this.infoReaders) {
      if (!infoReader.isUsed()) {
        count++;
      }
    }
    return count;
  }

  @Override
  public int getUnusedOptionCount() {
    int tmpCnt = 0;
    for (OptionFileChanger optionFileChanger : this.options) {
      if (!optionFileChanger.isUsed()) {
        tmpCnt++;
      }
    }
    return tmpCnt;
  }

  @Override
  public IGisFileInformationStorage getGisFileInformationStorage() {
    return this.xmlInformation;
  }

  @Override
  public void setGisFileInformationStorage(IGisFileInformationStorage xmlInformation) {
    this.xmlInformation = xmlInformation;
  }

  @Override
  public String getName() {
    return gisFile.getName();
  }

  @Override
  public int hashCode() {
    if ((this.getAbsOutputDirectoryPath() + this.outputFileName)
        .equals(gisFile.getAbsolutePath())) {
      return super.hashCode();
    } else {
      return new File((this.getAbsOutputDirectoryPath() + this.outputFileName)).hashCode();
    }
  }

  /**
   * //todo: be careful.. rename a fileset causes this to be wrong
   */
  @Override
  public boolean isHardCopy() {
    // if this is true, it has to be a hard copy
    return gisFile.getFileRef().getAbsolutePath()
        .equals(this.getAbsOutputDirectoryPath() + this.getOutputFileName())
        && gisFile.exists();
    // if not..

  }

  @Override
  public synchronized boolean isLocked() {
    return this.lockedCnt > 0;

  }

  @Override
  public void notifyObserversDelete() {
    for (FileObserver fileObserver : this.fileObservers) {
      fileObserver.notifyDelete(this);
    }
  }

  /**
   * loads the Information from the XML File.
   */
  private void restoreAllStoredPluginExecInstancesIfAvailable() {
    InfoManager infoManager = new InfoManager();
    OptionManager optionManager = new OptionManager();
    List<InfoReader> newInfosList = new ArrayList<>();
    this.description = this.xmlInformation.getDescription();

    xmlInformation.getRestorer().loadCollection("infos", (s, restorer) -> {
      InfoReader newInfoReader = infoManager.getInfoReader(s);
      if (newInfoReader instanceof InfoConnection) {
        ((ConnectionFileChanger) newInfoReader).setOutputFileContainer(this);
        this.parent.getProject().addConnection((ConnectionFileChanger) newInfoReader);
      }
      newInfoReader.restore(restorer);
      newInfosList.add(newInfoReader);
    });
    for (InfoReader infoReader : newInfosList) {
      this.addInfoReader(infoReader, true);
    }

    xmlInformation.getRestorer().loadCollection("options", (s, restorer) -> {
      Optional<OptionFileChanger> newOpionalOption = optionManager.getOption(s);
      if (newOpionalOption.isPresent()) {
        OptionFileChanger newOption = newOpionalOption.get();
        newOption.restore(restorer);
        newOption.setOldGisFile(this);
        this.options.add(newOption);
        this.optionThreadPool.addOptionThread(newOption);
      }
    });

    xmlInformation.getRestorer().loadCollection("infoConnections", (s, restorer) -> {
      InfoConnection newInfoConnection = new InfoConnectionManager().getInfoConnection(s);
      newInfoConnection.setOutputFileSet((IGisFileSet) parent);
      newInfoConnection.setParentFileContainer(this);
      newInfoConnection.restore(restorer);
      this.infoConnections.add(newInfoConnection);
    });

    xmlInformation.getRestorer().loadCollection("connections", (s, restorer) -> {
      ConnectionFileChanger newConnection = new ConnectionManager().getConnectionFileChanger(s);
      if (newConnection == null) {
        log.error("Could not create connection with id \"{}\"", s);
        return;
      }
      newConnection.setOutputFileContainer(this);
      newConnection.restore(restorer);
      this.connectionFileChanger = newConnection;
      this.parent.getProject().addConnection(this.connectionFileChanger);
    });
  }


  /**
   * relocates the ref file if the path changed and the name did not!.
   */
  @Override
  public void relocateFile() {
    String parentPath = this.parent.getFullPath();
    String fullPath = parentPath + this.getName();
    gisFile.relocateFile(fullPath);
  }

  /**
   * TODO BUG 1002.
   */
  @Override
  public void removeFileObserver(FileObserver fileObserver) {
    this.fileObservers.remove(fileObserver);
  }

  @Override
  public void removeInfoReader(InfoReader infoReader) throws IOException {
    this.xmlInformation.removePluginExec(infoReader);
    this.infoReaderExecPool.removeInfoReader(infoReader);
    if (!this.infoReaders.remove(infoReader)) {
      throw new IOException("RasterFileContainer: removeInfoReader: InfoReader "
          + infoReader.getId() + " not found");
    }
  }

  @Override
  public void removeInfoConnection(InfoConnection infoConnection) throws IOException {
    this.xmlInformation.removePluginExec(infoConnection);
    if (!this.infoConnections.remove(infoConnection)) {
      throw new IOException("RasterFileContainer: removeInfoConnection: InfoConnection "
          + infoConnection.getId() + " not found");
    }
    for (IGisFileContainer iterFile : infoConnection.getSourceFileList()) {
      iterFile.removeFileObserver(infoConnection);
    }
  }

  @Override
  public void removeOptionFileChanger(OptionFileChanger option) throws IOException {
    if (option.isUsed()) {
      return;
    }
    this.xmlInformation.removePluginExec(option);
    this.optionThreadPool.removeOptionFileChanger(option);
    if (!this.options.remove(option)) {
      throw new IOException("RasterFileContainer: removeOptionFileChanger: OptionFileChanger "
          + option.getId() + " not found");
    }
  }

  @Override
  public boolean renameTo(File dest) throws IOException {
    // hard copy
    if (gisFile.getFileRef().getAbsolutePath()
        .equals(this.getAbsOutputDirectoryPath() + this.getName())) {
      return gisFile.renameTo(dest);
    } else {
      throw new IOException("Error: you tried to rename the original file");
    }
  }

  @Override
  public boolean isConnection() {
    return (getConnectionFileChanger() != null);
  }

  @Override
  public boolean isStack() {
    return isConnection() && (getConnectionFileChanger() instanceof StackConnection);
  }

  @Override
  public synchronized void setLock(boolean locked) {
    if (locked) {
      this.lockedCnt++;
    } else {
      if (this.lockedCnt > 0) {
        this.lockedCnt--;
      }
    }
  }

  @Override
  public String toString() {
    return this.getOutputFileName();
  }

  @Override
  public void update(IGisFileContainer file) {

  }

  public void unlock() {
    getGisFile().unlock();
  }

  public void refresh() throws IOException, GisFileException {
    getGisFile().refresh();
  }
}

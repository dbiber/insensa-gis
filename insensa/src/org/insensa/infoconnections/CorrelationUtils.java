/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.infoconnections;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.TDistribution;
import org.apache.commons.math.distribution.TDistributionImpl;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CorrelationUtils {

  public static Float createTTestValue(Float cor, long degreeOfFreedom) throws IOException {
    // degrees of freedom
//    long df = dataValuesCnt - 2;
    double t1 = cor.doubleValue() * Math.sqrt((double) degreeOfFreedom);
    double t2 = Math.sqrt(1.0 - cor.doubleValue() * cor.doubleValue());
    double t = t1 / t2;
    TDistribution distr = new TDistributionImpl((double) degreeOfFreedom);
    try
    {
      double p;
      if (cor > 0)
        p = (1.0 - distr.cumulativeProbability(t));
      else
        p = distr.cumulativeProbability(t);

      return (float) p;
    } catch (MathException e)
    {
      throw new IOException(e.getCause());
    }
  }

  public static Float findCorrelationValue(List<CorrelationData> coVarList,
                                           int index1,
                                           int index2) throws IOException {
    for (CorrelationData data : coVarList) {
      int i1 = data.file1;
      int i2 = data.file2;
      if ((i1 == index1 && i2 == index2) || (i2 == index1 && i1 == index2)) {
        return data.correlation;
      }
    }
    throw new IOException("Could not found correlation value for index2:\"" + index1 + "\"" +
        " and index2:\"" + index2 + "\"");
  }

  public static List<CorrelationData> writePartialCorrelation2(
      List<IGisFileContainer> rasterFileList,
      List<CorrelationData> coVarList,
      List<CorrelationData> partialList) throws IOException {
    for (int indexConst = 0; indexConst < rasterFileList.size(); indexConst++) {

      // the constant file (adjusted for)
      for (int j = 0; j < coVarList.size(); j++) {
        CorrelationData data = coVarList.get(j);
        int index1, index2;
        index1 = data.file1;
        index2 = data.file2;
        if (index1 == indexConst || index2 == indexConst)
          continue;

        CorrelationData tmpPartialData = new CorrelationData();
        Float correlation12 = data.correlation;
        Float correlation123;
        if (index1 == index2) {
          correlation123 = correlation12;
        } else {
          Float correlation13 = findCorrelationValue(coVarList, index1, indexConst);
          Float correlation23 = findCorrelationValue(coVarList, index2, indexConst);
          Float cor13sq = correlation13 * correlation13;
          Float cor23sq = correlation23 * correlation23;
          correlation123 = (correlation12 - (correlation13 * correlation23));
          Float correlation123_2 = (float) Math.sqrt(1.0f - cor13sq) * (float) Math.sqrt(1.0f - cor23sq);
          correlation123 = correlation123 / correlation123_2;
        }
        tmpPartialData.constantIndex = indexConst;
        tmpPartialData.file1 = index1;
        tmpPartialData.file2 = index2;
        tmpPartialData.correlation = correlation123;
        partialList.add(tmpPartialData);
      }

    }
    return partialList;
  }

}

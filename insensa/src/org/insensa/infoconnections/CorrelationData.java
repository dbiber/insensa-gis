package org.insensa.infoconnections;


import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.IStorageData;

public class CorrelationData implements IStorageData {
  public int file1;
  public int file2;
  public Integer constantIndex;
  public Float covariance;
  public Float correlation;
  public Float stdDevX;
  public Float stdDevY;
  public Float meanX;
  public Float meanY;
  public Float pValue;
  public boolean tTest;

  @Override
  public void restore(ISavableRestorer restorer) {
    file1 = restorer.loadInteger("file1").get();
    file2 = restorer.loadInteger("file2").get();
    constantIndex = restorer.loadInteger("constant").orElse(null);
    correlation = restorer.loadFloat("correlation").get();
    covariance = restorer.loadFloat("covariance").orElse(null);
    stdDevX = restorer.loadFloat("stdDevX").orElse(null);
    stdDevY = restorer.loadFloat("stdDevY").orElse(null);
    meanX = restorer.loadFloat("meanX").orElse(null);
    meanY = restorer.loadFloat("meanY").orElse(null);
    pValue = restorer.loadFloat("p_value").orElse(null);
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("file1", file1);
    saver.save("file2", file2);
    saver.save("correlation", correlation);

    if(constantIndex != null) {
      saver.save("constant",constantIndex);
    }
    if (covariance != null) {
      saver.save("covariance", covariance);
    }
    if (stdDevX != null) {
      saver.save("stdDevX", stdDevX);
    }
    if (stdDevY != null) {
      saver.save("stdDevY", stdDevY);
    }
    if (meanX != null) {
      saver.save("meanX", meanX);
    }
    if (meanY != null) {
      saver.save("meanY", meanY);
    }
    if (pValue != null) {
      saver.save("p_value", pValue);
    }
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.infoconnections;

import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class SpearmanRankCorrelation extends CorrelationBase {

  private int countCalc;
  private int index = 0;

  private double getGaussSum(long count) { //TODO: FIXME: CHeck Correctness
    return ((((double) count + 1.0F) * (double) count) / 2.0F);
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> deps = new ArrayList<String>();
    deps.add(CountValues.NAME);
    return deps;
  }

  private Map<Float, Double> getRankMap(CountValues countValues, int index, double[] stdDevValuesX, double[] meanValuesX) throws IOException {
    Map<Float, Double> rankMap = new TreeMap<>();
    Map<Float, Long> countMap = countValues.getCountValues().getMap();

    long steps = 0;
    double gSum1 = 0;
    double gSum2 = 0;
    double gSum = 0;
    double sqr = 0.0;
    long numOfData = countValues.getNumOfData();
    meanValuesX[index] = (numOfData + 1.0) / 2.0;
    stdDevValuesX[index] = 0.0;
    for (Map.Entry<Float, Long> entry: countMap.entrySet()) {
      Float key = entry.getKey();
      Long counts = entry.getValue();
      gSum1 = this.getGaussSum(steps);
      gSum2 = this.getGaussSum(steps + counts);
      gSum = (gSum2 - gSum1) / counts;
      sqr = (gSum - meanValuesX[index]) * (gSum - meanValuesX[index]);
      stdDevValuesX[index] += (sqr * counts) / numOfData;
      Double gSumDouble = gSum;
      rankMap.put(key, gSumDouble);
      steps += counts;
    }
    stdDevValuesX[index] = Math.sqrt(stdDevValuesX[index]);
    return rankMap;
  }


  public void solveDependencies(List<IGisFileContainer> files, RasterFileContainer templateFile) throws IOException {
    ExecDependencyController iDepChecker;
    InfoManager iManager = new InfoManager();
    if (this.workerStatus != null)
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") " + "Resolve Dependencies");

    if (this.workerStatus != null) {
      this.workerStatus.removeChildrenWorkerStatusLists();
      this.workerStatus.setChildrenWorkerStatusLists(files.size());
    }

    int i = -1;
    for (IGisFileContainer iFile: files) {
      i++;
      WorkerStatusList workerStatusList = this.workerStatus.getChildWorkerStatusList(i);
      InfoReader iReader;
      iReader = iFile.getInfoReader(CountValues.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iDepChecker = iFile.getDepChecker();

      InfoReader countValues = iManager.getInfoReader(CountValues.NAME);
      if (!iFile.equals(templateFile))
        ((CountValues) countValues).setTemplateFile(templateFile);
      iFile.addInfoReader(countValues, true);
      if (workerStatusList != null)
        iFile.executeAllInfos(workerStatusList);
      else
        iFile.executeAllInfos(null);
      iDepChecker.checkInfoDependencies(this);
    }

  }

  @Override
  public void readInfos() throws IOException, GisFileException {
    this.index = 0;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.toString());
    }

//    if (!this.check()) {
//      throw new IOException("The files are incompatible");
//    }

//    if (this.getParentFileContainer() == null)
//      throw new IOException("No Output File Selected");

    List<List<InfoReader>> iFileReaderList = new ArrayList<>();
    InfoManager iManager = new InfoManager();
    for (IGisFileContainer iFile: sourceFileList) {
      List<InfoReader> iReaderList = new ArrayList<>();
      for (InfoReader iReader: iFile.getInfoReaders()) {
        if (!iReader.getId().equals(CountValues.NAME))
          iReaderList.add(iManager.getInfoReaderCopy(iReader));
      }
      iFileReaderList.add(iReaderList);
    }

    if (this.option == NODATA_SKIP) {
      this.countCalc = (((sourceFileList.size() * (sourceFileList.size() + 1)) / 2) * 2) + (sourceFileList.size());
      for (int i = 0; i < sourceFileList.size(); i++) {
        this.index++;
        List<IGisFileContainer> newList = new ArrayList<>();
        newList.addAll(sourceFileList.subList(i, sourceFileList.size()));
        this.writeFitFile(((RasterFileContainer) sourceFileList.get(i)), newList);
      }
    }
    if (this.partial) {
      CorrelationUtils.writePartialCorrelation2(sourceFileList,
          this.correlationData,
          this.partCorList);
    }
    for (int i = 0; i < sourceFileList.size(); i++) {
      sourceFileList.get(i).addInfoReaders(iFileReaderList.get(i), true);
    }
    this.used = true;
  }

  private void writeFitFile(RasterFileContainer fileX, List<IGisFileContainer> files) throws IOException {
    double[] meanValuesX = new double[files.size()];
    double[] stdDevValuesX = new double[files.size()];
    long[] numOfDataValuesX = new long[files.size()];
    List<TreeMap<Float, Double>> rankMapListX = new ArrayList<>();
    CountValues tmpCountValues;
    for (int i = 0; i < files.size(); i++) {
      List<IGisFileContainer> ilist = new ArrayList<>();
      ilist.add(fileX);
      this.solveDependencies(ilist, (RasterFileContainer) files.get(i));
      tmpCountValues = ((CountValues) fileX.getInfoReader(CountValues.NAME));
      numOfDataValuesX[i] = tmpCountValues.getNumOfData();
      rankMapListX.add((TreeMap<Float, Double>) this.getRankMap(tmpCountValues, i, stdDevValuesX, meanValuesX));
      this.index++;
    }

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) sourceFileList.get(0)).getNRows());

    int xSize = fileX.getBand(RasterFileAccess.READ_ONLY).getXSize();
    float[][] doubleDoubleArray;
    float[] doubleArrayXFile;
    float fileXNoData = fileX.getNoDataValue();
    double[] meanValues = new double[files.size()];
    long[] numOfDataValues = new long[files.size()];
    double[] stddevValues = new double[files.size()];
    List<TreeMap<Float, Double>> rankMapList = new ArrayList<>();
    // long prec = new Double(Math.pow(10, precition)).longValue();
    long prec = (long) Math.pow(10, 3);
    double[] sumDSqr = new double[files.size()];
    double[] cov = new double[files.size()];
    double[] cor = new double[files.size()];

    double newReadValue;
    double newReadValueX;
    Double tmpRank1;
    Double tmpRank2;

    for (int i = 0; i < sumDSqr.length; i++) {
      sumDSqr[i] = 0.0;
      cov[i] = 0.0;
      cor[i] = 0.0;
    }
    this.solveDependencies(files, fileX);
    for (int i = 0; i < files.size(); i++) {
      tmpCountValues = ((CountValues) files.get(i).getInfoReader(CountValues.NAME));
      numOfDataValues[i] = tmpCountValues.getNumOfData();
      rankMapList.add((TreeMap<Float, Double>) this.getRankMap(tmpCountValues, i, stddevValues, meanValues));
      this.index++;
    }
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") " + this.toString());
    }
    InfoReader iReader;

    iReader = fileX.getInfoReader(CountValues.NAME);
    if (iReader != null)
      fileX.removeInfoReader(iReader);

    for (IGisFileContainer iFile: files) {

      iReader = iFile.getInfoReader(CountValues.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);
    }
    doubleDoubleArray = new float[files.size()][xSize];
    doubleArrayXFile = new float[xSize];

    int rowCount;

    // Schleife 1 Anzahl der Zeilen
    rowCount = ((RasterFileContainer) sourceFileList.get(0)).getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      fileX.getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleArrayXFile);
      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < files.size(); j++) {
        ((RasterFileContainer) files.get(j)).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        float tmpValueX1 = doubleArrayXFile[j];
        float tmpValueX2;

        //TODO Dieser Wert wird nur einmal genutzt und darf deshalt NUR EINMAL gerundet werden
        newReadValueX = tmpValueX1 * prec;
        newReadValueX = Math.round(newReadValueX);
        newReadValueX /= prec;
        tmpValueX2 = (float) newReadValueX;

        // schleife 4 Anzahl der Dateien -1
        for (int k = 0; k < files.size(); k++) {
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == ((RasterFileContainer) files.get(k)).getNoDataValue() || tmpValueX1 == fileXNoData) {

          } else {
            newReadValue = tmpValue *= prec;
            newReadValue = Math.round(newReadValue);
            newReadValue /= prec;
            tmpValue = (float) newReadValue;

            tmpRank1 = rankMapListX.get(k).get(tmpValueX2);
            tmpRank2 = rankMapList.get(k).get(tmpValue);
            if (tmpRank1 == null) {
              throw new IOException("No rank found in map: Var=tmpRank1 , x=" + j + ", y=" + i + " , tmpValueX=" + tmpValueX2 + " , newReadValueX=" + newReadValueX);
            }
            if (tmpRank2 == null) {
              throw new IOException("No rank found in map: Var=tmpRank2 , x=" + j + ", y=" + i + " , tmpValue=" + tmpValue + " , newReadValue=" + newReadValue);
            }
            cov[k] += ((tmpRank1 - meanValuesX[k]) * (tmpRank2 - meanValues[k])) / (numOfDataValuesX[k]);
          }
        }// s4-ENDE
      }
    }// Schleife1-Ende

    this.correlationData = new ArrayList<>();
    for (int i = 0; i < sumDSqr.length; i++) {
      CorrelationData data = new CorrelationData();
      data.file1 = sourceFileList.indexOf(fileX);
      data.file2 = sourceFileList.indexOf(files.get(i));
      data.covariance = (float) cov[i];
      cor[i] = cov[i] / (stddevValues[i] * stdDevValuesX[i]);
      data.correlation = (float) cor[i];

      // TODO t-test
      if (this.t_test) {
        data.pValue = CorrelationUtils.createTTestValue(data.correlation,
            numOfDataValuesX[i] - 3);
      }
      this.correlationData.add(data);
    }

    for (IGisFileContainer file: files) {
      file.getGisFile().unlock();
    }
    fileX.getGisFile().unlock();
  }

  @Override
  public void notifyDelete(IGisFileContainer subject) {

  }
}

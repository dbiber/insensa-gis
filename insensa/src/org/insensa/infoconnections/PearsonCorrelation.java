/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.infoconnections;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.helpers.MathUils;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.MeanValue;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.inforeader.StandardDeviationValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class PearsonCorrelation extends CorrelationBase {

  private int countCalc;
  private int index = 0;

  protected void solveDependencies(List<IGisFileContainer> files,
                                   IGisFileContainer templateFile)
      throws IOException {
    ExecDependencyController iDepChecker;
    InfoManager iManager = new InfoManager();
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") "
          + "Resolve Dependencies");
    }

    if (this.workerStatus != null) {
      this.workerStatus.removeChildrenWorkerStatusLists();
      this.workerStatus.setChildrenWorkerStatusLists(files.size());
    }

    int i = -1;
    for (IGisFileContainer iFile: files) {
      i++;
      WorkerStatusList workerStatusList = null;
      if (workerStatus != null) {
        workerStatusList = this.workerStatus.getChildWorkerStatusList(i);
      }

      InfoReader iReader;
      int tmpMaxPrecValue = -1;
      iReader = iFile.getInfoReader(StandardDeviationValue.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(MeanValue.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(CountValues.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(PrecisionValues.NAME);
      if (iReader != null) {
        tmpMaxPrecValue = ((PrecisionValues) iReader).getMaxPrecision();
        iFile.removeInfoReader(iReader);
      }

      iDepChecker = iFile.getDepChecker();

      PrecisionValues precValues = (PrecisionValues) iManager.getInfoReader(PrecisionValues.NAME);
      if (tmpMaxPrecValue > -1)
        precValues.setMaxPrecision(tmpMaxPrecValue);
      iFile.addInfoReader(precValues, true);
      InfoReader countValues = iManager.getInfoReader(CountValues.NAME);
      if (!iFile.equals(templateFile))
        ((CountValues) countValues).setTemplateFile((RasterFileContainer) templateFile);
      iFile.addInfoReader(countValues, true);
      iFile.addInfoReader(iManager.getInfoReader(MeanValue.NAME), true);
      iFile.addInfoReader(iManager.getInfoReader(StandardDeviationValue.NAME), true);

      if (workerStatusList != null)
        iFile.executeAllInfos(workerStatusList);
      else
        iFile.executeAllInfos(null);
      iDepChecker.checkInfoDependencies(this);
    }
  }

  @Override
  public void readInfos() throws IOException {
    this.index = 0;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.toString());
    }

    if (!this.check()) {
      throw new IOException("The files are incompatible");
    }

    if (this.getParentFileContainer() == null)
      throw new IOException("No Output File Selected");

    List<List<InfoReader>> iFileReaderList = new ArrayList<>();
    InfoManager iManager = new InfoManager();
    for (IGisFileContainer iFile: sourceFileList) {
      List<InfoReader> iReaderList = new ArrayList<>();
      for (InfoReader iReader: iFile.getInfoReaders()) {
        if (!iReader.getId().equals(CountValues.NAME))
          iReaderList.add(iManager.getInfoReaderCopy(iReader));
        else//TODO: CountValues will not be "used" at the end
          iReaderList.add(iManager.getInfoReader(CountValues.NAME));
      }
      iFileReaderList.add(iReaderList);
    }

    if (this.option == NODATA_SKIP) {
      this.countCalc = (((sourceFileList.size() * (sourceFileList.size() + 1)) / 2) * 2) + (sourceFileList.size());
      for (int i = 0; i < sourceFileList.size(); i++) {
        this.index++;
        List<IGisFileContainer> newList = new ArrayList<>();
        newList.addAll(sourceFileList.subList(i, sourceFileList.size()));
        this.writeFitFile(sourceFileList.get(i), newList);
      }
    } else if (this.option == MEAN_INITIATION || this.option == ZERO_INITIATION) {
      this.countCalc = ((sourceFileList.size()));
      this.solveDependencies();
      for (int i = 0; i < sourceFileList.size(); i++) {
        this.index++;
        List<IGisFileContainer> newList = new ArrayList<>();
        newList.addAll(sourceFileList.subList(i, sourceFileList.size()));
        this.writeFile(sourceFileList.get(i), newList);
      }
    }
    if (this.partial) {
      CorrelationUtils.writePartialCorrelation2(sourceFileList,
          this.correlationData,
          this.partCorList);
    }
    for (int i = 0; i < sourceFileList.size(); i++) {//TODO: CountValues will not be "used" at the end
      sourceFileList.get(i).addInfoReaders(iFileReaderList.get(i), true);
    }

    this.used = true;
  }

  public List<CorrelationData> getCorrelationData() {
    return correlationData;
  }

  public List<CorrelationData> getPartCorList() {
    return partCorList;
  }

  private void writeFile(IGisFileContainer fileX, List<IGisFileContainer> files)
      throws IOException {

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) sourceFileList.get(0)).getNRows());
    if (this.workerStatus != null)
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") " + this.toString());

    int xSize = ((RasterFileContainer) fileX).getXSize();
    int ySize = ((RasterFileContainer) fileX).getYSize();
    int dataValues = xSize * ySize;// ((CountValues)fileX.getInfoReader(CountValues.NAME)).getNumOfData();//0;//xSize*ySize;
    float[][] doubleDoubleArray;
    List<IGisFileContainer> newList = new ArrayList<>();

    newList.add(fileX);
    newList.addAll(files);
    float fileXNoData = ((RasterFileContainer) fileX).getNoDataValue();
    double[] meanValues = new double[newList.size()];
    double[] stddevValues = new double[newList.size()];
    int[] precValues = new int[newList.size()];
    double[] coVar = new double[newList.size() - 1];
    long[] dataValuesCnts = new long[newList.size() - 1];
    for (int i = 0; i < coVar.length; i++) {
      coVar[i] = 0.0F;
      dataValuesCnts[i] = 0;
    }
    for (int i = 0; i < newList.size(); i++) {
      precValues[i] = (int) Math.pow(10, ((PrecisionValues) newList.get(i).getInfoReader(PrecisionValues.NAME)).getMaxPrecision());
      meanValues[i] = ((MeanValue) newList.get(i).getInfoReader(MeanValue.NAME)).getMeanValue();
      stddevValues[i] = ((StandardDeviationValue) newList.get(i).getInfoReader(StandardDeviationValue.NAME)).getStandardDeviationValue();
    }
    doubleDoubleArray = new float[newList.size()][xSize];

    int rowCount;

    // Schleife 1 Anzahl der Zeilen
    // Calculating the Covariances
    rowCount = ((RasterFileContainer) sourceFileList.get(0)).getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }
      // schleife 2 Anzahl der Dateien
      // Loading Data into doubleDoubleArray...
      for (int j = 0; j < newList.size(); j++) {
        ((RasterFileContainer) newList.get(j)).readRaster(0, i, xSize, 1, doubleDoubleArray[j]);
//				newList.get(j).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        float tmpValue = doubleDoubleArray[0][j];
        for (int k = 1; k < newList.size(); k++) {
          float tmpValue2 = doubleDoubleArray[k][j];
          if (tmpValue2 == ((RasterFileContainer) newList.get(k)).getNoDataValue() && tmpValue == fileXNoData) {

          } else {
            if (tmpValue2 == ((RasterFileContainer) newList.get(k)).getNoDataValue()) {
              if (option == MEAN_INITIATION) {
                tmpValue2 = new Double(meanValues[k]).floatValue();
              } else if (this.option == ZERO_INITIATION) {
                tmpValue2 = 0;
              }
            } else if (tmpValue == fileXNoData) {
              if (this.option == MEAN_INITIATION) {
                tmpValue = new Double(meanValues[0]).floatValue();
              } else if (this.option == ZERO_INITIATION) {
                tmpValue = 0;
              }
            }

            dataValuesCnts[k - 1]++;
            tmpValue = MathUils.roundWithPrecition(tmpValue, precValues[0]);
            tmpValue2 = MathUils.roundWithPrecition(tmpValue2, precValues[k]);
            coVar[k - 1] += (((tmpValue - meanValues[0]) * (tmpValue2 - meanValues[k])) / dataValues);
          }

        }// s4-ENDE
      }
    }// Schleife1-Ende
    this.correlationData = new ArrayList<>();
    for (int i = 0; i < coVar.length; i++) {
      CorrelationData data = new CorrelationData();
      data.file1 = sourceFileList.indexOf(fileX);
      data.file2 = sourceFileList.indexOf(files.get(i));
      coVar[i] *= dataValues;
      coVar[i] /= dataValuesCnts[i];
      data.covariance = (float) coVar[i];
      data.correlation = (float) (coVar[i] / (stddevValues[0] * stddevValues[i + 1]));

      // TODO Test fuer regressioin
      data.stdDevX = (float) stddevValues[0];
      data.stdDevY = (float) stddevValues[i + 1];
      data.meanX = (float) meanValues[0];
      data.meanY = (float) meanValues[i + 1];
      // TODO t-test
      if (this.t_test) {
        data.pValue = CorrelationUtils.createTTestValue(data.correlation, dataValuesCnts[i] - 2);
      }
      this.correlationData.add(data);
    }
    for (int j = 0; j < files.size(); j++) {
      ((RasterFileContainer) files.get(j)).unlock();
    }
    ((RasterFileContainer) fileX).unlock();
  }

  private void writeFitFile(IGisFileContainer fileX, List<IGisFileContainer> files)
      throws IOException {
    double[] meanValuesX = new double[files.size()];
    double[] stdDevValuesX = new double[files.size()];
    long[] countValuesX = new long[files.size()];
    int[] precisionValuesX = new int[files.size()];

    for (int i = 0; i < files.size(); i++) {
      List<IGisFileContainer> ilist = new ArrayList<>();
      ilist.add(fileX);
      this.solveDependencies(ilist, files.get(i));
      precisionValuesX[i] = (int) Math.pow(10,
          ((PrecisionValues) fileX.getInfoReader(PrecisionValues.NAME)).getMaxPrecision());
      countValuesX[i] = ((CountValues) fileX.getInfoReader(CountValues.NAME)).getNumOfData();
      meanValuesX[i] = ((MeanValue) fileX.getInfoReader(MeanValue.NAME)).getMeanValue();
      stdDevValuesX[i] = ((StandardDeviationValue) fileX.getInfoReader(StandardDeviationValue.NAME))
          .getStandardDeviationValue();
      this.index++;
    }

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) sourceFileList.get(0)).getNRows());

    int xSize = ((RasterFileContainer) fileX).getXSize();
    int ySize = ((RasterFileContainer) fileX).getYSize();
    float[][] doubleDoubleArray;
    float[] doubleArrayXFile;
    float fileXNoData = ((RasterFileContainer) fileX).getNoDataValue();
    double[] meanValues = new double[files.size()];
    int[] precValues = new int[files.size()];
    long[] countValues = new long[files.size()];
    double[] stddevValues = new double[files.size()];
    double[] coVar = new double[files.size()];
    long[] dataValuesCnts = new long[files.size()];
    for (int i = 0; i < coVar.length; i++) {
      coVar[i] = 0.0F;
      dataValuesCnts[i] = 0;
    }
    this.solveDependencies(files, fileX);
    for (int i = 0; i < files.size(); i++) {
      precValues[i] = (int) Math.pow(10, ((PrecisionValues) files.get(i).getInfoReader(PrecisionValues.NAME)).getMaxPrecision());
      countValues[i] = ((CountValues) files.get(i).getInfoReader(CountValues.NAME)).getNumOfData();
      meanValues[i] = ((MeanValue) files.get(i).getInfoReader(MeanValue.NAME)).getMeanValue();
      stddevValues[i] = ((StandardDeviationValue) files.get(i).getInfoReader(StandardDeviationValue.NAME)).getStandardDeviationValue();
      this.index++;
    }
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") " + this.toString());
    }
    InfoReader iReader;
    iReader = fileX.getInfoReader(StandardDeviationValue.NAME);
    if (iReader != null) {
      fileX.removeInfoReader(iReader);
    }

    iReader = fileX.getInfoReader(MeanValue.NAME);
    if (iReader != null) {
      fileX.removeInfoReader(iReader);
    }

    iReader = fileX.getInfoReader(CountValues.NAME);
    if (iReader != null) {
      fileX.removeInfoReader(iReader);
    }

    for (IGisFileContainer iFile: files) {
      iReader = iFile.getInfoReader(StandardDeviationValue.NAME);
      if (iReader != null) {
        iFile.removeInfoReader(iReader);
      }
      iReader = iFile.getInfoReader(MeanValue.NAME);
      if (iReader != null) {
        iFile.removeInfoReader(iReader);
      }
      iReader = iFile.getInfoReader(CountValues.NAME);
      if (iReader != null) {
        iFile.removeInfoReader(iReader);
      }
    }
    doubleDoubleArray = new float[files.size()][xSize];
    doubleArrayXFile = new float[xSize];

    int rowCount;

    // Schleife 1 Anzahl der Zeilen
    rowCount = ((RasterFileContainer) sourceFileList.get(0)).getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }
      // schleife 2 Anzahl der Dateien
//			fileX.getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleArrayXFile);
      ((RasterFileContainer) fileX).readRaster(0, i, xSize, 1, doubleArrayXFile);
      for (int j = 0; j < files.size(); j++) {
//				files.get(j).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
        ((RasterFileContainer) files.get(j)).readRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        float tmpValueX = doubleArrayXFile[j];
        // schleife 4 Anzahl der Dateien -1
        for (int k = 0; k < files.size(); k++) {
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == ((RasterFileContainer) files.get(k)).getNoDataValue() || tmpValueX == fileXNoData) {

          } else {
            dataValuesCnts[k]++;
            // TODO Test Runden
            tmpValue = MathUils.roundWithPrecition(tmpValue, precValues[k]);
            tmpValueX = MathUils.roundWithPrecition(tmpValueX, precisionValuesX[k]);

            coVar[k] += ((tmpValueX - meanValuesX[k]) * (tmpValue - meanValues[k])) / (countValues[k]);
          }

        }// s4-ENDE
      }
    }// Schleife1-Ende

    this.correlationData = new ArrayList<>();
    for (int i = 0; i < coVar.length; i++) {
      CorrelationData data = new CorrelationData();
      data.file1 = sourceFileList.indexOf(fileX);
      data.file2 = sourceFileList.indexOf(files.get(i));
      data.covariance = (float) coVar[i];
      data.correlation = (float) (coVar[i] / (stdDevValuesX[i] * stddevValues[i]));
      data.stdDevX = (float) stdDevValuesX[i];
      data.stdDevY = (float) stddevValues[i];
      data.meanX = (float) meanValuesX[i];
      data.meanY = (float) meanValues[i];
      // TODO t-test
      if (this.t_test) {
        data.pValue = CorrelationUtils.createTTestValue(data.correlation,
            dataValuesCnts[i] - 2);
      }
      this.correlationData.add(data);
    }

    for (IGisFileContainer file: files) {
      file.getGisFile().unlock();
    }
    fileX.getGisFile().unlock();
  }


  @Override
  public void notifyDelete(IGisFileContainer subject) {

  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.infoconnections;

import org.insensa.Environment;
import org.insensa.GisFileType;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceDependency;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.model.storage.IStorageCloner;
import org.insensa.model.storage.JdomStorageCloner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class InfoConnectionManager {
  private static final Logger log = LoggerFactory.getLogger(InfoConnectionManager.class);

  ExtensionManager exManager;
  private IStorageCloner cloner = new JdomStorageCloner();

  public InfoConnectionManager() {
    this.exManager = ExtensionManager.getInstance();
  }

  public InfoConnection getInfoConnection(String infoConnectionName) {
    ClassLoader cl = this.exManager.getUrlClassLoader();
    ServiceList list = this.exManager.getInfoConnectionServices().get(infoConnectionName);
//    ServiceList list = this.exManager.getInfoConnectionServices()
//        .get(StringUtils.uncapitalize(infoConnectionName));
    if (list == null) {
      return null;
    }
    Service service = list.getService(ServiceType.EXEC);
    if (service == null) {
      return null;
    } else {
      try {
        InfoConnection infoConnection = null;
        if (service.getScript() != null) {
          infoConnection =
              new RRasterInfoConnection(infoConnectionName);
          Path absFilePath = service.getScript().extractFromFile();
          ((RRasterInfoConnection) infoConnection).setScriptPath(absFilePath.toString());
        } else {
          Class classConnection = cl.loadClass(service.getPackageName() + "." + service.getClassName());
          infoConnection = (InfoConnection) classConnection.newInstance();
          infoConnection.setId(infoConnectionName);
        }
        infoConnection.setInfoDependencies(
            service.getDependencies(ServiceDependency.DependencyType.SOURCE));
        infoConnection.setInfoConnectionDependencies(
            service.getDependencies(ServiceDependency.DependencyType.TARGET));

        return infoConnection;
      } catch (ClassNotFoundException
          | InstantiationException
          | IllegalAccessException
          | IOException e) {
        log.error("Error creating infoConnection \"" + infoConnectionName + "\"", e);
        return null;
      }
    }
  }

  public InfoConnection getInfoConnection(String connectionName,
                                          List<? extends IGisFileContainer> fileList,
                                          IGisFileSet outputFileSet,
                                          IGisFileContainer file) {
    InfoConnection tmpCon = this.getInfoConnection(connectionName);
    if (tmpCon instanceof IScriptPluginExec) {
      Environment.restoreDefaultPluginVariables((IScriptPluginExec) tmpCon);
    }

    tmpCon.setSourceFileList(fileList);
    tmpCon.setOutputFileSet(outputFileSet);
    tmpCon.setParentFileContainer(file);
//    tmpCon.setOutputFile(file);
    return tmpCon;
  }

  public List<String> getInfoConnectionList(GisFileType fileType) {
    List<String> lCon = new ArrayList<String>();
    for (String name : this.exManager.getInfoConnectionServices().keySet()) {
      ServiceList serviceList = this.exManager.getInfoConnectionServices().get(name);
      Service service = serviceList.getService(ServiceType.EXEC);
      if (service != null && service.getGisType() == fileType) {
        lCon.add(name);
      }
    }
    return lCon;

  }
}

package org.insensa.infoconnections;

import org.insensa.IGisFileContainer;
import org.insensa.connections.AbstractRasterInfoConnection;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.MeanValue;
import org.insensa.inforeader.StandardDeviationValue;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

abstract public class CorrelationBase extends AbstractRasterInfoConnection {

  static final int NODATA_SKIP = 1;
  static final int MEAN_INITIATION = 2;
  static final int ZERO_INITIATION = 3;
  static final String PARTIAL_COR = "partial";
  static final String T_TEST = "t-test";

  int option = NODATA_SKIP;
  boolean partial = false;
  boolean t_test = true;
  List<CorrelationData> correlationData = new ArrayList<>();
  List<CorrelationData> partCorList = new ArrayList<>();

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save(PARTIAL_COR, partial);
    saver.save(T_TEST, t_test);
    if (this.option == NODATA_SKIP) {
      saver.save("option", "NODATA_SKIP");
    } else if (this.option == MEAN_INITIATION) {
      saver.save("option", "MEAN_INITIATION");
    } else if (this.option == ZERO_INITIATION) {
      saver.save("option", "ZERO_INITIATION");
    }


    correlationData.forEach(correlationData1
        -> saver.save("covarianceCorrelation", correlationData1));
    if (this.partial) {
      partCorList.forEach(correlationData1
          -> saver.save("PartialCorrelation", correlationData1));
    }
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> deps = new ArrayList<String>();
    deps.add(CountValues.NAME);
    deps.add(MeanValue.NAME);
    deps.add(StandardDeviationValue.NAME);
    return deps;
  }

  public int getOption() {
    return this.option;
  }

  public void setOption(int option) {
    this.option = option;
  }

  public String getOptionName() {
    switch (this.option) {
      case NODATA_SKIP:
        return "NODATA_SKIP";
      case MEAN_INITIATION:
        return "MEAN_INITIATION";
      case ZERO_INITIATION:
        return "ZERO_INITIATION";
      default:
        return "NA";
    }
  }

  public boolean getPartial() {
    return this.partial;
  }

  public void setPartial(boolean partial) {
    this.partial = partial;
  }

  public boolean getT_test() {
    return this.t_test;
  }

  public void setT_test(boolean t_test) {
    this.t_test = t_test;
  }


  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);

  }


  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {

  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {

  }

  public List<CorrelationData> getCorrelationData() {
    return correlationData;
  }

  public List<CorrelationData> getPartCorList() {
    return partCorList;
  }

  @Override
  public void readInfos() throws IOException {

  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.infoconnections;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.inforeader.CountValues;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;
import org.insensa.inforeader.MeanValue;
import org.insensa.inforeader.StandardDeviationValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class LinearRegression extends CorrelationBase {

  private static final Logger log = LoggerFactory.getLogger(LinearRegression.class);

  //  private List<List<Object>> corStdDev = new ArrayList<>();
  private int countCalc;
  private int index = 0;

//  @Override
//  public void save(ISavableSaver saver) {
//    super.save(saver);
//    if (this.option == NODATA_SKIP) {
//      saver.save("option", "NODATA_SKIP");
//    } else if (this.option == MEAN_INITIATION) {
//      saver.save("option", "MEAN_INITIATION");
//    } else if (this.option == ZERO_INITIATION) {
//      saver.save("option", "ZERO_INITIATION");
//    }
//
//    for (List<Object> iterCorStdDev : corStdDev) {
//      saver.save("covariance", saver1 -> {
//        saver1.save("file1", (String) iterCorStdDev.get(0));
//        saver1.save("file2", (String) iterCorStdDev.get(1));
//        saver1.save("covariance", (Float) iterCorStdDev.get(2));
//        saver1.save("correlation", (Float) iterCorStdDev.get(3));
//      });
//    }
//  }
//
//  @Override
//  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
//  }
//
//  @Override
//  public void restore(ISavableRestorer restorer) {
//    super.restore(restorer);
//    this.corStdDev.clear();
//    this.option = restorer.loadInteger("option").orElse(NODATA_SKIP);
//    restorer.loadCollection("covariance", restorer1 -> {
//      List<Object> corStdDev = new ArrayList<>();
//      corStdDev.add(restorer.loadString("file1"));
//      corStdDev.add(restorer.loadString("file2"));
//      corStdDev.add(restorer.loadFloat("covariance"));
//      corStdDev.add(restorer.loadFloat("correlation"));
//      this.corStdDev.add(corStdDev);
//    });
//
//  }
//
//  @Override
//  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
//
//  }
//
//
//  public List<List<Object>> getCoVarList() {
//    return this.corStdDev;
//  }
//
//  @Override
//  public List<String> getInfoDependencies() {
//    List<String> deps = new ArrayList<String>();
//    deps.add(CountValues.NAME);
//    deps.add(MeanValue.NAME);
//    deps.add(StandardDeviationValue.NAME);
//    return deps;
//  }
//
//  public int getOption() {
//    return this.option;
//  }
//
//  public void setOption(int option) {
//    this.option = option;
//  }
//
//  public String getOptionName() {
//    switch (this.option) {
//      case NODATA_SKIP:
//        return "NODATA_SKIP";
//      case MEAN_INITIATION:
//        return "MEAN_INITIATION";
//      case ZERO_INITIATION:
//        return "ZERO_INITIATION";
//      default:
//        return "NA";
//    }
//  }

  public void solveDependencies(List<IGisFileContainer> files, IGisFileContainer templateFile) throws IOException {
    ExecDependencyController iDepChecker;
    InfoManager iManager = new InfoManager();
    if (this.workerStatus != null)
      this.workerStatus.startPause("(" + this.index + "/" + this.countCalc + ") " + "Resolve Dependencies");

    for (IGisFileContainer iFile: files) {
      InfoReader iReader;
      iReader = iFile.getInfoReader(StandardDeviationValue.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(MeanValue.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(CountValues.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iDepChecker = iFile.getDepChecker();

      InfoReader countValues = iManager.getInfoReader(CountValues.NAME);
      if (!iFile.equals(templateFile))
        ((CountValues) countValues).setTemplateFile((RasterFileContainer) templateFile);
      iFile.addInfoReader(countValues, true);
      iFile.addInfoReader(iManager.getInfoReader(MeanValue.NAME), true);
      iFile.addInfoReader(iManager.getInfoReader(StandardDeviationValue.NAME), true);

      iFile.executeAllInfos(null);
      iDepChecker.checkInfoDependencies((ConnectionFileChanger)this);
    }

    if (this.workerStatus != null)
      this.workerStatus.endPause();
  }

  @Override
  public void readInfos() throws IOException {
    this.index = 0;
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.toString());
    }

    if (!this.check()) {
      throw new IOException("The files are incompatible");
    }

//    if (this.getParentFileContainer() == null)
//      throw new IOException("No Output File Selected");

    if (this.option == NODATA_SKIP) {
      this.countCalc = (((sourceFileList.size() * (sourceFileList.size() + 1)) / 2) * 2) + (sourceFileList.size());
      for (int i = 0; i < sourceFileList.size(); i++) {
        this.index++;
        List<IGisFileContainer> newList = new ArrayList<>();
        newList.addAll(sourceFileList.subList(i, sourceFileList.size()));
        this.writeFitFile(sourceFileList.get(i), newList);
      }
    } else if (this.option == MEAN_INITIATION || this.option == ZERO_INITIATION) {
      this.countCalc = ((sourceFileList.size()));
      this.solveDependencies();
      for (int i = 0; i < sourceFileList.size(); i++) {
        this.index++;
        List<IGisFileContainer> newList = new ArrayList<>();
        newList.addAll(sourceFileList.subList(i, sourceFileList.size()));
        this.writeFile(sourceFileList.get(i), newList);
      }
    }

    this.used = true;
  }

  private void writeFile(IGisFileContainer fileX, List<IGisFileContainer> files) throws IOException {

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) sourceFileList.get(0)).getNRows());
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") " + this.toString());
    }

    int xSize = ((RasterFileContainer) fileX).getBand(RasterFileAccess.READ_ONLY).getXSize();
    int ySize = ((RasterFileContainer) fileX).getBand(RasterFileAccess.READ_ONLY).getYSize();
    int dataValues = xSize * ySize;// ((CountValues)fileX.getInfoReader(CountValues.NAME)).getNumOfData();//0;//xSize*ySize;
    float[][] doubleDoubleArray;
    List<IGisFileContainer> newList = new ArrayList<>();

    newList.add(fileX);
    newList.addAll(files);
    float fileXNoData = ((RasterFileContainer) fileX).getNoDataValue();
    double[] meanValues = new double[newList.size()];
    double[] stddevValues = new double[newList.size()];
    double[] coVar = new double[newList.size() - 1];
    int[] dataValuesCnts = new int[newList.size() - 1];
    for (int i = 0; i < coVar.length; i++) {
      coVar[i] = 0.0F;
      dataValuesCnts[i] = 0;
    }
    for (int i = 0; i < newList.size(); i++) {
      meanValues[i] = ((MeanValue) newList.get(i).getInfoReader(MeanValue.NAME)).getMeanValue();
      stddevValues[i] = ((StandardDeviationValue) newList.get(i).getInfoReader(StandardDeviationValue.NAME)).getStandardDeviationValue();
    }
    doubleDoubleArray = new float[newList.size()][xSize];

    int rowCount;

    // Schleife 1 Anzahl der Zeilen
    rowCount = ((RasterFileContainer) sourceFileList.get(0)).getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }
      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < newList.size(); j++) {
        ((RasterFileContainer) newList.get(j)).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        double tmpValue = doubleDoubleArray[0][j];
        for (int k = 1; k < newList.size(); k++) {
          double tmpValue2 = doubleDoubleArray[k][j];
          if (tmpValue2 == ((RasterFileContainer) newList.get(k)).getNoDataValue() && tmpValue == fileXNoData) {

          } else {
            if (tmpValue2 == ((RasterFileContainer) newList.get(k)).getNoDataValue()) {
              if (this.option == MEAN_INITIATION) {
                tmpValue2 = meanValues[k];
              } else if (this.option == ZERO_INITIATION) {
                tmpValue2 = 0;
              }
            } else if (tmpValue == fileXNoData) {
              if (this.option == MEAN_INITIATION) {
                tmpValue = meanValues[0];
              } else if (this.option == ZERO_INITIATION) {
                tmpValue = 0;
              }
            }
            dataValuesCnts[k - 1]++;
            coVar[k - 1] += (((tmpValue - meanValues[0]) * (tmpValue2 - meanValues[k])) / dataValues);
          }

        }// s4-ENDE
      }
    }// Schleife1-Ende

    this.correlationData = new ArrayList<>();
    for (int i = 0; i < coVar.length; i++) {
      CorrelationData data = new CorrelationData();
      data.file1 = sourceFileList.indexOf(fileX);
      data.file2 = sourceFileList.indexOf(files.get(i));
      coVar[i] *= dataValues;
      coVar[i] /= dataValuesCnts[i];
      data.covariance = (float) coVar[i];
      data.correlation = (float) (coVar[i] / (stddevValues[0] * stddevValues[i + 1]));
      if (this.t_test) {
        data.pValue = CorrelationUtils.createTTestValue(data.correlation,
            dataValuesCnts[i] - 2);
      }
      this.correlationData.add(data);
    }
    for (IGisFileContainer file: files) {
      file.getGisFile().unlock();
    }
    fileX.getGisFile().unlock();
  }

  /**
   * @param fileX
   * @param files
   * @throws IOException
   */
  private void writeFitFile(IGisFileContainer fileX, List<IGisFileContainer> files) throws IOException {
    double[] meanValuesX = new double[files.size()];
    double[] stdDevValuesX = new double[files.size()];
    long[] countValuesX = new long[files.size()];

    for (int i = 0; i < files.size(); i++) {
      List<IGisFileContainer> ilist = new ArrayList<>();
      ilist.add(fileX);
      this.solveDependencies(ilist, files.get(i));
      countValuesX[i] = ((CountValues) fileX.getInfoReader(CountValues.NAME)).getNumOfData();
      meanValuesX[i] = ((MeanValue) fileX.getInfoReader(MeanValue.NAME)).getMeanValue();
      stdDevValuesX[i] = ((StandardDeviationValue) fileX.getInfoReader(StandardDeviationValue.NAME)).getStandardDeviationValue();
      this.index++;

    }

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) sourceFileList.get(0)).getNRows());

    int xSize = ((RasterFileContainer) fileX).getBand(RasterFileAccess.READ_ONLY).getXSize();
    // int ySize = fileX.getBand().getYSize();
    // int dataValues = xSize*ySize;
    float[][] doubleDoubleArray;
    float[] doubleArrayXFile;
    float fileXNoData = ((RasterFileContainer) fileX).getNoDataValue();
    double[] meanValues = new double[files.size()];
    long[] countValues = new long[files.size()];
    double[] stddevValues = new double[files.size()];
    double[] coVar = new double[files.size()];
    int[] dataValuesCnts = new int[files.size()];
    for (int i = 0; i < coVar.length; i++) {
      coVar[i] = 0.0F;
      dataValuesCnts[i] = 0;
    }
    this.solveDependencies(files, fileX);
    for (int i = 0; i < files.size(); i++) {
      countValues[i] = ((CountValues) files.get(i).getInfoReader(CountValues.NAME)).getNumOfData();
      meanValues[i] = ((MeanValue) files.get(i).getInfoReader(MeanValue.NAME)).getMeanValue();
      stddevValues[i] = ((StandardDeviationValue) files.get(i).getInfoReader(StandardDeviationValue.NAME)).getStandardDeviationValue();
      this.index++;
    }
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("(" + this.index + "/" + this.countCalc + ") " + this.toString());
    }
    InfoReader iReader;
    iReader = fileX.getInfoReader(StandardDeviationValue.NAME);
    if (iReader != null)
      fileX.removeInfoReader(iReader);

    iReader = fileX.getInfoReader(MeanValue.NAME);
    if (iReader != null)
      fileX.removeInfoReader(iReader);

    iReader = fileX.getInfoReader(CountValues.NAME);
    if (iReader != null)
      fileX.removeInfoReader(iReader);

    for (IGisFileContainer iFile: files) {
      iReader = iFile.getInfoReader(StandardDeviationValue.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(MeanValue.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);

      iReader = iFile.getInfoReader(CountValues.NAME);
      if (iReader != null)
        iFile.removeInfoReader(iReader);
    }
    doubleDoubleArray = new float[files.size()][xSize];
    doubleArrayXFile = new float[xSize];

    int rowCount;

    // Schleife 1 Anzahl der Zeilen
    rowCount = ((RasterFileContainer) sourceFileList.get(0)).getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }
      // schleife 2 Anzahl der Dateien
      ((RasterFileContainer) fileX).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleArrayXFile);
      for (int j = 0; j < files.size(); j++) {
        ((RasterFileContainer) files.get(j)).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        double tmpValueX = doubleArrayXFile[j];
        // schleife 4 Anzahl der Dateien -1
        for (int k = 0; k < files.size(); k++) {
          double tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == ((RasterFileContainer) files.get(k)).getNoDataValue() || tmpValueX == fileXNoData) {

          } else {
            dataValuesCnts[k]++;
            coVar[k] += ((tmpValueX - meanValuesX[k]) * (tmpValue - meanValues[k])) / (countValues[k]);
          }

        }// s4-ENDE
      }
    }// Schleife1-Ende

    this.correlationData = new ArrayList<>();
    for (int i = 0; i < coVar.length; i++) {
      CorrelationData data = new CorrelationData();
      data.file1 = sourceFileList.indexOf(fileX);
      data.file2 = sourceFileList.indexOf(files.get(i));
      data.covariance = (float) coVar[i];
      data.correlation = (float) (coVar[i] / (stdDevValuesX[i] * stddevValues[i]));
      this.correlationData.add(data);
    }
    for (IGisFileContainer file: files) {
      file.getGisFile().unlock();
    }
    fileX.getGisFile().unlock();
  }


  @Override
  public void notifyDelete(IGisFileContainer subject) {

  }
}

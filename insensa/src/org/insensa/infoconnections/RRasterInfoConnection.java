package org.insensa.infoconnections;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.connections.AbstractRasterInfoConnection;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.model.generic.DataVariableConverter;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableImpl;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.jdom.JDOMException;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RRasterInfoConnection extends AbstractRasterInfoConnection implements IScriptPluginExec {

  private String scriptPath;
  private Map<String, IVariable> variables;

  public RRasterInfoConnection(String id) {
    setId(id);
    variables = new LinkedHashMap<>();
  }

  public void setScriptPath(String scriptPath) {
    this.scriptPath = scriptPath;
  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    if (variables != null) {
      variables.forEach((s, variable) -> saver.save(variable));
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (variables == null) {
      variables = new LinkedHashMap<>();
    }

    List<IRestorableData> data = restorer
        .loadCollection("variable", VariableImpl.class);
    data.forEach(iRestorableData ->
        variables.put(((IVariable) iRestorableData).getName(),
            (IVariable) iRestorableData));
  }

  @Override
  public Map<String, IVariable> getVariables() {
    return variables;
  }

  @Override
  public void setVariables(Map<String, IVariable> variableMap) {
    this.variables = variableMap;
  }

  @Override
  public void readInfos() throws IOException {
    RSession rSession = null;
    try {
      rSession = new RSession("127.0.0.1");
      RMessageStream stream = rSession.loadOutput(RMessageType.ALL);
      RMessageStreamMonitor.getInstance().registerMessageStream(stream);

      rSession.assignInsensaIsActive();
      rSession.assignInsensaWs(getParentFileContainer());
      rSession.assignSourceFiles(getSourceFileList());


      for (IVariable var: variables.values()) {
        rSession.assign(var);
      }

      DataVariableConverter converter = new DataVariableConverter();
      List<InfoConnection> infoConnections = getParentFileContainer().getInfoConnections();
      for (InfoConnection infoConnection: infoConnections) {
        if (!infoConnection.isUsed() || infoConnection == this) {
          continue;
        }
        if (infoConnection instanceof IScriptPluginExec) {
          for (IVariable var: ((IScriptPluginExec) infoConnection).getVariables().values()) {
            rSession.assign(var);
          }
        } else {
          infoConnection.save(converter);
        }
      }
      Map<String, IVariable> tmpVariableMap = converter.getVariableMap();
      for (Map.Entry<String, IVariable> entry: tmpVariableMap.entrySet()) {
        rSession.assign(entry.getValue());
      }

      rSession.sourceFile(new File(scriptPath));
      String str = rSession
          .eval("toString.default(insensa:::getDataXmlRoot())")
          .asString();


      JdomRestorer restorer = new JdomRestorer(str);
      List<IRestorableData> dataCollection = restorer
          .loadCollection("variable",
              VariableImpl.class);
      for (IRestorableData data: dataCollection) {
        variables.put(((IVariable) data).getName(), (IVariable) data);
      }

      rSession.unloadOuput();
      setUsed(true);
    } catch (REngineException | REXPMismatchException e) {
      e.printStackTrace();
    } finally {
      if (rSession != null) {
        try {
          rSession.closeAndClear();
        } catch (REXPMismatchException | REngineException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void notifyDelete(IGisFileContainer subject) {
  }
}

package org.insensa.extensions;

import org.insensa.model.generic.IVariable;
import org.insensa.view.dialogs.ViewSettingsCloseListener;
import org.insensa.view.extensions.ViewChangeType;

import java.util.Map;

public interface IPluginSettings {
  void setPluginExec(IPluginExec pluginExec);

  void startView(ViewSettingsCloseListener closeListener);

  void notifyViewChange(ViewChangeType type, Object payload);
}

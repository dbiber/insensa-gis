/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import org.insensa.view.menubar.DefaultExtensionItem;
import org.insensa.view.menubar.IExtensionItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MenuItemFactory {
  private ExtensionManager extensionManager;


  public MenuItemFactory() {
    this.extensionManager = ExtensionManager.getInstance();
  }

  public List<String> getExtensionItemNames() {
    Map<String, MenuExtension> itemMap = extensionManager.getMenuExtensions();
    List<String> nameList = new ArrayList<>(itemMap.keySet());
    return nameList;
  }

  public IExtensionItem getExtensionItem(String itemName) {
    ClassLoader cl = this.extensionManager.getUrlClassLoader();
    MenuExtension menuExtension = this.extensionManager.getMenuExtensions().get(itemName);
    if (menuExtension == null || menuExtension.getName() == null) {
      return null;
    } else {
      try {
        if (menuExtension.getPackageName() == null || menuExtension.getClassName() == null) {
          return new DefaultExtensionItem(menuExtension.getName());
        }
        Class classInfoReader = cl.loadClass(menuExtension.getPackageName() + "." + menuExtension.getClassName());
        return (IExtensionItem) classInfoReader.newInstance();
      } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
}

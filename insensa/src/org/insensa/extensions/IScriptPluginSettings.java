package org.insensa.extensions;

import org.insensa.model.generic.IVariable;

import java.util.Map;

public interface IScriptPluginSettings extends IPluginSettings {
  Map<String, IVariable> getVariables();
}

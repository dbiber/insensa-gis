/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HelpSetLoader {
  private static final Logger log = LoggerFactory.getLogger(HelpSetLoader.class);

  private static HelpSetLoader instance = new HelpSetLoader();

  public HelpSetLoader() {
    try {
      // clearPluginHelp();

      @SuppressWarnings("unused")
      Map<String, ServiceList> serviceMap = new HashMap<String, ServiceList>();
      ExtensionManager exManager = ExtensionManager.getInstance();
      this.addPluginHelp(exManager.getConnectionServices(), "plugins/Connections/");
      this.addPluginHelp(exManager.getOptionServices(), "plugins/Options/");
      this.addPluginHelp(exManager.getConnectionServices(), "plugins/Information Reader/");

    } catch (JDOMException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static HelpSetLoader getInstance() {
    return instance;
  }

  private boolean addMapEntry(String id, String fileURL) throws JDOMException, IOException {
    File mapFile = new File("help/pluginHelp/plugins.jhm");
    if (mapFile.exists() == false)
      return false;
    Document mapDoc = new SAXBuilder().build(mapFile);
    XMLOutputter mapOut = new XMLOutputter(Format.getPrettyFormat());
    Element mapRoot = mapDoc.getRootElement();
    // Element pluginElement = mapRoot.getChild("tocitem");
    @SuppressWarnings("unchecked")
    List<Object> eList = mapRoot.getChildren("mapID");
    // search if its already installed
    for (Object eObj : eList) {
      Element tocElem = (Element) eObj;
      if (tocElem.getAttributeValue("target").equals(id))
        return false;
    }
    Element pluginHelpElem = new Element("mapID");
    pluginHelpElem.setAttribute("target", id);
    pluginHelpElem.setAttribute("url", fileURL);
    mapRoot.addContent(pluginHelpElem);
    this.writeConfig(mapRoot, mapFile, mapDoc, mapOut);
    return true;
  }

  private void addPluginHelp(Map<String, ServiceList> connectionMap, String baseFolder) throws JDOMException, IOException {
    Iterator<String> nameIter = connectionMap.keySet().iterator();
    while (nameIter.hasNext()) {
      String name = nameIter.next();
      ServiceList list = connectionMap.get(name);
      Service execService = list.getService(ServiceType.EXEC);
      if (execService != null)
        if (execService.getHelp() != null) {
          this.addTocEntry(baseFolder + name, execService.getHelp().getId());
          if (execService.getHelp().getFileURL().toString() != null)
            this.addMapEntry(execService.getHelp().getId(), execService.getHelp().getFileURL().toString());
        }
      List<Service> serviceList = new ArrayList<Service>();
      serviceList.addAll(list.getServices(ServiceType.SETTINGS));
      serviceList.addAll(list.getServices(ServiceType.VIEWER));
      for (Service iService : serviceList) {
        ServiceHelp sHelp = iService.getHelp();
        if (sHelp != null) {
          this.addTocEntry(baseFolder + name + "/" + sHelp.getTocTitle(), sHelp.getId());
          if (sHelp.getFileURL().toString() != null)
            this.addMapEntry(sHelp.getId(), sHelp.getFileURL().toString());
        }
      }
    }
  }

  private boolean addTocEntry(String tocTitle, String id) throws JDOMException, IOException {
    File tocFile = new File("help/pluginHelp/plugins-toc.xml");
    if (tocFile.exists() == false)
      return false;
    Document tocDoc = new SAXBuilder().build(tocFile);
    XMLOutputter tocOut = new XMLOutputter(Format.getPrettyFormat());
    Element tocRoot = tocDoc.getRootElement();
    Element tmpElem = tocRoot;
    String[] tocArray = tocTitle.split("/");
    boolean foundElem = false;
    for (int i = 0; i < tocArray.length - 1; i++) {
      @SuppressWarnings("unchecked")
      List<Element> childList = tmpElem.getChildren();
      for (Element eElem : childList) {
        String text = eElem.getAttributeValue("text");
        if (text != null && text.equals(tocArray[i])) {
          foundElem = true;
          tmpElem = eElem;
          break;
        }
      }
      if (foundElem == false) {
        Element newElem = new Element("tocitem");
        newElem.setAttribute("text", tocArray[i]);
        tmpElem.addContent(newElem);
        tmpElem = newElem;
      } else
        foundElem = false;
    }
    Element titleElem = new Element("tocitem");
    titleElem.setAttribute("target", id);
    titleElem.setAttribute("text", tocArray[tocArray.length - 1]);
    tmpElem.addContent(titleElem);

    this.writeConfig(tocRoot, tocFile, tocDoc, tocOut);
    return true;
  }

  public URL getHelpSetURL() throws MalformedURLException {
    return new File("help/pluginHelp/plugins.hs").toURI().toURL();
  }

  private void writeConfig(Element rootDoc, File xmlFile, Document doc, XMLOutputter out) throws IOException {
    String fullFilename = xmlFile.getAbsolutePath();
    xmlFile.delete();

    xmlFile = new File(fullFilename);
    xmlFile.createNewFile();
    FileOutputStream streamOut = new FileOutputStream(xmlFile);
    if (out == null || doc == null) {
      log.error("Write Config Error: {}", xmlFile.getAbsolutePath());
    }
    out.output(doc, streamOut);
    streamOut.close();
  }
}

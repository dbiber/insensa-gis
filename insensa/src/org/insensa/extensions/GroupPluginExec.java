package org.insensa.extensions;

import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatus;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.VariableValue;
import org.insensa.model.generic.IVariable;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupPluginExec implements IScriptPluginExec {

  private List<IPluginExec> pluginExecs;
  private IPluginExec pluginExec;

  public GroupPluginExec(List<IPluginExec> pluginExecs) {
    this.pluginExecs = pluginExecs;
    this.pluginExec = pluginExecs.get(0);
  }

  @Override
  public Map<String, IVariable> getVariables() {
    if (pluginExec instanceof IScriptPluginExec) {
      return ((IScriptPluginExec) pluginExec).getVariables();
    }
    return new HashMap<>();
  }

  @Override
  public void setVariables(Map<String, IVariable> variableMap) {
    if (pluginExec instanceof IScriptPluginExec) {
      for(IPluginExec iScriptPluginExec : pluginExecs) {
        ((IScriptPluginExec)iScriptPluginExec).setVariables(variableMap);
      }
    }
  }

  @Override
  public String getGroupName() {
    return pluginExec.getGroupName();
  }

  @Override
  public String getId() {
    return pluginExec.getId();
  }

  @Override
  public void setId(String id) {
  }

  @Override
  public int getOrderId() {
    return pluginExec.getOrderId();
  }

  @Override
  public void setOrderId(int orderId) {
  }

  @Override
  public List<String> getInfoDependencies() {
    return pluginExec.getInfoDependencies();
  }

  @Override
  public void setInfoDependencies(List<String> infoDependencies) {
  }

  @Override
  public boolean isUsed() {
    return pluginExec.isUsed();
  }

  @Override
  public void setUsed(boolean used) {

  }

  @Override
  public void setWorkerStatus(WorkerStatus workerStatus) {
  }

  @Override
  public AttributeTable getData() {
    return null;
  }

  @Override
  public String getErrorMessage() {
    return null;
  }

  @Override
  public List<VariableValue> getVariableList() {
    return pluginExec.getVariableList();
  }

  @Override
  public void setVariableList(List<VariableValue> vaList) {
    for(IPluginExec iPluginExec : pluginExecs) {
      iPluginExec.setVariableList(vaList);
    }
  }

  @Override
  public IGisFileContainer getParentFileContainer() {
    return null;
  }

  @Override
  public void run() {

  }

  @Override
  public void restore(ISavableRestorer restorer) {

  }

  @Override
  public void save(ISavableSaver saver) {
    for(IPluginExec iPluginExec : pluginExecs) {
      iPluginExec.save(saver);
    }
  }

  public void refreshPlugin() throws IOException {
    for(IPluginExec iPluginExec : pluginExecs){
      iPluginExec
          .getParentFileContainer()
          .getGisFileInformationStorage()
          .refreshPluginExec(iPluginExec);
    }
  }
}

package org.insensa.extensions;

public enum ServiceType {
  EXEC("exec"),
  SETTINGS("settings"),
  VIEWER("viewer"),
  UNKNOWN("");

  private String type;

  ServiceType(String type) {
    this.type = type;
  }

  public static ServiceType from(String type) {
    for(ServiceType sType : ServiceType.values()) {
      if(sType.type.equalsIgnoreCase(type)) {
        return sType;
      }
    }
    return UNKNOWN;
  }

  public String value() {
    return type;
  }
}

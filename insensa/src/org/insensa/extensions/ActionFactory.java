/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;


import java.util.ArrayList;
import java.util.List;


/**
 * @author dennis
 *
 */
public class ActionFactory
{
	private ExtensionManager exManager;
	

	public ActionFactory()
	{
		this.exManager = ExtensionManager.getInstance();
	}
	
	public List<String> getActionNames()
	{
		return new ArrayList<String>(exManager.getActionExtensions().keySet());
	}
	
	public boolean contains(String actionName)
	{
		ActionExtension actionExt =  exManager.getActionExtensions().get(actionName);
		if(actionExt!=null)
			return true;
		return false;
	}

	public boolean isDialogAction(String actionName)
	{
		
		ActionExtension actionExt =  exManager.getActionExtensions().get(actionName);
		if(actionExt.getType()!=null && actionExt.getType().equals(ActionExtension.TYPE_DIALOG))
			return true;
		return false;
	}
	
//	public AbstractImporterSettingDialog getDialogAction(String actionName)
//	{
//		ActionExtension actionExt =  exManager.getActionExtensions().get(actionName);
//		if(actionExt==null)
//			return null;
//		ClassLoader cl = this.exManager.getUrlClassLoader();
//		try
//		{
//			Class classInfoReader = cl.loadClass(actionExt.getPackageName() + "." + actionExt.getClassName());
//			return (AbstractImporterSettingDialog) classInfoReader.newInstance();
//		} catch (ClassNotFoundException e)
//		{
//			e.printStackTrace();
//			return null;
//		} catch (InstantiationException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		} catch (IllegalAccessException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//	}
}

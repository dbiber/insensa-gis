/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;


import org.insensa.helpers.IOHelper;
import org.insensa.view.extensions.IConnectionView;
import org.insensa.view.extensions.IGisFileView;
import org.insensa.view.extensions.IInfoConnectionView;
import org.insensa.view.extensions.IInfoReaderView;
import org.insensa.view.r.GenericRView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.util.Map;

public class ViewerFactory {
  private static final Logger log = LoggerFactory.getLogger(ViewerFactory.class);

  private ExtensionManager extensionManager;

  public ViewerFactory(ExtensionManager extensionManager) {
    this.extensionManager = extensionManager;
  }


  private Object createView(String name, String id, Map<String, ServiceList> serviceMap) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    ClassLoader cl = this.extensionManager.getUrlClassLoader();
    Service service = null;
    Service serviceGlobal = null;
    ServiceList normalServiceList = serviceMap.get(name);
    if (normalServiceList != null) {
      service = normalServiceList.getService(ServiceType.VIEWER, id);
    }

    ServiceList globalList = serviceMap.get("global");
    if (globalList != null) {
       serviceGlobal = globalList.getService(ServiceType.VIEWER, id);
    }

    Service selectedService;
    if (service != null) {
      selectedService = service;
    } else if (serviceGlobal != null) {
      selectedService = serviceGlobal;
    } else {
      return null;
    }

    if (selectedService.getScript() != null) {
      return getGenericViewer(service);
    } else {
      Class<?> viewInfo = cl.loadClass(selectedService.getPackageName() + "." + selectedService.getClassName());
      return viewInfo.newInstance();
    }
  }

  public IInfoConnectionView getInfoConnectionView(String connectionName, String id) throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
    return (IInfoConnectionView) createView(connectionName, id, extensionManager.getInfoConnectionServices());
  }

  public IConnectionView getConnectionView(String connectionName, String id) throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
    return (IConnectionView) createView(connectionName, id, extensionManager.getConnectionServices());
  }

  public IInfoReaderView getInfoReaderViewer(String infoReaderName, String id) throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
    return (IInfoReaderView) createView(infoReaderName, id, extensionManager.getInfoReaderServices());
  }

  public IGisFileView getRasterFileView(String id) throws ClassNotFoundException,
      InstantiationException, IllegalAccessException {


    Service service = this.extensionManager.getFileServices()
        .getService(ServiceType.VIEWER, id);
    if (service == null) {
      log.error("Could not find service with orderId \"" + id + "\"");
      return null;
    }

    if (service.getScript() != null) {
      return getGenericViewer(service);
    } else {
      ClassLoader cl = this.extensionManager.getUrlClassLoader();
      Class viewFile = cl.loadClass(service.getPackageName() + "." + service.getClassName());
      return (IGisFileView) viewFile.newInstance();
    }
  }

  public IGisFileView getGenericViewer(Service service) {
    GenericRView genericRasterView = new GenericRView();
    URL scriptUrl = this.extensionManager.getUrlClassLoader()
        .findResource("scripts/" + service.getScript().getFilename());
    try {

      JarURLConnection jarURLConnection = (JarURLConnection) scriptUrl.openConnection();
      String zipFileName = jarURLConnection.getJarFile().getName();
      Path zipFilePath = new File(zipFileName).toPath();
      String fileToExtract = "scripts/" + service.getScript().getFilename();
      File outputFile = new File(IOHelper.getInstance().getPluginsDir(), fileToExtract);
      if (outputFile.exists()) {
        outputFile.delete();
      }
      Path outputPath = outputFile.toPath();
      IOHelper.extractFile(zipFilePath, fileToExtract, outputPath);
      genericRasterView.setFilePath(outputPath.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return genericRasterView;
  }
}

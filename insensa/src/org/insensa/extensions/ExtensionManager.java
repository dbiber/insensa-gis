/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.extensions;


import de.bibertech.javahelp.viewer.model.HelpMappingObject;

import org.insensa.Environment;
import org.insensa.GisFileType;
import org.insensa.InsensaGIS;
import org.insensa.helpers.IOHelper;
import org.insensa.view.extensions.ExtensionCreator;
import org.insensa.view.extensions.ExtensionEntity;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class ExtensionManager {
  public static final String EXTENSION_FILE = "xml" + File.separator + "extensions.xml";
  private static final Logger log = LoggerFactory.getLogger(ExtensionManager.class);
  private static ExtensionManager instance = new ExtensionManager(EXTENSION_FILE);
  private ServiceList fileServices;
  private Map<String, ServiceList> connectionServices;
  private Map<String, ServiceList> infoReaderServices;
  private Map<String, ServiceList> optionServices;
  private Map<String, ServiceList> exportFilesServices;
  private Map<String, ServiceList> importFilesServices;
  private Map<String, ServiceList> infoConnectionServices;
  private Map<String, MenuExtension> menuExtensions;
  private Map<String, ActionExtension> actionExtensions;
  private List<ServiceHelp> helpList;
  private URLClassLoader urlClassLoader;
  private ServiceHelp rootserviceHelp;

  private ExtensionManager(String filename) {
    this.connectionServices = new TreeMap<>();
    this.infoReaderServices = new TreeMap<>();
    this.optionServices = new TreeMap<>();
    this.fileServices = new ServiceList();
    this.exportFilesServices = new TreeMap<>();
    this.importFilesServices = new TreeMap<>();
    this.menuExtensions = new TreeMap<>();
    this.actionExtensions = new TreeMap<>();
    this.infoConnectionServices = new TreeMap<>();
    this.rootserviceHelp = new ServiceHelp();

//    try {
      this.helpList = new ArrayList<>();
      rootserviceHelp.setId("Extensions");
      rootserviceHelp.setTocTitle("Extensions");
//      rootserviceHelp.setFilename("");
      helpList.add(rootserviceHelp);

//      File exXmlFile = new File(filename);
//      if (!exXmlFile.exists()) {
//        throw new RuntimeException("File " + filename + " do not exists");
//      }
//      Document doc = new SAXBuilder().build(filename);
//      Element rootElement = doc.getRootElement();
//      this.readXmlConfig(rootElement, filename);
//      this.readPluginFolder();
//    } catch (IOException | JDOMException exception) {
//      throw new RuntimeException(exception);
//    }
  }

  public void readPlugins() {
    try {
      File exXmlFile = new File(EXTENSION_FILE);
      if (!exXmlFile.exists()) {
        throw new RuntimeException("File " + EXTENSION_FILE + " do not exists");
      }
      Document doc = new SAXBuilder().build(EXTENSION_FILE);
      Element rootElement = doc.getRootElement();
      this.readXmlConfig(rootElement, EXTENSION_FILE);
      this.readPluginFolder();
    } catch (IOException | JDOMException exception) {
      throw new RuntimeException(exception);
    }
  }

  public static ExtensionManager getInstance() {
    return instance;
  }

  public void loadLocalPlugins() throws IOException {
    List<ExtensionEntity> extensions = Environment.getRootProperties().getLocalExtensions();
    for (ExtensionEntity extension : extensions) {
      ExtensionCreator creator = new ExtensionCreator();
      creator.setEntity(extension);
      creator.loadPlugin();
    }
  }

  public Map<String, ServiceList> getConnectionServices() {
    if (this.connectionServices == null) {
      return new HashMap<>();
    }
    return this.connectionServices;
  }

  public Map<String, ServiceList> getInfoConnectionServices() {
    if (this.infoConnectionServices == null) {
      return new HashMap<>();
    }
    return this.infoConnectionServices;
  }

  public Map<String, ServiceList> getFileExporterServices() {
    return this.exportFilesServices;
  }

  public Map<String, ServiceList> getFileImporterServices() {
    return this.importFilesServices;
  }

  public ServiceList getFileServices() {
    return this.fileServices;
  }

  public List<ServiceHelp> getHelpList() {
    return this.helpList;
  }

  public Map<String, ServiceList> getInfoReaderServices() {
    if (this.infoReaderServices == null) {
      return new HashMap<>();
    }
    return this.infoReaderServices;
  }

  public Map<String, ServiceList> getOptionServices() {
    if (this.optionServices == null) {
      return new HashMap<>();
    }
    return this.optionServices;
  }

  public Map<String, MenuExtension> getMenuExtensions() {
    if (this.menuExtensions == null) {
      return new HashMap<>();
    }
    return menuExtensions;
  }

  public Map<String, ActionExtension> getActionExtensions() {
    if (this.actionExtensions == null) {
      return new HashMap<>();
    }
    return actionExtensions;
  }

  public URLClassLoader getUrlClassLoader() {
    return this.urlClassLoader;
  }

  /**
   * Read help content.
   */
  private ServiceHelp readHelp(Element parentHelpElement, ServiceHelp parentHelp, String archiveName) {
    // Later on, we could define the default in the service itself.
    ServiceHelp help = null;
    List helpElemList = parentHelpElement.getChildren("help",
        parentHelpElement.getNamespace());
    if (helpElemList != null) {
      for (Object objElem : helpElemList) {
        Element iterElem = (Element) objElem;
        help = new ServiceHelp();

        help.setFilename(iterElem.getAttributeValue("filename"));
        help.setId(iterElem.getAttributeValue("id"));
        help.setTocTitle(iterElem.getAttributeValue("toc"));
        help.setParentId(iterElem.getAttributeValue("parentID"));
        if (archiveName != null) {
          URLClassLoader classLoader = null;
          try {
            classLoader = new URLClassLoader(new URL[]{new URL("file:" + archiveName)});
          } catch (MalformedURLException e) {
            throw new RuntimeException("Error creating plugin help ", e);
          }
          URL helpURL = classLoader.findResource("help/" + help.getFilename());
          help.setFileURL(helpURL);
        } else {
          try {
            help.setFileURL(new File("help/", help.getFilename()).toURI().toURL());
          } catch (MalformedURLException e) {
            throw new RuntimeException("Error creating plugin help ", e);
          }
        }
//        URL helpURL;
//        if (this.urlClassLoader == null) {
//          try {
//            helpURL = new URL("file:" + help.getFilename());
//            help.setFileURL(helpURL);
//          } catch (MalformedURLException e1) {
//            throw new IOException(e1.getCause());
//          }
//        } else {
//          helpURL = this.urlClassLoader.findResource("help/" + help.getFilename());
//          help.setFileURL(helpURL);
//        }
        if (parentHelp != null) {
          parentHelp.addHelpChild(help);
        } else {
          this.helpList.add(help);
        }
        this.readHelp(iterElem, help, archiveName);
      }
    }
    return help;
  }

  private void readPluginFolder() throws IOException, JDOMException {
    File pluginFolder = IOHelper.getInstance().getPluginsDir();
    //Just to create the Directory if it does not exist
    List<URL> classloaderURLs = new ArrayList<>();
    ClassLoader classLoader = InsensaGIS.class.getClassLoader();
    // Load root classloader and add all internal URLs
    if (classLoader instanceof URLClassLoader) {
      URL[] urls = ((URLClassLoader) classLoader).getURLs();
      classloaderURLs.addAll(Arrays.asList(urls));
    }
    // Add all jar files URLs within the "pluginFolder" to the classloaderURLs List
    if (pluginFolder.exists() && pluginFolder.isDirectory()) {
      File[] fileArray = pluginFolder.listFiles();
      if (fileArray == null) {
        throw new IOException("Error reading file directory from pluginFolder "
            + pluginFolder.getAbsolutePath());
      }
      for (File selFile : fileArray) {
        if (selFile.getName().endsWith(".jar")) {
          classloaderURLs.add(selFile.toURI().toURL());
        } else if (selFile.getName().endsWith(".zip")) {
          classloaderURLs.add(selFile.toURI().toURL());
        }
      }
    } else {
      log.error("Plugin folder not found", new Throwable());
    }


    //Create a URL Classloader and add all URLs
    URL[] urlArray = new URL[classloaderURLs.size()];
    classloaderURLs.toArray(urlArray);
    this.urlClassLoader = new URLClassLoader(urlArray);

    //TODO: Test external Lib loader;
    List<URL> testUrlList = new ArrayList<>();
    Map<String, String> nativeMap = new HashMap<>();
    //Walk through every URL (jar)
    for (URL tUrl : classloaderURLs) {
//			URL tUrl = urlArray[i];
      InputStream is = tUrl.openStream();
      ZipInputStream zis = new ZipInputStream(is);
      ZipEntry ze = null;
      //Iterate every zipped file
      while (!(zis.available() <= 0) && (ze = zis.getNextEntry()) != null) {
        //Load native dependencies
        if (ze.getName().startsWith("jni/" + "native/" + System.getProperty("os.name").toLowerCase())) {
          String name = ze.getName();
          name = name.substring(name.lastIndexOf(org.insensa.helpers.SystemSpec.separator) + 1);
          URL internalJarUrl = this.urlClassLoader.findResource(ze.getName());
          if (internalJarUrl != null) {
            InputStream is2 = internalJarUrl.openStream();

            String tmpDirName = System.getProperty("java.io.tmpdir");
            File tmpLib = new File(tmpDirName + File.separator + name);
            tmpLib.deleteOnExit();
            if (!tmpLib.exists()) {
              OutputStream os = new FileOutputStream(tmpLib);
              int readCount;
              byte[] buffer = new byte[4096];
              while ((readCount = is2.read(buffer)) != -1) {
                os.write(buffer, 0, readCount);
              }
              nativeMap.put(name, tmpLib.getAbsolutePath());
              os.close();
              is2.close();

            } else
              nativeMap.put(name, tmpLib.getAbsolutePath());
          }
        }
        //Extract every internal plugin jar to a temp file and add to the classloader
        if (ze.getName().startsWith("libs") && ze.getName().endsWith(".jar")) {
          URL internalJarUrl = this.urlClassLoader.findResource(ze.getName());
          InputStream is2 = internalJarUrl.openStream();
          String name = ze.getName().replace('/', '_');
          int i = name.lastIndexOf(".");
          String extension = i > -1 ? name.substring(i) : "";
          File outputTmpFile = File.createTempFile(name.substring(0, name.length() - extension.length()) + ".", extension);
          OutputStream os = new FileOutputStream(outputTmpFile);
          int readCount;
          byte[] buffer = new byte[4096];
          while ((readCount = is2.read(buffer)) != -1) {
            os.write(buffer, 0, readCount);
          }
          outputTmpFile.deleteOnExit();
          URL newOutUrl = outputTmpFile.toURI().toURL();
          testUrlList.add(newOutUrl);
          os.close();
          is2.close();
        }
      }
      is.close();
      zis.close();
    }

    //TODO: Test external Lib loader;
    classloaderURLs.addAll(testUrlList);
    urlArray = new URL[classloaderURLs.size()];
    classloaderURLs.toArray(urlArray);
    this.urlClassLoader = new ExtensionClassLoader(urlArray);
    ((ExtensionClassLoader) this.urlClassLoader).setNativeMap(nativeMap);


    Enumeration<URL> urlEnum = this.urlClassLoader.findResources("xml/" + "extensions.xml");
    if (!urlEnum.hasMoreElements()) {
      log.warn("No plugins found");
    }
    while (urlEnum.hasMoreElements()) {
      URL nextUrl = urlEnum.nextElement();
      if (nextUrl != null) {
        parseConfigFromUrl(nextUrl);
      }
    }
  }
//
//  private void extractAndParseScriptPlugin(File file) {
//    try {
//      ExtensionClassLoader tempClassLoader = new ExtensionClassLoader(new URL[]{file.toURI().toURL()});
//      URL url = tempClassLoader.findResource("xml/extensions.xml");
//      if (url != null) {
//        parseConfigFromUrl(url);
//      }
//    } catch (JDOMException | IOException e) {
//      e.printStackTrace();
//    }
//  }

  private void parseConfigFromUrl(URL url) throws IOException, JDOMException {
    String archiveName = "";
    URLConnection urlConn = url.openConnection();
    if (urlConn instanceof JarURLConnection) {
      archiveName = ((JarURLConnection) urlConn).getJarFile().getName();
    }
    InputStream iStream = urlConn.getInputStream();
    Document doc = new SAXBuilder().build(iStream);

    Element rootElement = doc.getRootElement();
    this.readXmlConfig(rootElement, archiveName);
  }

  private List<Script> createScriptsFromElement(Element serviceElement, String archiveName) {
    List<Script> scripts = new ArrayList<>();
    List<?> childElemList = serviceElement.getChildren(Service.SCRIPT, serviceElement.getNamespace());
    for (Object scriptElemObj : childElemList) {
      Element scriptElem = (Element) scriptElemObj;
      Script script = new Script();
      script.setArchiveFile(new File(archiveName));
      script.setFilename(scriptElem.getAttributeValue(Script.FILENAME));
      script.setLanguage(Languages.valueOf(scriptElem.getAttributeValue(Script.LANGUAGE)));
      scripts.add(script);
    }
    return scripts;
  }

  private List<ServiceDependency> createDependenciesFromElement(Element serviceElement) {
    List<ServiceDependency> serviceDependencies = new ArrayList<>();
    Element depsElem = serviceElement.getChild(Service.DEPENDENCIES, serviceElement.getNamespace());
    if (depsElem == null) {
      return serviceDependencies;
    }
    List childElemList = depsElem.getChildren();
    for (Object dependencyElemObj : childElemList) {
      Element depElem = (Element) dependencyElemObj;
      ServiceDependency serviceDependency = new ServiceDependency();
      serviceDependency.setId(depElem.getAttributeValue(Service.ID));
      String type = depElem.getAttributeValue(Service.DEPENDENCY_TYPE);
      serviceDependency.setType(ServiceDependency.DependencyType.SOURCE);
      if (type != null) {
        serviceDependency.setType(ServiceDependency.DependencyType.fromName(type));
      }
      serviceDependencies.add(serviceDependency);
    }
    return serviceDependencies;
  }

  private Service createServiceFromElement(Element serviceElement,
                                           String archiveName) {
    Service service = new Service();
    service.setType(serviceElement.getAttributeValue(Service.TYPE));
    service.setClassName(serviceElement.getAttributeValue("classname"));
    service.setPackageName(serviceElement.getAttributeValue("package"));
    service.setPriority(serviceElement.getAttributeValue("priority"));
    service.setName(serviceElement.getAttributeValue("name"));
    service.setTitle(serviceElement.getAttributeValue("title"));
    GisFileType gisFileType = GisFileType.byName(serviceElement.getAttributeValue("gisType"));
    if (gisFileType != null && gisFileType != GisFileType.UNKNOWN) {
      service.setGisType(gisFileType);
    }

    List<Script> scripts = createScriptsFromElement(serviceElement, archiveName);
    if (!scripts.isEmpty()) {
      service.setScript(scripts.get(0));
    }
    List<ServiceDependency> serviceDependencies =
        createDependenciesFromElement(serviceElement);
    if (!serviceDependencies.isEmpty()) {
      service.setDependencies(serviceDependencies);
    }
    return service;
  }

  private void createServicesFromParent(Element rootElement,
                                        String childElementsName,
                                        Map<String, ServiceList> serviceMapToAddTo,
                                        String archiveName) {
    if (rootElement == null) {
      return;
    }
    List<?> childElemList = rootElement.getChildren(childElementsName, rootElement.getNamespace());
    for (Object childElem : childElemList) {
      List<?> serviceElemList = ((Element) childElem).getChildren("service", rootElement.getNamespace());
      String elemId = ((Element) childElem).getAttributeValue("id");
      ServiceList serviceList = serviceMapToAddTo.get(elemId);
      if (serviceList == null) {
        serviceList = new ServiceList();
      }
      for (Object iService : serviceElemList) {
        Service service = createServiceFromElement((Element) iService, archiveName);
        ServiceHelp serviceHelp = readHelp((Element) iService, rootserviceHelp, archiveName);
        service.setHelp(serviceHelp);
        serviceList.add(service);
      }
//      String lowerCaseName = elemId.substring(0, 1).toLowerCase() + elemId.substring(1);
//      serviceMapToAddTo.put(lowerCaseName, serviceList);
      serviceMapToAddTo.put(elemId, serviceList);
    }

  }

  public void registerInfoReader(Class mClass,
                                 String id,
                                 GisFileType fileType,
                                 ServicePriority priority,
                                 String title,
                                 String name,
                                 ServiceType type) {
    Service service = new Service();
    service.setName(name);
    service.setClassName(mClass.getSimpleName());
    service.setGisType(fileType);
    service.setPackageName(mClass.getPackage().getName());
    service.setPriority(priority.value());
    service.setType(type.value());
    service.setTitle(title);
    ServiceList serviceList = infoReaderServices.get(id);
    if (serviceList == null) {
      serviceList = new ServiceList();
    }
    serviceList.add(service);
    infoReaderServices.put(id, serviceList);
  }

  private void readXmlConfig(Element rootDoc, String archiveName)
      throws IOException {
    Element fileElem = rootDoc.getChild("files", rootDoc.getNamespace());
    if (fileElem != null) {
      List<?> serviceElemList = fileElem.getChildren("service", rootDoc.getNamespace());
      for (Object iService : serviceElemList) {
        this.fileServices.add(createServiceFromElement((Element) iService,
            archiveName));
      }
    }

    Element infoConElem = rootDoc.getChild("infoConnections", rootDoc.getNamespace());
    createServicesFromParent(infoConElem, "infoConnection",
        infoConnectionServices, archiveName);

    Element conElem = rootDoc.getChild("connections", rootDoc.getNamespace());
    createServicesFromParent(conElem, "connection",
        connectionServices, archiveName);

    Element infos = rootDoc.getChild("inforeaders", rootDoc.getNamespace());
    createServicesFromParent(infos, "inforeader",
        infoReaderServices, archiveName);

    Element options = rootDoc.getChild("options", rootDoc.getNamespace());
    createServicesFromParent(options, "option",
        optionServices, archiveName);

    Element exporter = rootDoc.getChild("exporter", rootDoc.getNamespace());
    if (exporter != null) {
      Element files = exporter.getChild("files", rootDoc.getNamespace());
      createServicesFromParent(files, "export",
          exportFilesServices, archiveName);
    }

    Element importer = rootDoc.getChild("importer", rootDoc.getNamespace());
    if (importer != null) {
      Element files = importer.getChild("files", rootDoc.getNamespace());
      createServicesFromParent(files, "import",
          importFilesServices, archiveName);
//      readHelp(importer, null);
    }

    Element menus = rootDoc.getChild("menu", rootDoc.getNamespace());
    if (menus != null) {
      List<?> menuItemList = menus.getChildren("menuItem", rootDoc.getNamespace());
      Map<String, MenuExtension> menuExtMap = new TreeMap<String, MenuExtension>();

      for (Object iMenuItem : menuItemList) {
        String menuItemId = ((Element) iMenuItem).getAttributeValue("id");
        MenuExtension mExt = new MenuExtension();
        mExt.setName(((Element) iMenuItem).getAttributeValue("name"));
        mExt.setClassName(((Element) iMenuItem).getAttributeValue("classname"));
        mExt.setPackageName(((Element) iMenuItem).getAttributeValue("package"));
        mExt.setPriority(((Element) iMenuItem).getAttributeValue("priority"));
        menuExtMap.put(menuItemId, mExt);

        List<?> linkElemList = ((Element) iMenuItem).getChildren("actionLink", rootDoc.getNamespace());

        for (Object iActionLink : linkElemList) {
          ActionLink actionLink = new ActionLink();
          actionLink.setTarget(((Element) iActionLink).getAttributeValue("target"));
          mExt.addActionLink(actionLink);

        }
        this.menuExtensions.putAll(menuExtMap);
      }
    }

    Element actions = rootDoc.getChild("actions", rootDoc.getNamespace());
    if (actions != null) {
      List<?> actionList = actions.getChildren("action", rootDoc.getNamespace());
      Map<String, ActionExtension> actionMap = new TreeMap<String, ActionExtension>();

      for (Object iAction : actionList) {
        String actionId = ((Element) iAction).getAttributeValue("id");
        ActionExtension actionExt = new ActionExtension();
        actionExt.setName(((Element) iAction).getAttributeValue("name"));
        actionExt.setClassName(((Element) iAction).getAttributeValue("classname"));
        actionExt.setPackageName(((Element) iAction).getAttributeValue("package"));
        actionExt.setPriority(((Element) iAction).getAttributeValue("priority"));
        actionExt.setType(((Element) iAction).getAttributeValue("type"));
        actionMap.put(actionId, actionExt);
      }
      this.actionExtensions.putAll(actionMap);
    }
  }


  private List<String> getServiceIdList(Map<String, ServiceList> services,
                                        ServiceType serviceType,
                                        GisFileType fileType) {
    List<String> serviceNameList = new ArrayList<>();
    services
        .forEach((s, serviceList) -> {
          serviceList.getServices(serviceType, fileType).forEach(service -> {
            if (service.getScript() == null) {
              serviceNameList.add(s);
            } else if (service.getScript().getLanguage().toString().equals("R")) {
              if (Environment.getInstance().getModel().getProperties().isRServeRunning()) {
                serviceNameList.add(s);
              }
            }
          });

        });
    return serviceNameList;
  }

  public List<String> getUsableInfoReaderList(ServiceType exec, GisFileType geoTiff) {
    return getServiceIdList(infoReaderServices, exec, geoTiff);
  }

  public List<String> getUsableOptionList(ServiceType exec, GisFileType gisFileType) {
    return getServiceIdList(optionServices, exec, gisFileType);
  }

  public List<String> getUsableConnectionList(ServiceType exec, GisFileType gisFileType) {
    return getServiceIdList(connectionServices, exec, gisFileType);
  }

  public List<String> getUsableInfoConnectionList(ServiceType exec, GisFileType gisFileType) {
    return getServiceIdList(infoConnectionServices, exec, gisFileType);
  }

  private HelpMappingObject buildHelpMapping(ServiceHelp serviceHelp) {

    HelpMappingObject mo = new HelpMappingObject(serviceHelp.getId(),
        "",
        serviceHelp.getTocTitle());
    if (serviceHelp.getFileURL() != null) {
      mo.setHref(serviceHelp.getFileURL().getFile());
      mo.setUrl(serviceHelp.getFileURL());
    }
    for (ServiceHelp child : serviceHelp.getHelpChildren()) {
      mo.addMappingObject(buildHelpMapping(child));
    }
    return mo;
  }

  public List<HelpMappingObject> getPluginHelp() {
    List<HelpMappingObject> helpMappins = new ArrayList<>();
    List<ServiceHelp> helpList = getHelpList();
    for (ServiceHelp help : helpList) {
      HelpMappingObject mo = buildHelpMapping(help);
      helpMappins.add(mo);
    }
    return helpMappins;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ServiceHelp {
  private String filename;
  private String id;
  private String parentId;
  private String tocTitle;
  private URL fileURL;
  private ServiceHelp parentHelp;
  private List<ServiceHelp> helpChildren;


  public ServiceHelp() {
    this.helpChildren = new ArrayList<ServiceHelp>();
  }

  public void addHelpChild(ServiceHelp child) {
    this.helpChildren.add(child);
    child.setParentHelp(this);
  }

  public String getFilename() {
    return this.filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public URL getFileURL() {
    return this.fileURL;
  }

  public void setFileURL(URL fileURL) {
    this.fileURL = fileURL;
  }

  public List<ServiceHelp> getHelpChildren() {
    return this.helpChildren;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ServiceHelp getParentHelp() {
    return this.parentHelp;
  }

  public void setParentHelp(ServiceHelp parentHelp) {
    this.parentHelp = parentHelp;
  }

  public String getParentId() {
    return this.parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getTocTitle() {
    return this.tocTitle;
  }

  public void setTocTitle(String tocTitle) {
    this.tocTitle = tocTitle;
  }
}

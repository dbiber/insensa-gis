/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;


import org.insensa.GisFileType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Service {
  public static final String SETTINGS = "settings";
  public static final String EXEC = "exec";
  public static final String VIEWER = "viewer";
  public static final String TYPE = "type";
  public static final String TYPE_VIEWER = "viewer";
  public static final String TYPE_EXEC = "exec";
  public static final String SCRIPT = "script";
  public static final String ID = "id";
  public static final String TITLE = "title";
  public static final String DEPENDENCIES = "dependencies";
  public static final String DEPENDENCY = "dependency";
  public static final String DEPENDENCY_TYPE = "for";
  private static final Logger log = LoggerFactory.getLogger(Service.class);
  private String name = "";
  private String className;
  private String packageName;
  private ServicePriority priority;
  private ServiceType type;
  private Script script;
  private String title;
  private ServiceHelp help;
  private List<ServiceDependency> dependencies;
  private GisFileType gisType = GisFileType.GEO_TIFF;
  private String archiveName;


  public Service() {
    dependencies = new ArrayList<>();
  }

  public String getClassName() {
    return this.className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public ServiceHelp getHelp() {
    return this.help;
  }

  public void setHelp(ServiceHelp help) {
    this.help = help;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPackageName() {
    return this.packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public ServicePriority getPriority() {
    return this.priority;
  }

  public void setPriority(String priorityName) {
    this.priority = ServicePriority.fromName(priorityName);
  }

  public void setPriority(ServicePriority priority) {
    this.priority = priority;
  }

  public void setType(ServiceType type) {
    this.type = type;
  }

  public ServiceType getType() {
    return type;
  }

  public void setType(String typeName) {
    ServiceType mType = ServiceType.from(typeName);
    if (mType == ServiceType.UNKNOWN) {
      log.error("Type of service \"" + name + "\" is unknown");
    }
    this.type = ServiceType.from(typeName);
  }

  public Script getScript() {
    return script;
  }

  public void setScript(Script script) {
    this.script = script;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<ServiceDependency> getDependencies() {
    return dependencies;
  }

  public void setDependencies(List<ServiceDependency> dependencies) {
    this.dependencies = dependencies;
  }

  public void addDependency(ServiceDependency serviceDependency) {
    this.dependencies.add(serviceDependency);
  }

  public GisFileType getGisType() {
    return gisType;
  }

  public void setGisType(GisFileType gisType) {
    this.gisType = gisType;
  }

  public String getArchiveName() {
    return archiveName;
  }

  public void setArchiveName(String archiveName) {
    this.archiveName = archiveName;
  }

  public List<String> getDependencies(ServiceDependency.DependencyType type) {
    return dependencies.stream()
        .filter(serviceDependency -> serviceDependency.getType() == type)
        .map(ServiceDependency::getId)
        .collect(Collectors.toList());
  }
}

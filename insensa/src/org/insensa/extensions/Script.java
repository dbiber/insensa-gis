/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.extensions;


import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.insensa.helpers.IOHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Script {
  public static final String FILENAME = "filename";
  public static final String LANGUAGE = "language";
  private static final Logger log = LoggerFactory.getLogger(Script.class);
  private Languages language;
  private File archiveFile;
  private String filename;

  public Script() {

  }

  public Languages getLanguage() {
    return language;
  }

  public void setLanguage(Languages language) {
    this.language = language;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public File getArchiveFile() {
    return archiveFile;
  }

  public void setArchiveFile(File archiveFile) {
    this.archiveFile = archiveFile;
  }

  public Path extractFromFile(File outputDirectory) throws IOException {

    if (!archiveFile.exists()) {
      throw new IOException("Could not extract plugin file " + filename);
    }
    String zipFileNameWithoutExt = FilenameUtils.removeExtension(archiveFile.getName());
    String targetDirName = StringUtils.removePattern(zipFileNameWithoutExt, "-\\d*\\.\\d*\\.\\d*$");
    Path zipFilePath = archiveFile.toPath();
    String fileToExtract = "scripts/" + filename;
    File outputFile = new File(outputDirectory
        + File.separator + targetDirName, fileToExtract);
    if (outputFile.exists()) {
      if (!outputFile.delete()) {
        log.error("Could not delete file " + outputFile);
      }
    }
    Path outputPath = outputFile.toPath();
    IOHelper.extractFile(zipFilePath, fileToExtract, outputPath);

    return outputPath;
  }

  public Path extractFromFile() throws IOException {
    return extractFromFile(IOHelper.getInstance().getPluginsDir());
  }
}

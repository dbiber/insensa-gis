/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class ExtensionClassLoader extends URLClassLoader {
  private Map<String, String> nativeMap;

  public ExtensionClassLoader(URL[] urls) {
    super(urls);
    nativeMap = new HashMap<>();
  }

  public ExtensionClassLoader(URL[] urls, ClassLoader parent) {
    super(urls, parent);
  }

  public ExtensionClassLoader(URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory) {
    super(urls, parent, factory);
  }

  @Override
  protected Class<?> findClass(String name) throws ClassNotFoundException {
    return super.findClass(name);
  }

  @Override
  public URL findResource(String name) {
    return super.findResource(name);
  }

  @Override
  public Enumeration<URL> findResources(String name) throws IOException {
    return super.findResources(name);
  }

  public void addNativeUrl(String name, String absPath) {
    nativeMap.put(name, absPath);
  }

  public void setNativeMap(Map<String, String> nativeMap) {
    this.nativeMap = nativeMap;
  }

  @Override
  protected String findLibrary(String libname) {
    String library = super.findLibrary(libname);
    if (library == null) {
      return nativeMap.get(System.mapLibraryName(libname));
    }
    return libname;
  }


}

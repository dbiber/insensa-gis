package org.insensa.extensions;

import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatus;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.VariableValue;
import org.insensa.model.storage.IStorageData;

import java.util.List;

public interface IPluginExec extends Runnable, IStorageData {

  String getGroupName();

  String getId();

  void setId(String id);

  int getOrderId();

  void setOrderId(int orderId);

  List<String> getInfoDependencies();

  void setInfoDependencies(List<String> infoDependencies);

  boolean isUsed();

  void setUsed(boolean used);

  void setWorkerStatus(WorkerStatus workerStatus);

  AttributeTable getData();

  String getErrorMessage();

  List<VariableValue> getVariableList();

  void setVariableList(List<VariableValue> vaList);

  IGisFileContainer getParentFileContainer();
}

/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

public class ActionExtension
{
	public static final String TYPE_DIALOG = "dialog";
	
	private String name;
	private String type;
	private String packageName;
	private String className;
	private String priority;


	public ActionExtension()
	{
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setPackageName(String packageStr)
	{
		this.packageName = packageStr;
	}
	
	public void setPriority(String priority)
	{
		this.priority = priority;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void setClassName(String classnameStr)
	{
		this.className = classnameStr;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getClassName()
	{
		return className;
	}
	
	public String getPackageName()
	{
		return packageName;
	}

	public String getPriority()
	{
		return priority;
	}

	public String getType()
	{
		return type;
	}
	
}

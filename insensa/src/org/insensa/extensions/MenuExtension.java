/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import java.util.ArrayList;
import java.util.List;


/**
 * @author dennis
 *
 */
public class MenuExtension
{
	private List<ActionLink> actionLinkList;
	
	private String name;
	private String packageName;
	private String className;
	private String priority;
	

	public MenuExtension()
	{
		actionLinkList = new ArrayList<ActionLink>();
	}
	
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * @return the actionLinkList
	 */
	public List<ActionLink> getActionLinkList()
	{
		return actionLinkList;
	}
	
	
	/**
	 * 
	 * @param mActionLink
	 */
	public void addActionLink(ActionLink mActionLink)
	{
		actionLinkList.add(mActionLink);
	}

	
	/**
	 * @return the packageName
	 */
	public String getPackageName()
	{
		return packageName;
	}
	
	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}
	
	/**
	 * @return the className
	 */
	public String getClassName()
	{
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className)
	{
		this.className = className;
	}

	/**
	 * @return the priority
	 */
	public String getPriority()
	{
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority)
	{
		this.priority = priority;
	}
	
	
}

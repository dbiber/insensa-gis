/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import org.insensa.exceptions.CreateSettingsViewException;
import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.IScriptConfigurationTranslator;
import org.insensa.model.generic.RScriptConfigurationTranslator;
import org.insensa.model.generic.XmlScriptConfigurationTranslator2;
import org.insensa.view.dialogs.connections.IConnectionSetting;
import org.insensa.view.exports.IViewFileExporterSetting;
import org.insensa.view.generic.IGenericSettingsView;
import org.insensa.view.generic.IVariableObjectFactory;
import org.insensa.view.generic.VariableViewPresenter;
import org.insensa.view.generic.swing.GenericSwingDialog;
import org.insensa.view.generic.swing.SwingGenericSettingsView;
import org.insensa.view.generic.swing.SwingObjectFactory;
import org.insensa.view.imports.IViewFileImporterSetting;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;


public class ViewSettingsFactory {
  ExtensionManager extensionManager;


  public ViewSettingsFactory(ExtensionManager extensionManager) {
    this.extensionManager = extensionManager;
  }

  public IPluginSettings getSettingsPlugin(IPluginExec exec)
      throws CreateSettingsViewException,
      ClassNotFoundException,
      IllegalAccessException,
      InstantiationException {
    ServiceList list = null;
    switch (exec.getGroupName()) {
      case "infos":
        list = extensionManager.getInfoReaderServices().get(exec.getId());
        break;
      case "connections":
        list = extensionManager.getConnectionServices().get(exec.getId());
        break;
      case "options":
        list = extensionManager.getOptionServices().get(exec.getId());
        break;
      case "infoConnections":
        list = extensionManager.getInfoConnectionServices().get(exec.getId());
        break;
    }
    ClassLoader cl = this.extensionManager.getUrlClassLoader();

    if (list == null) {
      return null;
    }
    Service service = list.getService(ServiceType.SETTINGS);
    if (service == null) {
      return null;
    } else {
      if (service.getScript() != null) {
        return createGenericSwingDialog(service);
      }
      Class viewConSetting = cl.loadClass(service.getPackageName() + "." + service.getClassName());
      return (IPluginSettings) viewConSetting.newInstance();
    }
  }

//  public IConnectionSetting getViewConnnectionSetting(String connectionName)
//      throws ClassNotFoundException, InstantiationException,
//      IllegalAccessException, CreateSettingsViewException {
//    ClassLoader cl = this.extensionManager.getUrlClassLoader();// InsensaGIS.class.getClassLoader();
//
//    ServiceList list = this.extensionManager.getConnectionServices().get(connectionName);
//    if (list == null)
//      return null;
//    Service service = list.getService(ServiceType.SETTINGS);
//    if (service == null)
//      return null;
//    else {
//      if (service.getScript() != null) {
//        return createGenericSwingDialog(service);
//      }
//      Class viewConSetting = cl.loadClass(service.getPackageName() + "." +
//          service.getClassName());
//      return (IConnectionSetting) viewConSetting.newInstance();
//    }
//  }

  public IViewFileExporterSetting getViewFileExporter(String fileExporter)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException, CreateSettingsViewException {
    ClassLoader cl = this.extensionManager.getUrlClassLoader();

    ServiceList list = this.extensionManager.getFileExporterServices().get(fileExporter);
    if (list == null)
      return null;
    Service service = list.getService(ServiceType.SETTINGS);
    if (service == null)
      return null;
    else {
      if (service.getScript() != null) {
        return createGenericSwingDialog(service);
      }
      Class viewFileExporterSetting = cl.loadClass(service.getPackageName() + "." +
          service.getClassName());
      return (IViewFileExporterSetting) viewFileExporterSetting.newInstance();
    }
  }


  /**
   * @return a new instance of a found IViewFileImporterSetting
   * or null if no class is found
   */
  public Optional<IViewFileImporterSetting> getViewFileImporter(String fileImporter)
      throws CreateSettingsViewException {
    ClassLoader cl = this.extensionManager.getUrlClassLoader();// InsensaGIS.class.getClassLoader();

    ServiceList list = this.extensionManager.getFileImporterServices().get(fileImporter);
    if (list == null) {
      return Optional.empty();
    }
    Service service = list.getService(ServiceType.SETTINGS);
    if (service == null) {
      return Optional.empty();
    } else {
      if (service.getScript() != null) {
        return Optional.ofNullable(createGenericSwingDialog(service));
      }
      //TODO: This does not call the constructor nor init private member!!!
      Class viewFileImporterSetting;
      try {
        viewFileImporterSetting = cl.loadClass(service.getPackageName() +
            "." + service.getClassName());
      } catch (ClassNotFoundException e) {
        throw new CreateSettingsViewException("Could not load class for service \"" +
            service.getName() + "\"", e);
      }
      try {
        return Optional.of((IViewFileImporterSetting) viewFileImporterSetting.newInstance());
      } catch (InstantiationException | IllegalAccessException e) {
        throw new CreateSettingsViewException("Could not instantiate class for service \"" +
            service.getName() + "\"", e);
      }
    }
  }

//  public IInfoReaderSetting getViewInfoReaderSetting(String infoReaderName)
//      throws ClassNotFoundException, InstantiationException,
//      IllegalAccessException, CreateSettingsViewException {
//    ClassLoader cl = this.extensionManager.getUrlClassLoader();
//
//    ServiceList list = this.extensionManager.getInfoReaderServices().get(infoReaderName);
//    if (list == null)
//      return null;
//    Service service = list.getService(ServiceType.SETTINGS);
//    if (service == null)
//      return null;
//    else {
//      if (service.getScript() != null) {
//        return createGenericSwingDialog(service);
//      }
//      Class viewInfoSetting = cl.loadClass(service.getPackageName() +
//          "." + service.getClassName());
//      return (IInfoReaderSetting) viewInfoSetting.newInstance();
//    }
//  }

//  public IOptionSetting getViewOptionSetting(String optionName)
//      throws ClassNotFoundException, InstantiationException,
//      IllegalAccessException, CreateSettingsViewException {
//    ServiceList list = this.extensionManager.getOptionServices().get(optionName);
//    ClassLoader cl = this.extensionManager.getUrlClassLoader();


//    if (list == null)
//      return null;
//    Service service = list.getService(ServiceType.SETTINGS);
//    if (service == null)
//      return null;
//    else {
//      if (service.getScript() != null) {
//        return (IOptionSetting) createGenericSwingDialog(service);
//      }
//      Class viewOptionSetting = cl.loadClass(service.getPackageName() +
//          "." + service.getClassName());
//      return (IOptionSetting) viewOptionSetting.newInstance();
//    }
//  }

  public GenericSwingDialog createGenericSwingDialog(Service service)
      throws CreateSettingsViewException {
    IScriptConfigurationTranslator translator;
    Path absFilePath;
    try {
      absFilePath = service.getScript().extractFromFile();
    } catch (IOException e) {
      throw new CreateSettingsViewException("Error extracting file \"" +
          service.getScript().getFilename() +
          "\" from script: ", e);
    }
    if (service.getScript().getLanguage() == Languages.R) {
      translator = new RScriptConfigurationTranslator(absFilePath.toFile());
    } else if (service.getScript().getLanguage() == Languages.XML) {
      translator = new XmlScriptConfigurationTranslator2(absFilePath.toFile());
      try {
        translator.parse(absFilePath.toFile());
      } catch (ScriptParseException e) {
        throw new CreateSettingsViewException("Could not parse script \"" +
            absFilePath.toString() +
            "\"", e);
      }
    } else {
      throw new CreateSettingsViewException("Script language \"" +
          service.getScript().getLanguage().toString() +
          "\" not supported");
    }
    //    try {
    //      translator.parse(absFilePath.toFile());
    //    } catch (ScriptParseException e) {
    //      throw new CreateSettingsViewException("Could not parse script \"" +
    //          absFilePath.toString() +
    //          "\"", e);
    //    }

    IGenericSettingsView settingsView = new SwingGenericSettingsView();
    IVariableObjectFactory factory = new SwingObjectFactory();

    VariableViewPresenter presenter = new VariableViewPresenter(translator, settingsView, factory);
    GenericSwingDialog genericSwingDialog = new GenericSwingDialog(presenter);
    if (service.getHelp() != null) {
      genericSwingDialog.setHelpId(service.getHelp().getId());
    }
    return genericSwingDialog;
  }
}

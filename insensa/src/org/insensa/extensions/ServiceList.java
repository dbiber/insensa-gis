/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.extensions;

import org.insensa.GisFileType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ServiceList {
  private List<Service> serviceList = new ArrayList<>();


  public ServiceList() {

  }

  public void add(Service service) {
    this.serviceList.add(service);
  }

  /**
   * @param serviceType type of service to return
   * @return returns the service with type serviceType. If there are
   * more services with that {@link ServiceType},
   * the service with the highest {@link ServicePriority}
   * will be returned.
   */
  public Service getService(ServiceType serviceType) {
    Service serviceToRet = null;
    for (Service iService: this.serviceList) {
      if (iService.getType() == serviceType) {
        if (serviceToRet == null || serviceToRet.getPriority().lowerThen(iService.getPriority())) {
          serviceToRet = iService;
        }
      }
    }
    return serviceToRet;
  }

  /**
   * @param serviceType type of service to return
   * @param serviceName name of the service
   * @return returns the service with type serviceType and name serviceName.
   * If there are more services with that {@link ServiceType} and serviceName,
   * the service with the highest {@link ServicePriority}
   * will be returned.
   */
  public Service getService(ServiceType serviceType, String serviceName) {
    Service serviceToRet = null;
    for (Service iService: this.serviceList) {
      if (iService.getType() == serviceType && iService.getName().equals(serviceName)) {
        if (serviceToRet == null ||
            serviceToRet.getPriority().lowerThen(iService.getPriority())) {
          serviceToRet = iService;
        }
      }
    }
    return serviceToRet;
  }

  public List<Service> getServices(ServiceType serviceType) {
    List<Service> tmpList = new ArrayList<>();
    for (Service iService: this.serviceList) {
      if (iService.getType() == serviceType) {
        tmpList.add(iService);
      }
    }
    return tmpList;
  }

  public List<Service> getServices(ServiceType serviceType, GisFileType gisFileType) {
    List<Service> tmpList = new ArrayList<>();
    for (Service iService: this.serviceList) {
      if (iService.getType() == serviceType && iService.getGisType() == gisFileType) {
        tmpList.add(iService);
      }
    }
    return tmpList;
  }

  public int size() {
    return this.serviceList.size();
  }

}

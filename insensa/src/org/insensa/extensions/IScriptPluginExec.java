package org.insensa.extensions;

import org.insensa.model.generic.IVariable;

import java.util.Map;

public interface IScriptPluginExec extends IPluginExec {
  Map<String, IVariable> getVariables();

  void setVariables(Map<String, IVariable> variableMap);
}

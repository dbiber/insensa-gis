package org.insensa.extensions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ServicePriority {

  LOWEST(0,"lowest"),
  LOW(1,"low"),
  MEDIUM(2, "medium"),
  HIGH(3,"high"),
  HIGHEST(4, "highest");

  private static final Logger log = LoggerFactory.getLogger(ServicePriority.class);
  private int order;
  private String name;

  ServicePriority(int order, String name) {
    this.order = order;
    this.name = name;
  }

  public static ServicePriority fromName(String name) {
    for(ServicePriority sType : ServicePriority.values()) {
      if(sType.value().equalsIgnoreCase(name)) {
        return sType;
      }
    }
    log.debug("Could not read priority: " +
        "\"" + name + "\" - " +
        "Default to \"medium\"");
    return MEDIUM;
  }

  public String value() {
    return name;
  }

  public int order() {
    return order;
  }

  public boolean higherThen(ServicePriority prio) {
    return order > prio.order();
  }

  public boolean lowerThen(ServicePriority prio) {
    return order < prio.order();
  }
}

package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.PluginDependencyUtils;
import org.insensa.WorkerStatus;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.VariableValue;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.model.storage.AttributeTableSaver;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractInfoConnection
    implements InfoConnection {

  private static final String ORDER_ID = "orderId";
  private static final String E_OUTPUT_PATH = "outputPath";
  private static final String E_OUTPUT_NAME = "outputName";
  private static final Logger log = LoggerFactory.getLogger(AbstractInfoConnection.class);
  private static final String USED = "used";
  private static final String OUTPUT_SET = "outputSet";
  private static final String OUTPUT_NAME = "outputName";
  protected List<IGisFileContainer> sourceFileList = new ArrayList<>();
  protected List<String> fileNameList = new ArrayList<>();
  protected WorkerStatus workerStatus;
  protected int orderId = 0;
  protected boolean used = false;
  private String id;
  private IGisFileContainer parentFileContainer;
  private IGisFileSet outputFileSet;
  private List<String> infoDependencies = new ArrayList<>();
  private List<String> infoConnectionDependencies = new ArrayList<>();

  @Override
  public List<IGisFileContainer> getSourceFileList() {
    return sourceFileList;
  }

  @Override
  public void setSourceFileList(List<? extends IGisFileContainer> sourceFileList) {
    this.sourceFileList.clear();
    this.fileNameList.clear();
    for (IGisFileContainer iterFile: sourceFileList) {
      this.fileNameList.add(iterFile.getOutputFileName());
    }
    this.sourceFileList.addAll(sourceFileList);
  }

  @Override
  public List<String> getInfoDependencies() {
    return infoDependencies;
  }

  @Override
  public void setInfoDependencies(List<String> infoDependencies) {
    this.infoDependencies = infoDependencies;
  }

  @Override
  public List<String> getInfoConnectionDependencies() {
    return infoConnectionDependencies;
  }

  @Override
  public void setInfoConnectionDependencies(List<String> infoConnectionDependencies) {
    this.infoConnectionDependencies = infoConnectionDependencies;
  }

  @Override
  public String getGroupName() {
    return "infoConnections";
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public int getOrderId() {
    return this.orderId;
  }

  @Override
  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  @Override
  public boolean isUsed() {
    return this.used;
  }

  @Override
  public void setUsed(boolean used) {
    this.used = used;
  }

  @Override
  public void setWorkerStatus(WorkerStatus workerStatus) {
    this.workerStatus = workerStatus;
  }

  @Override
  public AttributeTable getData() {
    AttributeTableSaver attributeTableSaver = new AttributeTableSaver();
    save(attributeTableSaver);
    return attributeTableSaver.getTable();
  }

  @Override
  public String getErrorMessage() {
    return null;
  }

  @Override
  public List<VariableValue> getVariableList() {
    return Collections.emptyList();
  }

  @Override
  public void setVariableList(List<VariableValue> vaList) {
  }

  @Override
  public IGisFileContainer getParentFileContainer() {
    return parentFileContainer;
  }

  @Override
  public void setParentFileContainer(IGisFileContainer parentFileContainer) {
    this.parentFileContainer = parentFileContainer;
  }

  @Override
  public void run() {
    if (sourceFileList.isEmpty()) {
      if (this.workerStatus != null) {
        this.workerStatus.endPause();
        this.workerStatus.refreshPercentage(100.0F);
        this.workerStatus.setProgressName("ERROR: No Source Files available");
        this.workerStatus.errorProcess();
      }
    } else {
      try {
        this.parentFileContainer.setLock(true);
        for (IGisFileContainer iterFile: sourceFileList) {
          iterFile.setLock(true);
        }
        readInfos();
        for (IGisFileContainer iterFile: sourceFileList) {
          iterFile.removeFileObserver(this);
          iterFile.setLock(false);
        }
        this.parentFileContainer.getGisFileInformationStorage().refreshPluginExec(this);
        // targetFileSet.getFileSetProperties().refreshConnectionFileChanger(this);
        if (this.workerStatus != null) {
          this.workerStatus.endPause();
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.endProgress();
        }
      } catch (Exception e) {
        log.error("UNKNOWN", e);
        if (this.workerStatus != null) {
          this.workerStatus.endPause();
          this.workerStatus.refreshPercentage(100.0F);
          if (e.getMessage() != null) {
            this.workerStatus.setProgressName(e.getMessage());
          } else {
            this.workerStatus.setProgressName(e.toString());
          }
          this.workerStatus.errorProcess();
          e.printStackTrace();
        }
      } finally {
        this.parentFileContainer.setLock(false);
        for (IGisFileContainer iterFile: sourceFileList) {
          iterFile.setLock(false);
        }

      }
    }
  }

  /**
   * Restores the date of current ConnectionFileChanger
   *
   * {@link AbstractConnection} restores :
   * <ul>
   * <li>bool <b>used</b></li>
   * <li>bool <b>orderId</b></li>
   * <li>bool <b>outputFileName</b></li>
   * </ul>
   *
   * And if the Connection was used, it iterated over all source files
   * and calls {@link #restoreFile(IGisFileContainer, ISavableRestorer)}:
   */
  @Override
  public void restore(ISavableRestorer restorer) {
    used = restorer.loadBoolean(USED).orElseThrow(RuntimeException::new);
    orderId = restorer.loadInteger(ORDER_ID).get();
    if (!used) {
      restorer.loadCollection("file", restorer1 -> {
        String path = restorer1.loadString(E_OUTPUT_PATH).get();
        String name = restorer1.loadString(E_OUTPUT_NAME).get();
        IGisFileContainer container = findGisFileContainer(path, name);
        if (container == null) {
          log.debug("File \"" + path + name + "\"not found for Connection");
        } else {
          restoreFile(container, restorer1);
        }
      });
    }
  }

  public IGisFileContainer findGisFileContainer(String path, String name) {
    for (IGisFileContainer container: sourceFileList) {
      if ((container.getPathRelativeToProject() + container.getOutputFileName()).equals(path + name)) {
        return container;
      }
    }
    return null;
  }

  /**
   * Will be called for every source file of this {@link ConnectionFileChanger}.
   */
  protected abstract void restoreFile(IGisFileContainer container, ISavableRestorer restorer);

  @Override
  public void save(ISavableSaver saver) {
    saver.save(USED, used);
    saver.save(ORDER_ID, orderId);
    saver.save(OUTPUT_SET, this.parentFileContainer.getName());
    for (IGisFileContainer fileContainer: sourceFileList) {
      saver.save("file", saver1 -> {
        saver1.save(E_OUTPUT_PATH, fileContainer.getPathRelativeToProject());
        saver1.save(E_OUTPUT_NAME, fileContainer.getOutputFileName());
        saveFile(fileContainer, saver1);
      });
    }
  }

  protected abstract void saveFile(IGisFileContainer container, ISavableSaver saver);

  public List<String> getFileNameList() {
    return fileNameList;
  }

  protected void solveDependencies() throws IOException {
    PluginDependencyUtils pluginDependencyUtils = new PluginDependencyUtils(
        sourceFileList,
        this.workerStatus,
        this);
    pluginDependencyUtils.solveDependencies();
  }

  @Override
  public IGisFileSet getOutputFileSet() {
    return this.outputFileSet;
  }

  @Override
  public void setOutputFileSet(IGisFileSet outputSet) {
    this.outputFileSet = outputSet;
  }

  @Override
  public String toString() {
    return this.getId();
//    String connName = this.getId();
//    return connName.substring(0, 1).toUpperCase() + connName.substring(1);
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.gdal.gdal.Band;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;


public class CMultiplication extends AbstractRasterConnection {

  @Override
  public void write() throws IOException, JDOMException, GisFileException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(getId());
    }

    if (this.check() == false)
      throw new IOException("The files are incompatible");
    RasterFileContainer endFile;
    if (this.outputFileContainer == null)
      endFile = new RasterFileContainer(this.outputFileSet, this.outputFileName,
          this.outputFileSet.getPath() + this.outputFileSet.getName(), null);
    else
      endFile = (RasterFileContainer) this.outputFileContainer;
    RasterFileContainer tmpFile2 = (RasterFileContainer) getSourceFileList().get(0);

    endFile.getGisFile().createNewFile(tmpFile2.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, Float.MAX_VALUE);
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) getSourceFileList().get(0)).getNRows());
    int xSize = ((RasterFileContainer) getSourceFileList().get(0)).getBand(RasterFileAccess.READ_ONLY).getXSize();
    float[] outputArray = new float[xSize];
    float[][] floatFloatArray = new float[getSourceFileList().size()][xSize];
    int rowCnt = ((RasterFileContainer) getSourceFileList().get(0)).getNRows();
    int valueCnt = 0;
    float sumValue = 0.0F;
    float noDataValue = Float.MAX_VALUE;
    Band bandOut = endFile.getBand(RasterFileAccess.UPDATE);
    for (int i = 0; i < rowCnt; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      for (int j = 0; j < getSourceFileList().size(); j++)
        ((RasterFileContainer) getSourceFileList().get(j)).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, floatFloatArray[j]);

      for (int j = 0; j < xSize; j++) {
        valueCnt = 0;
        sumValue = 1;

        for (int k = 0; k < getSourceFileList().size(); k++) {
          float tmpVal = floatFloatArray[k][j];
          if (tmpVal == ((RasterFileContainer) getSourceFileList().get(k)).getNoDataValue()) {
          } else {
            valueCnt++;
            sumValue *= tmpVal;
          }
        }
        if (valueCnt == 0)
          outputArray[j] = noDataValue;
        else {
          outputArray[j] = sumValue;
        }
      }
      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
    }
    for (int j = 0; j < getSourceFileList().size(); j++)
      ((RasterFileContainer) getSourceFileList().get(j)).unlock();
    endFile.unlock();
    endFile.refresh();
    this.used = true;
  }

  @Override
  public void writeToContainer(RasterFileContainer file) throws IOException {

  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IGisFileContainerFactory;
import org.insensa.IRasterGisFile;
import org.insensa.ParentModule;
import org.insensa.RasterFileContainer;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.List;


public abstract class AbstractRasterConnection extends AbstractConnection {

  @Override
  public void write() throws IOException, JDOMException {
  }

  public abstract void writeToContainer(RasterFileContainer file) throws IOException;

  @Override
  public List<RasterFileContainer> getSourceFileList() {
    return (List<RasterFileContainer>) super.getSourceFileList();
  }

  /**
   *
   * Prepares a target Container to store the newly created file.
   * This will not create the physical file itself,
   * this has to be done in {@link #writeToContainer(RasterFileContainer)}
   *
   * @see IGisFileContainerFactory#create(ParentModule, String, String, OptionFileChanger)
   * @see #solveDependencies()
   */
  protected RasterFileContainer prepareOutputFile(
      IGisFileContainerFactory fileContainerFactory) throws IOException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.toString());
    }

    if (!this.check()) {
      throw new IOException("The files are incompatible");
    }

    this.solveDependencies();

    IGisFileContainer targetFileContainer;
    if (this.outputFileContainer == null) {
      targetFileContainer = fileContainerFactory.create(
          this.outputFileSet,//parentModule
          this.outputFileName,
          this.outputFileSet.getPath()
              + this.outputFileSet.getName()
              + File.separator
              + this.outputFileName,
          null);
    } else {
      targetFileContainer = this.outputFileContainer;
    }

    IGisFileContainer tmpFile2 = getSourceFileList().get(0);

    //FIXME Default, This was stupid
//    if (!targetFileContainer.getGisFile().createNewFile(tmpFile2.getGisFile(),
//        DataTypeHelper.RasterDataType.GDT_Float32, -Float.MAX_VALUE)) {
//      throw new IOException("Error Creating new File");
//    }
    return (RasterFileContainer) targetFileContainer;
  }

  /**
   * <ul>
   * <li>Checks source file compatibility</li>
   * <li>Creates output file formatted based on source files</li>
   * <li>calls #writeToContainer(ouputFile)</li>
   * </ul>
   */
  @Override
  public void write(IGisFileContainerFactory fileContainerFactory) throws IOException {
    if (this instanceof InfoConnection) {
      ((InfoConnection) this).readInfos();
    } else {
      RasterFileContainer endFile =
          prepareOutputFile(fileContainerFactory);
      this.writeToContainer(endFile);
    }
    this.used = true;
  }

  protected boolean check() throws IOException {
    if (getSourceFileList().isEmpty()) {
      throw new IOException("rasterFileList is empty");
    }
    if (getSourceFileList().size() < 2) {
      throw new IOException("rasterFileList size is: " + getSourceFileList().size());
    }
    RasterFileContainer rasterFileInformation = getSourceFileList().get(0);
    IRasterGisFile rasterGisFile = rasterFileInformation.getGisFile();
    float cellSize = rasterGisFile.getCellSize();
    float xllCorner = rasterGisFile.getXllCorner();
    float yllCorner = rasterGisFile.getYllCorner();
    int rows = rasterGisFile.getNRows();
    int cols = rasterGisFile.getNCols();
    for (IGisFileContainer iGisFile: getSourceFileList()) {
//      IGisFileContainer  iFile =  iGisFile;
      IRasterGisFile iFile = (IRasterGisFile) iGisFile.getGisFile();
      if (iFile == null)
        throw new IOException("one or more source files are missing");
      // return false;
      if (cellSize != iFile.getCellSize())
        throw new IOException("cellsize \"" + cellSize + "\" is different from \"" + iFile.getCellSize() + "\"");
      if (xllCorner != iFile.getXllCorner())
        throw new IOException("xllCorner \"" + xllCorner + "\" is different from \"" + iFile.getXllCorner() + "\"");
      if (yllCorner != iFile.getYllCorner())
        throw new IOException("yllCorner \"" + yllCorner + "\" is different from \"" + iFile.getYllCorner() + "\"");
      if (rows != iFile.getNRows())
        throw new IOException("rows \"" + rows + "\" is different from \"" + iFile.getNRows() + "\"");
      if (cols != iFile.getNCols())
        throw new IOException("cols \"" + cols + "\" is different from \"" + iFile.getNCols() + "\"");
    }
    return true;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CoefficientOfVariation extends AbstractRasterConnection {

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    return depList;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {

  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {

  }

  public void writeToContainer(RasterFileContainer file) throws IOException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / getSourceFileList().get(0).getGisFile().getNRows();
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName(this.toString());
    }

    List<RasterFileContainer> gisFileList = getSourceFileList();
    file.getGisFile().createNewFile(gisFileList.get(0).getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, -Float.MAX_VALUE);

    float noDataValue = -Float.MAX_VALUE;

    int xSize = getSourceFileList().get(0).getGisFile().getNCols();
    float[][] doubleDoubleArray;
    List<RasterFileContainer> newList = new ArrayList<>();
    newList.addAll(getSourceFileList());

    doubleDoubleArray = new float[newList.size()][];
    float[] outputArray = new float[xSize];
    int rowCount;
    float sumValue = 0.0F;
    float sqrSum = 0.0F;
    int valueCount = 0;
    double sum1, sum2, stdDev, mean;

    // Schleife 1 Anzahl der Zeilen
    rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < newList.size(); j++) {
        doubleDoubleArray[j] = newList.get(j).getGisFile().readRaster(0, i, xSize, 1);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        valueCount = 0;
        sumValue = 0;
        sqrSum = 0;
        // schleife 4 Anzahl der Dateien
        for (int k = 0; k < newList.size(); k++) {
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == newList.get(k).getGisFile().getNoDataValue()) {
          } else {
            valueCount++;
            sumValue += tmpValue;
            sqrSum += tmpValue * tmpValue;
          }
        }

        // s4-ENDE
        if (valueCount == 0)
          outputArray[j] = noDataValue;
        else {
          sum1 = sqrSum / valueCount;
          sum2 = Math.pow((sumValue / valueCount), 2.0);
          mean = sumValue / valueCount;
          stdDev = Math.sqrt(sum1 - sum2);
          outputArray[j] = (float) (stdDev / mean);
        }
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
    }// Schleife1-Ende
    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();
  }// writeToContainer End

}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.connections;


import org.insensa.GisFileContainerFactory;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileContainerFactory;
import org.insensa.IGisFileSet;
import org.insensa.PluginDependencyUtils;
import org.insensa.WorkerStatus;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.VariableValue;
import org.insensa.model.storage.AttributeTableSaver;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractConnection implements ConnectionFileChanger {
  public static final String ORDER_ID = "orderId";
  public static final String E_OUTPUT_PATH = "outputPath";
  public static final String E_OUTPUT_NAME = "outputName";
  private static final Logger log = LoggerFactory.getLogger(AbstractConnection.class);
  private static final String USED = "used";
  private static final String OUTPUT_SET = "outputSet";
  private static final String OUTPUT_NAME = "outputName";
  private static final String XML_ELEMENT_FILE = "file";
  protected String outputFileName = "out.tif";
  protected boolean used = false;
  protected int orderId = 0;
  protected IGisFileSet outputFileSet = null;
  protected IGisFileContainer outputFileContainer = null;
  protected List<String> fileNameList = new ArrayList<>();
  protected WorkerStatus workerStatus;
  private List<IGisFileContainer> gisFileList = new ArrayList<>();
  private List<String> infoConnectionDependencies = new ArrayList<>();
  private List<String> infoDependencies = new ArrayList<>();
  private String id;


  @Override
  public void write(IGisFileContainerFactory fileContainerFactory)
      throws IOException {
  }

  @Override
  public String getErrorMessage() {
    return null;
  }

  @Override
  public List<VariableValue> getVariableList() {
    return Collections.emptyList();
  }

  @Override
  public void setVariableList(List<VariableValue> vaList) {
  }

  /**
   * Restores the date of current ConnectionFileChanger
   *
   * {@link AbstractConnection} restores :
   * <ul>
   * <li>bool <b>used</b></li>
   * <li>bool <b>orderId</b></li>
   * <li>bool <b>outputFileName</b></li>
   * </ul>
   *
   * And if the Connection was used, it iterated over all source files
   * and calls {@link #restoreFile(IGisFileContainer, ISavableRestorer)}:
   */
  @Override
  public void restore(ISavableRestorer restorer) {
    used = restorer.loadBoolean(USED).orElseThrow(RuntimeException::new);
    orderId = restorer.loadInteger(ORDER_ID).get();
    outputFileName = restorer.loadString(OUTPUT_NAME).get();
//    if (!used) {
    restorer.loadCollection("file", restorer1 -> {
      String path = restorer1.loadString(E_OUTPUT_PATH).get();
      String name = restorer1.loadString(E_OUTPUT_NAME).get();
      IGisFileContainer container = null;
//      if (!used) {
        container = findGisFileContainer(path, name);
//      } else {
////        new GisFileContainerFactory().create(parent, fullPath, outputName, serialize, workerStatus);
////        new GisFileContainerFactory().create(parent, outputFileName, fullName, OptionFileChanger);
//      }
      if (container == null) {
        log.debug("File \"" + path + name + "\"not found for Connection");
      } else {
        restoreFile(container, restorer1);
      }
    });
//    }
  }

  /**
   * Will be called for every source file of this {@link ConnectionFileChanger}.
   */
  protected abstract void restoreFile(IGisFileContainer container, ISavableRestorer restorer);

  public IGisFileContainer findGisFileContainer(String path, String name) {
    for (IGisFileContainer container : gisFileList) {
      if ((container.getPathRelativeToProject() + container.getOutputFileName()).equals(path + name)) {
        return container;
      }
    }
    return null;
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save(USED, used);
    saver.save(ORDER_ID, orderId);
    saver.save(OUTPUT_SET, this.outputFileSet.getName());
    saver.save(OUTPUT_NAME, outputFileName);
    for (IGisFileContainer fileContainer : gisFileList) {
      saver.save("file", saver1 -> {
        saver1.save(E_OUTPUT_PATH, fileContainer.getPathRelativeToProject());
        saver1.save(E_OUTPUT_NAME, fileContainer.getOutputFileName());
        saveFile(fileContainer, saver1);
      });
    }
  }

  protected abstract void saveFile(IGisFileContainer container, ISavableSaver saver);

  @Override
  public AttributeTable getData() {
    AttributeTableSaver attributeTableSaver = new AttributeTableSaver();
    save(attributeTableSaver);
    return attributeTableSaver.getTable();
  }

  public List<String> getFileNameList() {
    return this.fileNameList;
  }

  @Override
  public int getOrderId() {
    return this.orderId;
  }

  @Override
  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  @Override
  public List<String> getInfoDependencies() {
    return infoDependencies;
  }

  @Override
  public void setInfoDependencies(List<String> infoDependencies) {
    this.infoDependencies = infoDependencies;
  }

  @Override
  public List<String> getInfoConnectionDependencies() {
    return infoConnectionDependencies;
  }

  @Override
  public void setInfoConnectionDependencies(List<String> infoConnectionDependencies) {
    this.infoConnectionDependencies = infoConnectionDependencies;
  }

  @Override
  public IGisFileContainer getOutputFileContainer() {
    return this.outputFileContainer;
  }

  @Override
  public void setOutputFileContainer(IGisFileContainer outputGisFile) {
    this.outputFileContainer = outputGisFile;
  }

  @Override
  public String getOutputFileName() {
    return this.outputFileName;
  }

  @Override
  public void setOutputFileName(String outputFileName) {
    if (outputFileName.endsWith(".tif")) {
      this.outputFileName = outputFileName;
    } else {
      this.outputFileName = (outputFileName + ".tif");
    }
  }

  @Override
  public IGisFileSet getOutputFileSet() {
    return this.outputFileSet;
  }

  @Override
  public void setOutputFileSet(IGisFileSet outputSet) {
    this.outputFileSet = outputSet;
  }

  @Override
  public List<? extends IGisFileContainer> getSourceFileList() {
    return gisFileList;
  }

  public void setSourceFileList(List<? extends IGisFileContainer> sourceFileList) {
    gisFileList.clear();
    this.fileNameList.clear();
    for (IGisFileContainer iFile : sourceFileList) {
      this.fileNameList.add(iFile.getOutputFileName());
    }
    gisFileList.addAll(sourceFileList);
  }

  @Override
  public boolean isUsed() {
    return this.used;
  }

  @Override
  public void setUsed(boolean used) {
    this.used = used;
  }

  @Override
  public void notifyDelete(IGisFileContainer subject) {
    int lIndex = gisFileList.lastIndexOf(subject);
    if (lIndex != -1) {
      gisFileList.set(lIndex, null);
    }
  }

  @Override
  public void run() {
    if (gisFileList.isEmpty()) {
      if (this.workerStatus != null) {
        this.workerStatus.endPause();
        this.workerStatus.refreshPercentage(100.0F);
        this.workerStatus.setProgressName("ERROR: No Source Files available");
        this.workerStatus.errorProcess();
      }
    } else {
      try {
        outputFileContainer.setLock(true);
        for (IGisFileContainer iterFile : gisFileList) {
          iterFile.setLock(true);
        }
        this.write(new GisFileContainerFactory());
        this.write();
        outputFileContainer.setLock(false);
        for (IGisFileContainer iterFile : gisFileList) {
          iterFile.removeFileObserver(this);
          iterFile.setLock(false);
        }
        this.outputFileContainer.getGisFileInformationStorage().refreshPluginExec(this);
        // targetFileSet.getFileSetProperties().refreshConnectionFileChanger(this);
        if (this.workerStatus != null) {
          this.workerStatus.endPause();
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.endProgress();
        }
      } catch (Exception e) {
        log.error("UNKNOWN", e);
        if (this.workerStatus != null) {
          this.workerStatus.endPause();
          this.workerStatus.refreshPercentage(100.0F);
          if (e.getMessage() != null) {
            this.workerStatus.setProgressName(e.getMessage());
          } else {
            this.workerStatus.setProgressName(e.toString());
          }
          this.workerStatus.errorProcess();
          e.printStackTrace();
        }
        if (this.getOutputFileContainer() != null && this.outputFileContainer.getGisFile().exists()) {
          this.outputFileContainer.getGisFile().delete();
        }
      } finally {
        this.outputFileContainer.setLock(false);
        for (IGisFileContainer iterFile : gisFileList) {
          iterFile.setLock(false);
        }
      }
    }
  }

  @Override
  public void setWorkerStatus(WorkerStatus workerStatus) {
    this.workerStatus = workerStatus;
  }

  protected void solveDependencies() throws IOException {
    PluginDependencyUtils pluginDependencyUtils = new PluginDependencyUtils(
        gisFileList,
        this.workerStatus,
        this);
    pluginDependencyUtils.solveDependencies();
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return this.getId();
//    String connName = this.getId();
//    return connName.substring(0, 1).toUpperCase() + connName.substring(1);
  }

  @Override
  public String getGroupName() {
    return "connections";
  }

  @Override
  public IGisFileContainer getParentFileContainer() {
    return getOutputFileContainer();
  }
}

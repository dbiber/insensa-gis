/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CDivision extends AbstractRasterConnection {
  public final static int SET_NODATA = 0;
  public final static int SET_DUMMY = 1;
  private static final String DIVISION = "division";
  // dividend/divisor
  private RasterFileContainer dividend = null;
  private RasterFileContainer divisor = null;
  private float dummyValue = 0.0F;
  private int divByZeroOption = SET_NODATA;


  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("divByZeroOption", divByZeroOption);
    if (this.divByZeroOption == SET_DUMMY)
      saver.save("dummy", this.dummyValue);
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
    if (divisor != null) {
      if (divisor == container) {
        saver.save("divisor", true);
      } else if (dividend == container) {
        saver.save("dividend", true);
      }
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    divByZeroOption = restorer.loadInteger("divByZeroOption").orElse(0);
    if (divByZeroOption == SET_DUMMY) {
      dummyValue = restorer.loadFloat("dummy").orElse(0.0F);
    }
  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
    if (restorer.loadString("divisor") != null) {
      this.divisor = (RasterFileContainer) container;
    } else if (restorer.loadString("dividend") != null) {
      this.dividend = (RasterFileContainer) container;
    }
  }

  @Override
  public AttributeTable getData() {
    AttributeTable dataMap = super.getData();
    if (this.outputFileContainer == null) {
      return dataMap;
    } else {
      if (divisor != null) {
        dataMap.put("Filename (divisor)", divisor.getOutputFileName());
      } else if (dividend != null) {
        dataMap.put("Filename (dividend)", dividend.getOutputFileName());
      } else {
        for (IGisFileContainer container : getSourceFileList()) {
          dataMap.put("Filename", container.getOutputFileName());
        }
      }
    }
    return dataMap;
  }

  public int getDivByZeroOption() {
    return this.divByZeroOption;
  }

  public void setDivByZeroOption(int divByZeroOption) {
    this.divByZeroOption = divByZeroOption;
  }

  public IGisFileContainer getDivisor() {
    return this.divisor;
  }

  public void setDivisor(IGisFileContainer divisor) {
    this.divisor = getSourceFileList().get(getSourceFileList().indexOf(divisor));
  }

  public float getDummyValue() {
    return this.dummyValue;
  }

  public void setDummyValue(float dummyValue) {
    this.dummyValue = dummyValue;
  }

  public void setDividend(IGisFileContainer dividend) {
    this.dividend = getSourceFileList().get(getSourceFileList().indexOf(dividend));
  }

//  @Override
//  public void write() throws IOException, JDOMException, GisFileException {
//    if (this.workerStatus != null) {
//      this.workerStatus.startProcess();
//      this.workerStatus.setProgressName(DIVISION);
//    }
//
//    if (!this.check())
//      throw new IOException("The files are incompatible");
//    RasterFileContainer endFile;
//    if (this.outputFileContainer == null) {
//      endFile = new RasterFileContainer(this.outputFileSet, this.outputFileName, this.outputFileSet.getPath() + this.outputFileSet.getName(), null);
//    } else {
//      endFile = (RasterFileContainer) this.outputFileContainer;
//    }
//    RasterFileContainer tmpFile2 = ((RasterFileContainer) gisFileList.get(0));
//
//
//
//    this.writeInFile(endFile);
//    this.used = true;
//  }

  @Override
  public void writeToContainer(RasterFileContainer file) throws IOException {

    file.getGisFile().createNewFile(getSourceFileList().get(0).getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, Float.MAX_VALUE);

    if (this.divisor == null) {
      throw new IOException(file.getName() + ": CDivision: No divisor selected");
    }
    if (this.dividend == null) {
      throw new IOException(file.getName() + ": CDivision: No dividend selected");
    }
    if (this.dividend == this.divisor) {
      throw new IOException(file.getName() + ": CDivision: dividend and divisor are equal");
    }

    List<IGisFileContainer> fileList = new ArrayList<>();
    fileList.addAll(getSourceFileList());
    fileList.remove(this.divisor);
    fileList.remove(this.dividend);
    // RasterFileContainer dividend = fileList.get(0);
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (this.dividend.getGisFile().getNRows());
    int xSize = getSourceFileList().get(0).getGisFile().getNCols();

    float[] outputArray = new float[xSize];
    float[] divisorArray = new float[xSize];
    float[] dividendArray = new float[xSize];
    int rowCnt = this.dividend.getGisFile().getNRows();
    float noDataValue = Float.MAX_VALUE;
//    Band bandOut = file.getGisFile().getBand(RasterFileAccess.UPDATE);

    for (int i = 0; i < rowCnt; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      dividendArray = this.dividend.getGisFile().readRaster(0, i, xSize, 1);
      divisorArray = this.divisor.getGisFile().readRaster(0, i, xSize, 1);

      for (int j = 0; j < xSize; j++) {
        if (dividendArray[j] == this.dividend.getGisFile().getNoDataValue()
            || divisorArray[j] == this.divisor.getGisFile().getNoDataValue()) {
          outputArray[j] = noDataValue;
        } else {
          if (divisorArray[j] == 0) {
            if (this.divByZeroOption == SET_DUMMY)
              outputArray[j] = this.dummyValue;
            else if (this.divByZeroOption == SET_NODATA)
              outputArray[j] = noDataValue;
          } else {
            outputArray[j] = dividendArray[j] / divisorArray[j];
          }
        }
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
    }
    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    this.dividend.getGisFile().unlock();
    this.divisor.getGisFile().unlock();
    file.getGisFile().refresh();
  }

//  private void writeInFile(RasterFileContainer file) throws IOException, GisFileException {
//    if (this.divisor == null)
//      throw new IOException(file.getName() + ": CDivision: No divisor selected");
//    if (this.dividend == null)
//      throw new IOException(file.getName() + ": CDivision: No dividend selected");
//    if (this.dividend == this.divisor)
//      throw new IOException(file.getName() + ": CDivision: dividend and divisor are equal");
//
//    List<IGisFileContainer> fileList = new ArrayList<>();
//    fileList.addAll(gisFileList);
//    fileList.remove(this.divisor);
//    fileList.remove(this.dividend);
//    // RasterFileContainer dividend = fileList.get(0);
//    float tmpStatus = 0.0F;
//    float tmpSteps = 100.0F / (this.dividend.getNRows());
//    int xSize = this.dividend.getBand(RasterFileAccess.READ_ONLY).getXSize();
//    float[] outputArray = new float[xSize];
//    float[] divisorArray = new float[xSize];
//    float[] dividendArray = new float[xSize];
//    int rowCnt = this.dividend.getNRows();
//    float noDataValue = Float.MAX_VALUE;
//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
//
//    for (int i = 0; i < rowCnt; i++) {
//      if (this.workerStatus != null) {
//        tmpStatus += tmpSteps;
//        this.workerStatus.refreshPercentage(tmpStatus);
//      }
//
//      this.dividend.getBand(RasterFileAccess.READ_ONLY)
//          .ReadRaster(0, i, xSize, 1, dividendArray);
//      this.divisor.getBand(RasterFileAccess.READ_ONLY)
//          .ReadRaster(0, i, xSize, 1, divisorArray);
//
//      for (int j = 0; j < xSize; j++) {
//        if (dividendArray[j] == this.dividend.getNoDataValue()
//            || divisorArray[j] == this.divisor.getNoDataValue()) {
//          outputArray[j] = noDataValue;
//        } else {
//          if (divisorArray[j] == 0) {
//            if (this.divByZeroOption == SET_DUMMY)
//              outputArray[j] = this.dummyValue;
//            else if (this.divByZeroOption == SET_NODATA)
//              outputArray[j] = noDataValue;
//          } else {
//            outputArray[j] = dividendArray[j] / divisorArray[j];
//          }
//        }
//      }
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
//    }
//    for (int j = 0; j < gisFileList.size(); j++) {
//      ((RasterFileContainer) gisFileList.get(j)).unlock();
//    }
//    file.unlock();
//    this.dividend.unlock();
//    this.divisor.unlock();
//    file.refresh();
//  }
}

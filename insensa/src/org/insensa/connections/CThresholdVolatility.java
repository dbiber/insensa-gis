/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.gdal.gdal.Band;
import org.insensa.IGisFileContainer;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.inforeader.MinMaxValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CThresholdVolatility extends AbstractRasterConnection {

  public static final String NAME = "ThresholdVolatility";
  private static final Logger log = LoggerFactory.getLogger(CThresholdVolatility.class);
  private int threshold1Perc = 0;
  private int threshold2Perc = 0;
  private float[] threshold1List;
  private float[] threshold2List;

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("threshold1", this.threshold1Perc);
    saver.save("threshold2", this.threshold2Perc);
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
    int idx = this.getSourceFileList().indexOf(container);
    if (idx < 0) {
      log.error("Could not find container \"{}\" within the source file list", container.toString());
    } else if (threshold1List == null || threshold2List == null
        || getSourceFileList().size() != threshold1List.length
        || getSourceFileList().size() != threshold2List.length) {
      log.error("source file list size does not match threshold array size");
    } else {
      saver.save("threshold1", threshold1List[idx]);
      saver.save("threshold2", threshold2List[idx]);
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.threshold1List = new float[getSourceFileList().size()];
    this.threshold2List = new float[getSourceFileList().size()];

    this.threshold1Perc = restorer.loadInteger("threshold1").orElse(0);
    this.threshold2Perc = restorer.loadInteger("threshold2").orElse(0);
  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
    int idx = this.getSourceFileList().indexOf(container);
    if (idx < 0) {
      log.error("Could not find container \"{}\" within the source file list", container.toString());
    } else if (getSourceFileList().size() != threshold1List.length
        || getSourceFileList().size() != threshold2List.length) {
      log.error("source file list size does not match threshold array size");
    } else {
      threshold1List[idx] = restorer.loadFloat("threshold1").orElse(0f);
      threshold2List[idx] = restorer.loadFloat("threshold2").orElse(0f);
    }
  }

  @Override
  public AttributeTable getData() {
    return super.getData();
    //TODO: More specialized ?
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> deps = new ArrayList<String>();
    deps.add(MinMaxValues.NAME);
    return deps;
  }

  public int getThreshold1Perc() {
    return this.threshold1Perc;
  }

  public void setThreshold1Perc(int threshold1Perc) {
    this.threshold1Perc = threshold1Perc;
  }

  public int getThreshold2Perc() {
    return this.threshold2Perc;
  }

  public void setThreshold2Perc(int threshold2Perc) {
    this.threshold2Perc = threshold2Perc;
  }

  protected void solveDependencies() throws IOException {
    super.solveDependencies();
    this.threshold1List = new float[getSourceFileList().size()];
    this.threshold2List = new float[getSourceFileList().size()];
    for (int i = 0; i < getSourceFileList().size(); i++) {
      MinMaxValues minMaxValue = (MinMaxValues) getSourceFileList().get(i).getInfoReader(MinMaxValues.NAME);
      float newMin = minMaxValue.getMinValue();
      float newMax = minMaxValue.getMaxValue();
      float distance = newMax - newMin;
      float perc = distance / 100;

      float newValue1 = newMin + this.threshold1Perc * perc;
      this.threshold1List[i] = newValue1;

      float newValue2 = newMin + this.threshold2Perc * perc;
      this.threshold2List[i] = newValue2;
    }
  }

  @Override
  public void write() throws IOException, JDOMException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.toString());
    }
    this.threshold1List = new float[getSourceFileList().size()];
    this.threshold2List = new float[getSourceFileList().size()];
    this.solveDependencies();
    if (!this.check()) {
      throw new IOException("The files are incompatible");
    }

    RasterFileContainer endFile;
    if (this.outputFileContainer == null) {
      endFile = new RasterFileContainer(this.outputFileSet, this.outputFileName, this.outputFileSet.getPath() + this.outputFileSet.getName()
          + File.separator + this.outputFileName, null);
    } else {
      endFile = (RasterFileContainer) this.outputFileContainer;
    }

    RasterFileContainer tmpFile2 = (RasterFileContainer) getSourceFileList().get(0);

    endFile.getGisFile().createNewFile(tmpFile2.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Byte, Byte.MAX_VALUE);

    this.writeInFile(endFile);
    this.used = true;

  }

  @Override
  public void writeToContainer(RasterFileContainer file) throws IOException {

  }

  private void writeInFile(RasterFileContainer file) throws IOException, GisFileException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (((RasterFileContainer) getSourceFileList().get(0)).getNRows());
    boolean isNodata = true;
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName(this.toString());
    }

    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
    float noDataValue = Byte.MAX_VALUE;

    int xSize = ((RasterFileContainer) getSourceFileList().get(0)).getBand(RasterFileAccess.READ_ONLY).getXSize();
    float[] outputArray = new float[xSize];
    float[][] doubleDoubleArray = new float[getSourceFileList().size()][xSize];
    int rowCount;
    int fileCount = getSourceFileList().size();
    int iterFileCount = fileCount;

    // Schleife 1 Anzahl der Zeilen
    rowCount = ((RasterFileContainer) getSourceFileList().get(0)).getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < getSourceFileList().size(); j++) {
        ((RasterFileContainer) getSourceFileList().get(j)).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        outputArray[j] = noDataValue;
        // schleife 4 Anzahl der Dateien
        for (int k = 0; k < getSourceFileList().size(); k++) {
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == ((RasterFileContainer) getSourceFileList().get(k)).getNoDataValue()) {

          } else {
            isNodata = false;
            if (tmpValue < this.threshold1List[k] || tmpValue > this.threshold2List[k])
              iterFileCount--;
          }

        }// s4-ENDE
        // if(iterFileCount<fileCount)
        if (isNodata == false) {
          outputArray[j] = iterFileCount;
          isNodata = true;
        }
        iterFileCount = fileCount;
      }
      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
    }// Schleife1-Ende
    file.unlock();
    for (int j = 0; j < getSourceFileList().size(); j++)
      ((RasterFileContainer) getSourceFileList().get(j)).unlock();
    file.refresh();
  }// writeToContainer End
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.MedianValue;
import org.insensa.inforeader.MinMaxValues;
import org.insensa.inforeader.QuartileValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ErrorIndexation extends CIndexation {

  public final static int WORST_CASE = -1;
  public final static int BEST_CASE = 1;
  public final static int NORMAL = 0;
  private static final Logger log = LoggerFactory.getLogger(ErrorIndexation.class);
  private int rowCount = 0;
  private float maxValue = Float.MIN_VALUE;
  private float minValue = Float.MAX_VALUE;
  private int errorCase = 0;
  private Map<String, Weighting> fileWeightingMap = new HashMap<>();
  private AggregationMethod method = AggregationMethod.ADDITIVE;

  @Override
  protected void setWeightMap(List<? extends IGisFileContainer> sourceFileList) {
    fileWeightingMap.clear();
    for (IGisFileContainer fileContainer : sourceFileList) {
      fileWeightingMap.put(fileContainer.getPathRelativeToProject() + fileContainer.getOutputFileName(),
          new ErrorWeighting());
    }
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    switch (errorCase) {
      case 1:
        saver.save("errorCase", "BEST_CASE");
        break;
      case -1:
        saver.save("errorCase", "WORST_CASE");
        break;
      case 0:
        saver.save("errorCase", "NORMAL");
        break;
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    String sEerrorCase = restorer.loadString("errorCase").orElse("NORMAL");
    switch (sEerrorCase) {
      case "BEST_CASE":
        errorCase = 1;
        break;
      case "WORST_CASE":
        errorCase = -1;
        break;
      case "NORMAL":
        errorCase = 0;
        break;
    }
  }

  @Override
  protected void restoreWeighting(String filePath, ISavableRestorer restorer) {
    restorer.loadRestorable("errWeighting", ErrorIndexation.ErrorWeighting.class)
        .ifPresent(data -> fileWeightingMap.put(filePath, (ErrorWeighting) data));
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
    Weighting w = getWeighting(container);
    saver.save("errWeighting", w);
  }

  @Override
  public ErrorWeighting getWeighting(IGisFileContainer container) {
    return (ErrorWeighting) fileWeightingMap.get(container.getPathRelativeToProject() + container.getOutputFileName());
  }

  @Override
  public AttributeTable getData() {
    //TODO: make it nicer
    return super.getData();
  }


  //  @Override
//  public AttributeTable getData() {
//    AttributeTable dataMap = super.getData();
//    if (this.outputFile != null) {
//      Element tmpInfo;
//      try {
//        tmpInfo = this.outputFile.getGisFileInformationStorage().getConnectionElements().get(0);
//      } catch (IOException e) {
//        return dataMap;
//      }
//      if (tmpInfo != null) {
//        String sAggregationMethod = tmpInfo.getAttributeValue("AggregationMethod");
//        if (sAggregationMethod != null)
//          dataMap.put("AggregationMethod", sAggregationMethod);
//        String sCase = tmpInfo.getAttributeValue("errorCase");
//        if (sCase != null) {
//          if (sCase.equals("BEST_CASE"))
//            dataMap.put("Case", "High Case");
//          else if (sCase.equals("WORST_CASE"))
//            dataMap.put("Case", "Low Case");
//          else if (sCase.equals("NORMAL"))
//            dataMap.put("Case", "Normal");
//        } else
//          dataMap.put("Case", "nul");
//
//        List<Element> leFiles = tmpInfo.getChildren();
//        for (Element eFile : leFiles) {
//          dataMap.put("Filename", eFile.getAttributeValue("outputName"));
//
//          Element eWeight = eFile.getChild("weighting");
//          if (eWeight != null) {
//            dataMap.put("  Weighting", eWeight.getAttributeValue("weight"));
//            dataMap.put("  Inverse", eWeight.getAttributeValue("isNeg"));
//            dataMap.put("  Lower Error", eWeight.getAttributeValue("lowerError"));
//            dataMap.put("  Upper Error", eWeight.getAttributeValue("upperError"));
//          }
//
//          Element eThreshold1 = eFile.getChild("threshold1");
//          if (eThreshold1 != null) {
//            String cond = eThreshold1.getAttributeValue("condition");
//            String condVal = eThreshold1.getAttributeValue("ConVal");
//            String condMode = eThreshold1.getAttributeValue("mode");
//            String assign = eThreshold1.getAttributeValue("assign");
//            String fullString = "if Value " + cond + " " + condVal + condMode + " then: " + assign;
//
//            dataMap.put("  Threshold1", fullString);
//          }
//          Element eThreshold2 = eFile.getChild("threshold2");
//          if (eThreshold2 != null) {
//            String cond = eThreshold2.getAttributeValue("condition");
//            String condVal = eThreshold2.getAttributeValue("ConVal");
//            String condMode = eThreshold2.getAttributeValue("mode");
//            String assign = eThreshold2.getAttributeValue("assign");
//            String fullString = "if Value " + cond + " " + condVal + condMode + " then: " + assign;
//
//            dataMap.put("  Threshold2", fullString);
//          }
//
//        }
//      }
//    }
//    return dataMap;

//  }

  public int getErrorCase() {
    return this.errorCase;
  }

  public void setErrorCase(int errorCase) {
    this.errorCase = errorCase;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> deps = new ArrayList<String>();
    deps.add(MinMaxValues.NAME);
    deps.add(MedianValue.NAME);
    deps.add(QuartileValues.NAME);
    return deps;
  }

  public AggregationMethod getMethod() {
    return this.method;
  }

  public void setMethod(AggregationMethod method) {
    this.method = method;
  }

  @Override
  protected void solveWeightingDependencies(IGisFileContainer fileContainer) {
    ErrorWeighting errorWeighting = this.getWeighting(fileContainer);
    int tmpCase = this.errorCase;
    MedianValue medianValue = (MedianValue) fileContainer.getInfoReader(MedianValue.NAME);
    QuartileValues quartileValues = (QuartileValues) fileContainer.getInfoReader(QuartileValues.NAME);
    float bound2 = ((float) quartileValues.getHighQuartile()) - ((float) medianValue.getMedianValue());
    float bound1 = ((float) medianValue.getMedianValue()) - ((float) quartileValues.getLowQuartile());
    if (errorWeighting.negative) {
      tmpCase *= -1;
    }
    if (bound1 < bound2) {
      if (tmpCase == BEST_CASE) {
        errorWeighting.weight = errorWeighting.lowerError;
      } else if (tmpCase == WORST_CASE) {
        errorWeighting.weight = errorWeighting.upperError;
      }
    } else if (bound1 > bound2) {
      if (tmpCase == BEST_CASE) {
        errorWeighting.weight = errorWeighting.upperError;
      } else if (tmpCase == WORST_CASE) {
        errorWeighting.weight = errorWeighting.lowerError;
      }
    }
  }

  protected void solveDependencies() throws IOException {

    new InfoManager();
    if (this.workerStatus != null)
      this.workerStatus.startPause("Resolve Dependencies");

    super.solveDependencies();

    if (this.workerStatus != null)
      this.workerStatus.endPause();

  }

  @Override
  public void write() throws IOException, JDOMException, GisFileException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName("Indexation");
    }
    this.solveDependencies();

    if (this.check() == false) {
      throw new IOException("The files are incompatible");
    }

    RasterFileContainer endFile;
    if (this.outputFileContainer == null)
      endFile = new RasterFileContainer(this.outputFileSet, this.outputFileName, this.outputFileSet.getPath() + this.outputFileSet.getName()
          + File.separator + this.outputFileName, null);
    else
      endFile = (RasterFileContainer) this.outputFileContainer;
    RasterFileContainer tmpFile = new RasterFileContainer(this.outputFileSet, "TMP" + this.outputFileName, this.outputFileSet.getPath()
        + this.outputFileSet.getName() + File.separator + "TMP" + this.outputFileName, null);

    RasterFileContainer tmpFile2 = getSourceFileList().get(0);

    tmpFile.getGisFile().createNewFile(tmpFile2.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, -9999.0);

    if (this.method == AggregationMethod.ADDITIVE) {
      this.writeInFileAdditive(tmpFile);
    } else {
      this.writeInFileGeometric(tmpFile);
    }

    this.fillPlaceholder(tmpFile, endFile);

    if (!tmpFile.delete()) {
      log.error("Error deleting tmp file {}", tmpFile.getAbsolutePath());
    }

    this.used = true;
  }

//  private void writeInFileAdditive(RasterFileContainer file) throws IOException, GisFileException {
//    float tmpStatus = 0.0F;
//    RasterFileContainer[] rasterFileArray = new RasterFileContainer[gisFileList.size()];
//    for (int i = 0; i < gisFileList.size(); i++)
//      rasterFileArray[i] = ((RasterFileContainer) gisFileList.get(i));
//
////    Float[] weightArray = new Float[this.weightList.size()];
////    for (int i = 0; i < this.weightList.size(); i++)
////      weightArray[i] = this.weightList.get(i);
////
////    Boolean[] negWeightArray = new Boolean[this.negWeightList.size()];
////    for (int i = 0; i < this.negWeightList.size(); i++)
////      negWeightArray[i] = this.negWeightList.get(i);
//    // float tmpSteps = 100.0F/(rasterFileList.get(0).getNRows());
//    float tmpSteps = 100.0F / (rasterFileArray[0].getNRows());
//    if (this.workerStatus != null) {
//      this.workerStatus.setProgressName("Prioritization Step 1/2 " + this.getOutputFile().getName());
//    }
//
//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
//    float noDataValue = -9999.0F;
//
//    // int xSize = rasterFileList.get(0).getBand().getXSize();
//    int xSize = rasterFileArray[0].getBand(RasterFileAccess.READ_ONLY).getXSize();
//
//    float[] outputArray = new float[xSize];
//
//    // float[][] doubleDoubleArray = new
//    // float[rasterFileList.size()][xSize];
//    float[][] doubleDoubleArray = new float[rasterFileArray.length][xSize];
//    float thresholdValue = 0.0F;
//
//    // Schleife 1 Anzahl der Zeilen
//    // rowCount=rasterFileList.get(0).getNRows();
//    this.rowCount = rasterFileArray[0].getNRows();
//    for (int i = 0; i < this.rowCount; i++) {
//      if (this.workerStatus != null) {
//        tmpStatus += tmpSteps;
//        this.workerStatus.refreshPercentage(tmpStatus);
//      }
//
//      // schleife 2 Anzahl der Dateien
//      for (int j = 0; j < rasterFileArray.length; j++) {
//        rasterFileArray[j].getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
//      }
//
//      // schleife 3 Anzahl der Elemente in einer Zeile
//      for (int j = 0; j < xSize; j++) {
//        outputArray[j] = noDataValue;
//        for (int k = 0; k < rasterFileArray.length; k++) {
//          Weighting weighting = getWeighting(k);
//          if (weighting.weight == 0.0F)
//            continue;
//          float tmpValue = doubleDoubleArray[k][j];
//          float rewPenString;
//          if (tmpValue == rasterFileArray[k].getNoDataValue()) {
//            tmpValue = noDataValue;
//          } else {
//            rewPenString = this.calculateThreshold(tmpValue, k);
//            if (rewPenString != 0) {
//              if (rewPenString == Float.MIN_NORMAL)
//                thresholdValue -= weighting.weight;
//              else if (rewPenString == Float.MAX_VALUE)
//                thresholdValue += weighting.weight;
//            }
//            tmpValue *= weighting.weight;
//            if (weighting.negative)
//              tmpValue *= (-1);
//            if (outputArray[j] == noDataValue)
//              outputArray[j] = 0.0F;
//            outputArray[j] += tmpValue;
//          }
//          // s3-ENDE
//        }
//        if (outputArray[j] != noDataValue && outputArray[j] != Float.MIN_NORMAL && outputArray[j] != Float.MAX_VALUE) {
//          if (outputArray[j] < this.minValue)
//            this.minValue = outputArray[j];
//          if (outputArray[j] > this.maxValue)
//            this.maxValue = outputArray[j];
//        }
//        if (thresholdValue == 0) {
//
//        } else if (thresholdValue < 0)
//          outputArray[j] = Float.MIN_NORMAL;
//        else if (thresholdValue > 0)
//          outputArray[j] = Float.MAX_VALUE;
//        thresholdValue = 0.0F;
//      }
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
//      // Schleife1-Ende
//    }
//
//    // TODO TEST wegen delete Problem
//    // file.getDataset().FlushCache();
//    file.unlock();
//    for (int j = 0; j < gisFileList.size(); j++)
//      ((RasterFileContainer) gisFileList.get(j)).unlock();
//    file.refresh();
//  }

//  private void writeInFileGeometric(RasterFileContainer file) throws IOException, GisFileException {
//    float tmpStatus = 0.0F;
//    RasterFileContainer[] rasterFileArray = new RasterFileContainer[gisFileList.size()];
//    for (int i = 0; i < gisFileList.size(); i++)
//      rasterFileArray[i] = ((RasterFileContainer) gisFileList.get(i));
//
//    Float[] weightArray = new Float[this.weightList.size()];
//    for (int i = 0; i < this.weightList.size(); i++)
//      weightArray[i] = this.weightList.get(i);
//
//    Boolean[] negWeightArray = new Boolean[this.negWeightList.size()];
//    for (int i = 0; i < this.negWeightList.size(); i++)
//      negWeightArray[i] = this.negWeightList.get(i);
//    // float tmpSteps = 100.0F/(rasterFileList.get(0).getNRows());
//    float tmpSteps = 100.0F / (rasterFileArray[0].getNRows());
//    if (this.workerStatus != null) {
//      this.workerStatus.setProgressName("Prioritization Step 1/2 " + this.getOutputFile().getName());
//    }
//
//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
//    float noDataValue = -9999.0F;
//
//    // int xSize = rasterFileList.get(0).getBand().getXSize();
//    int xSize = rasterFileArray[0].getBand(RasterFileAccess.READ_ONLY).getXSize();
//
//    float[] outputArray = new float[xSize];
//
//    // float[][] doubleDoubleArray = new
//    // float[rasterFileList.size()][xSize];
//    float[][] doubleDoubleArray = new float[rasterFileArray.length][xSize];
//    float thresholdValue = 0.0F;
//
//    // Schleife 1 Anzahl der Zeilen
//    // rowCount=rasterFileList.get(0).getNRows();
//    this.rowCount = rasterFileArray[0].getNRows();
//    for (int i = 0; i < this.rowCount; i++) {
//      if (this.workerStatus != null) {
//        tmpStatus += tmpSteps;
//        this.workerStatus.refreshPercentage(tmpStatus);
//      }
//
//      // schleife 2 Anzahl der Dateien
//      for (int j = 0; j < rasterFileArray.length; j++) {
//        rasterFileArray[j].getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
//      }
//
//      // schleife 3 Anzahl der Elemente in einer Zeile
//      for (int j = 0; j < xSize; j++) {
//        outputArray[j] = noDataValue;
//        for (int k = 0; k < rasterFileArray.length; k++) {
//          if (this.weightList.get(k) == 0.0F)
//            continue;
//          float tmpValue = doubleDoubleArray[k][j];
//          float rewPenString;
//          if (tmpValue == rasterFileArray[k].getNoDataValue()) {
//            tmpValue = noDataValue;
//          } else {
//            rewPenString = this.calculateThreshold(tmpValue, k);
//            if (rewPenString != 0) {
//              if (rewPenString == Float.MIN_NORMAL)
//                thresholdValue -= this.weightList.get(k);
//              else if (rewPenString == Float.MAX_VALUE)
//                thresholdValue += this.weightList.get(k);
//            }
//            float weightValue = this.weightList.get(k);
//            if (this.negWeightList.get(k) == true)
//              weightValue *= (-1.0f);
//            tmpValue = new Double(Math.pow(tmpValue, weightValue)).floatValue();
//            if (outputArray[j] == noDataValue)
//              outputArray[j] = 0.0F;
//            outputArray[j] *= tmpValue;
//          }
//          // s3-ENDE
//        }
//        if (outputArray[j] != noDataValue && outputArray[j] != Float.MIN_NORMAL && outputArray[j] != Float.MAX_VALUE) {
//          if (outputArray[j] < this.minValue)
//            this.minValue = outputArray[j];
//          if (outputArray[j] > this.maxValue)
//            this.maxValue = outputArray[j];
//        }
//        if (thresholdValue == 0) {
//
//        } else if (thresholdValue < 0)
//          outputArray[j] = Float.MIN_NORMAL;
//        else if (thresholdValue > 0)
//          outputArray[j] = Float.MAX_VALUE;
//        thresholdValue = 0.0F;
//      }
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
//      // Schleife1-Ende
//    }
//
//    // TODO TEST wegen delete Problem
//    // file.getDataset().FlushCache();
//    for (int j = 0; j < gisFileList.size(); j++)
//      ((RasterFileContainer) gisFileList.get(j)).unlock();
//    file.unlock();
//    file.refresh();
//
//    // writeToContainer End
//  }

  public static class ErrorWeighting extends Weighting {
    private Float lowerError;
    private Float upperError;

    public ErrorWeighting() {
      super();
      lowerError = 0.0F;
      upperError = 0.0F;
    }

    public void setLowerError(Float lowerError) {
      this.lowerError = lowerError;
    }

    public void setUpperError(Float upperError) {
      this.upperError = upperError;
    }

    @Override
    public void restore(ISavableRestorer restorer) {
      super.restore(restorer);
      lowerError = restorer.loadFloat("lowerError").orElse(0.0F);
      upperError = restorer.loadFloat("upperError").orElse(0.0F);
    }

    @Override
    public void save(ISavableSaver saver) {
      super.save(saver);
      saver.save("lowerError", lowerError);
      saver.save("upperError", upperError);
    }
  }
}

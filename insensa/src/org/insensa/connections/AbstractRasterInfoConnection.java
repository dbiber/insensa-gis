package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;

import java.io.IOException;

public abstract class AbstractRasterInfoConnection extends AbstractInfoConnection {
  protected boolean check() throws IOException {
    if (sourceFileList.isEmpty())
      throw new IOException("rasterFileList is empty");
    if (sourceFileList.size() < 2)
      throw new IOException("rasterFileList size is: " + sourceFileList.size());
    RasterFileContainer rasterFileInformation = (RasterFileContainer) sourceFileList.get(0);
    float cellSize = rasterFileInformation.getGisFile().getCellSize();
    float xllCorner = rasterFileInformation.getGisFile().getXllCorner();
    float yllCorner = rasterFileInformation.getGisFile().getYllCorner();
    int rows = rasterFileInformation.getGisFile().getNRows();
    int cols = rasterFileInformation.getGisFile().getNCols();
    for (IGisFileContainer iGisFile: sourceFileList) {
//      IGisFileContainer  iFile =  iGisFile;
      IRasterGisFile iFile = (IRasterGisFile) iGisFile.getGisFile();
      if (iFile == null)
        throw new IOException("one or more source files are missing");
      // return false;
      if (cellSize != iFile.getCellSize())
        throw new IOException("cellsize \"" + cellSize + "\" is different from \"" + iFile.getCellSize() + "\"");
      if (xllCorner != iFile.getXllCorner())
        throw new IOException("xllCorner \"" + xllCorner + "\" is different from \"" + iFile.getXllCorner() + "\"");
      if (yllCorner != iFile.getYllCorner())
        throw new IOException("yllCorner \"" + yllCorner + "\" is different from \"" + iFile.getYllCorner() + "\"");
      if (rows != iFile.getNRows())
        throw new IOException("rows \"" + rows + "\" is different from \"" + iFile.getNRows() + "\"");
      if (cols != iFile.getNCols())
        throw new IOException("cols \"" + cols + "\" is different from \"" + iFile.getNCols() + "\"");
    }
    return true;
  }
}

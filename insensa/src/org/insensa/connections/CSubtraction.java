/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IGisFileContainerFactory;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CSubtraction extends AbstractRasterConnection {
  public final static int SET_NODATA = 0;
  public final static int SET_MAX_PRECISION = 1;
  public final static int SET_DUMMY = 2;
  private RasterFileContainer minuend = null;
  private boolean relative = false;
  private boolean ignoreNodataValues = true;
  private boolean absolute = false;
  private float dummyValue = 0.0F;
  private int divByZeroOption = SET_NODATA;

  public boolean getAbsolute() {
    return this.absolute;
  }

  public void setAbsolute(boolean absolute) {
    this.absolute = absolute;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("absolute", absolute);
    saver.save("relative", relative);
    saver.save("ignoreNodataValues", ignoreNodataValues);
    saver.save("divByZeroOption", divByZeroOption);
    if (divByZeroOption == SET_DUMMY) {
      saver.save("dummy", dummyValue);
    }
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
    if (container == minuend) {
      saver.save("minuend", true);
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    if (!isUsed()) {
      relative = restorer.loadBoolean("relative").orElse(false);
      absolute = restorer.loadBoolean("absolute").orElse(false);
      ignoreNodataValues = restorer.loadBoolean("ignoreNodataValues").orElse(false);
      divByZeroOption = restorer.loadInteger("divByZeroOption").orElse(0);
      if (divByZeroOption == SET_DUMMY) {
        dummyValue = restorer.loadFloat("dummy").orElse(0.0F);
      }

    }
  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
    Boolean bMinuend = restorer
        .loadBoolean("minuend")
        .orElse(false);
    if (bMinuend != null && bMinuend) {
      this.minuend = (RasterFileContainer) container;
    }
  }

//  @Override
//  public AttributeTable getData() {
//    AttributeTable dataMap = super.getData();
//
//    if (this.outputFile == null)
//      return dataMap;
//
//    if (this.outputFile != null) {
//      Element tmpInfo;
//      try {
//        tmpInfo = this.outputFile.getGisFileInformationStorage().getConnectionElements().get(0);
//      } catch (IOException e) {
//        return dataMap;
//      }
//      if (tmpInfo != null) {
//        String rel = tmpInfo.getAttributeValue("relative");
//        String abs = tmpInfo.getAttributeValue("absolute");
//        if (abs != null && abs.equals("true")) {
//          dataMap.put("Absolute", "true");
//        }
//        if (rel != null && rel.equals("true")) {
//          dataMap.put("Relative", "true");
//          String divZeroOption = tmpInfo.getAttributeValue("divByZeroOption");
//          if (divZeroOption != null) {
//            int tmpDiv = Integer.valueOf(divZeroOption).intValue();
//            switch (tmpDiv) {
//              case SET_NODATA:
//                dataMap.put("DivByZeroOption", "SET_NODATA");
//                break;
//              case SET_MAX_PRECISION:
//                dataMap.put("DivByZeroOption", "SET_MAX_PRECITION");
//                break;
//              case SET_DUMMY:
//                dataMap.put("DivByZeroOption", "SET_DUMMY");
//                String sDummyValue = tmpInfo.getAttributeValue("dummy");
//                if (sDummyValue != null) {
//                  dataMap.put("Dummy Value", sDummyValue);
//                }
//                break;
//              default:
//                break;
//            }
//          }
//        } else {
//          dataMap.put("Relative", "false");
//        }
//        String ignNodataVal = tmpInfo.getAttributeValue("ignoreNodataValues");
//
//        dataMap.put("Ignore NoDataValue", ignNodataVal);
//
//        @SuppressWarnings("unchecked")
//        List<Element> leFiles = tmpInfo.getChildren();
//        for (Element eFile : leFiles) {
//          String minuend = eFile.getAttributeValue("minuend");
//          if (minuend != null && minuend.equals("true")) {
//            dataMap.put("Filename (minuend)", eFile.getAttributeValue("outputName"));
//          } else
//            dataMap.put("Filename", eFile.getAttributeValue("outputName"));
//        }
//      }
//    }
//
//    return dataMap;
//  }

  public int getDivByZeroOption() {
    return this.divByZeroOption;
  }

  public void setDivByZeroOption(int divByZeroOption) {
    this.divByZeroOption = divByZeroOption;
  }

  public float getDummyValue() {
    return this.dummyValue;
  }

  public void setDummyValue(float dummyValue) {
    this.dummyValue = dummyValue;
  }

  public boolean getIgnoreNodata() {
    return this.ignoreNodataValues;
  }

  public void setIgnoreNodata(boolean ignoreNodata) {
    this.ignoreNodataValues = ignoreNodata;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    depList.add(PrecisionValues.NAME);
    return depList;
  }

  public IGisFileContainer getMinuend() {
    return this.minuend;
  }

  public void setMinuend(IGisFileContainer minuend) {
    this.minuend = getSourceFileList().get(getSourceFileList().indexOf(minuend));
  }

  public boolean getRelative() {
    return this.relative;
  }

  public void setRelative(boolean relative) {
    this.relative = relative;
  }

  @Override
  public void write(IGisFileContainerFactory fileContainerFactory) throws IOException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(this.toString());
    }

    if (!this.check()) {
      throw new IOException("The files are incompatible");
    }
    if (this.minuend == null)
      throw new IOException("No minuend set");
    List<IGisFileContainer> tmpList = new ArrayList<>();
    tmpList.addAll(getSourceFileList());
    tmpList.remove(this.minuend);
    for (IGisFileContainer file : tmpList) {
      if (file.getGisFile()
          .getAbsolutePath().equals(this.minuend.getAbsolutePath())) {
        throw new IOException("One file can not be the minuend AND subtrahend");
      }
    }

    RasterFileContainer endFile;
//    RasterFileContainer endFile;
    if (this.outputFileContainer == null) {
      endFile = (RasterFileContainer) fileContainerFactory.create(this.outputFileSet, this.outputFileName,
          this.outputFileSet.getPath() + this.outputFileSet.getName()
              + File.separator + this.outputFileName, null);
//      endFile = new RasterFileContainer(this.outputFileSet, this.outputFileName,
//          this.outputFileSet.getPath() + this.outputFileSet.getName()
//          + File.separator + this.outputFileName, null);
    } else {
      endFile = (RasterFileContainer) this.outputFileContainer;
    }

    IGisFileContainer tmpFile2 = getSourceFileList().get(0);

    endFile.getGisFile().createNewFile((IRasterGisFile) tmpFile2.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, -9999.0);

    this.writeToContainer(endFile);
    this.used = true;
  }

  @Override
  public void write() throws IOException, JDOMException {
  }

  /**
   * Minuend - Subtrahent = Differenz Rel = Differenz / Subtrahent.
   */
  @Override
  public void writeToContainer(RasterFileContainer file) throws IOException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / getSourceFileList().get(0).getGisFile().getNRows();
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName(this.toString());
    }

    //    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
    float noDataValue = -9999.0F;

    int xSize = getSourceFileList().get(0).getGisFile().getNCols();
    float[] outputMinuendArray;// = new float[xSize];
    float[][] subtrahentDoubleDouble = new float[getSourceFileList().size() - 1][];
    int rowCount;
    float zeroRepl = 0;
    // float outDifferenceValue;
    float tmpMinuend;
    float tmpSubtrahend;
    float difference;
    int precition = 0;
    List<RasterFileContainer> newList = new ArrayList<>();
    newList.addAll(getSourceFileList());

    // Schleife 1 Anzahl der Zeilen
    rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }
      if (this.minuend != null) {
        newList.remove(this.minuend);
      } else {
        this.minuend = newList.get(0);
        newList.remove(0);
      }
      if (this.relative == true && this.divByZeroOption == SET_MAX_PRECISION) {
        List<IGisFileContainer> fileList = new ArrayList<>();
        fileList.add(this.minuend);
        this.solveDependencies();
        PrecisionValues precInfo = (PrecisionValues) this.minuend.getInfoReader(PrecisionValues.NAME);
        precition = precInfo.getMaxPrecision();
        zeroRepl = new Double(1 / (Math.pow(10, precition))).floatValue();
      }
      outputMinuendArray = this.minuend.getGisFile().readRaster(0, i, xSize, 1);
      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < newList.size(); j++) {
        subtrahentDoubleDouble[j] = newList.get(j).getGisFile().readRaster(0, i, xSize, 1);
//        ((RasterFileContainer) newList.get(j)).getBand(RasterFileAccess.READ_ONLY).ReadRaster(0, i, xSize, 1, subtrahentDoubleDouble[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        tmpMinuend = outputMinuendArray[j];
        if (tmpMinuend == minuend.getGisFile().getNoDataValue()) {
          difference = noDataValue;
          outputMinuendArray[j] = noDataValue;
          continue;
        } else {
          difference = tmpMinuend;
        }
        tmpSubtrahend = 0.0F;
        // schleife 4 Anzahl der Dateien
        for (int k = 0; k < newList.size(); k++) {
          Float tmpSubtrahend2 = subtrahentDoubleDouble[k][j];
          if (tmpSubtrahend2 == newList.get(k).getGisFile().getNoDataValue()) {
            // subtrahendValue=noDataValue;
            tmpSubtrahend = noDataValue;
            if (!this.ignoreNodataValues) {
              difference = noDataValue;
              break;
            }
          } else {
            tmpSubtrahend += tmpSubtrahend2;
            difference -= tmpSubtrahend2;
          }

        }// s4-ENDE
        if (tmpSubtrahend == noDataValue) {
          outputMinuendArray[j] = noDataValue;
          continue;
        }
        /*
         * Differenz = Minunend - Subtrahend Rel = Differenz /
         * Subtrahend outMinuendArray = Das ist der Minuend Wert und
         * hier wird der Endwert gespeichert!! tmpMinunend = DER
         * MINUNEND tmpSubtrahend = DER SUBTRAHEND (Summe der
         * Subtrahenten) difference = DIE DIFFERENZ
         */

        // Wenn Relative Werte Berechnet werden sollen,
        // Die Differenz NICHT NoDataValue ist UND
        // nur ein Subtrahend existiert
        if (this.relative && difference != noDataValue && subtrahentDoubleDouble.length == 1) {
          // DIV DURCH 0
          if (tmpSubtrahend == 0) {
            switch (this.divByZeroOption) {
              case SET_NODATA:
                outputMinuendArray[j] = noDataValue;
                break;
              case SET_MAX_PRECISION:
                outputMinuendArray[j] = ((tmpMinuend - zeroRepl) / zeroRepl) * 100.0F;
                break;
              case SET_DUMMY:
                outputMinuendArray[j] = this.dummyValue;
                break;
              default:
                break;
            }
          } else if (tmpSubtrahend < 0) {
            if (this.divByZeroOption == SET_MAX_PRECISION) {
              if (tmpSubtrahend > zeroRepl)
                outputMinuendArray[j] = ((tmpMinuend - zeroRepl) / zeroRepl) * 100.0F;
              else
                outputMinuendArray[j] = (difference / Math.abs(tmpSubtrahend)) * 100.0F;
            } else
              outputMinuendArray[j] = (difference / Math.abs(tmpSubtrahend)) * 100.0F;
            if (this.absolute)
              outputMinuendArray[j] = Math.abs(outputMinuendArray[j]);
          } else {
            if (this.divByZeroOption == SET_MAX_PRECISION) {
              if (tmpSubtrahend < zeroRepl)
                outputMinuendArray[j] = ((tmpMinuend - zeroRepl) / zeroRepl) * 100.0F;
              else
                outputMinuendArray[j] = (difference / tmpSubtrahend) * 100.0F;
            } else
              outputMinuendArray[j] = (difference / tmpSubtrahend) * 100.0F;
            if (this.absolute)
              outputMinuendArray[j] = Math.abs(outputMinuendArray[j]);
          }
        } else {
          if (this.absolute)
            difference = Math.abs(difference);
          outputMinuendArray[j] = difference;
        }
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputMinuendArray);
//      bandOut.WriteRaster(0, i, xSize, 1, outputMinuendArray);
    }// Schleife1-Ende
    for (int j = 0; j < getSourceFileList().size(); j++)
      getSourceFileList().get(j).getGisFile().unlock();
    if (this.minuend != null) {
      minuend.getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();
  }// writeToContainer End

}

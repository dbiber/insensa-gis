package org.insensa.connections;

import org.insensa.StackFile;
import org.insensa.IGisFileContainer;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;

public class StackConnection extends AbstractConnection {

  public static final String NAME = "stack";

  @Override
  public String getId() {
    return "stack";
  }


  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
  }

  @Override
  public void write() throws IOException, JDOMException {
  }
}

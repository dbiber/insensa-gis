/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IGisFileContainerFactory;
import org.insensa.RasterFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.inforeader.MeanValue;
import org.insensa.inforeader.MinMaxValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.IStorageData;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CIndexation extends AbstractRasterConnection {
  private static final Logger log = LoggerFactory.getLogger(CIndexation.class);

  // Normale Priorisierung
  private int rowCount = 0;
  private Map<String, Weighting> fileWeightingMap = new LinkedHashMap<>();
  private float maxValue = Float.MIN_VALUE;
  private float minValue = Float.MAX_VALUE;
  private AggregationMethod method = AggregationMethod.ADDITIVE;

  @Override
  public void setSourceFileList(List<? extends IGisFileContainer> sourceFileList) {
    super.setSourceFileList(sourceFileList);
    setWeightMap(sourceFileList);
  }

  private boolean hasThreshold() {

    for (Map.Entry<String, Weighting> entry : fileWeightingMap.entrySet()) {
      if (entry.getValue().reward.getActivate() || entry.getValue().penalty.getActivate()) {
        return true;
      }
    }
    return false;
  }

  protected void setWeightMap(List<? extends IGisFileContainer> sourceFileList) {
    fileWeightingMap.clear();
    for (IGisFileContainer fileContainer : sourceFileList) {
      fileWeightingMap.put(fileContainer.getPathRelativeToProject() + fileContainer.getOutputFileName(),
          new Weighting());
    }
  }

  public Weighting getWeighting(IGisFileContainer container) {
    return fileWeightingMap.get(container.getPathRelativeToProject() + container.getOutputFileName());
  }

  private Weighting getWeighting(int index) {
    return getWeighting(getSourceFileList().get(index));
  }


  private float calculateThreshold(double tmpValue, int fileIndex, Threshold threshold) {
    if (threshold.getActivate() == true) {
      if (threshold.getCondition().equals("<")) {
        if (tmpValue < ((Float) threshold.getValueReal()).doubleValue()) {
          if (threshold.getAssignment() == Assignment.min) {
            return Float.MIN_NORMAL;
          } else {
            return Float.MAX_VALUE;
          }
        }
      }
      if (threshold.getCondition().equals(">")) {
        if (tmpValue > ((Float) threshold.getValueReal()).doubleValue()) {
          if (threshold.getAssignment() == Assignment.min) {
            return Float.MIN_NORMAL;
          } else {
            return Float.MAX_VALUE;
          }
        }
      }
      if (threshold.getCondition().equals("<=")) {
        if (tmpValue <= ((Float) threshold.getValueReal()).doubleValue()) {
          if (threshold.getAssignment() == Assignment.min) {
            return Float.MIN_NORMAL;
          } else {
            return Float.MAX_VALUE;
          }
        }
      }
      if (threshold.getCondition().equals(">=")) {
        if (tmpValue >= ((Float) threshold.getValueReal()).doubleValue()) {
          if (threshold.getAssignment() == Assignment.min) {
            return Float.MIN_NORMAL;
          } else {
            return Float.MAX_VALUE;
          }
        }
      }
    }
    return 0.0F;
  }

  protected float calculateThreshold(double tmpValue, int fileIndex) {
    float outVal;
    Threshold tmpRewPen = getWeighting(fileIndex).reward;
    outVal = calculateThreshold(tmpValue, fileIndex, tmpRewPen);
    if (outVal != 0.0F) {
      return outVal;
    } else {
      return calculateThreshold(tmpValue, fileIndex, getWeighting(fileIndex).penalty);
    }
  }

  /**
   * The key is the concat of relativePathToProject + outputName: <br>
   * {@link IGisFileContainer#getPathRelativeToProject()} +
   * {@link IGisFileContainer#getOutputFileName()}
   */
  public Map<String, Weighting> getFileWeightingMap() {
    return fileWeightingMap;
  }

  public void setFileWeightingMap(Map<String, Weighting> fileWeightingMap) {
    this.fileWeightingMap = fileWeightingMap;
  }

  protected IGisFileContainer
  fillPlaceholder(RasterFileContainer tmpFile,
                  RasterFileContainer endFile) throws IOException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / getSourceFileList().get(0).getGisFile().getNRows();

    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("Indexation Step 2/2 " + this.getOutputFileContainer().getName());
      this.workerStatus.refreshPercentage(tmpStatus);
    }
    int ySize = tmpFile.getGisFile().getNRows();//band.getYSize();
    int xSize = tmpFile.getGisFile().getNCols();//band.GetXSize();
    float[] dData;// = new float[xSize];

    // Zeilenweise abarbeiten
    for (int i = 0; i < ySize; i++) {
      dData = tmpFile.getGisFile().readRaster(0, i, xSize, 1);
      // Wertweise abarbeiten
      for (int j = 0; j < dData.length; j++) {
        // stringToken = stringTokenizer.nextToken();
        if (dData[j] == Float.MAX_VALUE)
          dData[j] = this.maxValue;
        else if (dData[j] == Float.MIN_NORMAL)
          dData[j] = this.minValue;
      }
      endFile.getGisFile().writeRaster(0, i, xSize, 1, dData);
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }
    }
    // datei schliessen
    endFile.getGisFile().unlock();
    tmpFile.getGisFile().unlock();
    endFile.getGisFile().refresh();
    return endFile;
  }


  /**
   * In case that this Connection is unused, you have to set the
   * sourceFileList before invoking this function.
   */
  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
//    if (!used) {
      this.method = AggregationMethod
          .fromString(restorer.loadString("AggregationMethod").orElse(null));
//    }
  }

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
    restoreWeighting(container.getPathRelativeToProject() + container.getOutputFileName(),
        restorer);
  }

  protected void restoreWeighting(String filePath, ISavableRestorer restorer) {
    restorer.loadRestorable("weighting", Weighting.class)
        .ifPresent(data -> fileWeightingMap.put(filePath, (Weighting) data));
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("AggregationMethod", method.toString());
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
    Weighting w = getWeighting(container);
    saver.save("weighting", w);
  }

  @Override
  public AttributeTable getData() {
    //TODO: make it nicer
    return super.getData();
  }

  //  @Override
//  public AttributeTable getData() {
//    AttributeTable dataMap = super.getData();
//
//    if (this.outputFile != null) {
//      Element tmpInfo;
//      try {
//        tmpInfo = this.outputFile.getGisFileInformationStorage().getConnectionElements().get(0);
//      } catch (IOException e) {
//        return dataMap;
//      }
//      if (tmpInfo != null) {
//        String sAggregationMethod = tmpInfo.getAttributeValue("AggregationMethod");
//        if (sAggregationMethod != null)
//          dataMap.put("AggregationMethod", sAggregationMethod);
//        List<Element> leFiles = tmpInfo.getChildren();
//        for (Element eFile : leFiles) {
//          dataMap.put("Filename", eFile.getAttributeValue("outputName"));
//
//          Element eWeight = eFile.getChild("weighting");
//          if (eWeight != null) {
//            dataMap.put("  Weighting", eWeight.getAttributeValue("weight"));
//            dataMap.put("  Inverse", eWeight.getAttributeValue("isNeg"));
//          }
//
//          Element eThreshold1 = eFile.getChild("threshold1");
//          if (eThreshold1 != null) {
//            String cond = eThreshold1.getAttributeValue("condition");
//            String condVal = eThreshold1.getAttributeValue("ConVal");
//            String condMode = eThreshold1.getAttributeValue("mode");
//            String assign = eThreshold1.getAttributeValue("assign");
//            String fullString = "if Value " + cond + " " + condVal + condMode + " then: " + assign;
//
//            dataMap.put("  Threshold1", fullString);
//          }
//          Element eThreshold2 = eFile.getChild("threshold2");
//          if (eThreshold2 != null) {
//            String cond = eThreshold2.getAttributeValue("condition");
//            String condVal = eThreshold2.getAttributeValue("ConVal");
//            String condMode = eThreshold2.getAttributeValue("mode");
//            String assign = eThreshold2.getAttributeValue("assign");
//            String fullString = "if Value " + cond + " " + condVal + condMode + " then: " + assign;
//
//            dataMap.put("  Threshold2", fullString);
//          }
//
//        }
//      }
//    }
//    return dataMap;
//  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> deps = new ArrayList<>();
    deps.add(MinMaxValues.NAME);
    deps.add(MeanValue.NAME);
    return deps;
  }

  public AggregationMethod getMethod() {
    return this.method;
  }

  public void setMethod(AggregationMethod method) {
    this.method = method;
  }

  @Override
  protected void solveDependencies() throws IOException {
    super.solveDependencies();
    for (int i = 0; i < getSourceFileList().size(); i++) {
      solveWeightingDependencies(getSourceFileList().get(i));
      solveThreasholdDependencies(getSourceFileList().get(i));
    }
  }

  protected void solveWeightingDependencies(IGisFileContainer fileContainer) {
  }

  protected void solveThreasholdDependencies(IGisFileContainer fileContainer) {
    Weighting weighting = getWeighting(fileContainer);
    Threshold tmpPenalty = weighting.penalty;
    if (!tmpPenalty.getActivate()) {
    } else {
      if (tmpPenalty.getMode().equals(Mode.percentage)) {
        MinMaxValues minMaxValue = (MinMaxValues) fileContainer.getInfoReader(MinMaxValues.NAME);
        float newMin = minMaxValue.getMinValue();
        float newMax = minMaxValue.getMaxValue();
        float distance = newMax - newMin;
        float perc = distance / 100;
        float newValue = newMin + tmpPenalty.getValuePerc() * perc;
        tmpPenalty.setValueReal(newValue);
      }
    }
    Threshold tmpReward = weighting.reward;
    if (!tmpReward.getActivate()) {
    } else {
      if (tmpReward.getMode().equals(Mode.percentage)) {
        MinMaxValues minMaxValue = (MinMaxValues) fileContainer.getInfoReader(MinMaxValues.NAME);
        float newMin = minMaxValue.getMinValue();
        float newMax = minMaxValue.getMaxValue();
        float distance = newMax - newMin;
        float perc = distance / 100;
        float newValue = newMin + tmpReward.getValuePerc() * perc;
        tmpReward.setValueReal(newValue);
      }
    }
  }

  @Override
  public void write() throws IOException, JDOMException {
  }

  @Override
  public void write(IGisFileContainerFactory fileContainerFactory) throws IOException {
    RasterFileContainer endFile = prepareOutputFile(fileContainerFactory);
    endFile.getGisFile().createNewFile(getSourceFileList().get(0).getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, -Float.MAX_VALUE);

    if (hasThreshold()) {
      RasterFileContainer tmpFile =
          (RasterFileContainer) fileContainerFactory.create(this.outputFileSet,
              "TMP" + this.outputFileName,
              this.outputFileSet.getPath()
                  + this.outputFileSet.getName()
                  + File.separator
                  + "TMP"
                  + this.outputFileName, null);
      tmpFile.getGisFile().createNewFile(getSourceFileList().get(0).getGisFile(),
          DataTypeHelper.RasterDataType.GDT_Float32, -Float.MAX_VALUE);

      if (this.method == AggregationMethod.ADDITIVE) {
        this.writeInFileAdditive(tmpFile);
      } else {
        this.writeInFileGeometric(tmpFile);
      }
      this.fillPlaceholder(tmpFile, endFile);
      if (!tmpFile.delete()) {
        log.error("Error deleting tmp file {}", tmpFile.getAbsolutePath());
      }
    } else {
      if (this.method == AggregationMethod.ADDITIVE) {
        this.writeInFileWithoutThesholdAdditive(endFile);
      } else {
        this.writeInFileWithoutThesholdGeometric(endFile);
      }
    }

    this.used = true;
  }

  @Override
  public void writeToContainer(RasterFileContainer file) {
  }

  protected void writeInFileAdditive(RasterFileContainer file) throws IOException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / getSourceFileList().get(0).getGisFile().getNRows();
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("Indexation Step 1/2 " + this.getOutputFileContainer().getName());
    }

//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
    float noDataValue = -Float.MAX_VALUE;

//    int xSize = getSourceFileList().get(0).getBand(RasterFileAccess.READ_ONLY).getXSize();
    int xSize = getSourceFileList().get(0).getGisFile().getNCols();

    float[] outputArray = new float[xSize];

//    float[][] doubleDoubleArray = new float[getSourceFileList().size()][xSize];
    float[][] doubleDoubleArray = new float[getSourceFileList().size()][];
    float thresholdValue = 0.0F;

    // Schleife 1 Anzahl der Zeilen
    this.rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < this.rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < getSourceFileList().size(); j++) {
        doubleDoubleArray[j] = getSourceFileList().get(j).getGisFile().readRaster(0, i, xSize, 1);
//        ((RasterFileContainer) getSourceFileList().get(j))
//            .getBand(RasterFileAccess.READ_ONLY)
//            .ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        outputArray[j] = noDataValue;
        for (int k = 0; k < getSourceFileList().size(); k++) {
          Weighting weighting = getWeighting(k);
          if (weighting.weight == 0.0F)
            continue;
          float tmpValue = doubleDoubleArray[k][j];
          float rewPenString;
          if (tmpValue == getSourceFileList().get(k).getGisFile().getNoDataValue()) {
            tmpValue = noDataValue;

          } else {
            rewPenString = this.calculateThreshold(tmpValue, k);
            if (rewPenString != 0) {
              if (rewPenString == Float.MIN_NORMAL)
                thresholdValue -= weighting.weight;
              else if (rewPenString == Float.MAX_VALUE)
                thresholdValue += weighting.weight;
            }
            tmpValue *= weighting.weight;
            if (weighting.negative)
              tmpValue *= (-1);
            if (outputArray[j] == noDataValue)
              outputArray[j] = 0.0F;
            outputArray[j] += tmpValue;
          }
          // s3-ENDE
        }
        if (outputArray[j] != noDataValue && outputArray[j] != Float.MIN_NORMAL && outputArray[j] != Float.MAX_VALUE) {
          if (outputArray[j] < this.minValue)
            this.minValue = outputArray[j];
          if (outputArray[j] > this.maxValue)
            this.maxValue = outputArray[j];
        }
        if (thresholdValue == 0) {

        } else if (thresholdValue < 0)
          outputArray[j] = Float.MIN_NORMAL;
        else if (thresholdValue > 0)
          outputArray[j] = Float.MAX_VALUE;
        thresholdValue = 0.0F;
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
      // Schleife1-Ende
    }

    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();

    // writeToContainer End
  }

  protected void writeInFileGeometric(RasterFileContainer file) throws IOException, GisFileException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (getSourceFileList().get(0).getGisFile().getNRows());
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("Indexation Step 1/2 " + this.getOutputFileContainer().getName());
    }

//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
    float noDataValue = -Float.MAX_VALUE;

    int xSize = getSourceFileList().get(0).getGisFile().getNCols();//((RasterFileContainer) getSourceFileList().get(0)).getBand(RasterFileAccess.READ_ONLY).getXSize();

    float[] outputArray = new float[xSize];

    float[][] doubleDoubleArray = new float[getSourceFileList().size()][xSize];
    float thresholdValue = 0.0F;

    // Schleife 1 Anzahl der Zeilen
    this.rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < this.rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < getSourceFileList().size(); j++) {
//        ((RasterFileContainer) getSourceFileList().get(j))
//            .getBand(RasterFileAccess.READ_ONLY)
//            .ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
        doubleDoubleArray[j] = getSourceFileList().get(j).getGisFile().readRaster(0, i, xSize, 1);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        outputArray[j] = noDataValue;
        for (int k = 0; k < getSourceFileList().size(); k++) {
          Weighting weighting = getWeighting(k);
          if (weighting.weight == 0.0F)
            continue;
          float tmpValue = doubleDoubleArray[k][j];
          float rewPenString;
          if (tmpValue == getSourceFileList().get(k).getGisFile().getNoDataValue()) {
            tmpValue = noDataValue;
          } else {
            rewPenString = this.calculateThreshold(tmpValue, k);
            if (rewPenString != 0) {
              if (rewPenString == Float.MIN_NORMAL)
                thresholdValue -= weighting.weight;
              else if (rewPenString == Float.MAX_VALUE)
                thresholdValue += weighting.weight;
            }
            float weightValue = weighting.weight;
            if (weighting.negative)
              weightValue *= (-1.0f);
            tmpValue = new Double(Math.pow(tmpValue, weightValue)).floatValue();
            if (outputArray[j] == noDataValue)
              outputArray[j] = 1.0F;
            outputArray[j] *= tmpValue;
          }
          // s3-ENDE
        }
        if (outputArray[j] != noDataValue && outputArray[j] != Float.MIN_NORMAL && outputArray[j] != Float.MAX_VALUE) {
          if (outputArray[j] < this.minValue)
            this.minValue = outputArray[j];
          if (outputArray[j] > this.maxValue)
            this.maxValue = outputArray[j];
        }
        if (thresholdValue == 0) {

        } else if (thresholdValue < 0)
          outputArray[j] = Float.MIN_NORMAL;
        else if (thresholdValue > 0)
          outputArray[j] = Float.MAX_VALUE;
        thresholdValue = 0.0F;
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
      // Schleife1-Ende
    }
    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();

    // writeToContainer End
  }

  private void writeInFileWithoutThesholdAdditive(RasterFileContainer file) throws IOException, GisFileException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / (getSourceFileList().get(0).getGisFile().getNRows());
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("Indexation: " + this.getOutputFileContainer().getName());
    }

//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
    float noDataValue = -Float.MAX_VALUE;
    int xSize = getSourceFileList().get(0).getGisFile().getNCols();//((RasterFileContainer) getSourceFileList().get(0)).getBand(RasterFileAccess.READ_ONLY).getXSize();
    float[] outputArray = new float[xSize];
    float[][] doubleDoubleArray = new float[getSourceFileList().size()][];

    // Schleife 1 Anzahl der Zeilen
    this.rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < this.rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < getSourceFileList().size(); j++) {
        doubleDoubleArray[j] = getSourceFileList().get(j).getGisFile()
            .readRaster(0, i, xSize, 1);
//        ((RasterFileContainer) getSourceFileList().get(j))
//            .getBand(RasterFileAccess.READ_ONLY)
//            .ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        outputArray[j] = noDataValue;
        for (int k = 0; k < getSourceFileList().size(); k++) {
          Weighting weighting = getWeighting(k);
          if (weighting.weight == 0.0F)
            continue;
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == getSourceFileList().get(k).getGisFile().getNoDataValue()) {
            tmpValue = noDataValue;
          } else {
            tmpValue *= weighting.weight;
            if (weighting.negative)
              tmpValue *= (-1);
            if (outputArray[j] == noDataValue)
              outputArray[j] = 0.0F;
            outputArray[j] += tmpValue;
          }
          // s3-ENDE
        }
        if (outputArray[j] != noDataValue && outputArray[j] != Float.MIN_NORMAL && outputArray[j] != Float.MAX_VALUE) {
          if (outputArray[j] < this.minValue)
            this.minValue = outputArray[j];
          if (outputArray[j] > this.maxValue)
            this.maxValue = outputArray[j];
        }
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
      // Schleife1-Ende
    }
    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();
    // writeToContainer End
  }

  private void writeInFileWithoutThesholdGeometric(RasterFileContainer file) throws IOException, GisFileException {
    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / getSourceFileList().get(0).getGisFile().getNRows();
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("Indexation: " + this.getOutputFileContainer().getName());
    }

//    Band bandOut = file.getBand(RasterFileAccess.UPDATE);
    float noDataValue = -Float.MAX_VALUE;
    int xSize = getSourceFileList().get(0).getGisFile().getNCols();//((RasterFileContainer) getSourceFileList().get(0)).getBand(RasterFileAccess.READ_ONLY).getXSize();
    float[] outputArray = new float[xSize];
    float[][] doubleDoubleArray = new float[getSourceFileList().size()][xSize];

    // Schleife 1 Anzahl der Zeilen
    this.rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < this.rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < getSourceFileList().size(); j++) {
//        getSourceFileList().get(j)
//            .getBand(RasterFileAccess.READ_ONLY)
//            .ReadRaster(0, i, xSize, 1, doubleDoubleArray[j]);
        doubleDoubleArray[j] = getSourceFileList().get(j).getGisFile().readRaster(0, i, xSize, 1);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        outputArray[j] = noDataValue;
        for (int k = 0; k < getSourceFileList().size(); k++) {
          Weighting weighting = getWeighting(k);
          if (weighting.weight == 0.0F)
            continue;
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == getSourceFileList().get(k).getGisFile().getNoDataValue()) {
            tmpValue = noDataValue;
          } else {
            float weightValue = weighting.weight;
            if (weighting.negative == true)
              weightValue *= (-1.0f);
            tmpValue = new Double(Math.pow(tmpValue, weightValue)).floatValue();// *=
            // this.weightList.get(k);
            if (outputArray[j] == noDataValue)
              outputArray[j] = 1.0F;
            outputArray[j] *= tmpValue;
          }
          // s3-ENDE
        }
        if (outputArray[j] != noDataValue && outputArray[j]
            != Float.MIN_NORMAL && outputArray[j]
            != Float.MAX_VALUE) {
          if (outputArray[j] < this.minValue)
            this.minValue = outputArray[j];
          if (outputArray[j] > this.maxValue)
            this.maxValue = outputArray[j];
        }
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
//      bandOut.WriteRaster(0, i, xSize, 1, outputArray);
      // Schleife1-Ende
    }
    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();
    // writeToContainer End
  }

  public enum AggregationMethod {
    ADDITIVE, GEOMETRIC;

    public static AggregationMethod fromString(String aggregationMethod) {
      if (aggregationMethod == null) {
        return ADDITIVE;
      } else {
        return AggregationMethod.valueOf(aggregationMethod);
      }
    }
  }

  public enum Assignment {
    min, max;

    public static Assignment fromString(String assign) {
      if (assign.equals("max")) {
        return Assignment.max;
      } else {
        return Assignment.min;
      }
    }
  }

  public enum Condition {
    l, g, lt, gt
  }

  public enum Mode {
    percentage, value;

    public static Mode fromString(String mode) {
      if (mode.equals("%")) {
        return percentage;
      } else {
        return value;
      }
    }
  }

  public static class Threshold implements IStorageData {
    private Boolean activate = false;
    private String condition;
    private float valuePerc;
    private Mode mode;
    private Assignment assignment;
    private float valueReal;

    public Threshold() {
      activate = false;
      valueReal = 0.0f;
      mode = Mode.value;
      condition = "";
      assignment = Assignment.min;
    }

    public Threshold(Boolean activate, String cond, float valPerc, float valReal, Mode mode, Assignment ass) {
      this.activate = activate;
      this.condition = cond;
      this.valuePerc = valPerc;
      this.mode = mode;
      this.assignment = ass;
      this.valueReal = valReal;
    }

    public Boolean getActivate() {
      return this.activate;
    }

    public void setActivate(Boolean activate) {
      this.activate = activate;
    }

    public Assignment getAssignment() {
      return this.assignment;
    }

    public void setAssignment(Assignment assignment) {
      this.assignment = assignment;
    }

    public String getCondition() {
      return this.condition;
    }

    public void setCondition(String condition) {
      this.condition = condition;
    }

    public Mode getMode() {
      return this.mode;
    }

    public void setMode(Mode mode) {
      this.mode = mode;
    }

    public float getValuePerc() {
      return this.valuePerc;
    }

    public void setValuePerc(float value) {
      this.valuePerc = value;
    }

    public float getValueReal() {
      return this.valueReal;
    }

    public void setValueReal(float valueReal) {
      this.valueReal = valueReal;
    }

    @Override
    public void restore(ISavableRestorer restorer) {
      this.activate = restorer.loadBoolean("activate").orElse(false);
      this.condition = restorer.loadString("condition").orElse("");
      this.valuePerc = restorer.loadFloat("ConVal").orElse(0.0F);
      this.mode = Mode.fromString(restorer.loadString("mode").orElse("%"));
      this.assignment = Assignment.fromString(restorer.loadString("assign").orElse("max"));
    }

    @Override
    public void save(ISavableSaver saver) {
      saver.save("activate", activate);
      saver.save("condition", condition);
      saver.save("ConVal", valuePerc);
      saver.save("mode", mode.toString());
      saver.save("assign", assignment.toString());
    }
  }

  public static class Weighting implements IStorageData {
    protected Float weight = 0f;
    protected Boolean negative = false;
    protected Threshold reward = new Threshold();
    protected Threshold penalty = new Threshold();

    public Weighting() {
    }

    public Float getWeight() {
      return weight;
    }

    public void setWeight(float weight) {
      this.weight = weight;
    }

    public Threshold getPenalty() {
      return penalty;
    }

    public Threshold getReward() {
      return reward;
    }

    public Boolean getNegative() {
      return negative;
    }

    public void setNegative(Boolean negative) {
      this.negative = negative;
    }

    @Override
    public void restore(ISavableRestorer restorer) {
      weight = restorer.loadFloat("weight").orElse(0.0F);
      negative = restorer.loadBoolean("isNeg").orElse(false);
      reward = (Threshold) restorer
          .loadRestorable("threshold1", Threshold.class)
          .orElse(new Threshold());
      penalty = (Threshold) restorer
          .loadRestorable("threshold2", Threshold.class)
          .orElse(new Threshold());
    }

    @Override
    public void save(ISavableSaver saver) {
      if (weight != null)
        saver.save("weight", weight);
      if (negative != null)
        saver.save("isNet", negative);
      if (reward != null)
        saver.save("threshold1", reward);
      if (penalty != null)
        saver.save("threshold2", penalty);
    }
  }
}

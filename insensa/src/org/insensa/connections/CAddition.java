/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.List;


public class CAddition extends AbstractRasterConnection {

  private static final String ADDITION = "addition";

  @Override
  protected void restoreFile(IGisFileContainer container, ISavableRestorer restorer) {
  }

  @Override
  protected void saveFile(IGisFileContainer container, ISavableSaver saver) {
  }

  @Override
  public void writeToContainer(RasterFileContainer file) throws IOException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName(ADDITION);
    }

    List<RasterFileContainer> gisFileList = getSourceFileList();
    file.getGisFile().createNewFile(gisFileList.get(0).getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, Float.MAX_VALUE);

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / gisFileList.get(0).getGisFile().getNRows();
    int xSize = gisFileList.get(0).getGisFile().getNCols();
    float[] outputArray = new float[xSize];
    float[][] floatFloatArray = new float[gisFileList.size()][];
    int rowCnt = gisFileList.get(0).getGisFile().getNRows();
    int valueCnt = 0;
    float sumValue = 0.0F;
    float noDataValue = Float.MAX_VALUE;
    for (int i = 0; i < rowCnt; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      for (int j = 0; j < gisFileList.size(); j++) {
        floatFloatArray[j] = gisFileList.get(j).getGisFile().readRaster(0, i, xSize, 1);
      }

      for (int j = 0; j < xSize; j++) {
        valueCnt = 0;
        sumValue = 0;

        for (int k = 0; k < gisFileList.size(); k++) {
          float tmpVal = floatFloatArray[k][j];
          if (!(tmpVal == gisFileList.get(k).getGisFile().getNoDataValue())) {
            valueCnt++;
            sumValue += tmpVal;
          }
        }
        if (valueCnt == 0)
          outputArray[j] = noDataValue;
        else {
          outputArray[j] = sumValue;
        }
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
    }
    for (int j = 0; j < gisFileList.size(); j++) {
      gisFileList.get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.WorkerStatusList;
import org.insensa.extensions.IPluginExec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class StackFilesThreadPools {
  private ExecutorService exeService;
  private List<Runnable> runnableList = new ArrayList<>();
  private WorkerStatusList workerList;


  public StackFilesThreadPools() {
    int prozCount = 1;//Environment.getMaxThreadsAvailable();
    this.exeService = Executors.newFixedThreadPool(prozCount);
  }

  public void addPluginExec(Runnable runnableObject) {
    if (!this.runnableList.contains(runnableObject)) {
      this.runnableList.add(runnableObject);
    }
  }

  //  public void executeThread(IPluginExec con) throws IOException {
  //    if (con.isUsed()) {
  //      return;
  //    }
  //
  //    if (this.workerList != null) {
  //      this.workerList.startAllProcesses("Execute Connection", 1);
  //    }
  //
  //    for (int i = 0; i < this.runnableList.size(); i++) {
  //      if (this.runnableList.get(i).equals(con)) {
  //        ((IPluginExec) this.runnableList.get(i))
  //            .setWorkerStatus(this.workerList.getWorkerStatus(0));
  //        this.exeService.execute(this.runnableList.get(i));
  //      }
  //    }
  //  }

  public void executeThreads() throws IOException {
    List<Runnable> runList = new ArrayList<>();
    for (Runnable runnable : this.runnableList) {
      if (!((IPluginExec) runnable).isUsed()) {
        runList.add(runnable);
      }
    }
    if (this.workerList != null) {
      this.workerList.startAllProcesses("Execute Connection", runList.size());
    }

    for (int i = 0; i < runList.size(); i++) {
      if (this.workerList != null) {
        ((IPluginExec) runList.get(i)).setWorkerStatus(this.workerList.getWorkerStatus(i));
      }
      this.exeService.execute(runList.get(i));
    }
  }

  public ExecutorService getExeService() {
    return this.exeService;
  }

  public List<Runnable> getRunnableList() {
    return this.runnableList;
  }

  public void removeConnection(IPluginExec conn) throws IOException {
    if (conn instanceof StackConnection) {
      return;
    }
    if (!this.runnableList.remove(conn)) {
      throw new IOException("StackFilesThreadPools: removeConnection: Connection "
          + conn.getId() + " not found");
    }
  }

  public void setWorkerList(WorkerStatusList workerList) {
    this.workerList = workerList;
  }
}

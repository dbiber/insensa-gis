/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.connections;

import org.insensa.Environment;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceDependency;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.model.storage.IStorageCloner;
import org.insensa.model.storage.JdomStorageCloner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ConnectionManager {
  private static final Logger log = LoggerFactory.getLogger(ConnectionManager.class);

  ExtensionManager exManager;
  private IStorageCloner cloner = new JdomStorageCloner();


  public ConnectionManager() {
    this.exManager = ExtensionManager.getInstance();
  }

  public ConnectionFileChanger getConnectionFileChanger(String connectionName) {
    if (connectionName.equalsIgnoreCase(StackConnection.NAME)) {
      return new StackConnection();
    }
    ClassLoader cl = this.exManager.getUrlClassLoader();
    ServiceList list = this.exManager.getConnectionServices().get(connectionName);
    if (list == null) {
      return null;
    }
    Service service = list.getService(ServiceType.EXEC);
    if (service == null) {
      return null;
    } else {
      try {
        ConnectionFileChanger connectionFileChanger;
        if (service.getScript() != null) {
          connectionFileChanger =
              new RRasterConnection(connectionName);
          Path absFilePath = service.getScript().extractFromFile();
          ((RRasterConnection) connectionFileChanger).setScriptPath(absFilePath.toString());
        } else {
          Class classConnection = cl.loadClass(service.getPackageName()
              + "." + service.getClassName());
          connectionFileChanger =
              (ConnectionFileChanger) classConnection.newInstance();
          connectionFileChanger.setId(connectionName);
        }
        connectionFileChanger.setInfoDependencies(
            service.getDependencies(ServiceDependency.DependencyType.SOURCE));
        connectionFileChanger.setInfoConnectionDependencies(
            service.getDependencies(ServiceDependency.DependencyType.TARGET));
        return connectionFileChanger;
      } catch (ClassNotFoundException
          | InstantiationException
          | IllegalAccessException
          | IOException e) {
        log.error("Error creating connection \"" + connectionName + "\"", e);
        return null;
      }
    }
  }

  public ConnectionFileChanger getConnectionFileChanger(String connectionName,
                                                        List<? extends IGisFileContainer> fileList,
                                                        IGisFileSet outputFileSet,
                                                        IGisFileContainer file) {
    ConnectionFileChanger tmpCon = this.getConnectionFileChanger(connectionName);
    if (tmpCon instanceof IScriptPluginExec) {
      Environment.restoreDefaultPluginVariables((IScriptPluginExec) tmpCon);
    }
    if (tmpCon == null) {
      throw new RuntimeException("Could not create Connection \"" + connectionName + "\"");
    }
    tmpCon.setSourceFileList(fileList);
    tmpCon.setOutputFileSet(outputFileSet);
    tmpCon.setOutputFileContainer(file);
    return tmpCon;
  }

  public void copyConnection(ConnectionFileChanger oldConnection, ConnectionFileChanger newConnection) {
    cloner.store(oldConnection.getId(), oldConnection);
    List<? extends IGisFileContainer> oldFileList = oldConnection.getSourceFileList();
    List<IGisFileContainer> fileList = new ArrayList<>();
    if (oldFileList == null || oldFileList.isEmpty()) {
      cloner.getRestorer().loadCollection("file", restorer -> {
        String outputPath = restorer.loadString("outputPath").get();
        String outputName = restorer.loadString("outputName").get();
        IGisFileContainer file = oldConnection.getOutputFileSet().getProject()
            .getFileByRelativePath(outputPath, outputName);
        fileList.add(file);
        file.addFileObserver(newConnection);
      });
    } else {
      fileList.addAll(oldFileList);
    }
    newConnection.setSourceFileList(fileList);
    newConnection.setOutputFileSet(oldConnection.getOutputFileSet());
    newConnection.restore(cloner.getRestorer());
  }

  public ConnectionFileChanger getConnectionFileChangerCopy(ConnectionFileChanger con) {
    String sCon = con.getId();
    ConnectionFileChanger newCon = this.getConnectionFileChanger(sCon);
    copyConnection(con, newCon);
    return newCon;
  }

  public List<String> getConnectionList() {
    List<String> lCon = new ArrayList<String>();
    EConnection[] myEConnection = EConnection.values();
    for (EConnection element : myEConnection) {
      lCon.add(element.name());
    }
    Iterator<String> iter = this.exManager.getConnectionServices().keySet().iterator();
    while (iter.hasNext()) {
      String name = iter.next();
      if (this.exManager.getConnectionServices().get(name).getService(ServiceType.EXEC) != null)
        lCon.add(name);
    }
    return lCon;
  }
}

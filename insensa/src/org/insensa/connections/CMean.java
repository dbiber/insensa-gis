/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.connections;

import org.insensa.IGisFileContainer;
import org.insensa.RasterFile;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CMean extends AbstractRasterConnection {
  public static final String NAME = "Mean";
  private boolean relative = false;
  private RasterFileContainer relativeSource;
//  private RasterFileContainer relativeSource;

  @Override
  public String getId() {
    return NAME;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("relative", relative);
  }

  @Override
  protected void saveFile(IGisFileContainer container,
                          ISavableSaver saver) {
    if (relativeSource == container) {
      saver.save("relativeSource", true);
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.relative = restorer
        .loadBoolean("relative")
        .orElse(false);
  }

  @Override
  protected void restoreFile(IGisFileContainer container,
                             ISavableRestorer restorer) {
    Boolean relativeSource = restorer.loadBoolean("relativeSource")
        .orElse(false);
    if (relativeSource) {
      this.relativeSource = (RasterFileContainer) container;
    }
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> depList = new ArrayList<String>();
    return depList;
  }

  public boolean getRelative() {
    return this.relative;
  }

  public void setRelative(boolean relative) {
    this.relative = relative;
  }

  public IGisFileContainer getRelativeSource() {
    return this.relativeSource;
  }

  public void setRelativeSource(IGisFileContainer relativeSource) {
    this.relativeSource = getSourceFileList().get(getSourceFileList().indexOf(relativeSource));
  }

  @Override
  public void writeToContainer(RasterFileContainer file) throws IOException {

    float tmpStatus = 0.0F;
    float tmpSteps = 100.0F / getSourceFileList().get(0).getGisFile().getNRows();
    if (this.workerStatus != null) {
      this.workerStatus.setProgressName(this.toString());
    }

    file.getGisFile().createNewFile(getSourceFileList().get(0).getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Float32, -9999.0);


    float noDataValue = -9999.0F;

    int xSize = getSourceFileList().get(0).getGisFile().getNCols();
    List<RasterFileContainer>newList = new ArrayList<>();
    newList.addAll(getSourceFileList());
    if (this.relative == true) {
      if (this.relativeSource != null) {
        newList.remove(this.relativeSource);
      } else {
        this.relativeSource = (RasterFileContainer) newList.get(0);
        newList.remove(0);
      }
    }
    float[][] doubleDoubleArray = new float[newList.size()][];
    float[] outputArray = new float[xSize];
//    float[] relativeArray = new float[xSize];
    float[] relativeArray = new float[0];
    int rowCount;
    float sumValue = 0.0F;
    int valueCount = 0;

    // Schleife 1 Anzahl der Zeilen
    rowCount = getSourceFileList().get(0).getGisFile().getNRows();
    for (int i = 0; i < rowCount; i++) {
      if (this.workerStatus != null) {
        tmpStatus += tmpSteps;
        this.workerStatus.refreshPercentage(tmpStatus);
      }

      if (this.relative) {
        relativeArray = this.relativeSource.getGisFile()
            .readRaster(0, i, xSize, 1);
      }

      // schleife 2 Anzahl der Dateien
      for (int j = 0; j < newList.size(); j++) {
        doubleDoubleArray[j] = newList.get(j).getGisFile().readRaster(0, i, xSize, 1);
      }

      // schleife 3 Anzahl der Elemente in einer Zeile
      for (int j = 0; j < xSize; j++) {
        valueCount = 0;
        sumValue = 0;
        // schleife 4 Anzahl der Dateien
        for (int k = 0; k < newList.size(); k++) {
          float tmpValue = doubleDoubleArray[k][j];
          if (tmpValue == newList.get(k).getGisFile().getNoDataValue()) {
          } else {
            valueCount++;
            sumValue += tmpValue;
          }

        }// s4-ENDE
        if (valueCount == 0) {
          outputArray[j] = noDataValue;
        } else if (this.relative) {
          if (relativeArray[j] == this.relativeSource.getGisFile().getNoDataValue()
              || relativeArray[j] == 0)
            outputArray[j] = noDataValue;
          else
            outputArray[j] = (((sumValue / valueCount)) / relativeArray[j]) * 100;
        } else
          outputArray[j] = sumValue / valueCount;
      }
      file.getGisFile().writeRaster(0, i, xSize, 1, outputArray);
    }// Schleife1-Ende
    if (this.relativeSource != null) {
      this.relativeSource.getGisFile().unlock();
    }
    for (int j = 0; j < getSourceFileList().size(); j++) {
      getSourceFileList().get(j).getGisFile().unlock();
    }
    file.getGisFile().unlock();
    file.getGisFile().refresh();
  }// writeToContainer End

}

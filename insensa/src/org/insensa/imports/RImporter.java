package org.insensa.imports;

import org.insensa.WorkerStatus;
import org.insensa.commands.ImportRScriptCommand;
import org.insensa.commands.ModelCommand;
import org.insensa.model.generic.IVariable;
import org.insensa.view.RefreshTreeNodeListener;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.helper.ViewCommandManager;

import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class RImporter extends  RasterFileImporter {

  private String script;
  private RefreshTreeNodeListener refreshTreeListener;
  private WorkerStatus workerStatus;


  public void setScriptPath(String script) {
    this.script = script;
  }

  @Override
  public void doImport(Map<String, IVariable> configuration) {
    TreeNode activeNode = super.view.getActiveTreeNode();
    refreshTreeListener = new RefreshTreeNodeListener((DefaultMutableTreeNode) activeNode, view);
    workerStatus = new DialogSingleProgress(view);
    ModelCommand command = new ImportRScriptCommand(fileSet,this.script,configuration,refreshTreeListener,view);
    command.setWorkerStatus(workerStatus);
    ViewCommandManager.executeCommands(view,refreshTreeListener,command);
  }

}

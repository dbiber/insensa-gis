/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.imports;

import org.insensa.GenericGisFileSet;
import org.insensa.WorkerStatus;
import org.insensa.commands.ImportShapeFilesCommandTest;
import org.insensa.commands.ModelCommand;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.model.generic.IVariable;
import org.insensa.view.RefreshTreeNodeListener;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class ShapeFileImporter implements IFileImporter {
  private static final Logger log = LoggerFactory.getLogger(ShapeFileImporter.class);

  private List<File> fileList;
  private GenericGisFileSet fileSet;
  private View view;
  private RefreshTreeNodeListener refreshTreeListener;
  private WorkerStatusCallback callback;
  private WorkerStatus workerStatus;

  @Override
  public void doImport(Map<String, IVariable> configuration) {
    TreeNode activeNode = view.getActiveTreeNode();
    refreshTreeListener = new RefreshTreeNodeListener((DefaultMutableTreeNode) activeNode, view);
    workerStatus = new DialogSingleProgress(view);

    ModelCommand command = new ImportShapeFilesCommandTest(fileSet,
        fileList.toArray(new File[fileList.size()]),true);
    command.setWorkerStatus(workerStatus);
    try
    {
      ViewCommandManager.executeCommands(view, refreshTreeListener, command);
    } catch (Exception e)
    {
      log.error("UNKNOWN",e);
    };
  }

  @Override
  public void setWorkerStatusCallback(WorkerStatusCallback callback) {

  }

  @Override
  public void setFileList(List<File> fileList) {
    this.fileList = fileList;
  }

  @Override
  public void setTargetFileSet(GenericGisFileSet fileSet) {
    this.fileSet = fileSet;
  }

  @Override
  public void setView(View view) {
    this.view = view;
  }

  @Override
  public FileDescription[] getFileDescriptions() {

    FileDescription[] descriptions = new FileDescription[1];

    FileDescription fileDescription = new FileDescription();
    fileDescription.fileExtensions.add(".shp");
    fileDescription.isDirectory = false;
    fileDescription.isFile = true;
    fileDescription.name = "ESRI Shape File";
    fileDescription.depth = 0;

    descriptions[0] = fileDescription;
    return descriptions;
  }
}

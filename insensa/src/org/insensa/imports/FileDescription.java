/*
 * Copyright (C) 2011-2013 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.imports;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dennis
 *
 */
public class FileDescription
{
	/**
	 * The File extensions to search for.<br/>
	 * empty means every file allowed 
	 */
	public List<String> fileExtensions=new ArrayList<String>();
	/**
	 * Specifies if the target file is a single file 
	 */
	public boolean isFile=true;
	
	/**
	 * Specifies if the target file is located in a Directory 
	 */
	public boolean isDirectory=false;
	
	/**
	 * If the target file is located in a Directory,
	 * this specifies the directory depth to search
	 * <b>WARNING:</b> A high value can cause the FileChoosers to take 
	 * an unpleasant amount of time 
	 */
	public int depth=0;
	
	/**
	 * The type or name of the search file (e.g. Java Class)
	 */
	public String name="All Files";
}

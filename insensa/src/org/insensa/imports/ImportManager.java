/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.imports;

import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


public class ImportManager
{
	private ExtensionManager exManager;


	public ImportManager()
	{
		this.exManager = ExtensionManager.getInstance();
	}

  public IFileImporter getFileImporter(String importerName) {
    ClassLoader cl = this.exManager.getUrlClassLoader();// InsensaGIS.class.getClassLoader();
    ServiceList list = this.exManager.getFileImporterServices().get(importerName);
    if (list == null) {
      throw new RuntimeException("No importer with name " + importerName + " found");
    }
    Service service = list.getService(ServiceType.EXEC);
    if (service == null) {
      throw new RuntimeException("No exec service found for importer with name " + importerName);
    } else {
      try {
        if ( service.getScript() != null) {
          RImporter rImporter = new RImporter();
          Path absFilePath = service.getScript().extractFromFile();
          rImporter.setScriptPath(absFilePath.toString());
          return rImporter;
        }
        Class classfileImporter = cl.loadClass(service.getPackageName() + "." + service.getClassName());
        return (IFileImporter) classfileImporter.newInstance();
      } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
        e.printStackTrace();
        return null;
      }
    }
  }

	public List<String> getFileImporterList()
	{
		List<String> nameList = new ArrayList<>();
    for (String name : this.exManager.getFileImporterServices().keySet()) {
      if (this.exManager.getFileImporterServices().get(name).getService(ServiceType.EXEC) != null) {
        nameList.add(name);
      }
    }
		return nameList;
	}
}

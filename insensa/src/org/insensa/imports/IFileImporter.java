/*
 * Copyright (C) 2011-2013 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.imports;

import org.insensa.GenericGisFileSet;
import org.insensa.commands.ModelCommand;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.model.generic.IVariable;
import org.insensa.view.View;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;

import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * @author dennis
 */
public interface IFileImporter {
  /**
   * This method is responsible for the actual importing.<br/>
   * This method gets called within the "event dispatch thread" (AWT).
   * For long running tasks, you have to create a new thread.<br/>
   * <br/><b><i>
   * It is recommend to create a </i></b>{@link ModelCommand}<b><i> end execute it with the </i></b>{@link ViewCommandManager}
   * <br/>
   *
   * @see ModelCommand
   * @see ViewCommandManager
   * @see IProcessFinishedListener
   */
  void doImport(Map<String, IVariable> configuration);

  void setWorkerStatusCallback(WorkerStatusCallback callback);

  void setFileList(List<File> fileList);

  void setTargetFileSet(GenericGisFileSet fileSet);

  void setView(View view);

  /**
   * Returns a list of FileDescriptor arrays.<br/>
   * FileDescriptors in arrays mean, that they have almost the same
   * characteristics and can be combined in a FileChooser view.<br/>
   * e.g. (.tiff and .asc) are both rasterfiles, searched as single files
   * and not stored in a Directory.
   */
  FileDescription[] getFileDescriptions();
}

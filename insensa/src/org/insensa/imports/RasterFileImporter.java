/*
 * Copyright (C) 2011-2013 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.imports;

import org.insensa.GenericGisFileSet;
import org.insensa.WorkerStatus;
import org.insensa.commands.ImportFilesCommand;
import org.insensa.commands.ModelCommand;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.model.generic.IVariable;
import org.insensa.view.RefreshTreeNodeListener;
import org.insensa.view.View;
import org.insensa.view.dialogs.DialogSingleProgress;
import org.insensa.view.dialogs.helper.ViewCommandManager;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class RasterFileImporter implements IFileImporter {

  protected List<File> fileList;
  protected WorkerStatusCallback callback;
  protected GenericGisFileSet fileSet;
  protected View view;
  private RefreshTreeNodeListener refreshTreeListener;
  private WorkerStatus workerStatus;


  public RasterFileImporter() {
  }

  /**
   * setter
   *
   * @see IFileImporter#setView(org.insensa.view.View)
   */
  @Override
  public void setView(View view) {
    this.view = view;
  }

  /**
   * @see IFileImporter#doImport(Map)
   */
  @Override
  public void doImport(Map<String, IVariable> configuration) {
    TreeNode activeNode = view.getActiveTreeNode();
    refreshTreeListener = new RefreshTreeNodeListener((DefaultMutableTreeNode) activeNode, view);
    workerStatus = new DialogSingleProgress(view);
    ModelCommand command = new ImportFilesCommand(fileSet,
        fileList.toArray(new File[fileList.size()]), true);
    command.setWorkerStatus(workerStatus);
    ViewCommandManager.executeCommands(view, refreshTreeListener, command);
  }

  /**
   * @see IFileImporter#setWorkerStatusCallback(org.insensa.helpers.WorkerStatusCallback)
   */
  @Override
  public void setWorkerStatusCallback(WorkerStatusCallback callback) {
    this.callback = callback;
  }

  /**
   * @see IFileImporter#setFileList(java.util.List)
   */
  @Override
  public void setFileList(List<File> fileList) {
    if (fileList == null || fileList.isEmpty())
      return;
    this.fileList = fileList;
  }

  /**
   * the fileset to add the new file
   *
   * @see IFileImporter#setTargetFileSet(GenericGisFileSet)
   */
  @Override
  public void setTargetFileSet(GenericGisFileSet fileSet) {
    this.fileSet = fileSet;
  }

  /**
   * @see IFileImporter#getFileDescriptions()
   */
  @Override
  public FileDescription[] getFileDescriptions() {
    FileDescription[] descArray = new FileDescription[2];
    FileDescription desc1 = new FileDescription();
    desc1.fileExtensions.add(".asc");
    desc1.fileExtensions.add(".tif");
    desc1.isDirectory = false;
    desc1.isFile = true;
    desc1.name = ".asc ; tiff";

    FileDescription desc2 = new FileDescription();
    desc2.fileExtensions.add("w001001.adf");
    desc2.isDirectory = true;
    desc2.isFile = false;
    desc2.name = "ESRI Grid Files";
    desc2.depth = 1;

    descArray[0] = desc1;
    descArray[1] = desc2;
    return descArray;
  }

}

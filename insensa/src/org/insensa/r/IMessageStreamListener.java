package org.insensa.r;

import java.io.File;

public interface IMessageStreamListener {
  void fileChanged(File file);
}

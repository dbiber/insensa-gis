package org.insensa.r;

public class RMessageStream {
  private String filePath;
  private RMessageType type;

  public RMessageStream(String filePath, RMessageType type) {
    this.filePath = filePath;
    this.type = type;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public RMessageType getType() {
    return type;
  }

  public void setType(RMessageType type) {
    this.type = type;
  }
}

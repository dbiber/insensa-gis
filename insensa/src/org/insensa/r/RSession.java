package org.insensa.r;


import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.SystemUtils;
import org.insensa.Environment;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.helpers.r.RCommandHelper;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableProcessor;
import org.insensa.updater.utils.PathUtils;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class RSession implements Closeable {
  private static final Logger log = LoggerFactory.getLogger(RSession.class);

  private RConnection rConnection;
  //  private IScriptConnection rConnection;
  private RMessageType type;
  private VariableProcessor variableProcessor;

  public RSession() {
  }

  public RSession(String ip) throws RserveException {
    if (!Environment.getInstance().getModel().getProperties().isRServeRunning()) {
      throw new RuntimeException("Rserve is not running!");
    }
    init(ip);
  }

  public RSession(RConnection rConnection) throws RserveException {
    this.rConnection = rConnection;
    init();
  }

  public void init(String ip) throws RserveException {
    this.rConnection = new RConnection(ip);
    init();
  }

  public void init() throws RserveException {
    variableProcessor = new VariableProcessor(new RScriptVariableAssigner(rConnection));
    if (SystemUtils.IS_OS_WINDOWS) {
      String wdFilepath = "";
      try {
        wdFilepath = FilenameUtils.separatorsToUnix(PathUtils.getInstance().getWindowsRDir().getAbsolutePath());
        eval("setwd('" + wdFilepath + "')");
      } catch (REXPMismatchException | REngineException e) {
        log.error("Error setting working directory, {}", wdFilepath);
      }
    }
    try {
      RCommandHelper.setDefaultCranLocation(rConnection);
      initInsensaPackage();
    } catch (REXPMismatchException | REngineException e) {
      log.error("Error setting default CRAN location", e);
    }
  }

  private void initInsensaPackage() throws REXPMismatchException, REngineException {
    this.eval("if(!requireNamespace(\"insensa\")) {\n"
        + "  if(!requireNamespace(\"devtools\")) {\n"
        + "    install.packages(\"devtools\")\n"
        + "  }\n"
        + "  devtools::install_bitbucket(\"dbiber/r-insensa\")\n"
        + "}\n");
  }

  public void assign(String name, String value) throws RserveException {
    rConnection.assign(name, value);
  }

  //TODO add method assign(String name, IValue value)
  public void assign(IVariable variable) {
    if (!variable.getValue().isPresent()) {
      log.warn("Variable {} has no value", variable.getName());
      return;
    }
    variableProcessor.assign(variable, variable.getName());
  }

  public REXP eval(String cmd) throws REXPMismatchException, REngineException {
    return RCommandHelper.evalAndPrint(rConnection, cmd);
  }

  public RMessageStream loadOutput(RMessageType type) throws REXPMismatchException, REngineException {
    this.type = type;
    File logFile = RCommandHelper.loadOutput(rConnection, type);
    return new RMessageStream(logFile.getAbsolutePath(), type);
  }

  public void unloadOuput() throws REXPMismatchException, REngineException {
    RCommandHelper.unloadOuput(rConnection, type);
  }

  public REXP sourceFile(File file) throws REXPMismatchException, REngineException {
    //To fix problam with "library" right after "install.package". Message, <library" not available
    RCommandHelper.evalAndPrint(rConnection, ".libPaths()");
    return RCommandHelper.sourceFile(rConnection, FilenameUtils.separatorsToUnix(file.getAbsolutePath()));
  }

  public void closeAndClear() throws REXPMismatchException, REngineException {
    REXP wd = RCommandHelper.evalAndPrint(rConnection, "getwd()");
    String sWd = wd.asString();
    log.info("Working Directory is : " + sWd);
    if (sWd.contains("tmp")) {
      try {
        //Just to get the Tailer to read the latest messages
        Thread.sleep(200);
      } catch (InterruptedException e) {
        log.error("Error sleeping thread");
      }
      RCommandHelper.evalAndPrint(rConnection, "unlink('" + sWd + "', recursive=TRUE)");
    }
    rConnection.close();
  }

  public RConnection getRawConnection() {
    return rConnection;
  }

//  public void assign(List<IGisFileContainer> sourceFileList) {
//    this.variableProcessor.getAssigner()
//        .assignContainerList("sourceFiles", sourceFileList);
//  }

  public void assignInsensaWs(IGisFileContainer file)
      throws REXPMismatchException, REngineException {
    File dataPath = Paths
        .get(FilenameUtils.separatorsToUnix(file.getParentModule().getFullPath()))
        .resolve("data")
        .toFile();
    if (!dataPath.exists()) {
      dataPath.mkdir();
    }

    eval("insensa:::setInsensaWs(\""
        + FilenameUtils.separatorsToUnix(dataPath.getAbsolutePath())
        + "\")");
  }

  public void assignOutputFile(String outputFilePath)
      throws REXPMismatchException, REngineException {
    eval("insensa:::setOutputFile(\""
        + FilenameUtils.separatorsToUnix(outputFilePath)
        + "\")");
  }

  public void assignSourceFiles(List<? extends IGisFileContainer> sourceFileList) {
    String s = sourceFileList
        .stream()
        .map(fc -> "'"
            + FilenameUtils.separatorsToUnix(fc.getAbsolutePath())
            + "'")
        .collect(Collectors.joining(","));
    s = "c( " + s + " )";
    try {
      eval("insensa:::setSourceFiles(" + s + ")");
    } catch (REngineException | REXPMismatchException ex) {
      log.error("Error assigning source files.", ex);
      throw new RuntimeException("Error assigning source files");
    }
  }

  public void assignInsensaCurrentFile(String absolutePath)
      throws REXPMismatchException, REngineException {
    eval("insensa:::setCurrentFile(\""
        + FilenameUtils.separatorsToUnix(absolutePath)
        + "\")");
  }

  public void assignInsensaIsActive()
      throws REXPMismatchException, REngineException {
    eval("insensa:::setActive()");
  }

  @Override
  public void close() {
    try {
      unloadOuput();
      closeAndClear();
    } catch (REXPMismatchException | REngineException e) {
      log.error("Error closing R Session", e);
    }
  }
}

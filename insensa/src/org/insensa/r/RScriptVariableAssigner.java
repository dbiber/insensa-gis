package org.insensa.r;

import org.apache.commons.io.FilenameUtils;
import org.insensa.Environment;
import org.insensa.IGisFileContainer;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.helpers.r.RCommandHelper;
import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.IVariableAssigner;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.ParameterListValue;
import org.insensa.model.generic.value.StringValue;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPInteger;
import org.rosuda.REngine.REXPList;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REXPString;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import javax.swing.table.DefaultTableModel;

public class RScriptVariableAssigner implements IVariableAssigner {

  private static final Logger log = LoggerFactory.getLogger(RScriptVariableAssigner.class);

  private RConnection rConnection;

  public RScriptVariableAssigner(RConnection rConnection) {
    this.rConnection = rConnection;
  }

  private void assignList(String variableName, List<String> list) {
//    RList rList = new RList();
//    list.forEach(parameter -> rList.add(new REXPString(parameter)));
//    REXPList rexpList = new REXPList(rList);
//    REXPString rexpList = new REXPString((String[]) rList.toArray());
    try {

      rConnection.assign(variableName, list.stream().toArray(String[]::new));
//      rConnection.assign(variableName, rexpList);
    } catch (RserveException e) {
      log.error("Error assigning variable " + variableName, e);
    } catch (REngineException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void assignDirectory(IVariable variable, String name) {
    try {
      rConnection.assign(name,
          FilenameUtils.separatorsToUnix(variable.getValue().get().toScriptString() + "/"));
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }

  @Override
  public void assignFile(IVariable variable, String name) {
    try {
      rConnection.assign(name,
          FilenameUtils.separatorsToUnix(variable.getValue().get().toScriptString()));
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }

  @Override
  public void assignString(IVariable variable, String name) {
    try {
      rConnection.assign(name,
          FilenameUtils.separatorsToUnix(variable.getValue().get().toScriptString()));
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }

  @Override
  public void assignBoolean(IVariable variable, String name) {
    try {
      rConnection.assign(name, variable.getValue().get().toScriptString());
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }


  @Override
  public void assignNumeric(IVariable variable, String name) {
    try {
      rConnection.assign(name,
          new REXPDouble(Double.parseDouble(variable.getValue().get().toScriptString())));
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }

  @Override
  public void assignSelectOne(IVariable variable, String name) {
    IParameter param = (IParameter) variable.getValue().get();
    try {
      rConnection.assign(name, param.getValue());
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }


  @Override
  public Map<String, IVariable> assignInternal(IVariable variable, String name) {
    assignString(variable, name);

    Optional<IValue> value = variable.getValue();
    if (!value.isPresent()) {
      log.error("Variable {} does not contain a value.", name);
      return Collections.emptyMap();
    }
    StringValue val = (StringValue) value.get();
    IGisFileContainer container = Environment.getInstance()
        .getModel().getActiveProject()
        .getFileByRelativePath(val.getValue());

    if (container == null) {
      log.error("Stack value \"{}\" for variable \"{}\" "
              + "could not be found in project.", ((StringValue) value.get()).getValue(),
          name);
      return Collections.emptyMap();
    }

    container.getConnectionFileChanger().getVariableList();

    assignGisFileContainer(name + "_sources",
        container.getConnectionFileChanger().getSourceFileList());

    if (container.getConnectionFileChanger() instanceof IScriptPluginExec) {
      return ((IScriptPluginExec) container.getConnectionFileChanger()).getVariables();
    } else {
      return Collections.emptyMap();
    }

  }

  private void assignGisFileContainer(String name, List<? extends IGisFileContainer> fileContainers) {
    List<String> paths = fileContainers.stream()
        .map(IGisFileContainer::getAbsolutePath)
        .collect(Collectors.toList());

    assignList(name, paths);
  }

  @Override
  public void assignMultiple(IVariable variable, String name) {
    List<IParameter> params = ((ParameterListValue) variable
        .getValue().get()).getDataList();
    RList rList = new RList();
    params.forEach(parameter -> rList.add(new REXPString(parameter.getValue())));
    REXPList rexpList = new REXPList(rList);
    try {
      rConnection.assign(name, rexpList);
    } catch (RserveException e) {
      log.error("Error assigning variable " + name, e);
    }
  }


  @Override
  public void assignTable(IVariable variable, String name) {

    List<IParameter> params = ((ParameterListValue) variable.getValue().get()).getDataList();
    StringBuilder cmd = new StringBuilder(name + " <- as.data.frame(rbind(");
    for (IParameter parameter : params) {
      cmd.append("c(");
      cmd.append(parameter.getValue());
      cmd.append(")");
      if (params.indexOf(parameter) < params.size() - 1) {
        cmd.append(",");
      }
    }
    cmd.append("))");
    try {
      rConnection.parseAndEval(cmd.toString());
    } catch (REngineException | REXPMismatchException e) {
      log.error("Error assigning table " + name, e);
    }
    variable.getParameter("header").ifPresent(iParameter -> {
      StringJoiner joiner = new StringJoiner(",");
      for (IParameter headParam : iParameter.getParameter()) {
        joiner.add("\"" + headParam.getValue()+"\"");
      }
      try {
        rConnection.parseAndEval("names(" + name + ") <- " + "c(" + joiner.toString() + ")");
      } catch (REngineException | REXPMismatchException e) {
        log.error("Error assigning table " + name, e);
      }
    });
  }

  @Override
  public void assignMapFloatLong(IVariable variable, String name) {
    Map<Float, Long> floatLongMap = (Map<Float, Long>) variable.getValue().get();
    double[] keys = new double[floatLongMap.size()];
    int[] values = new int[floatLongMap.size()];
    int index = 0;
    for (Map.Entry<Float, Long> mapEntry : floatLongMap.entrySet()) {
      keys[index] = mapEntry.getKey();
      values[index] = mapEntry.getValue().intValue();
      index++;
    }
    try {
      REXP rexp = REXP.createDataFrame(new RList(new REXP[]{
          new REXPDouble(keys),
          new REXPInteger(values)
      }));

      rConnection.assign(name, rexp);
    } catch (RserveException | REXPMismatchException e) {
      log.error("Error assigning variable " + name, e);
    }
  }

  @Override
  public void assignRData(IVariable variable, String name) {
    String filePath = FilenameUtils
        .separatorsToUnix(variable.getValue().get().toScriptString());
    try {
      eval(name + " <- readRDS(\"" + filePath + "\")");
    } catch (REXPMismatchException | REngineException e) {
      log.error("Error assigning variable " + name, e);
    }
  }

//  @Override
//  public void assignContainerList(String name,
//                                  List<IGisFileContainer> gisFileContainers) {
//
//    String s = gisFileContainers
//        .stream()
//        .map(fc -> "'" + fc.getAbsOutputDirectoryPath() + "'")
//        .collect(Collectors.joining(","));
//    s = "c( " + s  + " )";
//    try {
//      eval(name + "<-" + s);
//    } catch (REngineException | REXPMismatchException ex) {
//      log.error("Error assigning source files.", ex);
//      throw new RuntimeException("Error assigning source files");
//    }
//  }


  public void assignString(String name, String value) {
    try {
      rConnection.assign(name, value);
    } catch (RserveException e) {
      log.error("Error assigning String " + name, e);
    }
  }

  private REXP eval(String cmd) throws REXPMismatchException, REngineException {
    return RCommandHelper.evalAndPrint(rConnection, cmd);
  }
}

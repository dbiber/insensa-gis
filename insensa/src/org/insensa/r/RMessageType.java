package org.insensa.r;

public enum RMessageType {
  MESSAGE("'message'"),
  OUTPUT("'output'"),
  ALL("c('output', 'message')");

  private final String messageType;

  RMessageType(String messageType) {
    this.messageType = messageType;
  }

  @Override
  public String toString() {
    return messageType;
  }
}

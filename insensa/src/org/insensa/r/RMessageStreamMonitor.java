package org.insensa.r;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class RMessageStreamMonitor {

  private static final Logger log = LoggerFactory.getLogger(RMessageStreamMonitor.class);

  private static RMessageStreamMonitor instance = new RMessageStreamMonitor();
//  private IMessageStreamListener messageStreamListener = null;
  private ReadWriteLock lock = new ReentrantReadWriteLock();
  private ConcurrentMap<String, Tailer> tailerMap = new ConcurrentHashMap<>();
  private TailerListener mainTailerListener;
  private List<TailerListener> childTailerListeners;

  private RMessageStreamMonitor() {
    childTailerListeners = new ArrayList<>();
    mainTailerListener = new TailerListenerAdapter(){
      @Override
      public void fileNotFound() {
        super.fileNotFound();
        log.info("Find deleted file, try removing handler");
        List<String> result = tailerMap.entrySet()
            .stream()
            .filter(entry -> !new File(entry.getKey()).exists())
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());
        result.forEach(s -> {
          log.info("Removing file {}", s);
          tailerMap.get(s).stop();
          tailerMap.remove(s);
        });
      }

      @Override
      public void handle(String line) {
        lock.readLock().lock();
        try {
          for (TailerListener tailerListener : childTailerListeners) {
            tailerListener.handle(line);
          }
        } finally {
          lock.readLock().unlock();
        }
      }
    };
  }

  public void addTailerLister(TailerListener listener) {
    lock.writeLock().lock();
    try {
      childTailerListeners.add(listener);
    } finally {
      lock.writeLock().unlock();
    }
  }

  public void removeTailerListener(TailerListener listener) {
    lock.writeLock().lock();
    try {
      childTailerListeners.remove(listener);
    } finally {
      lock.writeLock().unlock();
    }
  }

  static public RMessageStreamMonitor getInstance() {
    return instance;
  }

  public void registerMessageStream(RMessageStream stream) {
    if(tailerMap.containsKey(stream.getFilePath())) {
      removeMessageStream(stream);
    }
    Tailer tailer = Tailer.create(new File(stream.getFilePath()),mainTailerListener,100);
    tailerMap.put(stream.getFilePath(),tailer);
  }

  public void removeMessageStream(RMessageStream stream) {
    tailerMap.get(stream.getFilePath()).stop();
    tailerMap.remove(stream.getFilePath());
  }


}

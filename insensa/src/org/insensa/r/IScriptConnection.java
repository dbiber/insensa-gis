package org.insensa.r;

import org.insensa.model.generic.IVariable;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RserveException;

interface IScriptConnection {

  Object raw();

  void close();

  void assignDirectory(IVariable variable) throws RserveException;

  void assignString(IVariable variable) throws RserveException;

  void assignNumeric(IVariable variable) throws RserveException;

  void assignSelectOne(IVariable variable) throws RserveException;

  void assignFile(IVariable variable) throws RserveException;

  void assignMultiple(IVariable variable) throws RserveException;

  void assignMapFloatLong(IVariable variable) throws REXPMismatchException, RserveException;

  void assignRData(IVariable variable) throws REXPMismatchException, REngineException;

  void assignString(String name, String value) throws RserveException;
}

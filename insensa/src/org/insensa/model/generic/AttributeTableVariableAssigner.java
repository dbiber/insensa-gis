package org.insensa.model.generic;

import org.apache.commons.io.FilenameUtils;
import org.insensa.helpers.AttributeTable;
import org.insensa.model.generic.value.ParameterListValue;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class AttributeTableVariableAssigner implements IVariableAssigner {

  AttributeTable table;

  public AttributeTableVariableAssigner(AttributeTable table) {
    this.table = table;
  }

  public AttributeTableVariableAssigner() {
    table = new AttributeTable();
  }

  @Override
  public void assignDirectory(IVariable variable, String name) {
    variable.getValue().ifPresent(iValue -> {
      table.put(variable.getName(),
          FilenameUtils.separatorsToUnix(
              iValue.toScriptString() + "/"));
    });
  }

  @Override
  public void assignString(IVariable variable, String name) {
    variable.getValue().ifPresent(iValue -> {
      table.put(variable.getName(),
          FilenameUtils.separatorsToUnix(
              iValue.toScriptString()));
    });
  }

  @Override
  public void assignNumeric(IVariable variable, String name) {
    assignString(variable, variable.getName());
  }

  @Override
  public void assignSelectOne(IVariable variable, String name) {
    IParameter parameter = (IParameter) variable.getValue().get();
    table.put(name, parameter.getValue());
  }

  @Override
  public void assignFile(IVariable variable, String name) {
    assignString(variable, variable.getName());
  }

  @Override
  public void assignMultiple(IVariable variable, String name) {
    List<IParameter> params = ((ParameterListValue) variable
        .getValue().get()).getDataList();
    table.put(name,"");
    params.forEach(iParameter -> {
      table.put("", iParameter.getName());
    });
  }

  @Override
  public void assignTable(IVariable variable, String name) {
    List<IParameter> params = ((ParameterListValue)variable
        .getValue().get()).getDataList();
    table.put(name, "");
    params.forEach(iParameter -> {
      table.put("", iParameter.getValue());
    });
  }

  @Override
  public void assignMapFloatLong(IVariable variable, String name) {

  }

  @Override
  public void assignRData(IVariable variable, String name) {
    variable.getValue().ifPresent(iValue -> {
      String path = FilenameUtils.separatorsToUnix(variable.getValue().get().toScriptString());
      table.put("RData: "+name,path);
    });
  }

  @Override
  public void assignBoolean(IVariable variable, String name) {
    variable.getValue().ifPresent(iValue -> {
      String val = variable.getValue().get().toScriptString();
      table.put(name, val);
    });
  }

  @Override
  public Map<String, IVariable> assignInternal(IVariable variable, String name) {
    assignString(variable, variable.getName());
    return Collections.emptyMap();
  }


}

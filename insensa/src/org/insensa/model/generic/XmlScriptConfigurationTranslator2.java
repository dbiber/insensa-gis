package org.insensa.model.generic;

import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.JdomRestorer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class XmlScriptConfigurationTranslator2
    implements IScriptConfigurationTranslator {

  ISavableRestorer restorer;

  private Map<String, IVariable> selectedGroup = new LinkedHashMap<>();
  private Map<String, IVariable> mainGroup = new LinkedHashMap<>();
  private Map<String, Map<String, IVariable>> groupVariableMap =
      new LinkedHashMap<>();

  private File xmlFile = null;
  private String xmlString = null;
  private InputStream xmlStream = null;

  public XmlScriptConfigurationTranslator2() {
  }

  public XmlScriptConfigurationTranslator2(File xmlFile) {
    this.xmlFile = xmlFile;
//    restorer = new XmlConfigurationRestorer(xmlFile);
  }

  public XmlScriptConfigurationTranslator2(String xmlString) {
    this.xmlString = xmlString;
//    restorer = new XmlConfigurationRestorer(xmlString);
  }

  public XmlScriptConfigurationTranslator2(InputStream xmlStream) {
    this.xmlStream = xmlStream;
  }

  public void parse() throws ScriptParseException {
    if (xmlFile != null) {
      parse(xmlFile);
    } else if (xmlString != null) {
      parse(xmlString);
    } else if (xmlStream != null) {
      parse(xmlStream);
    } else {
      throw new ScriptParseException("Error Parsing, no input found, " +
          "neither file nor string or stream is available. " +
          "You probably did not use the correct constructor");
    }
  }

  @Override
  public void parse(File xmlFile) throws ScriptParseException {
    restorer = new JdomRestorer(xmlFile);
    buildVariables();
  }

  @Override
  public void parse(String xmlString) throws ScriptParseException {
    restorer = new JdomRestorer(xmlString);
    buildVariables();
  }

  @Override
  public void parse(InputStream stream) throws ScriptParseException {
    restorer = new JdomRestorer(stream);
    buildVariables();
  }

  /**
   * TODO: Add Schema Validation
   */
  private void buildVariables()
      throws ScriptParseException {
    //TODO: Do not clear to keep the variables !!!
    Map<String, IVariable> mSelectedGroup = new LinkedHashMap<>();
    Map<String, IVariable> mMainGroup = new LinkedHashMap<>();
    Map<String, Map<String, IVariable>> mGroupVariableMap = new LinkedHashMap<>();


    List<IRestorableData> data = restorer
        .loadCollection("variable", VariableImpl.class);

    for (IRestorableData iRestorableData: data) {
      VariableImpl var = (VariableImpl) iRestorableData;
      Optional<IParameter> optionalParam = var.getParameter("collection");
      if (optionalParam.isPresent()) {
        if (optionalParam.get().getParameter().isEmpty()) {
          throw new ScriptParseException("Found collection param without children");
        }
      }
      if (!var.getGroups().isEmpty()) {
        for (String s: var.getGroups()) {
          mGroupVariableMap.computeIfAbsent(s, s1 -> new LinkedHashMap<>());
          Map<String, IVariable> selGroup = mGroupVariableMap.get(s);
          Map<String, IVariable> oldGroup = groupVariableMap.get(s);
          buildVariableWithGroup(selGroup, oldGroup, var);
        }
      } else {
        buildVariableWithGroup(mMainGroup, mainGroup, var);
      }
    }

    clear();
    mainGroup.putAll(mMainGroup);
    selectedGroup.putAll(mSelectedGroup);
    groupVariableMap.putAll(mGroupVariableMap);
    triggerInitialEvents();
  }

  private void buildVariableWithGroup(Map<String, IVariable> group,
                                      Map<String, IVariable> oldGroup,
                                      VariableImpl vars)
      throws ScriptParseException {
    if (oldGroup != null) {
      IVariable oldVar = oldGroup.get(vars.getName());
      //If this variable existed previously AND there are currently
      //no values available, restore the old values
      if (oldVar != null && !vars.getValue().isPresent()) {
        oldVar.getValue().ifPresent(vars::setValue);
      }
    }

    group.put(vars.getName(), vars);
  }

  @Override
  public List<IVariable> getVariables() {
    List<IVariable> varList = new ArrayList<>(mainGroup.values());
    varList.addAll(selectedGroup.values());
    return varList;
  }

  @Override
  public Optional<IVariable> getVariable(String name) {
    if (mainGroup.get(name) != null) {
      return Optional.of(mainGroup.get(name));
    } else if (selectedGroup.get(name) != null) {
      return Optional.of(selectedGroup.get(name));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public void setDefaultValues(Map<String, IVariable> vars) {

    vars.forEach((s, variable) -> {
      if (!variable.getValue().isPresent()) {
        return;
      }
      if (mainGroup.containsKey(s)) {
        mainGroup.get(s).setValue(variable.getValue().get());
      }
    });

    this.groupVariableMap.forEach((s, stringIVariableMap) -> {
      vars.forEach((s1, variable) -> {
        if (variable.getValue().isPresent() && stringIVariableMap.get(s1) != null) {
          stringIVariableMap.get(s1).setValue(variable.getValue().get());
        }
      });
    });
    triggerInitialEvents();
  }

  /**
   * Events triggered by default values
   */
  private void triggerInitialEvents() {
    List<IVariable> variables = getVariables();
    variables.forEach(variable -> {
      if (variable.getValue().isPresent()) {
        Optional<IEvent> fEvent = variable
            .getEvents()
            .stream()
            .filter(event -> {
              if (event.getOnValue() == null || event.getOnValue().isEmpty()) {
                return false;
              }

              IValue selValue = variable.getValue().get();
              if(selValue instanceof IParameter) {
                return event.getOnValue().contains(((IParameter)selValue).getValue());
              }
              for (String searchEv : event.getOnValue()) {
                if (searchEv.equalsIgnoreCase(selValue.toScriptString())) {
                  return true;
                }
              }
              return event.getOnValue().contains(selValue.toScriptString());
            }).findFirst();
        fEvent.ifPresent(this::triggerEvent);
      }
    });
  }

  private void triggerEvent(IEvent event) {
    setActiveGroup(event.getShowGroup());
  }

  @Override
  public void setActiveGroup(String activeGroup) {
    this.selectedGroup.clear();
    if (activeGroup.equals("main")) {
    } else {
      Map<String, IVariable> group = groupVariableMap.get(activeGroup);
      if (group != null) {
        this.selectedGroup.putAll(group);
      }
    }
  }

  @Override
  public void executeFunction(String function, IValue value) {

  }

  private void clear() {
    this.mainGroup.clear();
    this.groupVariableMap.clear();
    this.selectedGroup.clear();
  }

  @Override
  public void close() throws IOException {

  }
}

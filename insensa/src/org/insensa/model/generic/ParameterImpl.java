package org.insensa.model.generic;

import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ParameterImpl implements IParameter {

  private String name;
  private String value;
  private Map<String, IParameter> parameterMap;

  public ParameterImpl() {
    parameterMap = new LinkedHashMap<>();
  }

  public ParameterImpl(String name, String value) {
    this();
    this.name = name;
    this.value = value;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getValue() {
    return value;
  }

  @Override
  public List<IParameter> getParameter() {
    return new ArrayList<>(parameterMap.values());
  }

  @Override
  public Optional<IParameter> getParameter(String name) {
    return Optional.ofNullable(parameterMap.get(name));
  }

  @Override
  public void addParameter(IParameter parameter) {
    this.parameterMap.put(parameter.getName(), parameter);
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    name = restorer.loadString("name").get();
    value = restorer.loadString("value").get();
    List<IRestorableData> paramData = restorer
        .loadCollection("param", ParameterImpl.class);
    for (IRestorableData iParamData : paramData) {
      addParameter((IParameter) iParamData);
    }
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("param", saver1 -> {
      saver1.save("name", name);
      saver1.save("value", value);
      if (parameterMap != null) {
        parameterMap.forEach(saver1::save);
      }
    });
  }

  @Override
  public String toScriptString() {
    return value;
  }
}

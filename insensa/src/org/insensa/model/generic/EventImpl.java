package org.insensa.model.generic;

import org.apache.commons.lang3.StringUtils;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class EventImpl implements IEvent {

  private List<String> valueList;
  private String showGroup;
  private String function;
  private String type;

  public EventImpl() {
    valueList = new ArrayList<>();
  }

  @Override
  public List<String> getOnValue() {
    return valueList;
  }

  public void setOnValue(List<String> valueList) {
    this.valueList = valueList;
  }

  @Override
  public String getShowGroup() {
    return showGroup;
  }

  public void setShowGroup(String showGroup) {
    this.showGroup = showGroup;
  }

  @Override
  public String getFunction() {
    return function;
  }

  public void setFunction(String function) {
    this.function = function;
  }

  @Override
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    this.function = restorer.loadString("function").orElse("");
    this.type = restorer.loadString("type").orElse("");
    this.showGroup = restorer.loadString("showGroup").orElse("");
    Optional<String> opOnValue = restorer.loadString("onValue");
    opOnValue.ifPresent(s -> setOnValue(
        Arrays.asList(StringUtils.split(s, ','))));
  }

  @Override
  public void save(ISavableSaver saver) {
    if (!function.isEmpty()) {
      saver.save("function", function);
    }
    if (!type.isEmpty()) {
      saver.save("type", type);
    }
    if (!showGroup.isEmpty()) {
      saver.save("showGroup", showGroup);
    }
    if (!getOnValue().isEmpty()) {
      saver.save("onValue", StringUtils.join(getOnValue(), ','));
    }
  }
}

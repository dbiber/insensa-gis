package org.insensa.model.generic;

import java.util.Map;

public interface IVariableAssigner {
  void assignDirectory(IVariable variable, String name);

  void assignString(IVariable variable, String name);

  void assignNumeric(IVariable variable, String name);

  void assignSelectOne(IVariable variable, String name);

  void assignFile(IVariable variable, String name);

  void assignMultiple(IVariable variable, String name);

  void assignMapFloatLong(IVariable variable, String name);

  void assignRData(IVariable variable, String name);

  void assignBoolean(IVariable variable, String name);

  Map<String, IVariable> assignInternal(IVariable variable, String name);

  void assignTable(IVariable variable, String name);

//  void assignContainerList(String name, List<IGisFileContainer> gisFileContainers);
}

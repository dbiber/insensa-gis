package org.insensa.model.generic;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.ex.ConversionException;
import org.apache.commons.configuration2.io.FileHandler;
import org.apache.commons.configuration2.tree.ImmutableNode;
import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.text.html.Option;

public class XmlConfigurationRestorer implements ISavableRestorer {

  private InputStream xmlStream;
  private XMLConfiguration conf;
  private HierarchicalConfiguration<ImmutableNode> currentConf;

  public XmlConfigurationRestorer(InputStream stream)
      throws ScriptParseException {
    this.xmlStream = stream;
    conf = new XMLConfiguration();
    currentConf = conf;
    FileHandler fh = new FileHandler(conf);
    try {
      fh.load(stream);
    } catch (ConfigurationException e) {
      throw new ScriptParseException("Could not load input stream", e);
    }
  }

  @Override
  public Optional<String> loadString() {
    return Optional.empty();
  }

  @Override
  public Optional<String> loadString(String name) {
    return Optional.ofNullable(currentConf.getString("[@" + name + "]"));
  }

  @Override
  public Optional<Boolean> loadBoolean(String name) {
    try {
      return Optional.of(currentConf.getBoolean("[@" + name + "]"));
    } catch (ConversionException ex) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Integer> loadInteger(String name) {
    try {
      return Optional.of(currentConf.getInt("[@" + name + "]"));
    } catch (ConversionException ex) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Long> loadLong(String name) {
    try {
      return Optional.of(currentConf.getLong("[@" + name + "]"));
    } catch (ConversionException ex) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Float> loadFloat(String name) {
    try {
      return Optional.of(currentConf.getFloat("[@" + name + "]"));
    } catch (ConversionException ex) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Double> loadDouble(String name) {
    try {
      return Optional.of(currentConf.getDouble("[@" + name + "]"));
    } catch (ConversionException ex) {
      return Optional.empty();
    }
  }

  @Override
  public void loadByteArray(String name, byte[] data) {

  }

  @Override
  public void loadRestorable(String name, Consumer<ISavableRestorer> data) {
    HierarchicalConfiguration<ImmutableNode> tmpConf = currentConf;
    currentConf = currentConf.configurationAt(name);
    data.accept(this);
  }

  @Override
  public void loadCollection(String name, Consumer<ISavableRestorer> data) {

  }

  @Override
  public void loadCollection(String name, Function<ISavableRestorer, Boolean> data) {

  }

  @Override
  public void loadCollection(String name, BiConsumer<String, ISavableRestorer> data) {

  }

  @Override
  public void loadCollection(String name, BiFunction<String, ISavableRestorer, Boolean> data) {

  }

  @Override
  public List<IRestorableData> loadCollection(String name, Class mClass) {
    return null;
  }

  @Override
  public void loadMap(String name, BiConsumer<String, ISavableRestorer> data) {

  }

  @Override
  public Optional<IRestorableData> loadRestorable(String value, Class restorableClass) {
    return Optional.empty();
  }
}

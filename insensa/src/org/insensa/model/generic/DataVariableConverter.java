package org.insensa.model.generic;

import org.insensa.model.generic.value.DoubleValue;
import org.insensa.model.generic.value.FloatValue;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.IntegerValue;
import org.insensa.model.generic.value.StringValue;
import org.insensa.model.storage.ISavableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class DataVariableConverter implements ISavableSaver {

  IVariable currentVar;

  Map<String, IVariable> variableMap = new HashMap<>();
  private ISavableRestorer restorer;
  private VariableBuilder builder;

  public DataVariableConverter() {
  }

  @Override
  public void save(String name, String data) {
    IVariable var = new VariableImpl(name, VariableType.STRING);
    IValue stringValue = new StringValue(data);
    var.setValue(stringValue);
    variableMap.put(name, var);
  }

  @Override
  public void save(String name, Boolean data) {
  }

  @Override
  public void save(String name, Integer data) {
    IVariable var = new VariableImpl(name, VariableType.NUMERIC);
    IValue stringValue = new IntegerValue(data);
    var.setValue(stringValue);
    variableMap.put(name, var);
  }

  @Override
  public void save(String name, Long data) {

  }

  @Override
  public void save(String name, Float data) {
    IVariable var = new VariableImpl(name, VariableType.NUMERIC);
    IValue stringValue = new FloatValue(data);
    var.setValue(stringValue);
    variableMap.put(name, var);
  }

  @Override
  public void save(String name, Double data) {
    IVariable var = new VariableImpl(name, VariableType.NUMERIC);
    IValue stringValue = new DoubleValue(data);
    var.setValue(stringValue);
    variableMap.put(name, var);
  }

  @Override
  public void save(String name, byte[] data) {

  }

  @Override
  public void save(String name, ISavableData data) {
//    if (data instanceof IVariable) {
//      this.variableMap.put(name, (IVariable) data);
//    }
  }

  @Override
  public void save(ISavableData variable) {
    if (variable instanceof IVariable) {
      this.variableMap.put(((IVariable) variable).getName(), (IVariable) variable);
    }
  }

  @Override
  public void saveInGroup(String groupName, Consumer<ISavableSaver> saver) {

  }

  @Override
  public void ifNotExist(String name, ISavableData data) {

  }

  public Map<String, IVariable> getVariableMap() {
    return variableMap;
  }
}

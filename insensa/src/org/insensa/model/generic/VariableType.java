package org.insensa.model.generic;

public enum VariableType {

  NUMERIC("numeric"),
  SELECT_ONE("select-one"),
  SELECT_MULTIPLE("select-multiple"),
  DIRECTORY("directory"),
  FILE("file"),
  MAP_FLOAT_LONG("map_float_long"),
  R_DATA("rdata"),
  STRING("string"),
  PASSWORD("password"),
  BOOLEAN("boolean"),
  SELECT_INTERNAL("select_internal"),
  TABLE("table");
//  SELECT_FILE("select_file");

  private String strType;

  VariableType(String strType) {
    this.strType = strType;
  }

  static VariableType fromString(String strType) {
    for (VariableType t : values()) {
      if (t.strType.equalsIgnoreCase(strType)) {
        return t;
      }
    }
    throw new IllegalArgumentException("Could not convert type "
        + strType
        + " to VariableType");
  }

  @Override
  public String toString() {
    return strType;
  }
}

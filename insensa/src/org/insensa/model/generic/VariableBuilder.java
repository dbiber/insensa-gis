package org.insensa.model.generic;

import org.insensa.model.generic.value.ParameterListValue;

import java.util.ArrayList;
import java.util.List;

public class VariableBuilder {

  private IVariable variable;
  private IParameter lastParam;
  private IParameter stepUpParam;
  private List<IParameter> paramValueList;

  public VariableBuilder(String name, VariableType type) {
    paramValueList = new ArrayList<>();
    variable = new VariableImpl(name, type);
  }

  public VariableBuilder withParam(String name, String val) {
    lastParam = new ParameterImpl(name, val);
    variable.addParameter(lastParam);
    return this;
  }

  public VariableBuilder withSubParam(String name, String val, boolean stepDown) {
    IParameter param = new ParameterImpl(name, val);
    lastParam.addParameter(param);
    stepUpParam = lastParam;
    if (stepDown) {
      this.lastParam = param;
    }
    return this;
  }

  public VariableBuilder stepUp() {
    this.lastParam = stepUpParam;
    return this;
  }

  public VariableBuilder withSubParam(String name, String value) {
    return withSubParam(name, value, false);
  }

  public IVariable build() {
    return variable;
  }


  public VariableBuilder withValueParameter(String name, String value) {
    IParameter param = new ParameterImpl(name, value);
    paramValueList.add(param);
    if (variable.getType() == VariableType.SELECT_ONE) {
      variable.setValue(paramValueList.get(0));
    } else if (variable.getType() == VariableType.SELECT_MULTIPLE) {
      variable.setValue(new ParameterListValue(paramValueList));
    }
    return this;
  }

}

package org.insensa.model.generic;

import java.util.Map;

public class VariableProcessor {

  private IVariableAssigner assigner;

  public VariableProcessor(IVariableAssigner assigner) {
    this.assigner = assigner;
  }

  public void assign(IVariable variable, String name) {
    switch (variable.getType()) {
      case DIRECTORY:
        assigner.assignDirectory(variable, name);
        break;
      case FILE:
        assigner.assignFile(variable, name);
        break;
      case STRING:
        assigner.assignString(variable, name);
        break;
      case NUMERIC:
        assigner.assignNumeric(variable, name);
        break;
      case SELECT_ONE:
        assigner.assignSelectOne(variable, name);
        break;
      case SELECT_MULTIPLE:
        assigner.assignMultiple(variable, name);
        break;
      case TABLE:
        assigner.assignTable(variable, name);
        break;
      case MAP_FLOAT_LONG:
        assigner.assignMapFloatLong(variable, name);
        break;
      case R_DATA:
        assigner.assignRData(variable, name);
        break;
      case PASSWORD:
        assigner.assignString(variable, name);
        break;
      case BOOLEAN:
        assigner.assignBoolean(variable, name);
        break;
      case SELECT_INTERNAL:
        Map<String, IVariable> varMap = assigner.assignInternal(variable, name);
        varMap.forEach((s, variable1) -> {
          if(variable1.getValue().isPresent()) {
            assign(variable1, name + "_" + variable1.getName());
          }
        });
        break;
      default:
        throw new RuntimeException("Could not assign variable of type:" + variable.getType());
    }
  }

  public IVariableAssigner getAssigner() {
    return assigner;
  }
}

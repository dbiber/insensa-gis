package org.insensa.model.generic;

import org.insensa.model.generic.value.IValue;
import org.insensa.model.storage.IStorageData;

import java.util.List;
import java.util.Optional;

public interface IVariable extends IStorageData {

  void setName(String name);

  String getName();

  VariableType getType();

  List<IParameter> getParameter();

  Optional<IParameter> getParameter(String name);

  void addParameter(IParameter parameter);

  Optional<IValue> getValue();

  void setValue(IValue value);

  void addEvent(IEvent event);

  List<IEvent> getEvents();

  List<String> getGroups();

  String getDisplay();

  void setDisplay(String display);

  boolean isGlobal();
}

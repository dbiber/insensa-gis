package org.insensa.model.generic;

import org.insensa.model.storage.IStorageData;

import java.util.List;

public interface IEvent extends IStorageData {

  List<String> getOnValue();

  String getShowGroup();

  String getFunction();

  String getType();
}

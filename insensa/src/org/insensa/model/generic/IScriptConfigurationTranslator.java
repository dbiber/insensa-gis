package org.insensa.model.generic;

import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.value.IValue;

import java.io.Closeable;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;


public interface IScriptConfigurationTranslator extends Closeable {

  void parse() throws ScriptParseException;

  void parse(File xmlFile) throws ScriptParseException;

  void parse(String xmlString) throws ScriptParseException;

  void parse(InputStream stream) throws ScriptParseException;

  List<IVariable> getVariables();

  Optional<IVariable> getVariable(String name);

  void setActiveGroup(String activeGroup);

  void setDefaultValues(Map<String, IVariable> vars);

  void executeFunction(String function, IValue value);
}

package org.insensa.model.generic;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.io.FileHandler;
import org.apache.commons.configuration2.tree.ImmutableNode;
import org.apache.commons.lang3.StringUtils;
import org.insensa.exceptions.ScriptParseException;
import org.insensa.model.generic.value.IValue;
import org.insensa.model.generic.value.IntegerValue;
import org.insensa.model.generic.value.ParameterListValue;
import org.insensa.model.generic.value.StringValue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class XmlScriptConfigurationTranslator
    implements IScriptConfigurationTranslator {

  private Map<String, IVariable> selectedGroup = new LinkedHashMap<>();
  private Map<String, IVariable> mainGroup = new LinkedHashMap<>();
  private Map<String, Map<String, IVariable>> groupVariableMap =
      new LinkedHashMap<>();

  private File xmlFile = null;
  private String xmlString = null;
  private InputStream xmlStream = null;

  public XmlScriptConfigurationTranslator() {
  }

  public XmlScriptConfigurationTranslator(File xmlFile) {
    this.xmlFile = xmlFile;
  }

  public XmlScriptConfigurationTranslator(String xmlString) {
    this.xmlString = xmlString;
  }

  public XmlScriptConfigurationTranslator(InputStream xmlStream) {
    this.xmlStream = xmlStream;
  }

  public void parse() throws ScriptParseException {
    if (xmlFile != null) {
      parse(xmlFile);
    } else if (xmlString != null) {
      parse(xmlString);
    } else if (xmlStream != null) {
      parse(xmlStream);
    } else {
      throw new ScriptParseException("Error Parsing, no input found, " +
          "neither file nor string or stream is available. " +
          "You probably did not use the correct constructor");
    }
  }

  @Override
  public void parse(File xmlFile) throws ScriptParseException {
    try {
      parse(new FileInputStream(xmlFile));
    } catch (FileNotFoundException e) {
      throw new ScriptParseException("Error parsing file \""
          + xmlFile.getAbsolutePath() + "\"");
    }
  }

  @Override
  public void parse(String xmlString) throws ScriptParseException {
    parse(new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8)));
  }

  @Override
  public void parse(InputStream stream) throws ScriptParseException {
    XMLConfiguration conf = new XMLConfiguration();
    FileHandler fh = new FileHandler(conf);
    try {
      fh.load(stream);
      buildVariables(conf);
    } catch (ConfigurationException e) {
      throw new ScriptParseException("Could not load input stream", e);
    }

  }

  /**
   * TODO: Add Schema Validation
   */
  private void buildVariables(XMLConfiguration configuration)
      throws ScriptParseException {
    //TODO: Do not clear to keep the variables !!!
//    clear();
    Map<String, IVariable> mSelectedGroup = new LinkedHashMap<>();
    Map<String, IVariable> mMainGroup = new LinkedHashMap<>();
    Map<String, Map<String, IVariable>> mGroupVariableMap = new LinkedHashMap<>();

    List<HierarchicalConfiguration<ImmutableNode>> varConfs =
        configuration.configurationsAt("variable");
    for (HierarchicalConfiguration<ImmutableNode> conf : varConfs) {

      String groups = conf.getString("[@groups]");
      if (groups != null) {
        List<String> groupList = Arrays.asList(StringUtils.split(groups, ','));
        groupList.forEach(s -> mGroupVariableMap.computeIfAbsent(s, k -> new LinkedHashMap<>()));
        for (String iGroup : groupList) {
          Map<String, IVariable> selGroup = mGroupVariableMap.get(iGroup);
          Map<String, IVariable> oldGroup = groupVariableMap.get(iGroup);
          buildVariableWithGroup(selGroup, oldGroup, conf);
        }
      } else {
        buildVariableWithGroup(mMainGroup, mainGroup, conf);
      }
    }
    clear();
    mainGroup.putAll(mMainGroup);
    selectedGroup.putAll(mSelectedGroup);
    groupVariableMap.putAll(mGroupVariableMap);
    triggerInitialEvents();
  }

  private void buildVariableWithGroup(Map<String, IVariable> group,
                                      Map<String, IVariable> oldGroup,
                                      HierarchicalConfiguration<ImmutableNode> conf)
      throws ScriptParseException {
    String name = conf.getString("[@name]");
    if (name == null) {
      throw new ScriptParseException("Could not find attribute \"name\" in variable element");
    }
    String type = conf.getString("[@type]");
    if (type == null) {
      throw new ScriptParseException("Could not find attribute \"type\" in variable element");
    }
    IVariable vars = new VariableImpl(name, VariableType.fromString(type));

    List<HierarchicalConfiguration<ImmutableNode>> valueConf = conf.configurationsAt("value");
    if (valueConf.size() > 0) {
      buildValue(valueConf.get(0), vars);
    }

    addSubParamater(vars, conf);
    if (oldGroup != null) {
      IVariable oldVar = oldGroup.get(vars.getName());
      if (oldVar != null) {
        oldVar.getValue().ifPresent(vars::setValue);
      }
    }

    group.put(vars.getName(), vars);

    List<HierarchicalConfiguration<ImmutableNode>> eventConfs = conf.configurationsAt("event");
    for (HierarchicalConfiguration<ImmutableNode> eConf : eventConfs) {
      buildEvents(eConf, vars);
    }
  }

  private void buildValue(HierarchicalConfiguration<ImmutableNode> conf,
                          IVariable variable) throws ScriptParseException {
    if (conf == null) {
      return;
    }
    IValue value = null;
    switch (variable.getType()) {
      case SELECT_ONE:
        value = getParam(conf.configurationAt("param"));
        break;
      case SELECT_MULTIPLE:
        List<IParameter> parameters = new ArrayList<>();
        addSubParamater(parameters, conf);
        value = new ParameterListValue(parameters);
        break;
      case FILE:
      case DIRECTORY:
      case STRING:
      case SELECT_INTERNAL:
      case R_DATA:
        value = new StringValue(conf.getString("[@value]"));
        break;
      case NUMERIC:
        value = new IntegerValue(conf.getInt("[@value]"));
      default:
        break;
    }
    variable.setValue(value);
  }

  private void buildEvents(HierarchicalConfiguration<ImmutableNode> conf,
                           IVariable val) {
    EventImpl event = new EventImpl();
    String onValue = conf.getString("[@onValue]");
    if (onValue != null) {
      List<String> onValues = new ArrayList<>(
          Arrays.asList(StringUtils.split(onValue, ',')));
      event.setOnValue(onValues);
    }
    String showGroup = conf.getString("[@showGroup]");
    if (showGroup != null) {
      event.setShowGroup(showGroup);
    }
    String type = conf.getString("[@type]");
    if (type != null) {
      event.setType(type);
    }
    String function = conf.getString("[@function]");
    if (function != null) {
      event.setFunction(function);
    }
    val.addEvent(event);
  }

  private void addSubParamater(Object parameter,
                               HierarchicalConfiguration<ImmutableNode> configuration) throws ScriptParseException {
    List<HierarchicalConfiguration<ImmutableNode>> paramConf =
        configuration.configurationsAt("param");
    for (HierarchicalConfiguration<ImmutableNode> pConf : paramConf) {
      IParameter childParam = getParam(pConf);
      addSubParamater(childParam, pConf);
      if (childParam.getName().equals("collection") && childParam.getParameter().isEmpty()) {
        throw new ScriptParseException("Found collection param without children");
      }
      if (parameter instanceof IVariable) {
        ((IVariable) parameter).addParameter(childParam);
      } else if (parameter instanceof IParameter) {
        ((IParameter) parameter).addParameter(childParam);
      } else if (parameter instanceof List) {
        ((List<IParameter>) parameter).add(childParam);
      }
    }
  }

  private IParameter getParam(HierarchicalConfiguration<ImmutableNode> pConf) throws ScriptParseException {
    String pName = pConf.getString("[@name]");
    if (pName == null) {
      throw new ScriptParseException("Could not find attribute \"name\" in parameter element");
    }
    String pValue = pConf.getString("[@value]");
    if (pValue == null) {
      throw new ScriptParseException("Could not find attribute \"value\" in parameter element");
    }
    return new ParameterImpl(pName, pValue);
  }

  @Override
  public List<IVariable> getVariables() {
    List<IVariable> varList = new ArrayList<>(mainGroup.values());
    varList.addAll(selectedGroup.values());
    return varList;
  }

  @Override
  public Optional<IVariable> getVariable(String name) {
    if (mainGroup.get(name) != null) {
      return Optional.of(mainGroup.get(name));
    } else if (selectedGroup.get(name) != null) {
      return Optional.of(selectedGroup.get(name));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public void setDefaultValues(Map<String, IVariable> vars) {

    vars.forEach((s, variable) -> {
      if (!variable.getValue().isPresent()) {
        return;
      }
      if (mainGroup.containsKey(s)) {
        mainGroup.get(s).setValue(variable.getValue().get());
      }
    });

    this.groupVariableMap.forEach((s, stringIVariableMap) -> {
      vars.forEach((s1, variable) -> {
        if (variable.getValue().isPresent() && stringIVariableMap.get(s1) != null) {
          stringIVariableMap.get(s1).setValue(variable.getValue().get());
        }
      });
    });
    triggerInitialEvents();
  }

  /**
   * Events triggered by default values
   */
  private void triggerInitialEvents() {
    List<IVariable> variables = getVariables();
    variables.forEach(variable -> {
      if (variable.getValue().isPresent()) {
        Optional<IEvent> fEvent = variable
            .getEvents()
            .stream()
            .filter(event -> {
              if (event.getOnValue() == null || event.getOnValue().isEmpty()) {
                return false;
              }
              IParameter selParam = (IParameter) variable.getValue().get();
              return event.getOnValue().contains(selParam.getValue());
            }).findFirst();
        fEvent.ifPresent(this::triggerEvent);
      }
    });
  }

  private void triggerEvent(IEvent event) {
    setActiveGroup(event.getShowGroup());
  }

  @Override
  public void setActiveGroup(String activeGroup) {
    this.selectedGroup.clear();
    if (activeGroup.equals("main")) {
    } else {
      Map<String, IVariable> group = groupVariableMap.get(activeGroup);
      if (group != null) {
        this.selectedGroup.putAll(group);
      }
    }
  }

  @Override
  public void executeFunction(String function, IValue value) {

  }

  private void clear() {
    this.mainGroup.clear();
    this.groupVariableMap.clear();
    this.selectedGroup.clear();
  }

  @Override
  public void close() throws IOException {

  }
}

package org.insensa.model.generic.value;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.util.TreeMap;

public class FloatLongValue extends TreeMap<Float, Long> implements IValue {

  public FloatLongValue() {
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    restorer.loadCollection("CountValue", restorer2 -> {
      put(
          restorer2.loadFloat("Value").get(),
          restorer2.loadLong("Count").get());
    });
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("size", size());
    forEach((aFloat, aLong) -> {
      saver.save("CountValue", saver2 -> {
        saver2.save("Value", aFloat);
        saver2.save("Count", aLong);
      });
    });
  }

  @Override
  public String toScriptString() {
    return "";
  }
}

package org.insensa.model.generic.value;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

public class DoubleValue implements IValue {
  Double value;

  public DoubleValue() {
  }

  public DoubleValue(Double value) {
    this.value = value;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    value = restorer.loadDouble("value").get();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("value", value);
  }

  @Override
  public String toScriptString() {
    return Double.toString(value);
  }
}

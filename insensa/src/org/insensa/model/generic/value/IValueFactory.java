package org.insensa.model.generic.value;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.IVariable;

import java.util.List;

public interface IValueFactory {
  IValue getValue(Integer value);

  IValue getValue(Double value);

  IValue getValue(Float value);

  IValue getValue(String value);

  IValue getValue(Boolean value);

  IValue getValue(IParameter value);

  IValue getValue(List<IParameter> value);

  IValue getValue(IVariable variable);
}

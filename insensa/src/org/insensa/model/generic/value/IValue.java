package org.insensa.model.generic.value;

import org.insensa.model.storage.IStorageData;

public interface IValue extends IStorageData {
  String toScriptString();
}

/*
 * Copyright (C) 2018 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.model.generic.value;

import org.insensa.model.generic.IParameter;
import org.insensa.model.generic.ParameterImpl;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParameterListValue implements IValue {

  private List<IParameter> dataList;

  public ParameterListValue() {
    this.dataList = new ArrayList<>();
  }

  public ParameterListValue(List<IParameter> dataList) {
    this.dataList = new ArrayList<>(dataList);
  }

  public static ParameterListValue fromList(List paramList) {
    List<IParameter> params = new ArrayList<>();
    for (Object val : paramList) {
      params.add(new ParameterImpl("", val.toString()));
    }
    return new ParameterListValue(params);
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    restorer.loadCollection("param", restorer1 -> {
      dataList.add(
          new ParameterImpl(restorer.loadString("name").get(),
              restorer.loadString("value").get()));
    });
  }

  @Override
  public void save(ISavableSaver saver) {
    dataList.forEach(parameter -> {
      saver.save("param", saver1 -> {
        saver1.save("name", parameter.getName());
        saver1.save("value", parameter.getValue());
      });
    });
  }

  public List<IParameter> getDataList() {
    return dataList;
  }

  @Override
  public String toScriptString() {
    return "c(" + dataList.stream().map(IParameter::getValue).collect(Collectors.joining(",")) + ")";
  }
}

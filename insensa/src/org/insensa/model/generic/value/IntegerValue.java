package org.insensa.model.generic.value;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

public class IntegerValue implements IValue {

  Integer value;

  public IntegerValue() {}

  public IntegerValue(Integer value) {
    this.value = value;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    value = restorer.loadInteger("value").get();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("value", value);
  }

  public Integer getValue() {
    return value;
  }

  @Override
  public String toScriptString() {
    return Integer.toString(value);
  }
}

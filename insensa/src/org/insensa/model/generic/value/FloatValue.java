package org.insensa.model.generic.value;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

public class FloatValue implements IValue {
  Float value;

  public FloatValue() {
  }

  public FloatValue(Float value) {
    this.value = value;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    value = restorer.loadFloat("value").get();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("value", value);
  }

  @Override
  public String toScriptString() {
    return Float.toString(value);
  }
}

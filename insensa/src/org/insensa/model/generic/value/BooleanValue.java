package org.insensa.model.generic.value;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

public class BooleanValue implements IValue {

  private Boolean value;

  public BooleanValue() {
  }

  public BooleanValue(Boolean value) {
    this.value = value;
  }

  @Override
  public String toScriptString() {
    return Boolean.toString(value);
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    this.value = restorer.loadBoolean("value").get();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("value", value);
  }

  public boolean getValue() {
    return value;
  }
}

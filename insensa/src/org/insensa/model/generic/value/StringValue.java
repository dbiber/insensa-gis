package org.insensa.model.generic.value;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;

public class StringValue implements IValue {

  String value;

  public static StringValue fromString(String value) {
    return new StringValue(value);
  }

  public StringValue() {
  }

  public StringValue(String value) {
    this.value = value;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    value = restorer.loadString("value").get();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("value", value);
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toScriptString() {
    return value;
  }
}

package org.insensa.model.generic;

import org.apache.commons.io.FilenameUtils;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.exceptions.ScriptParseException;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.model.generic.value.IValue;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class RScriptConfigurationTranslator
    extends XmlScriptConfigurationTranslator2 {

  private static final Logger log = LoggerFactory.getLogger(RScriptConfigurationTranslator.class);

  RSession session = null;

  private File xmlFile = null;
  private String xmlString = null;
  private InputStream xmlStream = null;

  public RScriptConfigurationTranslator(File xmlFile) {
    this();
    this.xmlFile = xmlFile;
  }

  public RScriptConfigurationTranslator(String xmlString) {
    this();
    this.xmlString = xmlString;
  }

  public RScriptConfigurationTranslator(InputStream xmlStream) {
    this();
    this.xmlStream = xmlStream;
  }

  public RScriptConfigurationTranslator() {
  }

  public void init() {
    try {
      session = new RSession("127.0.0.1");
      RMessageStream stream = session.loadOutput(RMessageType.ALL);
      RMessageStreamMonitor.getInstance().registerMessageStream(stream);
    } catch (REXPMismatchException | REngineException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void parse() throws ScriptParseException {
    if (xmlFile != null) {
      parse(xmlFile);
    } else if (xmlString != null) {
      parse(xmlString);
    } else if (xmlStream != null) {
      parse(xmlStream);
    } else {
      throw new ScriptParseException("Error Parsing, no input found, " +
          "neither file nor string or stream is available. " +
          "You probably did not use the correct constructor");
    }
  }

  @Override
  public void parse(File xmlFile) throws ScriptParseException {
    try {
      if (session == null || !session.getRawConnection().isConnected()) {
        init();
      }
      session.eval("insensa:::uiStart()");
      REXP r2 = session.sourceFile(xmlFile);
      REXP rexp = session.eval("toString.default(insensa:::getXmlRoot())");
      String xmlStr = rexp.asString();
      super.parse(xmlStr);
    } catch (REXPMismatchException | REngineException e) {
      log.error("Error Parsing R Script", e);
      try {
        session.unloadOuput();
        session.closeAndClear();
      } catch (REXPMismatchException | REngineException e2) {
        log.error("Error closing R session", e2);
      }
    }
  }

  @Override
  public void parse(String xmlString) throws ScriptParseException {
    super.parse(xmlString);
  }

  @Override
  public void parse(InputStream stream) throws ScriptParseException {
    super.parse(stream);
  }

  @Override
  public void executeFunction(String function, IValue value) {
    try {
      getSession().eval("insensa:::uiStart()");
      for (IVariable var : getVariables()) {
        session.assign(var);
      }
      getSession().eval("toString.default("
          + function
          + "())");
      REXP rexp = getSession().eval("toString.default(insensa:::getXmlRoot())");
      String xmlStr = rexp.asString();
      super.parse(xmlStr);
    } catch (REXPMismatchException | REngineException | ScriptParseException e) {
      e.printStackTrace();
    }
  }

  public RSession getSession() {
    if (session == null || !session.getRawConnection().isConnected()) {
      init();
    }
    return session;
  }

  @Override
  public void close() throws IOException {
    try {
      if (session != null && session.getRawConnection().isConnected()) {
        session.unloadOuput();
        session.closeAndClear();
        session=null;
      }
    } catch (REXPMismatchException | REngineException e) {
      log.error("Error closing R session", e);
    }
  }
}

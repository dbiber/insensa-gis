package org.insensa.model.generic;

import org.insensa.model.generic.value.IValue;

import java.util.List;
import java.util.Optional;

public interface IParameter extends IValue {
  String getName();

  String getValue();

  List<IParameter> getParameter();

  Optional<IParameter> getParameter(String name);

  void addParameter(IParameter parameter);
}

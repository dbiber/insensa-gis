package org.insensa.model.generic;

import org.insensa.model.storage.JdomRestorer;
import org.insensa.model.storage.JdomSaver;

public class XmlVariableBuilder {

  private JdomSaver saver;
  private StringBuilder stringBuilder = new StringBuilder();
  private VariableImpl variableImpl;

  public XmlVariableBuilder(String name, VariableType type) {
    variableImpl = new VariableImpl(name, type);
    saver = new JdomSaver("variable");
    saver.save(saver1 -> {
      saver1.save("name", name);
      saver1.save("type", type.toString());
    });
  }

  public XmlVariableBuilder withParam(String name, String val) {
    saver.save("param", saver1 -> {
      saver1.save("name", name);
      saver1.save("value", val);
    });
    return this;
  }

  public XmlVariableBuilder withSubParam(String name, String value) {
    saver.saveInGroup("param", iSavableSaver -> {
      iSavableSaver.save("param", saver1 -> {
        saver1.save("name", name);
        saver1.save("value", value);
      });
    });
    return this;
  }

  public IVariable build() {
    JdomRestorer restorer = new JdomRestorer(saver.getRootAsString());
    variableImpl.restore(restorer);
    return variableImpl;
  }

  public XmlVariableBuilder withValueParameter(String name, String value) {
    saver.saveInGroup("value", iSavableSaver -> {
      iSavableSaver.save("param", saver1 -> {
        saver1.save("name", name);
        saver1.save("value", value);
      });
    });
    return this;
  }

  public XmlVariableBuilder withValue(String value) {
    saver.save("value", saver1 -> {
      saver1.save("value", value);
    });
    return this;
  }

  @Override
  public String toString() {
    return saver.getRootAsString();
  }
}

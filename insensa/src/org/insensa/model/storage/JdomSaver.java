package org.insensa.model.storage;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class JdomSaver implements ISavableSaver {

  private static final Logger log = LoggerFactory.getLogger(JdomSaver.class);


  private File xmlFile;
  private Element rootElem;
  private Element currentElem;
  private Document doc = null;


  public JdomSaver(File xmlFile, String rootElement) {
    this.rootElem = new Element(rootElement);
    this.currentElem = rootElem;
    this.doc = new Document(rootElem);
    this.xmlFile = xmlFile;
  }

  public JdomSaver(File xmlFile) {
    this.xmlFile = xmlFile;
    try {
      doc = new SAXBuilder().build(this.xmlFile);
      this.rootElem = doc.getRootElement();
      this.currentElem = rootElem;
    } catch (JDOMException | IOException e) {
      e.printStackTrace();
    }
  }

  public JdomSaver(String rootElemName) {
    this.rootElem = new Element(rootElemName);
    this.currentElem = rootElem;
  }

  public JdomSaver(Element rootElem) {
    this.rootElem = rootElem;
    this.currentElem = rootElem;
  }

  private void saveToString(String name, Object data) {
    this.currentElem.setAttribute(name, data.toString());
  }

  @Override
  public void save(String name, String data) {
    this.saveToString(name, data);
  }

  @Override
  public void save(String name, Boolean data) {
    this.saveToString(name, data);
  }

  @Override
  public void save(String name, Integer data) {
    this.saveToString(name, data);
  }

  @Override
  public void save(String name, Long data) {
    this.saveToString(name, data);
  }

  @Override
  public void save(String name, Float data) {
    this.saveToString(name, data);
  }

  @Override
  public void save(String name, Double data) {
    this.saveToString(name, data);
  }

  @Override
  public void save(String name, byte[] data) {

  }

  @Override
  public void ifNotExist(String name, ISavableData data) {
    if (currentElem.getChild(name) == null) {
      save(name, data);
    }
  }

  @Override
  public void save(String name, ISavableData data) {
    Element elem = new Element(name);
    currentElem.addContent(elem);
    Element tmpElem = currentElem;
    currentElem = elem;
    data.save(this);
    currentElem = tmpElem;
  }

  @Override
  public void save(ISavableData variable) {
    variable.save(this);
  }

  public String getRootAsString() {
    return new XMLOutputter().outputString(rootElem);
  }

  public Element getRootElem() {
    return rootElem;
  }

  public Element getCurrentElem() {
    return currentElem;
  }

  public String getGroupAsString() {
    return new XMLOutputter().outputString(currentElem);
  }

  public void setGroup(String groupName) {
    Element elem = rootElem.getChild(groupName);
    if (elem == null) {
      log.info("Could not find group \"" + groupName + "\", creating it");
      elem = new Element(groupName);
      this.rootElem.addContent(elem);
    }
    this.currentElem = elem;
  }

  @Override
  public void saveInGroup(String groupName, Consumer<ISavableSaver> saver) {
    Element tmpCurrentElem = this.currentElem;
    Element tmpElem = this.currentElem.getChild(groupName);
    if (tmpElem == null) {
      log.info("Could not find group \"" + groupName + "\", creating it");
      tmpElem = new Element(groupName);
      currentElem.addContent(tmpElem);
    }
    this.currentElem = tmpElem;
    saver.accept(this);
    this.currentElem = tmpCurrentElem;
  }

  public void resetGroup() {
    if (currentElem != null) {
      this.rootElem = currentElem;
    }
  }

  public void setRootElement(Element rootElem) {
    this.currentElem = rootElem;
    this.rootElem = rootElem;
  }

  public void write() {
    try {
      if (doc == null) {
        return;
      }
      FileOutputStream streamOut = new FileOutputStream(this.xmlFile);
      XMLOutputter xmlOut = new XMLOutputter(Format.getPrettyFormat());
      xmlOut.output(this.doc, streamOut);
      streamOut.close();
    } catch (IOException e) {
      throw new RuntimeException("Error writing file", e);
    }

  }
}

package org.insensa.model.storage;

import org.jdom.Element;

public class JdomStorageCloner implements IStorageCloner {

  private JdomRestorer restorer;
  private JdomSaver saver;

  public JdomStorageCloner() {
  }

  public void store(String name, ISavableData data) {
    saver = new JdomSaver(new Element(name));
    saver.save(data);
    restorer = new JdomRestorer(saver.getCurrentElem());
  }

  @Override
  public ISavableRestorer getRestorer() {
    return restorer;
  }

}

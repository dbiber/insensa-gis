package org.insensa.model.storage;

import org.insensa.extensions.IPluginExec;

import javax.xml.bind.Element;

public class JdomPluginStorage implements IPluginStorage {
  private Element element;
  private ISavableSaver saver;
  private ISavableRestorer restorer;

  public JdomPluginStorage(Element element) {
    this.element = element;
  }

  @Override
  public void refreshPluginExec(IPluginExec pluginExec) {

  }

  @Override
  public void removePluginExec(IPluginExec pluginExec) {

  }

  public void addUser(String workspace, String name) {

    saver.saveInGroup("user",iSavableSaver -> {
      iSavableSaver.saveInGroup("bla", iSavableSaver1 -> {
        iSavableSaver1.save("workspace", workspace);
        iSavableSaver1.save("name", name);
      });
    });
    //writeConfig()
  }

}

package org.insensa.model.storage;

import org.insensa.extensions.IPluginExec;

public interface IPluginStorage {
  void refreshPluginExec(IPluginExec pluginExec);

  void removePluginExec(IPluginExec pluginExec);
}

package org.insensa.model.storage;

public interface ISavableData {
  void save(ISavableSaver saver);
}

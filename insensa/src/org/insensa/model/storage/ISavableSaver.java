package org.insensa.model.storage;


import java.util.function.Consumer;

public interface ISavableSaver {
  void save(String name, String data);

  void save(String name, Boolean data);

  void save(String name, Integer data);

  void save(String name, Long data);

  void save(String name, Float data);

  void save(String name, Double data);

  void save(String name, byte[] data);

  void save(String name, ISavableData data);

  void save(ISavableData variable);

  void saveInGroup(String groupName, Consumer<ISavableSaver> saver);

  void ifNotExist(String name, ISavableData data);

}

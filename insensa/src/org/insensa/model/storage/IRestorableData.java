package org.insensa.model.storage;

public interface IRestorableData {
  void restore(ISavableRestorer restorer);
}

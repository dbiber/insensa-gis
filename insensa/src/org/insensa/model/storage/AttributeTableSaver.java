package org.insensa.model.storage;

import org.insensa.helpers.AttributeTable;
import org.insensa.model.generic.AttributeTableVariableAssigner;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.IVariableAssigner;
import org.insensa.model.generic.VariableProcessor;

import java.util.function.Consumer;

public class AttributeTableSaver implements ISavableSaver {

  private AttributeTable table;
  private VariableProcessor processor;

  public AttributeTableSaver() {
    table = new AttributeTable();
    IVariableAssigner assigner = new AttributeTableVariableAssigner(table);
    processor = new VariableProcessor(assigner);
  }

  @Override
  public void save(String name, String data) {
    table.put(name, data);
  }

  @Override
  public void save(String name, Boolean data) {
    table.put(name, data.toString());
  }

  @Override
  public void save(String name, Integer data) {
    table.put(name, data.toString());
  }

  @Override
  public void save(String name, Long data) {
    table.put(name, data.toString());
  }

  @Override
  public void save(String name, Float data) {
    table.put(name, data.toString());
  }

  @Override
  public void save(String name, Double data) {
    table.put(name, data.toString());
  }

  @Override
  public void save(String name, byte[] data) {
  }

  @Override
  public void save(String name, ISavableData data) {
    if(data instanceof IVariable) {
      processor.assign((IVariable) data, ((IVariable)data).getName());
    } else {
      table.put(name, "");
      data.save(this);
    }
  }

  @Override
  public void save(ISavableData variable) {
    if(variable instanceof IVariable) {
      processor.assign((IVariable) variable, ((IVariable)variable).getName());
    } else {
      table.put("", "");
      variable.save(this);
    }

  }

  @Override
  public void saveInGroup(String groupName, Consumer<ISavableSaver> saver) {
    table.put("", "");
    table.put(groupName, "");
    saver.accept(this);
  }

  @Override
  public void ifNotExist(String name, ISavableData data) {
    boolean found = table.getAttibutes().stream().anyMatch(strings -> strings.get(0).equals(name));
    if (!found) {
      save(name, data);
    }
  }

  public AttributeTable getTable() {
    return table;
  }
}

package org.insensa.model.storage;

public interface IStorageData extends ISavableData, IRestorableData{
}

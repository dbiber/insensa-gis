package org.insensa.model.storage;

public interface IStorageCloner {
  void store(String name, ISavableData data);
  ISavableRestorer getRestorer();
}

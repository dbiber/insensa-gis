package org.insensa.model.storage;

import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class JdomRestorer implements ISavableRestorer {

  private static final Logger log = LoggerFactory.getLogger(JdomRestorer.class);

  private File xmlFile;
  private Element rootElem;
  private String xmlString;

  public JdomRestorer(String xmlString) {
    this.xmlString = xmlString;
    try {
      Document doc = new SAXBuilder().build(new StringReader(xmlString));
      this.rootElem = doc.getRootElement();
    } catch (JDOMException | IOException e) {
      e.printStackTrace();
    }
  }

  public JdomRestorer(File xmlFile) {
    this.xmlFile = xmlFile;
    try {
      Document doc = new SAXBuilder().build(this.xmlFile);
      this.rootElem = doc.getRootElement();
    } catch (JDOMException | IOException e) {
      e.printStackTrace();
    }
  }

  public JdomRestorer(InputStream inputStream) {
    try {
      Document doc = new SAXBuilder().build(inputStream);
      this.rootElem = doc.getRootElement();
    } catch (JDOMException | IOException e) {
      e.printStackTrace();
    }
  }

  public JdomRestorer(Element rootElem) {
    this.rootElem = rootElem;
  }

  public void setRootElem(String name) {
    this.rootElem = rootElem.getChild(name);
  }

  public void setRootElem(Element elem) {
    this.rootElem = elem;
  }

  @Override
  public Optional<String> loadString() {
    return Optional.ofNullable(rootElem.getText());
  }

  @Override
  public Optional<String> loadString(String name) {
    if (rootElem.getAttribute(name) == null) {
      return Optional.empty();
    } else {
      return Optional.of(rootElem.getAttribute(name).getValue());
    }
  }


  @Override
  public Optional<Boolean> loadBoolean(String name) {
    try {
      if (rootElem.getAttribute(name) == null) {
        return Optional.empty();
      } else {
        return Optional.of(rootElem.getAttribute(name).getBooleanValue());
      }
    } catch (DataConversionException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Optional<Integer> loadInteger(String name) {
    try {
      if (rootElem.getAttribute(name) == null) {
        return Optional.empty();
      } else {
        return Optional.of(rootElem.getAttribute(name).getIntValue());
      }
    } catch (DataConversionException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Optional<Long> loadLong(String name) {
    try {
      if (rootElem.getAttribute(name) == null) {
        return Optional.empty();
      } else {
        return Optional.of(rootElem.getAttribute(name).getLongValue());
      }
    } catch (DataConversionException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Optional<Float> loadFloat(String name) {
    try {
      if (rootElem.getAttribute(name) == null) {
        return Optional.empty();
      } else {
        return Optional.of(rootElem.getAttribute(name).getFloatValue());
      }
    } catch (DataConversionException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Optional<Double> loadDouble(String name) {
    try {
      if (rootElem.getAttribute(name) == null) {
        return Optional.empty();
      } else {
        return Optional.of(rootElem.getAttribute(name).getDoubleValue());
      }
    } catch (DataConversionException e) {
      throw new RuntimeException(e);
    }
  }

  private Optional<Attribute> getAttribute(String name) {
    return Optional.ofNullable(rootElem.getAttribute(name));
  }

  @Override
  public void loadByteArray(String name, byte[] data) {
    throw new IllegalArgumentException("loadByteArray not implemented");
  }

  @Override
  public void loadCollection(String name, Consumer<ISavableRestorer> data) {
    Element tmpRoot = rootElem;
    for (Object oElem : tmpRoot.getChildren(name)) {
      rootElem = (Element) oElem;
      data.accept(this);
    }
    rootElem = tmpRoot;
  }

  @Override
  public void loadCollection(String name, Function<ISavableRestorer, Boolean> data) {
    Element tmpRoot = rootElem;
    for (Object oElem : tmpRoot.getChildren(name)) {
      rootElem = (Element) oElem;
      Boolean retValue = data.apply(this);
      if (retValue != null && retValue) {
        break;
      }
    }
    rootElem = tmpRoot;
  }

  @Override
  public void loadCollection(String name, BiConsumer<String, ISavableRestorer> data) {
    Element tmpRoot = rootElem;
    if (tmpRoot.getChild(name) == null) {
      log.debug("Child with name \"{}\" not found", name);
      return;
    }
    for (Object oElem : tmpRoot.getChild(name).getChildren()) {
      rootElem = (Element) oElem;
      data.accept(rootElem.getName(), this);
    }
    rootElem = tmpRoot;
  }

  @Override
  public void loadCollection(String name, BiFunction<String, ISavableRestorer, Boolean> data) {
    Element tmpRoot = rootElem;
    for (Object oElem : tmpRoot.getChild(name).getChildren()) {
      rootElem = (Element) oElem;
      if (data.apply(rootElem.getName(), this)) {
        break;
      }
    }
    rootElem = tmpRoot;
  }

  @Override
  public List<IRestorableData> loadCollection(String name, Class c) {
    List<IRestorableData> dataList = new ArrayList<>();
    for (Object oElem : rootElem.getChildren(name)) {

      loadRestorable(c, (Element) oElem).ifPresent(dataList::add);
//      dataList.add();
    }
    return dataList;
  }

  private Optional<IRestorableData> loadRestorable(Class mClass, Element element) {

    if (element == null) {
      return Optional.empty();
//      try {
//        return (IRestorableData) mClass.newInstance();
//      } catch (InstantiationException | IllegalAccessException e) {
//        e.printStackTrace();
//      }
    }
    Element tmpElem = rootElem;
    rootElem = element;
    try {
      IRestorableData data = (IRestorableData) mClass.newInstance();
      data.restore(this);
      return Optional.ofNullable(data);
    } catch (InstantiationException | IllegalAccessException e) {
      e.printStackTrace();
    } finally {
      rootElem = tmpElem;
    }
    return Optional.empty();
  }

  @Override
  public void loadRestorable(String name, Consumer<ISavableRestorer> data) {
    Element childElem = rootElem.getChild(name);
    if (childElem == null) {
      log.debug("No child with name \"{}\" found", name);
      return;
    }
    Element tmpRoot = rootElem;
    rootElem = childElem;
    data.accept(this);
    rootElem = tmpRoot;
  }

  @Override
  public Optional<IRestorableData> loadRestorable(String value, Class restorableClass) {
    Element elem = rootElem.getChild(value);
    return loadRestorable(restorableClass, elem);
  }

  @Override
  public void loadMap(String name, BiConsumer<String, ISavableRestorer> data) {
  }
}

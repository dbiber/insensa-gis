package org.insensa.model.storage;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public interface ISavableRestorer {
  Optional<String> loadString();
  Optional<String> loadString(String name);
  Optional<Boolean> loadBoolean(String name);
  Optional<Integer> loadInteger(String name);
  Optional<Long> loadLong(String name);
  Optional<Float> loadFloat(String name);
  Optional<Double> loadDouble(String name);
  void loadByteArray(String name, byte[] data);
  void loadRestorable(String name, Consumer<ISavableRestorer> data);

  /**
   * restore a collection where each seperate element has the same name
   *
   * @param name The name of each element that forms this collection
   */
  void loadCollection(String name, Consumer<ISavableRestorer> data);

  /**
   * Same as {@link ISavableRestorer#loadCollection(String, Consumer)} but with
   * cancel condition. If lambda returns true, collection iteration is cancelled
   */
  void loadCollection(String name, Function<ISavableRestorer, Boolean> data);

  /**
   * Restores a collection that has one parent element and different child elements
   *
   * @param name the name of the parent element that contains the colleciton
   */
  void loadCollection(String name, BiConsumer<String, ISavableRestorer> data);

  void loadCollection(String name, BiFunction<String, ISavableRestorer, Boolean> data);

  List<IRestorableData> loadCollection(String name, Class mClass);
  void loadMap(String name, BiConsumer<String, ISavableRestorer> data);

  //TODO: Change to Optional because otherwise a IVariable will always have
  //TODO: a IValue, even if there is no value in xml
  //TODO: see VariableImpl...restore method
  Optional<IRestorableData> loadRestorable(String value, Class restorableClass);
}

package org.insensa;

import org.insensa.optionfilechanger.OptionFileChanger;

import java.io.IOException;

public interface IGisFileContainerFactory {

  IGisFileContainer create(
      ParentModule parentModule,
      String fullPath,
      String outputName,
      boolean serialize,
      WorkerStatus workerStatus) throws IOException;

  /**
   * @see RasterFileContainer#RasterFileContainer(ParentModule, String, String, OptionFileChanger)
   */
  IGisFileContainer create(ParentModule parent, String outputFileName,
                           String fullName, OptionFileChanger option) throws IOException;

//  IGisFileContainer create(String absolutePath);
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.GenericGisFileSet;
import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class RenameRasterFileCommand extends AbstractModelCommand
{

	private GenericGisFileSet selectedFileSet;
	private IGisFileContainer selectedRasterFile;
	private String newFileName;

	/**
   * @param fileSet
   * @param file
   * @param newFile
   */
	public RenameRasterFileCommand(GenericGisFileSet fileSet, IGisFileContainer file, String newFile)
	{
		this.selectedFileSet = fileSet;
		this.selectedRasterFile = file;
		this.newFileName = newFile;
	}

	/** 
	 * 
	 * @see org.insensa.commands.ModelCommand#execute()
	 */
	@Override
	public void execute() throws IOException, GisFileException
	{
		String newNameTmp;
		if (this.newFileName.lastIndexOf(".") <= 0)
		{
			newNameTmp = this.newFileName + ".tif";
		} else
		{
			newNameTmp = this.newFileName.substring(0, this.newFileName.lastIndexOf(".")) + ".tif";
		}

		List<IGisFileContainer> rasterFileList = this.selectedFileSet.getFileList();
		for (int i = 0; i < rasterFileList.size(); i++)
		{
			if (rasterFileList.get(i).getOutputFileName().equals(newNameTmp))
				throw new IOException("Filename " + this.selectedRasterFile.getOutputFileName() + " already exists");
			if (rasterFileList.get(i).getName().equals(newNameTmp))
			{
			}
		}

		// a hard copy
		if (this.selectedRasterFile.isHardCopy())
		{
			String oldOutputFile = this.selectedRasterFile.getOutputFileName();
			String oldName = this.selectedRasterFile.getName();
			this.selectedRasterFile.changeOutputFileName(newNameTmp);
			this.selectedFileSet.fireChildGisFileOutputNameChanged(this.selectedRasterFile, oldOutputFile, newNameTmp);
			File newFile = new File(this.selectedRasterFile.getAbsOutputDirectoryPath() + newNameTmp);
			this.selectedRasterFile.renameTo(newFile);
			this.selectedFileSet.fireChildGisFileRenamed(this.selectedRasterFile, oldName, newNameTmp);

		} else
		{
			String oldName = this.selectedRasterFile.getName();
			this.selectedRasterFile.changeOutputFileName(newNameTmp);
			this.selectedFileSet.fireChildGisFileOutputNameChanged(this.selectedRasterFile, oldName, newNameTmp);
		}
	}

}

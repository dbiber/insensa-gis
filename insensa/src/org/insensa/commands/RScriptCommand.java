package org.insensa.commands;

import org.apache.commons.io.input.TailerListener;
import org.insensa.exceptions.GisFileException;
import org.insensa.WorkerStatus;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import java.io.IOException;

public class RScriptCommand extends AbstractModelCommand {

  private String command;
  private RSession session;
  private TailerListener tailerListener;

  public RScriptCommand(String command) {
    this.command = command;
  }

  @Override
  public void setWorkerStatus(WorkerStatus wStatus) {
    super.setWorkerStatus(wStatus);
    if (wStatus instanceof TailerListener) {
      this.tailerListener = (TailerListener) wStatus;
    }
  }

  @Override
  public void execute() throws IOException, GisFileException {
    try {
      getWorkerStatus().ifPresent(workerStatus1 -> {
        workerStatus1.startProcess();
        workerStatus1.setProgressName("Starting Progress");
      });
      session = new RSession("127.0.0.1");
      if (tailerListener != null) {
        RMessageStream stream = session.loadOutput(RMessageType.ALL);
        RMessageStreamMonitor.getInstance().registerMessageStream(stream);
      }


      session.unloadOuput();
    } catch (REXPMismatchException | REngineException e) {
      e.printStackTrace();
    }
  }
}

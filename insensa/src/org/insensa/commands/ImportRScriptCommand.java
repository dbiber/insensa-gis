package org.insensa.commands;

import org.insensa.IGisFileSet;
import org.insensa.helpers.r.FileWatcherManager;
import org.insensa.helpers.r.IFileChangeListener;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.value.StringValue;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.insensa.view.dialogs.helper.IProcessFinishedListener;
import org.insensa.view.dialogs.helper.ViewCommandManager;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ImportRScriptCommand extends AbstractModelCommand {

  private static final Logger log = LoggerFactory.getLogger(ImportRScriptCommand.class);


  private IGisFileSet targetFileSet;
  private String script;
  private Map<String, IVariable> configurationMap;
  private IProcessFinishedListener refreshTreeListener;
  private Component view;

  public ImportRScriptCommand(IGisFileSet targetFileSet,
                              String script,
                              Map<String, IVariable> configurationMap,
                              IProcessFinishedListener refreshTreeListener,
                              Component view) {
    this.targetFileSet = targetFileSet;
    this.script = script;

    this.configurationMap = configurationMap;
  }

  @Override
  public void execute() throws IOException {
    RSession rSession = null;
    try {
      if (this.workerStatus != null) {
        this.workerStatus.startProcess();
        this.workerStatus.setProgressName("Import Files : Searching...");
      }

      if (!configurationMap.containsKey("targetPath")) {
        throw new IOException("Variable \"targetPath\" not found");
      }

      rSession = new RSession("127.0.0.1");
      RMessageStream stream = rSession.loadOutput(RMessageType.ALL);
      RMessageStreamMonitor.getInstance().registerMessageStream(stream);

      rSession.assignInsensaIsActive();

      for (IVariable var : configurationMap.values()) {
        rSession.assign(var);
      }

      IVariable taPathVar = configurationMap.get("targetPath");

      File targetFir = new File(((StringValue) taPathVar.getValue().get()).getValue());
      FileWatcherManager.getInstance().addFileChangeListener(targetFir.getAbsolutePath(),
          new IFileChangeListener() {
            @Override
            public void fileChanged(File file) {
            }

            @Override
            public void fileCreated(File file) {
              log.info("Created File {}", file.getAbsolutePath());

              //TODO: seems that the files are not ready directly..
              //TODO: We should fix that in the future
              try {
                Thread.sleep(100);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }

              ModelCommand command = new ImportFilesCommand(targetFileSet,
                  new File[]{file}, true);
              command.setWorkerStatus(workerStatus);
              ViewCommandManager.executeCommands(view, refreshTreeListener, command);
            }

            @Override
            public void fileDeleted(File file) {
              log.info("Deleted File {}", file.getAbsolutePath());
            }
          });

      rSession.sourceFile(new File(script));


      rSession.unloadOuput();
      FileWatcherManager.getInstance().removeFileChangeListener(targetFir.getAbsolutePath());

      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(100.f);
        this.workerStatus.endProgress();
      }

    } catch (Exception e) {
      if (workerStatus != null) {
        this.workerStatus.errorProcess(e);
      }
      log.error("Could not execute ImportRScriptCommand", e);
    } finally {
      if (rSession != null && rSession.getRawConnection().isConnected()) {
        try {
          Thread.sleep(1000);//FIXME
          rSession.closeAndClear();
        } catch (REXPMismatchException | REngineException | InterruptedException e) {
          log.error("Error closing R session", e);
        }
      }
    }
  }
}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.Model;
import org.insensa.exceptions.GisFileException;
import org.insensa.User;
import org.insensa.XMLProperties.XmlProperties;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class ImportConfigCommand extends AbstractModelCommand {
  private Model model;
  private String oldConfigFilename;
  private List<User> existingUser;

  /**
   *
   */
  public ImportConfigCommand(Model model, String oldConfigFilename) {
    this.oldConfigFilename = oldConfigFilename;
    this.model = model;
    this.existingUser = new ArrayList<User>();
  }

  /**
   * @see org.insensa.commands.ModelCommand#execute()
   */
  @Override
  public void execute() throws IOException, GisFileException {
    if (super.workerStatus != null) {
      super.workerStatus.startProcess();
      super.workerStatus.setProgressName("Importing Config");
    }
    XmlProperties oldProperties = new XmlProperties(this.oldConfigFilename);
    List<User> oldUserList = oldProperties.getUsers();
    List<User> userList = model.getUsers();
    float stepsize = 100.0f / userList.size();
    float step = 0.0f;
    StringBuffer existingUserStringBuffer = new StringBuffer();
    for (User iOldUser : oldUserList) {
      if (super.workerStatus != null) {
        super.workerStatus.refreshPercentage(step);
        step += stepsize;
      }
      if (exists(userList, iOldUser)) {
        existingUser.add(iOldUser);
        existingUserStringBuffer.append(iOldUser.getName());
        existingUserStringBuffer.append("\n");
      } else {
        model.addUser(iOldUser);
      }
    }
    if (!existingUser.isEmpty()) {
      super.throwErrorMessage("The following users already exists and could not be added\n"
          + existingUserStringBuffer.toString());
    }
    if (super.workerStatus != null)
      super.workerStatus.endProgress();
  }

  private boolean exists(List<User> existingUser, User testUser) {
    for (User iUser : existingUser)
      if (iUser.getName().equals(testUser.getName()))
        return true;
    return false;
  }
}

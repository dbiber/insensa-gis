/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.Project;
import org.insensa.exceptions.GisFileException;

import java.io.IOException;


public class RenameFileSetCommand extends AbstractModelCommand
{

	private GenericGisFileSet fileSet;
	private String newName;

	/**
	 * @param fileSet
	 * @param newName
	 */
	public RenameFileSetCommand(GenericGisFileSet fileSet, String newName)
	{
		this.fileSet = fileSet;
		this.newName = newName;
	}

	/** 
	 * 
	 * @see org.insensa.commands.ModelCommand#execute()
	 */
	@Override
	public void execute() throws IOException, GisFileException
	{
		String oldName = this.fileSet.getName();
		this.fileSet.renameTo(this.newName);
		this.relocateFilesIter(this.fileSet, this.fileSet.getProject());
		this.fileSet.getParent().fireChildFileSetRenamed(this.fileSet, oldName, this.newName);
	}

	/**
	 * @param fileSet
	 * @param project
	 */
	private void relocateFilesIter(IGisFileSet fileSet, Project project)
	{
		for (IGisFileSet iSet : fileSet.getChildSets()) {
			this.relocateFilesIter(iSet, project);
		}
		for (IGisFileContainer iFile : fileSet.getFileList()) {
			if (!iFile.getGisFile().getFileRef().exists()) {
				iFile.relocateFile();
			}
		}
	}
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.IProjectListener;
import org.insensa.Model;
import org.insensa.Project;
import org.insensa.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;


public class OpenProjectCommand extends AbstractModelCommand {
  private static final Logger log = LoggerFactory.getLogger(OpenProjectCommand.class);

  private Model model;
  private List<IProjectListener> projectListener = null;
  private User user;
  private Project project;

  public OpenProjectCommand(Model model, User user, Project project, List<IProjectListener> projectListener) {
    this.model = model;
    this.user = user;
    this.project = project;
    this.projectListener = projectListener;
  }

  @Override
  public void execute() throws IOException {
    try {
      if (this.user.getActiveProject() != null) {
        this.user.closeProject();
      }
      this.model.openProject(this.user, this.project, this.workerStatus, this.projectListener);
    } catch (IOException e) {
      if (this.user.getActiveProject() != null) {
        this.user.closeProject();
      }
      this.workerStatus.errorProcess(e);
      log.error("Error Opening Project", e);
      throw e;
    } catch (Exception e) {
      if (this.user.getActiveProject() != null) {
        this.user.closeProject();
      }
      this.workerStatus.errorProcess(e);
      log.error("Error Opening Project", e);
      throw new IOException(e);
    }
  }

}

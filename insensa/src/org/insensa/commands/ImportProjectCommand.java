/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import org.insensa.exceptions.GisFileException;
import org.insensa.User;
import org.insensa.XMLProperties.XmlFileSetProperties;
import org.insensa.XMLProperties.XmlProjectProperties;
import org.insensa.connections.AbstractRasterConnection;
import org.insensa.helpers.SystemSpec;
import org.insensa.helpers.VersionHelper;
import org.jdom.Element;

//TODO: Reimplement !!!
public class ImportProjectCommand extends AbstractModelCommand {
  private File sourceDir;
  private User user;
  private List<File> fileSetConfigs;
  private List<File> fileConfigs;
  private File projectConfig;
  private float status = 0.0f;

  public ImportProjectCommand(User user, File sourceDir) {
    this.user = user;
    this.sourceDir = sourceDir;
    this.fileSetConfigs = new ArrayList<File>();
    this.fileConfigs = new ArrayList<File>();
  }

  public void copyFiles(File source, File target, float stepSize) throws IOException {
    if (source == null)
      this.throwErrorMessage("No source file selected");

    if (target == null)
      this.throwErrorMessage("No target file selected");

    if (!source.exists())
      this.throwErrorMessage("Source file \"" + source.getAbsolutePath() + "\" does not exist");

    if (!target.exists())
      this.throwErrorMessage("Target file \"" + target.getAbsolutePath() + "\" does not exist");

    if (!source.isDirectory())
      this.throwErrorMessage("Source file \"" + source.getAbsolutePath() + "\" is not a Directory");

    if (!target.isDirectory())
      this.throwErrorMessage("Target file \"" + target.getAbsolutePath() + "\" is not a Directory");

    String newFolder = target.getAbsolutePath();
    if (newFolder.endsWith(SystemSpec.separator))
      newFolder += source.getName() + SystemSpec.separator;
    else
      newFolder += SystemSpec.separator + source.getName() + SystemSpec.separator;

    File newTargetDir = new File(newFolder);
    if (newTargetDir.exists()) {
      ICommandProblem problem = new AbstractCommandProblem();
      problem.setErrorMessage("Target directory \"" + newTargetDir.getAbsolutePath() + "\" already exists");
      DeleteFolderSolution sol = new DeleteFolderSolution(newTargetDir);
      problem.addSolution(sol);
      this.throwProblem(problem);
    }

    if (newTargetDir.mkdir() == false)
      this.throwErrorMessage("Error creating directory " + newTargetDir.getAbsolutePath());

    File[] files = source.listFiles();
    if (files != null) {
      for (File file : files) {
        if (file.isDirectory())
          this.copyFiles(file, newTargetDir, stepSize);
        else {
          String newFilePath = newFolder + file.getName();
          File newFile = new File(newFilePath);
          if (newFile.getName().equals("FileSetSettings.xml"))
            this.fileSetConfigs.add(newFile);
          else if (newFile.getName().endsWith(".xml") && newFile.getParentFile().getName().equals("infos"))
            this.fileConfigs.add(newFile);
          if (newFile.createNewFile() == false)
            this.throwErrorMessage("Error creating file " + newFile.getAbsolutePath());
          FileChannel sourceChannel = new FileInputStream(file).getChannel();
          FileChannel destinationChannel = new FileOutputStream(newFile).getChannel();
          sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
          sourceChannel.close();
          destinationChannel.close();

          if (this.workerStatus != null)
            this.workerStatus.refreshPercentage(this.status);
          this.status += stepSize;
        }
      }
    }
  }

  private void correctVersion() throws IOException {
    VersionHelper versionHelper = VersionHelper.getInstance();
    XmlProjectProperties pXml = new XmlProjectProperties(this.projectConfig.getAbsolutePath());
    String version = pXml.getVersion();
    if (VersionHelper.compareVersion(version, versionHelper.getVersion()) != VersionHelper.VERSION_OLDER)
      return;
//		if (VersionHelper.isOlderThan(version, VersionHelper.getVersion()) == false)
//			return;
    this.status = 0.0f;
    if (this.workerStatus != null) {
      this.workerStatus.refreshPercentage(this.status);
      this.workerStatus.setProgressName("Version Reconfiguration");
    }
    float fileCount = this.fileConfigs.size() + this.fileSetConfigs.size() + 1;
    float stepSize = 100.0f / fileCount;
    XmlFileSetProperties fXml;
    for (File iFile : this.fileSetConfigs) {
      fXml = new XmlFileSetProperties(iFile.getAbsolutePath());
      List fileElemList = fXml.getGisFileList();
      for (Object iObj : fileElemList) {
        Element fElem = (Element) iObj;
        Element outputPathElem = fElem.getChild(AbstractRasterConnection.E_OUTPUT_PATH);
        if (outputPathElem != null) {
          String outPath = outputPathElem.getText();
          String fPath = fElem.getAttributeValue("path");
          if (fPath != null && outPath.contains(fPath))
            fElem.removeAttribute("path");
          fElem.removeContent(outputPathElem);
        }
      }
      List fileSetElemList = fXml.getGisFileSetList();
      for (Object iObj : fileSetElemList) {
        Element fsElem = (Element) iObj;
        String fsPath = fsElem.getAttributeValue("path");
        if (fsPath != null)
          fsElem.removeAttribute("path");
      }
      if (this.workerStatus != null) {
        this.status += stepSize;
        this.workerStatus.refreshPercentage(this.status);
      }
      fXml.writeConfig();
    }

    List fileSetList = pXml.getRasterFileSets();
    String replaceString = null;
    for (Object iObj : fileSetList) {
      Element fsElem = (Element) iObj;
      String fsPath = fsElem.getAttributeValue("path");
      if (fsPath != null) {
        if (replaceString == null) {
          int i = fsPath.lastIndexOf(this.sourceDir.getName());
          if (i > -1) {
            replaceString = fsPath.substring(0, i) + this.sourceDir.getName();
          }
        }
        fsElem.removeAttribute("path");
      }
    }
    pXml.setVersion(versionHelper.getVersion());

    if (this.workerStatus != null) {
      this.status += stepSize;
      this.workerStatus.refreshPercentage(this.status);
    }
    pXml.writeConfig();

    for (File iFile : this.fileConfigs) {
//      XmlGisFileInformation rfXml = new XmlGisFileInformation(iFile);
//      rfXml.openGlobal();
//      List<Element> elements = rfXml.getConnectionElements();
//
//      if (elements == null)
//        continue;
//      for (Element iElem : elements) {
//        List<Element> childList = iElem.getChildren("file");
//        for (Element iChild : childList) {
//          String oldPath = iChild.getAttributeValue(AbstractRasterConnection.E_OUTPUT_PATH);
//          String newPath = oldPath.replace(replaceString, "");
//          String newPath2 = newPath.replaceAll("\\\\", SystemSpec.separator);
//          newPath2 = newPath2.replaceAll("/", SystemSpec.separator);
//          iChild.setAttribute(AbstractRasterConnection.E_OUTPUT_PATH, newPath2);
//        }
//      }
//      if (this.workerStatus != null) {
//        this.status += stepSize;
//        this.workerStatus.refreshPercentage(this.status);
//      }
//      rfXml.writeConfig();
//      rfXml.closeGlobal();
    }
  }

  public int countFiles(File source) throws ModelCommandException {
    int allFiles = 0;
    if (source == null)
      this.throwErrorMessage("No source file selected");
    if (!source.exists())
      this.throwErrorMessage("Source file \"" + source.getAbsolutePath() + "\" does not exist");
    if (!source.isDirectory())
      this.throwErrorMessage("Source file \"" + source.getAbsolutePath() + "\" is not a Directory");
    File[] files = source.listFiles();
    if (files != null) {
      allFiles += files.length;
      for (File file : files) {
        if (file.isDirectory()) {
          allFiles--;
          allFiles += this.countFiles(file);
        }
      }
    }
    return allFiles;
  }

  @Override
  public void execute() throws IOException, GisFileException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName("Copying Project");
    }
    this.fileSetConfigs.clear();
    this.fileConfigs.clear();
    File workspaceDir = new File(this.user.getWorkspacePath());
    String projectName = this.sourceDir.getName();

    if (this.sourceDir == null)
      this.throwErrorMessage("No source file selected");
    if (!this.sourceDir.exists())
      this.throwErrorMessage("Source file \"" + this.sourceDir.getAbsolutePath() + "\" does not exist");
    if (!this.sourceDir.isDirectory())
      this.throwErrorMessage("Source file \"" + this.sourceDir.getAbsolutePath() + "\" is not a Directory");
    // Check if this is a valid old project
    File[] files = this.sourceDir.listFiles();
    boolean valid = false;

    if (files != null) {
      for (File file : files) {
        if (file.getName().equals("settings.xml")) {
          this.projectConfig = new File(this.user.getWorkspacePath() + this.sourceDir.getName() + SystemSpec.separator + file.getName());
          valid = true;
          break;
        }
      }
    }
    if (valid == false)
      this.throwErrorMessage("\"" + this.sourceDir.getAbsolutePath() + "\" is not a valid project");

    int fCount = this.countFiles(this.sourceDir);
    float stepSize = 100.0f / fCount;
    this.copyFiles(this.sourceDir, workspaceDir, stepSize);
    this.user.addNewProject(projectName, null);
    this.correctVersion();
    if (this.workerStatus != null) {
      this.workerStatus.refreshPercentage(100f);
      this.workerStatus.endProgress();
    }
  }

  public void setSourceDir(File sourceDir) {
    this.sourceDir = sourceDir;
  }

  public void setUser(User user) {
    this.user = user;
  }

  private class DeleteFolderSolution extends AbstractModelCommand implements ISolution {
    private File dirToDelete;

    public DeleteFolderSolution(File dirToDelete) throws ModelCommandException {
      this.dirToDelete = dirToDelete;
    }

    @Override
    public void execute() throws IOException, GisFileException {
      if (this.dirToDelete.exists()) {
        if (this.dirToDelete.delete() == false) {
          this.throwErrorMessage("could not delete directory " + this.dirToDelete + " \n" + "Try to delete the directory manually");
        }
      }
    }

    @Override
    public String getMessage() {
      return "Delete directory " + this.dirToDelete.getAbsolutePath();
    }
  }

}

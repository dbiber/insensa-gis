package org.insensa.commands;

import org.insensa.model.generic.value.IValue;
import org.insensa.r.RSession;

public class RScriptExecuteFunction extends AbstractModelCommand {

  private RSession session;
  private String function;
  private IValue value;

  public RScriptExecuteFunction(RSession session,
                                String function,
                                IValue value) {
    this.session = session;
    this.function = function;
    this.value = value;
  }


  @Override
  public void execute() {


  }
}

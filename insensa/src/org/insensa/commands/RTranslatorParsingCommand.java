package org.insensa.commands;

import org.insensa.connections.ConnectionFileChanger;
import org.insensa.exceptions.ScriptParseException;
import org.insensa.extensions.IPluginExec;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.RScriptConfigurationTranslator;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import java.io.IOException;
import java.util.List;

public class RTranslatorParsingCommand extends AbstractModelCommand {

  RScriptConfigurationTranslator translator;
  IPluginExec pluginExec;

  public RTranslatorParsingCommand(RScriptConfigurationTranslator translator, IPluginExec pluginExec) {
    this.translator = translator;
    this.pluginExec = pluginExec;
  }

  private void assignInfoConnection(InfoConnection infoConnection) {
    translator.getSession().assignSourceFiles(
            infoConnection.getSourceFileList());
  }

  private void assignConnection(ConnectionFileChanger connectionFileChanger) {
    translator.getSession().assignSourceFiles(
            connectionFileChanger.getSourceFileList());
    List<InfoConnection> infoConnections =
            connectionFileChanger.getOutputFileContainer().getInfoConnections();
    for (InfoConnection infoConnection: infoConnections) {
      if (!infoConnection.isUsed()) {
        continue;
      }
      if (infoConnection instanceof IScriptPluginExec) {
        for (IVariable var: ((IScriptPluginExec) infoConnection).getVariables().values()) {
          translator.getSession().assign(var);
        }
      }
    }
  }

  private void assignVariables() throws REXPMismatchException, REngineException {
    if (pluginExec instanceof ConnectionFileChanger) {
      assignConnection((ConnectionFileChanger) pluginExec);
    } else if (pluginExec instanceof InfoConnection) {
      assignInfoConnection((InfoConnection) pluginExec);
    } else if (pluginExec instanceof InfoReader) {
      translator.getSession().assignInsensaCurrentFile(
          ((InfoReader) pluginExec).getTargetFile().getAbsolutePath());
      translator.getSession().assignInsensaWs(((InfoReader) pluginExec).getTargetFile());
    } else if (pluginExec instanceof OptionFileChanger) {
      translator.getSession().assignInsensaCurrentFile(
          ((OptionFileChanger) pluginExec).getActualFileContainer().getAbsolutePath());
      translator.getSession().assignInsensaWs(
          ((OptionFileChanger) pluginExec).getActualFileContainer());
    }
  }

  @Override
  public void execute() {
    try {
      if (workerStatus != null) {
        workerStatus.startProcess();
        workerStatus.setProgressName("Parsing R Script");
        workerStatus.refreshPercentage(0);
      }
      translator.getSession().assignInsensaIsActive();
      if (pluginExec != null) {
        assignVariables();
      }

      translator.parse();

      if (workerStatus != null) {
        workerStatus.endProgress();
      }
    } catch (ScriptParseException | RuntimeException e) {
      if (workerStatus != null) {
        workerStatus.errorProcess(e);
      }
    } catch (REngineException | REXPMismatchException e) {
      e.printStackTrace();
    }
//    finally {
//      try {
//        translator.close();
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
  }
}

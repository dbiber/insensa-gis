/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Stack;


public class CommandManager {
  private static final Logger log = LoggerFactory
      .getLogger(CommandManager.class);
  private static CommandManager instance = new CommandManager();
  private Stack<ModelCommand> commandStack;
  private Stack<ModelCommand> undoStack;
  private RuntimeException exception = null;

  private CommandManager() {
    this.commandStack = new Stack<>();
    this.undoStack = new Stack<>();
  }

  public static CommandManager getInstance() {
    return instance;
  }

  /**
   * Adds the command to the beginning of the stack.
   *
   * @param command {@link ModelCommand} to add
   */
  public void addCommand(ModelCommand command) {
    this.commandStack.insertElementAt(command, 0);
  }

  /**
   * Adds the command to the end of the stack.
   */
  public void pushCommand(ModelCommand command) {
    this.commandStack.push(command);
  }

  public void clearCommands() {
    this.commandStack.clear();
    this.undoStack.clear();
  }

  public CommandStatus executeCommands() {
    while (!this.commandStack.isEmpty()) {
      ModelCommand com = this.commandStack.pop();
      try {
        com.execute();
        this.undoStack.push(com);

      } catch (IOException e) {
        this.exception = new RuntimeException(e.getCause());
        com.getWorkerStatus().ifPresent(workerStatus -> {
          workerStatus.errorProcess(e);
        });
        this.commandStack.push(com);
        log.error("Error Executing Commands", this.exception);
        return CommandStatus.STATUS_FAIL;
      }
    }
    return CommandStatus.STATUS_SUCCESS;
  }

  public RuntimeException getException() {
    return this.exception;
  }

  public ModelCommand getLastCommand() {
    return this.commandStack.peek();
  }

  public CommandStatus undoCommands() {
    while (!this.undoStack.isEmpty()) {
      ModelCommand com = this.undoStack.pop();
      try {
        com.undo();
        this.commandStack.push(com);

      } catch (Exception e) {
        this.exception = new RuntimeException(e);
        com.getWorkerStatus().ifPresent(workerStatus -> {
          workerStatus.errorProcess(e);
        });
        this.undoStack.push(com);
        log.error("Error Executing Commands", this.exception);
        return CommandStatus.STATUS_FAIL;
      }
    }
    return CommandStatus.STATUS_SUCCESS;

  }

  public enum CommandStatus {
    STATUS_SUCCESS,
    STATUS_FAIL
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.WorkerStatus;

import java.util.Optional;


public abstract class AbstractModelCommand implements ModelCommand {
  protected WorkerStatus workerStatus = null;
  protected ICommandProblem problem;

  @Override
  public boolean check() {
    return false;
  }

  @Override
  public void createDependendCommands() {
  }

  public ICommandProblem getProblem() {
    return this.problem;
  }

  @Override
  public Optional<WorkerStatus> getWorkerStatus() {
    return Optional.ofNullable(this.workerStatus);
  }

  @Override
  public void setWorkerStatus(WorkerStatus wStatus) {
    this.workerStatus = wStatus;
  }

  @Override
  public void retry() {
  }

  protected void throwErrorMessage(String errorMessage)
      throws ModelCommandException {
    this.problem = new ErrorMessageProblem(errorMessage);
    throw new ModelCommandException(errorMessage);
  }

  protected void throwProblem(ICommandProblem problem)
      throws ModelCommandException {
    this.problem = problem;
    throw new ModelCommandException(problem.getErrorMessage());
  }

  @Override
  public void undo() {
  }
}

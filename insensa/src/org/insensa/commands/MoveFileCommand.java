/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.GenericGisFileSet;
import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.helpers.SystemSpec;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class MoveFileCommand extends AbstractModelCommand {
  private GenericGisFileSet sourceSet;
  private GenericGisFileSet targetSet;
  private IGisFileContainer file;

  public MoveFileCommand(GenericGisFileSet sourceSet, GenericGisFileSet targetSet, IGisFileContainer file) {
    this.sourceSet = sourceSet;
    this.targetSet = targetSet;
    this.file = file;
  }

  /**
   * @see org.insensa.commands.ModelCommand#execute()
   */
  @Override
  public void execute() throws IOException, GisFileException {
    IGisFileContainer activeFile = this.file;
    List<IGisFileContainer> rasterFileList = this.targetSet.getFileList();
    for (IGisFileContainer aRasterFileList: rasterFileList) {
      if (aRasterFileList.getName().equals(activeFile.getName()))
        throw new IOException("File " + activeFile.getName() + " already exists");
    }

    if (this.file.isHardCopy()) {
      File newFile = new File(this.targetSet.getPath() + this.targetSet.getName() + SystemSpec.separator + activeFile.getName());
      activeFile.renameTo(newFile);
      this.targetSet.addGisFileInformation(activeFile, true);
      this.sourceSet.removeGisFile(activeFile);
    } else {
      this.targetSet.addGisFileInformation(activeFile, true);
      this.sourceSet.removeGisFile(activeFile);
    }

  }
}

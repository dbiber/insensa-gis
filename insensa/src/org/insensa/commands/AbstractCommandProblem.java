/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import java.util.ArrayList;
import java.util.List;

public class AbstractCommandProblem implements ICommandProblem
{
	private List<ISolution> solutions;
	private String errorMessage = "";

	/** 
	 * 
	 * @see org.insensa.commands.ICommandProblem#addSolution(org.insensa.commands.ISolution)
	 */
	@Override
	public void addSolution(ISolution solution)
	{
		if (this.solutions == null)
			this.solutions = new ArrayList<ISolution>();
		this.solutions.add(solution);
	}

	/** 
	 * 
	 * @see org.insensa.commands.ICommandProblem#getErrorMessage()
	 */
	@Override
	public String getErrorMessage()
	{
		return this.errorMessage;
	}

	/** 
	 * 
	 * @see org.insensa.commands.ICommandProblem#getSolutions()
	 */
	@Override
	public List<ISolution> getSolutions()
	{
		return this.solutions;
	}

	/** 
	 * 
	 * @see org.insensa.commands.ICommandProblem#setErrorMessage(java.lang.String)
	 */
	@Override
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

}

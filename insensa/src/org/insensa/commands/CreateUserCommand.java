/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.Model;
import org.insensa.User;
import org.insensa.exceptions.GisFileException;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class CreateUserCommand extends AbstractModelCommand {
  private String userName;
  private String workspace;
  private Model model;

  public CreateUserCommand(Model model, String user, String workspace) {
    this.userName = user;
    this.workspace = workspace;
    this.model = model;
  }

  @Override
  public void execute() throws IOException, GisFileException {
    if (this.userName == null || this.userName.isEmpty()) {
      this.throwErrorMessage("No user name defined");
    }
    if (this.workspace == null || this.workspace.isEmpty()) {
      this.throwErrorMessage("No workspace defined");
    }
    List<User> userList = this.model.getUsers();
    if (this.userName != null && !this.userName.isEmpty()) {
      for (User iUser : userList) {
        if (iUser.getName().equals(this.userName)) {
          this.throwErrorMessage("User already exists");
        }
      }
    }
    File newFolder = new File(this.workspace);
    if (!newFolder.exists()) {
      this.throwErrorMessage("Folder "
          + newFolder.getAbsolutePath()
          + " does not exist");
    }
    User newUser = new User(this.userName, this.workspace);
    this.model.addUser(newUser);
  }
}

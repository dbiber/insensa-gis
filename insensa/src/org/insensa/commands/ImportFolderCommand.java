/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.LowPrioException;
import org.insensa.RasterFileContainer;
import org.insensa.optionfilechanger.OptionManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ImportFolderCommand extends AbstractModelCommand {
  private String path;
  private boolean serialize;
  private IGisFileSet targetFileSet;
  private float percentStatus;

  public ImportFolderCommand(IGisFileSet targetFileSet, String path, boolean serialize) {
    this.path = path;
    this.serialize = serialize;
    this.targetFileSet = targetFileSet;
    this.percentStatus = 0.0F;
  }

  @Override
  public void execute() throws IOException {
    if (this.workerStatus != null) {
      this.workerStatus.startProcess();
      this.workerStatus.setProgressName("Import Folder : Searching...");
    }

    float step;
    IGisFileSet activeFileSet = this.targetFileSet;
    File folder = new File(this.path);
    File file_array[] = folder.listFiles();
    // List<File> fileList = new ArrayList<File>();
    List<IGisFileContainer> activeFileList = activeFileSet.getFileList();

    List<String> filePathList = new ArrayList<String>();
    List<String> fileNameList = new ArrayList<String>();

    String tmp_name;

    for (File element : file_array) {
      if (element.isFile()) {
        tmp_name = element.getName();
        if (tmp_name.endsWith(".asc") || tmp_name.endsWith("tif") || tmp_name.endsWith("TIF")) {
          filePathList.add(element.getAbsolutePath());
          fileNameList.add(element.getName());
        }
      } else {
        File subFileArray[] = element.listFiles();
        if (subFileArray == null)
          if (this.workerStatus != null) {
            this.workerStatus.errorProcess("Error reading directory");
            throw new LowPrioException("Error reading directory");
          }
        for (File element2 : subFileArray) {
          if (element2.isDirectory()) {
            File subSubFileArray[] = element2.listFiles();
            for (File element3 : subSubFileArray) {
              if (element3.getName().equals("w001001.adf")) {
                filePathList.add(element2.getAbsolutePath());
                fileNameList.add(element2.getName());
              }
            }
          }
        }
      }
    }

    for (int i = 0; i < activeFileList.size(); i++) {
      for (int j = 0; j < filePathList.size(); ) {
        if (fileNameList.get(j).equals(activeFileList.get(i).getName()) || fileNameList.get(j).equals(activeFileList.get(i).getOutputFileName())) {
          filePathList.remove(j);
          fileNameList.remove(j);
          // break;
        } else {
          j++;
        }
      }
    }

    if (filePathList.isEmpty()) {
      if (this.workerStatus != null)
        this.workerStatus.errorProcess("no new file was found");
      throw new LowPrioException("no new file was found");
    }

    new OptionManager();
    int list_size = filePathList.size();
    step = 100.0F / list_size;

    try {
      for (int i = 0; i < list_size; i++) {
        if (this.workerStatus != null)
          this.workerStatus.setProgressName("Import: " + fileNameList.get(i));
        IGisFileContainer gisFileInformation = new RasterFileContainer(
            activeFileSet, fileNameList.get(i),
            filePathList.get(i), null);
        activeFileSet.addGisFile(gisFileInformation, this.serialize);
        this.percentStatus += step;
        if (this.workerStatus != null)
          this.workerStatus.refreshPercentage(this.percentStatus);
      }
    } catch (GisFileException e) {
      e.printStackTrace();
    }
    if (this.workerStatus != null)
      this.workerStatus.endProgress();
    // activeFileSet.addGisFileNameList(filePathList,fileNameList,null,
    // serialize);
  }

}

/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import java.io.File;
import java.io.IOException;

public class DeleteProjectCommand extends AbstractModelCommand
{
	private String folderName;
	private int fileCount = 0;
	private float stepsize = 0.0F;
	private float progress = 0.0F;

	/**
	 * @param folderName
	 * @throws IOException
	 */
	private void countFiles(String folderName) throws IOException
	{
		File folder = new File(folderName);
		if (folder == null || !(folder.exists()))
			throw new IOException("No Folder with name " + folderName + " exists");
		File[] children = folder.listFiles();
		this.fileCount += children.length;
		for (File element : children)
		{
			if (element.isDirectory())
				this.countFiles(element.getAbsolutePath());
		}
		this.fileCount++;
	}

	/**
	 * @param folderName
	 * @throws IOException
	 */
	private void deleteProject(String folderName) throws IOException
	{

		File folder = new File(folderName);
		if (folder == null || !(folder.exists()))
			throw new IOException("No Folder with name " + folderName + " exists");
		File[] children = folder.listFiles();
		for (File element : children)
		{
			if (element.isDirectory())
				this.deleteProject(element.getAbsolutePath());
			else
			{
				if (this.workerStatus != null)
					this.workerStatus.setProgressName("Delete \"" + element.getName() + "\"");
				element.delete();
				this.progress += this.stepsize;
				if (this.workerStatus != null)
					this.workerStatus.refreshPercentage(this.progress);
			}
		}
		if (this.workerStatus != null)
			this.workerStatus.setProgressName("Delete \"" + folder.getName() + "\"");
		folder.delete();
		this.progress += this.stepsize;
		if (this.workerStatus != null)
			this.workerStatus.refreshPercentage(this.progress);
	}

	@Override
	public void execute() throws IOException
	{

		if (this.workerStatus != null)
		{
			this.workerStatus.startProcess();
			this.workerStatus.startPause("Calculate file size");
		}
		this.countFiles(this.folderName);
		if (this.workerStatus != null)
			this.workerStatus.endPause();
		this.stepsize = 100.0f / this.fileCount;

		this.deleteProject(this.folderName);
		if (this.workerStatus != null)
		{
			this.workerStatus.refreshPercentage(100.0f);
			this.workerStatus.setProgressName("deletion completed successfully");
			this.workerStatus.endProgress();
		}

	}

	public void setFolderName(String folderName)
	{
		this.folderName = folderName;
	}
}

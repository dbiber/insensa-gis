/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.commands;

import org.insensa.GenericGisFileSet;
import org.insensa.GisFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.XMLProperties.XmlGisFileInformation;

import java.io.IOException;
import java.util.List;


public class LinkFileCommand extends AbstractModelCommand
{
	private GenericGisFileSet targetFileSet;
	private IGisFileContainer file;

	/**
	 * @param targetFileSet
	 * @param file
	 */
	public LinkFileCommand(GenericGisFileSet targetFileSet, IGisFileContainer file) {
		this.targetFileSet = targetFileSet;
		this.file = file;
	}

	@Override
	public void execute() throws IOException, GisFileException {

		IGisFileContainer activeRasterFile = this.file;
		List<IGisFileContainer> rasterFileList = this.targetFileSet.getFileList();
		for (int i = 0; i < rasterFileList.size(); i++) {
			if (rasterFileList.get(i).getOutputFileName().equals(activeRasterFile.getOutputFileName()))
				this.throwErrorMessage("File " + activeRasterFile.getName() + " already exists");
		}

		GisFileContainer newRasterFile = new GisFileContainer(activeRasterFile,
				this.targetFileSet,
				activeRasterFile.getOutputFileName(),
				true);
		this.targetFileSet.addGisFileInformation(newRasterFile, true);
		((XmlGisFileInformation) newRasterFile.getGisFileInformationStorage()).closeGlobal();
	}
}

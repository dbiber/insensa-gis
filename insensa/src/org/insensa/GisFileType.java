/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import java.util.stream.Stream;

public enum GisFileType {

  GEO_TIFF("tiff"),
  SHAPE("shape"),
  UNKNOWN("");

  private final String fileType;

  GisFileType(final String fileType) {
    this.fileType = fileType;
  }

  public static String toFileExtension(GisFileType type) {
    switch (type) {
      case SHAPE:
        return "shp";
      case GEO_TIFF:
        return "tif";
      case UNKNOWN:
        return "";
      default:
        return "";
    }
  }

  /**
   * @return the enum matches the name or null
   */
  public static GisFileType byName(String name) {
    if (name == null) {
      return UNKNOWN;
    }
    if (name.toLowerCase().equals("shape")
        || name.toLowerCase().equals("shp")) {
      return SHAPE;
    } else if (Stream.of("tiff", "tif", "geotiff", "geotif", "asc", "ascii")
        .anyMatch(s -> s.equals(name.toLowerCase()))) {
      return GEO_TIFF;
    }
    return UNKNOWN;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import org.gdal.gdalconst.gdalconstConstants;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;

import java.io.IOException;
import java.math.BigDecimal;

public class DataTypeHelper {

  public static boolean fitsInDataType(int dataType, BigDecimal number) {
    boolean floating = true;
    if (number.stripTrailingZeros().scale() > 0)
      floating = true;
    else
      floating = false;

    if (floating == false) {
      if (DataTypeHelper.isFloatingDataType(dataType) == true) {
        long num = number.longValue();
        switch (dataType) {
          case 6: // GDT_Float32
            if (num < Float.MIN_NORMAL && num > Float.MAX_VALUE)
              return false;
            else
              return true;
          case 7:// GDT_Float64
            if (num < Double.MAX_VALUE || num > Double.MAX_VALUE)
              return false;
            else
              return true;
          default:
            return false;
        }
      } else {
        long num = number.longValue();
        switch (dataType) {
          case 1: // GDT_BYTE
            if (num < 0 || num > 255)
              return false;
            else
              return true;
          case 2:// GDT_UInt16
            if (num < 0 || num > Character.MAX_VALUE)
              return false;
            else
              return true;
          case 3:// GDT_Int16
            if (num < Short.MIN_VALUE || num > Short.MAX_VALUE)
              return false;
            else
              return true;
          case 4: // GDT_Unt32
            if (num < 0 || num > (Math.pow(2, 32) - 1))
              return false;
            else
              return true;
          case 5: // GDT_Int32
            if (num < Integer.MIN_VALUE || num > Integer.MAX_VALUE)
              return false;
            else
              return true;
          default:
            return false;
        }
      }
    } else {
      if (DataTypeHelper.isFloatingDataType(dataType) == true) {
        double num = number.doubleValue();
        switch (dataType) {
          case 6: // GDT_Float32
            if (num < Float.MIN_NORMAL && num > Float.MAX_VALUE)
              return false;
            else
              return true;
          case 7:// GDT_Float64
            if (num < Double.MAX_VALUE || num > Double.MAX_VALUE)
              return false;
            else
              return true;
          default:
            return false;
        }
      } else
        return false;
    }
  }

  /**
   *
   */
  public static int getBestDatatype(BigDecimal number) {
    if (number.longValue() == 0 && number.doubleValue() == 0.0) {
      return gdalconstConstants.GDT_Unknown;
    } else {
      if (number.stripTrailingZeros().scale() > 0) {
        if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_Float32, number)) {
          return gdalconstConstants.GDT_Float32;
        } else if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_Float64, number)) {
          return gdalconstConstants.GDT_Float64;
        }
      } else {
        if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_Byte, number)) {
          return gdalconstConstants.GDT_Byte;
        } else if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_UInt16, number)) {
          return gdalconstConstants.GDT_UInt16;
        } else if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_Int16, number)) {
          return gdalconstConstants.GDT_Int16;
        } else if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_UInt32, number)) {
          return gdalconstConstants.GDT_UInt32;
        } else if (DataTypeHelper.fitsInDataType(gdalconstConstants.GDT_Int32, number)) {
          return gdalconstConstants.GDT_Int32;
        }

      }
    }
    return gdalconstConstants.GDT_Unknown;
  }

  public static int getBestDataType(float number) {
    return gdalconstConstants.GDT_Byte;
  }

  public static String getDataTypeInternalName(int dataType) {
    switch (dataType) {
      case 1: // GDT_BYTE
        return "GDT_Byte";
      case 2: // GDT_UInt16
        return "GDT_UInt16";
      case 3: // GDT_Int16
        return "GDT_Int16";
      case 4: // GDT_Unt32
        return "GDT_UInt32";
      case 5: // GDT_Int32
        return "GDT_Int32";
      case 6: // GDT_Float32
        return "GDT_Float32";
      case 7: // GDT_Float64
        return "GDT_Float64";
      default:
        return "UNKNOWN";
    }
  }

  public static boolean isFloatingDataType(int dataType) {
    switch (dataType) {
      case 1: // GDT_BYTE
        return false;
      case 2: // GDT_UInt16
        return false;
      case 3: // GDT_Int16
        return false;
      case 4: // GDT_Unt32
        return false;
      case 5: // GDT_Int32
        return false;
      case 6: // GDT_Float32
        return true;
      case 7: // GDT_Float64
        return true;
      default:
        return false;
    }
  }

  public static String getFileTypeString(IRasterGisFile rasterFile) throws IOException {
    int dType = rasterFile.getDataType();// getFileType(rasterFile);
    if (dType == gdalconstConstants.GDT_Byte)
      return "Byte";
    else if (dType == gdalconstConstants.GDT_Float32)
      return "Float 32";
    else if (dType == gdalconstConstants.GDT_UInt16)
      return "UINT 16";
    else if (dType == gdalconstConstants.GDT_UInt32)
      return "UINT 32";
    else if (dType == gdalconstConstants.GDT_Int16)
      return "INT 16";
    else if (dType == gdalconstConstants.GDT_Int32)
      return "INT 32";
    else
      return "unknown";
  }

  public enum RasterDataType_GDAL {
    GDT_Float32(gdalconstConstants.GDT_Float32),
    GDT_Float64(gdalconstConstants.GDT_Float64),
    GDT_Byte(gdalconstConstants.GDT_Byte),
    GDT_UInt16(gdalconstConstants.GDT_UInt16),
    GDT_Int16(gdalconstConstants.GDT_Int16),
    GDT_UInt32(gdalconstConstants.GDT_UInt32),
    GDT_Int32(gdalconstConstants.GDT_Int16);

    private final int gdalType;

    RasterDataType_GDAL(int gdalType) {
      this.gdalType = gdalType;
    }

    public int toGdalType() {
      return this.gdalType;
    }
  }

  public enum RasterDataType {
    GDT_Float32("GDT_Float32"),
    GDT_Float64("GDT_Float64"),
    GDT_Byte("GDT_Byte"),
    GDT_UInt16("GDT_UInt16"),
    GDT_Int16("GDT_Int16"),
    GDT_UInt32("GDT_UInt32"),
    GDT_Int32("GDT_Int16");

    private final String gdalTypeName;

    RasterDataType(String gdalTypeName) {
      this.gdalTypeName = gdalTypeName;
    }

    public String toGdalType() {
      return this.gdalTypeName;
    }
  }
}

/*
 * Copyright (C) 2016 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.helpers;

import org.insensa.WorkerStatus;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class MathUils {

  public static Float getNthData(Map<Float, Long> dataMap, long index,
                                 float processStatus, WorkerStatus workerStatus)
      throws IOException {
    long tmpCounter = 0;
    float steps = 100 / dataMap.size();
    Iterator<Float> iter = dataMap.keySet().iterator();
    Float val;
    while (iter.hasNext()) {
      val = iter.next();
      tmpCounter += dataMap.get(val);
      if (index <= tmpCounter)
        return val;
      processStatus += steps;
      if (workerStatus != null) {
        workerStatus.refreshPercentage(processStatus);
      }
    }
    throw new IOException("Could not find data for index " + index + ", counter is: " + tmpCounter);
  }

  public static float roundWithPrecition(float value, long precition) throws IOException {
    double newReadValue;
    long roundedValue;
    newReadValue = value * precition;
    roundedValue = Math.round(newReadValue);
    if (roundedValue==Long.MIN_VALUE || roundedValue==Long.MAX_VALUE) {
      throw new IOException("Number  is to big, use less precision.");
    }
    newReadValue = (double)roundedValue / precition;
    return new Double(newReadValue).floatValue();
  }
}

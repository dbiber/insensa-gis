/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import java.util.Vector;

public class AttributeTable {
  private Vector<Vector<String>> attibutes = new Vector<Vector<String>>();

  public Vector<Vector<String>> getAttibutes() {
    return this.attibutes;
  }

  public String getKey(int index) {
    return this.attibutes.get(index).get(0);
  }

  public Vector<String> getRow(int index) {
    return this.attibutes.get(index);
  }

  public String getValue(int index) {
    return this.attibutes.get(index).get(1);
  }

  public boolean isEmpty() {
    return this.attibutes.isEmpty();
  }

  public void put(String key, String value) {
    Vector<String> vecAttr = new Vector<String>();
    vecAttr.add(key);
    vecAttr.add(value);
    this.attibutes.add(vecAttr);
  }

  public int size() {
    return this.attibutes.size();
  }
}

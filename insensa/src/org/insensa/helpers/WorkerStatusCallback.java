/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import org.gdal.gdal.ProgressCallback;
import org.insensa.WorkerStatus;


public class WorkerStatusCallback extends ProgressCallback {
  private WorkerStatus workerStatus;

  public WorkerStatusCallback(WorkerStatus workerStatus) {
    this.workerStatus = workerStatus;
  }

  public WorkerStatus getWorkerStatus() {
    return this.workerStatus;
  }

  @Override
  public int run(double dfComplete, String pszMessage) {
    if (this.workerStatus != null) {
      if (pszMessage != null)
        this.workerStatus.setProgressName(pszMessage);
      this.workerStatus.refreshPercentage(new Float(dfComplete * 100));
    }
    return -1;
  }
}

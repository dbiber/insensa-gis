/*
 * Copyright (C) 2011-2013 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import org.apache.commons.io.FilenameUtils;
import org.insensa.GisFileFactory;
import org.insensa.GisFileType;
import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.exceptions.GisFileException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.CopyOption;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import javax.swing.filechooser.FileSystemView;


/**
 * @author dennis
 */
public class IOHelper {
  public static final int DEPLOY_DEFAULT = 0;
  public static final int DEPLOY_TESTING = 1;
  private static IOHelper instance = new IOHelper();
  private Integer deployType = DEPLOY_DEFAULT;

  private IOHelper() {
  }

  public static IOHelper getInstance() {
    return instance;
  }


  public static void moveFileInfoFileContainer(IGisFile gisFile,
                                               IGisFileContainer targetContainer) {
    if (targetContainer.isHardCopy() && !targetContainer.deleteFile()) {
      throw new org.insensa.exceptions.GisFileException("delete false: 1; For: "
          + targetContainer.getAbsolutePath());
    }
//    gisFile.refresh();
    gisFile.unlock();
    targetContainer.getGisFile().unlock();
    gisFile.renameTo(targetContainer.getGisFile().getFileRef());
    targetContainer.getGisFile().unlock();
    targetContainer.setLock(false);
  }

  public static void moveFileInfoFileContainer(String gisFilePath,
                                               IGisFileContainer targetContainer) {
    IGisFile gisFile = GisFileFactory.create(gisFilePath)
        .orElseThrow(() -> new GisFileException("Could not create GisFile out of \""
            + gisFilePath + "\""));
    if (targetContainer.isHardCopy() && !targetContainer.deleteFile()) {
      throw new org.insensa.exceptions.GisFileException("delete false: 1; For: "
          + targetContainer.getAbsolutePath());
    }
//    gisFile.refresh();
    gisFile.unlock();
    targetContainer.getGisFile().unlock();
    gisFile.renameTo(targetContainer.getGisFile().getFileRef());
    targetContainer.getGisFile().refresh();
    targetContainer.getGisFile().unlock();
    targetContainer.setLock(false);
  }


  /**
   * FIXME: This method did not make sense in one case, maybe this is generally true
   * FIXME: If targetContainer is already an empty container without a physical file
   * FIXME: then just create a file and move it to the container/IGisFile ??
   *
   *
   * Moves a temporary fileContainer outside of the current project directory
   * to a container path inside the project
   */
  public static void saveRasterFile(IGisFileContainer tmpContainer,
                                    IGisFileContainer targetContainer) throws IOException {
    if (targetContainer.isHardCopy() && !targetContainer.deleteFile()) {
      throw new IOException("delete false: 1; For: "
          + targetContainer.getAbsolutePath());
    }
    File newFile = new File(targetContainer.getAbsOutputDirectoryPath()
        + targetContainer.getOutputFileName());
    if (!tmpContainer.getGisFile().renameTo(newFile)) {
      throw new IOException("Error renaming "
          + tmpContainer.getAbsolutePath()
          + " to "
          + newFile.getAbsolutePath());
    } else {
      tmpContainer.getGisFile().refresh();
    }
    targetContainer.getGisFile().unlock();
    targetContainer.setLock(false);
  }

  public static String createTmpAbsoluteFilePath(IGisFileContainer gisFileContainer) {
    return createTmpAbsoluteFilePath(gisFileContainer, gisFileContainer.getGisFile().getType());
  }

  public static String createTmpAbsoluteFilePath(IGisFileContainer gisFileContainer,
                                                 GisFileType fileType) {
    String tmpName;
    File tmpTestFile;
    String extension = GisFileType.toFileExtension(fileType);


    for (int i = 0; i < 20; i++) {
      tmpName = gisFileContainer.getAbsOutputDirectoryPath()
          + gisFileContainer.getOutputFileName();
      tmpName = FilenameUtils.removeExtension(tmpName);
      tmpName += "_tmp" + i + "." + extension;
      tmpTestFile = new File(tmpName);
      if (!(tmpTestFile.exists())) {
        return tmpName;
      }
    }
    return null;
  }

  public static void extractFile(Path zipFile, String fileNameWithinZip, Path outputFile) throws IOException {
    Path parentPath = outputFile.getParent();
    if (!parentPath.toFile().exists()) {
      if (!parentPath.toFile().mkdirs()) {
        throw new IOException("Error creating directory " + parentPath.toString());
      }
    }
    if (zipFile.toFile().toString().toLowerCase().endsWith("jar")) {
      URLClassLoader classLoader = new URLClassLoader(new URL[]{zipFile.toUri().toURL()});
      classLoader.findResource(fileNameWithinZip);
      InputStream link = (classLoader.getResourceAsStream(fileNameWithinZip));
      if (link == null) {
        throw new IOException("could not find file or create stream for " + fileNameWithinZip);
      }
      Files.copy(link, outputFile, StandardCopyOption.REPLACE_EXISTING);
      return;
    }
    try (FileSystem fileSystem = FileSystems.newFileSystem(zipFile, null)) {
      Path fileToExtract = fileSystem.getPath(fileNameWithinZip);
      Files.copy(fileToExtract, outputFile, StandardCopyOption.REPLACE_EXISTING);
    }
  }

  public Integer getDeployType() {
    return deployType;
  }

  public void setDeployType(Integer deployType) {
    this.deployType = deployType;
  }

  public File getDocumentsDir() {
    File file = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getAbsolutePath(), "InsensaGIS");
    if (deployType == DEPLOY_TESTING) {
      file = new File(FileSystemView.getFileSystemView().getDefaultDirectory().getAbsolutePath(), "InsensaGIS-testing");
    }
    if (!file.exists()) {
      file.mkdirs();
    }
    return file;
  }

  public File getXmlDir() {
    File file = new File(getDocumentsDir(), "xml");
    if (!file.exists())
      file.mkdirs();
    return file;
  }

  public File getMainConfigFile() {
    return new File(getXmlDir(), "mainConfig.xml");
  }

  public File getLoggingDir() {
    File file = new File(getDocumentsDir(), "logs");
    if (!file.exists())
      file.mkdirs();
    return file;
  }

  public File getErrorLoggerFile() {
    return new File(getLoggingDir(), "errorlog.txt");
  }

  public File getPluginsDir() {
    File pluginsDir = new File(getDocumentsDir(), "plugins");
    if (!pluginsDir.exists())
      pluginsDir.mkdirs();
    return pluginsDir;
  }

}

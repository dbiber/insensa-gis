/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import org.insensa.exceptions.ThreadPoolException;
import org.insensa.extensions.IPluginExec;
import org.insensa.inforeader.InfoReader;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class ExecDependencyController {
  private static final Logger log = LoggerFactory.getLogger(ExecDependencyController.class);
  /**
   * Boolean[0] = isUsed
   * Boolean[1] = hasError
   */
  private Map<String, ExecStatus> dependenciesMap = new HashMap<>();
  private Map<Integer, ExecStatus> optionDepMap = new TreeMap<>();

  public synchronized void checkInfoDependencies(IPluginExec pluginExec) {
    for (String infoReaderName: pluginExec.getInfoDependencies()) {
      if (this.dependenciesMap.get(infoReaderName) == null) {
        throw new RuntimeException("can't find InfoReader: " + infoReaderName);
      }
      while (!this.dependenciesMap.get(infoReaderName).isUsed
          && !this.dependenciesMap.get(infoReaderName).hasError) {
        try {
          this.wait();
        } catch (InterruptedException e) {
          log.error("UNKNOWN", e);
        }
      }
    }
  }

//  public synchronized void checkInfoDependencies(IPluginExec pluginExec) {
//    for (String infoReaderName: pluginExec.getInfoDependencies()) {
//      if (this.dependenciesMap.get(infoReaderName) == null) {
//        throw new RuntimeException("can't find InfoReader: " + infoReaderName);
//      }
//      while (!this.dependenciesMap.get(infoReaderName).isUsed) {
//        try {
//          this.wait();
//        } catch (InterruptedException e) {
//          log.error("UNKNOWN", e);
//        }
//      }
//    }
//  }
//
//  public synchronized void checkInfoDependencies(InfoReader infoReader) throws IOException {
//    for (String infoReaderName: infoReader.getInfoDependencies()) {
//      if (this.dependenciesMap.get(infoReaderName) == null) {
//        throw new IOException("can't find InfoReader: " + infoReaderName);
//      }
//      while (!this.dependenciesMap.get(infoReaderName).isUsed
//          && !this.dependenciesMap.get(infoReaderName).hasError) {
//        try {
//          this.wait();
//        } catch (InterruptedException e) {
//          log.error("UNKNOWN", e);
//        }
//      }
//    }
//  }
//
//  public synchronized void checkInfoDependencies(OptionFileChanger option) throws IOException {
//
//    for (String infoReaderName: option.getInfoDependencies()) {
//      if (this.dependenciesMap.get(infoReaderName) == null) {
//        throw new IOException("can't find InfoReader: " + infoReaderName);
//      }
//      while (!this.dependenciesMap.get(infoReaderName).isUsed
//          && !this.dependenciesMap.get(infoReaderName).hasError) {
//        try {
//          this.wait();
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    }
//  }

  public synchronized boolean checkOptionPriority(OptionFileChanger option) {
    Integer orderId = option.getOrderId();
    boolean error = false;
    for (Map.Entry<Integer, ExecStatus> optionDepEntry: this.optionDepMap.entrySet()) {
      Integer iterOptionOrderId = optionDepEntry.getKey();
      ExecStatus iterOptionCheckedArr = optionDepEntry.getValue();
      if (iterOptionOrderId < orderId) {
        while (!iterOptionCheckedArr.isUsed && !iterOptionCheckedArr.hasError) {
          try {
            this.wait();
          } catch (InterruptedException exception) {
            exception.printStackTrace();
          }
        }
        error = iterOptionCheckedArr.hasError;
      }
    }
    return error;
  }

  public synchronized void endRunnable(InfoReader infoReader) {
    this.dependenciesMap.put(infoReader.getId(), new ExecStatus(true, false));
    this.notifyAll();
  }

  public synchronized void endRunnable(OptionFileChanger option) {
    this.optionDepMap.put(option.getOrderId(), new ExecStatus(true, false));
    this.notifyAll();
  }

  public synchronized void errorRunnable(InfoReader infoReader) {
    this.dependenciesMap.put(infoReader.getId(), new ExecStatus(false, true));
    this.notifyAll();
  }

  public synchronized void errorRunnableOrderId(OptionFileChanger option) {
    this.optionDepMap.put(option.getOrderId(), new ExecStatus(false, true));
    this.notifyAll();
  }

  public synchronized void resetRunnable(InfoReader infoReader) {
    ExecStatus status = this.dependenciesMap.get(infoReader.getId());
    if (status != null) {
      status.hasError = false;
      this.dependenciesMap.put(infoReader.getId(), status);
    }
  }

  public synchronized void setDependency(InfoReader info) {
    this.dependenciesMap.put(info.getId(), new ExecStatus(info.isUsed(), false));
    info.setDependencyChecker(this);
  }

  public synchronized void setDependency(OptionFileChanger option) {
    this.optionDepMap.put(option.getOrderId(), new ExecStatus(option.isUsed(), false));
  }

  public synchronized void unsetDependency(IPluginExec pluginExec) {
    if (this.dependenciesMap.remove(pluginExec.getId()) == null) {
      throw new ThreadPoolException("ExecDependencyController: unsetDependency: InfoKey "
          + pluginExec.getId()
          + " not found");
    }
  }

  public class ExecStatus {
    public boolean isUsed;
    public boolean hasError;

    public ExecStatus(boolean isUsed, boolean hasError) {
      this.isUsed = isUsed;
      this.hasError = hasError;
    }
  }
}

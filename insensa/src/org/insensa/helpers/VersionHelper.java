/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import org.insensa.updater.items.ItemInsensa;
import org.insensa.updater.items.ItemParser;

import java.io.File;
import java.io.IOException;


import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VersionHelper {
  public static final int VERSION_OLDER = 0;
  public static final int VERSION_NEWER = 1;
  public static final int VERSION_EQUAL = 2;
  public static final int VERSION_UNKNOWN = 3;
  public static final int OS_UNKNOWN = 0;
  public static final int OS_WIN = 1;
  public static final int OS_LINUX = 2;
  public static final int OS_MAC = 3;
  public static final int ARCH_UNKNOWN = 0;
  public static final int ARCH_32BIT = 1;
  public static final int ARCH_64BIT = 2;
  private static final Logger log = LoggerFactory.getLogger(VersionHelper.class);
  private static VersionHelper instance = new VersionHelper();
  private ItemInsensa itemInsensa = null;


  /**
   * @throws JDOMException
   * @throws IOException
   *
   */
  private VersionHelper() {
    try {
      File versionFile = new File("xml" + SystemSpec.separator + "version.xml");
      if (versionFile.exists())
        itemInsensa = ItemParser.parseInsensa(versionFile);
    } catch (JDOMException e) {
      itemInsensa = null;
      log.error("UNKNOWN", e);
    } catch (IOException e) {
      itemInsensa = null;
      log.error("UNKNOWN", e);
    }
  }

  /**
   * @return
   */
  public static VersionHelper getInstance() {
    return instance;
  }

  /**
   * Compares two version numbers
   */
  public static Integer compareVersion(String testObject, String compareWith) {
    if (testObject == null)
      return VERSION_UNKNOWN;
    String testAr[] = testObject.split("\\.");
    String compareAr[] = compareWith.split("\\.");

    int version = VERSION_UNKNOWN;
    for (int i = 0; i < compareAr.length; i++) {
      if (testAr.length < (i + 1))
        return VERSION_NEWER;
      Integer numTest = Integer.valueOf(testAr[i]);
      Integer numCompare = Integer.valueOf(compareAr[i]);
      if (numTest < numCompare)
        return VERSION_NEWER;
      else if (numTest > numCompare)
        return VERSION_OLDER;
      else
        version = VERSION_EQUAL;
    }
    return version;
  }

  /**
   * @deprecated <b>since 0.1.6</> Use {@link #compareVersion(String, String)} instead
   */
  public static boolean isOlderThan(String testObject, String compareWith) {
    if (testObject == null)
      return true;
    String testAr[] = testObject.split("\\.");
    String compareAr[] = compareWith.split("\\.");
    int testMainNumber = Integer.valueOf(testAr[0]);
    int testReleaseNumer = Integer.valueOf(testAr[1]);
    int testWorkingNumber = Integer.valueOf(testAr[2]);

    int compareMainNumber = Integer.valueOf(compareAr[0]);
    int compareReleaseNumer = Integer.valueOf(compareAr[1]);
    int compareWorkingNumber = Integer.valueOf(compareAr[2]);

    if (compareMainNumber < testMainNumber)
      return true;
    else if (compareMainNumber == testMainNumber) {
      if (compareReleaseNumer < testReleaseNumer)
        return true;
      else if (compareReleaseNumer == testReleaseNumer) {
        if (compareWorkingNumber < testWorkingNumber)
          return true;
        else
          return false;
      } else
        return false;

    } else
      return false;

  }

  /**
   * @return
   */
  public String getVersion() {
    if (itemInsensa != null)
      return itemInsensa.getVersion();
    else
      return "";
  }

  public String getArch() {
    if (itemInsensa != null)
      return itemInsensa.getItemFile().getArch();
    else
      return "";
  }

  public String getOs() {
    if (itemInsensa != null)
      return itemInsensa.getItemFile().getOs();
    else
      return "";
  }

}

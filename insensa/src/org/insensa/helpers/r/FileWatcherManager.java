/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.helpers.r;

import com.sun.nio.file.SensitivityWatchEventModifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

public class FileWatcherManager {
  private static final Logger log = LoggerFactory.getLogger(FileWatcherManager.class);


  private static FileWatcherManager instance = new FileWatcherManager();
  private ConcurrentMap<String, IFileChangeListener> fileChangeListenerMap
      = new ConcurrentHashMap<>();
  private WatchService watchService;
  private Map<String, WatchKey> fileWatchkeyMap = new HashMap<>();
  private ReentrantLock lock = new ReentrantLock();

  private FileWatcherManager() {
    try {
      watchService = FileSystems.getDefault().newWatchService();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  static public FileWatcherManager getInstance() {
    return instance;
  }

  public void addFileChangeListener(String filePath, IFileChangeListener fileChangeListener) {
    try {
      lock.lock();
      try {
        File fileToWatch = new File(filePath);
        Path pathToWatch = fileToWatch.toPath();
        if (fileToWatch.isFile()) {
          pathToWatch = fileToWatch.getParentFile().toPath();
        }

        WatchKey key = pathToWatch.register(watchService,
            new WatchEvent.Kind[]{StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE},
            SensitivityWatchEventModifier.HIGH);
        fileWatchkeyMap.put(filePath,key);

        fileChangeListenerMap.put(filePath, fileChangeListener);
      } finally {
        lock.unlock();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void startService() {
    new Thread(() -> {
      try {
        while (true) {
          WatchKey watchKey = watchService.poll();
          if (watchKey == null) {
            watchKey = watchService.take();
          }

          Path path = (Path) watchKey.watchable();
          List<WatchEvent<?>> events = watchKey.pollEvents();


          for (WatchEvent event : events) {
            Path fullPath = path.resolve((Path) event.context());
            String fullPathString = fullPath.toString();
            String parentPathString = fullPath.getParent().toFile().getAbsolutePath();
            if (event.kind() == StandardWatchEventKinds.OVERFLOW) {
              log.debug("OVERFLOW: " + event.context().toString());
              continue; //loop
            }
            if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
              log.debug("Created: " + event.context().toString());
              if (fileChangeListenerMap.containsKey(parentPathString)) {
                fileChangeListenerMap.get(parentPathString).fileCreated(new File(fullPathString));
              }
            }
            if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
              log.debug("Delete: " + event.context().toString());
              if(fileChangeListenerMap.containsKey(parentPathString)) {
                fileChangeListenerMap.get(parentPathString).fileDeleted(new File(fullPathString));
              }
            }
            if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
              log.debug("Modified: " + event.context().toString());
              if (fileChangeListenerMap.containsKey(fullPathString)) {
                fileChangeListenerMap.get(fullPathString).fileChanged(fullPath.toFile());
              }
            }
          }
          if (!watchKey.reset()) {
            break;
          }
        }
      } catch (InterruptedException e) {
        log.error( "FileWatcher Error", e);
        e.printStackTrace();
      }
    }).start();

    log.info("Service Stopped");
  }

  public void endService() {
    try {
      watchService.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void removeFileChangeListener(String filePathToUnwatch) {
    lock.lock();
    try {
      File fileToUnwatch = new File(filePathToUnwatch);
      Path pathToUnwatch = fileToUnwatch.toPath();
      if (fileToUnwatch.isFile()) {
        pathToUnwatch = fileToUnwatch.getParentFile().toPath();
      }

      WatchKey key = fileWatchkeyMap.get(filePathToUnwatch);
      if(key != null) {
        key.cancel();
      }
      fileWatchkeyMap.remove(filePathToUnwatch);
      fileChangeListenerMap.remove(filePathToUnwatch);

    } finally {
      lock.unlock();
    }
  }
}

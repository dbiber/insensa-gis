/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.helpers.r;

import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class RStarter {
  private static final Logger log = LoggerFactory.getLogger(RStarter.class);

  public static boolean launchRserve(String cmd) {
    return launchRserve(cmd, "--no-save --slave", "--no-save --slave", false);
  }

  public static boolean launchRserve(String cmd, String rargs, String rsrvargs, boolean debug) {
    try {
      Process p;
      boolean isWindows = false;
      String osname = System.getProperty("os.name");
      if (osname != null && osname.length() >= 7 && osname.substring(0, 7).endsWith("Windows")) {
        isWindows = true;
        p = Runtime.getRuntime().exec("\"" + cmd + "\" -e \"library(Rserve);Rserve("
            + (debug ? "TRUE" : "FALSE") + ",args='" + rsrvargs + "')\" " + rargs);
      } else {
        p = Runtime.getRuntime().exec(new String[]{"/bin/sh",
            "-c",
            "echo 'library(Rserve);Rserve(" +
                (debug ? "TRUE" : "FALSE") + ",args=\"" + rsrvargs + "\")'|" + cmd + " " + rargs

        });
      }
      log.debug("waiting for Rserve to start ... (" + p + ")");
      // we need to fetch the output - some platforms will die if you don't ...
      RStarterStreamFilter errorStream = new RStarterStreamFilter(p.getErrorStream(), false);
      RStarterStreamFilter outpuStream = new RStarterStreamFilter(p.getInputStream(), false);
      if (!isWindows) {
        p.waitFor();

        log.debug("call terminated, let us try to connect ...");
      }
    } catch (IOException | InterruptedException exception) {
      log.error("failed to start Rserve process with " + exception.getMessage());
      exception.printStackTrace();
      return false;
    }

    int attempts = 5;
    while (attempts > 0) {
      try {
        RConnection connection = new RConnection();
        log.info("RServe is running");
        connection.close();
        return true;
      } catch (Exception exception2) {
        log.debug("Try failed with: " + exception2.getMessage());
      }
      try {
        Thread.sleep(500);
      } catch (InterruptedException ix) {

      }
      attempts--;
    }
    return false;
  }



  /**
   * Checks whether Rserve is running and if that's not the case it attempts to start it using the
   * defaults for the platform where it is run on.
   * This method is meant to be set-and-forget and cover most default setups.
   * For special setups you may get more control over R with <<code>launchRserve</code> instead.
   */
  public static boolean checkLocalRserve() {
    if (isRserveRunning()) {
      return true;
    }
    String osname = System.getProperty("os.name");
    if (osname != null && osname.length() >= 7 && osname.substring(0, 7).equals("Windows")) {
      log.info("Windows: query registry to find where R is installed ...");
      String installPath = null;
      try {
        Process rp = Runtime.getRuntime().exec("reg query HKLM\\Software\\R-core\\R");
        RStarterStreamFilter regHog = new RStarterStreamFilter(rp.getInputStream(), true);
        rp.waitFor();
        regHog.join();
        installPath = regHog.getInstallPath();
      } catch (Exception rge) {
        log.error("ERROR: unable to run REG to find the location of R: " + rge);
        return false;
      }
      if (installPath == null) {
        log.error("ERROR: cannot find path to R. Make sure reg is available and R was installed with registry settings.");
        return false;
      }
      return launchRserve(installPath + "\\bin\\R.exe");
    }
    return (launchRserve("R") || /* try some common unix locations of R */
        ((new File("/Library/Frameworks/R.framework/Resources/bin/R")).exists()
            && launchRserve("/Library/Frameworks/R.framework/Resources/bin/R"))
        || ((new File("/usr/local/lib/R/bin/R")).exists()
        && launchRserve("/usr/local/lib/R/bin/R"))
        || ((new File("/usr/lib/R/bin/R")).exists()
        && launchRserve("/usr/lib/R/bin/R"))
        || ((new File("/usr/local/bin/R")).exists()
        && launchRserve("/usr/local/bin/R"))
        || ((new File("/sw/bin/R")).exists()
        && launchRserve("/sw/bin/R"))
        || ((new File("/usr/common/bin/R")).exists()
        && launchRserve("/usr/common/bin/R"))
        || ((new File("/opt/bin/R")).exists()
        && launchRserve("/opt/bin/R"))
    );
  }

  public static boolean isRserveRunning() {
    try {
      RConnection connection = new RConnection();
      log.info("Rserve is running");
      connection.close();
      return true;
    } catch (Exception exception) {
      log.warn("First connect try failed with. ");
    }
    return false;
  }

}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.helpers.r;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/** helper class that consumes output of a process. In addition,
 * it filter output of the REG command on Windows to look for InstallPath
 * registry entry which specifies the location of R.
 */
public class RStarterStreamFilter extends Thread {

  private static final Logger log = LoggerFactory.getLogger(RStarterStreamFilter.class);


  private InputStream is;
  private boolean capture;
  private String installPath;

  public RStarterStreamFilter(InputStream is, boolean capture) {
    this.is = is;
    this.capture = capture;
    start();
  }

  public String getInstallPath() {
    return installPath;
  }

  @Override
  public void run() {
    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      String line = null;
      while ((line = br.readLine()) != null) {
        if (capture) {
          int i = line.indexOf("InstallPath");
          if (i >= 0) {
            String s = line.substring(i+11).trim();
            int j = s.indexOf("REG_SZ");
            if (j >= 0) {
              s = s.substring(j+6).trim();
              installPath = s;
              log.info("R InstallPath = "+s);
            }
          } else {
            log.info("RServe>"+line);
          }
        }
      }
    } catch (IOException exception) {
      log.error("Error finding R InstallPath",exception);
    }
  }
}

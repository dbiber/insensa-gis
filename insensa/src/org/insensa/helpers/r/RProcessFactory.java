package org.insensa.helpers.r;


import org.insensa.Environment;
import org.insensa.updater.main.VersionChecker;
import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RProcessFactory {

    private static final Logger log = LoggerFactory.getLogger(RProcessFactory.class);
    private File rPath = new File("R");
    private VersionChecker versionChecker;
    private int installTries = 0;

    public RProcessFactory() {
        Optional<String> orPath = Environment.getRootProperties().getRPath();
        orPath.ifPresent(s -> rPath = new File(s));
    }

//  private void launchRCommand() throws IOException, InterruptedException {
//    String checkRserveCommand = rPath + "";
//
//    String rCmd = "local({r <- getOption('repos') \n"
//        + "r['CRAN']<-'http://cran.r-project.org'\n"
//        + "options(repos = r)\n"
//        + "})";
//
//    Process p = Runtime.getRuntime()
//        .exec(new String[]{rPath, rCmd});
//    p.waitFor();
//  }

    private boolean setManualRPath() {
        String msg = "If you want to be able to use R plugins, \n"
                + "you have to install \"R\" "
                + "as well as the R package \"Rserve\".\n"
                + "On Windows: install.packages(\"Rserve\")\n"
                + "On Linux/Mac: install.packages(\"Rserve\","
                + "\"Rserve_1.8-6.tgz\",\"http://www.rforge.net/\")\n\n"
                + "Set path to R binary (R.exe on Windows) or cancel to disable R plugins:";

        String sRPath = JOptionPane.showInputDialog(Environment.getInstance().getView(),
                msg,
                "Could not find R",
                JOptionPane.ERROR_MESSAGE);
        if (sRPath == null) {
            return false;
        }
        this.rPath = new File(sRPath);

        if (rPath.exists()) {
            Environment.getRootProperties().setRPath(rPath.getAbsolutePath());
            return true;
        } else {
            return setManualRPath();
        }
    }

    public void checkLocalRserve() {

        if (isRserveRunning()) {
            Environment.getRootProperties().setRServeRunning(true);
            log.info("RServe is running. ");
            return;
        }
        log.info("Rserve is not Running");
        if (!findR()) {
            log.info("Could not find R");
            if (!setManualRPath()) {
                log.info("Set manual path");
                Environment.getRootProperties().setRServeRunning(false);
                return;
            }
        }
        log.info("Found R.exe");
        if (!launchRserve()) {
            log.info("Could not start Rserve");
            installTries++;
            if (!installRserve() || installTries > 1) {
                log.info("Could not install and/or start ");
                Environment.getRootProperties().setRServeRunning(false);
                showMissingRserveMessage();
            } else {
                log.info("Installed Rserve");
                checkLocalRserve();
            }
            //Try to install
//            String msg = "If you want to be able to use R plugins, "
//                    + "you have to install \"R\" "
//                    + "as well as the R package \"Rserve\".\n"
//                    + "On Windows: install.packages(\"Rserve\")\n"
//                    + "On Linux/Mac: install.packages(\"Rserve\","
//                    + "\"Rserve_1.8-6.tgz\",\"http://www.rforge.net/\")\n\n"
//                    + "Would you like to try letting Insensa-GIS installing Rserve now ?";
//            int answer = JOptionPane.showConfirmDialog(Environment.getInstance().getView(),
//                    msg, "Could not launch Rserve",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
//            if (answer == JOptionPane.YES_OPTION) {
            //install Rserve

//            } else {
//                log.error("Error launching RServe");
//                Environment.getRootProperties().setRServeRunning(false);
//            }
        }
        Environment.getRootProperties().setRServeRunning(true);
    }

    private void showMissingRserveMessage() {
        String msg = "If you want to be able to use R plugins, "
                + "you have to install \"R\" "
                + "as well as the R package \"Rserve\".\n"
                + "On Windows: install.packages(\"Rserve\")\n"
                + "On Linux/Mac: install.packages(\"Rserve\","
                + "\"Rserve_1.8-6.tgz\",\"http://www.rforge.net/\")";
        JOptionPane.showMessageDialog(Environment.getInstance().getView(),
                msg, "Could not launch Rserve", JOptionPane.ERROR_MESSAGE);
    }

    private boolean installRserve() {
        Process p;
        try {
            if (versionChecker.getOsType() == VersionChecker.OS_WIN) {
                p = Runtime.getRuntime().exec("\"" + rPath.getAbsolutePath()
                        + "\" -e \"install.packages('Rserve',repos=c('http://cran.r-project.org'))");
            } else {
                p = Runtime.getRuntime().exec(new String[]{"/bin/sh",
                        "-c",
                        "echo 'install.packages(\"Rserve\",\"Rserve_1.8-6.tgz\",\"http://www.rforge.net/\");'|"
                                + rPath.getAbsolutePath()

                });
            }

            InputStream stderr = p.getErrorStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ( (line = br.readLine()) != null)
                log.warn("Error:" + line);
            int exitVal = p.waitFor();
            log.trace("Process exitValue: " + exitVal);
//            RStarterStreamFilter errorStream = new RStarterStreamFilter(p.getErrorStream(), false);
//            RStarterStreamFilter outpuStream = new RStarterStreamFilter(p.getInputStream(), false);

//            p.waitFor();
        } catch (InterruptedException | IOException e) {
            log.error("Error installing Rserve", e);
            return false;
        }

        log.trace("call terminated, let us try to connect ...");
        return true;
    }

    public boolean isRserveRunning() {
        try {
            RConnection connection = new RConnection();
            connection.close();
            return true;
        } catch (Exception exception) {
            log.trace("RServe Not Running. ");
        }
        return false;
    }

    public boolean findRInstallOnWindows() {
        log.info("Windows: query registry to find where R is installed ...");
        String installPath = null;
        try {
            Process rp = Runtime.getRuntime().exec("reg query HKLM\\Software\\R-core\\R");
            RStarterStreamFilter regHog = new RStarterStreamFilter(rp.getInputStream(), true);
            rp.waitFor();
            regHog.join();
            installPath = regHog.getInstallPath();
        } catch (Exception rge) {
            log.error("ERROR: unable to run REG to find the location of R: " + rge);
            return false;
        }
        if (installPath == null) {
            log.error("ERROR: cannot find path to R. Make sure reg is available and R was installed with registry settings.");
            return false;
        } else {
            installPath += "\\bin\\R.exe";
            rPath = new File(installPath);
        }
        return true;
    }

    public boolean findRInstallOnNix() {
        List<File> files = new ArrayList<File>() {
            {
                add(new File("/usr/bin/R"));
                add(new File("/Library/Frameworks/R.framework/Resources/bin/R"));
                add(new File("/usr/local/lib/R/bin/R"));
                add(new File("/usr/local/bin/R"));
                add(new File("/sw/bin/R"));
                add(new File("/usr/common/bin/R"));
                add(new File("/obt/bin/R"));
            }
        };
        for (File file : files) {
            if (file.exists()) {
                rPath = file;
                return true;
            }
        }
        return false;
    }

    public boolean launchRserve() {
        return launchRserve(rPath.getAbsolutePath());
    }

    private boolean launchRserve(String cmd) {
        return launchRserve(cmd, "--no-save --slave", "--no-save --slave", false);
    }

    private boolean launchRserve(String cmd, String rargs, String rsrvargs, boolean debug) {
        try {
            Process p;
            boolean isWindows = false;
            String osname = System.getProperty("os.name");
            if (osname != null && osname.length() >= 7 && osname.substring(0, 7).endsWith("Windows")) {
                isWindows = true;
                p = Runtime.getRuntime().exec("\"" + cmd + "\" -e \"library(Rserve);Rserve("
                        + (debug ? "TRUE" : "FALSE") + ",args='" + rsrvargs + "')\" " + rargs);
            } else {
                p = Runtime.getRuntime().exec(new String[]{"/bin/sh",
                        "-c",
                        "echo 'library(Rserve);Rserve("
                                + (debug ? "TRUE" : "FALSE")
                                + ",args=\""
                                + rsrvargs + "\")'|"
                                + cmd + " " + rargs

                });
            }
            log.trace("waiting for Rserve to start ... (" + p + ")");
            // we need to fetch the output - some platforms will die if you don't ...
            RStarterStreamFilter errorStream = new RStarterStreamFilter(p.getErrorStream(), false);
            RStarterStreamFilter outpuStream = new RStarterStreamFilter(p.getInputStream(), false);
            //if (!isWindows) {
            p.waitFor();

            log.trace("call terminated, let us try to connect ...");
            //}
        } catch (IOException | InterruptedException exception) {
            log.error("failed to start Rserve process with " + exception.getMessage());
            exception.printStackTrace();
            return false;
        }

        int attempts = 5;
        while (attempts > 0) {
            try {
                RConnection connection = new RConnection();
                log.info("RServe is running");
                connection.close();
                return true;
            } catch (Exception exception2) {
                log.debug("Try failed with: " + exception2.getMessage());
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException ix) {

            }
            attempts--;
        }
        return false;
    }

    public boolean findR() {
        if (rPath.exists()) {
            return true;
        }
        versionChecker = new VersionChecker();
        int type = versionChecker.getOsType();
        if (type == VersionChecker.OS_WIN) {
            return findRInstallOnWindows();
        } else {
            return findRInstallOnNix();
        }
    }
}

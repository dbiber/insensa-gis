/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.helpers.r;

import org.apache.commons.io.FilenameUtils;
import org.insensa.r.RMessageType;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class RCommandHelper {
  private static final Logger log = LoggerFactory.getLogger(RCommandHelper.class);


  /**
   * Enables the redirection of all R output to a log file (out.log).
   * The log file path depends on the workspace directory defined in the R session
   * (getwd())
   *
   * @param rConnection The R-Connection that should output to file
   */
  public static File loadOutput(RConnection rConnection, RMessageType types) throws REXPMismatchException, REngineException {
    evalAndPrint(rConnection, "options(warn=1)"); //Did not help
    String wd = rConnection.parseAndEval("getwd()").asString();
    File file = new File(wd + "/out.log");
    log.info("Temporary R working directory: {}", FilenameUtils.separatorsToUnix(file.getAbsolutePath()));
    evalAndPrint(rConnection,
        "out <- file('" + FilenameUtils.separatorsToUnix(file.getAbsolutePath()) + "',open='wt')");
//    evalAndPrint(rConnection, "sink(out)");
    if (types == RMessageType.MESSAGE || types == RMessageType.ALL) {
      evalAndPrint(rConnection, "sink(out, type='message')");
    }
    if (types == RMessageType.OUTPUT || types == RMessageType.ALL) {
      evalAndPrint(rConnection, "sink(out, type='output')");
    }
//    evalAndPrint(rConnection, "sink(out, type=" + types.toString() + ")");
    return file;
  }

  /**
   * Disables output File-Redirection and sends output back to console.
   * This is necessary when you want to access the log file (lock)
   */
  public static void unloadOuput(RConnection rConnection, RMessageType types) throws REXPMismatchException, REngineException {
    if (types == RMessageType.MESSAGE || types == RMessageType.ALL) {
      evalAndPrint(rConnection, "sink(type='message')");
    }
    if (types == RMessageType.OUTPUT || types == RMessageType.ALL) {
      evalAndPrint(rConnection, "sink(type='output')");
    }
  }

  /**
   * Send a normal command to the R-Connection but with try catch wrapped around it.
   * When Error happens, it will printed to console (at the moment)
   *
   * TODO: to more error handling/logging here
   */
  public static REXP evalAndPrint(RConnection rConnection, String command)
      throws REXPMismatchException, REngineException {
    REXP rResponseObject = rConnection.parseAndEval(
        "try(" + command + ")");
    if (rResponseObject.inherits("try-error")) {
      log.error("R Serve Eval Exception ; Command: "
          + command
          + " ; Error: "
          + rResponseObject.asString());
      throw new RuntimeException(rResponseObject.asString());
    }
    return rResponseObject;
  }

  public static REXP setDefaultCranLocation(RConnection rConnection) throws REXPMismatchException, REngineException {
    return evalAndPrint(rConnection, "local({r <- getOption('repos') \n"
        + "r['CRAN']<-'http://cran.r-project.org'\n"
        + "options(repos = r)\n"
        + "})");
  }

  public static RConnection prepareConnection(String address) throws RserveException {
    return new RConnection(address);
  }

  public static REXP sourceFile(RConnection rConnection, String filePath) throws REXPMismatchException, REngineException {
    return evalAndPrint(rConnection, "source('" + filePath + "',print.eval = TRUE)");
  }
}

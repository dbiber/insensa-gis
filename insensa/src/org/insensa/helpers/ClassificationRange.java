/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.helpers;

import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.IStorageData;

public class ClassificationRange implements IStorageData {
  private float lowValue;
  private float highValue;

  public ClassificationRange() {
  }

  public ClassificationRange(float minValue, float maxValue) {
    this.lowValue = minValue;
    this.highValue = maxValue;
  }

  public float getHighValue() {
    return this.highValue;
  }

  public void setHighValue(float highValue) {
    this.highValue = highValue;
  }

  public float getLowValue() {
    return this.lowValue;
  }

  public void setLowValue(float lowValue) {
    this.lowValue = lowValue;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    lowValue = restorer.loadFloat("minimum").get();
    highValue = restorer.loadFloat("maximum").get();
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("minimum" , getLowValue());
    saver.save("maximum" , getHighValue());
  }
}

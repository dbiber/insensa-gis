package org.insensa;

import org.gdal.gdalconst.gdalconstConstants;

public enum RasterFileAccess {
  READ_ONLY(gdalconstConstants.GA_ReadOnly),
  UPDATE(gdalconstConstants.GA_Update);

  private final int access;

  RasterFileAccess(int gdalAccess) {
    this.access = gdalAccess;
  }

  public int access() {
    return access;
  }
}

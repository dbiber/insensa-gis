/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.Driver;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.gdal.osr.SpatialReference;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.helpers.WorkerStatusCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;


public class RasterFile implements IRasterGisFile {
  private static final Logger log = LoggerFactory.getLogger(RasterFile.class);


  private int columns;
  private int rows;
  private float xllCorner;
  private float yllCorner;
  private float noDataValue;
  private float cellSize;

  private Dataset dataset;
  private Driver driver;
  private Band band;
  private int dataType;
  private String projectionWKT;
  private String projectedCS;
  private String geocentricCS;
  private String projection;
  private String geographicCS;
  private String datum;
  private String spheroid;
  private String localCS;
  private double[] geoTransform = new double[6];
  private SpatialReference spatialReference = null;

  private File fileRef;

  /**
   * Create a new File instance with path+name= newFullFileName and copy all
   * RasterFile properties!.
   */
  public RasterFile(RasterFile oldFile) throws IOException {
    this(oldFile, oldFile.getAbsolutePath());
  }

  /**
   * Create a new File instance with path+name= newFullFileName and copy all
   * RasterFile properties!.
   */
  public RasterFile(RasterFile oldFile, String newFullFileName)
      throws IOException {
    this(newFullFileName);
    if (!this.exists()) {
      this.geoTransform = oldFile.getGeoTransform();
      this.columns = oldFile.getNCols();
      this.rows = oldFile.getNRows();
      this.xllCorner = oldFile.getXllCorner();
      this.yllCorner = oldFile.getYllCorner();
      this.noDataValue = oldFile.getNoDataValue();
      this.cellSize = oldFile.getCellSize();
    }
  }

  /**
   * 1. Create a reference/link to a file by absoluteFilePath
   * 2. If file exists (does not have to
   *
   * @param absoluteFilePath the file to reference
   */
  public RasterFile(String absoluteFilePath) {
    this.fileRef = new File(absoluteFilePath);
    if (this.exists()) {
      this.readGdalFileInformations();
    }
  }

  public void createFileCopy(RasterFile oldFile, WorkerStatusCallback callback)
      throws IOException {
    if (!oldFile.exists()) {
      throw new IOException("RasterFile:createFileCopy: oldFile:\""
          + oldFile.getAbsolutePath() + "\" not exists");
    }
    if (this.exists()) {
      throw new IOException("RasterFile:createFileCopy: \""
          + this.getAbsolutePath() + "\" allready exists");
    }

    Dataset ds = oldFile.getDataset(RasterFileAccess.READ_ONLY);
    this.driver = gdal.GetDriverByName("GTiff");

    String outputFileName = this.getAbsolutePath();
    Vector<String> vOptions = new Vector<>();
    vOptions.add("COMPRESS=DEFLATE");
    this.dataset = this.driver.CreateCopy(outputFileName, ds, 1, vOptions, callback);

    oldFile.unlock();
    this.unlock();
    this.refresh();
  }

  public void createNewFile(IRasterGisFile oldFile,
                            DataTypeHelper.RasterDataType dataType,
                            double noDataValue)
      throws IOException {
    Dataset oldDataset = ((RasterFile) oldFile).getDataset(RasterFileAccess.READ_ONLY);

    this.driver = gdal.GetDriverByName("GTiff");
    String outputFileName = this.getAbsolutePath();
    Vector<String> optionVec = new Vector<String>();
    optionVec.add("COMPRESS=DEFLATE");
    int mDataType = DataTypeHelper.RasterDataType_GDAL.valueOf(dataType.name()).toGdalType();
    this.dataset = this.driver.Create(outputFileName, oldDataset.getRasterXSize(),
        oldDataset.GetRasterYSize(), 1, mDataType, optionVec);

    if (this.driver == null) {
      throw new IOException("RasterFile:createNewFile: unable to create Driver for File: "
          + outputFileName);
    }
    if (this.dataset == null) {
      throw new IOException("RasterFile:createNewFile: unable to create Dataset for File: "
          + outputFileName);
    }
    this.band = this.dataset.GetRasterBand(1);
    this.band.SetNoDataValue(noDataValue);
    this.dataset.SetGeoTransform(oldDataset.GetGeoTransform());
    this.dataset.SetProjection(oldFile.getProjectionWKT());
    this.unlock();
    oldFile.unlock();
    this.readGdalFileInformations();
  }

  /**
   * Opens the referenced file {@link #fileRef} using gdals and reads
   * all kinds of information to store within {@link RasterFile}
   * ({@link #noDataValue}, {@link #rows}, {@link #columns},
   * {@link #xllCorner} {@link #projectionWKT}...
   */
  private void readGdalFileInformations() {
    this.openGdalConnection(RasterFileAccess.READ_ONLY);
    Double[] nDV = new Double[1];
    this.band.GetNoDataValue(nDV);
    if (nDV[0] == null) {
      this.noDataValue = 0.0F;
    } else {
      this.noDataValue = nDV[0].floatValue();
    }
    this.rows = this.band.getYSize();
    this.columns = this.band.getXSize();
    this.dataset.GetGeoTransform(this.geoTransform);
    this.xllCorner = new Double(this.geoTransform[0]).floatValue();
    this.yllCorner = new Double(this.geoTransform[3] + this.dataset.getRasterYSize() * this.geoTransform[5]).floatValue();
    this.cellSize = new Double(this.geoTransform[1]).floatValue();
    this.dataType = this.band.getDataType();
    this.projectionWKT = this.dataset.GetProjection();
    if (!this.projectionWKT.isEmpty()) {
      this.spatialReference = new SpatialReference(this.projectionWKT);
      this.projectedCS = this.spatialReference.GetAttrValue("PROJCS");
      this.projection = this.spatialReference.GetAttrValue("PROJECTION");
      this.geographicCS = this.spatialReference.GetAttrValue("GEOGCS");
      this.datum = this.spatialReference.GetAttrValue("DATUM");
      this.spheroid = this.spatialReference.GetAttrValue("SPHEROID");
      this.geocentricCS = this.spatialReference.GetAttrValue("GEOCCS");
      this.localCS = this.spatialReference.GetAttrValue("LOCAL_CS");
    } else {
      File auxFile = new File(this.getAbsolutePath() + ".aux");
      if (auxFile.exists()) {
        Dataset auxDS = gdal.Open(auxFile.getAbsolutePath(), RasterFileAccess.READ_ONLY.access());
        if (auxDS != null) {
          this.projectionWKT = auxDS.GetProjection();
          if (!this.projectionWKT.isEmpty()) {
            this.spatialReference = new SpatialReference(this.projectionWKT);
            this.projectedCS = this.spatialReference.GetAttrValue("PROJCS");
            this.projection = this.spatialReference.GetAttrValue("PROJECTION");
            this.geographicCS = this.spatialReference.GetAttrValue("GEOGCS");
            this.datum = this.spatialReference.GetAttrValue("DATUM");
            this.spheroid = this.spatialReference.GetAttrValue("SPHEROID");
            this.geocentricCS = this.spatialReference.GetAttrValue("GEOCCS");
            this.localCS = this.spatialReference.GetAttrValue("LOCAL_CS");
          }
        }
      }
    }
    this.unlock();
  }

  public boolean delete() {
    // TODO TEST.. BISLANG NIX GEBRACHT
    this.unlock();
    return this.fileRef.delete();
  }

  @Override
  public boolean exists() {
    return this.fileRef.exists();
  }

  public File getAbsoluteFile() {
    return this.fileRef.getAbsoluteFile();
  }

  public String getAbsolutePath() {
    return this.fileRef.getAbsolutePath();
  }

  public Band getBand(RasterFileAccess gdalAccess) {
    if (this.band == null)
      this.openGdalConnection(gdalAccess);
    return this.band;
  }

  public float getCellSize() {
    return this.cellSize;
  }

  public void setCellSize(float cellSize) {
    this.cellSize = cellSize;
  }

  public Dataset getDataset(RasterFileAccess gdalAccess) {
    if (this.dataset == null)
      this.openGdalConnection(gdalAccess);
    return this.dataset;
  }

  public int getDataType() {
    return this.dataType;
  }

  public String getDatum() {
    return this.datum;
  }

  public Driver getDriver(RasterFileAccess gdalAccess) throws IOException {
    if (this.driver == null) {
      this.openGdalConnection(gdalAccess);
    }
    return this.driver;
  }

  public File getFileRef() {
    return this.fileRef;
  }

  public String getGeocentricCS() {
    return this.geocentricCS;
  }

  public String getGeographicCS() {
    return this.geographicCS;
  }

  public double[] getGeoTransform() {
    return this.geoTransform;
  }

  public String getName() {
    return this.fileRef.getName();
  }

  public int getNCols() {
    return this.columns;
  }

  public void setNCols(int nCols) {
    this.columns = nCols;
  }

  public float getNoDataValue() {
    return this.noDataValue;
  }

  public void setNoDataValue(float noDataValue) {
    this.noDataValue = noDataValue;
  }

  public int getNRows() {
    return this.rows;
  }

  public void setNRows(int nRows) {
    this.rows = nRows;
  }

  @Override
  public String getParent() {
    return this.fileRef.getParent();
  }

  @Override
  public boolean isTypeFloat() {
    int type = getDataType();
    return (type == gdalconstConstants.GDT_CFloat32
        || type == gdalconstConstants.GDT_CFloat64
        || type == gdalconstConstants.GDT_Float32
        || type == gdalconstConstants.GDT_Float64);

  }

  public String getProjectedCS() {
    return this.projectedCS;
  }

  public String getProjection() {
    return this.projection;
  }

  public String getProjectionWKT() {
    return this.projectionWKT;
  }

  public SpatialReference getSpatialReference() {
    return this.spatialReference;
  }

  public Map<String, String> getSpatialReferenceAttr() {
    Map<String, String> spm = new TreeMap<String, String>();
    if (this.projectedCS != null && !(this.projectedCS.isEmpty()))
      spm.put("Projected CS", this.projectedCS);
    if (this.projection != null && !(this.projection.isEmpty()))
      spm.put("Projection", this.projection);
    if (this.geocentricCS != null && !(this.geocentricCS.isEmpty()))
      spm.put("Geocentric CS", this.geocentricCS);
    if (this.geographicCS != null && !(this.geographicCS.isEmpty()))
      spm.put("Geographic CS", this.geographicCS);
    if (this.datum != null && !(this.datum.isEmpty()))
      spm.put("Datum", this.datum);
    if (this.spheroid != null && !(this.spheroid.isEmpty()))
      spm.put("Spheroid", this.spheroid);
    if (this.localCS != null && !(this.localCS.isEmpty()))
      spm.put("Local CS", this.localCS);
    return spm;
  }

  @Override
  public void writeRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException {
    getBand(RasterFileAccess.UPDATE).WriteRaster(xOff, yOff, xSize, ySize, arrayReference);
  }

  @Override
  public void readRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException {
    getBand(RasterFileAccess.READ_ONLY).ReadRaster(xOff, yOff, xSize, ySize, arrayReference);
  }

  @Override
  public float[] readRaster(int xOff, int yOff, int xSize, int ySize) throws IOException {
    int size = ySize * xSize;
    float[] data = new float[size];
    getBand(RasterFileAccess.READ_ONLY).ReadRaster(xOff, yOff, xSize, ySize, data);
    return data;
  }

  public String getSpheroid() {
    return this.spheroid;
  }

  public void getValues(RasterFile rasterFile) {
    this.geoTransform = rasterFile.getGeoTransform();
    this.columns = rasterFile.getNCols();
    this.rows = rasterFile.getNRows();
    this.cellSize = rasterFile.getCellSize();
    this.noDataValue = rasterFile.getNoDataValue();
    this.xllCorner = rasterFile.getXllCorner();
    this.yllCorner = rasterFile.getYllCorner();
  }

  /**
   * @return the xllCorner
   */
  public float getXllCorner() {
    return this.xllCorner;
  }

  public void setXllCorner(float xllCorner) {
    this.xllCorner = xllCorner;
  }

  /**
   * @return the yllCorner
   */
  public float getYllCorner() {
    return this.yllCorner;
  }

  public void setYllCorner(float yllCorner) {
    this.yllCorner = yllCorner;
  }

  /**
   * Opens a file file with gdal.
   * Stores class variables {@link #dataset}, {@link #driver}
   * and {@link #band}.
   *
   * @note only the first RasterBand (Layer) will be saved, it is
   * currently not possible to work with Files that have more bands.
   */
  private void openGdalConnection(RasterFileAccess gdalAccess) {
    String name = this.getAbsolutePath();
    if (this.exists()) {
      this.dataset = gdal.Open(name, gdalAccess.access());
      if (this.dataset == null) {
        throw new GisFileException("RasterFile:openGdalConnection: unable to create Dataset for File: " + name);
      }
      this.driver = this.dataset.GetDriver();
      if (this.driver == null) {
        throw new GisFileException("RasterFile:openGdalConnection: unable to create Driver for File: " + name);
      }
      this.band = this.dataset.GetRasterBand(1);
    } else {
      throw new GisFileException("RasterFile:openGdalConnection: File \"" + name + "\" does not exist");
    }
  }

  public void refresh() {
    if (this.exists()) {
      this.readGdalFileInformations();
    }
  }

  @Override
  public GisAttributes getGisAttributes() {
    GisAttributes gisAttributes = new GisAttributes();
    gisAttributes.add("NoDataValue", Float.toString(getNoDataValue()));
    gisAttributes.add("Columns", Integer.toString(getNCols()));
    gisAttributes.add("Rows", Integer.toString(getNRows()));
    gisAttributes.add("Cellsize", Float.toString(getCellSize()));
    try {
      gisAttributes.add("Data Type", DataTypeHelper.getFileTypeString(this));
    } catch (IOException e) {
      log.error("Could not determine datatype name", e);
    }
    Map<String, String> sRM = getSpatialReferenceAttr();
    Iterator<String> nameIter = sRM.keySet().iterator();
    while (nameIter.hasNext()) {
      String name = nameIter.next();
      String value = sRM.get(name);
      gisAttributes.add(name, value);
    }
    return gisAttributes;
  }

  public void relocateFile(String newPath) {
    this.fileRef = new File(newPath);
  }

  /**
   * just renames the file reference and saves dest as new file reference.
   */
  public boolean renameTo(File dest) {
    this.unlock();
    boolean rename = this.fileRef.renameTo(dest);
    if (!rename) {
      throw new GisFileException("rename from "
          + this.fileRef.getAbsolutePath()
          + " to "
          + dest.getAbsolutePath()
          + " failed");
    } else {
      // check if a aux.xml file is aviable
      File auxXmlFile = new File(this.fileRef.getAbsolutePath() + ".aux.xml");
      File newAuxXmlFile = new File(dest.getAbsolutePath() + ".aux.xml");
      if (auxXmlFile.exists()) {
        rename = auxXmlFile.renameTo(newAuxXmlFile);
      }
      this.fileRef = dest;
      if (!rename) {
        throw new GisFileException("rename from " + auxXmlFile.getAbsolutePath() + " to " + newAuxXmlFile.getAbsolutePath() + " failed");
      }
    }
    return true;
  }

  public void unlock() {
    if (this.band != null) {
      this.band.FlushCache();
    }
    if (this.dataset != null) {
      this.dataset.FlushCache();
    }
    if (this.band != null) {
      this.band.delete();
    }
    if (this.dataset != null) {
      this.dataset.delete();
    }
    this.band = null;
    this.dataset = null;
    this.driver = null;
  }

  public void writeNodataValue(double nodataValue) throws IOException {
    if (this.exists()) {
      this.openGdalConnection(RasterFileAccess.UPDATE);
      Double[] ndV = new Double[1];
      int test = this.band.SetNoDataValue(nodataValue);
      this.band.GetNoDataValue(ndV);
      if (test == gdalconstConstants.CE_None) {
      } else if (test == gdalconstConstants.CE_Failure) {
        throw new IOException("RasterFile: error setting NodataValue " + nodataValue);
      }
    }
    this.unlock();
  }

  private void setAndLogNodataValue(double nodataValue) {
    log.debug("SET NodataValue for {} : to max which is {}", this.getName(), nodataValue);

    int test = this.band.SetNoDataValue(nodataValue);
    if (test == gdalconstConstants.CE_None) {
      log.debug("No Error setting nodata value");
    } else if (test == gdalconstConstants.CE_Failure) {
      log.error("FAILURE setting nodata value to {}", nodataValue);
    }
  }

  public void writeNodataValueMax() {
    if (this.exists()) {
      this.openGdalConnection(RasterFileAccess.UPDATE);
      int type = this.band.getDataType();
      if (type == gdalconstConstants.GDT_Float32) {
        setAndLogNodataValue(Float.MAX_VALUE);
      } else if (type == gdalconstConstants.GDT_Byte) {
        setAndLogNodataValue(128);
      } else if (type == gdalconstConstants.GDT_UInt16) {
        setAndLogNodataValue(65535);
      } else if (type == gdalconstConstants.GDT_Int16) {
        setAndLogNodataValue(128);
      }
    }
    this.unlock();
  }

  @Override
  public GisFileType getType() {
    return GisFileType.GEO_TIFF;
  }

  @Override
  public String getDriverShortName() throws IOException {
    return getDriver(RasterFileAccess.READ_ONLY).getShortName();
  }
}

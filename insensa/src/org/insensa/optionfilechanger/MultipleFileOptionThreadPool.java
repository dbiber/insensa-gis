/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.optionfilechanger;

import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ExecDependencyController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MultipleFileOptionThreadPool {
  private Map<IGisFileContainer, ExecDependencyController> depMap = new HashMap<IGisFileContainer, ExecDependencyController>();
  private Map<IGisFileContainer, ArrayList<Runnable>> runnableMap = new HashMap<IGisFileContainer, ArrayList<Runnable>>();
  private ExecutorService exeService;


  public MultipleFileOptionThreadPool() {
    int prozCount = Runtime.getRuntime().availableProcessors();
    if (prozCount > 4)
      prozCount = 4;
    this.exeService = Executors.newFixedThreadPool(1);
    // TODO Schlaegt oft fehl. evtl wegen fehlender pthreads in gdal
    // kann nicht sein
    // exeService = Executors.newFixedThreadPool(prozCount);
  }

  public void addOptionThread(IGisFileContainer rasterFile, Runnable threadObject) {
    if (this.depMap.get(rasterFile) == null)
      this.depMap.put(rasterFile, rasterFile.getDepChecker());
    this.runnableMap.computeIfAbsent(rasterFile, k -> new ArrayList<Runnable>());
    this.runnableMap.get(rasterFile).add(threadObject);
    this.depMap.get(rasterFile).setDependency((OptionFileChanger) threadObject);
  }

  public void executeThreads(List<IGisFileContainer>rasterFileList,
                             List<WorkerStatusList> workerStatusListList) throws IOException {
    for (int j = 0; j < rasterFileList.size(); j++) {
      int unusedCnt = 0;
      List<Runnable> tmpRunList = this.runnableMap.get(rasterFileList.get(j));
      List<Runnable> runList = new ArrayList<Runnable>();
      for (int i = 0; i < tmpRunList.size(); i++) {
        if (!((OptionFileChanger) tmpRunList.get(i)).isUsed()) {
          runList.add(tmpRunList.get(i));
          unusedCnt++;
        }
      }
      if (workerStatusListList.get(j) != null) {
        // workerStatusListList.get(j).startAllProcesses(rasterFileList.get(j).getName(),
        // unusedCnt);
        workerStatusListList.get(j).startAllProcesses(rasterFileList.get(j).getOutputFileName(), unusedCnt);
      }
      for (int i = 0; i < runList.size(); i++) {
        if (workerStatusListList.get(j) != null)
          ((OptionFileChanger) runList.get(i)).setWorkerStatus(workerStatusListList.get(j).getWorkerStatus(i));
        this.exeService.execute(runList.get(i));
      }
    }
  }

  public ExecutorService getExeService() {
    return this.exeService;
  }

  public void removeOptionFileChanger(RasterFileContainer rasterFile, OptionFileChanger option)
      throws IOException {
    if (!this.runnableMap.get(rasterFile).remove(option))
      throw new IOException("MultipleFileOptionThreadPool: removeOptionFileChanger: Option: " + option.getId() + " not found");
    this.depMap.get(rasterFile).unsetDependency(option);
  }
}

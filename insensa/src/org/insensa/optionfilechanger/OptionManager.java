/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.optionfilechanger;

import org.insensa.Environment;
import org.insensa.GisFileType;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.IStorageCloner;
import org.insensa.model.storage.JdomStorageCloner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class OptionManager {
  private static final Logger log = LoggerFactory.getLogger(OptionManager.class);
  private IStorageCloner cloner = new JdomStorageCloner();
  private ExtensionManager exManager;

  /**
   * Creates a new OptionManager Objects that manages the Option types and the
   * subtypes listed in a XML File.
   */
  public OptionManager() {
    this.exManager = ExtensionManager.getInstance();
  }

  public OptionFileChanger getFileChangerCopy(OptionFileChanger option)
      throws IOException {
    cloner.store(option.getId(), option);
    Optional<OptionFileChanger> tmpOption = this.getOption(option.getId());
    if (!tmpOption.isPresent()) {
      throw new IOException("Error getting option " + option.getId());
    }
    tmpOption.get().restore(cloner.getRestorer());
    tmpOption.get().setId(option.getId());
    return tmpOption.get();
  }

  public Optional<OptionFileChanger> getOption(String optionName) {
    ClassLoader cl = this.exManager.getUrlClassLoader();// InsensaGIS.class.getClassLoader();
    ServiceList list = this.exManager.getOptionServices().get(optionName);
    if (list == null) {
      log.warn("No services found for option \"{}\"", optionName);
      return Optional.empty();
    }
    Service service = list.getService(ServiceType.EXEC);
    if (service == null) {
      log.error("No service type \"exec\" found for option \"{}\"", optionName);
      return Optional.empty();
    } else {
      try {
        if (service.getScript() != null) {
          ROptionFileChanger rOptionFileChanger =
              new ROptionFileChanger(optionName);
          Path absFilePath = service.getScript().extractFromFile();
          rOptionFileChanger.setScriptPath(absFilePath.toString());
          Environment.refreshDefaultPluginVariables(rOptionFileChanger);
          return Optional.of(rOptionFileChanger);
        }
        Class classInfoReader = cl.loadClass(service.getPackageName() + "." + service.getClassName());
        OptionFileChanger optionFileChanger = (OptionFileChanger) classInfoReader.newInstance();
        if (optionFileChanger != null) {
          optionFileChanger.setId(optionName);
        }
        return Optional.ofNullable(optionFileChanger);
      } catch (ClassNotFoundException
          | IllegalAccessException
          | InstantiationException
          | IOException e) {
        log.error("Error creating option \"" + optionName + "\"", e);
        return Optional.empty();
      }

    }
  }

  public List<String> getOptionFileChangerList(GisFileType fileType) {
    List<String> lOption = new ArrayList<>();
    for (String name : this.exManager.getOptionServices().keySet()) {
      ServiceList serviceList = this.exManager.getOptionServices().get(name);
      Service service = serviceList.getService(ServiceType.EXEC);
      if (service != null && service.getGisType() == fileType) {
        lOption.add(name);
      }
    }
    return lOption;
  }

}

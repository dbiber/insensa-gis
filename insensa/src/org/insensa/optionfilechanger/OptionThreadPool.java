/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.optionfilechanger;

import org.insensa.IGisFileContainer;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.ExecDependencyController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class OptionThreadPool {
  private ExecutorService exeService;
  private List<Runnable> runnableList = new ArrayList<>();

  public OptionThreadPool() {
    this.exeService = Executors.newFixedThreadPool(1);
  }

  public void addOptionThread(Runnable runnableObject) {
    this.runnableList.add(runnableObject);
  }

  public void executeThreads(WorkerStatusList workerStatusList, IGisFileContainer targetFile) throws IOException {
    List<Runnable> runList = new ArrayList<>();
    for (int i = 0; i < this.runnableList.size(); i++) {
      if (((OptionFileChanger) this.runnableList.get(i)).isUsed() != true) {
        runList.add(this.runnableList.get(i));
      }
    }

    if (workerStatusList != null) {
      workerStatusList.startAllProcesses("Execute option on: " + targetFile.getName(), runList.size());
    }

    for (int i = 0; i < runList.size(); i++) {
      if (workerStatusList != null)
        ((OptionFileChanger) runList.get(i)).setWorkerStatus(workerStatusList.getWorkerStatus(i));
      this.exeService.execute(runList.get(i));
    }

  }

  public void removeOptionFileChanger(OptionFileChanger option) throws IOException {
    if (!this.runnableList.remove(option))
      throw new IOException("OptionThreadPool: removeOptionFileChanger: option " + option.getId() + " not found");
  }
}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.optionfilechanger;


import org.insensa.FileInspector;
import org.insensa.GenericGisFileSet;
import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.Project;
import org.insensa.WorkerStatus;
import org.insensa.WorkerStatusList;
import org.insensa.helpers.AttributeTable;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.helpers.IOHelper;
import org.insensa.helpers.SystemSpec;
import org.insensa.helpers.VariableValue;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;
import org.insensa.model.storage.AttributeTableSaver;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class AbstractOptionFileChanger
    implements OptionFileChanger {
  public static final String XML_ATTR_OLD_PATH = "oldPath";
  public static final String XML_ATTR_OLD_NAME = "oldName";
  public static final String XML_ELEMENT_SOURCEFILE = "sourcefile";
  private static final Logger log = LoggerFactory.getLogger(AbstractOptionFileChanger.class);
  protected final String XML_ATTR_USED = "used";
  protected final String XML_ATTR_ORDER_ID = "orderId";
  protected boolean used;
  protected int orderId = 0;
  protected IGisFileContainer oldRasterFile;
  protected WorkerStatus workerStatus = null;
  protected float processStatus = 0.0F;
  protected String errorMessage = "";
  protected String oldFilePath;
  protected String oldFileName;
  private String id;
  private List<String> infoDependencies = new ArrayList<>();

  public IGisFileContainer getActualFileContainer() {
    IGisFileContainer actualRasterFile;
    if (this.oldRasterFile == null) {
      throw new RuntimeException("No Source File");
    } else {
      // wenn mehrere Optionen hintereinander ausgefuehrt wurden
      if (this.oldRasterFile.getTargetGisFileContainer() != null) {
        actualRasterFile = this.oldRasterFile.getTargetGisFileContainer();
      } else
        actualRasterFile = this.oldRasterFile;
    }
    return actualRasterFile;
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save(XML_ATTR_USED, used);
    saver.save(XML_ATTR_ORDER_ID, orderId);
    if (oldFilePath != null && oldFileName != null) {
      saver.save(XML_ELEMENT_SOURCEFILE, saver1 -> {
        saver1.save(XML_ATTR_OLD_PATH, oldFilePath);
        saver1.save(XML_ATTR_OLD_NAME, oldFileName);
      });
    }
  }

  @Override
  public String getOldFileRelativeFilePath() {
    if (oldFilePath != null && oldFileName != null) {
      return oldFilePath + oldFileName;
    } else {
      return null;
    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    used = restorer.loadBoolean(XML_ATTR_USED).get();
    orderId = restorer.loadInteger(XML_ATTR_ORDER_ID).get();
    restorer.loadCollection(XML_ELEMENT_SOURCEFILE, restorer1 -> {
      oldFileName = restorer1.loadString(XML_ATTR_OLD_NAME).get();
      oldFilePath = restorer1.loadString(XML_ATTR_OLD_PATH).get();
    });
  }

  @Override
  public AttributeTable getData() {
    AttributeTableSaver attributeTableSaver = new AttributeTableSaver();
    save(attributeTableSaver);
    return attributeTableSaver.getTable();
  }

  /**
   * returns the errorMessage if there is any. After calling this method, the
   * errorMessage will be deleted(set to empty String)
   */
  @Override
  public String getErrorMessage() {
    String errorMessage = this.errorMessage;
    this.errorMessage = "";
    return errorMessage;
  }

  @Override
  public int getOrderId() {
    return this.orderId;
  }

  @Override
  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  @Override
  public List<String> getInfoDependencies() {
    return infoDependencies;
  }

  @Override
  public void setInfoDependencies(List<String> infoDependencies) {
    this.infoDependencies = infoDependencies;
  }

  @Override
  public boolean isUsed() {
    return this.used;
  }

  @Override
  public void setUsed(boolean used) {
    this.used = used;
  }

  @Override
  public void run() {
    ExecDependencyController depChecker = null;
    if (this.oldRasterFile == null) {
      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(100.0F);
        this.workerStatus.setProgressName("ERROR: Original file == null");
        this.workerStatus.errorProcess();
      }
    } else {
      try {
        ((FileInspector) this.getActualFileContainer()).setLock(true);
        depChecker = this.getActualFileContainer().getDepChecker();
        if (this.workerStatus != null)
          this.workerStatus.setProgressName(this.getId());

        boolean error = depChecker.checkOptionPriority(this);
        if (error)
          throw new IOException("Error in previos OptionFileChanger");
        this.write();
        this.oldRasterFile.getGisFileInformationStorage()
            .refreshPluginExec(this);
        // TODO TEST
        if (this.oldRasterFile.getTargetGisFileContainer() != null)
          ((GenericGisFileSet) this.oldRasterFile.getParentModule())
              .replaceGisFileWithTarget((IGisFileContainer) this.oldRasterFile,
                  this);
        ((FileInspector) this.oldRasterFile).setLock(false);
        // depChecker.unsetOptionRunnung();
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.endProgress();
        }
        depChecker.endRunnable(this);
      } catch (Exception e) {
        log.error("UNKNOWN", e);
        if (this.workerStatus != null) {
          this.workerStatus.refreshPercentage(100.0F);
          this.workerStatus.setProgressName(e.getMessage());
          this.workerStatus.errorProcess();
          e.printStackTrace();
        }
        if (depChecker != null)
          depChecker.errorRunnableOrderId(this);
      } finally {
        ((FileInspector) this.getActualFileContainer()).setLock(false);
      }
    }
  }

  protected void saveRasterFile(IGisFileContainer tmpRasterFile,
                                IGisFileContainer actualRasterFile) throws IOException {
    IOHelper.saveRasterFile(tmpRasterFile, actualRasterFile);
    this.oldRasterFile.setTargetGisFileContainer(tmpRasterFile);
  }

  @Override
  public void setOldGisFile(IGisFileContainer oldRasterFile) {
    this.oldRasterFile = oldRasterFile;
    if (!used) {
      String absolutePath = this.oldRasterFile.getGisFile().getAbsolutePath();
      Project project = this.oldRasterFile.getParentModule().getProject();
      if (!project.isRelativeToProject(absolutePath))
        return;
      String relativePath = project.getPathRelativeToProject(absolutePath);
      String oldName = relativePath.substring(relativePath.lastIndexOf(SystemSpec.separator) + 1);
      relativePath = relativePath.replace(oldName, "");
      this.oldFilePath = relativePath;
      this.oldFileName = oldName;
    }
  }

  @Override
  public void setWorkerStatus(WorkerStatus workerStatus) {
    this.workerStatus = workerStatus;
  }

  protected void solveDependencies(IGisFileContainer actualRasterFile) throws IOException {
    if (this.workerStatus != null)
      this.workerStatus.setProgressName(this.getId() + ": Resolve Dependencies");
    // workerStatus.startPause("Resolve Dependencies");
    WorkerStatusList childWorkerStatusList = null;
    WorkerStatusList wList = this.workerStatus.getParentWorkerStatusList();
    if (wList != null)
      childWorkerStatusList = wList.getChildWorkerStatusList(this.workerStatus);
    // if(childWorkerStatusList!=null)
    ExecDependencyController iDepChecker;
    InfoManager iManager = new InfoManager();
    iDepChecker = actualRasterFile.getDepChecker();
    for (String idep : this.getInfoDependencies()) {
      actualRasterFile.addInfoReader(iManager.getInfoReader(idep), true);
    }
    for (String idep : this.getInfoDependencies()) {
      if (childWorkerStatusList != null)
        actualRasterFile.executeInfo(idep, childWorkerStatusList);
      else
        actualRasterFile.executeInfo(idep, null);
    }
    iDepChecker.checkInfoDependencies(this);

    InfoReader iReader;
    for (String idep : this.getInfoDependencies()) {
      iReader = actualRasterFile.getInfoReader(idep);
      if (iReader == null)
        throw new IOException("No InfoReader " + idep + "found");
      if (!iReader.isUsed()) {
        iDepChecker.resetRunnable(iReader);
        if (iReader.getErrorMessage().isEmpty())
          throw new IOException("InfoReader \"" + idep + "\" is not ready");
        else
          throw new IOException(iReader.getId() + " :" + iReader.getErrorMessage());
      }
    }
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public List<VariableValue> getVariableList() {
    return Collections.emptyList();
  }

  @Override
  public void setVariableList(List<VariableValue> vaList) {
  }

  @Override
  public String toString() {
    return getId();
  }

  @Override
  public String getGroupName() {
    return "options";
  }

  @Override
  public IGisFileContainer getParentFileContainer() {
    return getActualFileContainer();
  }
}

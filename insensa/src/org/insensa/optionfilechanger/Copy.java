/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.optionfilechanger;

import org.insensa.GisFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFile;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;


public class Copy extends AbstractRasterOptionFileChanger {
  public static final String NAME = "Copy";

  private String description;
  private boolean overwrite;


  public Copy() {
    this.overwrite = false;
    this.used = false;
  }

  public boolean checkApproval(IGisFileContainer rasterFile) {
    if (!this.isUsed()) {
      if (rasterFile.getGisFile().equals(rasterFile.getAbsOutputDirectoryPath() + rasterFile.getOutputFileName())) {
        this.errorMessage = "File " + rasterFile + " is already a hardcopy";
        return false;
      }
    }
    if (rasterFile.getUnusedOptionCount() > 0) {
      this.errorMessage = "File " + rasterFile + " has unused OptionFileChanger";
      return false;
    }
    return true;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
  }

  //  @Override
//  public Element getOptionElement() {
//    Element eOption = super.getOptionElement();
//
//    Element eOptionDescription = new Element("description");
//    eOptionDescription.setText(this.description);
//    Element eOptionOverwrite = new Element("overwrite");
//    if (!this.overwrite)
//      eOptionOverwrite.setAttribute("value", "false");
//    else
//      eOptionOverwrite.setAttribute("value", "true");
//    eOption.addContent(eOptionDescription);
//    eOption.addContent(eOptionOverwrite);
//
//    return eOption;
//  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
  }


//  @Override
//  public void setOptionAttributes(Element eOption) throws IOException {
//    super.setOptionAttributes(eOption);
//    this.overwrite = Boolean.valueOf(eOption.getChild("overwrite").getAttributeValue("value"));
//  }

  public void write() throws IOException, JDOMException, GisFileException {
    IGisFileContainer actualRasterFile;
    if (this.oldRasterFile == null) {
      throw new IOException("No Source File");
    } else {
      // wenn mehrere Optionen hintereinander ausgefuehrt wurden
      if (this.oldRasterFile.getTargetGisFileContainer() != null) {
        // throw new IOException("There is a Target File");
        actualRasterFile = this.oldRasterFile.getTargetGisFileContainer();
      } else
        actualRasterFile = this.oldRasterFile;
    }

    if (this.workerStatus != null) {
      this.workerStatus.setProgressName("Copy");
      this.workerStatus.startProcess();
    }

    // Erstelle neues raster File als Copy des alten mit dem neuen QuellPfad
    // und Namen
    //TODO: Factory ?
    IGisFileContainer outputRasterFile =
        new RasterFileContainer(
            actualRasterFile.getAbsOutputDirectoryPath() +
                actualRasterFile.getOutputFileName(),
            (GisFileContainer) actualRasterFile,
            actualRasterFile.getParentModule(),
            true, true, false);
    ((RasterFile) outputRasterFile.getGisFile()).createFileCopy((RasterFile) actualRasterFile.getGisFile(),
        new WorkerStatusCallback(this.workerStatus));

    this.oldRasterFile.setTargetGisFileContainer(outputRasterFile);

    this.used = true;

  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.optionfilechanger;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.insensa.exceptions.GisFileException;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.ClassificationRange;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.helpers.IOHelper;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.PrecisionValues;
import org.insensa.inforeader.QuantileBreaksValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class QuantileBreaks extends AbstractRasterOptionFileChanger implements BreaksValues {

  public static final String QUANTILE_BREAKS = "quantileBreaks";
  private static final String NUM_OF_CLASSES = "numOfClasses";
  private static final String TYPE = "type";
  private QuantileBreaksValues breaksValues = null;
  private String description = "Sort values in Quantile Breaks";
  private int numOfClasses = 5;
  private int type = 8;

  @Override
  public List<String> getInfoDependencies() {
    List<String> retList = new ArrayList<>();
    retList.add(QuantileBreaksValues.QUANTILE_BREAKS_VALUES);
    return retList;
  }

  @Override
  public int getNumOfClasses() {
    return this.numOfClasses;
  }

  @Override
  public void setNumOfClasses(int numOfClasses) {
    this.numOfClasses = numOfClasses;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
  }

  //  @Override
//  public Element getOptionElement() {
//    Element eOption = super.getOptionElement();
//    eOption.setAttribute(NUM_OF_CLASSES, Integer.toString(this.numOfClasses));
//    eOption.setAttribute(TYPE, Integer.toString(this.type));
//
//    Element eOptionDescription = new Element("description");
//    eOptionDescription.setText(this.description);
//    eOption.addContent(eOptionDescription);
//
//    return eOption;
//  }

  public QuantileBreaksValues getQuantileBreaksValues() {
    return this.breaksValues;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
//    throw new NotImplementedException("TO IMPLEMENT");
  }

  //  @Override
//  public void setOptionAttributes(Element eOption) throws IOException {
//    super.setOptionAttributes(eOption);
//    Attribute numOfClassesAttr = eOption.getAttribute(NUM_OF_CLASSES);
//    Attribute typeAttr = eOption.getAttribute(TYPE);
//    try {
//      if (numOfClassesAttr != null)
//        this.numOfClasses = numOfClassesAttr.getIntValue();
//      if (typeAttr != null)
//        this.type = numOfClassesAttr.getIntValue();
//    } catch (DataConversionException e) {
//      throw new IOException("QuantileBreaks: setInfo: Wrong Input Data");
//    }
//  }

  public void setType(int type) {
    this.type = type;
  }

  @Override
  public void write() throws IOException, JDOMException, GisFileException {
    RasterFileContainer actualRasterFile = (RasterFileContainer) this.getActualFileContainer();
    actualRasterFile.addInfoReader(new InfoManager().getInfoReader(QuantileBreaksValues.QUANTILE_BREAKS_VALUES), true);
    // QuantileBreaksValues iBreaks = (QuantileBreaksValues)
    // actualRasterFile.getInfoReader(QuartileValues.NAME);
    QuantileBreaksValues iBreaks = (QuantileBreaksValues) actualRasterFile.getInfoReader(QuantileBreaksValues.QUANTILE_BREAKS_VALUES);
    if (iBreaks == null)
      throw new IOException("Error adding InfoReader " + QuantileBreaksValues.QUANTILE_BREAKS_VALUES);
    iBreaks.setNumOfClasses(this.numOfClasses);
    iBreaks.setType(this.type);
    actualRasterFile.getGisFileInformationStorage().refreshPluginExec(iBreaks);

    this.solveDependencies(actualRasterFile);
    float steps = 100.0F / actualRasterFile.getNRows();

    if (this.workerStatus != null) {
      this.workerStatus.endPause();
      this.workerStatus.setProgressName("Calc QuantileBreaks");
      this.workerStatus.startProcess();
    }

    this.breaksValues = (QuantileBreaksValues) actualRasterFile.getInfoReader(QuantileBreaksValues.QUANTILE_BREAKS_VALUES);
    if (this.breaksValues.getRangeList().size() > 254)
      throw new IOException("Maximum number of 254 breaks exceeded");
    PrecisionValues precValue = (PrecisionValues) actualRasterFile.getInfoReader(PrecisionValues.NAME);
    if (precValue == null || !precValue.isUsed()) {
      throw new IOException("Precision Values not found");
    }
    int maxPrec = precValue.getMaxPrecision();
    int prec = (int) Math.pow(10, maxPrec);

    String tmpName = IOHelper.createTmpAbsoluteFilePath(actualRasterFile);

    // Erstelle neues raster File als Copy des alten mit dem neuen Pfad und Namen
    RasterFileContainer tmpRasterFile = new RasterFileContainer(tmpName, actualRasterFile,
        actualRasterFile.getParentModule(), false, true, false);
    if (tmpRasterFile.getGisFile().exists()) {
      throw new IOException("Tmp file already exists");
    }
    tmpRasterFile.getGisFile().createNewFile(actualRasterFile.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Byte, 255);
    Band band = actualRasterFile.getBand(RasterFileAccess.READ_ONLY);
    Dataset tmpDataset = tmpRasterFile.getDataset(RasterFileAccess.UPDATE);

    List<ClassificationRange> rangeList = this.breaksValues.getRangeList();
    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    float readValue;

    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        if (readValue == actualRasterFile.getNoDataValue()) {
          dData[i] = 255;
        } else {
          // TODO BUG:141 High und Low Values wurd gerundet, also muss
          readValue *= prec;
          readValue = Math.round(readValue);
          readValue /= prec;
          if (readValue >= rangeList.get(0).getLowValue() && readValue <= rangeList.get(0).getHighValue()) {
            dData[i] = 1;
          } else {
            for (int k = 1; k < rangeList.size(); k++) {
              if (readValue > rangeList.get(k).getLowValue() && readValue <= rangeList.get(k).getHighValue()) {
                dData[i] = k + 1;
              }
            }
          }
        }
      }
      tmpDataset.GetRasterBand(1).WriteRaster(0, j, xSize, 1, dData);
      this.processStatus += steps;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    tmpRasterFile.unlock();
    actualRasterFile.unlock();
    this.saveRasterFile(tmpRasterFile, actualRasterFile);
    this.used = true;

  }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.optionfilechanger;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.insensa.exceptions.GisFileException;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.ClassificationRange;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.inforeader.EqualBreaksValues;
import org.insensa.inforeader.InfoManager;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class EqualBreaks extends AbstractRasterOptionFileChanger implements BreaksValues {
  public static final String EQUAL_BREAKS = "equalBreaks";
  private static final String NUM_OF_CLASSES = "numOfClasses";
  private EqualBreaksValues breaksValues = null;
  private String description = "Sort values in Equal Breaks";
  private int numOfClasses = 5;

  public EqualBreaksValues getEqualBreaksValues() {
    return this.breaksValues;
  }

  @Override
  public String getId() {
    return EQUAL_BREAKS;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> retList = new ArrayList<String>();
    retList.add(EqualBreaksValues.NAME);
    return retList;
  }

  @Override
  public int getNumOfClasses() {
    return this.numOfClasses;
  }

  @Override
  public void setNumOfClasses(int numOfClasses) {
    this.numOfClasses = numOfClasses;

  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save(NUM_OF_CLASSES, numOfClasses);
  }

  //  @Override
//  public Element getOptionElement() {
//    Element eOption = super.getOptionElement();
//
//    eOption.setAttribute(NUM_OF_CLASSES, Integer.toString(this.numOfClasses));
//
//    Element eOptionDescription = new Element("description");
//    eOptionDescription.setText(this.description);
//    eOption.addContent(eOptionDescription);
//
//    return eOption;
//  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    this.numOfClasses = restorer.loadInteger(NUM_OF_CLASSES).orElse(5);
  }


//  @Override
//  public void setOptionAttributes(Element eOption) throws IOException {
//
//    super.setOptionAttributes(eOption);
//    Attribute numOfClasses = eOption.getAttribute(NUM_OF_CLASSES);
//
//    try {
//      if (numOfClasses != null)
//        this.numOfClasses = numOfClasses.getIntValue();
//    } catch (DataConversionException e) {
//      throw new IOException("EqualBreaks: setInfo: Wrong Input Data");
//    }
//  }

  @Override
  public void write() throws IOException, JDOMException, GisFileException {
    RasterFileContainer actualRasterFile = (RasterFileContainer) this.getActualFileContainer();
    actualRasterFile.addInfoReader(new InfoManager().getInfoReader(EqualBreaksValues.NAME), true);
    EqualBreaksValues iBreaks = (EqualBreaksValues) actualRasterFile.getInfoReader(EqualBreaksValues.NAME);
    if (iBreaks == null)
      throw new IOException("Error adding InfoReader " + EqualBreaksValues.NAME);
    iBreaks.setNumOfClasses(this.numOfClasses);
    actualRasterFile.getGisFileInformationStorage().refreshPluginExec(iBreaks);

    this.solveDependencies(actualRasterFile);
    float steps = 100.0F / actualRasterFile.getNRows();

    if (this.workerStatus != null) {
      this.workerStatus.endPause();
      this.workerStatus.setProgressName("Calc EqualBreaks");
      this.workerStatus.startProcess();
    }

    this.breaksValues = (EqualBreaksValues) actualRasterFile.getInfoReader(EqualBreaksValues.NAME);
    if (this.breaksValues.getRangeList().size() > 254)
      throw new IOException("Maximum number of 254 breaks exceeded");
    // finde eine nicht existirende TMP Datei
    String tmpName = actualRasterFile.getAbsOutputDirectoryPath() + "tmp.tif";
    File tmpTestFile;
    for (int i = 1; i < 20; i++) {
      tmpName = actualRasterFile.getAbsOutputDirectoryPath() + "tmp" + i + ".tif";
      tmpTestFile = new File(tmpName);
      if (!(tmpTestFile.exists())) {
        break;
      }
    }

    // Erstelle neues raster File als Copy des alten mit dem neuen Pfad und
    // Namen
    RasterFileContainer tmpRasterFile = new RasterFileContainer(tmpName,
        actualRasterFile,
        actualRasterFile.getParentModule(),
        false,
        true,
        false);
    if (tmpRasterFile.getGisFile().exists()) {
      throw new IOException("Tmp file already exists");
    }
    tmpRasterFile.getGisFile().createNewFile(actualRasterFile.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Byte, 255);
    Band band = actualRasterFile.getBand(RasterFileAccess.READ_ONLY);
    Dataset tmpDataset = tmpRasterFile.getDataset(RasterFileAccess.UPDATE);

    List<ClassificationRange> rangeList = this.breaksValues.getRangeList();
    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    float readValue;

    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        if (readValue == actualRasterFile.getNoDataValue()) {
          dData[i] = 255;
        } else {
          if (readValue >= rangeList.get(0).getLowValue() && readValue <= rangeList.get(0).getHighValue()) {
            dData[i] = 1;
          } else {
            for (int k = 1; k < rangeList.size(); k++) {
              if (readValue > rangeList.get(k).getLowValue() && readValue <= rangeList.get(k).getHighValue()) {
                dData[i] = k + 1;
              }
            }
          }
          if (readValue > rangeList.get(rangeList.size() - 1).getHighValue())
            dData[i] = rangeList.size();
        }
      }
      tmpDataset.GetRasterBand(1).WriteRaster(0, j, xSize, 1, dData);
      this.processStatus += steps;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    tmpRasterFile.unlock();
    actualRasterFile.unlock();
    this.saveRasterFile(tmpRasterFile, actualRasterFile);
    this.used = true;

  }

}

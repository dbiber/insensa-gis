package org.insensa.optionfilechanger;

import org.insensa.GisFileContainer;
import org.insensa.IGisFileContainer;
import org.insensa.IRasterGisFile;
import org.insensa.RasterFileContainer;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.helpers.IOHelper;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableImpl;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.r.RMessageStream;
import org.insensa.r.RMessageStreamMonitor;
import org.insensa.r.RMessageType;
import org.insensa.r.RSession;
import org.jdom.JDOMException;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ROptionFileChanger
    extends AbstractOptionFileChanger
    implements IScriptPluginExec {

  private static final Logger log = LoggerFactory.getLogger(ROptionFileChanger.class);

  private String scriptPath;
  private Map<String, IVariable> variables = new LinkedHashMap<>();

  public ROptionFileChanger(String optionId) {
    setId(optionId);
  }

  public void setScriptPath(String path) {
    this.scriptPath = path;
  }

  @Override
  public void write() throws IOException, JDOMException {
    RSession rSession = null;
    try {
      rSession = new RSession("127.0.0.1");
      RMessageStream stream = rSession.loadOutput(RMessageType.ALL);
      RMessageStreamMonitor.getInstance().registerMessageStream(stream);

      IGisFileContainer actualRasterFile = this.getActualFileContainer();
      String tmpName = IOHelper.createTmpAbsoluteFilePath(actualRasterFile);
      IGisFileContainer tmpRasterFile = new RasterFileContainer(tmpName,
          (GisFileContainer) actualRasterFile,
          actualRasterFile.getParentModule(),
          true,
          true,
          false);

      rSession.assignInsensaIsActive();
      rSession.assignInsensaWs(actualRasterFile);
      rSession.assignOutputFile(tmpName);
      rSession.assignInsensaCurrentFile(actualRasterFile.getAbsolutePath());
//      rSession.getRawConnection().assign("actualRasterFile", actualRasterFile.getAbsolutePath());
//      rSession.getRawConnection().assign("outputFile", tmpName);

      for (IVariable var : variables.values()) {
        rSession.assign(var);
      }
      rSession.sourceFile(new File(scriptPath));
      actualRasterFile.getGisFile().unlock();
      rSession.unloadOuput();

      this.saveRasterFile(tmpRasterFile, actualRasterFile);
      used = true;
    } catch (REngineException | REXPMismatchException e) {
      e.printStackTrace();
    } finally {
      if (rSession != null) {
        try {
          rSession.closeAndClear();
        } catch (REXPMismatchException | REngineException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void save(ISavableSaver saver) {
    saver.save("used", used);
    saver.save("orderId", orderId);
//    if (variables != null) {
    variables.forEach((s, variable) -> saver.save(variable));
//    }
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    used = restorer.loadBoolean("used").get();
    orderId = restorer.loadInteger("orderId").get();
//    if (variables == null) {
//      variables = new LinkedHashMap<>();
//    }

    List<IRestorableData> data = restorer
        .loadCollection("variable", VariableImpl.class);
    data.forEach(iRestorableData ->
        variables.put(((IVariable) iRestorableData).getName(),
            (IVariable) iRestorableData));
  }

  @Override
  public Map<String, IVariable> getVariables() {
    return variables;
  }

  @Override
  public void setVariables(Map<String, IVariable> configuration) {
    this.variables = configuration;
  }
}

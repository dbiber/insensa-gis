/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.optionfilechanger;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdalconst.gdalconstConstants;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.helpers.IOHelper;
import org.insensa.inforeader.MinMaxValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class ReplaceRasterFileValue extends AbstractRasterOptionFileChanger {

  public static final String REPLACE_VALUE = "ReplaceValue";
  private String description = "Replace a defined Type of values with a new value";
  private String fromValue = "";
  private String toValue = "";
  private String replaceValue = "";
  private Float fFromValue = null;
  private Float fToValue = null;
  private Float fReplaceValue = 0.0f;

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  private float getFloatValue(String value, float noDataValue, float minValue, float maxValue) {
    String tmpValue = value;
    if (tmpValue.contains(",")) {
      tmpValue = tmpValue.replace(",", ".");
    }
    if (tmpValue.equals("MIN")) {
      return minValue;
    }
    if (tmpValue.equals("MAX")) {
      return maxValue;
    }
    if (tmpValue.equals("NODATA")) {
      return noDataValue;
    } else {
      try {
        return Float.valueOf(value);
      } catch (NumberFormatException e) {
        return Float.NaN;
      }
    }
  }

  public String getFromValue() {
    return this.fromValue;
  }

  public void setFromValue(String fromValue) {
    this.fromValue = fromValue;
  }

  @Override
  public List<String> getInfoDependencies() {
    List<String> retArray = new ArrayList<String>();
    retArray.add(MinMaxValues.NAME);
    return retArray;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("from", fromValue);
    saver.save("to", toValue);
    saver.save("replaceValue", replaceValue);
  }

  public String getReplaceValue() {
    return this.replaceValue;
  }

  public void setReplaceValue(String replaceValue) {
    this.replaceValue = replaceValue;
  }

  public String getToValue() {
    return this.toValue;
  }

  public void setToValue(String toValue) {
    this.toValue = toValue;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);

    fromValue = restorer.loadString("from").orElse("");
    toValue = restorer.loadString("to").orElse("");
    replaceValue = restorer.loadString("replaceValue").orElse("");
  }

  //  @Override
//  public void setOptionAttributes(Element eOption) throws IOException {
//
//    super.setOptionAttributes(eOption);
//
//    if (this.used == true) {
//
//    } else {
//      String fromValue = eOption.getAttributeValue("from");
//      String toValue = eOption.getAttributeValue("to");
//      String replaceValue = eOption.getAttributeValue("replaceValue");
//      if (fromValue == null || toValue == null || replaceValue == null) {
//
//      } else if (fromValue.isEmpty() || toValue.isEmpty() || replaceValue.isEmpty()) {
//
//      } else {
//        this.setFromValue(fromValue);
//        this.setToValue(toValue);
//        this.setReplaceValue(replaceValue);
//      }
//    }
//
//  }

  @Override
  public String toString() {
    return getId();
  }

  public void write() throws IOException, JDOMException, GisFileException {
    RasterFileContainer actualRasterFile = (RasterFileContainer) this.getActualFileContainer();
    float steps = 100.0F / actualRasterFile.getNRows();

    if (this.fromValue.equals("MIN") || this.fromValue.equals("MAX") || this.toValue.equals("MIN") || this.toValue.equals("MAX")
        || this.replaceValue.equals("MIN") || this.replaceValue.equals("MAX")) {
      this.solveDependencies(actualRasterFile);
      MinMaxValues minMaxValues = (MinMaxValues) actualRasterFile.getInfoReader(MinMaxValues.NAME);
      this.fFromValue = this.getFloatValue(this.fromValue, actualRasterFile.getNoDataValue(), minMaxValues.getMinValue(),
          minMaxValues.getMaxValue());
      this.fToValue = this.getFloatValue(this.toValue, actualRasterFile.getNoDataValue(), minMaxValues.getMinValue(),
          minMaxValues.getMaxValue());
      this.fReplaceValue = this.getFloatValue(this.replaceValue, actualRasterFile.getNoDataValue(), minMaxValues.getMinValue(),
          minMaxValues.getMaxValue());
    } else {
      this.fFromValue = this.getFloatValue(this.fromValue, actualRasterFile.getNoDataValue(), Float.MIN_VALUE, Float.MAX_VALUE);
      this.fToValue = this.getFloatValue(this.toValue, actualRasterFile.getNoDataValue(), Float.MIN_VALUE, Float.MAX_VALUE);
      this.fReplaceValue = this.getFloatValue(this.replaceValue, actualRasterFile.getNoDataValue(), Float.MIN_VALUE, Float.MAX_VALUE);
    }
    if (Float.isNaN(this.fFromValue) || Float.isNaN(this.fToValue) || Float.isNaN(this.fReplaceValue)) {
      throw new IOException("One or more values are wrong");
    }

    if (this.workerStatus != null) {
      this.workerStatus.endPause();
      this.workerStatus.setProgressName("ReplaceValue");
      this.workerStatus.startProcess();
    }

    String tmpName = IOHelper.createTmpAbsoluteFilePath(actualRasterFile);

    // Erstelle neues raster File als Copy des alten mit dem neuen Pfad und Namen
    RasterFileContainer tmpRasterFile = new RasterFileContainer(tmpName, actualRasterFile,
        actualRasterFile.getParentModule(), true, true, false);
    if (tmpRasterFile.getGisFile().exists()) {
      throw new IOException("Tmp file already exists");
    }
    int dataType = actualRasterFile.getBand(RasterFileAccess.READ_ONLY).getDataType();
    if (this.fReplaceValue != actualRasterFile.getNoDataValue()) {
      try {
        BigDecimal bd = new BigDecimal(this.replaceValue);
        if (!DataTypeHelper.fitsInDataType(dataType, bd)) {
          int tmpDataType = DataTypeHelper.getBestDatatype(bd);
          if (tmpDataType != gdalconstConstants.GDT_Unknown) {
            dataType = tmpDataType;
          }
        }
      } catch (NumberFormatException e) {
        throw new IOException(e);
      }
    }

    tmpRasterFile.getGisFile().createNewFile(actualRasterFile.getGisFile(),
        DataTypeHelper.RasterDataType.valueOf(DataTypeHelper.getDataTypeInternalName(dataType)),
        actualRasterFile.getNoDataValue());
    Band band = actualRasterFile.getBand(RasterFileAccess.READ_ONLY);
    Dataset tmpDataset = tmpRasterFile.getDataset(RasterFileAccess.UPDATE);

    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    float readValue;

    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        if (readValue == actualRasterFile.getNoDataValue()
            && (actualRasterFile.getNoDataValue() != this.fFromValue || actualRasterFile.getNoDataValue() != this.fToValue)) {
        } else {
          if (readValue >= this.fFromValue.floatValue() && readValue <= this.fToValue.floatValue())
            dData[i] = this.fReplaceValue.floatValue();
        }
      }

      tmpDataset.GetRasterBand(1).WriteRaster(0, j, xSize, 1, dData);
      this.processStatus += steps;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }
    tmpRasterFile.unlock();
    actualRasterFile.unlock();
    this.saveRasterFile(tmpRasterFile, actualRasterFile);
    this.used = true;
  }

}

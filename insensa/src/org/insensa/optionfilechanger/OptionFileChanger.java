/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.optionfilechanger;

import java.io.IOException;

import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.RasterFileContainer;
import org.insensa.extensions.IPluginExec;
import org.jdom.JDOMException;


/**
 * It is strongly recommended that you do not implement the Interface
 * OptionFileChanger directly. Use the abstract class
 * {@link AbstractRasterOptionFileChanger} instead.
 *
 * @author dennis
 */
public interface OptionFileChanger
    extends IPluginExec {

//  /**
//   * Here you can check if it is correct to
//   * add this OptionFileChanger to this
//   * rasterFile.
//   */
//  boolean checkApproval(IGisFileContainer rasterFile);

  IGisFileContainer getActualFileContainer();

//  AttributeTable getData();

//  /**
//   * @return An error message if there is one
//   */
//  String getErrorMessage();

//  int getOrderId();

//  /**
//   * @param orderId a unique orderId for this option
//   * @deprecated should not be used since version 2.0.26
//   */
//  void setOrderId(int orderId);

//  List<String> getInfoDependencies();

//  /**
//   * Creates a new {@link org.jdom.Element} object that contains all
//   * Information that should be saved in a configuration file.<br>
//   * <br>
//   * The configuration file is a xml file that will be saved in the "infos"
//   * directory. <u>jdom</u> is used to generate the file. An Element will be
//   * represented as a XML Tag in the created file<br>
//   * <br>
//   * First you have to create a new Element with the name of the
//   * optionFileChanger.<br>
//   * This name must be the same name as you defined in the
//   * <code>extensions.xml</code> file<br>
//   * If you inherited and defined the method {@link getOptionName} correctly,
//   * you can use this function to get the name.<br>
//   * <br>
//   * <b>Example:</b><br>
//   *
//   * <pre>
//   * <code>Element optionElement = new Element(this.getOptionName());</code>
//   * </pre>
//   *
//   * There are TWO attributes which should ALWAYS be saved, the status of the
//   * execution (Therefore the same boolean expression that should be used in
//   * the method {@link isUsed}) and the orderId of the current optionFileChanger.<br>
//   * Both variables (<u>used</u> and <u>orderId</u>) are defined in
//   *
//   * @return {@link org.jdom.Element} with all contents that should be saved
//   * in the Configuration File {@link AbstractRasterOptionFileChanger}.<br>
//   * The Integer variable <u>orderId</u> is used internally and just have
//   * to be saved and reloaded by the plugin. <br>
//   * <br>
//   * <b>Example:</b> Assuming that <code><u>used</u></code> is the
//   * boolean expression<br>
//   *
//   * <pre>
//   * <code>optionElement.setAttribute("used",Boolean.toString(used));</code>
//   * </pre>
//   *
//   * Besides the used state, you can save what ever you want and it is
//   * possible to use the complex methods that <u>jdom</u> gives you.
//   * For example: creating subElements (<it>child XML tags<it>) <br>
//   * <br>
//   * <b>Note:</b> It is strongly recommended that you define a final
//   * variable for every <u>key</u> you need.<br>
//   * The keys used here are also required in the method
//   * {@link org.insensa.optionfilechanger.OptionFileChanger.setInfos} and
//   * it can be fatal to use wrong keys <br>
//   *
//   * <pre>
//   * <code>private final string XML_ATTR_USED = "used";</code>
//   * </pre>
//   */
//  @Deprecated
//  Element getOptionElement() throws IllegalDataException;

//  /**
//   * Returns the orderId of the OptionFileChanger. <br>
//   * <br>
//   * This MUST be the same orderId as defined in the <code>extensions.xml</code>
//   * or in {@link org.insensa.optionfilechanger.EOption}
//   *
//   * @return the name of the OptionFileChanger
//   */
//  String getOptionId();

//  /**
//   * @return maximum number of unused objects that can be appended to a file
//   * @deprecated should not be used since version 2.0.26
//   */
//  @Deprecated
//  int getUsableCount();

//  /**
//   * @return true if the option should be settable for the user .
//   * @deprecated should not be used since version 2.0.26
//   */
//  boolean isPublic();

//  /**
//   * @return true if the option has executed successful.
//   */
//  boolean isUsed();

  /**
   * @param oldRasterFile The RasterFileContainer that will use this optionFileChanger
   */
  void setOldGisFile(IGisFileContainer oldRasterFile);

//  /**
//   * Sets all attributes that are saved in the configuration file.<br>
//   * <br>
//   * This method is the counterpart of {@link getOptionElement}.<br>
//   *
//   * @param eOption the {@link jdom.Element} which holds the attributes
//   * @throws IOException {@code eOption} holds all information that was saved.<br>
//   *                     The information that should ALWAYS stored in the
//   *                     configuration is the status of the execution and the orderId of
//   *                     the optionFileChanger<br>
//   *                     <b>Example:</b>
//   *
//   *                     <pre>
//   *                     <code>
//   *                     Attribute attrUsed = element.getAttribute(XML_ATTR_USED);
//   *                     Attribute attrID = element.getAttribute(XML_ATTR_ORDER_ID);
//   *                     if(attrUsed==null || attrID==null)
//   *                     throw new IOException("one ore more necessary attributes cannot be found");
//   *                     try
//   *                     {
//   *                     used=attrUsed.getBooleanValue();
//   *                     orderId=attrID.getIntegerValue();
//   *                     }catch (DataConversionException e)
//   *                     {
//   *                     throw new IOException(e);
//   *                     }
//   *                     </code>
//   *                     </pre>
//   */
//  @Deprecated
//  void setOptionAttributes(Element eOption) throws IOException;

  /**
   * @return the relative File Path (with name) of the source RasterFile
   */
  String getOldFileRelativeFilePath();

//  /**
//   * @param workerStatus the {@link WorkerStatus} that should Listen to the execution
//   *                     progress
//   */
//  void setWorkerStatus(WorkerStatus workerStatus);

  /**
   * This method will be executed from the ThreadPool <br>
   * There are several steps that must be followed:<br>
   * <ol>
   * <li>Get actual file<br>
   * .
   *
   * {@link AbstractRasterOptionFileChanger#getActualFileContainer()}</li> <li>
   * Solve dependencies <br>
   * {@link AbstractRasterOptionFileChanger#solveDependencies(RasterFileContainer)}
   * </li> <li>Start process</li> <li>Create temporary file of
   * type RasterFileContainer</li> <li>Save the file under the
   * correct filename</li> <li>End process</li>
   * </ol>
   * <br>
   * <br>
   * <b>Example:</b><br>
   * In this Example, we want to create a file as a copy of the
   * old with every value that is &lt;= 0.0 replace by a variable
   * float value 0.5.<br>
   * <br>
   * First we have to <u>get the actual raster file</u>. This
   * could be the previously defined <u>oldRasterFile</u> but also
   * an other file that just lies in the oldRasterFile defined as
   * targetFile.
   *
   * <pre>
   * <code>RasterFileContainer actualRasterFile = getActualRasterFile()</code>
   * </pre>
   *
   * In the next step it is important to be sure that all
   * dependencies previously defined in
   * {@link getInfoDependencies} are available. the easiest way is
   * to use the method
   * {@link AbstractRasterOptionFileChanger#solveDependencies(RasterFileContainer)}
   *
   * <pre>
   * <code>solveDependencies(actualRasterFile);</code>
   * </pre>
   *
   * Lets assume we need the InfoReader <u>minMaxValues</u>.
   *
   * <pre>
   * <code>MinMaxValues minMaxValues = (MinMaxValues)actualRasterFile.getInfoReader(MinMaxValues.NAME);
   * if(minMaxValues==null)
   * throw new IOException("Can't solve minMaxValues")</code>
   * </pre>
   *
   * If a WorkerStatus was defined we can tell it that the
   * progress began.
   *
   * <pre>
   * <code>if(workerStatus!=null)
   * {
   * workerStatus.setProgressName("Starting replace zero");
   * workerStatus.startProcess();
   * }</code>
   * </pre>
   *
   * Next we need to create a new temporary file as a copy of the
   * old file with a new name
   *
   * <pre>
   * <code>
   * 	String tmpName = getTemporaryFileName(rasterFile);
   * 	RasterFileContainer tmpRasterFile = new RasterFileContainer(tmpName,rasterFile,rasterFile.getParentModule(),false,true,false);
   * 	int dataType = rasterFile.getBand(RasterFileAccess.READ_ONLY).getDataType();
   * 	tmpRasterFile.createNewFile(rasterFile, dataType, rasterFile.getNoDataValue());
   *
   * </code>
   * see {@link RasterFileContainer#CRasterFileInformation(String, RasterFileContainer, boolean, boolean, boolean)}
   * and
   * {@link RasterFileContainer#createNewFile(org.insensa.RasterFile, int, double)}
   * for more information
   * </pre>
   *
   * Now we can get Excess to the gdal functions to create the
   * data.<br>
   * <br>
   * <b>Create and Write Data:</b>
   *
   * <pre>
   * <code>
   * 	Band band = rasterFile.getBand(RasterFileAccess.READ_ONLY);
   * 	Dataset tmpDataset = tmpRasterFile.getDataset(RasterFileAccess.UPDATE);
   * 	int xSize = band.GetXSize();
   * 	int ySize = band.getYSize();
   * 	float steps = 100.0F / ySize;
   * 	float[] fData = new float[xSize];
   * 	float readData;
   * 	for (int i = 0; i < ySize; i++)
   *  {
   * 		band.ReadRaster(0, i, xSize, 1, fData);
   * 		for (int j = 0; j < fData.length; j++)
   *    {
   * 			readData = fData[j];
   * 			if (readData == rasterFile.getNoDataValue())
   *      {
   *      } else
   *      {
   * 				if (readData <= 0.0f)
   * 					fData[j] = 0.5f;
   *      }
   *    }
   * 		processStatus += steps;
   * 		if (workerStatus != null)
   * 	 		workerStatus.refreshPercentage(processStatus);
   * 	 	tmpDataset.GetRasterBand(1).WriteRaster(0, i, xSize, 1, fData);
   *  }
   * 	tmpDataset.FlushCache();
   * 	band.FlushCache();
   * 	tmpDataset = null;
   * 	band = null;
   * </code>
   * </pre>
   *
   * At last it is necessary to save the temporary file and set
   * the execution status:<br>
   *
   * <pre>
   * <code>saveRasterFile(tmpRasterFile, actualRasterFile);
   * used=true;</code>
   * </pre>
   */
  void write() throws IOException, JDOMException, GisFileException;

//  Map<String, IVariable> getVariables();
//
//  void setVariables(Map<String, IVariable> configuration);
}

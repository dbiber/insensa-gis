/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.optionfilechanger;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.ClassificationRange;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.helpers.IOHelper;
import org.insensa.inforeader.VariableBreaksValues;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class VariableBreaks extends AbstractRasterOptionFileChanger {

  public static final String VARIABLE_BREAKS = "variableBreaks";
  // private VariableBreaksValues breaksValues = null;
  private String description = "Sort values in Variable Breaks";
  private List<ClassificationRange> rangeList = new ArrayList<>();

  /**
   * @see AbstractRasterOptionFileChanger#getInfoDependencies()
   */
  @Override
  public List<String> getInfoDependencies() {
    List<String> retList = new ArrayList<String>();
    retList.add(VariableBreaksValues.NAME);
    return retList;
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    this.rangeList.forEach(classificationRange ->
        saver.save("Class", classificationRange));
//    saver.save("Classification", saver1 -> {
//      for (ClassificationRange range : rangeList) {
//        saver1.save(range);
//      }
//    });
  }


  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    List<IRestorableData> data = restorer.loadCollection("Class",
        ClassificationRange.class);

    this.rangeList = new ArrayList<>();

    data.forEach(iRestorableData ->
        rangeList.add((ClassificationRange) iRestorableData));
  }

  //  @Override
//  public void setOptionAttributes(Element eOption) throws IOException {
//    super.setOptionAttributes(eOption);
//
//    try {
//      List<Element> elementList = eOption.getChildren("Class");
//      ClassificationRange newRange;
//      float minimum;
//      float maximum;
//      for (Element iElement : elementList) {
//        minimum = iElement.getAttribute("minimum").getFloatValue();
//        maximum = iElement.getAttribute("maximum").getFloatValue();
//        newRange = new ClassificationRange(minimum, maximum);
//        this.rangeList.add(newRange);
//      }
//
//    } catch (DataConversionException e) {
//      throw new IOException("VariableBreaks: setInfo: Wrong Input Data");
//    }
//  }

  //  @Override
//  public Element getOptionElement() {
//    Element eOption = super.getOptionElement();
//    Element eRangeValue;
//    List<Element> eRangeValueList = new ArrayList<Element>();
//    for (int i = 0; i < this.rangeList.size(); i++) {
//      eRangeValue = new Element("Class");
//      eRangeValue.setAttribute("value", Integer.toString(i + 1));
//      eRangeValue.setAttribute("minimum", Float.toString(this.rangeList.get(i).getLowValue()));
//      eRangeValue.setAttribute("maximum", Float.toString(this.rangeList.get(i).getHighValue()));
//      eRangeValueList.add(eRangeValue);
//    }
//    for (int i = 0; i < eRangeValueList.size(); i++) {
//      eOption.addContent(eRangeValueList.get(i));
//    }
//
//    Element eOptionDescription = new Element("description");
//    eOptionDescription.setText(this.description);
//    eOption.addContent(eOptionDescription);
//
//    return eOption;
//  }

  public List<ClassificationRange> getRangeList() {
    return this.rangeList;
  }

  public void setRangeList(List<ClassificationRange> rangeList) {
    this.rangeList.clear();
    this.rangeList.addAll(rangeList);

  }


  @Override
  public void write() throws IOException, JDOMException, GisFileException {
    RasterFileContainer actualRasterFile = (RasterFileContainer) this.getActualFileContainer();
    this.solveDependencies(actualRasterFile);
    float steps = 100.0F / actualRasterFile.getNRows();

    if (this.workerStatus != null) {
      this.workerStatus.endPause();
      this.workerStatus.setProgressName("Create VariableBreaks");
      this.workerStatus.startProcess();
    }

    if (this.rangeList.size() > 254)
      throw new IOException("Maximum number of 254 breaks exceeded");
    if (this.rangeList.isEmpty())
      throw new IOException("No range list found");

    String tmpName = IOHelper.createTmpAbsoluteFilePath(actualRasterFile);

    // Erstelle neues raster File als Copy des alten mit dem neuen Pfad und
    // Namen
    RasterFileContainer tmpRasterFile = new RasterFileContainer(tmpName, actualRasterFile, actualRasterFile.getParentModule(), false, true, false);
    if (tmpRasterFile.getGisFile().exists()) {
      throw new IOException("Tmp file already exists");
    }
    tmpRasterFile.getGisFile().createNewFile(actualRasterFile.getGisFile(),
        DataTypeHelper.RasterDataType.GDT_Byte, 255);
    Band band = actualRasterFile.getBand(RasterFileAccess.READ_ONLY);
    Dataset tmpDataset = tmpRasterFile.getDataset(RasterFileAccess.UPDATE);

    // List<ClassificationRange> rangeList = breaksValues.getRangeList();
    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    float readValue;

    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        if (readValue == actualRasterFile.getNoDataValue()) {
          dData[i] = 255;
        } else {
          if (readValue >= this.rangeList.get(0).getLowValue() && readValue <= this.rangeList.get(0).getHighValue()) {
            dData[i] = 1;
          } else {
            for (int k = 1; k < this.rangeList.size(); k++) {
              if (readValue > this.rangeList.get(k).getLowValue() && readValue <= this.rangeList.get(k).getHighValue()) {
                dData[i] = k + 1;
              }
            }
          }
        }
      }
      tmpDataset.GetRasterBand(1).WriteRaster(0, j, xSize, 1, dData);
      this.processStatus += steps;
      if (this.workerStatus != null)
        this.workerStatus.refreshPercentage(this.processStatus);
    }

    tmpRasterFile.unlock();
    actualRasterFile.unlock();
    this.saveRasterFile(tmpRasterFile, actualRasterFile);
    this.used = true;

  }

}

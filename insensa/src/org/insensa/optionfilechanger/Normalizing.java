/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.optionfilechanger;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.DataTypeHelper;
import org.insensa.helpers.IOHelper;
import org.insensa.inforeader.MinMaxValues;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * This is a normalization
 *
 * @author dennis
 */
public class Normalizing extends AbstractRasterOptionFileChanger {
  public static final String NAME = "Normalizing";
  private String description = "Simple normalizing function, so the new " + "values are between 0 and 1";
  private int range = 100;
  private boolean inverse = false;

  @Override
  public List<String> getInfoDependencies() {
    List<String> retArray = new ArrayList<String>();
    retArray.add(MinMaxValues.NAME);
    return retArray;
  }

  public RasterFileContainer getOldRasterFile() {
    return (RasterFileContainer) this.oldRasterFile;
  }

  @Override
  public void restore(ISavableRestorer restorer) {
    super.restore(restorer);
    range = restorer.loadInteger("range").orElse(0);
    inverse = restorer.loadBoolean("inverse").orElse(false);
    description = restorer.loadString("description").orElse("");
  }

  @Override
  public void save(ISavableSaver saver) {
    super.save(saver);
    saver.save("range", range);
    saver.save("inverse", inverse);
    saver.save("description", description);
  }

  public int getRange() {
    return this.range;
  }

  public void setRange(int range) {
    this.range = range;
  }

  public boolean isInverse() {
    return this.inverse;
  }

  public void setInverse(boolean inverse) {
    this.inverse = inverse;
  }

  public void write() throws IOException, JDOMException {
    RasterFileContainer actualRasterFile = (RasterFileContainer) this.getActualFileContainer();
    float steps = 100.0F / actualRasterFile.getNRows();

    this.solveDependencies(actualRasterFile);

    if (this.workerStatus != null) {
      this.workerStatus.endPause();
      this.workerStatus.setProgressName("Normalizing");
      this.workerStatus.startProcess();
    }

    MinMaxValues minMaxValues = (MinMaxValues) actualRasterFile.getInfoReader(MinMaxValues.NAME);
    float minimum = minMaxValues.getMinValue();
    float maximum = minMaxValues.getMaxValue();

    String tmpName = IOHelper.createTmpAbsoluteFilePath(actualRasterFile);

    // Erstelle neues raster File als Copy des alten mit dem neuen Pfad und Namen
    RasterFileContainer tmpRasterFileContainer = new RasterFileContainer(tmpName,
        actualRasterFile, actualRasterFile.getParentModule(),
        false, true, false);
    if (tmpRasterFileContainer.getGisFile().exists()) {
      throw new IOException("Tmp file already exists");
    }
    if (this.range == 1000 || this.range == 10000) {
      tmpRasterFileContainer.getGisFile().createNewFile(actualRasterFile.getGisFile(),
          DataTypeHelper.RasterDataType.GDT_UInt16, Short.MAX_VALUE);
    } else if (this.range == 100) {
      tmpRasterFileContainer.getGisFile().createNewFile(actualRasterFile.getGisFile(),
          DataTypeHelper.RasterDataType.GDT_Byte, 255);
    }
    Band band = actualRasterFile.getBand(RasterFileAccess.READ_ONLY);
    Dataset tmpDataset = tmpRasterFileContainer.getDataset(RasterFileAccess.UPDATE);

    int xSize = band.getXSize();
    int ySize = band.getYSize();
    float[] dData = new float[xSize];
    float readValue;

    for (int j = 0; j < ySize; j++) {
      band.ReadRaster(0, j, xSize, 1, dData);
      for (int i = 0; i < dData.length; i++) {
        readValue = dData[i];
        if (readValue == actualRasterFile.getNoDataValue()) {
          if (this.range == 1000 || this.range == 10000)
            dData[i] = Short.MAX_VALUE;
          else if (this.range == 100)
            dData[i] = 255;
        } else {
          readValue = ((readValue - minimum) / (maximum - minimum));
          if (this.inverse) {
            readValue = 1.0F - readValue;
          }
          readValue *= this.range;
          readValue = Math.round(readValue);
          dData[i] = readValue;
        }
      }
      tmpDataset.GetRasterBand(1).WriteRaster(0, j, xSize, 1, dData);
      this.processStatus += steps;
      if (this.workerStatus != null) {
        this.workerStatus.refreshPercentage(this.processStatus);
      }
    }
    this.saveRasterFile(tmpRasterFileContainer, actualRasterFile);
    this.used = true;
  }
}

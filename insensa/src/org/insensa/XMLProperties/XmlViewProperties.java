/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.XMLProperties;


import org.insensa.exceptions.WriteConfigException;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class XmlViewProperties {
  private static final Logger log = LoggerFactory.getLogger(XmlViewProperties.class);


  private String filename;
  private Document doc;
  private Element rootDoc;
  private XMLOutputter xmlOut;
  private File xmlFile;

  public XmlViewProperties(String path) throws WriteConfigException {
    if (path.endsWith(File.separator))
      this.filename = path + "ViewProperties.xml";
    else
      this.filename = path + File.separator + "ViewProperties.xml";
    this.xmlFile = new File(this.filename);
    if (!this.xmlFile.exists()) {
      try {
        this.xmlFile.createNewFile();
      } catch (IOException e) {
        log.error( "UNKNOWN", e);
      }
      this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
      this.rootDoc = new Element("org.insensa.view");
      this.doc = new Document(this.rootDoc);
    } else {
      try {
        this.doc = new SAXBuilder().build(this.xmlFile);
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = this.doc.getRootElement();
      } catch (JDOMException e) {
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = new Element("org.insensa.view");
        this.doc = new Document(this.rootDoc);
        log.error("Fehler in XmlViewProperties(): " + e.getMessage());
      } catch (IOException e1) {
        log.error( "", e1);
      }
    }
    this.writeConfig();
  }

  public void addExpandList(List<Integer> iList) throws WriteConfigException {
    Element treeElem = this.rootDoc.getChild("tree");
    if (treeElem == null) {
      treeElem = new Element("tree");
      this.rootDoc.addContent(treeElem);
    }
    Element expandElem = treeElem.getChild("expandList");
    if (expandElem == null) {
      expandElem = new Element("expandList");
      treeElem.addContent(expandElem);
    } else {
      expandElem.removeContent();
    }
    for (Integer iInt : iList) {
      Element iElem = new Element("int");
      iElem.setAttribute("value", iInt.toString());
      expandElem.addContent(iElem);
    }
    this.writeConfig();
  }

  public void close() {
  }

  public boolean delete() {
    if (this.xmlFile.exists()) {
      return this.xmlFile.delete();
    }
    return false;
  }

  public List<Integer> getExpandList() {
    List<Integer> iList = new ArrayList<>();
    Element treeElem = this.rootDoc.getChild("tree");
    if (treeElem == null)
      return iList;
    Element expandElem = treeElem.getChild("expandList");
    if (expandElem == null)
      return iList;
    List<Element> eiList = expandElem.getChildren();
    for (Element iterElem : eiList) {
      String value = iterElem.getAttributeValue("value");
      if (value != null)
        iList.add(Integer.valueOf(value));
    }
    return iList;

  }

  public String getLastImportedFilePath() {
    Element importFilePath = this.rootDoc.getChild("ImportedFilePath");
    if (importFilePath == null)
      return "";
    Attribute attr = importFilePath.getAttribute("path");
    if (attr == null)
      return "";
    return attr.getValue();
  }

  public void setLastImportedFilePath(String path) throws WriteConfigException {
    Element importFilePath = this.rootDoc.getChild("ImportedFilePath");
    if (importFilePath == null) {
      importFilePath = new Element("ImportedFilePath");
      this.rootDoc.addContent(importFilePath);
    }
    importFilePath.setAttribute("path", path);
    this.writeConfig();
  }

  /**
   * @return path of the last imported folder or an empty String if this there is no property
   * available
   */
  public String getLastImportedFolderPath() {
    Element importFilePath = this.rootDoc.getChild("ImportedFolderPath");
    if (importFilePath == null)
      return "";
    Attribute attr = importFilePath.getAttribute("path");
    if (attr == null)
      return "";
    return attr.getValue();
  }

  public void setLastImportedFolderPath(String path) {
    log.debug("setLastImportedFolderPath:" + path);
    Element importFilePath = this.rootDoc.getChild("ImportedFolderPath");
    if (importFilePath == null) {
      importFilePath = new Element("ImportedFolderPath");
      this.rootDoc.addContent(importFilePath);
    }
    importFilePath.setAttribute("path", path);

    try {
      this.writeConfig();
    } catch (WriteConfigException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * @throws WriteConfigException If the config could not be written
   */
  private void writeConfig() throws WriteConfigException {
    if (xmlFile.exists()) {
      if (!this.xmlFile.delete()) {
        throw new WriteConfigException("Error deleting file \"" + xmlFile.getAbsolutePath() + "\"");
      }
    }
    this.xmlFile = new File(this.filename);
    try {
      if (!this.xmlFile.createNewFile()) {
        throw new WriteConfigException("File \"" + xmlFile.getAbsolutePath() + "\" already exists");
      }
    } catch (IOException e) {
      throw new WriteConfigException(e);
    }
    try (FileOutputStream streamOut = new FileOutputStream(this.xmlFile)) {
      this.xmlOut.output(this.doc, streamOut);
    } catch (IOException e) {
      throw new WriteConfigException(e);
    }
  }
}

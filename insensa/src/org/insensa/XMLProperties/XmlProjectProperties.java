/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.XMLProperties;

import org.insensa.GenericGisFileSet;
import org.insensa.IProjectListener;
import org.insensa.RasterFileContainer;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class XmlProjectProperties implements IProjectListener {
  public static final String C_OUTPUT_FILE_NAME = "outputFileName";
  public static final String C_OUTPUT_FILE_PATH = "outputFilePath";
  public static final String C_USED = "used";
  private static final Logger log = LoggerFactory.getLogger(XmlProjectProperties.class);
  private String filename;
  private Document doc;
  private Element rootDoc;
  private XMLOutputter xmlOut;
  private FileOutputStream streamOut;
  private File xmlFile;
  private String separator;

  public XmlProjectProperties(String path) throws IOException {
    this.separator = System.getProperties().getProperty("file.separator");
    if (path.endsWith("settings.xml")) {
      this.filename = path;
    } else {
      if (path.endsWith(this.separator))
        this.filename = path + "settings.xml";
      else
        this.filename = path + this.separator + "settings.xml";
    }
    this.xmlFile = new File(this.filename);
    if (this.xmlFile.exists() == false) {
      try {
        this.xmlFile.createNewFile();
      } catch (IOException e) {
        throw new IOException(e);
      }
      this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
      this.rootDoc = new Element("project");
      this.doc = new Document(this.rootDoc);
    } else {
      try {
        this.doc = new SAXBuilder().build(this.xmlFile);
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = this.doc.getRootElement();

      } catch (JDOMException e) {
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = new Element("project");
        this.doc = new Document(this.rootDoc);
        log.error("Fehler in XmlProjectProperties(): " + e.getMessage());
      } catch (IOException e1) {
        throw new IOException(e1);
      }
    }
  }

  public Element addFileSet(String setName) throws IOException {
    Element fileSet = new Element("FileSet");
    // fileSet.setAttribute("path", projectPath);
    fileSet.setAttribute("name", setName);
    this.rootDoc.addContent(fileSet);
    this.writeConfig();
    return fileSet;
  }

  public Element addRasterFileList(List<RasterFileContainer> rasterFileList, String fileSet) throws JDOMException, IOException {
    Element eFileSet = this.getRasterFileSet(fileSet);
    if (eFileSet == null)
      throw new JDOMException("Fehler in addRasterFileList: FileSet ist null");
    for (RasterFileContainer file : rasterFileList) {
      Element eFile = this.createFileElement(file);
      eFileSet.addContent(eFile);
    }
    this.writeConfig();
    return null;
  }

  public Element addRasterFileToFileSet(RasterFileContainer rasterFile, String fileSet) throws IOException {
    Element eFileSet = this.getRasterFileSet(fileSet);
    Element eFile = this.createFileElement(rasterFile);
    eFileSet.addContent(eFile);
    this.writeConfig();
    return eFile;
  }

  public void changeFileSetName(GenericGisFileSet fileSet, String newName) throws IOException {
    List<Element> eList = this.rootDoc.getChildren("FileSet");
    for (Element eFileSet : eList) {
      if (fileSet.getName().equals(eFileSet.getAttributeValue("name")))
        eFileSet.setAttribute("name", newName);
    }
    this.writeConfig();
  }

  public void changeFileSetName(String oldName, String newName) throws IOException {
    List<Element> eList = this.rootDoc.getChildren("FileSet");
    for (Element eFileSet : eList) {
      if (oldName.equals(eFileSet.getAttributeValue("name")))
        eFileSet.setAttribute("name", newName);
    }
    this.writeConfig();
  }

  @Override
  public void childFileSetRenamed(GenericGisFileSet fileSet, String oldName, String newName) throws IOException {
    log.debug("XmlProjectProperties: changeFileSetName");
    this.changeFileSetName(oldName, newName);
  }

  private Element createFileElement(RasterFileContainer file) {
    Element eFile = new Element("File");
    eFile.setAttribute("name", file.getName());
    eFile.setAttribute("path", file.getGisFile().getParent() + this.separator);
    Element eFileOutputPath = new Element("outputPath");
    eFileOutputPath.addContent(file.getAbsOutputDirectoryPath());
    Element eFileOutputName = new Element("outputName");
    eFileOutputName.addContent(file.getOutputFileName());

    eFile.addContent(eFileOutputPath);
    eFile.addContent(eFileOutputName);

    return eFile;
  }

  @Override
  public void fileSetRenamed(GenericGisFileSet fileSet, String oldName, String newName) {
  }

  public Element getRasterFileInFileSet(String fileSetName, String fileName) {
    Element eFileSet = this.getRasterFileSet(fileSetName);
    for (Object elem : eFileSet.getChildren("File")) {
      if (((Element) elem).getAttributeValue("name").compareTo(fileName) == 0) {
        return (Element) elem;
      }
    }
    return null;
  }

  public Element getRasterFileSet(String name) {
    for (Object elem : this.rootDoc.getChildren("FileSet")) {
      if (((Element) elem).getAttributeValue("name") == name) {
        return (Element) elem;
      }
    }
    return null;
  }

  public List<?> getRasterFileSets() {
    return this.rootDoc.getChildren("FileSet");
  }

  public String getVersion() {
    return this.rootDoc.getAttributeValue("version");
  }

  public void setVersion(String version) throws IOException {
    this.rootDoc.setAttribute("version", version);
    this.writeConfig();
  }

  @Override
  public void projectClosed(String projectName) {
  }

  @Override
  public void projectOpened(String projectName) {
  }

  public void removeFileFromFileSet(RasterFileContainer file, GenericGisFileSet fileSet) throws IOException {
    String fileSetName = fileSet.getName();
    String fileName = file.getName();
    Element eFileSet = this.getRasterFileSet(fileSetName);
    Element eFile = this.getRasterFileInFileSet(fileSetName, fileName);
    if (eFileSet != null && eFile != null) {
      eFileSet.removeContent(eFile);
    } else {
      throw new IOException("Fehler in der Serialisierung: X1");
    }
  }

  public void removeRasterFileSet(String name) throws IOException {
    Element fileSet = this.getRasterFileSet(name);
    if (fileSet != null) {
      this.rootDoc.removeContent(fileSet);
      this.writeConfig();
    } else
      throw new IOException("CXMLProjectProperties: Error Removie FileSer " + name);
  }

  @Override
  public void startClosingProject(String projectName) {
  }

  public void writeConfig() throws IOException {
    this.xmlFile.delete();
    this.xmlFile = new File(this.filename);
    this.xmlFile.createNewFile();
    this.streamOut = new FileOutputStream(this.xmlFile);
    this.xmlOut.output(this.doc, this.streamOut);
    this.streamOut.close();
  }

}

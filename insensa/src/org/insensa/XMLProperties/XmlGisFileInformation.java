/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.XMLProperties;

import org.apache.commons.io.FilenameUtils;
import org.insensa.IGisFileContainer;
import org.insensa.XMLPropertyException;
import org.insensa.connections.ConnectionFileChanger;
import org.insensa.extensions.IPluginExec;
import org.insensa.infoconnections.InfoConnection;
import org.insensa.inforeader.InfoReader;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.model.storage.JdomSaver;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.insensa.properties.IGisFileInformationStorage;
import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class XmlGisFileInformation implements IGisFileInformationStorage {

  private static final Logger log = LoggerFactory.getLogger(XmlGisFileInformation.class);
  final Lock lock = new ReentrantLock();
  private String separator = System.getProperties().getProperty("file.separator");
  private boolean inUse = false;
  //  private Element rootElement;
  private XMLOutputter xmlOut = new XMLOutputter(Format.getPrettyFormat());
  private SavableGroup savableGroup;

  private File xmlFile;
  private String fullFilename;

  /**
   * A physical file will be created if there is no available a the moment.
   * {@link IGisFileContainer} will be used to determine the path of the name.
   *
   * @param container used to determine the full path of the corresponding physical xml file.
   */
  public XmlGisFileInformation(IGisFileContainer container) {
    this.fullFilename = createFullPath(container);
    this.xmlFile = new File(this.fullFilename);

    if (!xmlFile.exists()) {
      Element rootElement = new Element("raster");
      this.savableGroup = initSavable(rootElement);
      try {
        this.xmlFile.createNewFile();
      } catch (IOException e) {
        throw new XMLPropertyException(e);
      }
      this.writeConfig(rootElement, this.xmlFile);
      //TODO: Why not closing here-- I will just try it ?
      this.close();
    } else {
      if (xmlFile.length() <= 0) {
        log.error("File is available but empty: ", this.fullFilename);
        throw new XMLPropertyException(this.fullFilename + ": File is empty",
            XMLPropertyException.FILE_EMPTY, container, this.fullFilename);
      } else {
        log.debug("File already exists, no initialization happened");
      }
    }
  }

  /**
   * Creates a new xml File for file newRasterFile with the information from
   * oldXmlFile.
   */
  public XmlGisFileInformation(XmlGisFileInformation oldXmlFile,
                               IGisFileContainer newRasterFile)
      throws IOException {
    this.fullFilename = createFullPath(newRasterFile);
    this.xmlFile = new File(this.fullFilename);

    if (!xmlFile.exists()) {
      xmlFile.createNewFile();
      Element rootElement = oldXmlFile.getRootElement();
      this.savableGroup = initSavable(rootElement);
      this.writeConfig(rootElement, xmlFile);
      this.close();
    } else {
      log.debug("File already exists, no initialization happened");
    }
  }

  //TODO: savableGroup not initialized here
  public XmlGisFileInformation(File file) throws IOException {
    this.fullFilename = file.getAbsolutePath();
    this.xmlFile = file;
    if (!this.xmlFile.exists()) {
      throw new IOException(this.fullFilename + ": File not exists");
    } else {
      if (this.xmlFile.length() <= 0)
        throw new IOException(this.fullFilename + ": File is empty");
    }
  }

  public SavableGroup initSavable(Element element) {
    SavableGroup group = new SavableGroup();
    group.saver = new JdomSaver(element);
    group.restorer = new JdomRestorer(element);
    group.setRootElement(element);
    return group;
  }

  public SavableGroup getSavableGroup() {
    return savableGroup;
  }

  public String createFullPath(String filesetPath, String gisFileName) {
    return Paths.get(filesetPath, "infos", FilenameUtils
        .removeExtension(gisFileName) + ".xml").toString();
  }

  public String createFullPath(IGisFileContainer fileContainer) {
    String path = fileContainer.getAbsOutputDirectoryPath();
    String filename = fileContainer.getOutputFileName();
    return createFullPath(path, filename);
  }

  @Override
  public void setConnectionFileChanger(ConnectionFileChanger con) {
    this.open(this.xmlFile);
    Element connElem = this.savableGroup.rootElement.getChild("connections");
    if (connElem != null) {
      connElem.removeContent();
    }
    savableGroup.saver
        .saveInGroup("connections", iSavableSaver
            -> iSavableSaver.save(con.getId(), con));
    this.writeConfig(savableGroup.getSavableElement(), xmlFile);
    this.close();
  }

  @Override
  public void addInfoConnection(InfoConnection infoConnection) {
    this.open(xmlFile);
    savableGroup.saver.saveInGroup("infoConnections", iSavableSaver -> {
      iSavableSaver.ifNotExist(infoConnection.getId(), infoConnection);
    });
    this.writeConfig(savableGroup.getSavableElement(), xmlFile);
    this.close();
  }

  /**
   * Adds a Description as Element to a raster Element.
   *
   * @return The new generated XML Element
   */
  @Override
  public void addDescription(String sDescription) throws IOException {
    this.open(this.xmlFile);
    if (sDescription != null) {
      Element description_old = this.savableGroup.rootElement.getChild("description");
      if (description_old != null)
        this.savableGroup.rootElement.removeChild("description");
      Element description = new Element("description");
      description.setText(sDescription);
      this.savableGroup.rootElement.addContent(description);
      this.writeConfig(savableGroup.getSavableElement(), xmlFile);
    }
    this.close();
  }

  /**
   * @return The new generated info Element or the existing one
   */
  @Override
  public void addInfoReader(InfoReader info) {
    this.open(this.xmlFile);

    savableGroup.saver.saveInGroup("infos", iSavableSaver -> {
      iSavableSaver.ifNotExist(info.getId(), info);
      try {
        this.writeConfig(savableGroup.getSavableElement(), xmlFile);
      } catch (XMLPropertyException e) {
        log.error("Error Writing Config", e);
      }
    });

    this.close();
  }

  /**
   * This is only public vor testing rights now.
   * We could change this in the future, but we would have to
   * change the open/close behaviour.
   * If we use this internally, we open twice and close twice, this does not work
   */
  public boolean pluginExists(IPluginExec exec) {
    AtomicBoolean exits = new AtomicBoolean(false);
    savableGroup.restorer.loadCollection(exec.getGroupName(),
        (s, restorer1) -> {
          if (s.equals(exec.getId()) && restorer1.loadInteger("orderId")
              .orElse(0) == exec.getOrderId()) {
            exits.set(true);
            return true;
          }
          return false;
        });
    return exits.get();
  }

  @Override
  public void addOptionFileChanger(OptionFileChanger option) {
    this.open(this.xmlFile);
    AtomicBoolean duplicate = new AtomicBoolean(false);
    savableGroup.restorer.loadRestorable("options", restorer1 -> {
      restorer1.loadCollection(option.getId(), restorer2 -> {
        int orderId = restorer2.loadInteger("orderId").get();
        if (option.getOrderId() == orderId) {
          duplicate.set(true);
          return true;
        }
        return false;
      });
    });
    if (!duplicate.get()) {
      savableGroup.saver.saveInGroup("options", iSavableSaver -> {
        iSavableSaver.save(option.getId(), option);
      });
    }
    this.writeConfig(savableGroup.getSavableElement(), xmlFile);
    this.close();
  }

  public void changeXmlFileSourcePath() throws IOException {
//    this.path = this.file.getAbsOutputDirectoryPath();
//    this.filename = this.file.getOutputFileName();
//
//    String newFillFilename;
//    if (this.path.endsWith(this.separator)) {
//      newFillFilename = this.path + "infos"
//          + this.separator + this.filename + ".xml";
//    } else {
//      newFillFilename = this.path + this.separator + "infos"
//          + this.separator + this.filename + ".xml";
//    }
//
//    File newFile = new File(newFillFilename);
//    if (!newFile.exists()) {
//      throw new IOException("File \"" + newFillFilename + "\" not exists");
//    }
//    this.fullFilename = newFillFilename;
//    this.xmlFile = newFile;
  }

  private void close() {
    if (this.inUse) {
      return;
    }
    this.savableGroup.rootElement = null;
    this.savableGroup.clear();
  }

  @Override
  public void closeGlobal() {
    if (this.inUse) {
      this.inUse = false;
      this.close();
    }
  }

  /**
   * @return
   */
  public boolean delete() {
    if (this.xmlFile.exists()) {
      boolean canDetele = this.xmlFile.delete();
      if (canDetele) {
        this.close();
      }
      return canDetele;
    }
    return false;
  }

  @Override
  public String getDescription() {
    this.open(this.xmlFile);
    AtomicReference<String> text = new AtomicReference<>("");
    savableGroup.restorer.loadRestorable("description", restorer1 -> {
      text.set(restorer1.loadString().orElse(""));
    });
    this.close();
    return text.get();
  }

//  public Document getDoc() {
//    return this.doc;
//  }
//
//  public void setDoc(Document doc) {
//    this.doc = doc;
//  }

  @Override
  public ISavableSaver getSaver() {
    return savableGroup.saver;
  }

  public void setSaver(ISavableSaver saver) {
    this.savableGroup.saver = saver;
  }

  @Override
  public ISavableRestorer getRestorer() {
    try {
      open(this.xmlFile);
    } catch (XMLPropertyException e) {
      log.error("Could not retrieve restorer, opening failed.", e);
    }
    return savableGroup.restorer;
  }

  public void setRestorer(ISavableRestorer restorer) {
    this.savableGroup.restorer = restorer;
  }

  public Element getRootElement() throws IOException {
    this.open(this.xmlFile);
    Element rootElement = (Element) this.savableGroup.getSavableElement().clone();
    this.close();
    return rootElement;
  }

//  public void setRootElement(Element rootElement) {
//    this.savableGroup.rootElement = rootElement;
//  }

  public File getXmlFile() {
    return this.xmlFile;
  }

  private synchronized void open(File xmlFile) {
    if (this.savableGroup == null
        || this.savableGroup.rootElement == null) {
      if (this.inUse) {
        throw new XMLPropertyException("XmlFileSetProperties: open: File is in use but still closed");
      }
      try {
        if (!xmlFile.exists()) {
          log.error("File does not exists, why should this happen ??");
          throw new XMLPropertyException("File does not exists, why should this happen ??");
        }
        Element element = new SAXBuilder().build(xmlFile).getRootElement();
        this.savableGroup = initSavable(element);
      } catch (JDOMException | IOException e) {
        log.error("Error reading xml file \"" + xmlFile.getAbsolutePath() + "\"", e);
        throw new XMLPropertyException(e);
      }
    } else {
      Element element;
      try {
        element = new SAXBuilder().build(xmlFile).getRootElement();
        this.savableGroup.setRootElement(element);
      } catch (JDOMException | IOException e) {
        log.error("Error reading xml file \"" + xmlFile.getAbsolutePath() + "\"", e);
        throw new XMLPropertyException(e);
      }
    }
  }

  @Override
  public void openGlobal() {
    if (this.inUse
        && this.savableGroup != null
        && this.savableGroup.rootElement != null)
      return;
    this.open(this.xmlFile);
    this.inUse = true;
  }

  @Override
  public synchronized void refreshPluginExec(IPluginExec pluginExec) throws IOException {
    this.lock.lock();
    this.open(this.xmlFile);

    if (pluginExists(pluginExec)) {
      Element eCons = ((JdomSaver) savableGroup.saver)
          .getCurrentElem().getChild(pluginExec.getGroupName());
      List<Element> eConList = eCons.getChildren(pluginExec.getId());
      eConList.stream().filter(element -> {
        Attribute orderIdAttr = element.getAttribute("orderId");
        try {
          int oldOrderId = (orderIdAttr != null) ? orderIdAttr.getIntValue() : 0;
          return oldOrderId == pluginExec.getOrderId();
        } catch (DataConversionException e) {
          log.error("Error getting orderId from plugin \"" + pluginExec.getId() + "\"", e);
          return false;
        }
      }).findFirst()
          .ifPresent(eCons::removeContent);
    }
    savableGroup.saver.saveInGroup(pluginExec.getGroupName(), iSavableSaver -> {
      iSavableSaver.save(pluginExec.getId(), pluginExec);
    });

    this.writeConfig(savableGroup.getSavableElement(), this.xmlFile);
    this.close();
    this.lock.unlock();
  }

  @Override
  public void removePluginExec(IPluginExec pluginExec) throws IOException {
    this.open(this.xmlFile);
    if (pluginExists(pluginExec)) {
      Element eCons = ((JdomSaver) savableGroup.saver)
          .getCurrentElem().getChild(pluginExec.getGroupName());
      List<Element> eConList = eCons.getChildren(pluginExec.getId());

      eConList.stream().filter(element -> {
        String attrVal = element.getAttributeValue("orderId");
        return attrVal == null || attrVal.equals(String.valueOf(pluginExec.getOrderId()));
      })
          .findFirst()
          .ifPresent(eCons::removeContent);
    }
    this.writeConfig(savableGroup.getSavableElement(), xmlFile);
    this.close();
  }
//
//  @Override
//  public void removeConnection(ConnectionFileChanger conn) throws IOException {
//    this.open(this.xmlFile);
//    Element eConn = getRootElement().getChild("connection");
//    Element eActConn = null;
//    List<Element> eConnList = eConn.getChildren();
//    for (Element eConnId : eConnList) {
//      if (Integer.valueOf(eConnId.getAttributeValue("orderId")) == conn.getOrderId()) {
//        eActConn = eConnId;
//      }
//    }
//    if (eActConn == null) {
//      this.close();
//      throw new IOException("XmlFileSetProperties: removeConnections1: Connection " + conn.getId() + " not found");
//    }
//    if (!eConn.removeContent(eActConn)) {
//      this.close();
//      throw new IOException("XmlFileSetProperties: removeConnections2: Connection " + conn.getId() + " not found");
//    }
//
//    this.writeConfig(savableGroup.getSavableElement(), xmlFile);
//    this.close();
//  }

//  @Override
//  public void removeInfoReader(InfoReader iReader) throws IOException {
//    this.open(this.xmlFile);
//    Element eInfos = savableGroup.rootElement.getChild("infos");
//    if (!eInfos.removeChild(iReader.getId())) {
//      this.close();
//      throw new IOException("XmlGisFileInformation: removeInfoReader: InfoReader" + iReader.getId() + "not found");
//    }
//    this.writeConfig(savableGroup.getSavableElement(), xmlFile);
//    this.close();
//  }

  @Override
  public void removeInfoReaders() throws IOException {
    this.open(xmlFile);
    Element eInfos = savableGroup.getSavableElement().getChild("infos");

    if (eInfos != null) {
//      savableGroup.rootElement.removeContent(eInfos);
      eInfos.removeContent();
      this.writeConfig(savableGroup.getSavableElement(), xmlFile);
    }
    this.close();
  }

//  @Override
//  public void removeOptionFileChanger(OptionFileChanger option) throws IOException {
//    this.open(xmlFile);
//    Element eOptions = savableGroup.rootElement.getChild("options");
//    String name = option.getId();
//    String id = String.valueOf(option.getOrderId());
//    List<Element> optionList = eOptions.getChildren(name);
//    if (optionList == null || optionList.size() <= 0) {
//      this.close();
//      throw new IOException("XmlGisFileInformation: removeOptionFileChanger: option " + option.getId() + " not found");
//    }
//
//    for (Element iElem : optionList) {
//      if (iElem.getAttributeValue("orderId").equals(id)) {
//        eOptions.removeContent(iElem);
//        this.writeConfig(savableGroup.getSavableElement(), xmlFile);
//        this.close();
//        return;
//      }
//    }
//    this.close();
//    throw new IOException("XmlGisFileInformation: removeOptionFileChanger: option " + option.getId() + " with orderId " + id + "not found");
//  }

  @Override
  public void removeAllOptionFileChangers() throws IOException {
    this.open(this.xmlFile);
    Element eOptions = this.savableGroup.getSavableElement().getChild("options");

    if (eOptions != null) {
      eOptions.removeContent();
      writeConfig(this.savableGroup.getSavableElement(), this.xmlFile);
//      getRootElement().removeContent(eOptions);
    }
    this.close();
  }


  /**
   * Moves a file to a new directory, old filename will be the same
   */
  public boolean renameTo(IGisFileContainer file, String newPath) {
    this.separator = System.getProperties().getProperty("file.separator");
    String newFilename = file.getOutputFileName();
    String newFullFilename = createFullPath(newPath, newFilename);
    File newFile = new File(newFullFilename);
    boolean renamed = this.xmlFile.renameTo(newFile);
    if (!renamed)
      return false;
    else {
      this.fullFilename = newFullFilename;
      this.xmlFile = newFile;
      return true;
    }
  }

  /**
   * @TODO Staendiges loeschen und erstellen ist nicht sehr elegant
   */
  public synchronized void writeConfig(Element elemToSave, File xmlFile) {
    Element currentElem = elemToSave;
    if (elemToSave == null) {
      this.open(xmlFile);
      currentElem = ((JdomSaver) this.savableGroup.saver).getRootElem();
    }
    try {
      //FIXME, missing result eval
      xmlFile.delete();
      //FIXME, missing result eval
      xmlFile.createNewFile();
      FileOutputStream steamOut = new FileOutputStream(xmlFile);
      this.xmlOut.output(currentElem, steamOut);
      steamOut.close();
    } catch (IOException e) {
      throw new XMLPropertyException(e);
    }
    this.close();
  }

  public class SavableGroup {
    public ISavableSaver saver;
    public ISavableRestorer restorer;
    public Element rootElement;

    public Element getSavableElement() {
      return ((JdomSaver) saver).getRootElem();
    }

    public void setRootElement(Element rootElement) {
      this.rootElement = rootElement;
      ((JdomSaver) saver).setRootElement(rootElement);
      ((JdomRestorer) restorer).setRootElem(rootElement);
    }

    public void clear() {
      this.rootElement = null;
      ((JdomSaver) saver).setRootElement(null);
      ((JdomRestorer) restorer).setRootElem((Element) null);
    }
  }

  public class CXmlRasterFileInformationOpenFailedException extends IOException {

    private static final long serialVersionUID = -4524191317842505217L;

    public CXmlRasterFileInformationOpenFailedException(String string) {
      super(string);
    }
  }

}

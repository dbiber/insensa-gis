/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.XMLProperties;


import org.insensa.FileSetListener;
import org.insensa.GenericGisFileSet;
import org.insensa.IGisFileContainer;
import org.insensa.IGisFileSet;
import org.insensa.RasterFileContainer;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class XmlFileSetProperties implements FileSetListener {
  private static final Logger log = LoggerFactory.getLogger(XmlFileSetProperties.class);
  final Lock lock = new ReentrantLock();
  private String filename;
  private Document doc;
  private Element rootDoc;
  private XMLOutputter xmlOut;
  private FileOutputStream streamOut;
  private File xmlFile;
  private String separator = System.getProperties().getProperty("file.separator");
  private IGisFileSet fileSet;

  public XmlFileSetProperties(IGisFileSet fileSet) throws IOException {
    this.fileSet = fileSet;
    String path = fileSet.getFullPath();
    if (path.endsWith(this.separator))
      this.filename = path + "FileSetSettings.xml";
    else
      this.filename = path + this.separator + "FileSetSettings.xml";

    this.xmlFile = new File(this.filename);
    if (this.xmlFile.exists() == false) {
      try {
        this.xmlFile.createNewFile();
      } catch (IOException e) {
        throw new IOException(e);
      }
      this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
      this.rootDoc = new Element("fileset");
      this.doc = new Document(this.rootDoc);
    } else {
      try {
        this.doc = new SAXBuilder().build(this.xmlFile);
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = this.doc.getRootElement();

      } catch (JDOMException e) {
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = new Element("fileset");
        this.doc = new Document(this.rootDoc);
        log.error("Fehler in XmlFileSetProperties(): ", e);
      } catch (IOException e1) {
        throw new IOException(e1);
      }
    }
    this.writeConfig();
  }

  public XmlFileSetProperties(String filename) throws IOException {
    this.filename = filename;
    this.xmlFile = new File(filename);
    if (this.xmlFile.exists() == false) {
      try {
        this.xmlFile.createNewFile();
      } catch (IOException e) {
        throw new IOException(e);
      }
      this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
      this.rootDoc = new Element("fileset");
      this.doc = new Document(this.rootDoc);
    } else {
      try {
        this.doc = new SAXBuilder().build(this.xmlFile);
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = this.doc.getRootElement();

      } catch (JDOMException e) {
        this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
        this.rootDoc = new Element("fileset");
        this.doc = new Document(this.rootDoc);
        log.error("Fehler in XmlFileSetProperties(): " + e.getMessage());
      } catch (IOException e1) {
        throw new IOException(e1);
      }
    }
    this.writeConfig();
  }

  public Element addChildSet(GenericGisFileSet childSet) throws IOException {
    Element childSetElement = new Element("fileset");
    childSetElement.setAttribute("name", childSet.getName());
    this.rootDoc.addContent(childSetElement);
    this.writeConfig();
    return childSetElement;
  }

  public Element addGisFile(IGisFileContainer rasterFile) throws IOException {
    Element eFile = this.createFileElement(rasterFile);
    this.rootDoc.addContent(eFile);
    this.writeConfig();
    return eFile;
  }

  public Element addRasterFileList(List<RasterFileContainer> rasterFileList) throws JDOMException, IOException {
    for (RasterFileContainer file : rasterFileList) {
      Element eFile = this.createFileElement(file);
      this.rootDoc.addContent(eFile);
    }
    this.writeConfig();
    return null;
  }

  public void changeChildFileSetName(GenericGisFileSet fileSet, String newName) throws IOException {
    List<Element> eList = this.rootDoc.getChildren("fileset");
    for (Element eFileSet : eList) {
      if (fileSet.getName().equals(eFileSet.getAttributeValue("name")))
        eFileSet.setAttribute("name", newName);
    }
    this.writeConfig();
  }

  public void changeChildFileSetName(String oldName, String newName) throws IOException {
    List<Element> eList = this.rootDoc.getChildren("fileset");
    for (Element eFileSet : eList) {
      if (oldName.equals(eFileSet.getAttributeValue("name")))
        eFileSet.setAttribute("name", newName);
    }
    this.writeConfig();
  }

  private void changeFileName(String oldFileName, String newFileName, String outputName) throws IOException {
    Element fileElem = this.getRasterFile(oldFileName, outputName);
    if (fileElem != null) {
      fileElem.setAttribute("name", newFileName);
      this.writeConfig();
    }
  }

  private void changeFileOutputName(String oldOutputName, String newOutputName, String fileName) throws IOException {
    Element fileElem = this.getRasterFile(fileName, oldOutputName);
    if (fileElem != null) {
      Element nameElem = fileElem.getChild("outputName");
      if (nameElem != null) {
        nameElem.setText(newOutputName);
        this.writeConfig();
      }
    }
  }

  private void changeXmlFileSourcePath(String xmlFileSourcePath) throws IOException {
    String newFullFilename;
    if (xmlFileSourcePath.endsWith(this.separator))
      newFullFilename = xmlFileSourcePath + "FileSetSettings.xml";
    else
      newFullFilename = xmlFileSourcePath + this.separator + "FileSetSettings.xml";

    File newFile = new File(newFullFilename);
    if (newFile.exists() != true)
      throw new IOException("File \"" + newFullFilename + "\" not exists");
    this.filename = newFullFilename;
    this.xmlFile = newFile;
  }

  @Override
  public void childFileOutputNameChanged(IGisFileSet fileSet, IGisFileContainer file, String oldName, String newName) throws IOException {
    this.changeFileOutputName(oldName, newName, file.getName());
  }

  @Override
  public void childFileRenamed(IGisFileSet fileSet, IGisFileContainer file, String oldName, String newName) throws IOException {
    this.changeFileName(oldName, newName, file.getOutputFileName());
  }

  @Override
  public void childFileSetRenamed(IGisFileSet fileSet, String oldName, String newName) throws IOException {
    log.debug("XmlFileSetProperties: childFileSetRenamed : fileSet=" + fileSet.getName());
    this.changeChildFileSetName(oldName, newName);
  }

  public void close() {
  }

  private Element createFileElement(IGisFileContainer file) {
    Element eFile = new Element("File");
    eFile.setAttribute("name", file.getName());
    if (!file.isHardCopy())
      eFile.setAttribute("path", file.getGisFile().getFileRef()
          .getParent() + this.separator);
    Element eFileOutputName = new Element("outputName");
    eFileOutputName.addContent(file.getOutputFileName());

    eFile.addContent(eFileOutputName);

    return eFile;
  }

  public boolean delete() {
    if (this.xmlFile.exists()) {
      return this.xmlFile.delete();
    }
    return true;
  }

  @Override
  public void fileSetRenamed(IGisFileSet fileSet, String oldName, String newName) {
    try {
      log.debug("XmlFileSetProperties: fileSetRenamed : fileSet=" + fileSet.getName());
      this.changeXmlFileSourcePath(fileSet.getFullPath());
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

  public Element getRasterFile(String fileName, String outputFileName) {
    for (Object elem : this.rootDoc.getChildren("File")) {
      if (((Element) elem).getAttributeValue("name").equals(fileName) && ((Element) elem).getChild("outputName").getText().equals(outputFileName)) {
        return (Element) elem;
      }
    }
    return null;
  }

  public List<?> getGisFileList() {
    return this.rootDoc.getChildren("File");
  }

  public Element getRasterFileSet(String setName) {
    for (Object elem : this.rootDoc.getChildren("fileset")) {
      if (((Element) elem).getAttributeValue("name").compareTo(setName) == 0) {
        return (Element) elem;
      }
    }
    return null;
  }

  public List<?> getGisFileSetList() {
    return this.rootDoc.getChildren("fileset");
  }

  public void removeFile(IGisFileContainer file) throws IOException {
    String fileName = file.getName();
    Element eFile = this.getRasterFile(fileName, file.getOutputFileName());
    if (eFile != null) {
      this.rootDoc.removeContent(eFile);
      this.writeConfig();
    } else {
      throw new IOException("Fehler in der Serialisierung: X1: XmlFileSetProperties");
    }
  }

  public void removeFileSet(IGisFileSet fileSet) throws IOException {
    String fileSetName = fileSet.getName();
    Element eFile = this.getRasterFileSet(fileSetName);
    if (eFile != null) {
      this.rootDoc.removeContent(eFile);
      this.writeConfig();
    } else {
      throw new IOException("Fehler in der Serialisierung: X2: XmlFileSetProperties");
    }
  }

  /**
   * @param oldFile
   * @param newFile
   * @return
   * @throws IOException
   */
  public Element replaceGisFileInformation(IGisFileContainer oldFile, IGisFileContainer newFile) throws IOException {
    Element eFile = this.getRasterFile(oldFile.getName(), oldFile.getOutputFileName());
    if (eFile != null) {
      // TODO geht nur bei ROOT FileSets.. umschreiben.
      // Was hab ich damit gemein??? nut bei ROOT FileSets???
      int index = this.rootDoc.indexOf(eFile);
      this.rootDoc.removeContent(eFile);
      Element eFileNew = this.createFileElement(newFile);
      this.rootDoc.addContent(index, eFileNew);
      this.writeConfig();
      return eFile;
    }
    return null;
  }

  /**
   * @TODO Staendiges loeschen und erstellen ist nicht sehr elegant
   */
  public void writeConfig() throws IOException {
    // maybe the source path has changed because of a rename oder move
    // try to relocate
    if (!this.xmlFile.exists()) {
      this.changeXmlFileSourcePath(this.fileSet.getFullPath());
    }
    this.xmlFile.delete();
    this.xmlFile = new File(this.filename);
    this.xmlFile.createNewFile();
    this.streamOut = new FileOutputStream(this.xmlFile);
    this.xmlOut.output(this.doc, this.streamOut);
    this.streamOut.close();
  }

}

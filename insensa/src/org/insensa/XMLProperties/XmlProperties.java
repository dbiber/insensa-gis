/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.XMLProperties;

import org.insensa.Project;
import org.insensa.User;
import org.insensa.UserListener;
import org.insensa.XMLPropertyException;
import org.insensa.extensions.IPluginExec;
import org.insensa.extensions.IScriptPluginExec;
import org.insensa.model.generic.IVariable;
import org.insensa.model.generic.VariableImpl;
import org.insensa.model.storage.IRestorableData;
import org.insensa.model.storage.ISavableRestorer;
import org.insensa.model.storage.ISavableSaver;
import org.insensa.model.storage.JdomRestorer;
import org.insensa.model.storage.JdomSaver;
import org.insensa.settings.PluginCache;
import org.insensa.view.extensions.ExtensionEntity;
import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class XmlProperties implements UserListener {
  private static final Logger log = LoggerFactory.getLogger(XmlProperties.class);

  private Document doc;
  private Element rootDoc;
  private XMLOutputter xmlOut;
  private FileOutputStream streamOut;
  private File xmlFile;
  private String filename;
  private ISavableSaver saver;
  private ISavableRestorer restorer;


  public XmlProperties(String mFilename) {
    this.filename = mFilename;
    this.xmlFile = new File(this.filename);
    if (!this.xmlFile.exists()) {
      createNewConfig();
      initNewConfigFile();
    } else {
      if (this.xmlFile.length() <= 0) {
        initNewConfigFile();
      } else {
        initExistingConfig();
      }
    }
  }


  private void createNewConfig() {
    try {
      if (!this.xmlFile.createNewFile()) {
        throw new XMLPropertyException("Config xml can not be created");
      }
    } catch (IOException e) {
      throw new XMLPropertyException("Config xml can not be created");
    }
  }

  private void initNewConfigFile() {
    this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
    this.rootDoc = new Element("ConfigRoot");
    // TODO set installPath in XML config
    this.doc = new Document(this.rootDoc);
  }

  private void initExistingConfig() {
    try {
      this.doc = new SAXBuilder().build(this.filename);
      this.xmlOut = new XMLOutputter(Format.getPrettyFormat());
      this.rootDoc = this.doc.getRootElement();
    } catch (JDOMException | IOException e) {
      throw new XMLPropertyException(e);
    }
  }

  public void restorePluginExec(IPluginExec pluginExec, String username) {
    Element element = getGlobalVariablesElement(username);
    JdomRestorer restorer = new JdomRestorer(element);
    restorer.loadRestorable(pluginExec.getGroupName(), restorer1 -> {
      restorer1.loadRestorable(pluginExec.getId(), pluginExec::restore);
    });
  }

  public void restorePluginVariables(IScriptPluginExec pluginExec, String username) {
    Element element = getGlobalVariablesElement(username);
    JdomRestorer restorer = new JdomRestorer(element);
    restorer.loadRestorable(pluginExec.getGroupName(), restorer1 -> {
      restorer1.loadRestorable(pluginExec.getId(), restorer2 -> {
        Map<String, IVariable> variables = new LinkedHashMap<>();
        List<IRestorableData> data = restorer2.loadCollection("variable",
            VariableImpl.class);
        data.forEach(iRestorableData -> variables.put(((IVariable) iRestorableData)
                .getName(),
            (IVariable) iRestorableData));
        pluginExec.setVariables(variables);
      });
    });
  }

  public void refreshPluginExec(IPluginExec pluginExec, String username) {
    removePluginExec(pluginExec, username);
    Element userVarElem = getGlobalVariablesElement(username);
    JdomSaver saver = new JdomSaver(userVarElem);
    saver.saveInGroup(pluginExec.getGroupName(), iSavableSaver -> {
      iSavableSaver.save(pluginExec.getId(), pluginExec);
    });
    try {
      writeConfig();
    } catch (IOException e) {
      log.error("Could not refresh plugin {} in global config", pluginExec);
    }
  }

  public void refreshPluginVariables(IScriptPluginExec pluginExec, String username) {
    removePluginExec(pluginExec, username);
    Element userVarElem = getGlobalVariablesElement(username);
    JdomSaver saver = new JdomSaver(userVarElem);
    saver.saveInGroup(pluginExec.getGroupName(), iSavableSaver -> {
      iSavableSaver.saveInGroup(pluginExec.getId(), iSavableSaver1 -> {
        pluginExec.getVariables().forEach((s, variable) -> {
          if (variable.isGlobal()) {
            iSavableSaver1.save(variable);
          }
        });
      });
    });
    try {
      writeConfig();
    } catch (IOException e) {
      log.error("Could not refresh plugin {} in global config", pluginExec);
    }
  }

  public void removePluginExec(IPluginExec pluginExec, String username) {
    Element varElem = getGlobalVariablesElement(username);
    Element groupElem = varElem.getChild(pluginExec.getGroupName());
    if (groupElem == null) {
      return;
    }
    groupElem.removeChild(pluginExec.getId());
  }

  public Element addProject(Element user, String name) {
    Date timeStamp = new Date(System.currentTimeMillis());
    Element eProject = new Element("project");
    eProject.setAttribute("name", name);
    DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
    eProject.setAttribute("time", df.format(timeStamp));
    user.addContent(eProject);
    return eProject;
  }

  /**
   * Adds a project with the specified name to a user
   */
  public Element addProject(String user, String name) throws IOException {
    Element userElem = this.getUser(user);
    if (userElem != null) {
      Date timeStamp = new Date(System.currentTimeMillis());
      Element eProject = new Element("project");
      eProject.setAttribute("name", name);
      DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
      eProject.setAttribute("time", df.format(timeStamp));
      userElem.addContent(eProject);
      this.writeConfig();
      return eProject;

    }
    return null;
  }

  public Element addUser(String workspace, String name) throws IOException {
    Element user = new Element("user");
    user.setAttribute("workspace", workspace);
    user.setAttribute("name", name);
    this.rootDoc.addContent(user);
    this.writeConfig();
    return user;
  }


  public Element addUser(User user) throws IOException {
    Element eUser = new Element("user");
    eUser.setAttribute("workspace", user.getWorkspacePath());
    eUser.setAttribute("name", user.getName());
    List<Project> projectList = user.getProjects();
    for (Project iProject : projectList) {
      Element eProject = new Element("project");
      eProject.setAttribute("name", iProject.getName());
      DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
      eProject.setAttribute("time", df.format(iProject.getDate()));
      eProject.setAttribute("fileCnt", Integer.toString(iProject.getFileCount()));
      eUser.addContent(eProject);
    }
    this.rootDoc.addContent(eUser);
    this.writeConfig();
    return eUser;
  }

  @SuppressWarnings("unchecked")
  private Object getProject(Element user, String projectName) {
    List<Element> eProjects = user.getChildren("project");
    if (eProjects == null)
      return null;
    for (Object eProject : eProjects) {
      if (((Element) eProject).getAttributeValue("name").equals(projectName))
        return eProject;
    }
    return null;
  }

  public Object getProject(String user, String workspace, String projectName) {
    Element eUser = this.getUser(user);
    if (eUser != null && eUser.getAttributeValue("workspace").equals(workspace)) {
      return this.getProject(eUser, projectName);
    }
    return null;
  }

  public List<String> getProjectNames(String user) {
    Element eUser = this.getUser(user);
    List<String> nameList = new ArrayList<String>();
    if (eUser != null) {
      List<Object> eProjects = eUser.getChildren("project");
      for (Object iPro : eProjects)
        nameList.add(((Element) iPro).getAttributeValue("name"));
    }
    return nameList;
  }

  public Element getGlobalVariablesElement(String username) {
    Element userElem = getUser(username);
    if (userElem == null) {
      throw new XMLPropertyException("Could not find user " + username);
    }
    Element varElem = userElem.getChild("variables");
    if (varElem == null) {
      varElem = new Element("variables");
      userElem.addContent(varElem);
    }
    return varElem;
  }


  @SuppressWarnings("unchecked")
  public Element getUser(String user) {
    List<Element> users = this.rootDoc.getChildren("user");
    if (users.size() > 0) {
      for (int i = 0; i < users.size(); i++) {
        if (user.equals(users.get(i).getAttributeValue("name"))) {
          return users.get(i);
        }
      }
    }
    return null;
  }

  public List<User> getUsers() throws IOException {
    List<User> users = new ArrayList<User>();
    List<Element> eUsers = this.rootDoc.getChildren("user");
    if (users != null) {
      for (Element iUser : eUsers) {
        String userName = iUser.getAttributeValue("name");

        String workspacePath = this.getWorkspace(userName);
        User newUser = new User(userName, workspacePath);
        List<String> projectNames = this.getProjectNames(userName);
        for (String iProjectName : projectNames) {
          Project tmpProject = new Project(iProjectName, userName, null);
          Object projectObj = this.getProject(iUser, iProjectName);
          if (projectObj != null) {
            Element projectElem = (Element) projectObj;
            String fileCnt = projectElem.getAttributeValue("fileCnt");
            if (fileCnt != null)
              tmpProject.setFileCount(Integer.valueOf(fileCnt));
            String sDate = projectElem.getAttributeValue("time");
            try {
              if (sDate != null && !sDate.isEmpty()) {
                DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
                Date newDate = df.parse(sDate);
                tmpProject.setDate(newDate);
              }
            } catch (ParseException e) {
              e.printStackTrace();
            }
          }
          newUser.addProject(tmpProject);
        }
        users.add(newUser);
      }
    }

    return users;
  }
//
//  public GlobalSettings getGlobalSettings() {
//    Element globalElem = this.rootDoc.getChild("global");
//    if (globalElem != null) {
//      GlobalSettings globalSettings = new GlobalSettings();
//      globalSettings.setUpdateCache(getUpdateCache(globalElem));
//    }
//    return null;

//  }
//  private UpdateCache getUpdateCache(Element globalElem) {
//    Element updateElem = globalElem.getChild("update");
//    if (updateElem != null) {
//      UpdateCache updateCache = new UpdateCache();
//      List<PluginCache> tmpList = getPluginCacheList(updateElem);
//      if (tmpList != null)
//        updateCache.setPluginCacheList(tmpList);
//    }
//    return null;

//  }

  public Optional<String> getRPath() {
    Element rElem = getOrCreateRElem();
    String rPath = rElem.getAttributeValue("rpath");
    if (rPath == null || rPath.isEmpty()) {
      return Optional.empty();
    } else {
      return Optional.of(rPath);
    }
  }

  public void setRPath(String absolutePath) {
    Element rElem = getOrCreateRElem();
    rElem.setAttribute("rpath", absolutePath);
    try {
      writeConfig();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Element getOrCreateLocalExtensionsElement() {
    Element globalElem = getOrCreateGlobalElement();
    Element rElem = globalElem.getChild("extensions");
    if (rElem == null) {
      rElem = new Element("extensions");
      globalElem.addContent(rElem);
    }
    return rElem;
  }

  private Element getOrCreateRElem() {
    Element globalElem = getOrCreateGlobalElement();
    Element rElem = globalElem.getChild("R");
    if (rElem == null) {
      rElem = new Element("R");
      globalElem.addContent(rElem);
    }
    return rElem;
  }


  public boolean isRServeRunning() {
    Element rElem = getOrCreateRElem();
    Attribute attribute = rElem.getAttribute("RServeRunning");
    if (attribute == null) {
      return false;
    }
    try {
      return attribute.getBooleanValue();
    } catch (DataConversionException e) {
      log.error("Error getting Attribute", e);
    }
    return false;
  }

  public void setRServeRunning(boolean found) {
    Element rElem = getOrCreateRElem();
    rElem.setAttribute("RServeRunning", Boolean.toString(found));
    try {
      this.writeConfig();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private List<PluginCache> getPluginCacheList(Element updateElem) {
    Element pluginsChecked = updateElem.getChild("pluginsChecked");
    if (pluginsChecked != null) {
      List<Element> pluginElemList = pluginsChecked.getChildren("plugin");
      if (pluginElemList != null && !pluginElemList.isEmpty()) {
        List<PluginCache> tmpList = new ArrayList<PluginCache>();
        for (Element iElem : pluginElemList) {
          PluginCache tmpPlug = new PluginCache();
          tmpPlug.setName(iElem.getAttributeValue("name"));
          tmpPlug.setVersion(iElem.getAttributeValue("version"));
          tmpList.add(tmpPlug);
        }
        return tmpList;
      }
    }
    return null;
  }

  public List<PluginCache> getPluginCacheList() {
    Element updateElem = getUpdateElement();
    if (updateElem != null)
      return getPluginCacheList(updateElem);
    return null;
  }

  public void setPluginCacheList(List<PluginCache> pluginCacheList) throws IOException {
    Element updateElem = getUpdateElement();
    if (updateElem == null)
      updateElem = createUpdateElem();
    updateElem.removeChild("pluginsChecked");
    Element checkedElem = new Element("pluginsChecked");
    for (PluginCache iCache : pluginCacheList) {
      Element pluginElem = new Element("plugin");
      pluginElem.setAttribute("name", iCache.getName());
      pluginElem.setAttribute("version", iCache.getVersion());
      checkedElem.addContent(pluginElem);
    }
    updateElem.addContent(checkedElem);
    this.writeConfig();
  }
//  private List<Element> getPluginCacheElementList() {
//    Element updateElem = getUpdateElement();
//    if (updateElem != null) {
//      Element pluginsChecked = updateElem.getChild("pluginsChecked");
//      if (pluginsChecked != null) {
//        return pluginsChecked.getChildren("plugin");
//      }
//    }
//    return null;

//  }

  private Element getUpdateElement() {
    Element globalElem = getOrCreateGlobalElement();
    if (globalElem != null) {
      return globalElem.getChild("update");
    }
    return null;
  }

  public Element getOrCreateGlobalElement() {
    Element globElem = rootDoc.getChild("global");
    if (globElem == null) {
      Element globalElem = new Element("global");
      rootDoc.addContent(globalElem);
    }
    return globElem;
  }

  private Element createUpdateElem() {
    Element globalElement = getOrCreateGlobalElement();
    Element updateElem = new Element("update");
    globalElement.addContent(updateElem);
    return updateElem;
  }

  public String getWorkspace(String user) {
    Element eUser = this.getUser(user);
    if (eUser != null) {
      return eUser.getAttributeValue("workspace");
    }
    return null;
  }

  @Override
  public void projectAdded(Project project) {
    try {
      this.addProject(project.getOwner(), project.getName());
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

  @Override
  public void projectClosed(Project project) {
    try {
      this.setFileCount(project.getOwner(), project.getName(),
          project.getUser().getWorkspacePath(), project.getFileCount());
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

  @Override
  public void projectClosing(Project project) {

  }

  @Override
  public void projectRemoved(Project project) {
    try {
      this.removeProject(project.getOwner(), project.getName());
    } catch (IOException e) {
      log.error("UNKNOWN", e);
    }
  }

  @Override
  public void propertyChanged(User user, String property, String oldValue, String newValue) {
    if (property == User.PROP_WORKSPACE) {
      if (this.getWorkspace(user.getName()) != oldValue)
        try {
          this.setWorkspace(user.getName(), newValue);
        } catch (IOException e) {
          log.error("UNKNOWN", e);
        }
    }
  }
//  /**
//   * @param user
//   */
//  public void removeAllProjects(String user) {
//    Element eUser = this.getUser(user);
//    if (eUser != null) {
//      eUser.removeChildren("project");
//    }

//  }

  public void removeProject(String user, String project) throws IOException {
    Element eUser = this.getUser(user);
    Element eProject = (Element) this.getProject(eUser, project);
    boolean removed = eUser.removeContent(eProject);
    if (removed == false)
      throw new IOException("Removing Project Failed");
    this.writeConfig();
  }

  public void setFileCount(String user, String project, String workspace, int fileCount) throws IOException {
    Element projectElem = (Element) this.getProject(user, workspace, project);
    if (projectElem != null) {
      projectElem.setAttribute("fileCnt", Integer.toString(fileCount));
      this.writeConfig();
    }
  }

  public void setWorkspace(String user, String workspacePath) throws IOException {
    Element eUser = this.getUser(user);
    if (eUser != null) {
      eUser.setAttribute("workspace", workspacePath);
      this.writeConfig();
    }
  }

  public void writeConfig() throws IOException {
    this.xmlFile.delete();
    this.xmlFile = new File(this.filename);
    this.xmlFile.createNewFile();
    this.streamOut = new FileOutputStream(this.xmlFile);
    this.xmlOut.output(this.doc, this.streamOut);
    this.streamOut.close();
  }


  public List<ExtensionEntity> getLocalExtensions() {
    Element extensionsElem = getOrCreateLocalExtensionsElement();
    ISavableRestorer restorer = new JdomRestorer(extensionsElem);
    List<ExtensionEntity> entities = restorer.loadCollection("extension", ExtensionEntity.class)
        .stream().map(iRestorableData -> (ExtensionEntity) iRestorableData)
        .collect(Collectors.toList());
    return entities;
  }

  public void saveLocalExtensions(List<ExtensionEntity> entities) throws IOException {
    Element extensionElem = getOrCreateLocalExtensionsElement();
    extensionElem.removeContent();
    ISavableSaver savableSaver = new JdomSaver(extensionElem);
    for (ExtensionEntity entity : entities) {
      savableSaver.save("extension", entity);
    }
    writeConfig();
  }
}

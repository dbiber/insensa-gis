/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import de.bibertech.javahelp.viewer.model.HelpMappingObject;
import de.bibertech.javahelp.viewer.model.HelpModel;

import org.gdal.gdal.gdal;
import org.gdal.ogr.ogr;
import org.insensa.controller.Controller;
import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.HelpSetLoader;
import org.insensa.helpers.IOHelper;
import org.insensa.helpers.r.FileWatcherManager;
import org.insensa.helpers.r.RProcessFactory;
import org.insensa.helpers.r.RStarter;
import org.insensa.updater.main.VersionChecker;
import org.insensa.updater.utils.PathUtils;
import org.insensa.view.View;
import org.insensa.view.dialogs.helper.DialogConstants;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Frame;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.FontUIResource;


public class InsensaGIS {
  private static final Logger log = LoggerFactory.getLogger(InsensaGIS.class);
  private static View mainView;

  public static void main(String[] args) {


    SwingUtilities.invokeLater(() -> {
      try {
        //Enable Antialiasing, this works for arch linux
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");

        if (args.length > 0 && args[0].equals("testing")) {
          IOHelper.getInstance().setDeployType(IOHelper.DEPLOY_TESTING);
          PathUtils.getInstance().setDeployType(PathUtils.DEPLOY_TESTING);
          log.info("Started in testing mode.");
        } else {
          log.info("Started in default mode. (not testing)");
        }

        InsensaErrorLogger.config();
        VersionChecker versionChecker = new VersionChecker();
        versionChecker.setInsensaDocumentPath(IOHelper.getInstance().getDocumentsDir()
            .getAbsolutePath());
        if (versionChecker.getArchType() == VersionChecker.ARCH_64BIT
            && versionChecker.getJvmArchType() == VersionChecker.JVM_ARCH_32BIT) {
          String message = "You have installed the 64Bit version of Insensa-GIS\n"
              + "but your currently used Java Virtual Machine is 32Bit.\n\n"
              + "Check your Java installation!";
          JOptionPane.showMessageDialog(null, message,
              "Wrong Java installation!", JOptionPane.ERROR_MESSAGE);
          return;
        }
        if (versionChecker.getArchType() == VersionChecker.ARCH_32BIT
            && versionChecker.getJvmArchType() == VersionChecker.JVM_ARCH_64BIT) {
          String message = "You have installed the 32Bit version of Insensa-GIS\n"
              + "but your currently used Java Virtual Machine is 64Bit.\n\n"
              + "Check your installation!";
          JOptionPane.showMessageDialog(null, message,
              "Wrong installation!", JOptionPane.ERROR_MESSAGE);
          return;
        }

        gdal.AllRegister();
        gdal.SetConfigOption("GDAL_DATA", "lib/gdal/data");
        ogr.RegisterAll();

        log.info("Set UI Font");
        DialogConstants.setUIFont(new FontUIResource(DialogConstants.getDialogFontPlain()));


        log.info("Init ExtensionManager and Model/View");
        ExtensionManager exManager = ExtensionManager.getInstance();

        Model model = new Model();
        mainView = new View(model);
        mainView.initialize();
        mainView.setExtendedState(Frame.MAXIMIZED_BOTH);

        Environment.getInstance().setModel(model);
        Environment.getInstance().setView(mainView);
        log.info("Init local development extensions");
        exManager.loadLocalPlugins();
        exManager.readPlugins();

        log.info("Init R");
        RProcessFactory factory = new RProcessFactory();
        factory.checkLocalRserve();

//        log.info("Init HelpSetLoader");
//        HelpSetLoader.getInstance();
        log.info("Init JavahelpViewer");
        HelpModel.getInstance().init("help");

//        HelpMappingObject helpMappingObject =
//            new HelpMappingObject("MyTarget", "http://google.de", "Such mal");
//        HelpModel.getInstance().addPluginHelp(helpMappingObject);

        List<HelpMappingObject> pluginHelpMapping = ExtensionManager.getInstance().getPluginHelp();
        for (HelpMappingObject helpMappingObject : pluginHelpMapping) {
          HelpModel.getInstance().addPluginHelp(helpMappingObject);
        }

        FileWatcherManager.getInstance().startService();
        //TODO: this is an odd way to create a controller... why ?
        Controller ctrl = new Controller(model, mainView, exManager);
        Environment.getInstance().setController(ctrl);

      } catch (UnsatisfiedLinkError er) {
        JOptionPane.showMessageDialog(null, er.getMessage(),
            "Error opening libraries", JOptionPane.ERROR_MESSAGE);
        log.error("Error starting main", er);
      } catch (JDOMException | IOException e) {
        log.error("UNKNOWN", e);
      }
    });
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa;

import java.io.IOException;

public class XMLPropertyException extends RuntimeException {

  public final static int FILE_EMPTY = 1;
  private static final long serialVersionUID = 4927843684115910424L;
  private IGisFileContainer file;
  private int errorType;
  private String configFileName;

  public XMLPropertyException(int errorType, RasterFileContainer file, String configFileName) {
    super();
    this.errorType = errorType;
    this.file = file;
    this.configFileName = configFileName;
  }

  public XMLPropertyException(String message, int errorType,
                              IGisFileContainer file, String configFileName) {
    super(message);
    this.errorType = errorType;
    this.file = file;
    this.configFileName = configFileName;
  }

  public XMLPropertyException(Throwable e, int errorType, RasterFileContainer file, String configFileName) {
    super(e);
    this.errorType = errorType;
    this.file = file;
    this.configFileName = configFileName;
  }

  public XMLPropertyException(Exception e) {
    super(e);
  }

  public XMLPropertyException(String s) {
    super(s);
  }

  public String getConfigFileName() {
    return this.configFileName;
  }

  public int getErrorType() {
    return this.errorType;
  }

  public IGisFileContainer getFile() {
    return this.file;
  }

}

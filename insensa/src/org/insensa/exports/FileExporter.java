/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.exports;

import org.insensa.IGisFile;
import org.insensa.IGisFileContainer;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.WorkerStatusCallback;
import org.insensa.view.exports.IViewFileExporterSetting;

import java.io.IOException;
import java.util.List;


public interface FileExporter
{
	
	/**
	 * This method gets called after {@link #setOutputFilePath(String)}
	 * and {@link #setRasterFileList(List)} are finished.
	 * It is responsible for the actual export work.
	 * 
	 * @throws IOException
	 */
	void export() throws IOException;

	/**
	 * Listener method that gets called when the output filepath is selected
	 * from a registered Settings Dialog ( {@link IViewFileExporterSetting} )
	 * 
	 * 
	 * @param outputFilePath the chosen output filepath
	 */
	void setOutputFilePath(String outputFilePath);

	/**
	 * @param rasterFileList the chosen {@link RasterFileContainer}
	 */
	void setRasterFileList(List<IGisFileContainer> rasterFileList);

	/**
	 * 
	 * 
	 * @param callback a listener object
	 * 
	 */
	void setWorkerStatusCallback(WorkerStatusCallback callback);
}

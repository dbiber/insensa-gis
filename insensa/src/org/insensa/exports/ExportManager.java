/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.exports;

import org.insensa.extensions.ExtensionManager;
import org.insensa.extensions.Service;
import org.insensa.extensions.ServiceList;
import org.insensa.extensions.ServiceType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ExportManager
{
	private ExtensionManager exManager;


	public ExportManager()
	{
		this.exManager = ExtensionManager.getInstance();
	}

	/**
	 * @param exporterName
	 * @return
	 */
	public FileExporter getFileExporter(String exporterName)
	{
		ClassLoader cl = this.exManager.getUrlClassLoader();// InsensaGIS.class.getClassLoader();
		ServiceList list = this.exManager.getFileExporterServices().get(exporterName);
		if (list == null)
			return null;
		Service service = list.getService(ServiceType.EXEC);
		if (service == null)
			return null;
		else
		{
			try
			{
				Class classfileExporter = cl.loadClass(service.getPackageName() + "." + service.getClassName());
				return (FileExporter) classfileExporter.newInstance();
			} catch (ClassNotFoundException e)
			{
				e.printStackTrace();
				return null;
			} catch (InstantiationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IllegalAccessException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * @return
	 */
	public List<String> getFileExporterList()
	{
		List<String> nameList = new ArrayList<String>();
		Iterator<String> iter = this.exManager.getFileExporterServices().keySet().iterator();
		while (iter.hasNext())
		{
			String name = iter.next();
			if (this.exManager.getFileExporterServices().get(name).getService(ServiceType.EXEC) != null)
				nameList.add(name);
		}
		return nameList;
	}
}

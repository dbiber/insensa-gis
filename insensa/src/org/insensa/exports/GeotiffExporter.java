/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.exports;

import org.gdal.gdal.Dataset;
import org.gdal.gdal.Driver;
import org.gdal.gdal.gdal;
import org.insensa.IGisFileContainer;
import org.insensa.RasterFile;
import org.insensa.RasterFileAccess;
import org.insensa.RasterFileContainer;
import org.insensa.helpers.WorkerStatusCallback;

import java.io.IOException;
import java.util.List;
import java.util.Vector;


public class GeotiffExporter implements FileExporter {
  private List<IGisFileContainer> rasterFileList;
  private String outputFilePath;
  private WorkerStatusCallback workerStatusCallback;
  private String separator = System.getProperties().getProperty("file.separator");
  private Vector<String> optionVector;


  public GeotiffExporter() {
    this.optionVector = new Vector<String>();
  }

  /**
   * @see org.insensa.exports.FileExporter#export()
   */
  public void export() throws IOException {
    if (this.rasterFileList == null || this.rasterFileList.isEmpty())
      throw new IOException("RasterFile:createFileCopy: no files selected");
    if (this.workerStatusCallback != null) {
      this.workerStatusCallback.getWorkerStatus().setProgressName("Export ");
      this.workerStatusCallback.getWorkerStatus().startProcess();
    }
    for (int i = 0; i < this.rasterFileList.size(); i++) {
      RasterFileContainer oldFile = (RasterFileContainer) this.rasterFileList.get(i);
      if (oldFile.getGisFile().exists() == false)
        continue;

      if (!this.outputFilePath.endsWith(this.separator))
        this.outputFilePath += this.separator;
      String fullName = this.outputFilePath + oldFile.getOutputFileName();

      Dataset ds = ((RasterFile) oldFile.getGisFile()).getDataset(RasterFileAccess.READ_ONLY);
      Driver gtiffDriver = gdal.GetDriverByName("GTiff");

      if (this.workerStatusCallback != null)
        this.workerStatusCallback.getWorkerStatus().setProgressName(
            "Export ( " + (i + 1) + " / " + this.rasterFileList.size() + " ) :" + oldFile.getOutputFileName());

      Dataset exportedDataset = gtiffDriver.CreateCopy(fullName, ds, 1, this.optionVector, this.workerStatusCallback);
      exportedDataset.delete();
      oldFile.getGisFile().unlock();
    }

    if (this.workerStatusCallback != null)
      this.workerStatusCallback.getWorkerStatus().endProgress();
  }

  /**
   *
   */
  public Vector<String> getOptionVector() {
    return this.optionVector;
  }

  /**
   *
   */
  public void setOptionVector(Vector<String> optionVector) {
    this.optionVector = optionVector;
  }

  /**
   * @see org.insensa.exports.FileExporter#setOutputFilePath(java.lang.String)
   */
  @Override
  public void setOutputFilePath(String outputFilePath) {
    this.outputFilePath = outputFilePath;
  }

  /**
   * @see org.insensa.exports.FileExporter#setRasterFileList(java.util.List)
   */
  @Override
  public void setRasterFileList(List<IGisFileContainer> rasterFileList) {
    this.rasterFileList = rasterFileList;
  }

  /**
   * @see org.insensa.exports.FileExporter#setWorkerStatusCallback(org.insensa.helpers.WorkerStatusCallback)
   */
  @Override
  public void setWorkerStatusCallback(WorkerStatusCallback callback) {
    this.workerStatusCallback = callback;
  }
}

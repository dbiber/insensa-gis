package org.insensa.exceptions;

public class ScriptParseException extends Exception {
  public ScriptParseException() {
  }

  public ScriptParseException(String message) {
    super(message);
  }

  public ScriptParseException(String message, Throwable cause) {
    super(message, cause);
  }

  public ScriptParseException(Throwable cause) {
    super(cause);
  }

  public ScriptParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}

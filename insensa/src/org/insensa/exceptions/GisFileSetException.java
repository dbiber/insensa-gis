/*
 * Copyright (C) 2011 Dennis Biber 
 *
 * Insensa-GIS - http://www.insensa.org 
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa.exceptions;

import java.io.IOException;

public class GisFileSetException extends IOException {

  public static final int CREATE_DIR_WHILE_OPEN_PROJECT = 1;
  private static final long serialVersionUID = -1921988134222644727L;
  private int errorCause;
  private String rasterFilePath = "";

  public GisFileSetException(String message, int cause) {
    super(message);
    this.errorCause = cause;
  }

  public int getErrorCause() {
    return this.errorCause;
  }

  public String getRasterFilePath() {
    return this.rasterFilePath;
  }

  public void setGisFilePath(String rasterFilePath) {
    this.rasterFilePath = rasterFilePath;
  }
}

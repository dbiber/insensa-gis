package org.insensa.exceptions;

import java.io.IOException;

public class WriteConfigException extends Exception {
  public WriteConfigException(String msg) {
    super(msg);
  }

  public WriteConfigException(IOException e) {
    super(e);
  }
}

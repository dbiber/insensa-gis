package org.insensa.exceptions;

import java.io.IOException;

public class CreateSettingsViewException extends Exception {
  public CreateSettingsViewException(String msg) {
    super(msg);
  }

  public CreateSettingsViewException(IOException e) {
    super(e);
  }

  public CreateSettingsViewException(String message, Throwable cause) {
    super(message, cause);
  }
}

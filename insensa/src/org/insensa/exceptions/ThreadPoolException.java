package org.insensa.exceptions;

public class ThreadPoolException extends RuntimeException {
  public ThreadPoolException(String s) {
    super(s);
  }
}

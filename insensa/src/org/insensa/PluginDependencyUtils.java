package org.insensa;

import org.insensa.extensions.IPluginExec;
import org.insensa.helpers.ExecDependencyController;
import org.insensa.inforeader.InfoManager;
import org.insensa.inforeader.InfoReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PluginDependencyUtils {

  private List<IGisFileContainer> fileContainers;
  private WorkerStatus workerStatus;
  private IPluginExec pluginExec;

  public PluginDependencyUtils(List<IGisFileContainer> fileContainers,
                               WorkerStatus workerStatus,
                               IPluginExec pluginExec) {
    this.fileContainers = fileContainers;
    this.workerStatus = workerStatus;
    this.pluginExec = pluginExec;
  }

  public void solveDependencies() throws IOException {
    ExecDependencyController depChecker;

    InfoManager infoManager = new InfoManager();
    List<IGisFileContainer> solvableFileList = new ArrayList<>();
    for (IGisFileContainer fileContainer: fileContainers) {
      boolean addedInfoReader = false;
      for (String infoDependencies: this.pluginExec.getInfoDependencies()) {
        InfoReader infoReader = fileContainer.getInfoReader(infoDependencies);
        if (infoReader == null) {
          if (!fileContainer.addInfoReader(infoManager.getInfoReader(infoDependencies), true)) {
            throw new IOException("Error adding InfoReader: " + infoDependencies + " for File: " + fileContainer.getName());
          }
          addedInfoReader = true;
        } else if (!infoReader.isUsed()) {
          addedInfoReader = true;
        }
      }
      if (addedInfoReader) {
        solvableFileList.add(fileContainer);
      }
    }

    if (this.workerStatus != null) {
      this.workerStatus.setChildrenWorkerStatusLists(solvableFileList.size());
    }
    for (int i = 0; i < solvableFileList.size(); i++) {
      WorkerStatusList workerStatusList = null;
      if (workerStatus != null) {
        workerStatusList = this.workerStatus.getChildWorkerStatusList(i);
      }
      depChecker = solvableFileList.get(i).getDepChecker();
      if (workerStatusList != null) {
        solvableFileList.get(i).executeInfos(this.pluginExec.getInfoDependencies(), workerStatusList);
      } else {
        solvableFileList.get(i).executeInfos(this.pluginExec.getInfoDependencies(), null);
      }
      depChecker.checkInfoDependencies(this.pluginExec);
    }
  }
}

/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa;

import org.insensa.optionfilechanger.OptionFileChanger;

import java.io.IOException;

public class GisFileContainerFactory implements IGisFileContainerFactory {

  public static IGisFileContainer createGisFileContainer(
      ParentModule parentModule,
      String fullPath,
      String outputName,
      boolean serialize,
      WorkerStatus workerStatus) {
    return new RasterFileContainer(parentModule, outputName, fullPath);
  }

  @Override
  public IGisFileContainer create(ParentModule parentModule,
                                  String fullPath,
                                  String outputName,
                                  boolean serialize,
                                  WorkerStatus workerStatus) {
    return new RasterFileContainer(parentModule, outputName, fullPath);
  }

  /**
   * @see RasterFileContainer#RasterFileContainer(ParentModule, String, String, OptionFileChanger)
   */
  @Override
  public IGisFileContainer create(ParentModule parent, String outputFileName,
                                  String fullName, OptionFileChanger option) {
    return new RasterFileContainer(parent, outputFileName, fullName, option);
  }

  public IGisFileContainer create(String outputFileName, String fullName) {
    return new RasterFileContainer(outputFileName, fullName);
  }

//  public IGisFileContainer create(String absoluteFile) {
//    return new GisFileContainer(path,name);
//  }

}

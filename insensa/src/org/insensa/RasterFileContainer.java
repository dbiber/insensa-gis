/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.gdal.gdal.Band;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.Driver;
import org.gdal.osr.SpatialReference;
import org.insensa.exceptions.GisFileException;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.Map;


/**
 * Enthaelt informationen ueber ein Raster File incl. der verwendeten Option und
 * Infos
 */
public class RasterFileContainer extends GisFileContainer {

  public RasterFileContainer(GisFileContainer oldFile,
                             ParentModule parent, String newOutputFileName,
                             boolean keepOpen)
      throws IOException, GisFileException {
    super(oldFile, parent, newOutputFileName, keepOpen);
  }

  public RasterFileContainer(ParentModule parent, String outputFileName,
                             String fullName)
      throws GisFileException {
    super(parent, outputFileName, fullName, null);
  }

  /**
   * @see GisFileContainer#GisFileContainer(ParentModule, String, String, OptionFileChanger)
   */
  public RasterFileContainer(ParentModule parent, String outputFileName,
                             String fullName, OptionFileChanger option)
      throws GisFileException {
    super(parent, outputFileName, fullName, option);
  }

  public RasterFileContainer(String outputFileName, String fullName)
      throws GisFileException {
    super(outputFileName, fullName);
  }

  public RasterFileContainer(String newAbsoluteFilePath,
                             GisFileContainer oldFile,
                             ParentModule parent, boolean copyInfos,
                             boolean copyOptions, boolean keepOpen)
      throws IOException, JDOMException, GisFileException {
    super(newAbsoluteFilePath, oldFile, parent, copyInfos, copyOptions, keepOpen);
  }

  @Override
  public IRasterGisFile getGisFile() {
    return (IRasterGisFile) super.getGisFile();
  }

  public float getCellSize() {
    return getGisFile().getCellSize();
  }

  public float getXllCorner() {
    return getGisFile().getXllCorner();
  }

  public float getYllCorner() {
    return getGisFile().getYllCorner();
  }

  public int getNRows() {
    return getGisFile().getNRows();
  }

  public int getNCols() {
    return getGisFile().getNCols();
  }

//  public boolean createNewFile(RasterFileContainer tmpFile2, int gdt_float32, double maxValue)
//      throws IOException, GisFileException {
//    return getGisFile().createNewFile(tmpFile2.getGisFile(), gdt_float32, maxValue);
//  }

  public Band getBand(RasterFileAccess access) throws IOException {
    return ((RasterFile) getGisFile()).getBand(access);
  }

  public float getNoDataValue() {
    return getGisFile().getNoDataValue();
  }

  public Dataset getDataset(RasterFileAccess access) throws IOException {
    return ((RasterFile) getGisFile()).getDataset(access);
  }

  public int getDataType() {
    return getGisFile().getDataType();
  }

  public void writeNodataValue(double newValue) throws IOException {
    getGisFile().writeNodataValue(newValue);
  }

  public SpatialReference getSpatialReference() {
    return getGisFile().getSpatialReference();
  }

  public double[] getGeoTransform() {
    return getGisFile().getGeoTransform();
  }

  public Map<String, String> getSpatialReferenceAttr() {
    return getGisFile().getSpatialReferenceAttr();
  }

  public void writeNodataValueMax() throws IOException {
    getGisFile().writeNodataValueMax();
  }

  public Driver getDriver(RasterFileAccess access) throws IOException {
    return ((RasterFile) getGisFile()).getDriver(access);
  }

  public int getXSize() throws IOException {
    return getBand(RasterFileAccess.READ_ONLY).getXSize();
  }

  public int getYSize() throws IOException {
    return getBand(RasterFileAccess.READ_ONLY).getYSize();
  }

  public void readRaster(int xOff, int yOff, int xSize, int ySize, float[] arrayReference) throws IOException {
    getBand(RasterFileAccess.READ_ONLY).ReadRaster(xOff, yOff, xSize, ySize, arrayReference);
  }

  public float[] readRaster(int xOff, int yOff, int xSize, int ySize) throws IOException {
    int size = ySize * xSize;
    float[] data = new float[size];
    getBand(RasterFileAccess.READ_ONLY).ReadRaster(xOff, yOff, xOff, yOff, data);
    return data;
  }

//  public String getDriverShortName() throws IOException {
//    return getGisFile().getDriver(RasterFileAccess.READ_ONLY).getShortName();
//  }

}

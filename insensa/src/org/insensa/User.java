/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa;

import org.insensa.exceptions.GisFileException;
import org.insensa.helpers.SystemSpec;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class User {
  public static final String PROP_WORKSPACE = "workspace";
  public static final String PROP_USER = "user";
  private java.util.List<Project> projects = new ArrayList<Project>();
  private String name;
  private String workspacePath;
  private Project activeProject;
  private List<UserListener> userListener;

  public User(String name, String workspacePath) {
    this.userListener = new ArrayList<>();
    setName(name);
    setWorkspacePath(workspacePath);
  }

  /**
   * Creates a new Project and adds it to the user.
   * This will NOT create the project on the filesystem, you have to
   * call {@link Project#create()} or User{@link #createProject(Project)} for that.
   */
  public Project addNewProject(String projectName, List<IProjectListener> listener) throws IOException {
    Project newProject = new Project(projectName, this.name, listener);
    this.addProject(newProject);
    newProject.setUser(this);
    return newProject;
  }

  public void addProject(Project project) {
    this.projects.add(project);
    project.setUser(this);
    for (UserListener iListener : this.userListener) {
      iListener.projectAdded(project);
    }
  }

  /**
   * 1. Created the project on disk (if the user owens the project)
   * 2. set project as activeProject
   *
   * @see Project#create()
   */
  public boolean createProject(Project project) throws IOException {
    if (!this.projects.contains(project)) {
      return false;
    }
    if (project.create()) {
      this.activeProject = project;
      return true;
    }
    return false;
  }

  public void addUserListener(UserListener listener) {
    this.userListener.add(listener);
  }


  public void closeProject() {
    int fileCnt = 0;
    if (this.activeProject != null) {
      List<IGisFileSet> fileSetList = this.activeProject.getFileSetList();
      if (fileSetList != null) {
        for (IGisFileSet iSet : fileSetList) {
          fileCnt += this.countFiles(iSet);
        }
      }
      this.activeProject.setFileCount(fileCnt);
      for (UserListener iListener : this.userListener) {
        iListener.projectClosing(this.activeProject);
      }
      this.getActiveProject().close();
      for (UserListener iListener : this.userListener) {
        iListener.projectClosed(this.activeProject);
      }
      this.activeProject = null;
    }
  }

  private int countFiles(IGisFileSet fileSet) {
    int fileCnt = 0;
    for (IGisFileSet iSet : fileSet.getChildSets()) {
      fileCnt += this.countFiles(iSet);
    }
    fileCnt += fileSet.getFileList().size();
    return fileCnt;
  }


  public Project getActiveProject() {
    return this.activeProject;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Project getProject(String selProjectName) {
    for (Project iProject : this.projects) {
      if (iProject.getName().equals(selProjectName)) {
        return iProject;
      }
    }
    return null;
  }

  /**
   * @return a list of all projects or an empty List
   */
  public java.util.List<Project> getProjects() {
    return this.projects;
  }

  public String getWorkspacePath() {
    return this.workspacePath;
  }

  public void setWorkspacePath(String workspacePath) {
    String oldValue = this.workspacePath;
    this.workspacePath = workspacePath;
    if (!this.workspacePath.endsWith(SystemSpec.separator)) {
      this.workspacePath += SystemSpec.separator;
    }
    for (UserListener iListener : this.userListener) {
      iListener.propertyChanged(this, "workspace", oldValue, workspacePath);
    }
  }

  public void openProject(Project project, WorkerStatus wStatus)
      throws IOException, JDOMException, GisFileException {
    if (this.projects.contains(project)) {
      this.activeProject = project;
      if (project.exists()) {
        project.open(wStatus);
      }
    }
  }

  public void removeProject(Project project) {
    if (this.projects.contains(project)) {
      this.projects.remove(project);
      for (UserListener iListener : this.userListener) {
        iListener.projectRemoved(project);
      }
      if (this.activeProject == project) {
        this.activeProject = null;
      }
    }
  }

  public void removeProject(String projectName) throws IOException {
    Project project = null;
    for (Project iProject : this.projects) {
      if (iProject.getName().equals(projectName)) {
        project = iProject;
        break;
      }
    }
    if (project == null) {
      throw new IOException("Can't find project " + projectName);
    } else {
      this.removeProject(project);
    }
  }

}

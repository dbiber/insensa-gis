/*
 * Copyright (C) 2017 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.insensa;

import org.insensa.XMLProperties.XmlFileSetProperties;
import org.insensa.exceptions.GisFileException;
import org.insensa.optionfilechanger.OptionFileChanger;
import org.jdom.JDOMException;

import java.io.IOException;
import java.util.List;

public interface IGisFileSet extends ParentModule {
  //TODO: Replace GisFileException
  IGisFileSet addChildSet(String parentSetPath, String name, boolean serialize,
                          WorkerStatus workerStatus)
      throws IOException, JDOMException, GisFileException;

  void addFileSetListener(FileSetListener listener);

  void addGisFileInformation(IGisFileContainer file, boolean serialize) throws IOException;

  void addGisFileAt(IGisFileContainer file, boolean serialize, int index) throws IOException;

  void addGisFile(IGisFileContainer newGisFile, boolean serialize) throws IOException,
      GisFileException;

  void addGisFileNameList(List<String> fullNameList, List<String> outputNameList,
                          String optionName, boolean serialize) throws JDOMException,
      IOException;

  void closeIter();

  boolean createChildFileSetDir(String path) throws IOException;

  void createFileSetDir() throws IOException;

  void delete() throws IOException;

  boolean exists();

  void fireChildGisFileOutputNameChanged(IGisFileContainer file, String oldOutputFile,
                                         String newNameTmp) throws IOException;

  void fireChildGisFileRenamed(IGisFileContainer file, String oldName, String newNameTmp)
      throws IOException;

  IGisFileContainer getActiveGisFile();

  IGisFileSet getChildSetByName(String name);

  List<IGisFileSet> getChildSets();

  String getDescription();

  void setDescription(String description);

  List<IGisFileContainer> getFileList();

  XmlFileSetProperties getFileSetProperties();

  IGisFileSet getLastActiveFileSet();

  String getName();

  void setName(String name);

  ParentModule getParent();

  String getPath();

  int indexOfIter(IGisFileSet fileSet);

  void removeChildFileSet(IGisFileSet fileSet) throws IOException;

  int removeGisFile(IGisFileContainer gisFileInformation) throws IOException;

  void renameTo(String newName) throws IOException;

  boolean replaceGisFileWithTarget(IGisFileContainer oldGisFile,
                                   OptionFileChanger option) throws IOException;

  void setActiveParentFileSetIter();

  void setParentModule(ParentModule parent);

  void unsetActiveSetsIter();
}

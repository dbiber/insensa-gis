# Insensa-GIS
INSENSA-GIS is a free and open source software for statistical 
computing and display of GIS data  

[https://insensa.org](https://insensa.org)

**Features**

 - Raster file formats: 
   - ArcInfo binary grid
   - ArcInfo ASCII grid
   - Geo-Tiff
 - Vector file formats:
   -  Shape File
 - Simple Interface
 - Expandable with Java and R
 - In-App Plugin Download Store
 - Update Notification

## Background and Motivation
Insensa-GIS started as a educational project to learn Java 
but did get some attention and grew quiet fast.
After some years of absence I've decided to share this project and try to 
put my years of experience after starting with Insensa-GIS back into it. 

For this reason, there are still a lot of code parts which are really old
and done during my educational phase.


## Installation ( beta )
The latest releases can be found on Insensa-GIS Homepage
 ([Get Insensa](http://insensa.org/get-insensa/download-software.html)).
Please be aware, that Indensa GIS is still under development and will surely still have bugs.
Reporting issues will really help improve this software.


## Building from source
All you have to do to get Insensa-GIS up and running is cloning this
 project and calling the gradle wrapper

```bash
git clone "Insensa repo address"
cd <Path to Insensa>
./gradlew.bat gdalDownload runInsensa
```
 
## Dependencies
- Java 1.8
- R >= 3.3 (Optional, for R plugins to work)
- rJava (Optional, for R plugins to work)

## Support
If you need any supoort from us or the community, we recommend to use [gis.stackexchange](http://gis.stackexchange.com/).
If you have a question, just tag your question with "insensa" or use this [link](http://gis.stackexchange.com/questions/ask?tags=insensa). 

If you want to support us improving Insensa-GIS, there are various ways you can do this:

 - Testing the software and reporting the issues directly on Github
 - Creating your own plugins, [see...](http://insensa.org/development/plugin-development.html)

## Copyright

 Copyright (C) 2017 Dennis Biber
 
 Insensa-GIS - http://www.insensa.org
 
 Insensa-GIS is free software; you can redistribute it and/or modify  
 it under the terms of the GNU General Public License as published  
 by the Free Software Foundation; either version 3 of the License,  
 or (at your option) any later version.  
  
 Insensa-GIS is distributed in the hope that it will be useful, but  
 WITHOUT ANY WARRANTY; without even the implied warranty of  
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  
 General Public License for more details.  
 
 You should have received a copy of the GNU General Public License  
 along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.  

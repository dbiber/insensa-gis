/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.items;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;


public class ItemPlugin {
    private String minVersion;
    private String version;
    private ItemFile itemFile;
    private ItemDescription itemDescription;
    private List<ItemScreenshot> itemScreenshotList;
    private String name;


    public ItemPlugin() {
    }

    @SuppressWarnings("unchecked")
    public ItemPlugin(Element element) throws IOException {

        version = element.getAttributeValue("version");
        if (version == null)
            throw new IOException("version attribute not Found");
        name = element.getAttributeValue("name");
        if (name == null)
            throw new IOException("name attribute not Found");
        itemFile = new ItemFile(element.getChild("file"));

        minVersion = element.getAttributeValue("minVersion");

        Element elem = element.getChild("description");
        if (elem != null)
            itemDescription = new ItemDescription(elem);
        else
            itemDescription = null;

        elem = element.getChild("screenshots");
        if (elem != null) {
            itemScreenshotList = new ArrayList<>();
            for (Element iElem : ((List<Element>) elem.getChildren())) {
                itemScreenshotList.add(new ItemScreenshot(iElem));
            }
        } else {
            itemScreenshotList = new ArrayList<>();
        }
    }

    public String getMinVersion() {
        return minVersion;
    }

    public void setMinVersion(String minVersion) {
        this.minVersion = minVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ItemFile getItemFile() {
        return itemFile;
    }

    public void setItemFile(ItemFile itemFile) {
        this.itemFile = itemFile;
    }

    public ItemDescription getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(ItemDescription itemDescription) {
        this.itemDescription = itemDescription;
    }

    public List<ItemScreenshot> getItemScreenshotList() {
        return itemScreenshotList;
    }

    public void setItemScreenshotList(List<ItemScreenshot> itemScreenshotList) {
        this.itemScreenshotList = itemScreenshotList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

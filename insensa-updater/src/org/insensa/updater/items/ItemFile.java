/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.items;

import java.io.IOException;

import org.jdom.Element;

public class ItemFile {
    private String path;
    private String name;
    private String os;
    private String arch;

    public ItemFile() {
    }

    public ItemFile(Element element) throws IOException {
        if (element == null) {
            throw new IOException("element is null");
        }
        name = element.getAttributeValue("name");
        if (name == null) {
            throw new IOException("\"name\" attribute in file element not found");
        }
        path = element.getAttributeValue("path");
        os = element.getAttributeValue("os");
        arch = element.getAttributeValue("arch");
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOs() {
        return os;
    }

    public void setArch(String arch) {
        this.arch = arch;
    }

    public String getArch() {
        return arch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.items;

import java.io.IOException;

import org.jdom.Element;

public class ItemInsensa {
  private String version;
  private ItemFile itemFile;
  private ItemDescription itemDescription;

  public ItemInsensa(Element element) throws IOException {
    if (element == null) {
      throw new IOException("Insensa element is null");
    }
    version = element.getAttributeValue("version");
    if (version == null) {
      throw new IOException("version attribute in insensa element not found");
    }

    itemFile = new ItemFile(element.getChild("file"));
    itemDescription = new ItemDescription(element.getChild("description"));
  }

  public ItemFile getItemFile() {
    return itemFile;
  }

  public String getVersion() {
    return version;
  }

  public ItemDescription getItemDescription() {
    return itemDescription;
  }
}

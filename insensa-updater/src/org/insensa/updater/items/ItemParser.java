/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.items;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.JDOMParseException;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ItemParser {
    private Document document;
    private List<ItemPlugin> itemPluginList;

    public ItemParser(Document xmlDocument) throws IOException {
        this.document = xmlDocument;
        parse();
    }

    public void parse() throws IOException {
        itemPluginList = new ArrayList<>();
        List<Element> elemList = document.getRootElement().getChildren();
        if (elemList == null || elemList.isEmpty()) {
            throw new IOException("No children in root element found");
        }
        for (Element iElem : elemList) {
            itemPluginList.add(new ItemPlugin(iElem));
        }
    }

    public List<ItemPlugin> getItemPluginList() {
        return itemPluginList;
    }

    public static ItemPlugin parsePlugin(InputStream inputStream) throws IOException, JDOMParseException {
        SAXBuilder builder = new SAXBuilder();
        Document doc;
        try {
            doc = builder.build(inputStream);
        } catch (JDOMException e) {
            throw new JDOMParseException("Error building inpuStream: " + inputStream.toString(), e);
        } catch (IOException e) {
            throw new IOException("Error building inpuStream: " + inputStream.toString(), e);
        }
        ItemPlugin itemPlugin = new ItemPlugin(doc.getRootElement());
        inputStream.close();
        return itemPlugin;
    }

    public static ItemInsensa parseInsensa(File inputFile) throws IOException, JDOMException {
        return parseInsensa(new FileInputStream(inputFile));
    }

    public static ItemInsensa parseInsensa(InputStream inputStream) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        Document doc;
        try {
            doc = builder.build(inputStream);
        } catch (JDOMException e) {
            throw new JDOMParseException("Error building inpuStream: " + inputStream.toString(), e);
        } catch (IOException e) {
            throw new IOException("Error building inpuStream: " + inputStream.toString(), e);
        }
        ItemInsensa itemInsensa = new ItemInsensa(doc.getRootElement());
        inputStream.close();
        return itemInsensa;
    }


}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.main;

import org.insensa.updater.items.ItemInsensa;
import org.insensa.updater.items.ItemParser;
import org.insensa.updater.items.ItemPlugin;
import org.insensa.updater.utils.PathUtils;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class VersionChecker {
  public static final int OS_UNKNOWN = 0;
  public static final int OS_WIN = 1;
  public static final int OS_LINUX = 2;
  public static final int OS_MAC = 3;
  public static final int ARCH_UNKNOWN = 0;
  public static final int ARCH_32BIT = 1;
  public static final int ARCH_64BIT = 2;
  public static final int JVM_ARCH_UNKNOWN = 0;
  public static final int JVM_ARCH_32BIT = 1;
  public static final int JVM_ARCH_64BIT = 2;
  public static final int VERSION_OLDER = 0;
  public static final int VERSION_NEWER = 1;
  public static final int VERSION_EQUAL = 2;
  public static final int VERSION_UNKNOWN = 3;
  private String insensaDocumentsPath;
  private String insensaJarPath;
  private File insensaJar;
  private File insensaPathFile;
  private Integer archType;
  private Integer jvmArchType;
  private Integer osType;
  private Map<String, ItemPlugin> pluginItemMap;
  private Map<String, File> pluginFileMap;
  private ItemInsensa itemInsensa;


  public VersionChecker() {
    archType = ARCH_UNKNOWN;
    jvmArchType = findJvmArchType();
  }

  public static String getOsName(int osType) {
    switch (osType) {
      case OS_LINUX:
        return "lin";
      case OS_WIN:
        return "win";
      case OS_MAC:
        return "mac";
      default:
        return "UNKNOWN";
    }
  }

  public static String getArchName(int archType) {
    switch (archType) {
      case ARCH_32BIT:
        return "32bit";
      case ARCH_64BIT:
        return "64bit";
      default:
        return "UNKNOWN";
    }
  }

  public static boolean isWowAvailable() {
    String value = System.getenv("path");
    if (value != null) {
      System.out.println("path value = " + value);
      if (value.contains("C:\\Windows\\SysWOW64"))
        return true;
    }
    return false;
  }

  public static String getPathWithoutWow() {
    String value = System.getenv("path");
    if (value != null) {
      value = value.replace("C:\\Windows\\SysWOW64", "");
    }
    return value;
  }

  public static Integer compareVersion(String testObject, String compareWith) {
    if (testObject == null) {
      return VERSION_UNKNOWN;
    }
    String testAr[] = testObject.split("\\.");
    String compareAr[] = compareWith.split("\\.");

    int version = VERSION_UNKNOWN;
    for (int i = 0; i < compareAr.length; i++) {
      if (testAr.length < (i + 1)) {
        return VERSION_NEWER;
      }
      Integer numTest = Integer.valueOf(testAr[i]);
      Integer numCompare = Integer.valueOf(compareAr[i]);
      if (numTest < numCompare) {
        return VERSION_NEWER;
      } else if (numTest > numCompare) {
        return VERSION_OLDER;
      } else {
        version = VERSION_EQUAL;
      }
    }
    return version;
  }

  public void setInsensaDocumentPath(String insensaPath) {
    this.insensaDocumentsPath = insensaPath;
    insensaJar = null;
    pluginItemMap = null;
    pluginFileMap = null;
    itemInsensa = null;
    insensaPathFile = null;
  }

  public String getInsensaPath() {
    return insensaDocumentsPath;
  }

  private void loadPluginMaps() throws IOException {
    if (pluginItemMap != null)
      return;

    pluginItemMap = new HashMap<>();
    String path = insensaDocumentsPath;
    if (path == null) {
      path = PathUtils.getInstance().getDocumentsDir().getAbsolutePath();
    }
    File parentPathFile = new File(path + File.separator + "plugins");
    if (!parentPathFile.exists())
      throw new IOException("plugin folder missing");
    File[] pluginArchives = parentPathFile.listFiles();
    for (File pluginArchive : pluginArchives) {
      if (pluginArchive.getName().endsWith(".jar") ||
          pluginArchive.getName().endsWith(".zip")) {
        ItemPlugin itemPlugin = getItemPlugin(pluginArchive);
        if (itemPlugin != null)
          pluginItemMap.put(itemPlugin.getName(), itemPlugin);
      }
    }
  }

  private Integer findJvmArchType() {
    String keys[] = {"sun.arch.data.model",
        "sun.arch.data.model",
        "os.arch"};
    String value = null;
    for (int i = 0; i < keys.length; i++) {
      value = System.getProperty(keys[i]);
      if (value == null)
        continue;
      if (value.contains("32"))
        return JVM_ARCH_32BIT;
      else if (value.contains("64"))
        return JVM_ARCH_64BIT;

    }
    return JVM_ARCH_UNKNOWN;
  }

  public File findInsensaVersionXml() {
    File file = new File(".");
    File[] files = file.listFiles();
    if (files == null || files.length <= 0)
      return null;
    for (int i = 0; i < files.length; i++) {
      if (files[i].getName().equals("xml")) {
        File filesXml[] = files[i].listFiles();
        for (int j = 0; j < filesXml.length; j++) {
          if (filesXml[j].getName().equals("version.xml"))
            return filesXml[j];
        }
      }
    }
    return null;
  }

  public ItemInsensa getItemInsensa() throws IOException, JDOMException {
    if (itemInsensa != null)
      return itemInsensa;

    File xmlFile = findInsensaVersionXml();
    if (xmlFile == null || !xmlFile.exists())
      return null;
    itemInsensa = ItemParser.parseInsensa(xmlFile);
    return itemInsensa;
  }

  public int getOsType() {
    if (osType != null)
      return osType;
    String name = "";
    try {
      if (getItemInsensa() != null) {
        name = getItemInsensa().getItemFile().getOs();
      } else {
        name = System.getProperty("os.name").toLowerCase();
      }
    } catch (IOException | JDOMException e) {
      throw new RuntimeException("Could not find OS type");
    }
    if (name.contains("win"))
      return OS_WIN;
    if (name.contains("mac"))
      return OS_MAC;
    if (name.contains("nux") || name.contains("nix") || name.equals("lin"))
      return OS_LINUX;
    return OS_UNKNOWN;
  }

  public void setOsType(Integer osType) {
    this.osType = osType;
  }

  public String getOs() throws IOException, JDOMException {
    if (osType != null)
      return getOsName(osType);
    String name;
    if (getItemInsensa() != null)
      name = getItemInsensa().getItemFile().getOs();
    else
      name = System.getProperty("os.name").toLowerCase();
    if (name.contains("win"))
      return "win";
    if (name.contains("mac"))
      return "mac";
    if (name.contains("nux") || name.contains("nix") || name.equals("lin"))
      return "lin";
    return null;
  }

  public int getArchType() throws IOException, JDOMException {
    if (getItemInsensa() == null)
      return archType;

    if (itemInsensa.getItemFile().getArch().equals("64bit"))
      archType = ARCH_64BIT;
    else if (itemInsensa.getItemFile().getArch().equals("32bit"))
      archType = ARCH_32BIT;
    return archType;
  }

  public void setArchType(Integer archType) {
    this.archType = archType;
  }

  public String getArch() throws IOException, JDOMException {
    String name = "UNKNOWN";
    if (getItemInsensa() != null)
      return getItemInsensa().getItemFile().getArch();
    else
      return getArchName(archType);
  }

  public Integer getJvmArchType() {
    return jvmArchType;
  }

  public ItemPlugin getItemPlugin(File pluginJar) {
    ZipFile jFile;
    try {
      jFile = new ZipFile(pluginJar);
      ZipEntry entry = jFile.getEntry("xml/version.xml");

      if (entry != null) {
        InputStream is = jFile.getInputStream(entry);
        SAXBuilder parser = new SAXBuilder();
        Document doc = parser.build(is);
        ItemPlugin itemPlugin = new ItemPlugin(doc.getRootElement());
        is.close();
        jFile.close();
        return itemPlugin;
      }
      jFile.close();

    } catch (IOException | JDOMException e) {
      e.printStackTrace();
    }
    return null;
  }

  public ItemPlugin getItemPlugin(String name) throws IOException {
    loadPluginMaps();
    return pluginItemMap.get(name);
  }

  public boolean pluginFileExists(String name) {
    File pluginPathFile = new File(insensaDocumentsPath + File.separator + "plugins");
    if (!pluginPathFile.exists())
      return false;
    File[] jarFiles = pluginPathFile.listFiles();
    if (jarFiles == null || jarFiles.length <= 0)
      return false;
    for (int i = 0; i < jarFiles.length; i++) {
      File file = jarFiles[i];
      if (file.getName().equals(name))
        return true;
    }
    return false;
  }

  public File getPluginFile(String name) {
    File pluginPathFile = new File(insensaDocumentsPath + File.separator + "plugins");
    if (!pluginPathFile.exists())
      return null;
    File[] jarFiles = pluginPathFile.listFiles();
    if (jarFiles == null || jarFiles.length <= 0)
      return null;
    for (File file : jarFiles) {
      if (file.getName().contains(name))
        return file;
    }
    return null;
  }

  public String getOsName() throws IOException, JDOMException {
    return getOsName(getOsType());
  }
}

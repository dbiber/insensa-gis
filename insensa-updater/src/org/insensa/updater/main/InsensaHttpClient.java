/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.main;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.insensa.updater.actions.ActionThreadPool;
import org.insensa.updater.items.ItemFile;
import org.insensa.updater.items.ItemParser;
import org.insensa.updater.items.ItemParserException;
import org.insensa.updater.items.ItemPlugin;
import org.insensa.updater.utils.PathUtils;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

public class InsensaHttpClient {
  public static final String INSENSA_URL = "https://insensa.org/";
  private HttpClient client;
  private VersionChecker versionChecker;
  private HttpEntity entity = null;


  public InsensaHttpClient(VersionChecker versionChecker) {
    this.versionChecker = versionChecker;
    HttpParams params = new BasicHttpParams();
    HttpConnectionParams.setConnectionTimeout(params, 3000);
    HttpConnectionParams.setSoTimeout(params, 3000);
    client = new DefaultHttpClient(params);
  }

  public static String buildRequestString(String osName, String archName, String comp) {
    int deployType = PathUtils.getInstance().getDeployType();
    String requestString = INSENSA_URL + "get_insensa/update.php" + "?os=" +
        osName + "&arch=" + archName + "&comp=" + comp;

    if (deployType == PathUtils.DEPLOY_TESTING) {
      requestString += "&deploy=testing";
    }
    return requestString;
  }

  public static String buildRequestString(VersionChecker versionChecker, String comp)
      throws IOException, JDOMException {
    return buildRequestString(versionChecker.getOsName(), versionChecker.getArch(), comp);
  }

  public Vector<String> getPluginNameList() throws JDOMException, IOException {
    Vector<String> rowVector = new Vector<>();
    if (entity != null) {
      EntityUtils.consume(entity);
      entity = null;
    }
    HttpGet httpGet = new HttpGet(buildRequestString(versionChecker, "plugin_names"));
    HttpResponse response;
    response = client.execute(httpGet);
    int statusCode = response.getStatusLine().getStatusCode();
    if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_ACCEPTED) {
      throw new IOException("Get request to \"" + httpGet.getURI().toString() + "\" result status is " + statusCode);
    }

    entity = response.getEntity();

    if (entity != null) {

      long contentLen = entity.getContentLength();
      if (contentLen <= 0) {
        throw new IOException("There is no content in the response\nPlease check your connection");
      }

      String names = EntityUtils.toString(entity);
      String namesArray[] = names.split(":");
      if (namesArray.length <= 0) {
        throw new IOException("No plugin names found");
      }

      rowVector.addAll(Arrays.asList(namesArray));
      return rowVector;
    }
    return rowVector;
  }

  /**
   * Returns a Vector with Plugin Informations information<br>
   * 0. Column = The PluginName<br>
   * 1. Column = ItemPlugin from the old version OR empty String<br>
   * 2. Column = ItemPlugin from the new version<br>
   * 3. Column = Boolean value to set (useful for tables)<br>
   * 4. Column = old File OR null<br>
   *
   * <b>WARNING: </b>You have to set an an Installation mode in the ActionThreadPool
   * In future Versions, there will be an alternative approach
   */
  public Vector<Object> getPluginRow(String name) throws ItemParserException, IllegalStateException, IOException {

    Vector<Object> rowVector = new Vector<>();
    try {
      if (entity != null) {
        EntityUtils.consume(entity);
        entity = null;
      }

      String arch = versionChecker.getArch();
      String os = versionChecker.getOs();
      if (arch.equals("UNKNOWN"))
        throw new ItemParserException("Arch is UNKNOWN");
      if (os.equals("UNKNOWN"))
        throw new ItemParserException("OS is UNKNOWN");

      HttpGet httpGet = new HttpGet(buildRequestString(versionChecker, "plugin&name=" + name));
      HttpResponse response;
      response = client.execute(httpGet);
      int statusCode = response.getStatusLine().getStatusCode();
      if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_ACCEPTED) {
        throw new ItemParserException("Get request to \"" + httpGet.getURI().toString() + "\" result status is " + statusCode);
      }
      HttpEntity entity = response.getEntity();
      if (entity != null) {
        long contentLen = entity.getContentLength();
        if (contentLen <= 0) {
          throw new IOException("There is no content in the response\nPlease check your connection");
        }
        ItemPlugin itemPlugin = ItemParser.parsePlugin(entity.getContent());

        if (itemPlugin == null)
          throw new ItemParserException("No online ItemPlugin Object created");
        ActionThreadPool actionThreadPool = ActionThreadPool.getInstance();
        rowVector.add(itemPlugin.getName());

        if (actionThreadPool.getInstallMode() == ActionThreadPool.MODE_UPDATE) {
          ItemPlugin oldItem = versionChecker.getItemPlugin(itemPlugin.getName());
          if (oldItem != null) {
            int compareVersion = VersionChecker.compareVersion(oldItem.getVersion(), itemPlugin.getVersion());
            if (compareVersion != VersionChecker.VERSION_NEWER) {
              return null;
            }
          }

          if (oldItem != null) {
            // Check if the name is wrong
            if (versionChecker.pluginFileExists(oldItem.getItemFile().getName())) {
              rowVector.add(oldItem);
              rowVector.add(itemPlugin);
              rowVector.add(Boolean.TRUE);
              rowVector.add(versionChecker.getPluginFile(oldItem.getName()));
            } else {
              File oldFile = versionChecker.getPluginFile(oldItem.getName());
              if (oldFile != null) {
                oldItem.getItemFile().setName(oldFile.getName());
                rowVector.add(oldItem);
                rowVector.add(itemPlugin);
                rowVector.add(Boolean.TRUE);
                rowVector.add(versionChecker.getPluginFile(oldItem.getName()));
              } else {
                throw new ItemParserException("Plugin File " + oldItem.getName() + " notFound");
              }
            }

          } else {
            File oldFile = versionChecker.getPluginFile(itemPlugin.getName());
            if (oldFile == null) {
              rowVector.add("");
              rowVector.add(itemPlugin);
              rowVector.add(Boolean.FALSE);
              rowVector.add(null);
            } else {
              oldItem = new ItemPlugin();
              oldItem.setVersion("unknown");
              oldItem.setName(oldFile.getName());
              ItemFile itemFile = new ItemFile();
              itemFile.setName(oldFile.getName());
              oldItem.setItemFile(itemFile);
              rowVector.add(oldItem);
              rowVector.add(itemPlugin);
              rowVector.add(Boolean.TRUE);
              rowVector.add(oldFile);
            }
          }
        } else
          throw new ItemParserException("No installation mode found");

        return rowVector;
      }
    } catch (ClientProtocolException | JDOMException e) {
      throw new ItemParserException(e);
    }
    return null;
  }

  public HttpEntity getEntity(HttpGet httpGet) throws IOException {
    try {
      HttpResponse response;
      response = client.execute(httpGet);
      int statusCode = response.getStatusLine().getStatusCode();
      if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_ACCEPTED) {
        throw new IOException("Get request to \"" + httpGet.getURI().toString() +
            "\" result status is " + statusCode);
      }
      HttpEntity entity = response.getEntity();
      return entity;
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    }
    return null;
  }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.main;

import org.insensa.updater.components.InstallerView;
import org.insensa.updater.utils.PathUtils;

import java.awt.EventQueue;

public class InsensaUpdater {
  public static void main(String[] args) {
    EventQueue.invokeLater(() -> {
      if (args.length > 0 && args[0].equals("testing")) {
        PathUtils.getInstance().setDeployType(PathUtils.DEPLOY_TESTING);
        System.out.println("Started in testing mode. (not testing)");
      } else {
        System.out.println("Started in default mode");
      }

      VersionChecker vc = new VersionChecker();
      vc.setInsensaDocumentPath(PathUtils.getInstance().getDocumentsDir().getAbsolutePath());
      InstallerView iv = new InstallerView(vc);
      iv.setVisible(true);
    });
  }
}

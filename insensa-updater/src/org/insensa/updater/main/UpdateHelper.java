/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.main;

import org.insensa.updater.actions.ActionThreadPool;
import org.insensa.updater.items.ItemParserException;
import org.insensa.updater.items.ItemPlugin;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

public class UpdateHelper {

  private InsensaHttpClient httpClient;
  private VersionChecker versionChecker;
  private Map<String, File> updateableMap;
  private Map<String, ItemPlugin> newPluginMap;
  private Vector<Object> coreVec;


  public UpdateHelper(InsensaHttpClient httpClient, VersionChecker versionChecker) {
    this.httpClient = httpClient;
    this.versionChecker = versionChecker;
    updateableMap = null;
    newPluginMap = null;
    coreVec = null;
  }

  public void parse() throws JDOMException, IOException {
    updateableMap = new TreeMap<>();
    newPluginMap = new TreeMap<>();
    coreVec = new Vector<>();

    Vector<String> pluginNamesVector = httpClient.getPluginNameList();
    ActionThreadPool.getInstance().setInstallMode(ActionThreadPool.MODE_UPDATE);
    for (String iNames : pluginNamesVector) {
      try {
        Vector<Object> pluginVec = httpClient.getPluginRow(iNames);
        if (pluginVec == null)
          continue;
        String name = ((String) pluginVec.get(0));
        if (pluginVec.get(1) instanceof ItemPlugin) {
          ItemPlugin oldPlugin = (ItemPlugin) pluginVec.get(1);
          ItemPlugin newPlugin = (ItemPlugin) pluginVec.get(2);
          if (oldPlugin.getVersion().equals("unknown")) {
            updateableMap.put(name, (File) pluginVec.get(4));
            continue;
          }

          int compare = VersionChecker.compareVersion(oldPlugin.getVersion(), newPlugin.getVersion());
          if (compare == VersionChecker.VERSION_NEWER) {
            updateableMap.put(name, (File) pluginVec.get(4));
          }
        } else {
          ItemPlugin newPlugin = (ItemPlugin) pluginVec.get(2);
          newPluginMap.put(name, newPlugin);
        }
      } catch (ItemParserException e1) {
        System.out.println("Error Plugin: " + iNames);
        e1.printStackTrace();
      }
    }
  }

  public Map<String, ItemPlugin> getNewPluginMap() throws JDOMException, IOException {
    if (newPluginMap == null) {
      parse();
    }
    return newPluginMap;
  }

  public Map<String, File> getUpdateableMap() throws JDOMException, IOException {
    if (updateableMap == null) {
      parse();
    }
    return updateableMap;
  }

}

package org.insensa.updater.utils;

import java.io.File;

import javax.swing.filechooser.FileSystemView;

public class PathUtils {
  public static final int DEPLOY_DEFAULT = 0;
  public static final int DEPLOY_TESTING = 1;
  private static PathUtils instance = new PathUtils();
  private Integer deployType = DEPLOY_DEFAULT;

  private PathUtils() {
  }

  public static PathUtils getInstance() {
    return instance;
  }

  public Integer getDeployType() {
    return deployType;
  }

  public void setDeployType(Integer deployType) {
    this.deployType = deployType;
  }

  public File getDocumentsDir() {
    File file = new File(FileSystemView
        .getFileSystemView()
        .getDefaultDirectory()
        .getAbsolutePath(), "InsensaGIS");
    if (deployType == DEPLOY_TESTING) {
      file = new File(FileSystemView.getFileSystemView()
          .getDefaultDirectory()
          .getAbsolutePath(), "InsensaGIS-testing");
    }

    if (!file.exists()) {
      file.mkdirs();
    }
    return file;
  }

  public File getWindowsRDir() {
    File file = new File(getDocumentsDir(), "RServe");
    if (!file.exists()) {
      file.mkdirs();
    }
    return file;
  }

  public File getPluginsDir() {
    File pluginsDir = new File(getDocumentsDir(), "plugins");
    if (!pluginsDir.exists()) {
      pluginsDir.mkdirs();
    }
    return pluginsDir;
  }
}

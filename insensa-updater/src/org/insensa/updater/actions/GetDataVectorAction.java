/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.actions;

import org.jdom.JDOMException;

import java.io.IOException;
import java.util.Vector;

import javax.swing.SwingWorker;

import org.insensa.updater.items.ItemParserException;
import org.insensa.updater.main.InsensaHttpClient;

public class GetDataVectorAction {

    private IProgressListener processListener;
    private InsensaHttpClient httpClient;
    private Vector<Vector<Object>> dataVec;


    public GetDataVectorAction(InsensaHttpClient httpClient) {
        this.httpClient = httpClient;
        dataVec = new Vector<>();
    }

    synchronized public Vector<Vector<Object>> getDataVec() {
        return dataVec;
    }

    public void start() {
        new SwingWorker<String, String>() {

            @Override
            protected String doInBackground() {
                try {
                    if (processListener != null)
                        processListener.start();
                    Vector<String> pluginNamesVector = httpClient.getPluginNameList();
                    for (String iNames : pluginNamesVector) {
                        try {

                            Vector<Object> pluginVec = httpClient.getPluginRow(iNames);
                            if (pluginVec != null)
                                dataVec.add(pluginVec);
                        } catch (ItemParserException e1) {
                            if (processListener != null)
                                processListener.minorError(e1.getMessage());
                            e1.printStackTrace();
                        }
                    }
                    if (dataVec.isEmpty()) {
                        if (processListener != null)
                            processListener.error("No Data Available");
                    } else {
                        if (processListener != null)
                            processListener.end();
                    }

                } catch (IOException | JDOMException e) {
                    if (processListener != null)
                        processListener.error(e);
                }
                return "done";
            }
        }.execute();
    }

    public void setProcessListener(IProgressListener processListener) {
        this.processListener = processListener;
    }


}

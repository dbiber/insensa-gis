/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;
import javax.swing.plaf.SliderUI;

import org.apache.http.client.methods.HttpGet;
import org.jdom.JDOMException;

import org.insensa.updater.main.InsensaHttpClient;
import org.insensa.updater.main.VersionChecker;

public abstract class AbstractDownloadAction {
    protected InsensaHttpClient httpClient;
    protected VersionChecker versionChecker;
    protected List<IProgressListener> progressListenerList;
    protected String installationPath;
    protected int installationMode;


    public AbstractDownloadAction(InsensaHttpClient httpClient, VersionChecker versionChecker) {
        this.httpClient = httpClient;
        this.versionChecker = versionChecker;
        progressListenerList = new ArrayList<>();

    }

    public void addProgressListener(IProgressListener progressListener) {
        this.progressListenerList.add(progressListener);
    }

    public String getInstallationPath() {
        return installationPath;
    }

    public void setInstallationPath(String installationPath) {
        this.installationPath = installationPath;
    }

    public int getInstallationMode() {
        return installationMode;
    }

    public void setInstallationMode(int installationMode) {
        this.installationMode = installationMode;
    }

    public abstract void execute() throws IOException, InterruptedException, JDOMException;
}

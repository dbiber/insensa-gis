/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.actions;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.insensa.updater.items.ItemPlugin;
import org.insensa.updater.main.InsensaHttpClient;
import org.insensa.updater.main.InsensaUpdater;
import org.insensa.updater.main.VersionChecker;
import org.insensa.updater.utils.PathUtils;

public class PluginDownloadAction extends AbstractDownloadAction {
  private ItemPlugin itemNewPlugin;
  private ItemPlugin itemOldPlugin;

  public PluginDownloadAction(InsensaHttpClient httpClient, VersionChecker versionChecker) {
    super(httpClient, versionChecker);
  }

  @Override
  public void execute() throws IOException, InterruptedException {
    String pluginName = itemNewPlugin.getName();
    String pluginFileName = itemNewPlugin.getItemFile().getName();
    String pluginSuffix = FilenameUtils.getExtension(pluginFileName);
    String remotePluginPath = InsensaHttpClient.INSENSA_URL + "get_insensa/plugins/all/" +
        pluginName + "/" + pluginFileName;
    if (PathUtils.getInstance().getDeployType() == PathUtils.DEPLOY_TESTING) {
      remotePluginPath = InsensaHttpClient.INSENSA_URL + "get_insensa/plugins/all/test/" +
          pluginName + "/" + pluginFileName;
    }
    HttpGet httpGet = new HttpGet(remotePluginPath);
    HttpEntity entity = httpClient.getEntity(httpGet);
    if (entity != null) {
      long contentLen = entity.getContentLength();
      if (contentLen <= 0) {
        throw new IOException("Content len=" + contentLen + " ; request=" + httpGet.getURI().toString());
      }

      String outputPath = versionChecker.getInsensaPath();
      if (!outputPath.endsWith(File.separator)) {
        outputPath += File.separator;
      }
      String tmpFolder = outputPath + "tmp" + File.separator;
      File tmpFolderFile = new File(tmpFolder);
      if (!tmpFolderFile.exists()) {
        if (!tmpFolderFile.mkdir()) {
          throw new IOException("Error creating Folder " + tmpFolderFile.getAbsolutePath());
        }
      }

      File outputFilePath = new File(outputPath + "tmp" +
          File.separator +
          pluginName +
          "." +
          pluginSuffix);

      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputFilePath));
      long fileSize = entity.getContentLength();
      InputStream inputStream = entity.getContent();

      for (IProgressListener iProgress: progressListenerList) {
        iProgress.setText("Downloading " + pluginName);
      }

      byte[] input = new byte[4096];
      int buf_len = 4096;
      int len = 0;
      int offset = 0;
      Integer progress;
      while ((len = inputStream.read(input, 0, buf_len)) > 0) {
        offset += len;
        bos.write(input, 0, len);
        progress = new Integer((int) Math.round(((double) offset / (double) fileSize) * 100.0));
        for (IProgressListener iListener: progressListenerList) {
          iListener.setProgress(progress);
        }
        Thread.sleep(2);
      }
      inputStream.close();
      bos.flush();
      bos.close();
      for (IProgressListener iProgress: progressListenerList) {
        iProgress.setText("Downloading " + pluginName + " Done");
      }

      if (installationMode == ActionThreadPool.MODE_UPDATE) {
        startUpdate(super.installationPath, outputFilePath);
      }
    }
  }

  private void startUpdate(String installPath, File sourceFile) throws IOException {
    String targetDir = installPath;
    if (!targetDir.endsWith(File.separator)) {
      targetDir += File.separator;
    }
    File targetFile;
    if (itemOldPlugin != null) {
      targetFile = new File(targetDir + "plugins" + File.separator + itemOldPlugin.getItemFile().getName());
      if (targetFile.exists()) {
        FileUtils.forceDelete(targetFile);
      } else {
        throw new IOException("ERROR: " + targetFile.getAbsolutePath() + " not exists");
      }
    } else {
      targetFile = new File(targetDir + "plugins" + File.separator + sourceFile.getName());
      if (targetFile.exists()) {
        FileUtils.forceDelete(targetFile);
      }
    }
    targetFile = new File(targetDir + "plugins" + File.separator + sourceFile.getName());
    if (!sourceFile.renameTo(targetFile)) {
      throw new IOException("Error moving file :\n"
          + sourceFile.getAbsolutePath() + "\nTo\n"
          + targetFile.getAbsolutePath());
    }
  }

  /**
   * @param itemNewPlugin the itemNewPlugin to set
   */
  public void setItemNewPlugin(ItemPlugin itemNewPlugin) {
    this.itemNewPlugin = itemNewPlugin;
  }

  /**
   * @param itemOldPlugin the itemOldPlugin to set
   */
  public void setItemOldPlugin(ItemPlugin itemOldPlugin) {
    this.itemOldPlugin = itemOldPlugin;
  }

}

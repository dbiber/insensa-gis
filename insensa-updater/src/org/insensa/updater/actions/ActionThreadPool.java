/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.actions;

import org.insensa.updater.utils.PathUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ActionThreadPool {

    private static ActionThreadPool actionThreadPool = new ActionThreadPool();
    private CoreDownloadAction coreDownloadAction;
    private List<PluginDownloadAction> pluginDownloadActionList;
    private String installationPath;
    private int installMode;
    private List<IProgressListener> progressListenerList;

    public static final int MODE_UNKNOWN = 0;
    public static final int MODE_UPDATE = 1;

    private ActionThreadPool() {
        coreDownloadAction = null;
        pluginDownloadActionList = null;
        installMode = MODE_UPDATE;
        progressListenerList = new ArrayList<>();
    }

    synchronized public static ActionThreadPool getInstance() {
        return actionThreadPool;
    }

    public void addProgressListener(IProgressListener progressListener) {
        progressListenerList.add(progressListener);
    }

    public void executeAll() {
        new SwingWorker<Object, Object>() {

            @Override
            protected Object doInBackground() {
                try {
                    notifyStart();
                    if (installMode == MODE_UNKNOWN) {
                        throw new IOException("Unknown install mode");
                    }

                    if (installationPath == null || installationPath.isEmpty()) {
                        installationPath = PathUtils.getInstance().getDocumentsDir().getAbsolutePath();
                    }


                    if (coreDownloadAction != null) {
                        coreDownloadAction.setInstallationPath(installationPath);
                        coreDownloadAction.setInstallationMode(installMode);
                        coreDownloadAction.execute();
                    }
                    if (pluginDownloadActionList != null) {
                        for (PluginDownloadAction iAction : pluginDownloadActionList) {
                            iAction.setInstallationPath(installationPath);
                            iAction.setInstallationMode(installMode);
                            iAction.execute();
                        }
                    }
                    String path = installationPath;
                    if (!path.endsWith(File.separator))
                        path += File.separator;
                    path += "tmp";
                    new File(path).delete();
                    notifyEnd();
                } catch (IOException | InterruptedException e) {
                    printError(e);
                }
                return null;
            }
        }.execute();
    }

    private void notifyStart() {
        for (IProgressListener iProgressListener : progressListenerList)
            iProgressListener.start();
    }

    private void notifyEnd() {
        for (IProgressListener iProgressListener : progressListenerList)
            iProgressListener.end();
    }

    private void printError(String message) {
        for (IProgressListener iProgressListener : progressListenerList)
            iProgressListener.error(message);
    }

    private void printError(Exception e) {
        for (IProgressListener iProgressListener : progressListenerList)
            iProgressListener.error(e);
    }

    public void setCoreDownloadAction(CoreDownloadAction coreDownloadAction) {
        this.coreDownloadAction = coreDownloadAction;
    }

    public void addPluginDownloadAction(PluginDownloadAction action) {
        if (pluginDownloadActionList == null)
            pluginDownloadActionList = new ArrayList<PluginDownloadAction>();
        pluginDownloadActionList.add(action);
    }

    public String getInstallationPath() {
        return installationPath;
    }

    public void setInstallationPath(String installationPath) {
        this.installationPath = installationPath;
    }

    public int getInstallMode() {
        return installMode;
    }

    public void setInstallMode(int installMode) {
        this.installMode = installMode;
    }
}

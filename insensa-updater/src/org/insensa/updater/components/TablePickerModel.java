/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.components;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.insensa.updater.items.ItemInsensa;
import org.insensa.updater.items.ItemPlugin;

public class TablePickerModel extends DefaultTableModel {

    public TablePickerModel(Vector data, Vector title) {
        super.setDataVector(data, title);
    }

    private Class[] types = new Class[]
            {
                    java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
    private boolean[] canEdit = new boolean[]
            {
                    false, false, false, true
            };

    public Object getSourceItem(int row) {
        if (row > -1)
            return super.getValueAt(row, 2);
        else
            return null;
    }

    public Object getTargetItem(int row) {
        if (row > -1)
            return super.getValueAt(row, 1);
        else
            return null;
    }

    public Vector<Object> getSelectedSourceItems() {
        Vector<Object> objectVec = new Vector<Object>();
        for (int i = 0; i < getRowCount(); i++) {
            if (((Boolean) getValueAt(i, 3)) == true) {
                objectVec.add(getSourceItem(i));
            }
        }
        return objectVec;
    }

    public Vector<Object> getSelectedTargetItems() {
        Vector<Object> objectVec = new Vector<Object>();
        for (int i = 0; i < getRowCount(); i++) {
            if (((Boolean) getValueAt(i, 3)) == true) {
                objectVec.add(getTargetItem(i));
            }
        }
        return objectVec;
    }

    public void clearTable() {
        int cnt = super.getRowCount();
        for (int i = 0; i < cnt; i++)
            super.removeRow(0);
    }

    public void clearAllCheckButtons() {
        int cnt = super.getRowCount();
        for (int i = 0; i < cnt; i++) {
            super.setValueAt(Boolean.FALSE, i, 3);
        }
    }

    public void setAllCheckButtons() {
        int cnt = super.getRowCount();
        for (int i = 0; i < cnt; i++) {
            super.setValueAt(Boolean.TRUE, i, 3);
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (row <= -1 || column <= -1)
            return null;
        if (column == 2 || column == 1) {
            Object tmpObj = super.getValueAt(row, column);
            if (tmpObj instanceof ItemInsensa)
                return ((ItemInsensa) tmpObj).getVersion();
            else if (tmpObj instanceof ItemPlugin)
                return ((ItemPlugin) tmpObj).getVersion();
            else
                return tmpObj;
        } else
            return super.getValueAt(row, column);
    }

    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[columnIndex];
    }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.components;

import com.install4j.api.launcher.ApplicationLauncher;
import org.insensa.updater.actions.IProgressListener;
import org.insensa.updater.main.VersionChecker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;


public class BottomPanel extends JPanel implements IProgressListener {
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonFinish;
    private javax.swing.JSeparator seperator;
    private JPanel cardsPanel;
    private int currentCard = 0;
    private VersionChecker versionChecker;


    public BottomPanel(JPanel cardsPanel, VersionChecker versionChecker) {
        this.cardsPanel = cardsPanel;
        this.versionChecker = versionChecker;

        initComponents();
    }

    private void initComponents() {
        seperator = new javax.swing.JSeparator();
        buttonCancel = new javax.swing.JButton();
        buttonFinish = new javax.swing.JButton();

        buttonCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonCancel.setText("Cancel");

        buttonCancel.addActionListener(e -> {
        });

        buttonFinish.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonFinish.setText("Finish");

        javax.swing.GroupLayout panelBottomLayout = new javax.swing.GroupLayout(this);
        this.setLayout(panelBottomLayout);
        panelBottomLayout.setHorizontalGroup(panelBottomLayout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(
                        javax.swing.GroupLayout.Alignment.TRAILING,
                        panelBottomLayout.createSequentialGroup()
                                .addContainerGap(400, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(buttonFinish)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(buttonCancel).addGap(6, 6, 6))
                .addComponent(seperator));
        panelBottomLayout.setVerticalGroup(panelBottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                panelBottomLayout
                        .createSequentialGroup()
                        .addComponent(seperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addGroup(
                                panelBottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(buttonFinish)
                                        .addComponent(buttonCancel)
                        ).addContainerGap()));
        buttonFinish.setEnabled(false);
    }

    public javax.swing.JButton getButtonCancel() {
        return buttonCancel;
    }

    public javax.swing.JButton getButtonFinish() {
        return buttonFinish;
    }

    public javax.swing.JSeparator getSeperator() {
        return seperator;
    }

    public JPanel getCardsPanel() {
        return cardsPanel;
    }

    public int getCurrentCard() {
        return currentCard;
    }

    @Override
    public void start() {
        SwingUtilities.invokeLater(() -> {
            buttonFinish.setEnabled(false);
        });
    }

    @Override
    public void end() {
        SwingUtilities.invokeLater(() -> {
            buttonCancel.setText("Close");
            buttonFinish.setEnabled(true);
            buttonFinish.setText("Restart InsensaGIS");
            ActionListener[] all = buttonFinish.getActionListeners();
            if (all != null) {
                for (int i = 0; i < all.length; i++)
                    buttonFinish.removeActionListener(all[i]);
            }
            buttonFinish.addActionListener(e -> {
                try {
                    ApplicationLauncher.launchApplication("103", null, true, null);
                } catch (IOException e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog(BottomPanel.this,
                            "Please restart InsensaGIS manually", "Restart Problem", JOptionPane.WARNING_MESSAGE);

                }
            });
        });
    }

    @Override
    public void setProgress(int progress) {
    }

    @Override
    public void setText(String text) {
    }

    @Override
    public void error(final String message) {
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(BottomPanel.this, message, "ERROR", JOptionPane.ERROR_MESSAGE));
    }

    @Override
    public void error(final Exception e) {
        SwingUtilities.invokeLater(() -> {
            JOptionPane.showMessageDialog(BottomPanel.this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        });

    }

    public class ExecThread implements Runnable {

        private String path;

        public void setPath(String path) {
            this.path = path;
        }

        @Override
        public void run() {
            java.lang.ProcessBuilder pb = new ProcessBuilder(path + File.separator + "insensa.exe");
            if (versionChecker.getJvmArchType() == VersionChecker.JVM_ARCH_64BIT && VersionChecker.isWowAvailable()) {
                pb.environment().put("path", VersionChecker.getPathWithoutWow());
            }
            pb.directory(new File(path));
            try {
                java.lang.Process p = pb.start();
            } catch (IOException e) {
                error(e);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void minorError(String message) {
    }
}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.components;

import org.jdom.JDOMException;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import org.insensa.updater.actions.ActionThreadPool;
import org.insensa.updater.actions.PluginDownloadAction;
import org.insensa.updater.items.ItemPlugin;
import org.insensa.updater.main.InsensaHttpClient;
import org.insensa.updater.main.VersionChecker;

public class InstallerView extends JFrame {
    //MAIN
    private CardLayout cardLayout;
    private javax.swing.JPanel panelCardContainer;
    protected HTMLEditorKit kit;
    protected StyleSheet styleSheet;
    protected HTMLDocument doc;
    protected Element body;

    //HEAD
    private javax.swing.JScrollPane scrollPaneHead;
    private javax.swing.JEditorPane editorPane;

    // BOTTOM
    private BottomPanel bottomPanel;


    // Middle
    private DownloadPickerPanel downloadPickerPanel;
    private ProgressPanel progressPanel;

    private VersionChecker versionChecker;
    private InsensaHttpClient httpClient;

    public InstallerView(VersionChecker versionChecker) {
        this.versionChecker = versionChecker;
        httpClient = new InsensaHttpClient(versionChecker);
        super.setTitle("Insensa GIS - Installer/Updater");
        initComponents();
    }

    /**
     *
     */
    public InstallerView() {
        versionChecker = new VersionChecker();
        httpClient = new InsensaHttpClient(versionChecker);
        super.setTitle("Insensa GIS - Installer/Updater");
        initComponents();
    }

    private void initComponents() {
        panelCardContainer = new javax.swing.JPanel();
        downloadPickerPanel = new DownloadPickerPanel(httpClient);
        bottomPanel = new BottomPanel(panelCardContainer, versionChecker);
        bottomPanel.getButtonCancel().addActionListener(e -> InstallerView.this.dispose());

        downloadPickerPanel.setBottomPanel(bottomPanel);


        bottomPanel.getButtonFinish().addActionListener(e -> {

            TablePickerModel model = (TablePickerModel) downloadPickerPanel.getTablePicker().getModel();
            ActionThreadPool actionThreadPool = ActionThreadPool.getInstance();
            actionThreadPool.addProgressListener(bottomPanel);
            Vector<Object> selSourceItems = model.getSelectedSourceItems();
            Vector<Object> selTargetItems = model.getSelectedTargetItems();
            progressPanel = new ProgressPanel(httpClient, versionChecker, selSourceItems.size());
            progressPanel.setBottomPanel(bottomPanel);
            panelCardContainer.add(progressPanel, "card2");
            for (int i = 0; i < selSourceItems.size(); i++) {
                Object iObj = selSourceItems.get(i);
                Object iObjTarget = selTargetItems.get(i);
                if (iObj instanceof ItemPlugin) {
                    PluginDownloadAction da = new PluginDownloadAction(httpClient, versionChecker);
                    InsensaProgressBar tmpBar = progressPanel.getProgressBar(i);
                    da.addProgressListener(tmpBar);
                    da.setItemNewPlugin((ItemPlugin) iObj);
                    if (iObjTarget instanceof ItemPlugin)
                        da.setItemOldPlugin((ItemPlugin) iObjTarget);
                    else
                        da.setItemOldPlugin(null);
                    actionThreadPool.addPluginDownloadAction(da);
                }
            }

            CardLayout cl = (CardLayout) panelCardContainer.getLayout();
            cl.show(panelCardContainer, "card2");

            actionThreadPool.executeAll();
        });


        this.editorPane = new javax.swing.JEditorPane();
        scrollPaneHead = new JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        this.scrollPaneHead.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        this.scrollPaneHead.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        this.scrollPaneHead.setViewportView(this.editorPane);

        this.editorPane.setEditable(false);
        this.editorPane.setContentType("text/html");
        try {
            this.initHead();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        setHeadTitle("Insensa GIS - Installer/Updater");

        cardLayout = new CardLayout();
        panelCardContainer.setLayout(cardLayout);
        panelCardContainer.add(downloadPickerPanel, "card1");


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(bottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(scrollPaneHead, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(panelCardContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(scrollPaneHead, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 392, Short.MAX_VALUE)
                                .addComponent(bottomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(78, 78, 78)
                                        .addComponent(panelCardContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(50, 50, 50)))
        );
        pack();
    }

    private void initHead() throws IOException, URISyntaxException {
        this.kit = new HTMLEditorKit();
        this.editorPane.setEditorKit(this.kit);
        this.editorPane.setBorder(null);

        this.styleSheet = this.kit.getStyleSheet();
        URL imageUrl = getClass().getResource("/images/PanelHead2.png");
        if (imageUrl != null) {
            URI imageUri = getClass().getResource("/images/PanelHead2.png").toURI();
            this.styleSheet.addRule("body {color:#000; font-family:times;"
                    + "margin:5px;"
                    + "border:0px;"
                    + " background-image:url(" + imageUri
                    + ");" + "background-repeat:no-repeat;" + "background-position:right top;" + "}");
        }
        this.styleSheet.addRule("h4 {color: black;" + "align=left;}");
        this.kit.setStyleSheet(this.styleSheet);

        this.doc = (HTMLDocument) this.kit.createDefaultDocument();
        this.editorPane.setDocument(this.doc);
        Element[] roots = this.doc.getRootElements();// #0 is the HTML element,
        // #1
        for (int i = 0; i < roots[0].getElementCount(); i++) {
            Element element = roots[0].getElement(i);
            if (element.getAttributes().getAttribute(StyleConstants.NameAttribute) == HTML.Tag.BODY) {
                this.body = element;
                break;
            }
        }
        if (this.body == null)
            throw new IOException("can't find <body>");

        this.styleSheet.removeStyle("body");
        this.kit.setStyleSheet(this.styleSheet);
    }

    public void setHeadTitle(String title) {
        try {
            this.kit.setStyleSheet(this.styleSheet);
            String newTitle = "<h4>" + title + "</h4>";
            this.doc.insertAfterStart(this.body, newTitle);
            this.styleSheet.removeStyle("body");
            this.kit.setStyleSheet(this.styleSheet);
        } catch (BadLocationException | IOException e) {
        }
    }


}

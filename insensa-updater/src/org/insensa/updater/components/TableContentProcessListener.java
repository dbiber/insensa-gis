/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.components;

import java.util.Vector;

import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.insensa.updater.actions.GetDataVectorAction;
import org.insensa.updater.actions.IProgressListener;

public class TableContentProcessListener implements IProgressListener {

    private GetDataVectorAction action;
    private TablePickerModel model;
    private JDialog dialog;
    private JComponent parent;


    public TableContentProcessListener(GetDataVectorAction action, TablePickerModel model, JComponent parent) {
        this.action = action;
        this.model = model;
        this.parent = parent;
        Object[] options = {"Close"};
        JOptionPane optionPane = new JOptionPane("Connecting... Please Wait!",
                JOptionPane.INFORMATION_MESSAGE,
                JOptionPane.OK_CANCEL_OPTION,
                null, options, options[0]);
        dialog = optionPane.createDialog(parent, "Connecting...");
    }

    synchronized public JDialog getDialog() {
        return dialog;
    }

    @Override
    public void start() {
        SwingUtilities.invokeLater(() -> getDialog().setVisible(true));

    }

    @Override
    public void end() {
        SwingUtilities.invokeLater(() -> {
            if (getDialog().isVisible())
                getDialog().setVisible(false);
            Vector<String> titleVec = new Vector<>();
            titleVec.add("Name");
            titleVec.add("Old Version");
            titleVec.add("New Version");
            titleVec.add("Install");

            Vector<Vector<Object>> dataVec = new Vector<>();
            dataVec.addAll(action.getDataVec());
            model.setDataVector(dataVec, titleVec);
        });
    }

    @Override
    public void setProgress(int progress) {
    }

    @Override
    public void setText(String text) {
    }

    @Override
    public void error(final String message) {
        SwingUtilities.invokeLater(() -> {
            if (getDialog().isVisible())
                getDialog().setVisible(false);
            JOptionPane.showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
        });

    }

    @Override
    public void error(final Exception e) {
        SwingUtilities.invokeLater(() -> {
            if (getDialog().isVisible())
                getDialog().setVisible(false);
            JOptionPane.showMessageDialog(parent, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        });
    }

    @Override
    public void minorError(final String message) {
        SwingUtilities.invokeLater(() -> {
            if (getDialog().isVisible())
                getDialog().setVisible(false);
            JOptionPane.showMessageDialog(parent, message, "Error", JOptionPane.WARNING_MESSAGE);
        });
    }

}

/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.components;

import org.insensa.updater.main.InsensaHttpClient;
import org.insensa.updater.main.VersionChecker;

import javax.swing.GroupLayout.Group;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.util.ArrayList;
import java.util.List;

public class ProgressPanel extends javax.swing.JPanel {

    private javax.swing.JPanel panelProgress;
    private javax.swing.JScrollPane scrollPaneProgress;
    private InsensaHttpClient httpClient;
    private VersionChecker versionChecker;
    private BottomPanel bottomPanel;
    private int progressCnt;
    private List<InsensaProgressBar> progressBarList;

    public ProgressPanel(InsensaHttpClient httpClient, VersionChecker versionChecker, int progressCnt) {
        this.versionChecker = versionChecker;
        this.httpClient = httpClient;
        this.progressCnt = progressCnt;
        this.progressBarList = new ArrayList<>();
        initComponents();
    }

    public InsensaProgressBar getProgressBar(int num) {
        return progressBarList.get(num);
    }

    private void initComponents() {

        scrollPaneProgress = new javax.swing.JScrollPane();
        panelProgress = new javax.swing.JPanel();
        for (int i = 0; i < progressCnt; i++) {
            InsensaProgressBar pb = new InsensaProgressBar();
            pb.setBackground(new java.awt.Color(255, 255, 255));
            pb.setStringPainted(true);
            progressBarList.add(pb);
        }

        panelProgress.setBorder(javax.swing.BorderFactory.createTitledBorder("Progress"));

        javax.swing.GroupLayout panelProgressLayout = new javax.swing.GroupLayout(panelProgress);
        panelProgress.setLayout(panelProgressLayout);

        Group horBarGroup = panelProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (InsensaProgressBar iBar : progressBarList) {
            horBarGroup.addComponent(iBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }
        panelProgressLayout.setHorizontalGroup(
                panelProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelProgressLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(horBarGroup)
                                .addContainerGap())
        );


        SequentialGroup verBarGroup = panelProgressLayout.createSequentialGroup();
        verBarGroup.addContainerGap();
        for (InsensaProgressBar iBar : progressBarList) {
            verBarGroup.addComponent(iBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                    javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE);
            verBarGroup.addPreferredGap(ComponentPlacement.RELATED);
        }
        verBarGroup.addContainerGap();

        panelProgressLayout.setVerticalGroup(
                panelProgressLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(verBarGroup)
        );

        scrollPaneProgress.setViewportView(panelProgress);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(scrollPaneProgress)
                                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(scrollPaneProgress)
        );
    }

    public void setBottomPanel(BottomPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }
}


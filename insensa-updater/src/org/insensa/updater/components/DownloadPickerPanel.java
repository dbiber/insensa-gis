/*
 * Copyright (C) 2011 Dennis Biber
 *
 * Insensa-GIS - http://www.insensa.org
 *
 * Insensa-GIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3 of the License,
 * or (at your option) any later version.
 *
 * Insensa-GIS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Insensa-GIS.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.insensa.updater.components;

import org.insensa.updater.actions.GetDataVectorAction;
import org.insensa.updater.items.ItemInsensa;
import org.insensa.updater.items.ItemPlugin;
import org.insensa.updater.main.InsensaHttpClient;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class DownloadPickerPanel extends JPanel {
  private static final long serialVersionUID = 3472662544865564721L;
  private javax.swing.JCheckBox checkBoxDetails;
  private javax.swing.JLabel labelChooseComp;
  private javax.swing.JPanel panelPicker;
  private javax.swing.JScrollPane scrollPaneDescription;
  private javax.swing.JScrollPane scrollPaneTablePicker;
  private javax.swing.JSplitPane splitPanePicker;
  private javax.swing.JTable tablePicker;
  private javax.swing.JEditorPane editorPaneDescription;
  private BottomPanel bottomPanel;
  private InsensaHttpClient httpClient;
  private JButton buttonClearAll;
  private JButton buttonSetAll;


  public DownloadPickerPanel(InsensaHttpClient httpClient) {
    this.httpClient = httpClient;
    initComponents();
  }


  private void initComponents() {
    panelPicker = this;
    splitPanePicker = new javax.swing.JSplitPane();
    scrollPaneTablePicker = new javax.swing.JScrollPane();
    tablePicker = new javax.swing.JTable();
    scrollPaneDescription = new javax.swing.JScrollPane();
    editorPaneDescription = new javax.swing.JEditorPane();
    checkBoxDetails = new javax.swing.JCheckBox();
    labelChooseComp = new javax.swing.JLabel();
    buttonClearAll = new JButton();
    buttonSetAll = new JButton();

    editorPaneDescription.setEditable(false);
    editorPaneDescription.setContentType("text/html");

    splitPanePicker.setBackground(new java.awt.Color(255, 255, 255));
    splitPanePicker.setResizeWeight(1.0);
    splitPanePicker.setOneTouchExpandable(true);
    splitPanePicker.setDividerLocation(this.getSize().width);

    checkBoxDetails.addActionListener(e -> {
      if (checkBoxDetails.isSelected()) {
        scrollPaneDescription.setVisible(true);
        splitPanePicker.setDividerLocation(0.6);
      } else {
        scrollPaneDescription.setVisible(false);
        splitPanePicker.setDividerLocation(DownloadPickerPanel.this.getSize().width);
      }
    });

    buttonClearAll.setFont(new java.awt.Font("Dialog", 0, 12));
    buttonClearAll.setText("Clear All");
    buttonClearAll.addActionListener(e -> {
      TablePickerModel model = ((TablePickerModel) tablePicker.getModel());
      model.clearAllCheckButtons();
      tablePicker.revalidate();
    });

    buttonSetAll.setFont(new java.awt.Font("Dialog", 0, 12));
    buttonSetAll.setText("Set All");
    buttonSetAll.addActionListener(e -> {
      TablePickerModel model = ((TablePickerModel) tablePicker.getModel());
      model.setAllCheckButtons();
      tablePicker.revalidate();
    });

    scrollPaneTablePicker.setBackground(new java.awt.Color(204, 204, 204));
    scrollPaneTablePicker.setOpaque(false);

    this.addAncestorListener(new AncestorListener() {
      @Override
      public void ancestorRemoved(AncestorEvent event) {
      }

      @Override
      public void ancestorMoved(AncestorEvent event) {
      }

      @Override
      public void ancestorAdded(AncestorEvent event) {
        checkControlls();
        ((TablePickerModel) tablePicker.getModel()).clearTable();
        GetDataVectorAction action = new GetDataVectorAction(httpClient);
        TableContentProcessListener listener = new TableContentProcessListener(action, (TablePickerModel) tablePicker.getModel(), DownloadPickerPanel.this);
        action.setProcessListener(listener);
        action.start();
      }
    });

    Vector<String> titleVec = new Vector<>();
    titleVec.add("Name");
    titleVec.add("Old Version");
    titleVec.add("New Version");
    titleVec.add("Install");

    Vector<Vector<Object>> dataVec = new Vector<>();

    TablePickerModel model = new TablePickerModel(dataVec, titleVec);
    tablePicker.setModel(model);
    tablePicker.setFillsViewportHeight(true);
    tablePicker.setGridColor(new java.awt.Color(204, 204, 204));
    tablePicker.getTableHeader().setReorderingAllowed(false);
    scrollPaneTablePicker.setViewportView(tablePicker);
    tablePicker.getColumnModel().getColumn(1).setResizable(false);
    tablePicker.getColumnModel().getColumn(2).setResizable(false);
    tablePicker.getColumnModel().getColumn(3).setResizable(false);
    tablePicker.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tablePicker.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      Integer row = -1;

      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          row = tablePicker.getSelectedRow();
          TablePickerModel model = (TablePickerModel) tablePicker.getModel();
          Object tmpObj = model.getSourceItem(row);
          if (tmpObj instanceof ItemPlugin)
            editorPaneDescription.setText(((ItemPlugin) tmpObj).getItemDescription().getText());
          else if (tmpObj instanceof ItemInsensa)
            editorPaneDescription.setText(((ItemInsensa) tmpObj).getItemDescription().getText());
          else
            editorPaneDescription.setText("No Description Available for " + tablePicker.getModel().getValueAt(row, 0));
        }
        checkControlls();
      }
    });

    tablePicker.getModel().addTableModelListener(e -> checkControlls());

    splitPanePicker.setLeftComponent(scrollPaneTablePicker);

    scrollPaneDescription.setViewportView(editorPaneDescription);

    splitPanePicker.setRightComponent(scrollPaneDescription);
    scrollPaneDescription.setVisible(false);

    checkBoxDetails.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    checkBoxDetails.setText("Show Details");

    labelChooseComp.setText("Choose Components:");

    javax.swing.GroupLayout panelPickerLayout = new javax.swing.GroupLayout(panelPicker);
    panelPicker.setLayout(panelPickerLayout);
    panelPickerLayout.setHorizontalGroup(
        panelPickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPickerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(splitPanePicker, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                    .addGroup(panelPickerLayout.createSequentialGroup()
                        .addComponent(labelChooseComp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(checkBoxDetails))
                    .addGroup(panelPickerLayout.createSequentialGroup()
                        .addComponent(buttonSetAll)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonClearAll)))
                .addContainerGap())
    );

    panelPickerLayout.setVerticalGroup(
        panelPickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPickerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkBoxDetails)
                    .addComponent(labelChooseComp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(splitPanePicker, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPickerLayout.createParallelGroup()
                    .addComponent(buttonSetAll)
                    .addComponent(buttonClearAll))
                .addContainerGap())
    );

  }

  public void setBottomPanel(BottomPanel bottomPanel) {
    this.bottomPanel = bottomPanel;
  }

  public void checkControlls() {
    if (((TablePickerModel) tablePicker.getModel()).getSelectedSourceItems().size() >= 1) {
      bottomPanel.getButtonFinish().setEnabled(true);
    } else {
      bottomPanel.getButtonFinish().setEnabled(false);
    }
  }

  public JTable getTablePicker() {
    return tablePicker;
  }
}
